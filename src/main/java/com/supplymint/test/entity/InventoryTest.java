package com.supplymint.test.entity;

public class InventoryTest {
	private int id;
	private String invId;
	private String invName;
	private String brandName;
	private String type;

	public InventoryTest() {

	}

	public InventoryTest(int id, String invId, String invName, String brandName, String type) {
		super();
		this.id = id;
		this.invId = invId;
		this.invName = invName;
		this.brandName = brandName;
		this.type = type;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getInvId() {
		return invId;
	}

	public void setInvId(String invId) {
		this.invId = invId;
	}

	public String getInvName() {
		return invName;
	}

	public void setInvName(String invName) {
		this.invName = invName;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "InventoryTest [id=" + id + ", invId=" + invId + ", invName=" + invName + ", brandName=" + brandName
				+ ", type=" + type + "]";
	}

}
