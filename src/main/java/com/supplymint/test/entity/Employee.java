package com.supplymint.test.entity;

import com.supplymint.util.JsonField;

public class Employee {

	@JsonField(msg = "name not empty")
	//@NotNull
	//@JsonProperty(required=true)
	private String name;

	@JsonField
	private String city;

	public Employee() {
		// TODO Auto-generated constructor stub
	}

	public Employee(String name, String city) {
		super();
		this.name = name;
		this.city = city;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Override
	public String toString() {
		return "Employee [name=" + name + ", city=" + city + "]";
	}

}
