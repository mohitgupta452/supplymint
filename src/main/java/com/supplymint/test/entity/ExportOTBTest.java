package com.supplymint.test.entity;

public class ExportOTBTest {

	private String assortmentCode;
	private String metric;
	private float billDate;

	public ExportOTBTest() {

	}

	public ExportOTBTest(String assortmentCode, String metric, float billDate) {
		super();
		this.assortmentCode = assortmentCode;
		this.metric = metric;
		this.billDate = billDate;
	}

	public String getAssortmentCode() {
		return assortmentCode;
	}

	public void setAssortmentCode(String assortmentCode) {
		this.assortmentCode = assortmentCode;
	}

	public String getMetric() {
		return metric;
	}

	public void setMetric(String metric) {
		this.metric = metric;
	}

	public float getBillDate() {
		return billDate;
	}

	public void setBillDate(float billDate) {
		this.billDate = billDate;
	}

	@Override
	public String toString() {
		return "ExportOTBTest [assortmentCode=" + assortmentCode + ", metric=" + metric + ", billDate=" + billDate
				+ "]";
	}

}
