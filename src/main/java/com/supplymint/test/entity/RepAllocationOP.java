package com.supplymint.test.entity;

public class RepAllocationOP {
	private String label51;
	private String label1;
	private String label6;
	private String label7;
	private String label8;
	private String label10;
	private String label12;
	private String label5;
	private String label22;
	private String label21;
	private String label23;
	private String label19;
	private String label24;
	public String getLabel51() {
		return label51;
	}
	public void setLabel51(String label51) {
		this.label51 = label51;
	}
	public String getLabel1() {
		return label1;
	}
	public void setLabel1(String label1) {
		this.label1 = label1;
	}
	public String getLabel6() {
		return label6;
	}
	public void setLabel6(String label6) {
		this.label6 = label6;
	}
	public String getLabel7() {
		return label7;
	}
	public void setLabel7(String label7) {
		this.label7 = label7;
	}
	public String getLabel8() {
		return label8;
	}
	public void setLabel8(String label8) {
		this.label8 = label8;
	}
	public String getLabel10() {
		return label10;
	}
	public void setLabel10(String label10) {
		this.label10 = label10;
	}
	public String getLabel12() {
		return label12;
	}
	public void setLabel12(String label12) {
		this.label12 = label12;
	}
	public String getLabel5() {
		return label5;
	}
	public void setLabel5(String label5) {
		this.label5 = label5;
	}
	public String getLabel22() {
		return label22;
	}
	public void setLabel22(String label22) {
		this.label22 = label22;
	}
	public String getLabel21() {
		return label21;
	}
	public void setLabel21(String label21) {
		this.label21 = label21;
	}
	public String getLabel23() {
		return label23;
	}
	public void setLabel23(String label23) {
		this.label23 = label23;
	}
	public String getLabel19() {
		return label19;
	}
	public void setLabel19(String label19) {
		this.label19 = label19;
	}
	public String getLabel24() {
		return label24;
	}
	public void setLabel24(String label24) {
		this.label24 = label24;
	}
	
	public RepAllocationOP() {
		// TODO Auto-generated constructor stub
	}
	public RepAllocationOP(String label51, String label1, String label6, String label7, String label8, String label10,
			String label12, String label5, String label22, String label21, String label23, String label19,
			String label24) {
		super();
		this.label51 = label51;
		this.label1 = label1;
		this.label6 = label6;
		this.label7 = label7;
		this.label8 = label8;
		this.label10 = label10;
		this.label12 = label12;
		this.label5 = label5;
		this.label22 = label22;
		this.label21 = label21;
		this.label23 = label23;
		this.label19 = label19;
		this.label24 = label24;
	}
	@Override
	public String toString() {
		return "Rep_Allocation_OP [label51=" + label51 + ", label1=" + label1 + ", label6=" + label6 + ", label7="
				+ label7 + ", label8=" + label8 + ", label10=" + label10 + ", label12=" + label12 + ", label5=" + label5
				+ ", label22=" + label22 + ", label21=" + label21 + ", label23=" + label23 + ", label19=" + label19
				+ ", label24=" + label24 + "]";
	}
	
	

}
