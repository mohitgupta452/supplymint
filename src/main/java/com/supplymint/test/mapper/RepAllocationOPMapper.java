package com.supplymint.test.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.supplymint.test.entity.RepAllocationOP;

@Mapper
public interface RepAllocationOPMapper {

	// @Select("SELECT * FROM REP_ALLOC_OUTPUT")
	List<RepAllocationOP> getAll();

}
