package com.supplymint.test.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.supplymint.test.entity.InventoryTest;

@Mapper
public interface AutoIncrementMapper {
	

	InventoryTest getById(Integer id);
	
	List<InventoryTest> getAll(@Param("offset")Integer offset,@Param("pagesize")Integer pagesize);
	
	Integer create(InventoryTest autoIncrement);
	
	Integer update(InventoryTest autoIncrement);

	@Delete("DELETE FROM TEST_INV WHERE INV_ID = #{id}")
	Integer delete(Integer id);
	
	@Select("SELECT COUNT(*) FROM TEST_INV")
	int record();
	
	Integer multipleListInsert(List<InventoryTest> autoIncrement);
    @Select("select * from stores_articles_rank where STATUS = 'true' ORDER BY	Type_S,RANK_S")
	List getPdfData();
	
	

}
