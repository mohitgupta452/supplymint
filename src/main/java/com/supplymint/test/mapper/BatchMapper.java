package com.supplymint.test.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.supplymint.test.entity.Batch;

@Mapper
public interface BatchMapper {

	int batchInsert(List<Batch> batch);
	
}
