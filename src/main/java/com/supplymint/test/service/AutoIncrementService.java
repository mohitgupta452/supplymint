package com.supplymint.test.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.supplymint.test.entity.InventoryTest;
import com.supplymint.test.entity.RepAllocationOP;
import com.supplymint.test.mapper.AutoIncrementMapper;
import com.supplymint.test.mapper.RepAllocationOPMapper;

@Service
public class AutoIncrementService {
	
	@Autowired
	private AutoIncrementMapper autoIncrementMapper;
	
	@Autowired
	RepAllocationOPMapper repAllocationOP;

	
	
	public InventoryTest getById(Integer id) {
		return autoIncrementMapper.getById(id);
	}
	public List<InventoryTest> getAll(Integer offset, Integer pageSize) {

		return autoIncrementMapper.getAll(offset, pageSize);
	}

	public Integer create(InventoryTest autoIncrement) {
		return autoIncrementMapper.create(autoIncrement);
	}

	public Integer update(InventoryTest autoIncrement) {
		return autoIncrementMapper.update(autoIncrement);
	}

	public Integer delete(int id) {
		return autoIncrementMapper.delete(id);
	}
	public Integer record() {
		return autoIncrementMapper.record();
	}
	
	public Integer multipleListInsert(List<InventoryTest> autoIncrement) {
		return autoIncrementMapper.multipleListInsert(autoIncrement);
	}
	
	public 	List<RepAllocationOP> getAll()
	{
		return repAllocationOP.getAll();
	}
	@SuppressWarnings("rawtypes")
	public List getPdfData() {
		// TODO Auto-generated method stub
		List data=autoIncrementMapper.getPdfData();
		return data;
	}

}
