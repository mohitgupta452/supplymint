package com.supplymint.test.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.supplymint.test.entity.Batch;
import com.supplymint.test.mapper.BatchMapper;

@Service
public class BatchService {

	@Autowired
	BatchMapper mapper;
	
	public int insert(List<Batch> batch) {
		
		return mapper.batchInsert(batch);
		
	}
}
