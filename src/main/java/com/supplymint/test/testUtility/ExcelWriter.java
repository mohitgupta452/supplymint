package com.supplymint.test.testUtility;

import static java.nio.file.StandardOpenOption.CREATE_NEW;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

public class ExcelWriter {

	// using auto flush and default window size 100
	public static <T> void writeToExcelAutoFlush(String fileName,List<T> excelModels) {
		SXSSFWorkbook wb = null;
		FileOutputStream fos = null;
		try {
			File file = new File(fileName);
			// keep 100 rows in memory, exceeding rows will be flushed to disk
			wb = new SXSSFWorkbook(SXSSFWorkbook.DEFAULT_WINDOW_SIZE/* 100 */);
			Sheet sh = wb.createSheet();

			Class<? extends Object> classz = excelModels.get(0).getClass();

			Field[] fields = classz.getDeclaredFields();
			int noOfFields = fields.length;

			int rownum = 0;
			Row row = sh.createRow(rownum);
			for (int i = 0; i < noOfFields; i++) {
				Cell cell = row.createCell(i);
				cell.setCellValue(fields[i].getName());
			}

			for (T excelModel : excelModels) {
				row = sh.createRow(rownum + 1);
				int colnum = 0;
				for (Field field : fields) {
					String fieldName = field.getName();
					Cell cell = row.createCell(colnum);
					Method method = null;
					try {
						method = classz.getMethod("get" + capitalize(fieldName));
					} catch (NoSuchMethodException nme) {
						method = classz.getMethod("get" + fieldName);
					}
					Object value = method.invoke(excelModel, (Object[]) null);
//					cell.setCellValue((String) value);
					if (value != null) {
						if (value instanceof String) {
							cell.setCellValue((String) value);
						} else if (value instanceof Long) {
							cell.setCellValue((Long) value);
						} else if (value instanceof Integer) {
							cell.setCellValue((Integer) value);
						} else if (value instanceof Double) {
							cell.setCellValue((Double) value);
						}
					}
					colnum++;
				}
				rownum++;
			}
			
			try {
				Path path = Paths.get(fileName);
				try (OutputStream out = Files.newOutputStream(path, CREATE_NEW)) {
					wb.write(out);
					out.close();
				} catch (FileAlreadyExistsException incrCounterAndRetry) {
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}			
		}catch(Exception ex) {
			
		}
	}

	// using manual flush and default window size 100
	public static <T> void writeToExcelManualFlush(String fileName, List<T> excelModels) {
		SXSSFWorkbook wb = null;
		FileOutputStream fos = null;
		try {
			File file = new File(fileName);
			// turn off auto-flushing and accumulate all rows in memory
			wb = new SXSSFWorkbook(-1);
			Sheet sh = wb.createSheet();

			Class<? extends Object> classz = excelModels.get(0).getClass();

			Field[] fields = classz.getDeclaredFields();
			int noOfFields = fields.length;

			int rownum = 0;
			Row row = sh.createRow(rownum);
			for (int i = 0; i < noOfFields; i++) {
				Cell cell = row.createCell(i);
				cell.setCellValue(fields[i].getName());
			}

			for (T excelModel : excelModels) {
				row = sh.createRow(rownum + 1);
				int colnum = 0;
				for (Field field : fields) {
					String fieldName = field.getName();
					Cell cell = row.createCell(colnum);
					Method method = null;
					try {
						method = classz.getMethod("get" + capitalize(fieldName));
					} catch (NoSuchMethodException nme) {
						method = classz.getMethod("get" + fieldName);
					}
					Object value = method.invoke(excelModel, (Object[]) null);
//					cell.setCellValue((String) value);
					if (value != null) {
						if (value instanceof String) {
							cell.setCellValue((String) value);
						} else if (value instanceof Long) {
							cell.setCellValue((Long) value);
						} else if (value instanceof Integer) {
							cell.setCellValue((Integer) value);
						} else if (value instanceof Double) {
							cell.setCellValue((Double) value);
						}
					}
					colnum++;
				}
				// manually control how rows are flushed to disk
				if (rownum % 100 == 0) {
					// retain 100 last rows and flush all others
					((SXSSFSheet) sh).flushRows(100);
				}
				rownum++;
			}
			try {
				Path path = Paths.get(fileName);
				try (OutputStream out = Files.newOutputStream(path, CREATE_NEW)) {
					wb.write(out);
					out.close();
				} catch (FileAlreadyExistsException incrCounterAndRetry) {
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}			
		}catch(Exception ex) {
			
		}
	}
	//capitalize the first letter of the field name for retriving value of the field later
		private static String capitalize(String s) {
			if (s.length() == 0)
				return s;
			return s.substring(0, 1).toUpperCase() + s.substring(1);
		}

}