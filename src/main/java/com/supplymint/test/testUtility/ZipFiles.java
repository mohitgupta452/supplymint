package com.supplymint.test.testUtility;

import static java.nio.file.StandardOpenOption.CREATE_NEW;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
 
/**
 * This Java program demonstrates how to compress multiple files in ZIP format.
 *
 * @author www.codejava.net
 */
public class ZipFiles {
 
    private static void zipFiles(String... filePaths) {
        try {
            File firstFile = new File(filePaths[0]);
            String zipFileName = firstFile.getName().concat(".zip");
            
            Path path = Paths.get(zipFileName);
            OutputStream out = Files.newOutputStream(path, CREATE_NEW);
            ZipOutputStream zos = new ZipOutputStream(out);
 
            for (String aFile : filePaths) {
                zos.putNextEntry(new ZipEntry(new File(aFile).getName()));
 
                byte[] bytes = Files.readAllBytes(Paths.get(aFile));
                zos.write(bytes, 0, bytes.length);
                zos.closeEntry();
            }
 
            zos.close();
 
        } catch (FileNotFoundException ex) {
        } catch (IOException ex) {
        }
    }
 
    public static void main(String[] args) {
        zipFiles(args);
    }
}