package com.supplymint.test.endpoint;

import static java.nio.file.StandardOpenOption.CREATE_NEW;

import java.io.File;
import java.io.OutputStream;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.validation.executable.ValidateOnExecution;

import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.tomcat.util.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.services.s3.model.S3Object;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.base.Objects;
import com.supplymint.config.application.AppCodeConfig;
import com.supplymint.config.aws.config.AmazonS3Client;
import com.supplymint.exception.StatusCodes.AppErrorCodes;
import com.supplymint.exception.StatusCodes.AppModuleErrors;
import com.supplymint.exception.StatusCodes.AppStatusCodes;
import com.supplymint.exception.StatusCodes.AppStatusMsg;
import com.supplymint.exception.StatusCodes.ClientErrorMessage;
import com.supplymint.exception.StatusCodes.ClientErrors;
import com.supplymint.exception.SupplyMintException;
import com.supplymint.layer.business.service.core.HeaderConfigService;
import com.supplymint.layer.business.service.tenant.TenantService;
import com.supplymint.layer.data.core.mapper.HeaderConfigMapper;
import com.supplymint.layer.web.endpoint.abstracts.AbstractEndpoint;
import com.supplymint.layer.web.endpoint.tenant.administration.ADMSiteMapEndPoint;
import com.supplymint.layer.web.model.AppResponse;
import com.supplymint.test.entity.Employee;
import com.supplymint.test.entity.InventoryTest;
import com.supplymint.test.service.AutoIncrementService;
import com.supplymint.util.CodeUtils;
import com.supplymint.util.ExcelUtils;
import com.supplymint.util.JsonValidator;
import com.supplymint.util.UploadUtils;

@RestController
@RequestMapping(path = AutoIncrementEndpoint.PATH, produces = { MediaType.APPLICATION_JSON_VALUE })
public class AutoIncrementEndpoint extends AbstractEndpoint {

	public static final String PATH = "test/inv";
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ADMSiteMapEndPoint.class);

	private static final String PATH_TO_EXCEL = "/home/tcemp0074/user.xlsx";
	private static final String UPLOADED_FOLDER = "E://upload//";

	@Autowired
	private AutoIncrementService autoIncrementService;

	@Autowired
	private AppCodeConfig appStatus;

	@Autowired
	TenantService tenantService;

	@Autowired
	private AmazonS3Client amazonS3Client;

	@Autowired
	private PlatformTransactionManager txManager;
	
	@Autowired
	private HeaderConfigMapper configMapper;
	
	@Autowired
	private HeaderConfigService configService;

	TransactionDefinition defaultTxn = new DefaultTransactionDefinition();

	@SuppressWarnings({ "finally", "unused", "rawtypes" })
	@RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getByIdentifierAndModule(@PathVariable(value = "id") String sid) {
		AppResponse appResponse = null;
		int id = 0;
		try {
			File fileName = null;
			S3Object s3Object = null;
			String key = "DEMAND_PLANNER/d3f78964-91ad-466c-9060-807e3cfc5fc2";
			key = amazonS3Client.getObjectSummary("bazaarkolkata-supplymint-develop-com", key).getKey();
			s3Object = amazonS3Client.getObjectRequest("bazaarkolkata-supplymint-develop-com", key);
			List<Map<String, String>> excelData = ExcelUtils.getExcelList(s3Object.getObjectContent());
			// amazonS3Client.getFileFromS3Bucket("bazaarkolkata-supplymint-develop-com","https://s3.ap-south-1.amazonaws.com/bazaarkolkata-supplymint-develop-com/FORECAST_REPORT/97c22f51-bb6d-4766-b077-e2a9e3a6b509/DEMAND_REPORT_WEEKLY.xlsx?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20190610T042634Z&X-Amz-SignedHeaders=host&X-Amz-Expires=172799&X-Amz-Credential=AKIAJ4VC37HSIUDIPMYA%2F20190610%2Fap-south-1%2Fs3%2Faws4_request&X-Amz-Signature=dc38e7550e6ce62b36938c1c3e9a3f2f907df4cb5d08f58aef671c4f9383286d");

			toJSON(excelData, "2019-05-01", "2019-09-01");

			List data1 = ExcelUtils.readExcel(fileName.getAbsolutePath());

			id = Integer.parseInt(sid);
			InventoryTest data = autoIncrementService.getById(id);
			if (!CodeUtils.isEmpty(data))
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, data)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			else
				appResponse = buildErrorResponse(AppErrorCodes.GENERIC_EVENT_NOT_AVAILABLE_ERROR_CODE,
						ClientErrors.NOT_FOUND, AppStatusMsg.BLANK);
			LOGGER.info(AppStatusMsg.BLANK);

		} catch (NumberFormatException ex) {
			ex.printStackTrace();

			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.NUMBER_FORMAT_MSG);

		} catch (DataAccessException ex) {
			ex.printStackTrace();

			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					ClientErrorMessage.BAD_REQUEST);
		} catch (Exception ex) {
			ex.printStackTrace();
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			throw new SupplyMintException(AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "unchecked", "deprecation", "rawtypes", "finally" })
	@RequestMapping(value = "/get/all", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getAllSites(@RequestParam("pageno") Integer pageNo) {

		AppResponse appResponse = null;
		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;
		try {
			totalRecord = autoIncrementService.record();
			maxPage = (totalRecord + pageSize - 1) / pageSize;
			previousPage = pageNo - 1;
			offset = (pageNo - 1) * pageSize;
			List data = autoIncrementService.getAll(offset, pageSize);
			ObjectNode objectNode = getMapper().createObjectNode();
			ArrayNode array = getMapper().createArrayNode();
			data.forEach(e -> {
				ObjectNode node = getMapper().valueToTree(e);
				array.add(node);
			});
			objectNode.put(CodeUtils.RESPONSE, array);
			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put("currPage", pageNo).put("prePage", previousPage)
								.put("maxPage", maxPage).put(CodeUtils.RESPONSE_MSG, "").putAll(objectNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				LOGGER.info(AppStatusMsg.BLANK);
			}
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					ClientErrorMessage.BAD_REQUEST);

		} catch (Exception ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			throw new SupplyMintException(AppStatusMsg.FAILURE_MSG);
		} finally {
			LOGGER.info(appResponse.toString());
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally" })
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> createInventory(@RequestBody InventoryTest autoIncrement) {

		AppResponse appResponse = null;
		int result = 0;
		try {

			result = autoIncrementService.create(autoIncrement);
			if (result != 0) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, autoIncrement)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
						ClientErrorMessage.BAD_REQUEST);
			}
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					ClientErrorMessage.BAD_REQUEST);
			LOGGER.info(appStatus.getMessage(AppModuleErrors.SITEMAP_CREATOR_ERROR));

		} catch (Exception ex) {
			ex.printStackTrace();
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings("finally")
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> updateSite(@RequestBody InventoryTest autoIncrement) {

		AppResponse appResponse = null;
		int result = 0;
		try {
			result = autoIncrementService.update(autoIncrement);

			if (result != 0) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, autoIncrement)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
						ClientErrorMessage.BAD_REQUEST);
			}
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					ClientErrorMessage.BAD_REQUEST);

		} catch (Exception ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings("finally")
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> deleteSite(@PathVariable(value = "id") String sid) {

		AppResponse appResponse = null;
		int id = 0;
		try {
			id = Integer.parseInt(sid);

			int result = autoIncrementService.delete(id);
			if (result != 0) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
						AppStatusMsg.FAILURE_MSG);
			}
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					ClientErrorMessage.BAD_REQUEST);

		} catch (NumberFormatException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.NUMBER_FORMAT_MSG);

		} catch (Exception ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings("finally")
	@RequestMapping(value = "/list/create", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> multipleListInsert(@RequestBody List<InventoryTest> autoIncrement) {

		AppResponse appResponse = null;
		int result = 0;
		try {
			result = autoIncrementService.multipleListInsert(autoIncrement);
			if (result != 0) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, autoIncrement)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
						ClientErrorMessage.BAD_REQUEST);
			}
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					ClientErrorMessage.BAD_REQUEST);
			LOGGER.info(appStatus.getMessage(AppModuleErrors.SITEMAP_CREATOR_ERROR));

		} catch (Exception ex) {
			ex.printStackTrace();
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings("finally")
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	@ResponseBody
	public String uploadMultipleFiles(@RequestParam("file") MultipartFile[] files) {

		String status = "";
		File dir = new File(UPLOADED_FOLDER);
		try {
			for (int i = 0; i < files.length; i++) {
				MultipartFile file = files[i];
				try {
					byte[] bytes = file.getBytes();

					if (!dir.exists())
						dir.mkdirs();

					String filePath=dir.getAbsolutePath() + File.separator + file.getOriginalFilename();
					try {
						Path path = Paths.get(filePath);
						try (OutputStream outputStream = Files.newOutputStream(path, CREATE_NEW)) {
							outputStream.write(bytes);
							outputStream.close();
						} catch (FileAlreadyExistsException incrCounterAndRetry) {
						}
					} catch (Exception ex) {
						ex.printStackTrace();
					}
//					BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(uploadFile));
//					outputStream.write(bytes);
//					outputStream.close();

					status = status + "You successfully uploaded file=" + file.getOriginalFilename();
				} catch (Exception e) {
					status = status + "Failed to upload " + file.getOriginalFilename() + " " + e.getMessage();
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			return status;
		}
	}

	@SuppressWarnings({ "unchecked", "deprecation", "rawtypes", "finally" })
	@RequestMapping(value = "/get/all/data", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getAll() {

		AppResponse appResponse = null;
		try {

			List data = autoIncrementService.getAll();

			ObjectNode objectNode = getMapper().createObjectNode();
			ArrayNode array = getMapper().createArrayNode();
			data.forEach(e -> {
				ObjectNode node = getMapper().valueToTree(e);
				array.add(node);
			});
			objectNode.put(CodeUtils.RESPONSE, array);
			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(objectNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				LOGGER.info(AppStatusMsg.BLANK);
			}
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					ClientErrorMessage.BAD_REQUEST);

		} catch (Exception ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			throw new SupplyMintException(AppStatusMsg.FAILURE_MSG);
		} finally {
			LOGGER.info(appResponse.toString());
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "unchecked", "deprecation", "rawtypes", "finally" })
	@RequestMapping(value = "/get/all/pdf", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getPdf() {

		AppResponse appResponse = null;
		try {

			List data = autoIncrementService.getPdfData();

			ObjectNode objectNode = getMapper().createObjectNode();
			ArrayNode array = getMapper().createArrayNode();
			data.forEach(e -> {
				ObjectNode node = getMapper().valueToTree(e);
				array.add(node);
			});
			objectNode.put(CodeUtils.RESPONSE, array);
			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(objectNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				LOGGER.info(AppStatusMsg.BLANK);
			}
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					ClientErrorMessage.BAD_REQUEST);

		} catch (Exception ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			throw new SupplyMintException(AppStatusMsg.FAILURE_MSG);
		} finally {
			LOGGER.info(appResponse.toString());
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes", "resource" })
	public static ArrayNode toJSON(Object object, String startDate, String endDate) {
		ObjectMapper mapper = new ObjectMapper();

		ArrayNode arrayNode = mapper.createArrayNode();
		Workbook workbook = new XSSFWorkbook();
		Sheet sheet = workbook.createSheet("OTB_Report");
		List headers = CodeUtils.getAllNormalMonths(startDate, endDate);

		int rowIndex = 1;
		Row row1 = sheet.createRow(0);
		row1.createCell(0).setCellValue("Assortment Code");
		for (int i = 0; i < headers.size(); i++) {
			row1.createCell(i + 1).setCellValue(headers.get(i).toString());
		}
		row1 = sheet.createRow(1);

		if (object instanceof List) {
			@SuppressWarnings("rawtypes")
			List list = (List) object;
			for (Object insideObject : list) {
				if (insideObject instanceof HashMap) {
					Map<String, String> map = (Map<String, String>) insideObject;
					Row row = sheet.createRow(rowIndex++);
					int cellIndex = 1;
					for (Entry<String, String> entry : map.entrySet()) {
						try {
							ObjectNode objectNode = mapper.createObjectNode();
							if (entry.getKey().equals(startDate) || entry.getKey().equals("ASSORTMENT_CODE")
									|| entry.getKey().equals(endDate) || entry.getKey().equals("2019-07-01")
									|| entry.getKey().equals("2019-08-01") || entry.getKey().equals("2019-06-01")) {

								objectNode.put(entry.getKey(), entry.getValue());
								if (entry.getKey().equals("ASSORTMENT_CODE"))
									row.createCell(0).setCellValue(entry.getValue());
								else if (!entry.getKey().equals("ASSORTMENT_CODE")) {
									int getCellIndex = cellIndex(entry.getKey(), headers);
									row.createCell(getCellIndex).setCellValue(entry.getValue());
								}
								arrayNode.add(objectNode);
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}

				}
			}
			try {
				Path path = Paths.get("/home/tcemp0074/SUPPLYMINT/abc.xlsx");
				try (OutputStream out = Files.newOutputStream(path, CREATE_NEW)) {
					workbook.write(out);
					out.close();
				} catch (FileAlreadyExistsException incrCounterAndRetry) {
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			
//			try {
//
//				FileOutputStream fos = new FileOutputStream("/home/tcemp0074/SUPPLYMINT/abc.xlsx");
//
//				workbook.write(fos);
//
//				fos.close();
//
//			} catch (FileNotFoundException e) {
//
//				e.printStackTrace();
//
//			} catch (IOException e) {
//
//				e.printStackTrace();
//
//			}

		}
		return arrayNode;
	}
	
	@SuppressWarnings("finally")
	@RequestMapping(value = "/trial", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> trialMethod(){
		AppResponse appResponse=null;
		try {
			ObjectMapper mapper=new ObjectMapper();
			String jsonString=configMapper.getFixedHeaders("TURNINGCLOUD", "TABLE HEADER", "ALLOCATION SUMMARY");
			JsonNode fixedNode = !CodeUtils.isEmpty(jsonString)?mapper.readTree(jsonString):mapper.createObjectNode();
			Map<String,String> toMap=mapper.readValue(jsonString, Map.class);
			System.out.println(toMap);
			Set<String> mapSet=toMap.keySet();
			boolean b=toMap.containsValue("STORE_CODE");
			Set<Entry<String, String>> mapEntry=toMap.entrySet();
			for(Entry<String,String> entry:mapEntry) {
				if(Objects.equal("STORE_CODE", entry.getValue())) {
					String key=entry.getKey();
					System.out.println(key);
				}
			}
			System.out.println(mapSet);
			ObjectNode objNode=configService.getHeaders("TURNINGCLOUD", "TABLE HEADER", "ALLOCATION SUMMARY");
			JsonNode jsonNode=objNode.get("Fixed Headers");
			if (!CodeUtils.isEmpty(objNode)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(objNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				LOGGER.info(AppStatusMsg.BLANK);
			}
		}catch(Exception ex) {
			
		}finally {
			return ResponseEntity.ok(appResponse);
		}
		
	}
	
	
	
	
	@Autowired
	JsonValidator json;

	@SuppressWarnings({ "finally" })
	@RequestMapping(value = "/check/emply", method = RequestMethod.POST)
	@ResponseBody
	@ValidateOnExecution
	public ResponseEntity<AppResponse> checkNullValues( @RequestBody Employee employee) {

		AppResponse appResponse = null;
		int result = 1;

		try {
			LOGGER.debug(String.format("Current JobID : %s  and JobRunState : %s",
					"8888888888888" , "jdfbhjdbfjdbh"));

		 json.serialize(employee);

			if (result != 0) {
				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
						.putPOJO(CodeUtils.RESPONSE, employee).put(CodeUtils.RESPONSE_MSG, ""))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				LOGGER.info(AppStatusMsg.BLANK);
			}
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					ClientErrorMessage.BAD_REQUEST);

		}
		catch(SupplyMintException ex) {
			
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE,
					ClientErrorMessage.BAD_REQUEST, ex.getMessage());
			
		}
		
		catch (Exception ex) {
			ex.printStackTrace();
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			throw new SupplyMintException(AppStatusMsg.FAILURE_MSG);
		} finally {
			LOGGER.info(appResponse.toString());
			return ResponseEntity.ok(appResponse);
		}
	}

	private static int cellIndex(String cellValue, List<String> headers) {
		int index = 0;
		for (int i = 0; i < headers.size(); i++) {
			if (headers.get(i).equals(cellValue)) {
				index = i + 1;
				break;
			}
		}
		return index;
	}
	
}