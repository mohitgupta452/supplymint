package com.supplymint.test.endpoint;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTimeConstants;
import org.joda.time.LocalDate;
import org.joda.time.Period;

public class TestDate {
	public static void main(String... ab) {
		
		
		String jobName="SMGLUE-TURNINGCLOUD-DEV-JOB";
		jobName=jobName.subSequence(0, jobName.lastIndexOf("-")+1)+"IPOAA"+"-JOB";
		String pattern=jobName.substring(jobName.indexOf("IP"), jobName.lastIndexOf("-"));
		
		
		
		
		String startDate = "2019-03-01";
		String endDate = "2019-03-31";
		LocalDate date1 = new LocalDate(startDate);
		LocalDate date2 = new LocalDate(endDate);
		LocalDate thisMonday = date1.withDayOfWeek(DateTimeConstants.MONDAY);
		if (date1.isAfter(thisMonday)) {
			date1 = thisMonday.plusWeeks(1); // start on previous monday
		} else {
			date1 = thisMonday; // start on this monday
		}
		List dateList = new ArrayList();
		while (date1.isBefore(date2)) {
			dateList.add(date1.toString("dd MMM yyyy"));
			date1 = date1.plus(Period.weeks(1));
		}
	}
}
