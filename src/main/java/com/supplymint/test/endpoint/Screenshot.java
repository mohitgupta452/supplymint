package com.supplymint.test.endpoint;

//Java Program to Capture full 
//Image of Screen 
import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Locale;

import javax.imageio.ImageIO; 

public class Screenshot { 
	public static final long serialVersionUID = 1L; 
	public static void main(String[] args) 
	{ 
		String path=null;
		try { 
			Thread.sleep(5000); 
			Robot r = new Robot(); 

			// It saves screenshot to desired path 
			String osName = System.getProperty("os.name");
			 osName = osName.toLowerCase(Locale.ENGLISH);
	            if (osName.contains("windows")) {
	            	path = "C:\\img/Shot.jpg"; 
	            }else if (osName.contains("linux")) {
	            	path = "/home/tcemp0074/img/Shot.jpg"; 
	            }else if (osName.contains("mac os")) {
	            	path = "/home/tcemp0074/img/Shot.jpg"; 
	            }

			// Used to get ScreenSize and capture image 
			Rectangle capture = 
			new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()); 
			BufferedImage image = r.createScreenCapture(capture); 
			ImageIO.write(image, "jpg", new File(path)); 
		} 
		catch (AWTException | IOException | InterruptedException ex) { 
		} 
	} 
} 
