/**
 * 
 */
package com.supplymint.security;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Manoj Singh
 * @since 18 OCT 2018
 * @version 1.0
 */

public class TokenResponse {

	@JsonProperty
	private String token;

	public TokenResponse() {
	}

	public TokenResponse(String token) {
		this.token = token;
	}
}
