package com.supplymint.layer.web.filters;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.ServletException;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.supplymint.config.datasource.ConfigProperties;
import com.supplymint.layer.business.service.tenant.TenantHelper;
import com.supplymint.layer.business.service.tenant.TenantService;
import com.supplymint.layer.data.config.TenantAwareRoutingSource;
import com.supplymint.layer.data.config.ThreadLocalStorage;
import com.supplymint.layer.data.core.entity.Tenant;

/**
 * 
 * 
 * @author Ashish Singh Dev
 * @version 1.0
 * @since 19-Sep-2018
 *
 */
@Service
public class TenantFilter {

	private final static Logger LOGGER = LoggerFactory.getLogger(TenantFilter.class);

	@Autowired
	private ConfigProperties configProperties;

	@Autowired
	private TenantService tenantService;

	@Autowired
	private TenantHelper tenantHelper;

	@Autowired
	private DataSource dataSource;

	@PostConstruct
	public void initTenantDS() throws SQLException {
		if (dataSource instanceof TenantAwareRoutingSource) {
			Map<Object, Object> tenantsDS = new HashMap<>();
			tenantsDS.putAll(tenantService.getTenantsDS());
			((TenantAwareRoutingSource) dataSource).setTargetDataSources(tenantsDS);
			((TenantAwareRoutingSource) dataSource).setDefaultTargetDataSource(tenantHelper.getCoreDs());
			((TenantAwareRoutingSource) dataSource).afterPropertiesSet();
		}

	}

	public void filter(String url, String tenantHashKey) throws IOException, ServletException, SQLException {
		if (url.contains("/core/") || url.contains("/proxy/")) {
			Tenant core = new Tenant();
			core.setId(configProperties.getCore_id());
			core.setUuid(configProperties.getCore_uid());
			String coreHashKey = tenantService.getTenantHashKey(core);
			ThreadLocalStorage.setTenantHashKey(coreHashKey);

		} else {
			ThreadLocalStorage.setTenantHashKey(tenantHashKey);
		}
	}

	public DataSource getDataSourcePool() {
		return dataSource;

	}

	public Map<String, String> getTenantConfigProperty() {
		return tenantService.getTenantProperty();
	}

}
