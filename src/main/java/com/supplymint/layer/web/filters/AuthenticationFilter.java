package com.supplymint.layer.web.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Component;

import com.amazonaws.services.glue.model.InternalServiceException;
import com.google.common.base.Optional;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.supplymint.config.aws.config.AWSGlueConfig;
import com.supplymint.exception.StatusCodes;
import com.supplymint.exception.StatusCodes.AppStatusMsg;
import com.supplymint.exception.SupplyMintException;
import com.supplymint.layer.data.config.ThreadLocalStorage;
import com.supplymint.util.CodeUtils;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureException;

/**
 * @author Manoj Singh
 * @since 20 OCT 2018
 * @version 1.0
 */

@Component
public class AuthenticationFilter implements Filter {

	private final static Logger LOGGER = LoggerFactory.getLogger(AuthenticationFilter.class);

	public static final String TOKEN_SESSION_KEY = "token";
	public static final String USER_SESSION_KEY = "user";
	public static final String USER_NAME = "aud";
	public static final String USER_PRINCIPAL = "prn";
	public static final String USER_EMAIL = "eml";
	public static final String USER_ENTERPRISE = "eid";
	public static final String USER_XTENANTID = "X-TENANT-ID";
//	private static String _REDIS_KEY_SECURE_KEY = "_SECKEY";

	private AuthenticationManager authenticationManager;

//	private String secretKey = null;

	private static final String EXEMPTION_PATH = "/generate/transfer/order";

	@Autowired
	private TenantFilter tenantFilter;

	boolean isValidToken = false;

	@Autowired
	private AWSGlueConfig awsGlueConfig;

//	@Autowired
//	private RedisTemplate<String, Object> redisTemplate;

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest httpRequest = asHttp(request);
		HttpServletResponse httpResponse = asHttp(response);
		String url = ((HttpServletRequest) request).getRequestURL().toString();
		try {
			if (url.contains("/ping") || url.contains("/presigned")) {
				chain.doFilter(request, response);
			} else if (url.contains(EXEMPTION_PATH)) {
				LOGGER.debug("Exempting the path by getting the tenant hashKey from url");
				tenantFilter.filter(url, getHashKey(url));
				chain.doFilter(httpRequest, httpResponse);
			} else if (url.contains("/swagger") || url.contains("/webjars") || url.contains("api-docs")) {
				LOGGER.debug("access swagger ui url without auth token");
				chain.doFilter(httpRequest, httpResponse);
			} else {
				Optional<String> token = Optional.fromNullable(httpRequest.getHeader("X-Auth-Token"));
				String authToken = httpRequest.getHeader("X-Auth-Token");

// 				Redis configuration for caching necessary information , uncomment code on production .
//				redisTemplate.getConnectionFactory().getConnection().getNativeConnection();
//				LOGGER.info(String.format("Fetching secure Key from redis server : %s", _REDIS_KEY_SECURE_KEY));
//				secretKey = redisTemplate.opsForValue().get(_REDIS_KEY_SECURE_KEY).toString();
//				LOGGER.info(String.format("Printing the secret key fetched from redis server : %s", secretKey));
//				isValidToken = isValidToken(authToken, secretKey);
//				LOGGER.info(String.format("Is valid token : %s", secretKey));

				if (token.isPresent()) {
					tokenValidationProcess(authToken);
					LOGGER.debug("Generating Tenant Hash Key for validation of Tenant Schema");
					tenantFilter.filter(url, getTenantHashKey(authToken));
					httpRequest.setAttribute("email", getTenantEmail(authToken));
					httpRequest.setAttribute("name", getTenantName(authToken));
					httpRequest.setAttribute("id", getTenantId(authToken));
					LOGGER.debug("AuthenticationFilter is passing request down the filter chain");
					chain.doFilter(request, response);
				}

			}
		} catch (Exception authenticationException) {
			SecurityContextHolder.clearContext();
			httpResponse.sendError(Integer.parseInt(StatusCodes.AppSecurityErrorCodes.TOKEN_EXPIRED),
					authenticationException.getMessage());
		} finally {
			MDC.remove(TOKEN_SESSION_KEY);
			ThreadLocalStorage.clearTenantHashKey();
			LOGGER.debug(String.format("Filter served the request : Expecting no Mapped tenantHashKey - %s",
					ThreadLocalStorage.getTenantHashKey()));
		}

	}

	private void processTokenAuthentication(Optional<String> token) {
		Authentication resultOfAuthentication = tryToAuthenticateWithToken(token);
		SecurityContextHolder.getContext().setAuthentication(resultOfAuthentication);
	}

	private String getHashKey(String url) {
		String[] pathArray = url.split("\\/");
		String length = pathArray[pathArray.length - 1];
		return length;
	}

	private boolean tokenValidationProcess(String token) throws SupplyMintException {
		JsonObject claimsJSON = decode(token);
		boolean isValidInfo = false;
		if (claimsJSON.has(USER_ENTERPRISE) && claimsJSON.has(USER_PRINCIPAL) && claimsJSON.has(USER_EMAIL)
				&& claimsJSON.has(USER_XTENANTID)) {
			if (!CodeUtils.isEmpty(claimsJSON.get(USER_PRINCIPAL).getAsString())
					&& !CodeUtils.isEmpty(claimsJSON.get(USER_XTENANTID).getAsString()))
				isValidInfo = true;
		} else
			throw new SupplyMintException(AppStatusMsg.TOKEN_EXPIRED);
		return isValidInfo;
	}

	private Authentication tryToAuthenticateWithToken(Optional<String> token) {
		PreAuthenticatedAuthenticationToken requestAuthentication = new PreAuthenticatedAuthenticationToken(token,
				null);
		return tryToAuthenticate(requestAuthentication);
	}

	private String getTenantHashKey(String token) {
		return decode(token).get(USER_XTENANTID).getAsString();
	}

	private String getTenantEmail(String token) {
		return decode(token).get(USER_EMAIL).getAsString();
	}

	private String getTenantName(String token) {
		return decode(token).get(USER_PRINCIPAL).getAsString();
	}

	private String getTenantId(String token) {
		return decode(token).get(USER_ENTERPRISE).getAsString();
	}

	private Authentication tryToAuthenticate(Authentication requestAuthentication) {

		Authentication responseAuthentication = authenticationManager.authenticate(requestAuthentication);
		if (responseAuthentication == null || !responseAuthentication.isAuthenticated()) {
			throw new InternalServiceException("Unable to authenticate Domain User for provided credentials");
		}
		LOGGER.debug("User successfully authenticated");
		return responseAuthentication;
	}

	public Boolean isValidToken(String token, String key) {
		try {
			if (token != null) {
				Jwts.parser().setSigningKey(key).parseClaimsJws(token);
				return Boolean.TRUE;
			} else
				return Boolean.FALSE;
		} catch (SignatureException e) {
			return Boolean.FALSE;
		}
	}

	public JsonObject decode(String token) {
		LOGGER.debug("Token", token);
		Base64 base64Url = new Base64(true);
		String decodeData = new String(base64Url.decode(token.split("\\.")[1]));
		return new Gson().fromJson(decodeData, JsonObject.class);
	}

	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		String username = authentication.getPrincipal() + "";
		String email = authentication.getName() + "";
		return new UsernamePasswordAuthenticationToken(username, email);
	}

	@Override
	public void destroy() {
		LOGGER.debug("Destroying the chain filter");
		ThreadLocalStorage.setTenantHashKey(null);

	}

	private HttpServletRequest asHttp(ServletRequest request) {
		return (HttpServletRequest) request;
	}

	private HttpServletResponse asHttp(ServletResponse response) {
		return (HttpServletResponse) response;
	}
}
