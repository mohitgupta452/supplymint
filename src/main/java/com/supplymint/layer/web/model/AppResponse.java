package com.supplymint.layer.web.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 
 * @author Ashish Singh Dev
 * @version 1.0
 * @since Sept 14, 2018
 *
 */
public class AppResponse {

	private static final ObjectMapper MAPPER = new ObjectMapper();

	private String status;
	private JsonNode data;

	private JsonNode error;

	private AppResponse() {
	}
	
	public static class Builder {
		private String status;
		private JsonNode data;
		private JsonNode error;

		public Builder status(String status) {
			this.status = status;
			return this;
		}

		public Builder data(JsonNode data) {
			this.data = data;
			return this;
		}

		public Builder data(Object data) {
			this.data = MAPPER.valueToTree(data);
			return this;
		}

		public Builder error(JsonNode error) {
			this.error = error;
			return this;
		}

		public AppResponse build() {
			return new AppResponse(this);
		}
	}

	private AppResponse(Builder builder) {
		this.status = builder.status;
		this.data = builder.data;
		this.error = builder.error;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public JsonNode getData() {
		return data;
	}

	public void setData(JsonNode data) {
		this.data = data;
	}

	public JsonNode getError() {
		return error;
	}

	public void setError(JsonNode error) {
		this.error = error;
	}

		
	@Override
	public String toString() {
		try {
			return MAPPER.writeValueAsString(this);
		} catch (JsonProcessingException e) {

			e.printStackTrace();
			
		}
		return null;
	}
	
	
}
