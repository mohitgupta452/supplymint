package com.supplymint.layer.web.endpoint.core;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.CannotGetJdbcConnectionException;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.supplymint.config.application.AppCodeConfig;
import com.supplymint.exception.StatusCodes.AppModuleErrors;
import com.supplymint.exception.StatusCodes.AppStatusCodes;
import com.supplymint.exception.StatusCodes.AppStatusMsg;
import com.supplymint.exception.StatusCodes.ClientErrors;
import com.supplymint.layer.business.service.core.HeaderConfigService;
import com.supplymint.layer.data.core.entity.HeaderConfig;
import com.supplymint.layer.web.endpoint.abstracts.AbstractEndpoint;
import com.supplymint.layer.web.model.AppResponse;
import com.supplymint.util.CodeUtils;

@RestController
@RequestMapping(path = HeaderConfigEndpoint.PATH, produces = { MediaType.APPLICATION_JSON_VALUE })
public class HeaderConfigEndpoint extends AbstractEndpoint {
	public static final String PATH = "/headerconfig";
	private static final Logger LOGGER = LoggerFactory.getLogger(HeaderConfigEndpoint.class);

	private HeaderConfigService configService;

	@Autowired
	public HeaderConfigEndpoint(HeaderConfigService configService) {
		this.configService = configService;

	}

	@Autowired
	private HttpServletRequest request;

	@Autowired
	private AppCodeConfig appStatus;

	@SuppressWarnings({ "finally" })
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> insertData(@RequestBody HeaderConfig headerConfiguration) {
		LOGGER.info("HeaderConfiguration insertData() method starts here....");
		AppResponse appResponse = null;
		int result = 0;

		try {
			LOGGER.info("Query for creating headers started....");

			result = configService.createdNewMethod(headerConfiguration);
			LOGGER.info("Query for creating headers ended....");
			if (result != 0) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, headerConfiguration)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.HEADER_SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
						AppModuleErrors.HEADER_CONFIG_CREATE_ERROR,
						appStatus.getMessage(AppModuleErrors.HEADER_CONFIG_CREATE_ERROR));
			}
		} catch (CannotGetJdbcConnectionException ex) {
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.HEADER_CONFIG_CREATE_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.HEADER_CONFIG_CREATE_ERROR,
					appStatus.getMessage(AppModuleErrors.HEADER_CONFIG_CREATE_ERROR));
		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally", "deprecation" })
	@RequestMapping(value = "/get/header", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getFixedHeaders(@RequestParam("enterpriseName") String enterpriseName,
			@RequestParam("attributeType") String attributeType, @RequestParam("displayName") String displayName) {
		AppResponse appResponse = null;
		try {
//			String token=request.getHeader("X-Auth-Token");
//			String enterpriseType = CodeUtils.decode(token).get("eid").getAsString();
//			System.out.println(enterpriseType);
			LOGGER.info("Query for fetching headers, started....");
			ObjectNode data = configService.getHeaders(enterpriseName.toUpperCase(), attributeType.toUpperCase(),
					displayName.toUpperCase());
			LOGGER.info("Query for fetching headers, ended....");
			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(data))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}
		} catch (CannotGetJdbcConnectionException ex) {
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.HEADER_CONFIG_CREATE_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.HEADER_CONFIG_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.HEADER_CONFIG_READER_ERROR));
		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

}
