package com.supplymint.layer.web.endpoint.tenant.users;

import java.util.Date;

import javax.servlet.ServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.CannotGetJdbcConnectionException;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.supplymint.config.application.AppCodeConfig;
import com.supplymint.config.aws.utils.AWSUtils.SessionDetail;
import com.supplymint.exception.StatusCodes.AppModuleErrors;
import com.supplymint.exception.StatusCodes.AppStatusCodes;
import com.supplymint.exception.StatusCodes.AppStatusMsg;
import com.supplymint.exception.StatusCodes.ClientErrors;
import com.supplymint.exception.SupplyMintException;
import com.supplymint.layer.business.contract.users.SessionDetailsService;
import com.supplymint.layer.data.tenant.users.entity.SessionDetails;
import com.supplymint.layer.web.endpoint.abstracts.AbstractEndpoint;
import com.supplymint.layer.web.model.AppResponse;
import com.supplymint.util.CodeUtils;

@RestController
@RequestMapping(path = SessionDetailsEndPoint.PATH, produces = { MediaType.APPLICATION_JSON_VALUE })

public class SessionDetailsEndPoint extends AbstractEndpoint {

	public static final String PATH = "user/session";

	private static final Logger LOGGER = LoggerFactory.getLogger(SessionDetailsEndPoint.class);

	private SessionDetailsService sessionDetailsService;

	@Autowired
	public SessionDetailsEndPoint(SessionDetailsService sessionDetailsService) {

		this.sessionDetailsService = sessionDetailsService;

	}

	@Autowired
	private AppCodeConfig appStatus;

	@SuppressWarnings("finally")
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> insertData(@RequestBody SessionDetails sessionDetails, ServletRequest request)
			throws SupplyMintException {

		AppResponse appResponse = null;
		int result = 0;
		
		try {
			result = sessionDetailsService.create(sessionDetails);
			LOGGER.debug(String.format("Requested result is : %s",result));

			if (result != 0) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, sessionDetails)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
						AppModuleErrors.SESSION_DETAIL_CREATOR_ERROR,
						appStatus.getMessage(AppModuleErrors.SESSION_DETAIL_CREATOR_ERROR));

			}

		} catch (CannotGetJdbcConnectionException ex) {
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.SESSION_DETAIL_CREATOR_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			ex.printStackTrace();
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.SESSION_DETAIL_CREATOR_ERROR,
					appStatus.getMessage(AppModuleErrors.SESSION_DETAIL_CREATOR_ERROR));
		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}

	}

	@SuppressWarnings({ "finally", "deprecation" })
	@RequestMapping(method = RequestMethod.GET, value = "/get/detail")
	@ResponseBody
	public ResponseEntity<AppResponse> getLastData(@RequestParam("userName") String userName) {

		LOGGER.info("SessionDetail getLastData() method starts here....");

		AppResponse appResponse = null;
		ObjectNode node = getMapper().createObjectNode();
		ArrayNode arrNode = getMapper().createArrayNode();
		SessionDetails data = null;
		long diffSeconds = 0;
		long diffMinutes = 0;
		long diffHours = 0;
		long diffDays = 0;
		long diffMonths = 0;

		String duration = null;

		try {
			data = sessionDetailsService.getByUserName(userName);

			if (!CodeUtils.isEmpty(data)) {
				Date currentTime = new Date();
				currentTime = CodeUtils.convertDateOnTimeZonePluseForAWS(currentTime, 5, 30);
				Date lastLogInTime = Date.from(data.getLoggedOut().toInstant());
				long diffTime = currentTime.getTime() - lastLogInTime.getTime();

				diffSeconds = diffTime / 1000 % 60;
				diffMinutes = diffTime / (60 * 1000) % 60;
				diffHours = diffTime / (60 * 60 * 1000) % 24;
				diffDays = diffTime / (24 * 60 * 60 * 1000);
				diffMonths = diffTime / ((24 * 60 * 60 * 1000) * 30);
				if (diffMonths >= 1) {
					duration = diffDays + " Months ago";
				} else if (diffDays >= 1) {
					duration = diffDays + " Days ago";
				} else if (diffHours >= 1) {
					duration = diffHours + " Hours ago";
				} else if (diffMinutes >= 1) {
					duration = diffMinutes + " Minutes ago";
				} else if (diffSeconds >= 1) {
					duration = diffSeconds + " Seconds ago";
				}

				ObjectNode tempNode = getMapper().createObjectNode();

				tempNode.put("deffernce", duration);

				arrNode.add(tempNode);

				node.put(CodeUtils.RESPONSE, arrNode);
				LOGGER.info("Final Object node data is {} ", node);

				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(node))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				LOGGER.info(AppStatusMsg.SUCCESS_MSG);

			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

				LOGGER.info(AppStatusMsg.BLANK);
			}
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.SESSION_DETAIL_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.SESSION_DETAIL_READER_ERROR));

			LOGGER.info(ex.getMessage());
		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.info(e.getMessage());
		} finally {
			LOGGER.info("SessionDetail getLastData() method ends here...");
			return ResponseEntity.ok(appResponse);
		}
	}

}
