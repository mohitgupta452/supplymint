package com.supplymint.layer.web.endpoint.tenant.administration;

import java.io.File;
import java.net.InetAddress;
import java.time.OffsetDateTime;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import javax.servlet.ServletRequest;

import org.apache.poi.ss.usermodel.Row;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.CannotGetJdbcConnectionException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.supplymint.config.application.AppCodeConfig;
import com.supplymint.exception.StatusCodes.AppErrorCodes;
import com.supplymint.exception.StatusCodes.AppModuleErrors;
import com.supplymint.exception.StatusCodes.AppStatusCodes;
import com.supplymint.exception.StatusCodes.AppStatusMsg;
import com.supplymint.exception.StatusCodes.ClientErrors;
import com.supplymint.layer.business.contract.administration.ADMSiteMapService;
import com.supplymint.layer.data.tenant.administration.entity.ADMSiteMap;
import com.supplymint.layer.web.endpoint.abstracts.AbstractEndpoint;
import com.supplymint.layer.web.model.AppResponse;
import com.supplymint.util.CodeUtils;
import com.supplymint.util.ExcelUtils;

/**
 * 
 * @author Prabhakar S,Bishnu D & Mohit G
 * this is part of Organisation Module which is used for managing site Mapping
 *
 */
@RestController
@RequestMapping(path = ADMSiteMapEndPoint.PATH, produces = { MediaType.APPLICATION_JSON_VALUE })
public class ADMSiteMapEndPoint extends AbstractEndpoint {

	public static final String PATH = "admin/sitemap";

	private static final Logger LOGGER = LoggerFactory.getLogger(ADMSiteMapEndPoint.class);

	private ADMSiteMapService siteMapService;

	@Autowired
	private AppCodeConfig appStatus;

	@Autowired
	public ADMSiteMapEndPoint(ADMSiteMapService siteMapService) {
		this.siteMapService = siteMapService;
	}

	/**
	 * 
	 * @param sid
	 * @return  API for getting all details according to ID
	 */
	@SuppressWarnings("finally")
	@RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getByIdentifierAndModule(@PathVariable(value = "id") String sid) {
		AppResponse appResponse = null;
		int id = 0;

		try {
			id = Integer.parseInt(sid);
			LOGGER.debug("Query,for finding the all details by specific id,is going to start...");
			ADMSiteMap data = siteMapService.getById(id);
			LOGGER.debug("Query,for finding the all details by specific id,has ended...");
			if (!CodeUtils.isEmpty(data)) {
				LOGGER.info("get site map by id");
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, data)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = buildErrorResponse(AppErrorCodes.GENERIC_EVENT_NOT_AVAILABLE_ERROR_CODE,
						ClientErrors.NOT_FOUND, AppStatusMsg.BLANK);
				LOGGER.info("Unable to get sitemap by id");
			}
		} catch (NumberFormatException ex) {
			LOGGER.debug(String.format("Error Occoured From NumberFormatException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.NUMBER_FORMAT_MSG);

		} catch (CannotGetJdbcConnectionException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.SITEMAP_READER_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.SITEMAP_READER_ERROR, appStatus.getMessage(AppModuleErrors.SITEMAP_READER_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	/**
	 * 
	 * @param pageNo
	 * @param type
	 * @param search
	 * @param fromSiteName
	 * @param toSiteName
	 * @param startDate
	 * @param tptLeadTime
	 * @param status
	 * @return API used for getting ,filtering and searching details
	 */
	@SuppressWarnings({ "finally" })
	@RequestMapping(value = "/get/all", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getAllSites(@RequestParam("pageNo") Integer pageNo,
			@RequestParam("type") Integer type, @RequestParam("search") String search,
			@RequestParam("fromSiteName") String fromSiteName, @RequestParam("toSiteName") String toSiteName,
			@RequestParam("startDate") String startDate, @RequestParam("tptLeadTime") String tptLeadTime,
			@RequestParam("status") String status) {

		AppResponse appResponse = null;
		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;
		ObjectNode objectNode;
		List<ADMSiteMap> data = null;
		int flag = 0;

		try {

			flag = type;

			if (flag == 1) {
				LOGGER.debug("Query,for getting data,is going to start...");
				totalRecord = siteMapService.record();
				LOGGER.debug("Query,for getting data,has ended...");
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo - 1) * pageSize;
				LOGGER.debug("Query,for getting all record,is going to start...");
				data = siteMapService.getAll(offset, pageSize);
				LOGGER.debug("Query,for getting all record,has ended...");
				if (!CodeUtils.isEmpty(data)) {

					objectNode = buildJson(data);
					appResponse = successResponse(pageNo, previousPage, maxPage, objectNode);
					LOGGER.info("get all site");

				} else {
					appResponse = blankResponse();
					LOGGER.info(AppStatusMsg.BLANK);
				}
			} else if (flag == 2) {

				LOGGER.debug("Query,for filtering all record,is going to start...");
				totalRecord = siteMapService.filterRecord(fromSiteName, toSiteName, startDate, tptLeadTime, status);
				LOGGER.debug("Query,for filtering all record,has ended...");
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo - 1) * pageSize;
				LOGGER.debug("Query,for getting filtered data,is going to start...");
				data = siteMapService.filter(offset, pageSize, fromSiteName, toSiteName, startDate, tptLeadTime,
						status);
				LOGGER.debug("Query,for getting filtered data,has ended...");
				if (!CodeUtils.isEmpty(data)) {
					objectNode = buildJson(data);
					appResponse = successResponse(pageNo, previousPage, maxPage, objectNode);

				} else {
					appResponse = blankResponse();
					LOGGER.info(AppStatusMsg.BLANK);
				}
			} else {
				if (flag == 3 && !search.isEmpty()) {
					LOGGER.debug("Query,for getting data,is going to start..");
					totalRecord = siteMapService.searchRecord(search);
					LOGGER.debug("Query,for getting data,has ended..");
					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo - 1) * pageSize;

					LOGGER.debug("Query,for getting all searched record,is going to start...");
					data = siteMapService.searchAll(offset, pageSize, search);
					LOGGER.debug("Query,for getting all searched record,has ended...");
					if (!CodeUtils.isEmpty(data)) {
						objectNode = buildJson(data);
						appResponse = successResponse(pageNo, previousPage, maxPage, objectNode);

					} else {
						appResponse = blankResponse();
					}
				} else {
					appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
							AppStatusMsg.FAILURE_MSG);
				}
			}
		} catch (CannotGetJdbcConnectionException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.SITEMAP_READER_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
		} catch (DataAccessException ex) {
			ex.printStackTrace();
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.SITEMAP_READER_ERROR, appStatus.getMessage(AppModuleErrors.SITEMAP_READER_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	/**
	 * 
	 * @param aDMSiteMap
	 * @return response after creating new data
	 */
	@SuppressWarnings("finally")
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> saveSite(@RequestBody ADMSiteMap aDMSiteMap) {

		AppResponse appResponse = null;
		int result = 0;
		try {

			aDMSiteMap.setCreatedBy("");
			OffsetDateTime creationTime = OffsetDateTime.now();
			creationTime = creationTime.plusHours(5);
			creationTime = creationTime.plusMinutes(30);
			aDMSiteMap.setCreatedTime(creationTime);
			String ipAddress = InetAddress.getLocalHost().getHostAddress().trim().toString();
			aDMSiteMap.setIpAddress(ipAddress);
			LOGGER.debug("Query,for creating,is going to start...");
			result = siteMapService.create(aDMSiteMap);
			LOGGER.debug("Query,for creating,has ended...");
			if (result != 0) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, aDMSiteMap)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
						AppModuleErrors.SITEMAP_CREATOR_ERROR,
						appStatus.getMessage(AppModuleErrors.SITEMAP_CREATOR_ERROR));
			}
		} catch (CannotGetJdbcConnectionException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.SITEMAP_CREATOR_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.SITEMAP_CREATOR_ERROR, appStatus.getMessage(AppModuleErrors.SITEMAP_CREATOR_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	/**
	 * 
	 * @param aDMSiteMap
	 * @return updating mapping site data
	 */
	@SuppressWarnings("finally")
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> updateSite(@RequestBody ADMSiteMap aDMSiteMap) {

		AppResponse appResponse = null;
		int result = 0;
		try {

			OffsetDateTime updationTime = OffsetDateTime.now();
			updationTime = updationTime.plusHours(5);
			updationTime = updationTime.plusMinutes(30);
			aDMSiteMap.setUpdationTime(updationTime);
			aDMSiteMap.setUpdatedBy("");
			String ipAddress = InetAddress.getLocalHost().getHostAddress().trim().toString();
			aDMSiteMap.setIpAddress(ipAddress);
			LOGGER.debug("Query,for updating data, is going to start...");
			result = siteMapService.update(aDMSiteMap);
			LOGGER.debug("Query,for updating data, is going to start...");

			if (result != 0) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, aDMSiteMap)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
						AppModuleErrors.SITEMAP_UPDATE_ERROR,
						appStatus.getMessage(AppModuleErrors.SITEMAP_UPDATE_ERROR));
			}
		} catch (CannotGetJdbcConnectionException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.SITEMAP_UPDATE_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.SITEMAP_UPDATE_ERROR, appStatus.getMessage(AppModuleErrors.SITEMAP_UPDATE_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	/**
	 * 
	 * @param sid
	 * @return deleting details according to @param
	 */
	@SuppressWarnings("finally")
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> deleteSite(@PathVariable(value = "id") String sid) {

		AppResponse appResponse = null;
		int id = 0;
		try {
			id = Integer.parseInt(sid);
			LOGGER.debug("Query,for deleting data, is going to start...");
			int result = siteMapService.delete(id);
			LOGGER.debug("Query,for deleting data, has ended...");
			if (result != 0) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
						AppModuleErrors.SITEMAP_DELETE_ERROR,
						appStatus.getMessage(AppModuleErrors.SITEMAP_DELETE_ERROR));
				LOGGER.info(appStatus.getMessage(AppModuleErrors.SITEMAP_DELETE_ERROR));
			}
		} catch (CannotGetJdbcConnectionException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.SITEMAP_DELETE_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.SITEMAP_DELETE_ERROR, appStatus.getMessage(AppModuleErrors.SITEMAP_DELETE_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (NumberFormatException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.NUMBER_FORMAT_MSG);
			LOGGER.debug(String.format("Error Occoured From NumberFormatException : %s", ex));
		} catch (Exception ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	/**
	 * 
	 * @param status
	 * @return List of data as per status @param
	 */
	@SuppressWarnings({ "unchecked", "deprecation", "rawtypes", "finally" })
	@RequestMapping(value = "/get/onstatus/{status}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getStatus(@PathVariable("status") String status) {

		AppResponse appResponse = null;
		try {
			LOGGER.debug("Query,for getting data,is going to start...");
			List data = siteMapService.getStatus(status);
			LOGGER.debug("Query,for getting data,has ended..");
			ObjectNode objectNode = getMapper().createObjectNode();
			ArrayNode array = getMapper().createArrayNode();
			data.forEach(e -> {
				ObjectNode node = getMapper().valueToTree(e);
				array.add(node);
			});
			objectNode.put(CodeUtils.RESPONSE, array);
			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(objectNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = buildErrorResponse(AppErrorCodes.GENERIC_EVENT_NOT_AVAILABLE_ERROR_CODE,
						ClientErrors.NOT_FOUND, AppStatusMsg.BLANK);
				LOGGER.info(AppStatusMsg.BLANK);
			}
		} catch (CannotGetJdbcConnectionException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.SITEMAP_READER_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));

		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.SITEMAP_READER_ERROR, appStatus.getMessage(AppModuleErrors.SITEMAP_READER_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
		} finally {
			return ResponseEntity.ok(appResponse);
		}

	}

	/**
	 * 
	 * @param status
	 * @param sid
	 * @return updating status of data as per @param
	 */
	@SuppressWarnings("finally")
	@RequestMapping(value = "/update/status/{status}", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> updateStatus(@PathVariable("status") String status,
			@RequestParam("sid") Integer sid) {

		AppResponse appResponse = null;
		int result = 0;
		try {
			OffsetDateTime updationTime = OffsetDateTime.now();

			String ipAddress = InetAddress.getLocalHost().getHostAddress().trim().toString();
			result = siteMapService.updateStatus(status, sid, ipAddress, updationTime);
			LOGGER.debug("Query,for finding the all details by specific id,is going to start...");
			ADMSiteMap sitemap = siteMapService.getById(sid);
			LOGGER.debug("Query,for finding the all details by specific id,has ended...");
			if (result != 0) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, sitemap)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
						AppModuleErrors.SITEMAP_UPDATE_ERROR,
						appStatus.getMessage(AppModuleErrors.SITEMAP_UPDATE_ERROR));
				LOGGER.info(appStatus.getMessage(AppModuleErrors.SITEMAP_UPDATE_ERROR));
			}
		} catch (CannotGetJdbcConnectionException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.SITEMAP_UPDATE_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.SITEMAP_UPDATE_ERROR, appStatus.getMessage(AppModuleErrors.SITEMAP_UPDATE_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	/**
	 * 
	 * @param fromid
	 * @param toid
	 * @return getting data between two ID's
	 */
	@SuppressWarnings({ "unchecked", "deprecation", "rawtypes", "finally" })
	@RequestMapping(value = "/get/detailson/{fromid}/mappedto/{toid}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getDetailsOnFromIdtoToId(@PathVariable("fromid") Integer fromid,
			@PathVariable("toid") Integer toid) {

		AppResponse appResponse = null;
		try {
			LOGGER.debug("Query,for getting data,is going to start...");
			List data = siteMapService.getDetailsOnFromIdtoToId(fromid, toid);
			LOGGER.debug("Query,for getting data,has ended...");
			ObjectNode objectNode = getMapper().createObjectNode();
			ArrayNode array = getMapper().createArrayNode();
			data.forEach(e -> {
				ObjectNode node = getMapper().valueToTree(e);
				array.add(node);
			});
			objectNode.put(CodeUtils.RESPONSE, array);
			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(objectNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = buildErrorResponse(AppErrorCodes.GENERIC_EVENT_NOT_AVAILABLE_ERROR_CODE,
						ClientErrors.NOT_FOUND, AppStatusMsg.BLANK);
				LOGGER.info(AppStatusMsg.BLANK);
			}
		} catch (CannotGetJdbcConnectionException ex) {
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.SITEMAP_READER_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.SITEMAP_READER_ERROR, appStatus.getMessage(AppModuleErrors.SITEMAP_READER_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
		} finally {
			return ResponseEntity.ok(appResponse);
		}

	}

	/**
	 * 
	 * @param fromsiteid
	 * @return  get details as per @param
	 */
	@SuppressWarnings({ "unchecked", "deprecation", "rawtypes", "finally" })
	@RequestMapping(value = "/get/detailson/{fromsiteid}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getDetailsOnFromSiteId(@PathVariable("fromsiteid") Integer fromsiteid) {

		AppResponse appResponse = null;
		try {
			LOGGER.debug("Query,for getting data,is going to start...");
			List data = siteMapService.getDetailsOnFromSiteId(fromsiteid);
			LOGGER.debug("Query,for getting data,has ended...");
			ObjectNode objectNode = getMapper().createObjectNode();
			ArrayNode array = getMapper().createArrayNode();
			data.forEach(e -> {
				ObjectNode node = getMapper().valueToTree(e);
				array.add(node);
			});
			objectNode.put(CodeUtils.RESPONSE, array);
			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(objectNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = buildErrorResponse(AppErrorCodes.GENERIC_EVENT_NOT_AVAILABLE_ERROR_CODE,
						ClientErrors.NOT_FOUND, AppStatusMsg.BLANK);
				LOGGER.info(AppStatusMsg.BLANK);
			}
		} catch (CannotGetJdbcConnectionException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.SITEMAP_READER_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.SITEMAP_READER_ERROR, appStatus.getMessage(AppModuleErrors.SITEMAP_READER_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	/**
	 * this method is used to show response 
	 * if there is no data-->
	 * @return 'not found' response
	 */
	AppResponse notFoundResponse() {
		AppResponse appResponse = buildErrorResponse(AppErrorCodes.GENERIC_EVENT_NOT_AVAILABLE_ERROR_CODE,
				ClientErrors.NOT_FOUND, AppStatusMsg.BLANK);
		return appResponse;
	}

	@SuppressWarnings("deprecation")
	ObjectNode buildJson(List<ADMSiteMap> data) {
		ObjectNode objectNode = getMapper().createObjectNode();
		ArrayNode array = getMapper().createArrayNode();
		data.forEach(e -> {
			ObjectNode node = getMapper().valueToTree(e);
			array.add(node);
		});
		objectNode.put(CodeUtils.RESPONSE, array);

		return objectNode;
	}


	@SuppressWarnings({ "finally", "unused", "rawtypes", "deprecation" })
	@RequestMapping(value = "/upload/data", method = RequestMethod.POST, consumes = {
			MediaType.MULTIPART_FORM_DATA_VALUE })
	@ResponseBody
	public ResponseEntity<AppResponse> uploadSiteData(@RequestParam("file") MultipartFile file,
			ServletRequest request) {

		AppResponse appResponse = null;
		Vector<Row> readDataFromFromExcel = null;
		int result = 0;
		String fromSite, fromSiteId, toSite, toSiteId, startDate, endDate, tptLeadTime, transportationMode, status;

		try {
			ADMSiteMap admSiteMap = new ADMSiteMap();

			File tmpFile = new File(System.getProperty("java.io.tmpdir") + System.getProperty("file.separator")
					+ file.getOriginalFilename());
			file.transferTo(tmpFile);
			String fileName = tmpFile.getPath();
			LOGGER.info(fileName);
			Vector data = ExcelUtils.readXLSFileWithBlankCells(fileName);
			if (!CodeUtils.isEmpty(data)) {
				for (int i = 1; i < data.size(); i++) {
					Vector cellStoreVector = (Vector) data.elementAt(i);
					for (int j = 0; j < cellStoreVector.size(); j++) {
						Object myCell = cellStoreVector.elementAt(j);
						if (j == 0) {
							fromSite = myCell.toString();
							admSiteMap.setFromSite(fromSite);
						} else if (j == 1) {
							fromSiteId = myCell.toString();
							if (!CodeUtils.isEmpty(fromSiteId)) {
								admSiteMap.setFromSiteId((int) Double.parseDouble(fromSiteId));
							}
						} else if (j == 2) {
							toSite = myCell.toString();
							admSiteMap.setToSite(toSite);
						} else if (j == 3) {
							toSiteId = myCell.toString();
							if (!CodeUtils.isEmpty(toSiteId)) {
								admSiteMap.setToSiteId((int) Double.parseDouble(toSiteId));
							}
						} else if (j == 4) {
							startDate = myCell.toString();
							if (!CodeUtils.isEmpty(startDate)) {
								admSiteMap.setStartDate(new Date(startDate));
							} else {
								admSiteMap.setStartDate(null);
							}
						} else if (j == 5) {
							endDate = myCell.toString();
							if (!CodeUtils.isEmpty(endDate)) {
								admSiteMap.setEndDate(new Date(endDate));
							} else {
								admSiteMap.setEndDate(null);
							}
						} else if (j == 6) {
							tptLeadTime = myCell.toString();
							if (!CodeUtils.isEmpty(tptLeadTime)) {
								admSiteMap.setTptLeadTime((int) Double.parseDouble(tptLeadTime));
							}
						} else if (j == 7) {
							transportationMode = myCell.toString();
							admSiteMap.setTransportationMode(transportationMode);
						} else if (j == 8) {
							status = myCell.toString();
							admSiteMap.setStatus(status);
						}
					}
					OffsetDateTime creationTime = OffsetDateTime.now();
					creationTime = creationTime.plusHours(5);
					creationTime = creationTime.plusMinutes(30);
					admSiteMap.setCreatedTime(creationTime);
					String ipAddress = InetAddress.getLocalHost().getHostAddress().trim().toString();
					admSiteMap.setIpAddress(ipAddress);
					if (admSiteMap.getStatus().equalsIgnoreCase("Active")) {
						admSiteMap.setActive('1');
					} else {
						admSiteMap.setActive('0');
					}

					result = siteMapService.create(admSiteMap);
				}
				if (result != 0) {
					appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
							.put(CodeUtils.RESPONSE, "").put(CodeUtils.RESPONSE_MSG, AppStatusMsg.FILE_UPLOADED))
							.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

				} else {
					appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
							AppStatusMsg.FAILURE_MSG);
				}
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				LOGGER.info(AppStatusMsg.BLANK);
			}
		}

		catch (CannotGetJdbcConnectionException ex) {
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.SITEMAP_CREATOR_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.SITEMAP_CREATOR_ERROR, appStatus.getMessage(AppModuleErrors.SITEMAP_CREATOR_ERROR));

		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

}
