package com.supplymint.layer.web.endpoint.tenant.inventory;

import java.net.InetAddress;
import java.time.OffsetDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.supplymint.config.application.AppCodeConfig;
import com.supplymint.exception.StatusCodes.AppModuleErrors;
import com.supplymint.exception.StatusCodes.AppStatusCodes;
import com.supplymint.exception.StatusCodes.AppStatusMsg;
import com.supplymint.exception.StatusCodes.ClientErrors;
import com.supplymint.exception.SupplyMintException;
import com.supplymint.layer.business.contract.inventory.AllocationService;
import com.supplymint.layer.data.tenant.inventory.entity.Allocation;
import com.supplymint.layer.web.endpoint.abstracts.AbstractEndpoint;
import com.supplymint.layer.web.model.AppResponse;
import com.supplymint.util.CodeUtils;

@RestController
@RequestMapping(path = AllocationEndpoint.PATH, produces = { MediaType.APPLICATION_JSON_VALUE })

public class AllocationEndpoint extends AbstractEndpoint {

	public static final String PATH = "admin/allocation";
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AllocationEndpoint.class);

	private AllocationService allocationService;
	
	@Autowired
	private AppCodeConfig appStatus;

	@Autowired
	public AllocationEndpoint(AllocationService allocationService) {
		this.allocationService = allocationService;
	}
	
	
	@SuppressWarnings("finally")
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> create(@RequestBody Allocation allocation) throws SupplyMintException {

		int result = 0;
		AppResponse appResponse = null;

		try {
			OffsetDateTime currentTime = OffsetDateTime.now();
			allocation.setCreatedTime(currentTime);
			allocation.setCreatedBy("");
			String ipAddress = InetAddress.getLocalHost().getHostAddress().trim().toString();
			allocation.setIpAddress(ipAddress);

			result = allocationService.insert(allocation);

			if (result != 0) {
				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
						.putPOJO(CodeUtils.RESPONSE, allocation).put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
						AppModuleErrors.ALLOCATION_CREATOR_ERROR, appStatus.getMessage(AppModuleErrors.ALLOCATION_CREATOR_ERROR));
			}

		}catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.ALLOCATION_CREATOR_ERROR,
					appStatus.getMessage(AppModuleErrors.ALLOCATION_CREATOR_ERROR));

			LOGGER.info(appStatus.getMessage(AppModuleErrors.ALLOCATION_CREATOR_ERROR));

		}  catch (Exception exception) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);

			exception.printStackTrace();

		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings("finally")
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> deleteMCSTR(@PathVariable(value = "id") String aId) throws SupplyMintException {

		AppResponse appResponse = null;
		int id = 0;
		try {

			id = Integer.parseInt(aId);
			int result = allocationService.deleteById(id);

			if (result != 0) {

				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
						AppModuleErrors.ALLOCATION_DELETE_ERROR, appStatus.getMessage(AppModuleErrors.ALLOCATION_DELETE_ERROR));
				LOGGER.info(appStatus.getMessage(AppModuleErrors.ALLOCATION_DELETE_ERROR));
			}
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.ALLOCATION_DELETE_ERROR,
					appStatus.getMessage(AppModuleErrors.ALLOCATION_DELETE_ERROR));

			LOGGER.info(appStatus.getMessage(AppModuleErrors.ALLOCATION_DELETE_ERROR));

		}catch (NumberFormatException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.NUMBER_FORMAT_MSG);

		} catch (Exception ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);

			throw new SupplyMintException(AppStatusMsg.FAILURE_MSG);

		} finally {
	/*		connection = (Connection)session.getAttribute("xtenantid");
			tenantService.getReleaseTenantsDS(connection);
	*/		return ResponseEntity.ok(appResponse);
		}

	}

}
