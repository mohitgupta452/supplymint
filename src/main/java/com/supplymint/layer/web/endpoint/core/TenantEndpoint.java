package com.supplymint.layer.web.endpoint.core;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;
import com.supplymint.exception.StatusCodes.AppStatusCodes;
import com.supplymint.layer.business.service.tenant.TenantService;
import com.supplymint.layer.web.endpoint.abstracts.AbstractEndpoint;
import com.supplymint.layer.web.model.AppResponse;

/**
 * @author Manoj Singh
 * @since 20 OCT 2018
 * @version 1.0
 */

@RestController
@RequestMapping(path = TenantEndpoint.PATH, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {
		MediaType.APPLICATION_JSON_VALUE })
public class TenantEndpoint extends AbstractEndpoint {

	public static final String PATH = "core/tenant";

	private TenantService tenantService;

	@Autowired
	public TenantEndpoint(TenantService tenantService) {
		this.tenantService = tenantService;
	}

	@RequestMapping(value = "id/{id}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getByIdentifierAndModule(@PathVariable(value = "id") Integer id) {

		AppResponse appResponse = new AppResponse.Builder()
				.data(getMapper().createArrayNode().add(getMapper().valueToTree(tenantService.getTenantById(id))))
				.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

		return ResponseEntity.ok(appResponse);
	}

	@RequestMapping(value = "/uuid/{uuid}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getByIdentifierAndModule(@PathVariable(value = "uuid") String uuid) {

		AppResponse appResponse = new AppResponse.Builder()
				.data(getMapper().createArrayNode().add(getMapper().valueToTree(tenantService.getTenantByUuid(uuid))))
				.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

		return ResponseEntity.ok(appResponse);
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> saveTenant(@RequestBody JsonNode tenant) {

		Assert.notNull(tenant, "Request body is null.");
		Integer id = tenantService.save(tenant.get("name").asText(), tenant.get("config").toString());

		AppResponse appResponse = new AppResponse.Builder()
				.data(getMapper().createArrayNode().add(getMapper().createObjectNode().put("id", id.toString())))
				.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

		return ResponseEntity.ok(appResponse);
	}
}
