package com.supplymint.layer.web.endpoint.tenant.demand;

import java.net.InetAddress;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.CannotGetJdbcConnectionException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.supplymint.config.application.AppCodeConfig;
import com.supplymint.config.aws.utils.AWSUtils;
import com.supplymint.config.aws.utils.AWSUtils.Status;
import com.supplymint.exception.SupplyMintException;
import com.supplymint.exception.StatusCodes.AppModuleErrors;
import com.supplymint.exception.StatusCodes.AppStatusCodes;
import com.supplymint.exception.StatusCodes.AppStatusMsg;
import com.supplymint.exception.StatusCodes.ClientErrors;
import com.supplymint.layer.business.contract.demand.DemandPlanningService;
import com.supplymint.layer.business.contract.setting.GeneralSettingService;
import com.supplymint.layer.data.config.ThreadLocalStorage;
import com.supplymint.layer.data.tenant.demand.entity.BudgetedDemandPlanning;
import com.supplymint.layer.data.tenant.demand.entity.BudgetedTemporary;
import com.supplymint.layer.data.tenant.demand.entity.DpUploadSummary;
import com.supplymint.layer.web.endpoint.abstracts.AbstractEndpoint;
import com.supplymint.layer.web.model.AppResponse;
import com.supplymint.util.CodeUtils;
import com.supplymint.util.UploadUtils;
import com.supplymint.util.ApplicationUtils.BucketParameter;
import com.supplymint.util.ApplicationUtils.DemandPlanning.ChooseType;
import com.supplymint.util.CodeUtils.Demand;

@RestController
@RequestMapping(path = BudgetedSalesEndpoint.PATH, produces = { MediaType.APPLICATION_JSON_VALUE })

public class BudgetedSalesEndpoint extends AbstractEndpoint {

	private final static Logger LOGGER = LoggerFactory.getLogger(BudgetedSalesEndpoint.class);

	public static final String PATH = "demand/budgeted";

	private DemandPlanningService demandPlanningService;

	@Autowired
	private HttpServletRequest servletRequest;
	@Autowired
	private GeneralSettingService generalSettingService;

	@Autowired
	public BudgetedSalesEndpoint(DemandPlanningService demandPlanningService) {
		this.demandPlanningService = demandPlanningService;
	}

	@Autowired
	private AppCodeConfig appStatus;

	private String tenantHashkey;
	private List<String> storeBudgetedSalesStatus;
	public String fileName = null;
	String newDateFormat = null;

	@SuppressWarnings({ "finally", "unused", "deprecation" })
	@RequestMapping(value = "/upload/data", method = RequestMethod.POST, consumes = {
			MediaType.MULTIPART_FORM_DATA_VALUE })
	@ResponseBody
	public ResponseEntity<AppResponse> graphBudgetedSale(@RequestParam("fileData") MultipartFile fileData,
			@RequestParam("type") String type, @RequestParam("userName") String userName, ServletRequest request) {

		AppResponse appResponse = null;

		String token = ((HttpServletRequest) request).getHeader("X-Auth-Token");
		JsonArray orgDetails=CodeUtils.decode(token).get("ORG_DETAILS").getAsJsonArray();
		
		JsonObject jNode=orgDetails.get(0).getAsJsonObject();
		String orgId = jNode.get("orgId").getAsString();
		
		
		tenantHashkey = ThreadLocalStorage.getTenantHashKey();
		try {
			String fileLocation = null;
			storeBudgetedSalesStatus = new ArrayList<String>();
			// Previous Code

//			File tmpFile = new File(System.getProperty("java.io.tmpdir") + System.getProperty("file.separator")
//					+ fileData.getOriginalFilename());

//			fileData.transferTo(tmpFile);

//			fileName = tmpFile.getPath();

			LOGGER.debug(String.format("Reading file from file location : %s", fileData.getOriginalFilename()));
			List<BudgetedTemporary> getExcelData = demandPlanningService.validDataInExcelFile(fileData.getInputStream(),
					userName, type);
			LOGGER.debug("Returned Excel data will validation type");
			int rowCount = getExcelData.size();
			if (rowCount != 0) {
				OffsetDateTime currentTime = OffsetDateTime.now();
				currentTime = currentTime.plusHours(5);
				currentTime = currentTime.plusMinutes(30);
				String name = fileData.getOriginalFilename();
				DpUploadSummary dpUploadSummary = new DpUploadSummary();
				String ipAddress = InetAddress.getLocalHost().getHostAddress().trim().toString();
				dpUploadSummary.setCreatedTime(currentTime);
				String id = UUID.randomUUID().toString();
				dpUploadSummary.setId(id);
				dpUploadSummary.setType(type);
				dpUploadSummary.setIpAddress(ipAddress);
				dpUploadSummary.setAttachUrl(fileLocation);
				dpUploadSummary.setFileName(name);
				dpUploadSummary.setDemandType(Demand.BUDGETED.toString());
				dpUploadSummary.setRowCount(rowCount);
				dpUploadSummary.setStatus(Status.UPLOADED.toString());
				dpUploadSummary.setCreatedBy(servletRequest.getAttribute("email").toString());
				dpUploadSummary.setLastSummary("true");
				dpUploadSummary.setUserName(userName);

				demandPlanningService.insertDpUploadSummary(dpUploadSummary);
				demandPlanningService.updateBudgetedSummary(userName, id);
				storeBudgetedSalesStatus.add(Status.UPLOADED.toString());
				ExecutorService executorService = Executors.newSingleThreadExecutor();
				executorService.execute(new Runnable() {
					@Override
					public void run() {
						ThreadLocalStorage.setTenantHashKey(tenantHashkey);
						List<BudgetedDemandPlanning> budgetedData = null;
						String status = null;
						try {

							if (type.equalsIgnoreCase(ChooseType.WEEKLY.toString())) {
								demandPlanningService.updateLastDemandBudgetedWeeklyData(userName);
							} else if (type.equalsIgnoreCase(ChooseType.MONTHLY.toString())) {
								demandPlanningService.updateLastDemandBudgeteMonthlyData(userName);
							}
							demandPlanningService.truncateTempBudgetedData(userName);

							LOGGER.info(String.format(
									"Inserting Budgeted data into demand planing budgeted sales table for %s",
									fileName));
							int size = 500;
							List<BudgetedTemporary> sublist = null;

							for (int start = 0; start < getExcelData.size(); start += size) {
								int end = Math.min(start + size, getExcelData.size());
								sublist = getExcelData.subList(start, end);
								demandPlanningService.insertFromExcelFileInList(sublist);
							}
							LOGGER.info(
									String.format("Successfully inserted into Temporary Budgeted data  %s", fileName));
							// update status after completing insert all data in Temporary Budgeted data
							status = demandPlanningService.getProgressStatus(userName);
							storeBudgetedSalesStatus.add(status);
							LOGGER.info(String.format("Your Inserting Budgeted data status is  %s", status));

							if (type.equalsIgnoreCase(ChooseType.WEEKLY.toString())) {
								demandPlanningService.deleteMatchedAssortmentCodeTempDataForWeeklyBudgeted(userName);
							} else if (type.equalsIgnoreCase(ChooseType.MONTHLY.toString())) {
								demandPlanningService.deleteMatchedAssortmentCodeTempDataForMonthlyBudgeted(userName);
							}

							if (type.equalsIgnoreCase(ChooseType.WEEKLY.toString())) {

								demandPlanningService.insertMatchBudgetedWeeklyData(userName);
								demandPlanningService.insertNotMatchBudgetedWeeklyData(userName);

							} else if (type.equalsIgnoreCase(ChooseType.MONTHLY.toString())) {

								demandPlanningService.insertMatchBudgetedMonthlyData(userName);
								demandPlanningService.insertNotMatchBudgetedMonthlyData(userName);

							}
							// update status after Verified and insert all Budgeted data
							status = demandPlanningService.getVerifiedAllData(userName);
							storeBudgetedSalesStatus.add(status);
							LOGGER.info(String.format("Your Inserting Budgeted data status is  %s", status));

							// update status after Verified and insert all Budgeted data With URL
							status = demandPlanningService.getFinalyData(type, userName,orgId);
							storeBudgetedSalesStatus.add(status);
							LOGGER.info(String.format("Your Inserting Budgeted data status is  %s", status));

						} catch (Exception e) {
							demandPlanningService.updateDPUploadSummary(AWSUtils.Status.FAILED.toString(), 0, 0, null,
									id);

						}
					}
				});
				ObjectNode objectNode = getMapper().createObjectNode();
				objectNode.put("id", id);
				// objectNode.put("fileName", fileName);
				objectNode.put("frequency", type);
				objectNode.put("userName", userName);
				ObjectNode node = getMapper().createObjectNode();
				node.put(CodeUtils.RESPONSE, objectNode);
				// if (readDataFromFromExcel.size() != 0) {
				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
						.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.EXCEL_UPLOAD).putAll(node))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				demandPlanningService.insertDPUploadSummaryData(userName, type, userName, rowCount);
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE,
						AppModuleErrors.DEMAND_BUDGETED_SALES_EXCEL_READER_ERROR, AppStatusMsg.BLANK);
				LOGGER.info(AppStatusMsg.BLANK);

			}
		}

		catch (SupplyMintException ex) {
			LOGGER.debug(String.format("Error Occoured From SupplyMintException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE,
					AppModuleErrors.DEMAND_BUDGETED_SALES_EXCEL_READER_ERROR, ex.getMessage());

		} catch (CannotGetJdbcConnectionException ex) {
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.DEMAND_BUDGETED_SALES_CREATOR_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.DEMAND_BUDGETED_SALES_CREATOR_ERROR,
					appStatus.getMessage(AppModuleErrors.DEMAND_BUDGETED_SALES_CREATOR_ERROR));

		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally" })
	@RequestMapping(value = "/download/data", method = RequestMethod.GET)
	public ResponseEntity<AppResponse> downloadDemandBudgeted(@RequestParam("frequency") String frequency,
			@RequestParam("fromDate") String fromDate, @RequestParam("toDate") String toDate,HttpServletRequest request) {

		AppResponse appResponse = null;
		String url = null;
		try {

			url = demandPlanningService.getBudgetedDownloadURL(frequency, fromDate, toDate,request);
			LOGGER.debug("obtaing download budgeted data url :- %s", url);
			if (!CodeUtils.isEmpty(url)) {

				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, url)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}

		} catch (CannotGetJdbcConnectionException ex) {
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.DEMAND_BUDGETED_SALES_READER_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.DEMAND_BUDGETED_SALES_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.DEMAND_BUDGETED_SALES_READER_ERROR));

		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally", "deprecation" })
	@RequestMapping(value = "/file/upload/status", method = RequestMethod.GET)
	public ResponseEntity<AppResponse> getBudgetedSalesFileUploadStaus(@RequestParam("userName") String userName) {

		AppResponse appResponse = null;
		DpUploadSummary dpUploadSummary = null;
		try {

			ObjectNode node = getMapper().createObjectNode();
			ObjectNode dataNode = getMapper().createObjectNode();
			LOGGER.info("get Last budgeted file uploaded status ");
			dpUploadSummary = demandPlanningService.getLastStatus(userName);
			if (!CodeUtils.isEmpty(dpUploadSummary)) {
				String lastStatus = dpUploadSummary.getStatus();
				if (lastStatus.equals("FAILED")) {
					node.put("status", lastStatus);
					node.put("lastStatus",
							!CodeUtils.isEmpty(storeBudgetedSalesStatus)
									? storeBudgetedSalesStatus.get(storeBudgetedSalesStatus.size() - 1)
									: null);
					if (dpUploadSummary.getRowCount() == 0) {
						node.put("message", AppStatusMsg.BLANK);
					} else {
						node.put("message", AppStatusMsg.BLANK);
					}
				} else {
					if (lastStatus.equalsIgnoreCase(AWSUtils.Status.SUCCEEDED.toString())) {
						demandPlanningService.truncateTempBudgetedData(userName);

						dataNode.put("total", dpUploadSummary.getRowCount());
						dataNode.put("valid", dpUploadSummary.getValidRow());
						dataNode.put("invalid", dpUploadSummary.getInvalidRow());
					}

					node.put("fileName", dpUploadSummary.getAttachUrl());
					node.put("frequency", dpUploadSummary.getType());
					node.put("status", dpUploadSummary.getStatus());
					node.put("data", dataNode);
				}
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, node)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}

		} catch (CannotGetJdbcConnectionException ex) {
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.DEMAND_BUDGETED_SALES_READER_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.DEMAND_BUDGETED_SALES_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.DEMAND_BUDGETED_SALES_READER_ERROR));

		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally", "deprecation" })
	@RequestMapping(value = "/summary", method = RequestMethod.GET)
	public ResponseEntity<AppResponse> getBudgetedSalesSummaryData(@RequestParam("pageNo") Integer pageNo,
			@RequestParam("type") Integer type, @RequestParam("uploadedDate") String uploadedDate,
			@RequestParam("fileName") String fileName, @RequestParam("totalData") String totalData,
			@RequestParam("valid") String valid, @RequestParam("invalid") String invalid,
			@RequestParam("search") String search) {

		AppResponse appResponse = null;
		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;
		List<DpUploadSummary> data = null;
		try {
			if (type == 1) {
				LOGGER.info("retrive budgeted sale summary get all");
				totalRecord = demandPlanningService.countBudgetedHistoryData();
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
				data = demandPlanningService.getBudgetedHistoryData(offset, pageSize);
				LOGGER.debug("obtaing budgeted sale summary get all :-%d", data.size());
			}

			if (type == 2) {
				LOGGER.info("retrive budgeted sale summary filter");
				totalRecord = demandPlanningService.countFilterBudgetedHistoryData(uploadedDate, fileName, totalData,
						valid, invalid);
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
				data = demandPlanningService.filterBudgetedHistoryData(offset, pageSize, uploadedDate, fileName,
						totalData, valid, invalid);
				LOGGER.debug("obtaing budgeted sale summary filter :-%d", data.size());
			}
			if (type == 3) {
				LOGGER.info("retrive budgeted sale summary search");
				totalRecord = demandPlanningService.countSearchBudgetedHistoryData(search);
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
				data = demandPlanningService.searchBudgetedHistoryData(offset, pageSize, search);
				LOGGER.debug("obtaing budgeted sale summary search :-%d", data.size());
			}

			ObjectNode objectNode = getMapper().createObjectNode();
			ArrayNode array = getMapper().createArrayNode();
			if (data.size() > 0 && !data.isEmpty()) {
				try {
					Map<String, String> generalSettingData = generalSettingService.getAll();

					String newDate = generalSettingData.get("dateFormat");
					String timeFormat = generalSettingData.get("timeFormat");
					String[] parts = new String[newDate.length()];
					if (newDate.contains("HH")) {
						if (timeFormat.equalsIgnoreCase("24 Hour")) {
							newDateFormat = newDate;
						}
						if (timeFormat.equalsIgnoreCase("12 Hour")) {
							parts = newDate.split("H");
							String h2r = "hh" + parts[2];
							newDateFormat = parts[0] + h2r;
						}
					} else {
						newDateFormat = newDate;
					}
				} catch (Exception ex) {
					newDateFormat = "DD MMM YYYY HH:mm";
				}

				data.stream().forEach(e -> {

					ObjectNode tempNode = getMapper().createObjectNode();

					tempNode.put("id", e.getId());
					tempNode.put("uploadedDate", e.getCreatedTime().format(DateTimeFormatter.ofPattern(newDateFormat)));
					tempNode.put("fileName", e.getFileName());
					tempNode.put("totalRecord", e.getRowCount());
					tempNode.put("valid", e.getValidRow());
					tempNode.put("invalid", e.getInvalidRow());
					tempNode.put("uploadedBy", e.getUserName());
					tempNode.put("url", e.getUrl());

					array.add(tempNode);

				});

				objectNode.put(CodeUtils.RESPONSE, array);
				LOGGER.debug(String.format("Final Object Node data is {} :  %s", objectNode));
				if (!CodeUtils.isEmpty(data)) {

					appResponse = new AppResponse.Builder()
							.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").put("currPage", pageNo)
									.put("prePage", previousPage).put("maxPage", maxPage).putAll(objectNode))
							.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

				} else {
					appResponse = new AppResponse.Builder()
							.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
									.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
							.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				}
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}
		} catch (CannotGetJdbcConnectionException ex) {
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.DEMAND_BUDGETED_SALES_READER_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.DEMAND_BUDGETED_SALES_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.DEMAND_BUDGETED_SALES_READER_ERROR));
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}

	}

	@SuppressWarnings({ "finally" })
	@RequestMapping(value = "/history/ur", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getUrlDemandBudgetedHistory(@RequestParam("uuid") String uuid,
			ServletRequest request) {

		AppResponse appResponse = null;
		String url = null;
		try {
			DpUploadSummary data = demandPlanningService.getBudgetedHistoryDataByID(uuid);

			url = UploadUtils.downloadHistory(data.getUrl(), BucketParameter.GENERATE_EXCELCSV, request);

			LOGGER.debug("obtaining the budgeted download url :-%s", url);
			if (!CodeUtils.isEmpty(url)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, url)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}
		} catch (CannotGetJdbcConnectionException ex) {
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.DEMAND_BUDGETED_SALES_READER_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.DEMAND_BUDGETED_SALES_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.DEMAND_BUDGETED_SALES_READER_ERROR));

		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		}

		finally {
			return ResponseEntity.ok(appResponse);
		}
	}

}
