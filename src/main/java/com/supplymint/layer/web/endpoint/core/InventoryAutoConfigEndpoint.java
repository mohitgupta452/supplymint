package com.supplymint.layer.web.endpoint.core;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.CannotGetJdbcConnectionException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.supplymint.exception.StatusCodes.AppModuleErrors;
import com.supplymint.exception.StatusCodes.AppStatusCodes;
import com.supplymint.exception.StatusCodes.AppStatusMsg;
import com.supplymint.exception.StatusCodes.ClientErrors;
import com.supplymint.layer.business.service.core.InventoryAutoConfigService;
import com.supplymint.layer.data.core.entity.InventoryAutoConfig;
import com.supplymint.layer.web.endpoint.abstracts.AbstractEndpoint;
import com.supplymint.layer.web.model.AppResponse;
import com.supplymint.util.CodeUtils;

@RestController
@RequestMapping(path = InventoryAutoConfigEndpoint.PATH, produces = { MediaType.APPLICATION_JSON_VALUE })
public class InventoryAutoConfigEndpoint extends AbstractEndpoint {

	public static final String PATH = "core/inv/auto";

	
	private InventoryAutoConfigService inventoryAutoConfigService;

	private static final Logger LOGGER = LoggerFactory.getLogger(InventoryAutoConfigEndpoint.class);

	@Autowired
	public InventoryAutoConfigEndpoint(InventoryAutoConfigService inventoryAutoConfigService) {
		this.inventoryAutoConfigService = inventoryAutoConfigService;
	}

	@SuppressWarnings("finally")
	@RequestMapping(value = "/get/all", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getAll() {

		AppResponse appResponse = null;
		try {
			List<InventoryAutoConfig> data = inventoryAutoConfigService.getAll();

			ObjectNode objectNode = getMapper().createObjectNode();
			objectNode.putPOJO(CodeUtils.RESPONSE, data);

			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(objectNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				LOGGER.info(AppStatusMsg.BLANK);
			}
		} catch (CannotGetJdbcConnectionException ex) {
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.DEMAND_PLANNING_READER_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}

	}

}
