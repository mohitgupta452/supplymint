package com.supplymint.layer.web.endpoint.setting;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.CannotGetJdbcConnectionException;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.supplymint.config.application.AppCodeConfig;
import com.supplymint.exception.StatusCodes.AppModuleErrors;
import com.supplymint.exception.StatusCodes.AppStatusCodes;
import com.supplymint.exception.StatusCodes.AppStatusMsg;
import com.supplymint.exception.StatusCodes.ClientErrors;
import com.supplymint.layer.business.contract.setting.GeneralSettingService;
import com.supplymint.layer.data.setting.entity.GeneralSetting;
import com.supplymint.layer.web.endpoint.abstracts.AbstractEndpoint;
import com.supplymint.layer.web.model.AppResponse;
import com.supplymint.util.CodeUtils;

@RestController
@RequestMapping(path = GeneralSettingEndpoint.PATH, produces = { MediaType.APPLICATION_JSON_VALUE })
public class GeneralSettingEndpoint extends AbstractEndpoint{
	
	public static final String PATH = "general/setting";
	
	private static final Logger LOGGER = LoggerFactory.getLogger(GeneralSettingEndpoint.class);

	private GeneralSettingService generalSettingService;

	@Autowired
	private AppCodeConfig appStatus;
	
	@Autowired
	public GeneralSettingEndpoint(GeneralSettingService generalSettingService) {
		this.generalSettingService = generalSettingService;
	}
	
	@SuppressWarnings({ "deprecation", "finally" })
	@RequestMapping(value = "/get/all", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getAll() {

		AppResponse appResponse = null;
		try {
			Map<String,String> data = generalSettingService.getAll();
			ObjectNode objectNode = getMapper().createObjectNode();
			objectNode.putPOJO(CodeUtils.RESPONSE, data);
			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(objectNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}
		} catch (DataAccessException ex) {
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.GENERAL_SETTING_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.GENERAL_SETTING_READER_ERROR));
		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}
	
	@SuppressWarnings("finally")
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseEntity<AppResponse> createGeneralSetting(@RequestBody GeneralSetting generalSetting,HttpServletRequest request) {

		AppResponse appResponse = null;
		int result = 0;
		try { 

			result = generalSettingService.create(generalSetting,request);
			if (result != 0) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, generalSetting)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SETTING_SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
						AppModuleErrors.GENERAL_SETTING_CREATOR_ERROR,
						appStatus.getMessage(AppModuleErrors.GENERAL_SETTING_CREATOR_ERROR));
			}
		} catch (CannotGetJdbcConnectionException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.GENERAL_SETTING_CREATOR_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.GENERAL_SETTING_CREATOR_ERROR,
					appStatus.getMessage(AppModuleErrors.GENERAL_SETTING_CREATOR_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}
	
//	@SuppressWarnings("finally")
//	@RequestMapping(value = "/update", method = RequestMethod.POST)
//	@ResponseBody
//	public ResponseEntity<AppResponse> updateSite(@RequestBody GeneralSetting generalSetting) {
//
//		AppResponse appResponse = null;
//		int result = 0;
//		try {
//
//			result = generalSettingService.update(generalSetting);
//
//			if (result != 0) {
//				appResponse = new AppResponse.Builder()
//						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, generalSetting)
//								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
//						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
//			} else {
//				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
//						AppModuleErrors.GENERAL_SETTING_UPDATE_ERROR,
//						appStatus.getMessage(AppModuleErrors.GENERAL_SETTING_UPDATE_ERROR));
//			}
//		} catch (CannotGetJdbcConnectionException ex) {
//			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
//					AppModuleErrors.GENERAL_SETTING_UPDATE_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
//			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
//		} catch (DataAccessException ex) {
//			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
//					AppModuleErrors.GENERAL_SETTING_UPDATE_ERROR,
//					appStatus.getMessage(AppModuleErrors.GENERAL_SETTING_UPDATE_ERROR));
//			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
//		} catch (Exception ex) {
//			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
//					AppStatusMsg.FAILURE_MSG);
//			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
//		} finally {
//			return ResponseEntity.ok(appResponse);
//		}
//	}

}
