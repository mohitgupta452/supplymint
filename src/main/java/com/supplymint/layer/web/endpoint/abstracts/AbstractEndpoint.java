package com.supplymint.layer.web.endpoint.abstracts;

import java.time.OffsetDateTime;
import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.supplymint.exception.StatusCodes.AppStatusCodes;
import com.supplymint.exception.StatusCodes.AppStatusMsg;
import com.supplymint.layer.data.tenant.administration.entity.ADMOrganisation;
import com.supplymint.layer.web.model.AppResponse;
import com.supplymint.util.CodeUtils;
import com.supplymint.util.LocalDateTimeSerializer;

/**
 * 
 * 
 * @author Ashish Singh Dev
 * @version 1.0
 * @since 19-Sep-2018
 *
 */

@RequestMapping(consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, produces = {
		MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
public class AbstractEndpoint {

	protected static ObjectMapper getMapper() {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, true);
		return mapper;
	}

	protected static ArrayNode getArrayNode() {
		return getMapper().createArrayNode();
	}

	protected static ObjectNode getObjectNode() {
		return getMapper().createObjectNode();
	}

	AppResponse buildOkResponse() {
		return new AppResponse.Builder().status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
	}

	public static AppResponse blankResponse() {

		AppResponse appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
				.putPOJO(CodeUtils.RESPONSE, null).put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
				.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

		return appResponse;
	}
	
	public static AppResponse successResponse(int pageNo, int previousPage, int maxPage, ObjectNode objectNode) {

		AppResponse appResponse = new AppResponse.Builder()
				.data(getMapper().createObjectNode().put("currPage", pageNo).put("prePage", previousPage)
						.put("maxPage", maxPage).put(CodeUtils.RESPONSE_MSG, "").putAll(objectNode))
				.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
		return appResponse;
	}
	
	protected AppResponse buildErrorResponse(String statusCode, String errorCode, String errorMessage) {

		ObjectNode error = getMapper().createObjectNode();
		error.put("errorCode", errorCode).put("errorMessage", errorMessage);
		return new AppResponse.Builder().status(statusCode).error(error).build();
	}

	
}
