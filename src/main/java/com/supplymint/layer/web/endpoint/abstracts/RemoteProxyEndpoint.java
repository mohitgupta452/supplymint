package com.supplymint.layer.web.endpoint.abstracts;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.supplymint.layer.business.service.core.ProxyService;
import com.supplymint.layer.data.core.entity.Proxy;

/**
 * 
 * @author Ashish Singh Dev
 * @version 1.0
 * @since Sept 14, 2018
 *
 */

@RestController
public class RemoteProxyEndpoint {

	private ProxyService proxyService;

	@Autowired
	public RemoteProxyEndpoint(ProxyService proxyService) {
		this.proxyService = proxyService;
	}

	@RequestMapping("/rp")
	String home() {
		return "Hello World!";
	}

	@RequestMapping(value = "/proxy/{identifier}/{module}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public JsonNode getByIdentifierAndModule(@PathVariable(value = "identifier") Integer identifier,
			@PathVariable(value = "module") Integer module) throws JsonProcessingException {
//		return proxyService.get1(identifier, module);
		Proxy data = proxyService.get1(identifier, module);
		try {
			ObjectMapper mapper = new ObjectMapper();
			JsonNode actualObj = mapper.readTree(data.getData());
			return actualObj;
		} catch (Exception e) {
			return null;
		}
	}

}