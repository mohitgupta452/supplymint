package com.supplymint.layer.web.endpoint.tenant;

import java.net.InetAddress;
import java.time.OffsetDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.supplymint.config.application.AppCodeConfig;
import com.supplymint.exception.StatusCodes.AppModuleErrors;
import com.supplymint.exception.StatusCodes.AppStatusCodes;
import com.supplymint.exception.StatusCodes.AppStatusMsg;
import com.supplymint.exception.StatusCodes.ClientErrors;
import com.supplymint.exception.SupplyMintException;
import com.supplymint.layer.business.contract.MCSTRService;
import com.supplymint.layer.data.tenant.mcstr.entity.MCSTR;
import com.supplymint.layer.web.endpoint.abstracts.AbstractEndpoint;
import com.supplymint.layer.web.model.AppResponse;
import com.supplymint.util.CodeUtils;

@RestController
@RequestMapping(path = MCSTREndpoint.PATH, produces = { MediaType.APPLICATION_JSON_VALUE })
public final class MCSTREndpoint extends AbstractEndpoint {

	public static final String PATH = "admin/mcstr";

	private static final Logger LOGGER = LoggerFactory.getLogger(MCSTREndpoint.class);

	private MCSTRService mcstrService;

	@Autowired
	private AppCodeConfig appStatus;

	@Autowired
	private MCSTREndpoint(MCSTRService mcstrService) {
		this.mcstrService = mcstrService;
	}

	@SuppressWarnings("finally")
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> insertDataM(@RequestBody MCSTR mcstr) {

		AppResponse appResponse = null;
		try {

			// String userName = (String)session.getAttribute("username");
			OffsetDateTime dateTime = OffsetDateTime.now();
			String ipAddress = InetAddress.getLocalHost().getHostAddress().trim().toString();
			// mcstr.setCreatedBy(userName);
			mcstr.setIpAddress(ipAddress);
			mcstr.setCreatedTime(dateTime);

			int result = mcstrService.insert(mcstr);

			if (result != 0) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, mcstr)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
						AppModuleErrors.MCSTR_CREATOR_ERROR, appStatus.getMessage(AppModuleErrors.MCSTR_CREATOR_ERROR));

			}

		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.MCSTR_CREATOR_ERROR,
					appStatus.getMessage(AppModuleErrors.MCSTR_CREATOR_ERROR));

			LOGGER.info(appStatus.getMessage(AppModuleErrors.MCSTR_CREATOR_ERROR));

		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);

		}

	}

	@SuppressWarnings("finally")

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)

	@ResponseBody
	public ResponseEntity<AppResponse> deleteMCSTR(@PathVariable(value = "id") String storeId) {

		AppResponse appResponse = null;
		int id = 0;
		try {

			id = Integer.parseInt(storeId);
			int result = mcstrService.deleteByStoreId(id);

			if (result != 0) {

				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
						AppModuleErrors.MCSTR_DELETE_ERROR, appStatus.getMessage(AppModuleErrors.MCSTR_DELETE_ERROR));
				LOGGER.info(appStatus.getMessage(AppModuleErrors.MCSTR_DELETE_ERROR));
			}
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.MCSTR_DELETE_ERROR,
					appStatus.getMessage(AppModuleErrors.MCSTR_DELETE_ERROR));

			LOGGER.info(appStatus.getMessage(AppModuleErrors.MCSTR_DELETE_ERROR));

		} catch (NumberFormatException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.NUMBER_FORMAT_MSG);

		}

		catch (Exception ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);

			throw new SupplyMintException(AppStatusMsg.FAILURE_MSG);

		}

		finally {
			return ResponseEntity.ok(appResponse);

		}

	}

}
