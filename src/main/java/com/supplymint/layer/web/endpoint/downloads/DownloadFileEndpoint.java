package com.supplymint.layer.web.endpoint.downloads;

import java.io.File;
import java.util.Map;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.supplymint.exception.StatusCodes.AppStatusCodes;
import com.supplymint.exception.StatusCodes.AppStatusMsg;
import com.supplymint.exception.StatusCodes.ClientErrors;
import com.supplymint.layer.business.contract.downloads.DownloadFile;
import com.supplymint.layer.business.contract.s3Bucket.S3BucketInfoService;
import com.supplymint.layer.business.service.s3bucketService.S3WrapperService;
import com.supplymint.layer.data.tenant.inventory.entity.S3BucketInfo;
import com.supplymint.layer.web.endpoint.abstracts.AbstractEndpoint;
import com.supplymint.layer.web.model.AppResponse;
import com.supplymint.util.ApplicationUtils.BucketParameter;
import com.supplymint.util.CodeUtils;

@RestController
@RequestMapping(path = DownloadFileEndpoint.PATH, produces = { MediaType.APPLICATION_JSON_VALUE })
public class DownloadFileEndpoint extends AbstractEndpoint {

	private final static Logger LOGGER = LoggerFactory.getLogger(DownloadFileEndpoint.class);
	public static final String PATH = "download";

	static String delimeter = "/";
	//public static final String GENERATE_EXCEL_CSV = "DOWNLOAD_EXCEL_CSV";
	
	@Autowired
	private S3WrapperService s3Wrapper;

	@Autowired
	private S3BucketInfoService s3BucketInfoService;

	@Autowired
	private DownloadFile downloadFile;

	@SuppressWarnings({ "finally", "rawtypes" })
	@GetMapping(value = "/module/{fileType}/{module}")
	public ResponseEntity<AppResponse> uploadExcel(@PathVariable(value = "fileType") String fileType,
			@PathVariable(value = "module") String moduleName, HttpServletRequest request) {

		
		AppResponse appResponse = null;
		File excelFile = null;
		String fileName = null;
		try {
			
			String token = ((HttpServletRequest) request).getHeader("X-Auth-Token");
			JsonArray orgDetails=CodeUtils.decode(token).get("ORG_DETAILS").getAsJsonArray();
			
			JsonObject jNode=orgDetails.get(0).getAsJsonObject();
			String orgId = jNode.get("orgId").getAsString();
			
			S3BucketInfo s3BucketInfo = s3BucketInfoService.getBucketByType(BucketParameter.GENERATE_EXCELCSV,orgId);

			if (!CodeUtils.isEmpty(s3BucketInfo.getS3bucketName())) {
				Map generatedExcelMap = downloadFile.generateExcelMap(fileType, moduleName, request);
				if (!CodeUtils.isEmpty(generatedExcelMap)) {
					String randomFolder = UUID.randomUUID().toString();
					String s3BucketFilePath = s3BucketInfo.getBucketPath() + delimeter + randomFolder;
					fileName = generatedExcelMap.get(BucketParameter.FILE_NAME).toString();
					excelFile = new File(generatedExcelMap.get(BucketParameter.FILE_PATH) + File.separator + fileName);
					fileName = s3BucketFilePath + delimeter + fileName;
					String etag = s3Wrapper.upload(excelFile, fileName, s3BucketInfo.getS3bucketName()).getETag();
					if (!CodeUtils.isEmpty(etag)) {
						s3BucketFilePath =BucketParameter.BUCKETPREFIX + s3BucketInfo.getS3bucketName() + delimeter + s3BucketFilePath;
						s3BucketFilePath = s3BucketFilePath.split("\\//")[1].replaceAll(
								s3BucketInfo.getS3bucketName() + delimeter, "") + delimeter.trim().toString();
						String url = s3Wrapper.downloadPreSignedURLOnHistory(s3BucketInfo.getS3bucketName(),
								s3BucketFilePath, Integer.parseInt(s3BucketInfo.getTime()));
						appResponse = new AppResponse.Builder()
								.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, url)
										.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
								.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
					}
				} else {
					appResponse = new AppResponse.Builder()
							.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
									.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
							.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				}
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SERVER_ERROR_CODE).build();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);

			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));

		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

}
