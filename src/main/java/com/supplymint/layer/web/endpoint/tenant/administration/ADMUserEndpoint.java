package com.supplymint.layer.web.endpoint.tenant.administration;

import java.io.BufferedOutputStream;
import java.io.File;
import java.net.InetAddress;
import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.dao.DataAccessException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.CannotGetJdbcConnectionException;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.supplymint.config.application.AppCodeConfig;
import com.supplymint.exception.StatusCodes.AppErrorCodes;
import com.supplymint.exception.StatusCodes.AppModuleErrors;
import com.supplymint.exception.StatusCodes.AppStatusCodes;
import com.supplymint.exception.StatusCodes.AppStatusMsg;
import com.supplymint.exception.StatusCodes.ClientErrors;
import com.supplymint.exception.StatusCodes.ServerErrors;
import com.supplymint.exception.SupplyMintException;
import com.supplymint.layer.business.contract.administration.ADMUserRoleService;
import com.supplymint.layer.business.contract.administration.ADMUserService;
import com.supplymint.layer.business.contract.s3Bucket.S3BucketInfoService;
import com.supplymint.layer.data.config.ThreadLocalStorage;
import com.supplymint.layer.data.downloads.entity.FileUpload;
import com.supplymint.layer.data.tenant.administration.entity.ADMUser;
import com.supplymint.layer.data.tenant.administration.entity.ADMUserRole;
import com.supplymint.layer.data.tenant.inventory.entity.S3BucketInfo;
import com.supplymint.layer.web.endpoint.abstracts.AbstractEndpoint;
import com.supplymint.layer.web.model.AppResponse;
import com.supplymint.util.ApplicationUtils.AppEnvironment;
import com.supplymint.util.ApplicationUtils.Conditions;
import com.supplymint.util.CodeUtils;
import com.supplymint.util.UploadUtils;

/**
 * 
 * This Endpoint is used inside #Administration for all the User details. it has
 * all the required CRUD operation.
 *
 */
@RestController
@RequestMapping(path = ADMUserEndpoint.PATH, produces = { MediaType.APPLICATION_JSON_VALUE })
public final class ADMUserEndpoint extends AbstractEndpoint {

	public static final String PATH = "admin/user";
	private static final String URI_CREATEPATH_PROD = "https://b7rav73vr7.execute-api.ap-south-1.amazonaws.com/prod/user/auth/create/user";
	private static final String URI_UPDATEPATH_PROD = "https://b7rav73vr7.execute-api.ap-south-1.amazonaws.com/prod/user/auth/update/user";
	private static final String URI_GET_USER_PROD = "https://b7rav73vr7.execute-api.ap-south-1.amazonaws.com/prod/user/auth/get/user/info";
	private static final String URI_CREATEPATH_DEV = "https://b7rav73vr7.execute-api.ap-south-1.amazonaws.com/dev/user/auth/create/user";
	private static final String URI_UPDATEPATH_DEV = "https://b7rav73vr7.execute-api.ap-south-1.amazonaws.com/dev/user/auth/update/user";
	private static final String URI_GET_USER_DEV = "https://b7rav73vr7.execute-api.ap-south-1.amazonaws.com/dev/user/auth/get/user/info";
	private static final Logger LOGGER = LoggerFactory.getLogger(ADMUserEndpoint.class);
	public static final String DOWNLOAD_IMAGE = "DOWNLOAD_IMG";

	@Autowired
	private S3BucketInfoService s3BucketInfoService;

	@Autowired
	private PlatformTransactionManager txManager;

	@Autowired
	private HttpServletRequest request;

	private ADMUserService admUserService;

	@Autowired
	private AppCodeConfig appStatus;

	@Autowired
	private ADMUserRoleService admUserRoleService;

	@Autowired
	private Environment env;

	@Autowired
	public ADMUserEndpoint(ADMUserService admUserService) {
		this.admUserService = admUserService;
	}

	/**
	 * 
	 * @param sid
	 * @return details by given specific ID from the table ADM_USERS
	 */
	@SuppressWarnings("finally")
	@RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getByUserId(@PathVariable(value = "id") String sid) {
		AppResponse appResponse = null;
		int id = 0;
		try {
			id = Integer.parseInt(sid);
			LOGGER.debug("Query,for finding the all details by specific id,is going to start...");
			ADMUser data = admUserService.findById(id);
			LOGGER.debug("Query,for finding the all details by specific id,has ended...");
			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, data)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = buildErrorResponse(AppErrorCodes.GENERIC_EVENT_NOT_AVAILABLE_ERROR_CODE,
						ClientErrors.NOT_FOUND, AppStatusMsg.BLANK);
				LOGGER.info(AppStatusMsg.BLANK);
			}
		} catch (CannotGetJdbcConnectionException ex) {
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.USER_READER_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.USER_READER_ERROR, appStatus.getMessage(AppModuleErrors.USER_READER_ERROR));
		} catch (NumberFormatException ex) {
			LOGGER.debug(String.format("Error Occoured From NumberFormatException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.NUMBER_FORMAT_MSG);

		} catch (Exception e) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", e));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.info(AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}

	}

	/**
	 * 
	 * @param userNode
	 * @return This API is used to validate and create new ADM user
	 */
	@SuppressWarnings({ "finally", "unused" })
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> create(@RequestBody JsonNode userNode) {

		TransactionDefinition defaultTxn = new DefaultTransactionDefinition();
		final TransactionStatus ts = txManager.getTransaction(defaultTxn);
		int result = 0;
		AppResponse appResponse = null;
		ResponseEntity<AppResponse> responseEntity = null;
		ADMUser admUser = null;
		boolean isExist = false;
		String userRoles[] = new String[4];
		String errMessage = null;
		ADMUserRole userRole = new ADMUserRole();

		try {

			admUser = getMapper().treeToValue(userNode, ADMUser.class);
			responseEntity = admUserService.sendUserDetailsOnRestAPI(admUser,
					env.getActiveProfiles().length == 0
							|| env.getActiveProfiles()[0].equalsIgnoreCase(AppEnvironment.Profiles.DEV.toString())
									? URI_CREATEPATH_DEV
									: env.getActiveProfiles()[0].equalsIgnoreCase(
											AppEnvironment.Profiles.PROD.toString()) ? URI_CREATEPATH_PROD
													: URI_CREATEPATH_DEV);
			
			if (Integer.parseInt(responseEntity.getBody().getStatus()) == 2000) {
				LOGGER.info("Query for validating user existence details in main DB is going to satrt...");
				List<ADMUser> validation = admUserService.checkUserExistance(admUser);
				LOGGER.info("Query for validating user existence details in main DB has ended...");
				if (validation.size() == 0) {
					// String username = admUser.getUserName();
					// int isExistUsername = admUserService.checkUsername(username);
					// if (isExistUsername == 0) {
					OffsetDateTime currentTime = OffsetDateTime.now();
					currentTime = currentTime.plusHours(5);
					currentTime = currentTime.plusMinutes(30);
					// String userName = (String)session.getAttribute("username");
					// admUser.setCreatedBy("");
					if (!CodeUtils.isEmpty(request)) {
						admUser.setCreatedBy(request.getAttribute("email").toString());
					}
					admUser.setCreatedTime(currentTime);
					String ipAddress = InetAddress.getLocalHost().getHostAddress().trim().toString();
					admUser.setIpAddress(ipAddress);
					if (admUser.getStatus().equalsIgnoreCase("Active")) {
						admUser.setActive('1');
					} else {
						admUser.setActive('0');
					}
					LOGGER.debug("QUery,for inserting user data into table,is going to satrt...");
					result = admUserService.create(admUser);
					LOGGER.debug("QUery,for inserting user data into table,has ended...");
					if (result != 0) {
						isExist = true;
						int roleResult = 0;
						boolean role = false;
						String newRole = Arrays.toString(admUser.getUserRoles()).substring(1);
						newRole = newRole.substring(0, newRole.length() - 1);
						userRole.setUserId(admUser.getUserId());
						userRole.setUserName(admUser.getUserName());
						userRole.setName(newRole);
						userRole.setActive(admUser.getActive());
						userRole.setStatus(admUser.getStatus());
						if (admUser.getMiddleName() != null) {
							userRole.setCreatedBy(admUser.getFirstName() + " " + admUser.getMiddleName() + " "
									+ admUser.getLastName());
						} else {
							userRole.setCreatedBy(admUser.getFirstName() + " " + admUser.getLastName());
						}
						userRole.setCreatedTime(admUser.getCreatedTime());
						userRole.setIpAddress(admUser.getIpAddress());
						LOGGER.debug("QUery,for inserting user role data into table,is going to satrt...");
						roleResult = admUserRoleService.create(userRole);
						LOGGER.debug("QUery,for inserting user role data into table,has ended...");
						if (roleResult == 1) {
							txManager.commit(ts);
//							admUserService.sendUserDetailsOnRestAPI(admUser, _URI_CREATEPATH);
							appResponse = new AppResponse.Builder()
									.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, admUser)
											.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_USER_MSG))
									.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
						} else {
							txManager.rollback(ts);
							appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
									AppModuleErrors.USER_ROLE_CREATOR_ERROR,
									appStatus.getMessage(AppModuleErrors.USER_ROLE_CREATOR_ERROR));
						}

					} else {
						LOGGER.info(appStatus.getMessage(AppModuleErrors.USER_CREATOR_ERROR));
						appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
								AppModuleErrors.USER_CREATOR_ERROR,
								appStatus.getMessage(AppModuleErrors.USER_CREATOR_ERROR));
					}
				} else {
					String email = admUser.getEmail();
					String userName = admUser.getUserName();

					boolean isContainingEmail = validation.parallelStream()
							.allMatch(i -> i.getEmail().toLowerCase().equals(email.toLowerCase()));

					boolean isContainingUserName = validation.parallelStream()
							.allMatch(i -> i.getUserName().toLowerCase().equals(userName.toLowerCase()));

					if (isContainingEmail) {
						appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE,
								ClientErrors.BAD_REQUEST, AppStatusMsg.EXIST_USER_EMAIL);
					} else if (isContainingUserName) {
						appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE,
								ClientErrors.BAD_REQUEST, AppStatusMsg.EXIST_USER);
					}

					else {
						appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE,
								ClientErrors.BAD_REQUEST, AppStatusMsg.EXIST_USER_MOBILE);

					}
				}
			} else if (responseEntity.getBody().getError().get(CodeUtils.RESPONSE_MSG) != null) {
				errMessage = responseEntity.getBody().getError().get(CodeUtils.RESPONSE_MSG).asText();
				if (errMessage != null) {
					if (errMessage.contains("unique")) {
						appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE,
								AppModuleErrors.USER_READER_ERROR, AppStatusMsg.AUTH_USER_EXIST);
					} else {
						appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE,
								AppModuleErrors.USER_READER_ERROR, errMessage);
					}
				}
			}
		} catch (CannotGetJdbcConnectionException ex) {
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.USER_CREATOR_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (IndexOutOfBoundsException | DataAccessException ex) {
			LOGGER.debug(String.format("Error Occoured From IndexOutOfBoundsException | DataAccessException : %s", ex));
			ex.printStackTrace();
			if (isExist == false)
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
						AppModuleErrors.USER_CREATOR_ERROR, appStatus.getMessage(AppModuleErrors.USER_CREATOR_ERROR));
			else {
				txManager.rollback(ts);
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
						AppModuleErrors.USER_ROLE_CREATOR_ERROR,
						appStatus.getMessage(AppModuleErrors.USER_ROLE_CREATOR_ERROR));
			}

			LOGGER.info(appStatus.getMessage(AppModuleErrors.USER_CREATOR_ERROR));

		} catch (JsonMappingException jsonException) {
			LOGGER.debug(String.format("Error Occoured From NumberFormatException : %s", jsonException));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.INVALID_JSON);
			LOGGER.info(jsonException.getMessage());

		} catch (SupplyMintException ex) {
			LOGGER.debug(String.format("Error Occoured From SupplyMintException : %s", ex));
			appResponse = new AppResponse.Builder()
					.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, admUser)
							.put(CodeUtils.RESPONSE_MSG, ex.getMessage()))
					.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			if (isExist == true)
				txManager.rollback(ts);

			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.info(ex.getMessage());
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	/**
	 * 
	 * @param userNode
	 * @param request
	 * @return This API is used to update the ADM user details.
	 */
	@SuppressWarnings({ "finally", "unused" })
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> update(@RequestBody JsonNode userNode, HttpServletRequest request) {
		AppResponse appResponse = null;
		ResponseEntity<AppResponse> responseEntity = null;
		ADMUser users = null;
		int result = 0;
		try {
			users = getMapper().treeToValue(userNode, ADMUser.class);
			LOGGER.debug("Query,for validating existence user details,is going to satrt...");
			List<ADMUser> validation = admUserService.checkUserUpdateExistance(users);
			LOGGER.debug("Query,for validating details,has ended...");
			int roleResult = 0;
			if (validation.size() == 0) {
				OffsetDateTime updationTime = OffsetDateTime.now();
				updationTime = updationTime.plusHours(5);
				updationTime = updationTime.plusMinutes(30);
				users.setUpdationTime(updationTime);
				if (!CodeUtils.isEmpty(request)) {
					users.setUpdatedBy(request.getAttribute("email").toString());
				}
				String ipAddress = InetAddress.getLocalHost().getHostAddress().trim().toString();
				users.setIpAddress(ipAddress);
				LOGGER.info(users.toString());
				LOGGER.info(users.getUserName());
				if (users.getStatus().equalsIgnoreCase("Active")) {
					users.setActive('1');
				} else {
					users.setActive('0');
				}
				LOGGER.debug("Query,for finding the all user details by specific user name,is going to start...");
				ADMUserRole userRoles = admUserRoleService.findByUserName(users.getUserName());
				LOGGER.debug("Query,for finding the all details by specific user name,has ended...");
				if (!CodeUtils.isEmpty(userRoles)) {
					LOGGER.info(users.getStatus());
					userRoles.setStatus(users.getStatus());
					if (!CodeUtils.isEmpty(request)) {
						userRoles.setUpdatedBy(request.getAttribute("email").toString());
					}
					userRoles.setIpAddress(users.getIpAddress());
					userRoles.setUpdationTime(users.getUpdationTime());
					userRoles.setActive(users.getActive());
					LOGGER.debug("Query,for updating status, is going to start...");
					roleResult = admUserRoleService.updateStatusByUserName(userRoles);
					LOGGER.debug("Query,for updating status,has ended...");
				}
				LOGGER.debug("Query,for updating user data, is going to start...");
				result = admUserService.update(users);
				LOGGER.debug("Query,for updating data, has ended...");
				if (result != 0) {
					responseEntity = admUserService.sendUserDetailsOnRestAPI(users,
							env.getActiveProfiles().length == 0 || env.getActiveProfiles()[0]
									.equalsIgnoreCase(AppEnvironment.Profiles.DEV.toString())
											? URI_UPDATEPATH_DEV
											: env.getActiveProfiles()[0].equalsIgnoreCase(
													AppEnvironment.Profiles.PROD.toString()) ? URI_UPDATEPATH_PROD
															: URI_UPDATEPATH_DEV);
					appResponse = new AppResponse.Builder()
							.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, users)
									.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.USER_UPDATE_SUCCESS_MSG))
							.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

				} else {
					appResponse = new AppResponse.Builder()
							.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
									.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
							.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				}

			} else {
				String email = users.getEmail();
				String userName = users.getUserName();

				boolean isContainingEmail = validation.parallelStream()
						.allMatch(i -> i.getEmail().toLowerCase().equals(email.toLowerCase()));

				boolean isContainingUserName = validation.parallelStream()
						.allMatch(i -> i.getUserName().toLowerCase().equals(userName.toLowerCase()));

				if (isContainingEmail) {
					appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
							AppStatusMsg.EXIST_USER_EMAIL);
				}

				else if (isContainingUserName) {
					appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
							AppStatusMsg.EXIST_USER);
				}

				else {
					appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
							AppStatusMsg.EXIST_USER_MOBILE);

				}

			}

		} catch (JsonMappingException jsonException) {
			LOGGER.debug(String.format("Error Occoured From JsonMappingException : %s", jsonException));
			jsonException.printStackTrace();
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.info(jsonException.getMessage());

		} catch (CannotGetJdbcConnectionException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.USER_UPDATE_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			ex.printStackTrace();
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.USER_UPDATE_ERROR, appStatus.getMessage(AppModuleErrors.USER_UPDATE_ERROR));
			LOGGER.info(appStatus.getMessage(AppModuleErrors.USER_UPDATE_ERROR));

		} catch (Exception e) {
			e.printStackTrace();
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	/**
	 * 
	 * @param sid
	 * @return delete the details by specific ID
	 */

	@SuppressWarnings("finally")
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> delete(@PathVariable(value = "id") String sid) {

		TransactionDefinition defaultTxn = new DefaultTransactionDefinition();
		final TransactionStatus ts = txManager.getTransaction(defaultTxn);
		AppResponse appResponse = null;
		int id = 0;
		int countUserRoleUser = 0;
		int roleResult = 0;
		int result = 0;
		boolean isExist = false;
		try {
			id = Integer.parseInt(sid);
			LOGGER.debug("Query,for counting the data,is going to start...");
			countUserRoleUser = admUserService.countUserRoleUser(id);
			LOGGER.debug("Query,for counting the data,is going to start...");
			roleResult = admUserRoleService.deleteUser(id);

			if (countUserRoleUser > 0 && countUserRoleUser == roleResult) {
				isExist = true;
				result = admUserService.delete(id);

				if (result != 0) {
					txManager.commit(ts);
					appResponse = new AppResponse.Builder()
							.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
									.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
							.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				} else {
					txManager.rollback(ts);
					appResponse = buildErrorResponse(AppErrorCodes.GENERIC_EVENT_NOT_AVAILABLE_ERROR_CODE,
							ClientErrors.NOT_FOUND, AppStatusMsg.BLANK);
					LOGGER.info(AppStatusMsg.BLANK);
				}
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				LOGGER.info(AppStatusMsg.BLANK);

			}
		} catch (CannotGetJdbcConnectionException ex) {
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.USER_DELETE_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {

			if (isExist)
				txManager.rollback(ts);

			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.USER_DELETE_ERROR, appStatus.getMessage(AppModuleErrors.USER_DELETE_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (NumberFormatException ex) {
			LOGGER.debug(String.format("Error Occoured From NumberFormatException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.NUMBER_FORMAT_MSG);

		} catch (Exception e) {
			if (isExist)
				txManager.rollback(ts);

			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", e));
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	/**
	 * 
	 * @param userNode
	 * @param request
	 * @return updated details
	 */
	@SuppressWarnings("finally")
	@RequestMapping(value = "/update/status", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> updateStatus(@RequestBody JsonNode userNode, HttpServletRequest request) {

		AppResponse appResponse = null;
		ResponseEntity<AppResponse> responseEntity = null;
		// ADMUser users = null;
		int result = 0;
		int id = 0;
		String status = null;
		String userName = null;
		int roleResult = 0;
		try {

			if (userNode.has("userName") && userNode.has("status")) {
				userName = userNode.get("userName").asText();
				status = userNode.get("status").asText();
			}
			String updatedBy = request.getAttribute("email").toString();

			OffsetDateTime updationTime = OffsetDateTime.now();
			updationTime = updationTime.plusHours(5);
			updationTime = updationTime.plusMinutes(30);
			String ipAddress = InetAddress.getLocalHost().getHostAddress().trim().toString();
			LOGGER.debug("Query,for getting details according to  user name,is going to start...");
			ADMUserRole userRoles = admUserRoleService.findByUserName(userName);
			LOGGER.debug("Query,for getting details according to  user name,has ended...");
			if (!CodeUtils.isEmpty(userRoles)) {

				LOGGER.info(status);
				userRoles.setStatus(status);
				if (!CodeUtils.isEmpty(request)) {
					userRoles.setUpdatedBy(request.getAttribute("email").toString());
				}

				userRoles.setIpAddress(ipAddress);
				userRoles.setUpdationTime(updationTime);
				LOGGER.debug("Query,for updating details,is going to start...");
				roleResult = admUserRoleService.updateStatusByUserName(userRoles);
				LOGGER.debug("Query,for updating details,has ended...");
			}
			LOGGER.debug("Query,for updating details,is going to start...");
			result = admUserService.updateStatus(status, userName, ipAddress, updationTime, updatedBy);
			LOGGER.debug("Query,for updating details,has ended...");
			if (result != 0) {
				LOGGER.debug("Query,for getting details according to  user name,is going to start...");
				ADMUser data = admUserService.findByUserName(userName);
				LOGGER.debug("Query,for getting details according to  user name,has ended...");
				if (data.getStatus().equalsIgnoreCase(Conditions.Active.toString()))
					data.setActive('1');
				else
					data.setActive('0');

				responseEntity = admUserService.sendUserDetailsOnRestAPI(data,
						env.getActiveProfiles().length == 0
								|| env.getActiveProfiles()[0].equalsIgnoreCase(AppEnvironment.Profiles.DEV.toString())
										? URI_UPDATEPATH_DEV
										: env.getActiveProfiles()[0].equalsIgnoreCase(
												AppEnvironment.Profiles.PROD.toString()) ? URI_UPDATEPATH_PROD
														: URI_UPDATEPATH_DEV);

				// responseEntity = admUserService.sendUserDetailsOnRestAPI(data,
				// _URI_UPDATEPATH);

				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, data)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_USER_STATUS_MSG + " " + status))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
						AppModuleErrors.USER_UPDATE_ERROR, appStatus.getMessage(AppModuleErrors.USER_UPDATE_ERROR));
				LOGGER.info(appStatus.getMessage(AppModuleErrors.USER_UPDATE_ERROR));
			}

		} catch (CannotGetJdbcConnectionException ex) {
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.USER_UPDATE_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.USER_UPDATE_ERROR, appStatus.getMessage(AppModuleErrors.USER_UPDATE_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));

		} catch (NumberFormatException ex) {

			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.NUMBER_FORMAT_MSG);
			LOGGER.debug(String.format("Error Occoured From NumberFormatException : %s", ex));

		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.debug(String.format("Error Occoured From Exception : %s", e));
		} finally {
			return ResponseEntity.ok(appResponse);
		}

	}

	/**
	 * 
	 * @param accessmode
	 * @return all the details according to assortment code
	 */
	@SuppressWarnings({ "unchecked", "deprecation", "finally", "rawtypes" })
	@RequestMapping(value = "/get/accessmode/{accessmode}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getAccessmode(@PathVariable("accessmode") String accessmode) {

		AppResponse appResponse = null;
		try {
			LOGGER.debug("Query,for getting all details according to assortment,is going to start...");
			List data = admUserService.getAccessmode(accessmode);
			LOGGER.debug("Query,for getting all details according to assortment,has ended...");
			ObjectNode objectNode = getMapper().createObjectNode();
			ArrayNode array = getMapper().createArrayNode();
			data.forEach(e -> {
				ObjectNode node = getMapper().valueToTree(e);
				array.add(node);
			});
			objectNode.put(CodeUtils.RESPONSE, array);

			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(objectNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}

		} catch (CannotGetJdbcConnectionException ex) {
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.USER_READER_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.USER_READER_ERROR, appStatus.getMessage(AppModuleErrors.USER_READER_ERROR));

			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.debug(String.format("Error Occoured From Exception : %s", e));
		} finally {
			return ResponseEntity.ok(appResponse);
		}

	}

	/**
	 * 
	 * @param sid
	 * @return details according to enterprise id
	 */
	@SuppressWarnings({ "finally", "unchecked", "rawtypes", "deprecation" })
	@RequestMapping(value = "/get/enterpriseid/{id}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getEnterPriseId(@PathVariable("id") String sid) {

		AppResponse appResponse = null;
		int id = 0;
		try {

			id = Integer.parseInt(sid);
			LOGGER.debug("Query,for getting details,is going to start...");
			List data = admUserService.getByEnterPriseId(id);
			LOGGER.debug("Query,for getting details,has ended...");
			ObjectNode objectNode = getMapper().createObjectNode();
			ArrayNode array = getMapper().createArrayNode();
			data.forEach(e -> {
				ObjectNode node = getMapper().valueToTree(e);
				array.add(node);
			});
			objectNode.put(CodeUtils.RESPONSE, array);

			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(objectNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}

		} catch (NumberFormatException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.NUMBER_FORMAT_MSG);
			LOGGER.debug(String.format("Error Occoured From NumberFormatException : %s", ex));
		} catch (CannotGetJdbcConnectionException ex) {
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.USER_READER_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.USER_READER_ERROR, appStatus.getMessage(AppModuleErrors.USER_READER_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));

		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.debug(String.format("Error Occoured From Exception : %s", e));
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings("finally")
	@RequestMapping(value = "/get/name/{name}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getUserName(@PathVariable("name") String name) {

		AppResponse appResponse = null;
		try {
			LOGGER.debug("Query,for getting details,is going to start...");
			ADMUser data = admUserService.getByUserName(name);
			LOGGER.debug("Query,for getting details,has ended...");
			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, data)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				LOGGER.info(AppStatusMsg.BLANK);
			}
		} catch (CannotGetJdbcConnectionException ex) {
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.USER_READER_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.USER_READER_ERROR, appStatus.getMessage(AppModuleErrors.USER_READER_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));

		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.debug(String.format("Error Occoured From Exception : %s", e));
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings("finally")
	@RequestMapping(value = "/get/email", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getEmailId(@RequestParam("email") String email) {

		AppResponse appResponse = null;
		try {
			LOGGER.debug("Query,for getting details,is going to start...");
			ADMUser data = admUserService.getByEmail(email).get(0);
			LOGGER.debug("Query,for getting details,has ended...");
			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, data)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				LOGGER.info(AppStatusMsg.BLANK);
			}
		} catch (CannotGetJdbcConnectionException ex) {
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.USER_READER_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.USER_READER_ERROR, appStatus.getMessage(AppModuleErrors.USER_READER_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					ServerErrors.INTERNAL_SERVER_ERROR, AppStatusMsg.FAILURE_MSG);
			LOGGER.debug(String.format("Error Occoured From Exception : %s", e));
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "deprecation", "finally" })
	@RequestMapping(value = "/get/all/userid", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getAllUser() {

		AppResponse appResponse = null;
		try {
			LOGGER.debug("Query,for getting details,is going to start...");
			List<ADMUser> data = admUserService.getAllUser();
			LOGGER.debug("Query,for getting details,has ended...");
			LOGGER.info(InetAddress.getLocalHost().getHostAddress().trim().toString());
			ObjectNode objectNode = getMapper().createObjectNode();
			ArrayNode array = getMapper().createArrayNode();
			data.forEach(e -> {
				// ObjectNode node = getMapper().valueToTree(e);
				ObjectNode userNode = getMapper().createObjectNode();
				userNode.put("userId", e.getUserId());
				userNode.put("userName", e.getUserName());
				String displayUserName = e.getFirstName() + " " + e.getLastName();
				userNode.put("displayName", displayUserName);

				array.add(userNode);
			});
			objectNode.put(CodeUtils.RESPONSE, array);

			if (!CodeUtils.isEmpty(data))
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(objectNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			else
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
		} catch (CannotGetJdbcConnectionException ex) {
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.USER_READER_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.USER_READER_ERROR, appStatus.getMessage(AppModuleErrors.USER_READER_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (NumberFormatException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.NUMBER_FORMAT_MSG);
			LOGGER.debug(String.format("Error Occoured From NumberFormatException : %s", ex));

		} catch (Exception ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally" })
	@RequestMapping(value = "/get/all", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getAll(@RequestParam("pageNo") Integer pageNo,
			@RequestParam("type") Integer type, @RequestParam("search") String search,
			@RequestParam("partnerEnterpriseName") String partnerEnterpriseName,
			@RequestParam("firstName") String firstName, @RequestParam("middleName") String middleName,
			@RequestParam("lastName") String lastName, @RequestParam("userName") String userName,
			@RequestParam("email") String email, @RequestParam("status") String status, HttpServletRequest request) {

		AppResponse appResponse = null;
		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;
		ObjectNode objectNode;
		List<ADMUser> data = null;
		ADMUser admUser = null;
		int flag = 0;

		try {
			flag = type;
			LOGGER.debug(String.format("TenantHashKey : %s", ThreadLocalStorage.getTenantHashKey()));
			String token = ((HttpServletRequest) request).getHeader("X-Auth-Token");
			String enterpriseUserName = CodeUtils.decode(token).get("prn").getAsString();
			if (flag == 1) {
				LOGGER.debug("Query,for getting all record,is going to start...");
				totalRecord = admUserService.record();
				LOGGER.debug("Query,for getting all record,has ended...");
				totalRecord = totalRecord - 1;
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo - 1) * pageSize;
				LOGGER.debug("Query,for getting all record,is going to start...");
				data = admUserService.findAll(offset, pageSize, enterpriseUserName);
				LOGGER.debug("Query,for getting all record,has ended...");
				for (int i = 0; i < data.size(); i++) {

					if (!CodeUtils.isEmpty(data.get(i).getUserRole())) {
						LOGGER.info(data.get(i).getUserRole().getName());

						if (data.get(i).getUserRole().getName().contains(",")) {
							String[] roles = data.get(i).getUserRole().getName().split(",");
							data.get(i).getUserRole().setUserRoles(roles);
						} else {
							data.get(i).getUserRole()
									.setUserRoles(new String[] { data.get(i).getUserRole().getName() });
						}
					}
				}

				if (!CodeUtils.isEmpty(data)) {

					objectNode = buildJson(data);
					appResponse = successResponse(pageNo, previousPage, maxPage, objectNode);
					LOGGER.info(AppStatusMsg.SUCCESS_MSG);

				} else {
					appResponse = blankResponse();
					LOGGER.info(AppStatusMsg.BLANK);
				}
			} else if (flag == 2) {
				// filter
				admUser = new ADMUser();
				admUser.setPartnerEnterpriseName(partnerEnterpriseName);
				admUser.setFirstName(firstName);
				admUser.setMiddleName(middleName);
				admUser.setLastName(lastName);
				admUser.setUserName(userName);
				admUser.setEmail(email);
				admUser.setStatus(status);
				admUser.setEnterpriseUserName(enterpriseUserName);
				LOGGER.debug("Query,for getting all record,is going to start...");
				totalRecord = admUserService.filterRecord(admUser);
				LOGGER.debug("Query,for getting all record,has ended...");
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo - 1) * pageSize;
				LOGGER.debug("Query,for filtering all record,is going to start...");
				data = admUserService.filterUser(offset, pageSize, admUser, enterpriseUserName);
				LOGGER.debug("Query,for filtering all record,has ended...");
				if (!CodeUtils.isEmpty(data)) {
					objectNode = buildJson(data);
					appResponse = successResponse(pageNo, previousPage, maxPage, objectNode);

				} else {
					appResponse = blankResponse();
					LOGGER.info(AppStatusMsg.BLANK);
				}
			} else {
				if (flag == 3 && !search.isEmpty() && !search.equalsIgnoreCase(enterpriseUserName)) {

					LOGGER.debug("Query,for searching all record,is going to start...");
					totalRecord = admUserService.searchRecord(search);
					LOGGER.debug("Query,for searching all record,has ended...");
					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo - 1) * pageSize;
					LOGGER.debug("Query,for all searched record,is going to start...");
					data = admUserService.searchAll(offset, pageSize, search);
					LOGGER.debug("Query,for all searched record,has ended...");
					if (!CodeUtils.isEmpty(data)) {
						appResponse = successResponse(pageNo, previousPage, maxPage, buildJson(data));

					} else {
						appResponse = blankResponse();
					}
				} else {
					appResponse = new AppResponse.Builder()
							.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
									.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
							.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				}
			}
		} catch (CannotGetJdbcConnectionException ex) {
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.USER_READER_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.USER_READER_ERROR, appStatus.getMessage(AppModuleErrors.USER_READER_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings("deprecation")
	private ObjectNode buildJson(List<ADMUser> data) {
		ObjectNode objectNode = getMapper().createObjectNode();
		ArrayNode array = getMapper().createArrayNode();
		data.forEach(e -> {
			ObjectNode node = getMapper().valueToTree(e);
			array.add(node);
		});
		objectNode.put(CodeUtils.RESPONSE, array);
		return objectNode;
	}

	@SuppressWarnings({ "finally", "unused" })
	@RequestMapping(value = "/update/profile", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> updateProfile(@RequestBody JsonNode payload, HttpServletRequest request) {

		AppResponse appResponse = null;
		ADMUser user = new ADMUser();
		int result = 0;
		long diff = 0L;
		long diffDays = 0L;
		long diffHours = 0L;
		long diffMinutes = 0L;
		String duration = null;
		try {
			String firstName = payload.get("firstName").asText();
			String lastName = payload.get("lastName").asText();
			String mobileNumber = payload.get("mobileNumber").asText();
			String userName = payload.get("userName").asText();
			int eid = Integer.parseInt(payload.get("eid").asText());
			LOGGER.debug("Query,for getting all data by user name,is going to start...");
			ADMUser admUser = admUserService.getByUserName(userName);
			LOGGER.debug("Query,for getting all data by user name,has ended...");
			Date updatedOn = new Date();
			updatedOn = CodeUtils.convertDateOnTimeZonePluseForAWS(updatedOn, 5, 30);
			Date previousUpdateTime = admUser.getImageUpdatedOn();
			if (previousUpdateTime != null) {
				diff = updatedOn.getTime() - previousUpdateTime.getTime();
				diffDays = diff / (24 * 60 * 60 * 1000);
				if (diffDays >= 1) {
					duration = Long.toString(diffDays) + " Days ago";
				} else {
					diffHours = diff / (60 * 60 * 1000) % 24;
					if (diffHours >= 1) {
						duration = Long.toString(diffHours) + " Hours ago";
					} else {
						diffMinutes = diff / (60 * 1000) % 60;
						duration = Long.toString(diffMinutes) + " Minutes ago";
					}
				}
			} else {
				diff = updatedOn.getTime();
				diffHours = diff / (60 * 60 * 1000) % 24;
				if (diffHours >= 1) {
					duration = Long.toString(diffHours) + " Hours ago";
				} else {
					diffMinutes = diff / (60 * 1000) % 60;
					duration = Long.toString(diffMinutes) + " Minutes ago";
				}
			}
			user.setFirstName(firstName);
			user.setLastName(lastName);
			user.setMobileNumber(mobileNumber);
			user.setUserName(userName);
			user.setPartnerEnterpriseId(eid);
			OffsetDateTime updationTime = OffsetDateTime.now();
			updationTime = updationTime.plusHours(5);
			updationTime = updationTime.plusMinutes(30);
			user.setUpdationTime(updationTime);
			LOGGER.debug("Query,for updating all data,is going to start...");
			int updateTimeByUserName = admUserService.updateTimeByUserName(user.getUserName(), updatedOn);
			LOGGER.debug("Query,for updating all data,has ended...");
			if (!CodeUtils.isEmpty(request)) {
				user.setUpdatedBy(request.getAttribute("email").toString());
			}
			String ipAddress = InetAddress.getLocalHost().getHostAddress().trim().toString();
			user.setIpAddress(ipAddress);
			LOGGER.debug("Query,for updating all data by user name,is going to start...");
			result = admUserService.updateByUserName(user);
			LOGGER.debug("Query,for updating all data by user name,has ended...");
			ObjectNode objectNode = getMapper().createObjectNode();
			objectNode.put("updatedOn", duration);
			if (result != 0) {
				admUserService.sendUserDetailsOnRestAPI(user,
						env.getActiveProfiles().length == 0
								|| env.getActiveProfiles()[0].equalsIgnoreCase(AppEnvironment.Profiles.DEV.toString())
										? URI_UPDATEPATH_DEV
										: env.getActiveProfiles()[0].equalsIgnoreCase(
												AppEnvironment.Profiles.PROD.toString()) ? URI_UPDATEPATH_PROD
														: URI_UPDATEPATH_DEV);
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, objectNode)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.PROFILE_SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			}
		} catch (CannotGetJdbcConnectionException ex) {
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.USER_UPDATE_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			ex.printStackTrace();
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.USER_UPDATE_ERROR, appStatus.getMessage(AppModuleErrors.USER_UPDATE_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception e) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", e));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally", "deprecation" })
	@RequestMapping(value = "/get/userName/{userName}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getByUserName(@PathVariable("userName") String userName) {

		AppResponse appResponse = null;
		ADMUser admUser = new ADMUser();
		String mobileNumber = null;
		ResponseEntity<AppResponse> responseEntity = null;
		String firstName = null;
		String lastName = null;
		String email = null;
		String eid = null;
		String ename = null;
		String errMessage = null;
		String newRole = "";
		long diff = 0L;
		long diffDays = 0L;
		long diffHours = 0L;
		long diffMinutes = 0L;
		String duration = null;
		ADMUserRole userRole = new ADMUserRole();
		int result = 0;
		int roleResult = 0;
		try {
			LOGGER.debug("Query,for validating all data by user name,is going to start...");
			int isExistUsername = admUserService.checkUsername(userName);
			LOGGER.debug("Query,for validating all data by user name,has ended...");
			if (isExistUsername > 0) {
				LOGGER.debug("Query,for getting all data by user name,is going to start...");
				admUser = admUserService.getUserDetail(userName);
				LOGGER.debug("Query,for getting all data by user name,has ended...");
				if (!CodeUtils.isEmpty(admUser)) {
					LOGGER.debug("Query,for getting all data by user name,is going to start...");
					admUser = admUserService.getByUserName(userName);
					LOGGER.debug("Query,for getting all data by user name,has ended...");
					Date updationTime = new Date();
					updationTime = CodeUtils.convertDateOnTimeZonePluseForAWS(updationTime, 5, 30);
					Date previousUpdateTime = admUser.getImageUpdatedOn();
					if (previousUpdateTime != null) {
						diff = updationTime.getTime() - previousUpdateTime.getTime();
						diffDays = diff / (24 * 60 * 60 * 1000);
						if (diffDays >= 1) {
							duration = Long.toString(diffDays) + " Days ago";
						} else {
							diffHours = diff / (60 * 60 * 1000) % 24;
							if (diffHours >= 1) {
								duration = Long.toString(diffHours) + " Hours ago";
							} else {
								diffMinutes = diff / (60 * 1000) % 60;
								duration = Long.toString(diffMinutes) + " Minutes ago";
							}
						}
					} else {
						diff = updationTime.getTime();
						diffHours = diff / (60 * 60 * 1000) % 24;
						if (diffHours >= 1) {
							duration = Long.toString(diffHours) + " Hours ago";
						} else {
							diffMinutes = diff / (60 * 1000) % 60;
							duration = Long.toString(diffMinutes) + " Minutes ago";
						}
					}

					ObjectNode objectNode = getMapper().createObjectNode();
					ObjectNode node = getMapper().createObjectNode();
					node.put("updatedOn", duration);
					node.putPOJO("userDetails", admUser);
					objectNode.put(CodeUtils.RESPONSE, node);
					appResponse = new AppResponse.Builder()
							.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(objectNode))
							.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				}
			} else {
				responseEntity = admUserService.getUserDetailsOnRestAPI(userName,
						env.getActiveProfiles().length == 0
								|| env.getActiveProfiles()[0].equalsIgnoreCase(AppEnvironment.Profiles.DEV.toString())
										? URI_GET_USER_DEV
										: env.getActiveProfiles()[0].equalsIgnoreCase(
												AppEnvironment.Profiles.PROD.toString()) ? URI_GET_USER_PROD
														: URI_GET_USER_DEV);

				if (responseEntity.toString().contains(CodeUtils.RESPONSE)) {
					firstName = responseEntity.getBody().getData().findValue("firstName").asText();
					lastName = responseEntity.getBody().getData().findValue("lastName").asText();
					email = responseEntity.getBody().getData().findValue("email").asText();
					eid = responseEntity.getBody().getData().findValue("eid").asText();
					ename = responseEntity.getBody().getData().findValue("ename").asText();
					if (responseEntity.toString().contains("mobileNumber")) {
						mobileNumber = responseEntity.getBody().getData().findValue("mobileNaumber").asText();
						if (mobileNumber.equalsIgnoreCase("null")) {
							mobileNumber = null;
						}
					}
					admUser.setFirstName(firstName);
					admUser.setLastName(lastName);
					admUser.setMobileNumber(mobileNumber);
					admUser.setEmail(email);
					admUser.setUserName(userName);
					admUser.setStatus("Active");
					String ipAddress = InetAddress.getLocalHost().getHostAddress().trim().toString();
					OffsetDateTime createdOn = OffsetDateTime.now();
					createdOn = createdOn.plusHours(5);
					createdOn = createdOn.plusMinutes(30);

					admUser.setIpAddress(ipAddress);
					admUser.setCreatedTime(createdOn);
					admUser.setUpdationTime(createdOn);
					admUser.setActive('1');
					admUser.setCreatedBy(request.getAttribute("email").toString());
					admUser.setPartnerEnterpriseId(Integer.parseInt(eid));
					admUser.setPartnerEnterpriseName(ename);
					LOGGER.debug("Query,for creating adm user,is going to start...");
					result = admUserService.create(admUser);
					LOGGER.debug("Query,for creating adm user,has ended...");
					if (result != 0) {
						JsonNode roles = responseEntity.getBody().getData().findPath("roles");
						for (JsonNode node : roles) {
							newRole += node.path("name").asText() + ",";
						}
						newRole = newRole.substring(0, newRole.length() - 1);

						userRole.setUserId(admUser.getUserId());
						userRole.setUserName(admUser.getUserName());
						userRole.setName(newRole);
						userRole.setActive('1');
						userRole.setStatus("Active");
						userRole.setCreatedBy(request.getAttribute("email").toString());
						userRole.setCreatedTime(admUser.getCreatedTime());
						userRole.setUpdationTime(admUser.getCreatedTime());
						userRole.setIpAddress(admUser.getIpAddress());
						LOGGER.debug("Query,for creating userRole,is going to start...");
						roleResult = admUserRoleService.create(userRole);
						LOGGER.debug("Query,for creating userRole,is going to start...");
					}
					if (!CodeUtils.isEmpty(admUser)) {
						LOGGER.debug("Query,for getting all details by user name,is going to start...");
						admUser = admUserService.getByUserName(userName);
						LOGGER.debug("Query,for getting all details by user name,has ended...");
						Date updationTime = new Date();
						updationTime = CodeUtils.convertDateOnTimeZonePluseForAWS(updationTime, 5, 30);
						Date previousUpdateTime = admUser.getImageUpdatedOn();
						if (previousUpdateTime != null) {
							diff = updationTime.getTime() - previousUpdateTime.getTime();
							diffDays = diff / (24 * 60 * 60 * 1000);
							if (diffDays >= 1) {
								duration = Long.toString(diffDays) + " Days ago";
							} else {
								diffHours = diff / (60 * 60 * 1000) % 24;
								if (diffHours >= 1) {
									duration = Long.toString(diffHours) + " Hours ago";
								} else {
									diffMinutes = diff / (60 * 1000) % 60;
									duration = Long.toString(diffMinutes) + " Minutes ago";
								}
							}
						} else {
							diff = updationTime.getTime();
							diffHours = diff / (60 * 60 * 1000) % 24;
							if (diffHours >= 1) {
								duration = Long.toString(diffHours) + " Hours ago";
							} else {
								diffMinutes = diff / (60 * 1000) % 60;
								duration = Long.toString(diffMinutes) + " Minutes ago";
							}
						}

						ObjectNode objectNode = getMapper().createObjectNode();
						ObjectNode node = getMapper().createObjectNode();
						node.put("updatedOn", duration);
						node.putPOJO("userDetails", admUser);
						objectNode.put(CodeUtils.RESPONSE, node);
						appResponse = new AppResponse.Builder()
								.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(objectNode))
								.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

					}
				} else if (responseEntity.getBody().getError().get(CodeUtils.RESPONSE_MSG) != null) {
					errMessage = responseEntity.getBody().getError().get(CodeUtils.RESPONSE_MSG).asText();
					if (errMessage != null) {
						appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE,
								AppModuleErrors.USER_READER_ERROR, errMessage);

					}
				} else {
					appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE,
							AppModuleErrors.USER_READER_ERROR, appStatus.getMessage(AppModuleErrors.USER_READER_ERROR));
				}
			}
		} catch (CannotGetJdbcConnectionException ex) {
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.USER_READER_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.USER_READER_ERROR, appStatus.getMessage(AppModuleErrors.USER_READER_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));

		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.debug(String.format("Error Occoured From Exception : %s", e));
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally", "unused", "deprecation" })
	@RequestMapping(value = "/update/profile/image", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> uploadFile(@RequestBody FileUpload fileUpload,HttpServletRequest request) {

		AppResponse appResponse = null;

		BufferedOutputStream stream;
		String file = fileUpload.getFile();
		String fileName = null;
		String filePath = null;
		File saveBucket = null;
		long diff = 0L;
		long diffDays = 0L;
		long diffHours = 0L;
		long diffMinutes = 0L;
		String duration = null;
		String extension = CodeUtils.getExtensionByApacheCommonLib(fileUpload.getFileName());

		try {
			
			String token = ((HttpServletRequest) request).getHeader("X-Auth-Token");
			JsonArray orgDetails=CodeUtils.decode(token).get("ORG_DETAILS").getAsJsonArray();
			
			JsonObject jNode=orgDetails.get(0).getAsJsonObject();
			String orgId = jNode.get("orgId").getAsString();

			S3BucketInfo s3BucketInfo = s3BucketInfoService.getBucketByType(DOWNLOAD_IMAGE,orgId);
			LOGGER.debug("Query,for getting all details by user name,is going to start...");
			ADMUser admUser = admUserService.getByUserName(fileUpload.getUserName());
			LOGGER.debug("Query,for getting all details by user name,has ended...");
			Date updationTime = new Date();
			updationTime = CodeUtils.convertDateOnTimeZonePluseForAWS(updationTime, 5, 30);
			Date previousUpdateTime = admUser.getImageUpdatedOn();
			if (previousUpdateTime != null) {
				diff = updationTime.getTime() - previousUpdateTime.getTime();
				diffDays = diff / (24 * 60 * 60 * 1000);
				if (diffDays >= 1) {
					duration = Long.toString(diffDays) + " Days ago";
				} else {
					diffHours = diff / (60 * 60 * 1000) % 24;
					if (diffHours >= 1) {
						duration = Long.toString(diffHours) + " Hours ago";
					} else {
						diffMinutes = diff / (60 * 1000) % 60;
						duration = Long.toString(diffMinutes) + " Minutes ago";
					}
				}
			} else {
				diff = updationTime.getTime();
				diffHours = diff / (60 * 60 * 1000) % 24;
				if (diffHours >= 1) {
					duration = Long.toString(diffHours) + " Hours ago";
				} else {
					diffMinutes = diff / (60 * 1000) % 60;
					duration = Long.toString(diffMinutes) + " Minutes ago";
				}
			}
			LOGGER.debug("Query,for updating all details by user name,is going to start...");
			int updateTimeByUserName = admUserService.updateTimeByUserName(fileUpload.getUserName(), updationTime);
			LOGGER.debug("Query,for updating all details by user name,has ended...");
			if (!file.isEmpty() && !CodeUtils.isEmpty(s3BucketInfo)) {

				/*
				 * String s3BucketFilePath = s3BucketInfo.getBucketPath(); fileName =
				 * s3BucketInfo.getEnterprise() + separator +
				 * CodeUtils.______dateFormat.format(new Date());
				 * 
				 * String savedPath = s3BucketFilePath + delimeter + fileName + "." + extension;
				 * filePath = FileUtils.createFilePath(FileUtils.getPlatformBasedParentDir(),
				 * savedPath);
				 * 
				 * String base64Image = file.split(",")[1];
				 * 
				 * byte[] imageBytes =
				 * javax.xml.bind.DatatypeConverter.parseBase64Binary(base64Image);
				 * 
				 * // byte[] imageBytes = file.getBytes(); stream = new BufferedOutputStream(new
				 * FileOutputStream(filePath)); stream.write(imageBytes); stream.close();
				 * saveBucket = new File(filePath); String etag = s3Wrapper.upload(saveBucket,
				 * savedPath, s3BucketInfo.getS3bucketName()).getETag();
				 */
				Map<String, String> uploadProfileImage = UploadUtils.uploadBase64Image(file, fileUpload.getFileName(),
						DOWNLOAD_IMAGE,request);
				if (!CodeUtils.isEmpty(uploadProfileImage)) {
					/*
					 * s3BucketFilePath = BucketPrefix + s3BucketInfo.getS3bucketName() + delimeter
					 * + s3BucketFilePath;
					 * 
					 * s3BucketFilePath = s3BucketFilePath.split("\\//")[1]
					 * .replaceAll(s3BucketInfo.getS3bucketName() + delimeter, "") +
					 * delimeter.trim().toString(); String URL =
					 * s3Wrapper.downloadPreSignedURLOnHistory(s3BucketInfo.getS3bucketName(),
					 * savedPath, Integer.parseInt(s3BucketInfo.getTime()));
					 */

//					int updateBucketByProfile = s3BucketInfoService.updateBucketByProfile(savedPath);
					int updateImageProfileUrl = admUserService
							.updateImageProfileUrl(uploadProfileImage.get("bucketKey"), fileUpload.getUserName());
					ObjectNode objectNode = getMapper().createObjectNode();
					ObjectNode node = getMapper().createObjectNode();
					node.put("path", uploadProfileImage.get("url"));
					node.put("updatedOn", duration);
					objectNode.put(CodeUtils.RESPONSE, node);
					appResponse = new AppResponse.Builder()
							.data(getMapper().createObjectNode()
									.put(CodeUtils.RESPONSE_MSG, "Image upload successfully!").putAll(objectNode))
							.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				}
			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE,
						AppModuleErrors.USER_CREATOR_ERROR, AppStatusMsg.FILE_NOT_UPLOADED);
			}

		} catch (ArrayIndexOutOfBoundsException e) {
			LOGGER.debug(String.format("Error Occoured From ArrayIndexOutOfBoundsException : %s", e));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE,
					AppModuleErrors.USER_CREATOR_ERROR, AppStatusMsg.FILE_NOT_UPLOADED);

		} catch (Exception e) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", e));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.USER_CREATOR_ERROR, AppStatusMsg.FILE_NOT_UPLOADED);
		} finally {
			return ResponseEntity.ok(appResponse);
		}

	}

	@SuppressWarnings("finally")
	@RequestMapping(value = "/get/url", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getUrlResponse(@RequestParam("userName") String userName,
			HttpServletRequest request) {

		AppResponse appResponse = null;
		String url = null;
		try {
			String token = ((HttpServletRequest) request).getHeader("X-Auth-Token");
			JsonArray orgDetails=CodeUtils.decode(token).get("ORG_DETAILS").getAsJsonArray();
			
			JsonObject jNode=orgDetails.get(0).getAsJsonObject();
			String orgId = jNode.get("orgId").getAsString();
			// download for profile image url link
			LOGGER.debug("Query,for getting all details by user name,is going to start...");
			ADMUser admUser = admUserService.getByUserName(userName);
			LOGGER.debug("Query,for getting all details by user name,has ended...");
			S3BucketInfo s3BucketInfo = s3BucketInfoService.getBucketByType(DOWNLOAD_IMAGE,orgId);

			if (!CodeUtils.isEmpty(admUser)) {
				url = UploadUtils.downloadHistory(admUser.getImageUrl(), DOWNLOAD_IMAGE, request);
//				String URL = s3Wrapper.downloadPreSignedURLOnHistory(s3BucketInfo.getS3bucketName(),
//						s3BucketInfo.getBucketKey(), Integer.parseInt(s3BucketInfo.getTime()));
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, url)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				url = UploadUtils.downloadHistory(s3BucketInfo.getBucketKey(), DOWNLOAD_IMAGE, request);
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, url)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				LOGGER.info(AppStatusMsg.BLANK);
			}
		} catch (CannotGetJdbcConnectionException ex) {
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.USER_READER_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.USER_READER_ERROR, appStatus.getMessage(AppModuleErrors.USER_READER_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.debug(String.format("Error Occoured From Exception : %s", e));
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally" })
	@RequestMapping(value = "test/update/profile/image", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	@ResponseBody
	public ResponseEntity<AppResponse> testuploadFile(@RequestParam("file") MultipartFile fileUpload,HttpServletRequest request) {

		AppResponse appResponse = null;

		try {
			Map<String, String> uploadFile = UploadUtils.uploadImageFile(fileUpload, DOWNLOAD_IMAGE,request);
			if (!CodeUtils.isEmpty(uploadFile)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, uploadFile.get("url"))
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE,
						AppModuleErrors.USER_CREATOR_ERROR, appStatus.getMessage(AppStatusMsg.FILE_NOT_UPLOADED));
			}

		} catch (ArrayIndexOutOfBoundsException e) {
			LOGGER.debug(String.format("Error Occoured From ArrayIndexOutOfBoundsException : %s", e));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE,
					AppModuleErrors.USER_CREATOR_ERROR, appStatus.getMessage(AppStatusMsg.FILE_NOT_UPLOADED));
		} catch (Exception e) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", e));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}

	}

}
