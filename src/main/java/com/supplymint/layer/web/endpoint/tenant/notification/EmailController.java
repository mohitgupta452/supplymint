package com.supplymint.layer.web.endpoint.tenant.notification;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.CannotGetJdbcConnectionException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.supplymint.config.application.AppCodeConfig;
import com.supplymint.exception.StatusCodes.AppErrorCodes;
import com.supplymint.exception.StatusCodes.AppModuleErrors;
import com.supplymint.exception.StatusCodes.AppStatusCodes;
import com.supplymint.exception.StatusCodes.AppStatusMsg;
import com.supplymint.exception.StatusCodes.ClientErrors;
import com.supplymint.layer.business.contract.notification.MailManagerService;
import com.supplymint.layer.data.notification.entity.DropDown;
import com.supplymint.layer.data.notification.entity.MailManager;
import com.supplymint.layer.data.notification.entity.MailManagerViewLog;
import com.supplymint.layer.web.endpoint.abstracts.AbstractEndpoint;
import com.supplymint.layer.web.model.AppResponse;
import com.supplymint.util.ApplicationUtils.Conditions;

import oracle.net.aso.o;

import com.supplymint.util.CodeUtils;

@RestController
@RequestMapping(path = EmailController.PATH, produces = { MediaType.APPLICATION_JSON_VALUE })
public class EmailController extends AbstractEndpoint {

	public static final String PATH = "notification/email";

	private static final Logger LOGGER = LoggerFactory.getLogger(EmailController.class);

	private MailManagerService mailManagerService;

	@Autowired
	private AppCodeConfig appStatus;

	@Autowired
	public EmailController(MailManagerService mailManagerService) {
		this.mailManagerService = mailManagerService;
	}

	@SuppressWarnings("finally")
	@RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getByIdentifierAndModule(@PathVariable(value = "id") String sid) {
		AppResponse appResponse = null;
		int id = 0;

		try {

			id = Integer.parseInt(sid);

			MailManager data = mailManagerService.getById(id);
			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, data)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = buildErrorResponse(AppErrorCodes.GENERIC_EVENT_NOT_AVAILABLE_ERROR_CODE,
						ClientErrors.NOT_FOUND, AppStatusMsg.BLANK);
			}
		} catch (NumberFormatException ex) {
			LOGGER.debug(String.format("Error Occoured From NumberFormatException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.NUMBER_FORMAT_MSG);

		} catch (CannotGetJdbcConnectionException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.EMAIL_CONFIG_READER_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.EMAIL_CONFIG_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.EMAIL_CONFIG_READER_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "deprecation", "finally" })
	@RequestMapping(value = "/get/all", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getAll(@RequestParam("pageNo") Integer pageNo,
			@RequestParam("type") Integer type, @RequestParam("module") String module,
			@RequestParam("subModule") String subModule, @RequestParam("configuration") String configuration,
			@RequestParam("search") String search, HttpServletRequest request) {

		AppResponse appResponse = null;
		try {

			ObjectNode objectNode = mailManagerService.getAll(pageNo, type, module, subModule, configuration, search,
					request);
			if (objectNode.get(CodeUtils.RESPONSE).toString().length() > 2) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(objectNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}
		} catch (DataAccessException ex) {
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.EMAIL_CONFIG_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.EMAIL_CONFIG_READER_ERROR));
		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings("finally")
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> createEmailConfig(@RequestBody MailManager emailConfig,
			HttpServletRequest request) {

		AppResponse appResponse = null;
		int result = 0;
		try {

			result = mailManagerService.create(emailConfig, request);
			if (result != 0) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, emailConfig)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.EMAIL_SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
						AppModuleErrors.EMAIL_CONFIG_CREATOR_ERROR,
						appStatus.getMessage(AppModuleErrors.EMAIL_CONFIG_CREATOR_ERROR));
			}
		} catch (CannotGetJdbcConnectionException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.EMAIL_CONFIG_CREATOR_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.EMAIL_CONFIG_CREATOR_ERROR,
					appStatus.getMessage(AppModuleErrors.EMAIL_CONFIG_CREATOR_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings("finally")
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> updateEmailNotification(@RequestBody MailManager emailConfig,
			HttpServletRequest request) {

		AppResponse appResponse = null;
		int result = 0;
		try {

			result = mailManagerService.update(emailConfig, request);
			if (result != 0) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, emailConfig)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
						AppModuleErrors.EMAIL_CONFIG_UPDATE_ERROR,
						appStatus.getMessage(AppModuleErrors.EMAIL_CONFIG_UPDATE_ERROR));
			}
		} catch (CannotGetJdbcConnectionException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.EMAIL_CONFIG_UPDATE_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.EMAIL_CONFIG_UPDATE_ERROR,
					appStatus.getMessage(AppModuleErrors.EMAIL_CONFIG_UPDATE_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings("finally")
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> deleteEmailConfigById(@PathVariable(value = "id") String sid) {

		AppResponse appResponse = null;
		int id = 0;
		try {

			id = Integer.parseInt(sid);

			int result = mailManagerService.delete(id);
			if (result != 0) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
						AppModuleErrors.EMAIL_CONFIG_DELETE_ERROR,
						appStatus.getMessage(AppModuleErrors.EMAIL_CONFIG_DELETE_ERROR));
				LOGGER.info(appStatus.getMessage(AppModuleErrors.EMAIL_CONFIG_DELETE_ERROR));
			}
		} catch (CannotGetJdbcConnectionException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.EMAIL_CONFIG_DELETE_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.EMAIL_CONFIG_DELETE_ERROR,
					appStatus.getMessage(AppModuleErrors.EMAIL_CONFIG_DELETE_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (NumberFormatException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.NUMBER_FORMAT_MSG);
			LOGGER.debug(String.format("Error Occoured From NumberFormatException : %s", ex));
		} catch (Exception ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings("finally")
	@RequestMapping(value = "/create/history", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> createEmailActivityLog(@RequestBody MailManagerViewLog mailManagerViewLog,
			HttpServletRequest request) {

		AppResponse appResponse = null;
		int result = 0;
		try {
			String token = ((HttpServletRequest) request).getHeader("X-Auth-Token");
			JsonArray jOrgDetail = CodeUtils.decode(token).get("ORG_DETAILS").getAsJsonArray();
			String orgId = jOrgDetail.get(0).getAsJsonObject().get("orgId").getAsString();

			result = mailManagerService.createEmailActivityLog(mailManagerViewLog, orgId);
			if (result != 0) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, mailManagerViewLog)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
						AppModuleErrors.EMAIL_CONFIG_CREATOR_ERROR,
						appStatus.getMessage(AppModuleErrors.EMAIL_CONFIG_CREATOR_ERROR));
			}
		} catch (CannotGetJdbcConnectionException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.EMAIL_CONFIG_CREATOR_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.EMAIL_CONFIG_CREATOR_ERROR,
					appStatus.getMessage(AppModuleErrors.EMAIL_CONFIG_CREATOR_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally", "rawtypes" })
	@RequestMapping(value = "/get/all/module", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getAllModule() {

		AppResponse appResponse = null;
		String moduleName = "EMAIL_GENERAL_MODULE";
		ArrayList convertJsonObject = null;
		try {

			DropDown moduleList = mailManagerService.getAllModule(moduleName);
			if (!CodeUtils.isEmpty(moduleList)) {
				convertJsonObject = getMapper().readValue(moduleList.getValue(), ArrayList.class);
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, convertJsonObject)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}
		} catch (DataAccessException ex) {
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.EMAIL_CONFIG_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.EMAIL_CONFIG_READER_ERROR));
		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally", "rawtypes" })
	@RequestMapping(value = "/get/all/subModule", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getAllSubModule(@RequestParam("moduleName") String moduleName) {

		AppResponse appResponse = null;
		DropDown subModuleList = null;
		try {
			
			subModuleList = mailManagerService.getAllSubModule(moduleName);
			if (!CodeUtils.isEmpty(subModuleList)) {
				ArrayList convertJsonObject = getMapper().readValue(subModuleList.getValue(), ArrayList.class);
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, convertJsonObject)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}
		} catch (DataAccessException ex) {
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.EMAIL_CONFIG_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.EMAIL_CONFIG_READER_ERROR));
		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally", "rawtypes" })
	@RequestMapping(value = "/get/configuration/property", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getConfigurationProperty(@RequestParam("subModuleName") String subModuleName) {

		AppResponse appResponse = null;
		DropDown getAllConfiguration = null;
		try {

			getAllConfiguration = mailManagerService.getAllConfiguration(subModuleName);
			if (!CodeUtils.isEmpty(getAllConfiguration)) {
				ArrayList convertJsonObject = getMapper().readValue(getAllConfiguration.getValue(), ArrayList.class);
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, convertJsonObject)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}
		} catch (DataAccessException ex) {
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.EMAIL_CONFIG_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.EMAIL_CONFIG_READER_ERROR));
		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally" })
	@RequestMapping(value = "/get/status", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getEmailStatus(HttpServletRequest request) {

		AppResponse appResponse = null;
		try {
			String token = ((HttpServletRequest) request).getHeader("X-Auth-Token");
			JsonArray jOrgDetail = CodeUtils.decode(token).get("ORG_DETAILS").getAsJsonArray();
			String orgId = jOrgDetail.get(0).getAsJsonObject().get("orgId").getAsString();

			List<MailManager> checkDataExistence = mailManagerService.getAllEmailData(orgId);
			ObjectNode objectNode = getMapper().createObjectNode();
			if (!CodeUtils.isEmpty(checkDataExistence)) {
				int result = mailManagerService.getEmailStatus(orgId);
				if (result != 0) {
					objectNode.put(Conditions.status.toString(), Conditions.Active.toString());
				} else {
					objectNode.put(Conditions.status.toString(), Conditions.Inactive.toString());
				}
			}

			appResponse = new AppResponse.Builder()
					.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, objectNode)
							.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
					.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

		} catch (DataAccessException ex) {
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.EMAIL_CONFIG_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.EMAIL_CONFIG_READER_ERROR));
		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings("finally")
	@RequestMapping(value = "/update/status", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> updateEmailStatus(@RequestBody JsonNode payload, HttpServletRequest request) {

		AppResponse appResponse = null;
		int result = 0;
		char active = '0';

		try {
			String token = ((HttpServletRequest) request).getHeader("X-Auth-Token");
			JsonArray jOrgDetail = CodeUtils.decode(token).get("ORG_DETAILS").getAsJsonArray();
			String orgId = jOrgDetail.get(0).getAsJsonObject().get("orgId").getAsString();

			String status = payload.get("status").asText();
			if (status.equalsIgnoreCase(Conditions.Active.toString())) {
				active = '1';
			} else {
				active = '0';
			}
			result = mailManagerService.updateEmailStatus(active, orgId);

			if (result != 0) {
				if (status.equalsIgnoreCase(Conditions.Active.toString())) {
					appResponse = new AppResponse.Builder()
							.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, "")
									.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.EMAIL_ENABLE_MSG))
							.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				} else {
					appResponse = new AppResponse.Builder()
							.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, "")
									.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.EMAIL_DISABLE_MSG))
							.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				}
			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
						AppStatusMsg.EMAIL_CONFIGURE_MSG);
			}
		} catch (CannotGetJdbcConnectionException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.EMAIL_CONFIG_UPDATE_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.EMAIL_CONFIG_UPDATE_ERROR,
					appStatus.getMessage(AppModuleErrors.EMAIL_CONFIG_UPDATE_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally", "deprecation" })
	@RequestMapping(value = "/get/configuration", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getConfiguration(@RequestParam("moduleName") String moduleName,
			@RequestParam("subModuleName") String subModuleName,
			@RequestParam("configurationProperty") String configurationProperty, HttpServletRequest request) {

		AppResponse appResponse = null;
		String[] to = null;
		List<String> temp1 = new ArrayList<String>();
		List<String> temp2 = new ArrayList<String>();
		try {
			String token = ((HttpServletRequest) request).getHeader("X-Auth-Token");
			JsonArray jOrgDetail = CodeUtils.decode(token).get("ORG_DETAILS").getAsJsonArray();
			String orgId = jOrgDetail.get(0).getAsJsonObject().get("orgId").getAsString();

			MailManager getEmailConfiguration = mailManagerService.getEmailConfiguration(moduleName, subModuleName,
					configurationProperty, orgId);

			if (!CodeUtils.isEmpty(getEmailConfiguration)) {
				if (getEmailConfiguration.getTo() != null) {
					to = getEmailConfiguration.getTo().split(",");

					List<String> toList = Arrays.asList(to);
					for (String toemail : toList) {
						String addEmailWithQuote = "" + toemail + "";
						temp1.add(addEmailWithQuote);
					}
				}
				if (getEmailConfiguration.getCc() != null) {
					String[] cc = getEmailConfiguration.getCc().split(",");
					List<String> ccList = Arrays.asList(cc);
					for (String ccemail : ccList) {
						String addEmailWithQuote = "" + ccemail + "";
						temp2.add(addEmailWithQuote);
					}
				}
			}
			ObjectNode objectNode = getMapper().createObjectNode();
			objectNode.putPOJO("to", temp1);
			objectNode.putPOJO("cc", temp2);
			ObjectNode node = getMapper().createObjectNode();
			node.put(CodeUtils.RESPONSE, objectNode);
			appResponse = new AppResponse.Builder()
					.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(node))
					.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
		} catch (DataAccessException ex) {
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.EMAIL_CONFIG_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.EMAIL_CONFIG_READER_ERROR));
		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

}
