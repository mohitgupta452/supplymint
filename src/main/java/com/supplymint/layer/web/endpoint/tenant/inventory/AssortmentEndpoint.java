package com.supplymint.layer.web.endpoint.tenant.inventory;

/**
 * @author Prabhakar Srivastava
 *
 */
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.EnumUtils;
import org.mybatis.spring.MyBatisSystemException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.dao.DataAccessException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.CannotGetJdbcConnectionException;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.supplymint.config.application.AppCodeConfig;
import com.supplymint.config.aws.utils.AWSUtils.Status;
import com.supplymint.exception.StatusCodes.AppModuleErrors;
import com.supplymint.exception.StatusCodes.AppStatusCodes;
import com.supplymint.exception.StatusCodes.AppStatusMsg;
import com.supplymint.exception.StatusCodes.ClientErrors;
import com.supplymint.exception.StatusCodes.Pagination;
import com.supplymint.layer.business.contract.demand.AssortmentService;
import com.supplymint.layer.data.config.ThreadLocalStorage;
import com.supplymint.layer.data.tenant.administration.entity.ADMItem;
import com.supplymint.layer.data.tenant.demand.entity.Assortment;
import com.supplymint.layer.data.tenant.demand.entity.AssortmentViewLog;
import com.supplymint.layer.data.tenant.demand.entity.MViewRefreshHistory;
import com.supplymint.layer.web.endpoint.abstracts.AbstractEndpoint;
import com.supplymint.layer.web.model.AppResponse;
import com.supplymint.util.ApplicationUtils;
import com.supplymint.util.ApplicationUtils.AssortmentParameter;
import com.supplymint.util.ApplicationUtils.Assortments;
import com.supplymint.util.ApplicationUtils.Conditions;
import com.supplymint.util.ApplicationUtils.CustomRunState;
import com.supplymint.util.CodeUtils;
import com.supplymint.util.JsonUtils;

@RestController
@RequestMapping(path = AssortmentEndpoint.PATH, produces = { MediaType.APPLICATION_JSON_VALUE })
public class AssortmentEndpoint extends AbstractEndpoint {

	/**
	 * This variable describes path of Assortment module
	 */
	public static final String PATH = "assortment";

	/**
	 * This variable describes Application Logger 
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(AssortmentEndpoint.class);

	/**
	 * This variable describes for AssortmentService instance 
	 */
	private transient final AssortmentService assortmentService;

	/**
	 * This constructor describes instantiating AppResponse Object
	 */
	@Autowired
	private transient AppCodeConfig appStatus;

	/**
	 * This constructor describes instantiating Assortment Service Object
	 */
	@Autowired
	public AssortmentEndpoint(final AssortmentService assortmentService) {
		super();
		this.assortmentService = assortmentService;
	}

	@SuppressWarnings({"finally" })
	@RequestMapping(value = "/get/hl1Name", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getHL1Name(@RequestParam("type") int type, @RequestParam("pageNo") int pageNo,
			@RequestParam("totalCount") int totalCount, @RequestParam("search") String search) {

		AppResponse appResponse = null;
		List<ADMItem> data = null;
		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;

		try {

			if (type == 1) {
				LOGGER.debug("retrive the distinct assortment get all");
				if (totalCount == 0) {
					totalRecord = assortmentService.getHl1Record();
				} else {
					totalRecord = totalCount * 10;
				}
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo - 1) * pageSize;

				data = assortmentService.getHL1Name(offset, pageSize);
				LOGGER.debug("obtaining the distinct assortment get all %d", data.size());
			} else if (type == 3) {
				if (!search.isEmpty()) {
					LOGGER.debug("retrive the distinct assortment search");
					if (totalCount == 0) {
						totalRecord = assortmentService.getHl1SearchRecord(search);
					} else {
						totalRecord = totalCount * 10;
					}
					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo - 1) * pageSize;
					data = assortmentService.getHl1Search(offset, pageSize, search);
					LOGGER.debug("obtaining the distinct assortment search %d", data.size());
				}
			}

			Set<String> h1Set = new HashSet<String>();
			List<String> hl1Name = null;
			if (type != 3 || !CodeUtils.isEmpty(search))
				hl1Name = data.stream().filter(e -> e != null && h1Set.add(e.gethL1Name())).map(m -> m.gethL1Name())
						.collect(Collectors.toList());
			ObjectNode objectNode = getMapper().createObjectNode();

			objectNode.putPOJO(CodeUtils.RESPONSE, hl1Name);
			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").put("currPage", pageNo)
								.put("prePage", previousPage).put("maxPage", maxPage).putAll(objectNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				LOGGER.info(AppStatusMsg.BLANK);
			}
		} catch (CannotGetJdbcConnectionException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.ASSORTMENT_READER_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.ASSORTMENT_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.ASSORTMENT_READER_ERROR));
			LOGGER.info(appStatus.getMessage(AppModuleErrors.ASSORTMENT_READER_ERROR));
		} catch (Exception ex) {
			ex.printStackTrace();
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "deprecation", "finally" })
	@RequestMapping(value = "/find/hl2Name", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> getHL2Name(@RequestBody JsonNode payload) {

		AppResponse appResponse = null;
		List<ADMItem> data = null;
		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;

		try {
			String hl1Name = payload.get("hl1Name").asText();

			int type = payload.get("type").asInt();
			int pageNo = payload.get("pageNo").asInt();
			int totalCount = payload.get("totalCount").asInt();
			String search = payload.get("search").asText();

			if (type == 1) {
				LOGGER.debug("retrive the distinct assortment get all");
				if (totalCount == 0) {
					totalRecord = assortmentService.getHl2Record(hl1Name);
				} else {
					totalRecord = totalCount * 10;
				}
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo - 1) * pageSize;

				data = assortmentService.getHL2Name(offset, pageSize, hl1Name);
				LOGGER.debug("obtaining the distinct assortment get all %d", data.size());
			} else if (type == 3) {
				if (!search.isEmpty()) {
					LOGGER.debug("retrive the distinct assortment search");
					if (totalCount == 0) {
						totalRecord = assortmentService.getHl2SearchRecord(hl1Name, search);
					} else {
						totalRecord = totalCount * 10;
					}
					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo - 1) * pageSize;
					data = assortmentService.getHl2Search(offset, pageSize, hl1Name, search);
					LOGGER.debug("obtaining the distinct assortment search %d", data.size());
				}
			}

			Set<String> h2Set = new HashSet<String>();
			List<String> hl2Name = null;
			if (type != 3 || !CodeUtils.isEmpty(search))
				hl2Name = data.stream().filter(e -> e != null && h2Set.add(e.gethL2Name())).map(m -> m.gethL2Name())
						.collect(Collectors.toList());
			ObjectNode objectNode = getMapper().createObjectNode();

			objectNode.putPOJO(CodeUtils.RESPONSE, hl2Name);
			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").put("currPage", pageNo)
								.put("prePage", previousPage).put("maxPage", maxPage).putAll(objectNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				LOGGER.info(AppStatusMsg.BLANK);
			}
		} catch (CannotGetJdbcConnectionException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.ASSORTMENT_READER_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.ASSORTMENT_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.ASSORTMENT_READER_ERROR));
			LOGGER.info(appStatus.getMessage(AppModuleErrors.ASSORTMENT_READER_ERROR));
		} catch (Exception ex) {
			ex.printStackTrace();
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "deprecation", "finally" })
	@RequestMapping(value = "/find/hl3Name", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> getHL3Name(@RequestBody JsonNode payload) {

		AppResponse appResponse = null;
		List<ADMItem> data = null;
		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;

		try {
			String hl1Name = payload.get("hl1Name").asText();
			String hl2Name = payload.get("hl2Name").asText();

			int type = payload.get("type").asInt();
			int pageNo = payload.get("pageNo").asInt();
			int totalCount = payload.get("totalCount").asInt();
			String search = payload.get("search").asText();

			if (type == 1) {
				LOGGER.debug("retrive the distinct assortment get all");
				if (totalCount == 0) {
					totalRecord = assortmentService.getHl3Record(hl1Name, hl2Name);
				} else {
					totalRecord = totalCount * 10;
				}
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo - 1) * pageSize;

				data = assortmentService.getHL3Name(offset, pageSize, hl1Name, hl2Name);
				LOGGER.debug("obtaining the distinct assortment get all %d", data.size());
			} else if (type == 3) {
				if (!search.isEmpty()) {
					LOGGER.debug("retrive the distinct assortment search");
					if (totalCount == 0) {
						totalRecord = assortmentService.getHl3SearchRecord(hl1Name, hl2Name, search);
					} else {
						totalRecord = totalCount * 10;
					}
					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo - 1) * pageSize;
					data = assortmentService.getHl3Search(offset, pageSize, hl1Name, hl2Name, search);
					LOGGER.debug("obtaining the distinct assortment search %d", data.size());
				}
			}

			Set<String> h3Set = new HashSet<String>();
			List<String> hl3Name = null;
			if (type != 3 || !CodeUtils.isEmpty(search))
				hl3Name = data.stream().filter(e -> e != null && h3Set.add(e.gethL3Name())).map(m -> m.gethL3Name())
						.collect(Collectors.toList());
			ObjectNode objectNode = getMapper().createObjectNode();

			objectNode.putPOJO(CodeUtils.RESPONSE, hl3Name);
			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").put("currPage", pageNo)
								.put("prePage", previousPage).put("maxPage", maxPage).putAll(objectNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				LOGGER.info(AppStatusMsg.BLANK);
			}
		} catch (CannotGetJdbcConnectionException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.ASSORTMENT_READER_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.ASSORTMENT_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.ASSORTMENT_READER_ERROR));
			LOGGER.info(appStatus.getMessage(AppModuleErrors.ASSORTMENT_READER_ERROR));
		} catch (Exception ex) {
			ex.printStackTrace();
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "deprecation", "finally" })
	@RequestMapping(value = "/find/hl4Name", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> getHL4Name(@RequestBody JsonNode payload) {

		AppResponse appResponse = null;
		List<ADMItem> data = null;
		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;

		try {
			String hl1Name = payload.get("hl1Name").asText();
			String hl2Name = payload.get("hl2Name").asText();
			String hl3Name = payload.get("hl3Name").asText();

			int type = payload.get("type").asInt();
			int pageNo = payload.get("pageNo").asInt();
			int totalCount = payload.get("totalCount").asInt();
			String search = payload.get("search").asText();
			
			Assortment container=new Assortment();
			container.setHl1Name(hl1Name);
			container.setHl2Name(hl2Name);
			container.setHl3Name(hl3Name);
			container.setSearch(search);

			if (type == 1) {
				LOGGER.debug("retrive the distinct assortment get all");
				if (totalCount == 0) {
					totalRecord = assortmentService.getHl4Record(container);
				} else {
					totalRecord = totalCount * 10;
				}
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo - 1) * pageSize;
				
				container.setOffset(offset);
				container.setPageSize(pageSize);

				data = assortmentService.getHL4Name(container);
				LOGGER.debug("obtaining the distinct assortment get all %d", data.size());
			} else if (type == 3) {
				if (!search.isEmpty()) {
					LOGGER.debug("retrive the distinct assortment search");
					if (totalCount == 0) {
						totalRecord = assortmentService.getHl4SearchRecord(container);
					} else {
						totalRecord = totalCount * 10;
					}
					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo - 1) * pageSize;
					
					container.setOffset(offset);
					container.setPageSize(pageSize);
					
					data = assortmentService.getHl4Search(container);
					LOGGER.debug("obtaining the distinct assortment search %d", data.size());
				}
			}
			Set<String> h4Set = new HashSet<String>();
			List<String> hl4Name = null;
			if (type != 3 || !CodeUtils.isEmpty(search))
				hl4Name = data.stream().filter(e -> e != null && h4Set.add(e.gethL4Name())).map(m -> m.gethL4Name())
						.collect(Collectors.toList());
			ObjectNode objectNode = getMapper().createObjectNode();

			objectNode.putPOJO(CodeUtils.RESPONSE, hl4Name);
			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").put("currPage", pageNo)
								.put("prePage", previousPage).put("maxPage", maxPage).putAll(objectNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				LOGGER.info(AppStatusMsg.BLANK);
			}
		} catch (CannotGetJdbcConnectionException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.ASSORTMENT_READER_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.ASSORTMENT_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.ASSORTMENT_READER_ERROR));
			LOGGER.info(appStatus.getMessage(AppModuleErrors.ASSORTMENT_READER_ERROR));
		} catch (Exception ex) {
			ex.printStackTrace();
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "deprecation", "finally", "unused" })
	@RequestMapping(value = "/find/assortmentCode", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> findAssortmentCode(@RequestBody JsonNode payload) {

		AppResponse appResponse = null;
//		int data = 0;
		int assetCount = 0;
		String code = "";
		String assortmentCode = null;
		Map<String, String> fieldMap = new LinkedHashMap<String, String>();

		try {
			String hl1Name = payload.get("hl1Name").asText();
			String hl2Name = payload.get("hl2Name").asText();
			String hl3Name = payload.get("hl3Name").asText();
			String hl4Name = payload.get("hl4Name").asText();
			String hl5Name = payload.get("hl5Name").asText();
			String hl6Name = payload.get("hl6Name").asText();

			fieldMap.put("hl1Name", hl1Name);
			fieldMap.put("hl2Name", hl2Name);
			fieldMap.put("hl3Name", hl3Name);
			fieldMap.put("hl4Name", hl4Name);
			
			Assortment container=new Assortment();
			container.setHl1Name(hl1Name);
			container.setHl2Name(hl2Name);
			container.setHl3Name(hl3Name);
			container.setHl4Name(hl4Name);
			

			int data = assortmentService.getMRP(container);
			if (!CodeUtils.isEmpty(data)) {
				assetCount = data;
			}
			ObjectNode objectNode = getMapper().createObjectNode();
			ObjectNode node = getMapper().createObjectNode();
			node.put("itemCount", assetCount);
			objectNode.put(CodeUtils.RESPONSE, node);
			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(objectNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				LOGGER.info(AppStatusMsg.BLANK);
			}
		} catch (CannotGetJdbcConnectionException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.ASSORTMENT_READER_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.ASSORTMENT_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.ASSORTMENT_READER_ERROR));
			LOGGER.info(appStatus.getMessage(AppModuleErrors.ASSORTMENT_READER_ERROR));
		} catch (Exception ex) {
			ex.printStackTrace();
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally", "deprecation", "rawtypes", "unused" })
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> createAssortment(@RequestBody JsonNode payload, ServletRequest request) {

		AppResponse appResponse = null;
		int createResult = 0;
		int deleteResult = 0;
		
		String code = "";

		String tenantHashKey = ThreadLocalStorage.getTenantHashKey();
		String token = ((HttpServletRequest) request).getHeader("X-Auth-Token");
		String userName = CodeUtils.decode(token).get("prn").getAsString();
		String enterpriseType = CodeUtils.decode(token).get("ENT_TYPE").getAsString();
		Map<String, String> fieldMap = new LinkedHashMap<String, String>();
		Map<String, String> createMap = new LinkedHashMap<String, String>();
		Map<String, String> hlMap = new LinkedHashMap<String, String>();
		Map<String, String> hierarchyMap = new LinkedHashMap<String, String>();
		Map<String, String> arguments = new HashMap<>();
		List<String> patternMap = new ArrayList<>();
		String defaultAssortment = null;
		String mainHierarchy = "";
		String subHierarchy = "";
		MViewRefreshHistory assortmentMViewRefresh = new MViewRefreshHistory();
		try {

			String hl1Name = payload.get("hl1Name").asText();
			String hl2Name = payload.get("hl2Name").asText();
			String hl3Name = payload.get("hl3Name").asText();
			String hl4Name = payload.get("hl4Name").asText();
			String hl5Name = payload.get("hl5Name").asText();
			String hl6Name = payload.get("hl6Name").asText();
			String itemName = payload.get("itemName").asText();
			String brand = payload.get("brand").asText();
			String color = payload.get("color").asText();
			String size1 = payload.get("size").asText();
			String pattern1 = payload.get("pattern").asText();
			String material = payload.get("material").asText();
			String design = payload.get("design").asText();
			String barCode = payload.get("barCode").asText();
			String sellPrice = payload.get("sell_Price").asText();
			String configureAssortment = payload.get("configureAssortment").asText();
			JsonNode assortmentInOrder = payload.get("assortmentcode");
			String newDefaultAssortment = "";
			
			Assortment container=new Assortment();
			container.setHl1Name(hl1Name);
			container.setHl2Name(hl2Name);
			container.setHl3Name(hl3Name);
			container.setHl4Name(hl4Name);
			

			fieldMap.put("itemName", itemName);
			fieldMap.put("brand", brand);
			fieldMap.put("color", color);
			fieldMap.put("size1", size1);
			fieldMap.put("pattern1", pattern1);
			fieldMap.put("material", material);
			fieldMap.put("design", design);
			fieldMap.put("barCode", barCode);
			fieldMap.put("sell_Price", sellPrice);

			createMap.put("hl1Name", hl1Name);
			createMap.put("hl2Name", hl2Name);
			createMap.put("hl3Name", hl3Name);
			createMap.put("hl4Name", hl4Name);

			if (createMap != null && !createMap.isEmpty()) {
				for (String key : createMap.keySet()) {
					if (!createMap.get(key).equals("NA")) {
						if (createMap.get(key).equals("")) {
							if (key.equalsIgnoreCase("hl1Name")) {
								hlMap.put(key, key.toUpperCase());
								hierarchyMap.put("Division", "All Division");
							} else if (key.equalsIgnoreCase("hl2Name")) {
								hlMap.put(key, key.toUpperCase());
								hierarchyMap.put("Section", "All Section");
							} else if (key.equalsIgnoreCase("hl3Name")) {
								hlMap.put(key, key.toUpperCase());
								hierarchyMap.put("Department", "All Department");
							} else if (key.equalsIgnoreCase("hl4Name")) {
								hlMap.put(key, key.toUpperCase());
								hierarchyMap.put("Article", "All Article");
							}
						} else {
							hlMap.put(key, key.toUpperCase());
							hierarchyMap.put(AssortmentParameter.valueOf(key.toUpperCase()).getDefaultAssortment(),
									createMap.get(key));
							arguments.put(key, createMap.get(key));
						}
					}
				}
			}

			if (fieldMap != null && !fieldMap.isEmpty()) {
				for (String key : fieldMap.keySet()) {
					if (fieldMap.get(key).equals("true")) {
						hlMap.put(key, key.toUpperCase());
						patternMap.add(key.toUpperCase());
					}
				}
			}
			if (CodeUtils.isEmpty(patternMap)) {
				patternMap.add("No pattern selected");
			}
			if (CodeUtils.isEmpty(hierarchyMap)) {
				hierarchyMap.put("Division,Section,Department,Article", "N/A");
			}
			// code comment due to new assormentcode in order

//			if (hlMap != null && !hlMap.isEmpty()) {
//				for (String key : hlMap.keySet()) {
//					code += hlMap.get(key) + "||'-'||";
//					subHierarchy+=hlMap.get(key)+",";
//				}
//			}

			// new assortment in order wise code start
			if (assortmentInOrder.isArray()) {
				for (final JsonNode objNode : assortmentInOrder) {
					String json = getMapper().writerWithDefaultPrettyPrinter().writeValueAsString(objNode);
					JSONObject obj = new JSONObject(json);
					Map<String, Object> convertMap = JsonUtils.toMap(obj);
					Set keys = convertMap.keySet();
					Iterator itr = keys.iterator();
					while (itr.hasNext()) {
						String key = (String) itr.next();
						if (key.toUpperCase().equalsIgnoreCase("PATTERN")) {
							key = "PATTERN1";
						} else if (key.toUpperCase().equalsIgnoreCase("SIZE")) {
							key = "SIZE1";
						}
						code += key.toUpperCase() + "||'-'||";
						subHierarchy += key.toUpperCase() + ",";
						if (EnumUtils.isValidEnum(AssortmentParameter.class, key.toUpperCase())) {
							newDefaultAssortment += AssortmentParameter.valueOf(key.toUpperCase())
									.getDefaultAssortment() + " ,";
						} else {
							newDefaultAssortment += key.toUpperCase() + " , ";
						}
					}

				}
			}

			// new assortment in order wise code end
			int length = code.length();
			if (length == 0) {
				defaultAssortment = assortmentService.getDefaultAssortment(Assortments.DEFAULT_ASSORTMENT.toString());
				String defAssortment[] = defaultAssortment.trim().split(",");
				for (int i = 0; i < defAssortment.length; i++) {
					code += defAssortment[i] + "||'-'||";
					subHierarchy += defAssortment[i] + ",";
					if (EnumUtils.isValidEnum(AssortmentParameter.class, defAssortment[i])) {
						newDefaultAssortment += AssortmentParameter.valueOf(defAssortment[i]).getDefaultAssortment()
								+ " ,";
					} else {
						newDefaultAssortment += defAssortment[i] + " ,";
					}
				}
			}
			if (!CodeUtils.isEmpty(newDefaultAssortment)) {
				newDefaultAssortment = newDefaultAssortment.substring(0, newDefaultAssortment.lastIndexOf(","));
			}
			LOGGER.info(newDefaultAssortment);
			mainHierarchy = code.substring(0, code.indexOf("||"));
			if (subHierarchy.length() - 1 >= subHierarchy.indexOf(",") + 1) {
				subHierarchy = subHierarchy.substring(subHierarchy.indexOf(",") + 1, subHierarchy.length() - 1);
			} else {
				subHierarchy = "";
			}
			String assortmentCode = code.substring(0, code.length() - 7);
			container.setAssortmentCode(assortmentCode);
			
			ObjectNode objectNode = getMapper().createObjectNode();
			ObjectNode node = getMapper().createObjectNode();
			node.put("assortmentCode", assortmentCode);
			node.put("itemCount", createResult);
			node.put("defaultAssortment", newDefaultAssortment);
			node.put("popup", configureAssortment);
			objectNode.put(CodeUtils.RESPONSE, node);

//			String hierarchyPattarn = hierarchyMap.toString();
//			String patternLog = String.join(",", patternMap);
//			String ipAddress = InetAddress.getLocalHost().getHostAddress().trim().toString();
//			if (createResult != 0) {
//				status = Status.SUCCEEDED.toString();
//			} else {
//				status = Status.FAILED.toString();
//			}
//			if (CodeUtils.isEmpty(hl1Name)) {
//				hl1Name = "All Division";
//			}
//			if (CodeUtils.isEmpty(hl2Name)) {
//				hl2Name = "All Section";
//			}
//			if (CodeUtils.isEmpty(hl3Name)) {
//				hl3Name = "All Department";
//			}
//			if (CodeUtils.isEmpty(hl4Name)) {
//				hl4Name = "All Article";
//			}
//			if (hl1Name.equals("NA") && hl2Name.equals("NA") && hl3Name.equals("NA") && hl4Name.equals("NA")) {
//				hierarchyMap.put("Division,Section,Department,Article", "N/A");
//				hl1Name = "N/A";
//				hl2Name = "N/A";
//				hl3Name = "N/A";
//				hl4Name = "N/A";
//
//			}
//			itemCount = createResult;
//			if (length == 0) {
//				patternMap.clear();
//				patternMap.add("Default pattern");
//				patternLog = String.join(",", patternMap);
//				hl1Name = "Default hierarchy";
//				hl2Name = "Default hierarchy";
//				hl3Name = "Default hierarchy";
//				hl4Name = "Default hierarchy";
//				hierarchyMap.put("Division,Section,Department,Article", "Default hierarchy");
//			}
//			String h1 = hl1Name;
//			String h2 = hl2Name;
//			String h3 = hl3Name;
//			String h4 = hl4Name;
//			String plog = patternLog;
			String forecastMainHierarchy = mainHierarchy;
			String forecastSubHierarchy = subHierarchy;
			String forecastedAssortment = newDefaultAssortment;
//			String hierarchyMaptoString = hierarchyMap.toString().substring(1, hierarchyMap.toString().length() - 1);
			// update Mview is refreshing now Active to Inactive
			if (configureAssortment.equalsIgnoreCase(Conditions.FALSE.toString())) {
				Date createdOn = CodeUtils.convertDateOnTimeZonePluseForAWS(new Date(), 5, 30);
				ExecutorService executorService = Executors.newSingleThreadExecutor();
				executorService.execute(new Runnable() {
					@Override
					public void run() {

						int deleteResult = 0;
						int createResult = 0;
						String status = null;
						String h1 = null;
						String h2 = null;
						String h3 = null;
						String h4 = null;
						String ipAddress = null;
						try {

							ThreadLocalStorage.setTenantHashKey(tenantHashKey);
							ipAddress = InetAddress.getLocalHost().getHostAddress().trim().toString();

							String refreshMView = "DEFAULT_ASSORTMENT,FINAL_ASSORTMENT,DEMAND_ACTUAL_MONTHLY,DEMAND_ACTUAL_WEEKLY";
							assortmentMViewRefresh.setModule(ApplicationUtils.Module.INVENTRY_PLANNING.toString());
							assortmentMViewRefresh.setSubModule(ApplicationUtils.SubModule.ASSORTMENT.toString());
							assortmentMViewRefresh.setUserName(userName);
							assortmentMViewRefresh.setIpAddress(ipAddress);
							assortmentMViewRefresh.setRefreshMView(refreshMView);
//						assortmentMViewRefresh.setActive('1');
//						assortmentMViewRefresh.setStatus("SUCCEEDED_VIEWED");
//						int updateMViewRefresh = assortmentService.updateMViewRefreshHistory(assortmentMViewRefresh);

							assortmentMViewRefresh.setActive('0');
							assortmentMViewRefresh.setStatus(CustomRunState.INPROGRESS.toString());
							int isExistenceMViewWithModule = assortmentService.isExistenceMViewWithModule(
									ApplicationUtils.Module.INVENTRY_PLANNING.toString(),
									ApplicationUtils.SubModule.ASSORTMENT.toString());
							if (isExistenceMViewWithModule == 0) {
								assortmentMViewRefresh.setCreatedOn(createdOn);
								LOGGER.debug("creating assortment MView LOGGER");
								int createMViewRefresh = assortmentService.createMViewRefresh(assortmentMViewRefresh);
							} else {
								int updateMViewRefresh = assortmentService
										.updateMViewRefreshHistory(assortmentMViewRefresh);
							}

							if (configureAssortment.equalsIgnoreCase(Conditions.FALSE.toString())) {
								LOGGER.debug(String.format("Delete Start Time at   : %s",
										CodeUtils._24HourTimeFormat.format(new Date())));
								deleteResult = assortmentService.deleteAssortmentCode(container);
								LOGGER.debug(String.format("Delete End Time at  : %s",
										CodeUtils._24HourTimeFormat.format(new Date())));
								LOGGER.debug(String.format("Create Start Time at : %s",
										CodeUtils._24HourTimeFormat.format(new Date())));
								createResult = assortmentService.createAssortmentCode(container);
								LOGGER.debug(String.format("Create End Time at : %s",
										CodeUtils._24HourTimeFormat.format(new Date())));
							}
							String hierarchyPattarn = hierarchyMap.toString();
							String patternLog = String.join(",", patternMap);

							if (createResult != 0) {
								status = Status.SUCCEEDED.toString();
							} else {
								status = Status.FAILED.toString();
							}
							if (CodeUtils.isEmpty(hl1Name)) {
								h1 = "All Division";
							}
							if (CodeUtils.isEmpty(hl2Name)) {
								h2 = "All Section";
							}
							if (CodeUtils.isEmpty(hl3Name)) {
								h3 = "All Department";
							}
							if (CodeUtils.isEmpty(hl4Name)) {
								h4 = "All Article";
							}
							if (hl1Name.equals("NA") && hl2Name.equals("NA") && hl3Name.equals("NA")
									&& hl4Name.equals("NA")) {
								hierarchyMap.put("Division,Section,Department,Article", "N/A");
								h1 = "N/A";
								h2 = "N/A";
								h3 = "N/A";
								h4 = "N/A";

							}
							if (length == 0) {
								patternMap.clear();
								patternMap.add("Default pattern");
								patternLog = String.join(",", patternMap);
								h1 = "Default hierarchy";
								h2 = "Default hierarchy";
								h3 = "Default hierarchy";
								h4 = "Default hierarchy";
								hierarchyMap.put("Division,Section,Department,Article", "Default hierarchy");
							}
							String plog = patternLog;
//						String forecastMainHierarchy = mainHierarchy;
//						String forecastSubHierarchy = subHierarchy;
//						String forecastedAssortment = newDefaultAssortment;
							String hierarchyMaptoString = hierarchyMap.toString().substring(1,
									hierarchyMap.toString().length() - 1);

							LOGGER.debug("creating assortment LOGGER");
//						String refreshMView = "DEFAULT_ASSORTMENT,FINAL_ASSORTMENT,DEMAND_ASSORTMENT_MONTHLY,DEMAND_ASSORTMENT_WEEKLY";
//						assortmentMViewRefresh.setModule(ApplicationUtils.Module.INVENTRY_PLANNING.toString());
//						assortmentMViewRefresh.setSubModule(ApplicationUtils.SubModule.ASSORTMENT.toString());
//						assortmentMViewRefresh.setUserName(userName);
//						assortmentMViewRefresh.setIpAddress(ipAddress);
//						assortmentMViewRefresh.setRefreshMView(refreshMView);
//						assortmentMViewRefresh.setActive('1');
//						assortmentMViewRefresh.setStatus("SUCCEEDED_VIEWED");
//						int updateMViewRefresh = assortmentService.updateMViewRefreshHistory(assortmentMViewRefresh);
							AssortmentViewLog viewLog = new AssortmentViewLog();
							viewLog.setHl1Name(h1);
							viewLog.setHl2Name(h2);
							viewLog.setHl3Name(h3);
							viewLog.setHl4Name(h4);
							viewLog.setHierarchyPattern(hierarchyMaptoString);
							viewLog.setPattern(plog);
							viewLog.setItemCount(createResult);
							viewLog.setCreatedOn(createdOn);
							viewLog.setStatus(status);
							viewLog.setIpAddress(ipAddress);
							viewLog.setAssortmentCode(forecastedAssortment);
							int createLog = assortmentService.createLog(viewLog);
							if (createResult != 0) {
//							refreshMView = "DEFAULT_ASSORTMENT,FINAL_ASSORTMENT,DEMAND_ASSORTMENT_MONTHLY,DEMAND_ASSORTMENT_WEEKLY";
//							assortmentMViewRefresh.setModule(ApplicationUtils.Module.INVENTRY_PLANNING.toString());
//							assortmentMViewRefresh.setSubModule(ApplicationUtils.SubModule.ASSORTMENT.toString());
//							assortmentMViewRefresh.setUserName(userName);
//							assortmentMViewRefresh.setIpAddress(ipAddress);
//							assortmentMViewRefresh.setRefreshMView(refreshMView);
//							assortmentMViewRefresh.setActive('0');
//							assortmentMViewRefresh.setStatus(CustomRunState.INPROGRESS.toString());
//							int isExistenceMViewWithModule = assortmentService.isExistenceMViewWithModule(
//									ApplicationUtils.Module.INVENTRY_PLANNING.toString(),
//									ApplicationUtils.SubModule.ASSORTMENT.toString());
//							if (isExistenceMViewWithModule == 0) {
//								assortmentMViewRefresh.setCreatedOn(createdOn);
//								LOGGER.debug("creating assortment MView LOGGER");
//								int createMViewRefresh = assortmentService.createMViewRefresh(assortmentMViewRefresh);
//							}
								LOGGER.debug("assortment MView LOGGER start Refreshing...");
								int updateMViewRefresh = assortmentService
										.updateMViewRefreshHistory(assortmentMViewRefresh);
								assortmentService.refreshAssortmentMV();
								assortmentMViewRefresh.setUpdatedOn(createdOn);
								assortmentMViewRefresh.setActive('1');
								assortmentMViewRefresh.setStatus(CustomRunState.SUCCEEDED.toString());
								updateMViewRefresh = assortmentService
										.updateMViewRefreshHistory(assortmentMViewRefresh);
								LOGGER.debug("assortment MView LOGGER Refreshing complete");
								if (enterpriseType.equalsIgnoreCase("BRAND")) {
									assortmentService.createMainHierarchyViewForForecast(forecastMainHierarchy,
											forecastSubHierarchy, enterpriseType);
								}
							}
						} catch (Exception ex) {
							if (createResult != 0) {
								String refreshMView = "DEFAULT_ASSORTMENT,FINAL_ASSORTMENT,DEMAND_ASSORTMENT_MONTHLY,DEMAND_ASSORTMENT_WEEKLY";
								assortmentMViewRefresh.setModule(ApplicationUtils.Module.INVENTRY_PLANNING.toString());
								assortmentMViewRefresh.setSubModule(ApplicationUtils.SubModule.ASSORTMENT.toString());
								assortmentMViewRefresh.setUserName(userName);
								assortmentMViewRefresh.setIpAddress(ipAddress);
								assortmentMViewRefresh.setRefreshMView(refreshMView);
								assortmentMViewRefresh.setActive('0');
								assortmentMViewRefresh.setStatus(CustomRunState.INPROGRESS.toString());
								int isExistenceMViewWithModule = assortmentService.isExistenceMViewWithModule(
										ApplicationUtils.Module.INVENTRY_PLANNING.toString(),
										ApplicationUtils.SubModule.ASSORTMENT.toString());
								if (isExistenceMViewWithModule == 0) {
									assortmentMViewRefresh.setCreatedOn(createdOn);
									int createMViewRefresh = assortmentService
											.createMViewRefresh(assortmentMViewRefresh);
								}
								LOGGER.debug("assortment MView LOGGER start Refreshing...");
								int updateMViewRefresh = assortmentService
										.updateMViewRefreshHistory(assortmentMViewRefresh);
								assortmentService.refreshAssortmentMV();
								assortmentMViewRefresh.setUpdatedOn(createdOn);
								assortmentMViewRefresh.setStatus(CustomRunState.SUCCEEDED.toString());
								assortmentMViewRefresh.setActive('1');
								updateMViewRefresh = assortmentService
										.updateMViewRefreshHistory(assortmentMViewRefresh);
								LOGGER.debug("assortment MView LOGGER Refreshing complete");
							}
							LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
						}
					}
				});
			}

			if (createResult == 0 || configureAssortment.equalsIgnoreCase(Conditions.TRUE.toString())) {
				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
						.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.ASSORTMENT_MSG).putAll(objectNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
						AppModuleErrors.ASSORTMENT_CREATOR_ERROR,
						appStatus.getMessage(AppModuleErrors.ASSORTMENT_CREATOR_ERROR));
			}
		} catch (CannotGetJdbcConnectionException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.ASSORTMENT_CREATOR_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.ASSORTMENT_CREATOR_ERROR,
					appStatus.getMessage(AppModuleErrors.ASSORTMENT_CREATOR_ERROR));

			LOGGER.info(appStatus.getMessage(AppModuleErrors.ASSORTMENT_CREATOR_ERROR));

		} catch (Exception ex) {
			ex.printStackTrace();
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "deprecation", "finally" })
	@RequestMapping(method = RequestMethod.GET, value = "/get/all")
	@ResponseBody
	public ResponseEntity<AppResponse> getAll(@RequestParam("pageNo") Integer pageNo,
			@RequestParam("type") Integer type, @RequestParam("search") String search) {

		AppResponse appResponse = null;
		ObjectNode node = getMapper().createObjectNode();
		ArrayNode arrNode = getMapper().createArrayNode();
		List<Assortment> data = null;

		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;

		try {
			if (type == 1) {
				totalRecord = assortmentService.assortmentRecord();

				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;

				data = assortmentService.getAllAssortment(offset, pageSize);
			} else if (type == 3) {
				if (!search.isEmpty()) {
					totalRecord = assortmentService.searchAssortmentRecord(search);
					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo - 1) * pageSize;
					data = assortmentService.searchAssortment(offset, pageSize, search);
				}
			}

			if (data.size() > 0 && !data.isEmpty()) {

				data.forEach(e -> {
					ObjectNode tempNode = getMapper().valueToTree(e);

					arrNode.add(tempNode);

				});

				node.put(CodeUtils.RESPONSE, arrNode);

				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").put("currPage", pageNo)
								.put("prePage", previousPage).put("maxPage", maxPage).putAll(node))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				LOGGER.info(AppStatusMsg.SUCCESS_MSG);

			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			}
		} catch (MyBatisSystemException ex) {
			LOGGER.debug(String.format("Error Occoured From MyBatisSystemException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CONNECTION_ERROR_CODE,
					AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, AppStatusMsg.CONNECTION_NOT_AVAILABLE);

		} catch (DataAccessException ex) {
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.ASSORTMENT_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.ASSORTMENT_READER_ERROR));

		} catch (Exception e) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", e));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {

			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "deprecation", "finally" })
	@RequestMapping(value = "/get/all/history", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getAllHistoty(@RequestParam("pageNo") Integer pageNo,
			@RequestParam("type") Integer type, @RequestParam("itemCount") String itemCount,
			@RequestParam("startDate") String sDate, @RequestParam("endDate") String eDate,
			@RequestParam("createdOn") String createdOn, @RequestParam("status") String status,
			@RequestParam("search") String search) {

		AppResponse appResponse = null;
		List<AssortmentViewLog> data = null;
		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;
		try {
			Assortment container=new Assortment();
			container.setItemCount(itemCount);
			container.setStatus(status);
			container.setSearch(search);
			container.setCreatedOn(createdOn);
			
			
			if (type == 1) {
				totalRecord = assortmentService.getAllHistotyCount();
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo - 1) * pageSize;
				container.setOffset(offset);
				container.setPageSize(pageSize);
				data = assortmentService.getAllHistoty(container);
			} else if (type == 2) {
				String startDate = sDate;
				String endDate = eDate;

				if (!CodeUtils.isEmpty(startDate) && !CodeUtils.isEmpty(endDate)) {
					startDate = CodeUtils.convertDataReferenceDateFormat(startDate);
					endDate = CodeUtils.convertDataReferenceDateFormat(endDate);
				} else if (!CodeUtils.isEmpty(endDate) || !CodeUtils.isEmpty(startDate)) {
					startDate = CodeUtils.convertDataReferenceDateFormat(startDate);
					endDate = startDate;
				}
				container.setStartDate(startDate);
				container.setEndDate(endDate);
				
				totalRecord = assortmentService.filterHistoryRecord(container);
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				
				container.setOffset(offset);
				container.setPageSize(pageSize);
				
				
				data = assortmentService.filterHistory(container);
			} else if (type == 3 && !search.isEmpty()) {
					totalRecord = assortmentService.searchHistoryRecord(container);
					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo - 1) * pageSize;
					
					container.setOffset(offset);
					container.setPageSize(pageSize);
					
					data = assortmentService.searchHistory(container);
			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
						AppStatusMsg.FAILURE_MSG);
			}

			ObjectNode objectNode = getMapper().createObjectNode();
			ArrayNode array = getMapper().createArrayNode();
			data.forEach(e -> {
				ObjectNode node = getMapper().valueToTree(e);
				array.add(node);
			});
			objectNode.put(CodeUtils.RESPONSE, array);
			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(Pagination.CURRENT_PAGE, pageNo)
								.put(Pagination.PREVIOUS_PAGE, previousPage).put(Pagination.MAXIMUM_PAGE, maxPage)
								.put(CodeUtils.RESPONSE_MSG, "").putAll(objectNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}
		} catch (DataAccessException ex) {
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.ASSORTMENT_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.ASSORTMENT_READER_ERROR));
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally", "deprecation", "unused" })
	@RequestMapping(value = "get/status", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getStatus() {

		AppResponse appResponse = null;
		String message = null;
		String runningStatus = null;

		try {
			String createStatus = assortmentService.getAssortmentStatus();
			runningStatus = assortmentService.getMVStatus();
			String forecastReadSatus = assortmentService.forecastReadSatus();
			if (forecastReadSatus.equals(Conditions.TRUE.toString())) {
				if (runningStatus != null) {
					if (runningStatus.equalsIgnoreCase(CustomRunState.INPROGRESS.toString())) {
						if (!CodeUtils.isEmpty(createStatus)) {
							if (createStatus.equals(Conditions.TRUE.toString())) {
								int updateMVForecastStatus = assortmentService.updateMVForecastStatus(
										ApplicationUtils.Module.INVENTRY_PLANNING.toString(),
										ApplicationUtils.SubModule.ASSORTMENT.toString());
							}
						}
						message = "Assortment data is loading to run forecast, please wait for some time";
					} else if (runningStatus.equalsIgnoreCase(CustomRunState.SUCCEEDED.toString())) {
						message = "Assortment data fetched successfully for Forecasting. Now you can create new Assortment.";
					} else if (runningStatus.equalsIgnoreCase("SUCCEEDED_VIEWED")) {
						message = "";
					} else {
						message = "";
					}
				} else {
					message = "";
					runningStatus = "SUCCEEDED_VIEWED";
				}
			} else {
				if (runningStatus != null) {
					if (runningStatus.equalsIgnoreCase(CustomRunState.INPROGRESS.toString())) {
						message = "New Assortment is in progress";
					} else if (runningStatus.equalsIgnoreCase(CustomRunState.SUCCEEDED.toString())) {
						message = "Assortment updated Successfully";
					} else if (runningStatus.equalsIgnoreCase("SUCCEEDED_VIEWED")) {
						message = "";
					} else {
						message = "";
					}
				} else {
					message = "";
					runningStatus = "SUCCEEDED_VIEWED";
				}
			}
			ObjectNode objectNode = getMapper().createObjectNode();
			ObjectNode node = getMapper().createObjectNode();
			objectNode.put("status", runningStatus);
			objectNode.put("message", message);
			node.put(CodeUtils.RESPONSE, objectNode);
			if (!CodeUtils.isEmpty(runningStatus)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(node))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}

		} catch (DataAccessException ex) {
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.ASSORTMENT_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.ASSORTMENT_READER_ERROR));
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}

	}

	@SuppressWarnings({ "finally" })
	@RequestMapping(value = "view/message", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> viewAssortmentCreateMessage() {

		AppResponse appResponse = null;
		try {
			int updateStatus = assortmentService.viewedAssortmentMessage();
			if (updateStatus != 0) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, updateStatus)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}

		} catch (DataAccessException ex) {
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.ASSORTMENT_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.ASSORTMENT_READER_ERROR));
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "deprecation", "finally" })
	@RequestMapping(value = "/get/all/dropdown", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getAllHlevelDropDown() {

		AppResponse appResponse = null;
		try {
			LOGGER.debug("Query,for getting all config data,is going to start...");
			ObjectNode data = assortmentService.getAllHlevelDropDown();
			LOGGER.debug("Query,for getting all config data,has ended...");
			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(data))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}
		} catch (DataAccessException ex) {
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.ASSORTMENT_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.ASSORTMENT_READER_ERROR));
		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

}
