package com.supplymint.layer.web.endpoint.tenant.administration;

import java.io.File;
import java.net.InetAddress;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.stream.Collectors;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.mybatis.spring.MyBatisSystemException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.CannotGetJdbcConnectionException;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.supplymint.config.application.AppCodeConfig;
import com.supplymint.exception.StatusCodes.AppErrorCodes;
import com.supplymint.exception.StatusCodes.AppModuleErrors;
import com.supplymint.exception.StatusCodes.AppStatusCodes;
import com.supplymint.exception.StatusCodes.AppStatusMsg;
import com.supplymint.exception.StatusCodes.ClientErrors;
import com.supplymint.layer.business.contract.administration.ADMSiteService;
import com.supplymint.layer.business.contract.procurement.PurchaseIndentService;
import com.supplymint.layer.data.tenant.administration.entity.ADMSite;
import com.supplymint.layer.data.tenant.administration.entity.ADMSiteDetail;
import com.supplymint.layer.web.endpoint.abstracts.AbstractEndpoint;
import com.supplymint.layer.web.model.AppResponse;
import com.supplymint.util.CodeUtils;
import com.supplymint.util.ExcelUtils;
import com.supplymint.util.JsonUtils;

/**
 * 
 * @author Prabhakar S,Bishnu D and Mohit G
 *
 */
@RestController
@RequestMapping(path = ADMSiteEndpoint.PATH, produces = { MediaType.APPLICATION_JSON_VALUE })
public class ADMSiteEndpoint extends AbstractEndpoint {

	public static final String PATH = "admin/site";

	@Autowired
	HttpServletRequest req;

	TransactionDefinition defaultTxn = new DefaultTransactionDefinition();

	@Autowired
	private PurchaseIndentService piService;

	@Autowired
	private AppCodeConfig appStatus;

	@Autowired
	private HttpServletRequest request;

	private ADMSiteService aDMSiteService;

	private static final Logger LOGGER = LoggerFactory.getLogger(ADMSiteEndpoint.class);

	@Autowired
	public ADMSiteEndpoint(ADMSiteService aDMSiteService) {
		this.aDMSiteService = aDMSiteService;
	}

	/**
	 * 
	 * @param sid
	 * @return all the details by required ID
	 */
	@SuppressWarnings("finally")
	@RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getByIdentifierAndModule(@PathVariable(value = "id") String sid) {
		AppResponse appResponse = null;
		ADMSite aDMSite = null;
		int id = 0;

		try {
			id = Integer.parseInt(sid);
			LOGGER.debug("Query,for getting all the site data by required ID,is going to start... ");
			aDMSite = aDMSiteService.getById(id);
			LOGGER.debug("Query,for getting all the data by required ID,has ended... ");
			if (!CodeUtils.isEmpty(aDMSite)) {
				LOGGER.info("get adm site data by id ");
				aDMSite.setContact(aDMSite.getSiteDetail().get(0).getDetailsType().equals("contact") ? null
						: aDMSite.getSiteDetail().get(0));
				aDMSite.setBilling(aDMSite.getSiteDetail().get(1).getDetailsType().equals("billing") ? null
						: aDMSite.getSiteDetail().get(1));
				aDMSite.setShipping(aDMSite.getSiteDetail().get(2).getDetailsType().equals("shipping") ? null
						: aDMSite.getSiteDetail().get(2));
				aDMSite.setReceiver(aDMSite.getSiteDetail().get(3).getDetailsType().equals("receiver") ? null
						: aDMSite.getSiteDetail().get(3));
				aDMSite.setSiteDetail(null);
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, aDMSite)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				LOGGER.info(AppStatusMsg.SUCCESS_MSG);

			} else {
				appResponse = buildErrorResponse(AppErrorCodes.GENERIC_EVENT_NOT_AVAILABLE_ERROR_CODE,
						ClientErrors.NOT_FOUND, AppStatusMsg.BLANK);
				LOGGER.info("Unable to get adm site data by id");
			}
		} catch (MyBatisSystemException ex) {
			LOGGER.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CONNECTION_ERROR_CODE,
					AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, AppStatusMsg.CONNECTION_NOT_AVAILABLE);

		} catch (IndexOutOfBoundsException | DataAccessException ex) {

			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.SITE_READER_ERROR, appStatus.getMessage(AppModuleErrors.SITE_READER_ERROR));
			LOGGER.info(ex.getMessage());
		} catch (NumberFormatException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.NUMBER_FORMAT_MSG);
			LOGGER.info(ex.getMessage());

		} catch (Exception ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.info(ex.getMessage());
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	/*
	 * 
	 * @SuppressWarnings({ "deprecation", "finally" })
	 * 
	 * @RequestMapping(value = "/get/all", method = RequestMethod.GET)
	 * 
	 * @ResponseBody public ResponseEntity<AppResponse>
	 * getAllSites(@RequestParam("pageno") Integer pageNo) {
	 * 
	 * AppResponse appResponse = null; int previousPage = 0; int pageSize = 10; int
	 * totalRecord = 0; int offset = 0; int maxPage = 0; ObjectNode objectNode;
	 * List<ADMSite> data = null;
	 * 
	 * try {
	 * 
	 * totalRecord = aDMSiteService.record(); maxPage = (totalRecord + pageSize - 1)
	 * / pageSize; previousPage = pageNo - 1; offset = (pageNo - 1) * pageSize;
	 * 
	 * data = aDMSiteService.findAll(offset, pageSize); if
	 * (!CodeUtils.isEmpty(data)) {
	 * 
	 * for (int i = 0; i < data.size(); i++) {
	 * 
	 * ADMSite site = data.get(i);
	 * 
	 * if (site.getSiteDetail() != null && !site.getSiteDetail().isEmpty()) {
	 * 
	 * site.setContact(site.getSiteDetail().get(0));
	 * site.setBilling(site.getSiteDetail().get(1));
	 * site.setShipping(site.getSiteDetail().get(2));
	 * site.setReceiver(site.getSiteDetail().get(3)); site.setSiteDetail(null);
	 * 
	 * data.set(i, site); }
	 * 
	 * }
	 * 
	 * objectNode = getMapper().createObjectNode(); ArrayNode array =
	 * getMapper().createArrayNode(); data.forEach(e -> { ObjectNode node =
	 * getMapper().valueToTree(e); array.add(node); });
	 * objectNode.put(CodeUtils.RESPONSE, array); appResponse = new
	 * AppResponse.Builder() .data(getMapper().createObjectNode().put("currPage",
	 * pageNo).put("prePage", previousPage) .put("maxPage",
	 * maxPage).put(CodeUtils.RESPONSE_MSG, "").putAll(objectNode))
	 * .status(AppStatusCodes.genericSuccessCode).build();
	 * 
	 * LOGGER.info(AppStatusMsg.SUCCESS_MSG); } else { appResponse = new
	 * AppResponse.Builder()
	 * .data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
	 * .put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
	 * .status(AppStatusCodes.genericSuccessCode).build();
	 * 
	 * LOGGER.info(AppStatusMsg.BLANK);
	 * 
	 * } } catch (IndexOutOfBoundsException ex) { appResponse =
	 * buildErrorResponse(AppStatusCodes.genericServerErrorCode,
	 * AppModuleErrors.siteDetailReaderError,
	 * appStatus.getMessage(AppModuleErrors.siteDetailReaderError));
	 * 
	 * LOGGER.info(appStatus.getMessage(AppModuleErrors.siteDetailReaderError));
	 * 
	 * } catch (DataAccessException ex) { appResponse =
	 * buildErrorResponse(AppStatusCodes.genericServerErrorCode,
	 * AppModuleErrors.siteReaderError,
	 * appStatus.getMessage(AppModuleErrors.siteReaderError));
	 * 
	 * LOGGER.info(appStatus.getMessage(AppModuleErrors.siteReaderError));
	 * 
	 * } catch (Exception ex) { appResponse =
	 * buildErrorResponse(AppStatusCodes.genericClientErrorCode,
	 * ClientErrors.badRequest, AppStatusMsg.FAILURE_MSG);
	 * 
	 * LOGGER.info(ex.getMessage());
	 * 
	 * } finally { return ResponseEntity.ok(appResponse); } }
	 * 
	 */
	/*
	 * @SuppressWarnings("finally")
	 * 
	 * @RequestMapping(value = "/create", method = RequestMethod.POST)
	 * 
	 * @ResponseBody public ResponseEntity<AppResponse> saveSite(@RequestBody
	 * JsonNode siteNode) { AppResponse appResponse = null; int siteData = 0;
	 * boolean containingData = false; int result[] = null; boolean isExist = false;
	 * final TransactionStatus ts = txManager.getTransaction(defaultTxn);
	 * TransactionSynchronizationManager.setActualTransactionActive(true); ADMSite
	 * aDMSite = null; ADMSiteDetail admSiteDetail = null;
	 * 
	 * try {
	 * 
	 * aDMSite = getMapper().treeToValue(siteNode, ADMSite.class);
	 * 
	 * 
	 * List<ADMSiteDetail> data = new ArrayList<>(); data.add(aDMSite.getContact());
	 * data.add(aDMSite.getBilling()); data.add(aDMSite.getShipping());
	 * data.add(aDMSite.getReceiver());
	 * 
	 * Iterator<ADMSiteDetail> iterator = data.iterator();
	 * 
	 * while (iterator.hasNext()) { admSiteDetail = iterator.next();
	 * List<ADMSiteDetail> validation =
	 * aDMSiteService.checkSiteExistance(admSiteDetail); if (validation.size() != 0)
	 * { containingData = true; String email = admSiteDetail.getEmail(); //String
	 * mobile = admSiteDetail.getMobile(); boolean isContainingEmail =
	 * validation.parallelStream() .allMatch(i ->
	 * i.getEmail().toLowerCase().equals(email.toLowerCase()));
	 * 
	 * if (isContainingEmail) { appResponse =
	 * buildErrorResponse(AppStatusCodes.genericClientErrorCode,
	 * ClientErrors.badRequest,
	 * admSiteDetail.getDetailsType()+" "+AppStatusMsg.EXIST_SITE_DETAIL_EMAIL); }
	 * 
	 * else { appResponse =
	 * buildErrorResponse(AppStatusCodes.genericClientErrorCode,
	 * ClientErrors.badRequest, admSiteDetail.getDetailsType()+" "+
	 * AppStatusMsg.EXIST_SITE_DETAIL_MOBILE);
	 * 
	 * }
	 * 
	 * }
	 * 
	 * }
	 * 
	 * if (!containingData) { OffsetDateTime creationTime = OffsetDateTime.now();
	 * aDMSite.setCreatedTime(creationTime); String ipAddress =
	 * InetAddress.getLocalHost().getHostAddress().trim().toString();
	 * aDMSite.setIpAddress(ipAddress); aDMSite.setActive('t');
	 * 
	 * siteData = aDMSiteService.create(aDMSite);
	 * 
	 * if (siteData != 0) { isExist = true; result = createSiteDetail(aDMSite);
	 * 
	 * if ((result[0] == 1) && (result[1] == 1) && (result[2] == 1) && (result[3] ==
	 * 1)) {
	 * 
	 * txManager.commit(ts); appResponse = new AppResponse.Builder()
	 * .data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, aDMSite)
	 * .put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
	 * .status(AppStatusCodes.genericSuccessCode).build();
	 * 
	 * } else { txManager.rollback(ts); appResponse =
	 * buildErrorResponse(AppStatusCodes.genericServerErrorCode,
	 * AppModuleErrors.siteDetailCreatorError,
	 * appStatus.getMessage(AppModuleErrors.siteDetailCreatorError)); }
	 * 
	 * } else {
	 * LOGGER.info(appStatus.getMessage(AppModuleErrors.siteDetailCreatorError));
	 * appResponse = buildErrorResponse(AppStatusCodes.genericServerErrorCode,
	 * AppModuleErrors.siteCreatorError,
	 * 
	 * appStatus.getMessage(AppModuleErrors.siteCreatorError)); }
	 * 
	 * }
	 * 
	 * }
	 * 
	 * catch (DataAccessException ex) { ex.printStackTrace();
	 * 
	 * if (isExist == false) appResponse =
	 * buildErrorResponse(AppStatusCodes.genericServerErrorCode,
	 * AppModuleErrors.siteCreatorError,
	 * appStatus.getMessage(AppModuleErrors.siteCreatorError)); else {
	 * txManager.rollback(ts); appResponse =
	 * buildErrorResponse(AppStatusCodes.genericServerErrorCode,
	 * AppModuleErrors.siteDetailCreatorError,
	 * appStatus.getMessage(AppModuleErrors.siteDetailCreatorError)); }
	 * 
	 * } catch (JsonMappingException jsonException) { appResponse =
	 * buildErrorResponse(AppStatusCodes.genericClientErrorCode,
	 * ClientErrors.badRequest, AppStatusMsg.INVALID_JSON);
	 * LOGGER.info(AppStatusMsg.INVALID_JSON);
	 * 
	 * } catch (Exception ex) { ex.printStackTrace(); if (isExist == true)
	 * txManager.rollback(ts);
	 * 
	 * appResponse = buildErrorResponse(AppStatusCodes.genericClientErrorCode,
	 * ClientErrors.badRequest, AppStatusMsg.FAILURE_MSG);
	 * 
	 * LOGGER.info(AppStatusMsg.FAILURE_MSG); } finally { return
	 * ResponseEntity.ok(appResponse); } }
	 */
	public int[] createSiteDetail(ADMSite aDMSite) {

		String[] detailsType = { "Contact", "Billing", "Shipping", "Receiver" };
		int[] result = new int[4];
		int data = 0;

		List<ADMSiteDetail> siteDetailList = new ArrayList<ADMSiteDetail>();
		siteDetailList.add(aDMSite.getContact());
		siteDetailList.add(aDMSite.getBilling());
		siteDetailList.add(aDMSite.getShipping());
		siteDetailList.add(aDMSite.getReceiver());

		for (int i = 0; i < 4; i++) {
			ADMSiteDetail billing = siteDetailList.get(i);

			billing.setSiteId(aDMSite.getId());
			billing.setDetailsType(detailsType[i]);
			billing.setStatus(aDMSite.getStatus());
			if (aDMSite.getStatus().equalsIgnoreCase("Active")) {
				billing.setActive('1');
			} else {
				billing.setActive('0');
			}
			billing.setCreatedBy("");
			billing.setCreatedTime(aDMSite.getCreatedTime());
			billing.setIpAddress(aDMSite.getIpAddress());

			data = aDMSiteService.createSiteDetail(billing);

			if (data == 1) {
				result[i] = data;
			}

		}

		return result;

	}

	/*
	 * @SuppressWarnings("finally")
	 * 
	 * @RequestMapping(value = "/update", method = RequestMethod.POST)
	 * 
	 * @ResponseBody public ResponseEntity<AppResponse> updateSite(@RequestBody
	 * JsonNode siteNode) {
	 * 
	 * AppResponse appResponse = null; int siteResult = 0; int result[] = null;
	 * ADMSite aDMSite = null; boolean isExist = false; final TransactionStatus ts =
	 * txManager.getTransaction(defaultTxn);
	 * TransactionSynchronizationManager.setActualTransactionActive(true);
	 * ADMSiteDetail admSiteDetail=null; boolean containingData = false;
	 * 
	 * try { aDMSite = getMapper().treeToValue(siteNode, ADMSite.class);
	 * 
	 * List<ADMSiteDetail> data = new ArrayList<>(); data.add(aDMSite.getContact());
	 * data.add(aDMSite.getBilling()); data.add(aDMSite.getShipping());
	 * data.add(aDMSite.getReceiver());
	 * 
	 * Iterator<ADMSiteDetail> iterator = data.iterator();
	 * 
	 * while (iterator.hasNext()) {
	 * 
	 * admSiteDetail=iterator.next(); List<ADMSiteDetail> validation =
	 * aDMSiteService.checkSiteUpdateExistance(admSiteDetail); if (validation.size()
	 * != 0) {
	 * 
	 * containingData = true; String email = admSiteDetail.getEmail(); boolean
	 * isContainingEmail = validation.parallelStream() .allMatch(i ->
	 * i.getEmail().toLowerCase().equals(email.toLowerCase()));
	 * 
	 * if (isContainingEmail) { appResponse =
	 * buildErrorResponse(AppStatusCodes.genericClientErrorCode,
	 * ClientErrors.badRequest,
	 * admSiteDetail.getDetailsType()+" "+AppStatusMsg.EXIST_SITE_DETAIL_EMAIL); }
	 * else { appResponse =
	 * buildErrorResponse(AppStatusCodes.genericClientErrorCode,
	 * ClientErrors.badRequest,
	 * admSiteDetail.getDetailsType()+" "+AppStatusMsg.EXIST_SITE_DETAIL_MOBILE); }
	 * } }
	 * 
	 * if (!containingData) { OffsetDateTime updationTime = OffsetDateTime.now();
	 * aDMSite.setUpdationTime(updationTime); String ipAddress =
	 * InetAddress.getLocalHost().getHostAddress().trim().toString();
	 * aDMSite.setIpAddress(ipAddress); siteResult = aDMSiteService.udpate(aDMSite);
	 * 
	 * if (siteResult != 0) { isExist = true; result = updateSiteDetail(aDMSite);
	 * 
	 * if ((result[0] == 1) && (result[1] == 1) && (result[2] == 1) && (result[3] ==
	 * 1)) { txManager.commit(ts); appResponse = new AppResponse.Builder()
	 * .data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, aDMSite)
	 * .put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
	 * .status(AppStatusCodes.genericSuccessCode).build();
	 * 
	 * } else { txManager.rollback(ts); appResponse =
	 * buildErrorResponse(AppErrorCodes.genericEventNotAvailableErrorCode,
	 * ClientErrors.notFound, AppStatusMsg.BLANK); }
	 * 
	 * } else { appResponse =
	 * buildErrorResponse(AppErrorCodes.genericEventNotAvailableErrorCode,
	 * ClientErrors.notFound, AppStatusMsg.BLANK); }
	 * 
	 * } } catch (DataAccessException ex) { ex.printStackTrace();
	 * 
	 * if (isExist == false) { appResponse =
	 * buildErrorResponse(AppStatusCodes.genericServerErrorCode,
	 * AppModuleErrors.siteUpdateError,
	 * appStatus.getMessage(AppModuleErrors.siteUpdateError)); } else {
	 * txManager.rollback(ts); appResponse =
	 * buildErrorResponse(AppStatusCodes.genericServerErrorCode,
	 * AppModuleErrors.siteDetailUpdateError,
	 * appStatus.getMessage(AppModuleErrors.siteDetailUpdateError)); } } catch
	 * (JsonMappingException jsonException) { jsonException.printStackTrace();
	 * appResponse = buildErrorResponse(AppStatusCodes.genericClientErrorCode,
	 * ClientErrors.badRequest, AppStatusMsg.INVALID_JSON);
	 * LOGGER.info(AppStatusMsg.INVALID_JSON);
	 * 
	 * } catch (Exception ex) { ex.printStackTrace(); if (isExist == true)
	 * txManager.rollback(ts);
	 * 
	 * appResponse = buildErrorResponse(AppStatusCodes.genericClientErrorCode,
	 * ClientErrors.badRequest, AppStatusMsg.FAILURE_MSG);
	 * LOGGER.info(ex.getMessage()); } finally { return
	 * ResponseEntity.ok(appResponse); } }
	 */
	public int[] updateSiteDetail(ADMSite aDMSite) {
		int[] result = new int[4];
		int data = 0;

		List<ADMSiteDetail> siteDetailList = new ArrayList<ADMSiteDetail>();
		siteDetailList.add(aDMSite.getContact());
		siteDetailList.add(aDMSite.getBilling());
		siteDetailList.add(aDMSite.getShipping());
		siteDetailList.add(aDMSite.getReceiver());

		for (int i = 0; i < 4; i++) {
			ADMSiteDetail billing = siteDetailList.get(i);

			billing.setAdditional("");
			billing.setUpdatedBy("");
			billing.setUpdationTime(aDMSite.getUpdationTime());
			billing.setIpAddress(aDMSite.getIpAddress());

			data = aDMSiteService.udpateSiteDetail(billing);

			if (data == 1) {
				result[i] = data;
			}

		}
		return result;

	}

	/*
	 * 
	 * @SuppressWarnings("finally")
	 * 
	 * @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	 * 
	 * @ResponseBody public ResponseEntity<AppResponse>
	 * deleteSite(@PathVariable(value = "id") String sid) {
	 * 
	 * final TransactionStatus ts = txManager.getTransaction(defaultTxn);
	 * TransactionSynchronizationManager.setActualTransactionActive(true); int
	 * siteDetailResult, siteResult = 0, siteMap = 0; AppResponse appResponse =
	 * null; int id = 0; boolean isExist = false; try { id = Integer.parseInt(sid);
	 * siteDetailResult = aDMSiteService.deleteSiteDetail(id); siteMap =
	 * 
	 * aDMSiteService.deleteSiteMap(id);
	 * 
	 * if (siteDetailResult >= 4) { isExist = true;
	 * 
	 * siteMap = aDMSiteService.deleteSiteMap(id);
	 * 
	 * if (siteMap >= 0) { siteResult = aDMSiteService.delete(id);
	 * 
	 * if (siteResult != 0) { txManager.commit(ts); appResponse = new
	 * AppResponse.Builder()
	 * .data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
	 * .put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
	 * .status(AppStatusCodes.genericSuccessCode).build();
	 * LOGGER.info(AppStatusMsg.SUCCESS_MSG);
	 * 
	 * } else { txManager.rollback(ts); appResponse =
	 * buildErrorResponse(AppErrorCodes.genericEventNotAvailableErrorCode,
	 * ClientErrors.notFound, AppStatusMsg.BLANK); LOGGER.info(AppStatusMsg.BLANK);
	 * } } else { txManager.rollback(ts); appResponse =
	 * buildErrorResponse(AppErrorCodes.genericEventNotAvailableErrorCode,
	 * ClientErrors.notFound, AppStatusMsg.BLANK); LOGGER.info(AppStatusMsg.BLANK);
	 * } } else { if (siteDetailResult >= 1 && siteDetailResult < 4)
	 * txManager.rollback(ts);
	 * 
	 * appResponse =
	 * buildErrorResponse(AppErrorCodes.genericEventNotAvailableErrorCode,
	 * ClientErrors.notFound, AppStatusMsg.BLANK); LOGGER.info(AppStatusMsg.BLANK);
	 * }
	 * 
	 * } catch (DataAccessException ex) { if (isExist) txManager.rollback(ts);
	 * 
	 * appResponse = buildErrorResponse(AppStatusCodes.genericServerErrorCode,
	 * AppModuleErrors.siteDeleteError,
	 * appStatus.getMessage(AppModuleErrors.siteDeleteError));
	 * LOGGER.info(appStatus.getMessage(AppModuleErrors.siteDeleteError)); } catch
	 * (NumberFormatException ex) { appResponse =
	 * buildErrorResponse(AppStatusCodes.genericClientErrorCode,
	 * ClientErrors.badRequest, AppStatusMsg.NUMBER_FORMAT_MSG);
	 * LOGGER.info(AppStatusMsg.NUMBER_FORMAT_MSG); } catch (Exception ex) { if
	 * (isExist) txManager.rollback(ts);
	 * 
	 * appResponse = buildErrorResponse(AppStatusCodes.genericClientErrorCode,
	 * ClientErrors.badRequest, AppStatusMsg.FAILURE_MSG);
	 * LOGGER.info(ex.getMessage()); } finally { return
	 * ResponseEntity.ok(appResponse); } }
	 * 
	 */

	/*
	 * @SuppressWarnings("finally")
	 * 
	 * @RequestMapping(value = "/sitedetail/create", method = RequestMethod.POST)
	 * 
	 * @ResponseBody public ResponseEntity<AppResponse> saveSiteDetail(@RequestBody
	 * ADMSiteDetail admSiteDetail) {
	 * 
	 * AppResponse appResponse = null; int data = 0; try { data =
	 * aDMSiteService.createSiteDetail(admSiteDetail);
	 * 
	 * if (!CodeUtils.isEmpty(data)) { appResponse = new AppResponse.Builder()
	 * .data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE,
	 * admSiteDetail) .put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
	 * .status(AppStatusCodes.genericSuccessCode).build(); } else appResponse =
	 * buildErrorResponse(AppStatusCodes.genericClientErrorCode,
	 * ClientErrors.badRequest, ClientErrorMessage.badRequest);
	 * 
	 * } catch (Exception ex) { appResponse =
	 * buildErrorResponse(AppStatusCodes.genericClientErrorCode,
	 * ClientErrors.badRequest, AppStatusMsg.FAILURE_MSG); throw new
	 * SupplyMintException(AppStatusMsg.FAILURE_MSG); } finally {
	 * LOGGER.info(appResponse.toString()); return ResponseEntity.ok(appResponse); }
	 * }
	 * 
	 * @SuppressWarnings("finally")
	 * 
	 * @RequestMapping(value = "/sitedetail/update", method = RequestMethod.POST)
	 * 
	 * @ResponseBody public ResponseEntity<AppResponse>
	 * updateSiteDetail(@RequestBody ADMSiteDetail admSiteDetail) {
	 * 
	 * AppResponse appResponse = null; int result = 0; try { result =
	 * aDMSiteService.udpateSiteDetail(admSiteDetail);
	 * 
	 * if (result != 0) { appResponse = new AppResponse.Builder()
	 * .data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE,
	 * admSiteDetail) .put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
	 * .status(AppStatusCodes.genericSuccessCode).build(); } else { appResponse =
	 * buildErrorResponse(AppStatusCodes.genericServerErrorCode,
	 * ServerErrors.internalServerError, ClientErrorMessage.badRequest); } } catch
	 * (Exception ex) { appResponse =
	 * buildErrorResponse(AppStatusCodes.genericServerErrorCode,
	 * ServerErrors.internalServerError, AppStatusMsg.FAILURE_MSG); throw new
	 * SupplyMintException(AppStatusMsg.FAILURE_MSG); } finally {
	 * LOGGER.info(appResponse.toString()); return ResponseEntity.ok(appResponse); }
	 * }
	 * 
	 * @SuppressWarnings("finally")
	 * 
	 * @RequestMapping(value = "/sitedetail/delete/{id}", method =
	 * RequestMethod.GET)
	 * 
	 * @ResponseBody public ResponseEntity<AppResponse>
	 * deleteSiteDetail(@PathVariable(value = "id") String sid) {
	 * 
	 * AppResponse appResponse = null; int id = 0; try { id = Integer.parseInt(sid);
	 * 
	 * int result = aDMSiteService.deleteSiteDetail(id);
	 * 
	 * if (result != 0) { appResponse = new AppResponse.Builder()
	 * .data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
	 * .put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
	 * .status(AppStatusCodes.genericSuccessCode).build(); } else { appResponse =
	 * buildErrorResponse(AppStatusCodes.genericClientErrorCode,
	 * ClientErrors.badRequest, ClientErrorMessage.badRequest); } } catch
	 * (NumberFormatException ex) { appResponse =
	 * buildErrorResponse(AppStatusCodes.genericClientErrorCode,
	 * ClientErrors.badRequest, AppStatusMsg.NUMBER_FORMAT_MSG); throw new
	 * SupplyMintException(AppException.NUMBER_FORMAT_EXCEPTION); } catch (Exception
	 * ex) { appResponse = buildErrorResponse(AppStatusCodes.genericClientErrorCode,
	 * ClientErrors.badRequest, AppStatusMsg.FAILURE_MSG); throw new
	 * SupplyMintException(AppStatusMsg.FAILURE_MSG); } finally {
	 * LOGGER.info(appResponse.toString()); return ResponseEntity.ok(appResponse); }
	 * }
	 *
	 * 
	 */

	@SuppressWarnings({ "deprecation", "finally" })
	@RequestMapping(value = "/get/all/siteid", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getAllSite() {

		AppResponse appResponse = null;
		ObjectNode objectNode;
		List<ADMSite> data = null;
		try {
			LOGGER.debug("Query,for getting site data,is going to start...");
			data = aDMSiteService.getAllSite();
			LOGGER.debug("Query,for getting data,has ended..");
			if (!CodeUtils.isEmpty(data)) {
				LOGGER.info("Get site list by site id");
				objectNode = getMapper().createObjectNode();
				ArrayNode array = getMapper().createArrayNode();
				data.forEach(e -> {
					// ObjectNode node = getMapper().valueToTree(e);
					ObjectNode siteNode = getMapper().createObjectNode();
					siteNode.put("id", e.getId());
					siteNode.put("name", e.getName());
					siteNode.put("displayName", e.getDisplayName());

					array.add(siteNode);
				});
				objectNode.put(CodeUtils.RESPONSE, array);

				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(objectNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {

				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

				LOGGER.info("Unable to get site list by site id");
			}
		} catch (Exception ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.info(ex.getMessage());
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	/*
	 * @SuppressWarnings({ "deprecation", "finally" })
	 * 
	 * @RequestMapping(value = "/search", method = RequestMethod.POST)
	 * 
	 * @ResponseBody public ResponseEntity<AppResponse>
	 * searchAll(@RequestParam("pageno") Integer pageNo,
	 * 
	 * @RequestParam("search") String search) {
	 * 
	 * AppResponse appResponse = null; int previousPage = 0; int pageSize = 10; int
	 * totalRecord = 0; int offset = 0; int maxPage = 0; List<ADMSite> data = null;
	 * try { totalRecord = aDMSiteService.count(search); if (totalRecord != 0) {
	 * 
	 * maxPage = (totalRecord + pageSize - 1) / pageSize; previousPage = pageNo - 1;
	 * offset = (pageNo - 1) * pageSize; data = aDMSiteService.searchAll(offset,
	 * pageSize, search); ObjectNode objectNode = getMapper().createObjectNode();
	 * ArrayNode array = getMapper().createArrayNode(); data.forEach(e -> {
	 * ObjectNode node = getMapper().valueToTree(e); array.add(node); });
	 * objectNode.put(CodeUtils.RESPONSE, array);
	 * 
	 * if (!CodeUtils.isEmpty(data)) { appResponse = new AppResponse.Builder()
	 * .data(getMapper().createObjectNode().put("currPage", pageNo).put("prePage",
	 * previousPage) .put("maxPage", maxPage).put(CodeUtils.RESPONSE_MSG,
	 * "").putAll(objectNode)) .status(AppStatusCodes.genericSuccessCode).build();
	 * LOGGER.info(AppStatusMsg.SUCCESS_MSG);
	 * 
	 * } else { appResponse = new AppResponse.Builder()
	 * .data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
	 * .put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
	 * .status(AppStatusCodes.genericSuccessCode).build();
	 * 
	 * LOGGER.info(AppStatusMsg.BLANK); } } else { appResponse = new
	 * AppResponse.Builder()
	 * .data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
	 * .put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
	 * .status(AppStatusCodes.genericSuccessCode).build();
	 * LOGGER.info(AppStatusMsg.BLANK); } } catch (DataAccessException ex) {
	 * appResponse = buildErrorResponse(AppStatusCodes.genericServerErrorCode,
	 * AppModuleErrors.siteReaderError,
	 * appStatus.getMessage(AppModuleErrors.siteReaderError));
	 * LOGGER.info(appStatus.getMessage(AppModuleErrors.siteReaderError)); } catch
	 * (Exception ex) { appResponse =
	 * buildErrorResponse(AppStatusCodes.genericClientErrorCode,
	 * ClientErrors.badRequest, AppStatusMsg.FAILURE_MSG);
	 * LOGGER.info(ex.getMessage());
	 * 
	 * } finally {
	 * 
	 * return ResponseEntity.ok(appResponse); } }
	 */

	// create clone
	@SuppressWarnings("finally")
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> saveSite(@RequestBody JsonNode siteNode) {
		AppResponse appResponse = null;
		int siteData = 0;
		boolean containingData = false;
		int result[] = null;
		boolean isExist = false;
		// final TransactionStatus ts = txManager.getTransaction(defaultTxn);
		// TransactionSynchronizationManager.setActualTransactionActive(true);
		ADMSite aDMSite = null;
		ADMSiteDetail admSiteDetail = null;

		try {

			aDMSite = getMapper().treeToValue(siteNode, ADMSite.class);

			List<ADMSiteDetail> data = new ArrayList<>();
			data.add(aDMSite.getContact());
			data.add(aDMSite.getBilling());
			data.add(aDMSite.getShipping());
			data.add(aDMSite.getReceiver());

			Iterator<ADMSiteDetail> iterator = data.iterator();

			while (iterator.hasNext()) {
				admSiteDetail = iterator.next();
				List<ADMSiteDetail> validation = aDMSiteService.checkSiteExistance(admSiteDetail);
				if (validation.size() != 0) {
					LOGGER.info("checked site existence");
					containingData = true;
					String email = admSiteDetail.getEmail();
					// String mobile = admSiteDetail.getMobile();
					boolean isContainingEmail = validation.parallelStream()
							.allMatch(i -> i.getEmail().toLowerCase().equals(email.toLowerCase()));

					if (isContainingEmail) {
						appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE,
								ClientErrors.BAD_REQUEST,
								admSiteDetail.getDetailsType() + " " + AppStatusMsg.EXIST_SITE_DETAIL_EMAIL);
					}

					else {
						appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE,
								ClientErrors.BAD_REQUEST,
								admSiteDetail.getDetailsType() + " " + AppStatusMsg.EXIST_SITE_DETAIL_MOBILE);

					}

				}

			}

			if (!containingData) {
				aDMSite.setCreatedBy("");

				OffsetDateTime creationTime = OffsetDateTime.now();
				creationTime = creationTime.plusHours(5);
				creationTime = creationTime.plusMinutes(30);
				aDMSite.setCreatedTime(creationTime);
				String ipAddress = InetAddress.getLocalHost().getHostAddress().trim().toString();
				aDMSite.setIpAddress(ipAddress);
				if (aDMSite.getStatus().equalsIgnoreCase("Active")) {
					aDMSite.setActive('1');
				} else {
					aDMSite.setActive('0');
				}
				LOGGER.debug("Query,for creating,is going to start...");
				siteData = aDMSiteService.create(aDMSite);
				LOGGER.debug("Query,for creating,has ended...");
				if (siteData != 0) {
					LOGGER.info("site created");
					isExist = true;
					result = createSiteDetail(aDMSite);

					if (result[0] == 1 && result[1] == 1 && result[2] == 1 && result[3] == 1) {
						LOGGER.info("created site details");
						// txManager.commit(ts);
						appResponse = new AppResponse.Builder()
								.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, aDMSite)
										.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SITE_SUCCESS_MSG))
								.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

					} else {
						LOGGER.info("Unable to create site detail");
						// txManager.rollback(ts);
						appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
								AppModuleErrors.SITE_DETAIL_CREATOR_ERROR,
								appStatus.getMessage(AppModuleErrors.SITE_DETAIL_CREATOR_ERROR));
					}

				} else {
					LOGGER.info("Unable to create site");
					appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
							AppModuleErrors.SITE_CREATOR_ERROR,
							appStatus.getMessage(AppModuleErrors.SITE_CREATOR_ERROR));
				}

			}

		} catch (MyBatisSystemException ex) {
			LOGGER.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CONNECTION_ERROR_CODE,
					AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, AppStatusMsg.CONNECTION_NOT_AVAILABLE);

		} catch (DataAccessException ex) {
			LOGGER.info(ex.getMessage());
			if (isExist == false)
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
						AppModuleErrors.SITE_CREATOR_ERROR, appStatus.getMessage(AppModuleErrors.SITE_CREATOR_ERROR));
			else {
				// txManager.rollback(ts);
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
						AppModuleErrors.SITE_DETAIL_CREATOR_ERROR,
						appStatus.getMessage(AppModuleErrors.SITE_DETAIL_CREATOR_ERROR));
			}

		} catch (JsonMappingException jsonException) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.INVALID_JSON);
			LOGGER.info(jsonException.getMessage());

		} catch (Exception ex) {
			LOGGER.info(ex.getMessage());
			if (isExist == true)
				// txManager.rollback(ts);

				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
						AppStatusMsg.FAILURE_MSG);

		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	// update clone

	@SuppressWarnings("finally")
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> updateSite(@RequestBody JsonNode siteNode) {

		AppResponse appResponse = null;
		int siteResult = 0;
		int result[] = null;
		ADMSite aDMSite = null;
		boolean isExist = false;
//		final TransactionStatus ts = txManager.getTransaction(defaultTxn);
//		TransactionSynchronizationManager.setActualTransactionActive(true);
		ADMSiteDetail admSiteDetail = null;
		boolean containingData = false;

		try {
			aDMSite = getMapper().treeToValue(siteNode, ADMSite.class);

			List<ADMSiteDetail> data = new ArrayList<>();
			data.add(aDMSite.getContact());
			data.add(aDMSite.getBilling());
			data.add(aDMSite.getShipping());
			data.add(aDMSite.getReceiver());

			Iterator<ADMSiteDetail> iterator = data.iterator();

			while (iterator.hasNext()) {

				admSiteDetail = iterator.next();
				LOGGER.debug("Query,for validating details,is going to satrt...");
				List<ADMSiteDetail> validation = aDMSiteService.checkSiteUpdateExistance(admSiteDetail);
				LOGGER.debug("Query,for validating details,has ended...");
				if (validation.size() != 0) {

					containingData = true;
					String email = admSiteDetail.getEmail();
					boolean isContainingEmail = validation.parallelStream()
							.allMatch(i -> i.getEmail().toLowerCase().equals(email.toLowerCase()));

					if (isContainingEmail) {
						appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE,
								ClientErrors.BAD_REQUEST,
								admSiteDetail.getDetailsType() + " " + AppStatusMsg.EXIST_SITE_DETAIL_EMAIL);
					} else {
						appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE,
								ClientErrors.BAD_REQUEST,
								admSiteDetail.getDetailsType() + " " + AppStatusMsg.EXIST_SITE_DETAIL_MOBILE);
					}
				}
			}

			if (!containingData) {
//				String userName = (String)session.getAttribute("username");
				aDMSite.setUpdatedBy("");
//				aDMSite.setUpdatedBy(userName);
				OffsetDateTime updationTime = OffsetDateTime.now();
				updationTime = updationTime.plusHours(5);
				updationTime = updationTime.plusMinutes(30);
				aDMSite.setUpdationTime(updationTime);
				String ipAddress = InetAddress.getLocalHost().getHostAddress().trim().toString();
				aDMSite.setIpAddress(ipAddress);
				LOGGER.debug("Query,for updating data, is going to start...");
				siteResult = aDMSiteService.udpate(aDMSite);
				LOGGER.debug("Query,for updating data, has ended...");
				if (siteResult != 0) {
					LOGGER.info("Updated site");
					isExist = true;
					LOGGER.debug("Query,for getting data,is going to start...");
					result = updateSiteDetail(aDMSite);
					LOGGER.debug("Query,for getting data,has ended..");
					if (result[0] == 1 && result[1] == 1 && result[2] == 1 && result[3] == 1) {
//						txManager.commit(ts);
						LOGGER.info("Updated site detail");
						appResponse = new AppResponse.Builder()
								.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, aDMSite)
										.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
								.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

					} else {
						LOGGER.info("Unable to update site detail");
//						txManager.rollback(ts);
						appResponse = buildErrorResponse(AppErrorCodes.GENERIC_EVENT_NOT_AVAILABLE_ERROR_CODE,
								ClientErrors.NOT_FOUND, AppStatusMsg.BLANK);
					}

				} else {
					LOGGER.info("Unable to update site detail");
					appResponse = buildErrorResponse(AppErrorCodes.GENERIC_EVENT_NOT_AVAILABLE_ERROR_CODE,
							ClientErrors.NOT_FOUND, AppStatusMsg.BLANK);
				}

			}
		} catch (MyBatisSystemException ex) {
			LOGGER.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CONNECTION_ERROR_CODE,
					AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, AppStatusMsg.CONNECTION_NOT_AVAILABLE);

		} catch (DataAccessException ex) {
			LOGGER.info(ex.getMessage());

			if (isExist == false) {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
						AppModuleErrors.SITE_UPDATE_ERROR, appStatus.getMessage(AppModuleErrors.SITE_UPDATE_ERROR));
			} else {
//				txManager.rollback(ts);
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
						AppModuleErrors.SITE_DETAIL_UPDATE_ERROR,
						appStatus.getMessage(AppModuleErrors.SITE_DETAIL_UPDATE_ERROR));
			}
		} catch (JsonMappingException jsonException) {
			LOGGER.info(jsonException.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.INVALID_JSON);
			LOGGER.info(AppStatusMsg.INVALID_JSON);

		} catch (Exception ex) {
			LOGGER.info(ex.getMessage());
			if (isExist == true)
//				txManager.rollback(ts);

				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
						AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	// clone delete

	@SuppressWarnings("finally")
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> deleteSite(@PathVariable(value = "id") String sid) {

//		final TransactionStatus ts = txManager.getTransaction(defaultTxn);
//		TransactionSynchronizationManager.setActualTransactionActive(true);
		int siteDetailResult = 0;
		int siteResult = 0;
		int siteMap = 0;
		AppResponse appResponse = null;
		int id = 0;
		boolean isExist = false;
		try {
			id = Integer.parseInt(sid);
			LOGGER.debug("Query,for deleting data,is going to start...");
			siteDetailResult = aDMSiteService.deleteSiteDetail(id);
			LOGGER.debug("Query,for deleting data,has ended...");

			LOGGER.debug("Query,for deleting data,is going to start...");
			siteMap = aDMSiteService.deleteSiteMap(id);
			LOGGER.debug("Query,for deleting data,has ended...");

			if (siteDetailResult >= 4) {
				LOGGER.info("deleted site detail");
				isExist = true;
				LOGGER.debug("Query,for deleting data,is going to start...");
				siteMap = aDMSiteService.deleteSiteMap(id);
				LOGGER.debug("Query,for deleting data,has ended...");
				if (siteMap >= 0) {
					LOGGER.info("deleted site map");
					siteResult = aDMSiteService.delete(id);

					if (siteResult != 0) {
						LOGGER.info("deleted site");
//						txManager.commit(ts);
						appResponse = new AppResponse.Builder()
								.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
										.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
								.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
						LOGGER.info(AppStatusMsg.SUCCESS_MSG);

					} else {
						LOGGER.info("Unable to delete site");

//						txManager.rollback(ts);
						appResponse = buildErrorResponse(AppErrorCodes.GENERIC_EVENT_NOT_AVAILABLE_ERROR_CODE,
								ClientErrors.NOT_FOUND, AppStatusMsg.BLANK);
					}
				} else {
//					txManager.rollback(ts);
					appResponse = buildErrorResponse(AppErrorCodes.GENERIC_EVENT_NOT_AVAILABLE_ERROR_CODE,
							ClientErrors.NOT_FOUND, AppStatusMsg.BLANK);
					LOGGER.info("Unable to delete site map");
				}
			} else {
				if (siteDetailResult >= 1 && siteDetailResult < 4)
//					txManager.rollback(ts);

					appResponse = buildErrorResponse(AppErrorCodes.GENERIC_EVENT_NOT_AVAILABLE_ERROR_CODE,
							ClientErrors.NOT_FOUND, AppStatusMsg.BLANK);
				LOGGER.info("Unable to delete site detail");
			}

		} catch (MyBatisSystemException ex) {
			LOGGER.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CONNECTION_ERROR_CODE,
					AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, AppStatusMsg.CONNECTION_NOT_AVAILABLE);

		} catch (DataAccessException ex) {
			if (isExist)
//				txManager.rollback(ts);

				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
						AppModuleErrors.SITE_DELETE_ERROR, appStatus.getMessage(AppModuleErrors.SITE_DELETE_ERROR));
			LOGGER.info(ex.getMessage());
		} catch (NumberFormatException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.NUMBER_FORMAT_MSG);
			LOGGER.info(ex.getMessage());
		} catch (Exception ex) {
			if (isExist)
//				txManager.rollback(ts);

				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
						AppStatusMsg.FAILURE_MSG);

			LOGGER.info(ex.getMessage());
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	// get all clone

	@SuppressWarnings({ "finally" })
	@RequestMapping(value = "/get/all", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getAllSites(@RequestParam("pageno") Integer pageNo,
			@RequestParam("type") Integer type, @RequestParam("search") String search,
			@RequestParam("name") String name, @RequestParam("code") String code,
			@RequestParam("displayName") String displayName, @RequestParam("country") String country,
			@RequestParam("state") String state, @RequestParam("zipCode") String zipCode,
			@RequestParam("status") String status) {

		AppResponse appResponse = null;
		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;
		ObjectNode objectNode;
		List<ADMSite> data = null;
		ADMSite siteData = null;
		int flag = 0;

		try {

			flag = type;

			if (flag == 1) {
				LOGGER.info("counting site record");
				totalRecord = aDMSiteService.record();
				LOGGER.debug("Query,for getting record,has ended..");
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo - 1) * pageSize;
				LOGGER.debug("Query,for getting data,is going to start...");
				data = aDMSiteService.findAll(offset, pageSize);
				LOGGER.debug("Query,for getting data,has ended..");
				if (!CodeUtils.isEmpty(data)) {

					data = convertSiteJson(data);
					objectNode = buildJson(data);
					appResponse = successResponse(pageNo, previousPage, maxPage, objectNode);
					LOGGER.info("Get all site ");
				} else {
					appResponse = blankResponse();
					LOGGER.info("Unable to get all site");
				}
			} else if (flag == 2) {
				siteData = new ADMSite();
				siteData.setName(name);
				siteData.setCode(code);
				siteData.setDisplayName(displayName);
				siteData.setCountry(country);
				siteData.setState(state);
				siteData.setZipCode(zipCode);
				siteData.setStatus(status);
				LOGGER.info("Counting site record through filter");
				totalRecord = aDMSiteService.filterRecord(siteData);
				LOGGER.debug("Query,for filtering all record,has ended...");
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
				LOGGER.debug("Query,for getting all filter record,is going to start...");
				data = aDMSiteService.filterSite(offset, pageSize, siteData);
				LOGGER.debug("Query,for getting all filter record has ended...");
				if (!CodeUtils.isEmpty(data)) {
					LOGGER.info("get site data through filter");
					data = convertSiteJson(data);

					objectNode = buildJson(data);
					appResponse = successResponse(pageNo, previousPage, maxPage, objectNode);

				} else {
					appResponse = blankResponse();
					LOGGER.info("Unable to get site data through filter");
				}
			} else {
				if (flag == 3 && !search.isEmpty()) {
					LOGGER.debug("Query,for searching,is going to start...");
					totalRecord = aDMSiteService.searchRecord(search);
					LOGGER.debug("Query,for searching,has ended...");
					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo - 1) * pageSize;
					LOGGER.debug("Query,for getting all searched record,is going to start...");
					data = aDMSiteService.searchAll(offset, pageSize, search);
					LOGGER.debug("Query,for getting all searched record,has ended...");
					if (!CodeUtils.isEmpty(data)) {
						LOGGER.info("Searched site data");
						data = convertSiteJson(data);

						objectNode = buildJson(data);
						appResponse = successResponse(pageNo, previousPage, maxPage, objectNode);

					} else {
						LOGGER.info("Unable to search site data");
						appResponse = blankResponse();
					}
				} else {
					appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
							AppStatusMsg.FAILURE_MSG);
				}
			}

		} catch (IndexOutOfBoundsException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.SITE_DETAIL_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.SITE_DETAIL_READER_ERROR));

			LOGGER.info(ex.getMessage());
		} catch (MyBatisSystemException ex) {
			LOGGER.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CONNECTION_ERROR_CODE,
					AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, AppStatusMsg.CONNECTION_NOT_AVAILABLE);

		} catch (DataAccessException ex) {
			ex.printStackTrace();
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.SITE_READER_ERROR, appStatus.getMessage(AppModuleErrors.SITE_READER_ERROR));

			LOGGER.info(ex.getMessage());
		} catch (Exception ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);

			LOGGER.info(ex.getMessage());
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	AppResponse notFoundResponse() {
		AppResponse appResponse = buildErrorResponse(AppErrorCodes.GENERIC_EVENT_NOT_AVAILABLE_ERROR_CODE,
				ClientErrors.NOT_FOUND, AppStatusMsg.BLANK);
		return appResponse;
	}

	@SuppressWarnings("deprecation")
	ObjectNode buildJson(List<ADMSite> data) {
		ObjectNode objectNode = getMapper().createObjectNode();
		ArrayNode array = getMapper().createArrayNode();
		data.forEach(e -> {
			ObjectNode node = getMapper().valueToTree(e);
			array.add(node);
		});
		objectNode.put(CodeUtils.RESPONSE, array);

		return objectNode;
	}

	public List<ADMSite> searchWord(String str, List<ADMSite> data) {
		List<ADMSite> record = null;
		record = data.stream().filter(site -> (site.getName() != null ? site.getName().toLowerCase().contains(str)
				: false) || (site.getCode() != null ? site.getCode().toLowerCase().contains(str) : false)
				|| (site.getDisplayName() != null ? site.getDisplayName().toLowerCase().contains(str) : false)
				|| (site.getCountry() != null ? site.getCountry().toLowerCase().contains(str) : false)
				|| (site.getState() != null ? site.getState().toLowerCase().contains(str) : false)
				|| (site.getCity() != null ? site.getCity().toLowerCase().contains(str) : false)
				|| (site.getZipCode() != null ? site.getZipCode().toLowerCase().contains(str) : false)
				|| (site.getLatitude() != null ? site.getLatitude().toLowerCase().contains(str) : false)
				|| (site.getLongitude() != null ? site.getLongitude().toLowerCase().contains(str) : false)
				|| (site.getType() != null ? site.getType().toLowerCase().contains(str) : false)
				|| (site.getArea() != null ? site.getArea().toLowerCase().contains(str) : false)
				|| (site.getAreaUnit() != null ? site.getAreaUnit().toLowerCase().contains(str) : false)
				|| (site.getNumberFloors() != null ? site.getNumberFloors().toLowerCase().contains(str) : false))
				.collect(Collectors.toList());

		return record;
	}

	public List<ADMSite> searchAll(Integer pageNo, String search) {

		int i = 0;
		List<ADMSite> data = null;
		List<ADMSite> records = null;
		data = aDMSiteService.getAllRecords();

		if (!CodeUtils.isEmpty(data)) {
			String searchArr[] = search.trim().split(" ");
			records = new ArrayList<ADMSite>();
			for (i = 0; i < searchArr.length; i++) {
				List<ADMSite> record = null;
				record = searchWord(searchArr[i].toLowerCase(), data);
				if (!CodeUtils.isEmpty(record))
					records.addAll(record);

			}

			if (!CodeUtils.isEmpty(records))
				return records;

		} else {
			return data;
		}

		return null;
	}

	List<ADMSite> convertSiteJson(List<ADMSite> data) {

		for (int i = 0; i < data.size(); i++) {
			ADMSite site = data.get(i);

			if (site.getSiteDetail() != null && !site.getSiteDetail().isEmpty()) {
				site.setContact(site.getSiteDetail().get(0));
				site.setBilling(site.getSiteDetail().get(1));
				site.setShipping(site.getSiteDetail().get(2));
				site.setReceiver(site.getSiteDetail().get(3));
				site.setSiteDetail(null);
				data.set(i, site);
			}
		}
		return data;
	}

	@SuppressWarnings({ "finally", "unchecked", "unused", "rawtypes", "deprecation" })
	@RequestMapping(value = "/upload/site/data", method = RequestMethod.POST, consumes = {
			MediaType.MULTIPART_FORM_DATA_VALUE })
	@ResponseBody
	public ResponseEntity<AppResponse> uploadSiteData(@RequestParam("file") MultipartFile file,
			ServletRequest request) {

		AppResponse appResponse = null;
		int result = 0;
		Vector<Row> readDataFromFromExcel = null;
		String name, code, country, state, city, zipCode, timeZone, displayName, latitude, longitude, status, type,
				area, areaUnit, numberFloors, optStoreSDate, optStoreEDate, isOnlineStore, isShipping, isBilling,
				contactName, contactdetailsType, contactphone, contactEmail, contactMobile, contactWebsite,
				contactAddress, billingName, billingdetailsType, billingphone, billingEmail, billingMobile,
				billingWebsite, billingAddress, shippingName, shippingdetailsType, shippingphone, shippingEmail,
				shippingMobile, shippingWebsite, shippingAddress, receiverName, receiverdetailsType, receiverphone,
				receiverEmail, receiverMobile, receiverWebsite, receiverAddress;

		try {
			ADMSite admSite = new ADMSite();
			List contact = new ArrayList<>();
			List billing = new ArrayList<>();
			List shipping = new ArrayList<>();
			List receiver = new ArrayList<>();
			int[] sidetailsResult = null;
			File tmpFile = new File(System.getProperty("java.io.tmpdir") + System.getProperty("file.separator")
					+ file.getOriginalFilename());
			file.transferTo(tmpFile);
			String fileName = tmpFile.getPath();
			LOGGER.info(fileName);
			Vector data = ExcelUtils.readExcel(fileName);
			ADMSiteDetail admSiteDetail = new ADMSiteDetail();
			if (!CodeUtils.isEmpty(data)) {
				for (int i = 0; i < data.size(); i++) {
					Vector cellStoreVector = (Vector) data.elementAt(i);
					for (int j = 0; j < cellStoreVector.size(); j++) {
						XSSFCell myCell = (XSSFCell) cellStoreVector.elementAt(j);
						if (j == 0) {
							name = myCell.toString();
							admSite.setName(name);
						} else if (j == 1) {
							code = myCell.toString();
							admSite.setCode(code);
						} else if (j == 2) {
							country = myCell.toString();
							admSite.setCountry(country);
						} else if (j == 3) {
							state = myCell.toString();
							admSite.setState(state);
						} else if (j == 4) {
							city = myCell.toString();
							admSite.setCity(city);
						} else if (j == 5) {
							zipCode = myCell.toString();
							admSite.setZipCode(zipCode);
						} else if (j == 6) {
							timeZone = myCell.toString();
							admSite.setTimeZone(timeZone);
						} else if (j == 7) {
							displayName = myCell.toString();
							admSite.setDisplayName(displayName);
						} else if (j == 8) {
							status = myCell.toString();
							admSite.setStatus(status);
						} else if (j == 9) {
							type = myCell.toString();
							admSite.setType(type);
						} else if (j == 10) {
							latitude = myCell.toString();
							admSite.setLatitude(latitude);
						} else if (j == 11) {
							longitude = myCell.toString();
							admSite.setLongitude(longitude);
						} else if (j == 12) {
							area = myCell.toString();
							admSite.setArea(area);
						} else if (j == 13) {
							contactAddress = myCell.toString();
							contact.add(contactAddress);
							admSiteDetail.setAddress(contactAddress);
							admSite.setContact(admSiteDetail);
						} else if (j == 14) {
							numberFloors = myCell.toString();
							admSite.setNumberFloors(numberFloors);
						} else if (j == 15) {
							optStoreSDate = myCell.toString();
							if (!CodeUtils.isEmpty(optStoreSDate)) {
								admSite.setOptStoreSDate(new Date(optStoreSDate));
							} else {
								admSite.setOptStoreSDate(null);
							}
						} else if (j == 16) {
							optStoreEDate = myCell.toString();
							if (!CodeUtils.isEmpty(optStoreEDate)) {
								admSite.setOptStoreEDate(new Date(optStoreEDate));
							} else {
								admSite.setOptStoreEDate(null);
							}
						} else if (j == 17) {
							isOnlineStore = myCell.toString();
							admSite.setIsOnlineStore(isOnlineStore);
						} else if (j == 18) {
							isShipping = myCell.toString();
							admSite.setIsShipping(isShipping);
						} else if (j == 19) {
							isBilling = myCell.toString();
							admSite.setIsBilling(isBilling);
						} else if (j == 20) {
							contactName = myCell.toString();
							contact.add(contactName);
							admSiteDetail.setName(contactName);
						} else if (j == 21) {
							contactdetailsType = myCell.toString();
							contact.add(contactdetailsType);
							admSiteDetail.setDetailsType(contactdetailsType);
						} else if (j == 22) {
							contactphone = myCell.toString();
							contact.add(contactphone);
							admSiteDetail.setPhone(contactphone);
						} else if (j == 23) {
							contactEmail = myCell.toString();
							contact.add(contactEmail);
							admSiteDetail.setEmail(contactEmail);
						} else if (j == 24) {
							contactMobile = myCell.toString();
							contact.add(contactMobile);
							admSiteDetail.setMobile(contactMobile);
						} else if (j == 25) {
							contactWebsite = myCell.toString();
							contact.add(contactWebsite);
							admSiteDetail.setWebsite(contactWebsite);
						} else if (j == 26) {
							contactAddress = myCell.toString();
							contact.add(contactAddress);
							admSiteDetail.setAddress(contactAddress);
							admSite.setContact(admSiteDetail);
						} else if (j == 27) {
							billingName = myCell.toString();
							billing.add(billingName);
							admSiteDetail.setName(billingName);
						} else if (j == 28) {
							billingdetailsType = myCell.toString();
							billing.add(billingdetailsType);
							admSiteDetail.setDetailsType(billingdetailsType);
						} else if (j == 29) {
							billingphone = myCell.toString();
							billing.add(billingphone);
							admSiteDetail.setPhone(billingphone);
						} else if (j == 30) {
							billingEmail = myCell.toString();
							billing.add(billingEmail);
							admSiteDetail.setEmail(billingEmail);
						} else if (j == 31) {
							billingMobile = myCell.toString();
							billing.add(billingMobile);
							admSiteDetail.setMobile(billingMobile);
						} else if (j == 32) {
							billingWebsite = myCell.toString();
							billing.add(billingWebsite);
							admSiteDetail.setWebsite(billingWebsite);
						} else if (j == 33) {
							billingAddress = myCell.toString();
							billing.add(billingAddress);
							admSiteDetail.setAddress(billingAddress);
							admSite.setBilling(admSiteDetail);
						} else if (j == 34) {
							shippingName = myCell.toString();
							shipping.add(shippingName);
							admSiteDetail.setName(shippingName);
						} else if (j == 35) {
							shippingdetailsType = myCell.toString();
							shipping.add(shippingdetailsType);
							admSiteDetail.setDetailsType(shippingdetailsType);
						} else if (j == 36) {
							shippingphone = myCell.toString();
							shipping.add(shippingphone);
							admSiteDetail.setPhone(shippingphone);
						} else if (j == 37) {
							shippingEmail = myCell.toString();
							shipping.add(shippingEmail);
							admSiteDetail.setEmail(shippingEmail);
						} else if (j == 38) {
							shippingMobile = myCell.toString();
							shipping.add(shippingMobile);
							admSiteDetail.setMobile(shippingMobile);
						} else if (j == 39) {
							shippingWebsite = myCell.toString();
							shipping.add(shippingWebsite);
							admSiteDetail.setWebsite(shippingWebsite);
						} else if (j == 40) {
							shippingAddress = myCell.toString();
							shipping.add(shippingAddress);
							admSiteDetail.setAddress(shippingAddress);
							admSite.setShipping(admSiteDetail);
						} else if (j == 41) {
							receiverName = myCell.toString();
							receiver.add(receiverName);
							admSiteDetail.setName(receiverName);
						} else if (j == 42) {
							receiverdetailsType = myCell.toString();
							receiver.add(receiverdetailsType);
							admSiteDetail.setDetailsType(receiverdetailsType);
						} else if (j == 43) {
							receiverphone = myCell.toString();
							receiver.add(receiverphone);
							admSiteDetail.setPhone(receiverphone);
						} else if (j == 44) {
							receiverEmail = myCell.toString();
							receiver.add(receiverEmail);
							admSiteDetail.setEmail(receiverEmail);
						} else if (j == 45) {
							receiverMobile = myCell.toString();
							receiver.add(receiverMobile);
							admSiteDetail.setMobile(receiverMobile);
						} else if (j == 46) {
							receiverWebsite = myCell.toString();
							receiver.add(receiverWebsite);
							admSiteDetail.setWebsite(receiverWebsite);
						} else if (j == 47) {
							receiverAddress = myCell.toString();
							receiver.add(receiverAddress);
							admSiteDetail.setAddress(receiverAddress);
							admSite.setReceiver(admSiteDetail);
						}
					}
					OffsetDateTime creationTime = OffsetDateTime.now();
					creationTime = creationTime.plusHours(5);
					creationTime = creationTime.plusMinutes(30);
					admSite.setCreatedTime(creationTime);
					String ipAddress = InetAddress.getLocalHost().getHostAddress().trim().toString();
					admSite.setIpAddress(ipAddress);
					if (admSite.getStatus().equalsIgnoreCase("Active")) {
						admSite.setActive('1');
					} else {
						admSite.setActive('0');
					}

					result = aDMSiteService.create(admSite);
				}
				if (result != 0) {
					appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
							.put(CodeUtils.RESPONSE, "").put(CodeUtils.RESPONSE_MSG, AppStatusMsg.FILE_UPLOADED))
							.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

				} else {
					appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
							AppStatusMsg.FAILURE_MSG);
				}
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				LOGGER.info(AppStatusMsg.BLANK);
			}
		}

		catch (CannotGetJdbcConnectionException ex) {
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.SITE_CREATOR_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.SITE_CREATOR_ERROR, appStatus.getMessage(AppModuleErrors.SITE_CREATOR_ERROR));

		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally", "unchecked", "unused", "rawtypes", "deprecation" })
	@RequestMapping(value = "/upload/data", method = RequestMethod.POST, consumes = {
			MediaType.MULTIPART_FORM_DATA_VALUE })
	@ResponseBody
	public ResponseEntity<AppResponse> uploadSiteTemplateData(@RequestParam("file") MultipartFile file,
			ServletRequest request) {

		AppResponse appResponse = null;
		Vector<Row> readDataFromFromExcel = null;
		String name, code, country, state, city, zipCode, timeZone, displayName, latitude, longitude, status, type,
				area, areaUnit, numberFloors, optStoreSDate, optStoreEDate, isOnlineStore, isShipping, isBilling,
				contactName, contactdetailsType, contactphone, contactEmail, contactMobile, contactWebsite,
				contactAddress, billingName, billingdetailsType, billingphone, billingEmail, billingMobile,
				billingWebsite, billingAddress, shippingName, shippingdetailsType, shippingphone, shippingEmail,
				shippingMobile, shippingWebsite, shippingAddress, receiverName, receiverdetailsType, receiverphone,
				receiverEmail, receiverMobile, receiverWebsite, receiverAddress;

		try {
			ADMSite admSite = new ADMSite();
			List contact = new ArrayList<>();
			List billing = new ArrayList<>();
			List shipping = new ArrayList<>();
			List receiver = new ArrayList<>();
			int[] sidetailsResult = null;
			File tmpFile = new File(System.getProperty("java.io.tmpdir") + System.getProperty("file.separator")
					+ file.getOriginalFilename());
			file.transferTo(tmpFile);
			String fileName = tmpFile.getPath();
			LOGGER.info(fileName);
			Vector data = ExcelUtils.readXLSFileWithBlankCells(fileName);
			ADMSiteDetail admSiteDetail = null;
			;
			if (!CodeUtils.isEmpty(data)) {
				// i=1 because of 2nd row start ignor header
				for (int i = 1; i < data.size(); i++) {

					Vector cellStoreVector = (Vector) data.elementAt(i);
					for (int j = 0; j < cellStoreVector.size(); j++) {
						admSiteDetail = new ADMSiteDetail();
						Object myCell = cellStoreVector.elementAt(j);
						if (j == 0) {
							name = myCell.toString();
							admSite.setName(name);
						} else if (j == 1) {
							code = myCell.toString();
							admSite.setCode(code);
						} else if (j == 2) {
							country = myCell.toString();
							admSite.setCountry(country);
						} else if (j == 3) {
							state = myCell.toString();
							admSite.setState(state);
						} else if (j == 4) {
							city = myCell.toString();
							admSite.setCity(city);
						} else if (j == 5) {
							zipCode = myCell.toString();
							admSite.setZipCode(zipCode);
						} else if (j == 6) {
							timeZone = myCell.toString();
							admSite.setTimeZone(timeZone);
						} else if (j == 7) {
							displayName = myCell.toString();
							admSite.setDisplayName(displayName);
						} else if (j == 8) {
							latitude = myCell.toString();
							admSite.setLatitude(latitude);
						} else if (j == 9) {
							longitude = myCell.toString();
							admSite.setLongitude(longitude);
						} else if (j == 10) {
							status = myCell.toString();
							admSite.setStatus(status);
						} else if (j == 11) {
							type = myCell.toString();
							admSite.setType(type);
						} else if (j == 12) {
							area = myCell.toString();
							admSite.setArea(area);
						} else if (j == 13) {
							areaUnit = myCell.toString();
							admSite.setAreaUnit(areaUnit);
						} else if (j == 14) {
							numberFloors = myCell.toString();
							admSite.setNumberFloors(numberFloors);
						} else if (j == 15) {
							optStoreSDate = myCell.toString();
							if (!CodeUtils.isEmpty(optStoreSDate)) {
								admSite.setOptStoreSDate(new Date(optStoreSDate));
							} else {
								admSite.setOptStoreSDate(null);
							}
						} else if (j == 16) {
							optStoreEDate = myCell.toString();
							if (!CodeUtils.isEmpty(optStoreEDate)) {
								admSite.setOptStoreEDate(new Date(optStoreEDate));
							} else {
								admSite.setOptStoreEDate(null);
							}
						} else if (j == 17) {
							isOnlineStore = myCell.toString();
							admSite.setIsOnlineStore(isOnlineStore);
						} else if (j == 18) {
							isShipping = myCell.toString();
							admSite.setIsShipping(isShipping);
						} else if (j == 19) {
							isBilling = myCell.toString();
							admSite.setIsBilling(isBilling);
						} else if (j == 20) {
							contactName = myCell.toString();
							contact.add(contactName);
							admSiteDetail.setName(contactName);
						} else if (j == 21) {
							contactdetailsType = myCell.toString();
							contact.add(contactdetailsType);
							admSiteDetail.setDetailsType(contactdetailsType);
						} else if (j == 22) {
							contactphone = myCell.toString();
							contact.add(contactphone);
							admSiteDetail.setPhone(contactphone);
						} else if (j == 23) {
							contactEmail = myCell.toString();
							contact.add(contactEmail);
							admSiteDetail.setEmail(contactEmail);
						} else if (j == 24) {
							contactMobile = myCell.toString();
							contact.add(contactMobile);
							admSiteDetail.setMobile(contactMobile);
						} else if (j == 25) {
							contactWebsite = myCell.toString();
							contact.add(contactWebsite);
							admSiteDetail.setWebsite(contactWebsite);
						} else if (j == 26) {
							contactAddress = myCell.toString();
							contact.add(contactAddress);
							admSiteDetail.setAddress(contactAddress);
							admSite.setContact(admSiteDetail);
						} else if (j == 27) {
							billingName = myCell.toString();
							billing.add(billingName);
							admSiteDetail.setName(billingName);
						} else if (j == 28) {
							billingdetailsType = myCell.toString();
							billing.add(billingdetailsType);
							admSiteDetail.setDetailsType(billingdetailsType);
						} else if (j == 29) {
							billingphone = myCell.toString();
							billing.add(billingphone);
							admSiteDetail.setPhone(billingphone);
						} else if (j == 30) {
							billingEmail = myCell.toString();
							billing.add(billingEmail);
							admSiteDetail.setEmail(billingEmail);
						} else if (j == 31) {
							billingMobile = myCell.toString();
							billing.add(billingMobile);
							admSiteDetail.setMobile(billingMobile);
						} else if (j == 32) {
							billingWebsite = myCell.toString();
							billing.add(billingWebsite);
							admSiteDetail.setWebsite(billingWebsite);
						} else if (j == 33) {
							billingAddress = myCell.toString();
							billing.add(billingAddress);
							admSiteDetail.setAddress(billingAddress);
							admSite.setBilling(admSiteDetail);
						} else if (j == 34) {
							shippingName = myCell.toString();
							shipping.add(shippingName);
							admSiteDetail.setName(shippingName);
						} else if (j == 35) {
							shippingdetailsType = myCell.toString();
							shipping.add(shippingdetailsType);
							admSiteDetail.setDetailsType(shippingdetailsType);
						} else if (j == 36) {
							shippingphone = myCell.toString();
							shipping.add(shippingphone);
							admSiteDetail.setPhone(shippingphone);
						} else if (j == 37) {
							shippingEmail = myCell.toString();
							shipping.add(shippingEmail);
							admSiteDetail.setEmail(shippingEmail);
						} else if (j == 38) {
							shippingMobile = myCell.toString();
							shipping.add(shippingMobile);
							admSiteDetail.setMobile(shippingMobile);
						} else if (j == 39) {
							shippingWebsite = myCell.toString();
							shipping.add(shippingWebsite);
							admSiteDetail.setWebsite(shippingWebsite);
						} else if (j == 40) {
							shippingAddress = myCell.toString();
							shipping.add(shippingAddress);
							admSiteDetail.setAddress(shippingAddress);
							admSite.setShipping(admSiteDetail);
						} else if (j == 41) {
							receiverName = myCell.toString();
							receiver.add(receiverName);
							admSiteDetail.setName(receiverName);
						} else if (j == 42) {
							receiverdetailsType = myCell.toString();
							receiver.add(receiverdetailsType);
							admSiteDetail.setDetailsType(receiverdetailsType);
						} else if (j == 43) {
							receiverphone = myCell.toString();
							receiver.add(receiverphone);
							admSiteDetail.setPhone(receiverphone);
						} else if (j == 44) {
							receiverEmail = myCell.toString();
							receiver.add(receiverEmail);
							admSiteDetail.setEmail(receiverEmail);
						} else if (j == 45) {
							receiverMobile = myCell.toString();
							receiver.add(receiverMobile);
							admSiteDetail.setMobile(receiverMobile);
						} else if (j == 46) {
							receiverWebsite = myCell.toString();
							receiver.add(receiverWebsite);
							admSiteDetail.setWebsite(receiverWebsite);
						} else if (j == 47) {
							receiverAddress = myCell.toString();
							receiver.add(receiverAddress);
							admSiteDetail.setAddress(receiverAddress);
							admSite.setReceiver(admSiteDetail);
						}
					}
					OffsetDateTime creationTime = OffsetDateTime.now();
					creationTime = creationTime.plusHours(5);
					creationTime = creationTime.plusMinutes(30);
					admSite.setCreatedTime(creationTime);
					String ipAddress = InetAddress.getLocalHost().getHostAddress().trim().toString();
					admSite.setIpAddress(ipAddress);
					if (admSite.getStatus().equalsIgnoreCase("Active")) {
						admSite.setActive('1');
					} else {
						admSite.setActive('0');
					}
					LOGGER.debug("Query,for creating,is going to start...");
					int result = aDMSiteService.create(admSite);
					LOGGER.debug("Query,for creating,has ended...");
					if (result != 0) {
						sidetailsResult = createSiteDetail(admSite);
					}
				}
				if (sidetailsResult[0] == 1 && sidetailsResult[1] == 1 && sidetailsResult[2] == 1
						&& sidetailsResult[3] == 1) {
					appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
							.put(CodeUtils.RESPONSE, "").put(CodeUtils.RESPONSE_MSG, AppStatusMsg.FILE_UPLOADED))
							.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

				} else {
					appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
							AppStatusMsg.FAILURE_MSG);
				}
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				LOGGER.info(AppStatusMsg.BLANK);
			}
		}

		catch (CannotGetJdbcConnectionException ex) {
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.SITE_CREATOR_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.SITE_CREATOR_ERROR, appStatus.getMessage(AppModuleErrors.SITE_CREATOR_ERROR));

		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally" })
	@RequestMapping(value = "po/get/all", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getAllPOSites(@RequestParam("type") Integer type,
			@RequestParam("pageno") Integer pageNo, @RequestParam("search") String search,
			@RequestParam("module") String module) {

		AppResponse appResponse = null;
		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;
		List<Map<String, String>> data = null;
		ObjectNode node = getMapper().createObjectNode();
		ArrayNode arrNode = getMapper().createArrayNode();
		String token = ((HttpServletRequest) request).getHeader("X-Auth-Token");
		JsonArray orgDetails = CodeUtils.decode(token).get("ORG_DETAILS").getAsJsonArray();
		JsonObject jNode = orgDetails.get(0).getAsJsonObject();
		String orgId = jNode.get("orgId").getAsString();
		

		String username = "";

		if (!CodeUtils.isEmpty(request)) {
			username = request.getAttribute("name").toString();
		}

		try {

			if (type == 1) {
				totalRecord = aDMSiteService.countPOSite(username);
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo - 1) * pageSize;
				data = aDMSiteService.getPOSite(username, offset, pageSize);

			} else if (type == 2) {

			} else {
				if (type == 3 && !search.isEmpty()) {
					totalRecord = aDMSiteService.countSearchPOSite(username, search);
					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo - 1) * pageSize;
					data = aDMSiteService.getSearchPOSite(username, search, offset, pageSize);
				}
			}

			if (!CodeUtils.isEmpty(data)) {

				data.stream().forEach(e -> {
					ObjectNode tempItemNode = getMapper().createObjectNode();
					tempItemNode.put("siteCode", e.get("SITE_CODE"));
					tempItemNode.put("siteName", e.get("SITE_NAME"));

					arrNode.add(tempItemNode);

				});

				Map<String, String> hm = aDMSiteService.getDefaultPOSite(username);

				String defaultSiteCode = "";
				String defaultSiteName = "";

				if (!CodeUtils.isEmpty(hm)) {

					defaultSiteCode = hm.get("SITE_CODE");
					defaultSiteName = hm.get("SITE_NAME");

				}
				ObjectNode defaultNode = getMapper().createObjectNode();
				defaultNode.put("defaultSiteCode", defaultSiteCode);
				defaultNode.put("defaultSiteName", defaultSiteName);
				node.put("defaultSiteKey", defaultNode);

				node.put(CodeUtils.RESPONSE, arrNode);

				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put("currPage", pageNo).put("prePage", previousPage)
								.put("maxPage", maxPage).put(CodeUtils.RESPONSE_MSG, "").putAll(node))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

//				appResponse = successResponse(pageNo, previousPage, maxPage, node);
			} else {

				if (username.equalsIgnoreCase("vmart")) {
					String udfExistenceStatus = piService.getUDFExistenceStatus(orgId);

					Map<String, Object> proMap = JsonUtils.toMap(JsonUtils.toJsonObject(udfExistenceStatus));

					String vmartSiteCode = proMap.get("SITE_CODE").toString();
					String vmartSiteName = proMap.get("SITE_NAME").toString();

					ObjectNode tempVmartNode = getMapper().createObjectNode();
					tempVmartNode.put("siteCode", vmartSiteCode);
					tempVmartNode.put("siteName", vmartSiteName);

					arrNode.add(tempVmartNode);

					ObjectNode defaultNode = getMapper().createObjectNode();
					defaultNode.put("defaultSiteCode", vmartSiteCode);
					defaultNode.put("defaultSiteName", vmartSiteName);
					node.put("defaultSiteKey", defaultNode);

					node.put(CodeUtils.RESPONSE, arrNode);

					appResponse = new AppResponse.Builder()
							.data(getMapper().createObjectNode().put("currPage", pageNo).put("prePage", previousPage)
									.put("maxPage", pageNo).put(CodeUtils.RESPONSE_MSG, "").putAll(node))
							.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

				} else {
					appResponse = blankResponse();
				}

			}

		} catch (IndexOutOfBoundsException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.SITE_DETAIL_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.SITE_DETAIL_READER_ERROR));

			LOGGER.info(ex.getMessage());
		} catch (MyBatisSystemException ex) {
			LOGGER.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CONNECTION_ERROR_CODE,
					AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, AppStatusMsg.CONNECTION_NOT_AVAILABLE);

		} catch (DataAccessException ex) {
			ex.printStackTrace();
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.SITE_READER_ERROR, appStatus.getMessage(AppModuleErrors.SITE_READER_ERROR));

			LOGGER.info(ex.getMessage());
		} catch (Exception ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);

			LOGGER.info(ex.getMessage());
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}
}
