package com.supplymint.layer.web.endpoint.abstracts;

import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.WebRequest;

import com.supplymint.exception.StatusCodes;
import com.supplymint.exception.SupplyMintException;
import com.supplymint.layer.web.model.AppResponse;

//@ControllerAdvice
public class ExceptionController //extends ResponseEntityExceptionHandler 
{
	
	
	
	//@ExceptionHandler(SupplyMintException.class)
	public ResponseEntity<AppResponse> exception(SupplyMintException supplyMintException,WebRequest webRequest)
	{
		
		SupplyMintException exp=null;
		
		if(supplyMintException instanceof SupplyMintException)
		{
			exp=(SupplyMintException)supplyMintException;
		}
		
		AbstractEndpoint abstractEndpoint=new AbstractEndpoint();
		
		AppResponse appResponse=abstractEndpoint.buildErrorResponse(StatusCodes.AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, "9999",
				exp.getMessage());
		
			
		return ResponseEntity.ok(appResponse);
		
		
	}
	
	
	
	
}
