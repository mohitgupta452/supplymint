package com.supplymint.layer.web.endpoint.abstracts;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.supplymint.exception.StatusCodes.AppStatusCodes;
import com.supplymint.layer.web.model.AppResponse;

/**
 * 
 * @author Ashish Singh Dev
 * @version 1.0
 * @since Sept 17, 2018
 *
 */

@RestController
public class ApplicationStatusEndpoint extends AbstractEndpoint {

	public static final String PATH = "/ping";

	@Autowired
	private Environment env;

	@RequestMapping(method = RequestMethod.GET, path = { "/" }, consumes = { MediaType.ALL_VALUE })
	void handleFoo(HttpServletRequest request, HttpServletResponse response) throws IOException {

		response.sendRedirect(request.getContextPath().concat("/ping"));
	}

	@RequestMapping(consumes = { MediaType.ALL_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE }, path = {
			"/ping" })
	public ResponseEntity<AppResponse> ping() {

		AppResponse serviceResponse = new AppResponse.Builder().status(AppStatusCodes.GENERIC_SUCCESS_CODE)
				.data(getArrayNode().add(getObjectNode().put("ping", "pong"))).build();

		return ResponseEntity.status(HttpStatus.OK).body(serviceResponse);
	}

	@RequestMapping(consumes = { MediaType.ALL_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE }, path = {
			"/profiles" })
	public ResponseEntity<AppResponse> callDNMAPI() {

		Map<String, String> headers = new HashMap<>();
		headers.put("Content-Type", "application/json");

		ObjectNode data = getMapper().createObjectNode();
		data.put("status", "2000");

		String activeProfiles = Arrays.stream(env.getActiveProfiles()).map(n -> n.toString())
				.collect(Collectors.joining(","));
		AppResponse response = new AppResponse.Builder().data(getArrayNode().add(activeProfiles))
				.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
}
