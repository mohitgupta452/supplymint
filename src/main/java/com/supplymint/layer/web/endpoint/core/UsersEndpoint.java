package com.supplymint.layer.web.endpoint.core;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.supplymint.config.application.AppCodeConfig;
import com.supplymint.exception.StatusCodes.AppException;
import com.supplymint.exception.StatusCodes.AppModuleErrors;
import com.supplymint.exception.StatusCodes.AppStatusCodes;
import com.supplymint.exception.StatusCodes.AppStatusMsg;
import com.supplymint.exception.StatusCodes.ClientErrorMessage;
import com.supplymint.exception.StatusCodes.ClientErrors;
import com.supplymint.exception.StatusCodes.ServerErrors;
import com.supplymint.exception.SupplyMintException;
import com.supplymint.layer.business.service.core.UserService;
import com.supplymint.layer.data.core.entity.Users;
import com.supplymint.layer.web.endpoint.abstracts.AbstractEndpoint;
import com.supplymint.layer.web.model.AppResponse;
import com.supplymint.util.CodeUtils;

/**
 * @author Manoj Singh
 * @since 20 OCT 2018
 * @version 1.0
 */

@RestController
@RequestMapping(path = UsersEndpoint.PATH, produces = { MediaType.APPLICATION_JSON_VALUE })
public class UsersEndpoint extends AbstractEndpoint {

	public static final String PATH = "core/user";

	private UserService userService;

	@Autowired
	private AppCodeConfig appStatus;

	@Autowired
	public UsersEndpoint(UserService userService) {
		this.userService = userService;
	}

	@SuppressWarnings("finally")
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> updateUser(@RequestBody JsonNode payload) {

		AppResponse appResponse = null;
		Users user = new Users();
		int result = 0;
		try {
			user.setFirstName(payload.get("firstName").asText());
			user.setLastName(payload.get("lastName").asText());
			user.setUsername(payload.get("userName").asText());
			user.setMobileNumber(payload.get("mobileNumber").asText());
			user.setEmail(payload.get("eid").asText());
			result = userService.updateUser(user);

			if (result != 0) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, payload)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.USER_UPDATE_ERROR,
						appStatus.getMessage(AppModuleErrors.USER_UPDATE_ERROR));
			}

		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.USER_UPDATE_ERROR,
					appStatus.getMessage(AppModuleErrors.USER_UPDATE_ERROR));

		} catch (Exception ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			throw new SupplyMintException(AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "deprecation", "finally", "unchecked", "rawtypes" })
	@RequestMapping(value = "/get/all", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getAllUsers() {

		AppResponse appResponse = null;

		try {
			List data = userService.getAllUsers();
			ObjectNode objectNode = getMapper().createObjectNode();
			ArrayNode array = getMapper().createArrayNode();
			data.forEach(e -> {
				ObjectNode node = getMapper().valueToTree(e);
				array.add(node);
			});
			objectNode.put(CodeUtils.RESPONSE, array);
			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(objectNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.USER_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.USER_READER_ERROR));
		} catch (Exception ex) {
			ex.printStackTrace();
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings("finally")
	@RequestMapping(value = "/get/{eid}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getByIdentifierAndModule(@PathVariable(value = "eid") String eid) {

		AppResponse appResponse = null;
		int id = 0;
		try {

			id = Integer.parseInt(eid);
			Users data = userService.findById(id);
			ObjectNode objectNode = getMapper().createObjectNode();

			objectNode.put("eid", data.getEid());
			objectNode.put("firstName", data.getFirstName());
			objectNode.put("lastName", data.getLastName());
			objectNode.put("userName", data.getUsername());
			objectNode.put("mobileNumber", data.getMobileNumber());
			objectNode.put("email", data.getEmail());

			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
						.putPOJO("resource", objectNode).put("message", AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {

				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
						ClientErrorMessage.BAD_REQUEST);
			}
		} catch (NumberFormatException ex) {

			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.NUMBER_FORMAT_MSG);
			throw new SupplyMintException(AppException.NUMBER_FORMAT_EXCEPTION);

		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, ServerErrors.INTERNAL_SERVER_ERROR,
					AppStatusMsg.FAILURE_MSG);
			throw new SupplyMintException(AppStatusMsg.FAILURE_MSG);
		} finally {

			return ResponseEntity.ok(appResponse);
		}

	}

}
