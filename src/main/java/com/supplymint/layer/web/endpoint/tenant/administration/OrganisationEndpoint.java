package com.supplymint.layer.web.endpoint.tenant.administration;

import java.net.InetAddress;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.CannotGetJdbcConnectionException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.supplymint.config.application.AppCodeConfig;
import com.supplymint.exception.StatusCodes.AppErrorCodes;
import com.supplymint.exception.StatusCodes.AppModuleErrors;
import com.supplymint.exception.StatusCodes.AppStatusCodes;
import com.supplymint.exception.StatusCodes.AppStatusMsg;
import com.supplymint.exception.StatusCodes.ClientErrors;
import com.supplymint.exception.SupplyMintException;
import com.supplymint.layer.business.contract.administration.OrganisationService;
import com.supplymint.layer.data.tenant.administration.entity.ADMOrganisation;
import com.supplymint.layer.web.endpoint.abstracts.AbstractEndpoint;
import com.supplymint.layer.web.model.AppResponse;
import com.supplymint.util.CodeUtils;
import com.supplymint.util.UploadUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;

@Api(value = "Organisation Controller", description = "REST Apis related to Organisation operation!!!!")
@RestController
@RequestMapping(path = OrganisationEndpoint.PATH, produces = { MediaType.APPLICATION_JSON_VALUE })
public final class OrganisationEndpoint extends AbstractEndpoint {

	private static final Logger LOGGER = LoggerFactory.getLogger(OrganisationEndpoint.class);
	public static final String PATH = "admin/org";
	private OrganisationService organisationService;
	public static final String DOWNLOAD_IMAGE = "DOWNLOAD_IMG";
	public static String defaultUrl = "define the bucket path from s3 bucket info for default image";

	@Autowired
	private OrganisationEndpoint(OrganisationService organisationService) {
		this.organisationService = organisationService;
	}

	@Autowired
	private AppCodeConfig appStatus;

	@ApiImplicitParams({
			@ApiImplicitParam(name = "X-AUTH-TOKEN", value = "Authorization token", required = true, dataType = "string", paramType = "header") })
	@ApiOperation(value = "Get specific organisation details By id ", response = ADMOrganisation.class, authorizations = {
			@Authorization(value = "apiKey") }, tags = "getOrganisationById")
	@SuppressWarnings("finally")
	@RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getById(@PathVariable("id") String orgId, ServletRequest request)
			throws SupplyMintException {
		AppResponse appResponse = null;
		try {
			if (!CodeUtils.isEmpty(orgId) && CodeUtils.isNumber(orgId)) {
				LOGGER.debug("Query is going to start to find the organization details by specific id...");
				ADMOrganisation data = organisationService.getById(Integer.parseInt(orgId));
				LOGGER.debug("Query has ended and got all details...");
				if (!CodeUtils.isEmpty(data)) {
					appResponse = new AppResponse.Builder()
							.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, data)
									.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
							.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				} else {
					appResponse = buildErrorResponse(AppErrorCodes.GENERIC_EVENT_NOT_AVAILABLE_ERROR_CODE,
							ClientErrors.NOT_FOUND, AppStatusMsg.BLANK);
				}
			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
						AppStatusMsg.NUMBER_FORMAT_MSG);
			}
		} catch (CannotGetJdbcConnectionException ex) {
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.ORG_READER_ERROR,
					AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.ORG_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.ORG_READER_ERROR));

		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}

	}

	@SuppressWarnings({ "finally", "unused" })
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> insertData(@RequestBody ADMOrganisation admOrganisation, ServletRequest request)
			throws SupplyMintException {

		AppResponse appResponse = null;
		Map<String, String> uploadImage = null;
		try {
			LOGGER.debug("Query,for validating existing organization data, is going to start... ");
			List<ADMOrganisation> validation = organisationService.checkOrgExistance(admOrganisation);
			LOGGER.debug("Query has ended the validation... ");

			if (validation.size() == 0) {
				// String userName = (String)session.getAttribute("username");
				OffsetDateTime dateTime = OffsetDateTime.now();
				dateTime = dateTime.plusHours(5);
				dateTime = dateTime.plusMinutes(30);
				String ipAddress = InetAddress.getLocalHost().getHostAddress().trim().toString();
				admOrganisation.setIpAddress(ipAddress);
				admOrganisation.setCreatedBy("");
				admOrganisation.setCreatedTime(dateTime);
				/*
				 * if (admOrganisation.getIsDefault().equalsIgnoreCase("FALSE")) {
				 * LOGGER.debug("Query ,for uploading the data,is going to start ...");
				 * uploadImage = organisationService.uploadImage(admOrganisation);
				 * LOGGER.debug("Query ,for uploading the data, has ended ...");
				 * 
				 * // admOrganisation.setImagePath(uploadImage.get("bucketKey"));
				 * admOrganisation.setImagePath(uploadImage.get("url"));
				 * 
				 * } else if (admOrganisation.getIsDefault().equalsIgnoreCase("TRUE")) {
				 * admOrganisation.setImagePath(defaultUrl); }
				 * 
				 */
				LOGGER.debug("Query, for creating organization,is going to start...");
				int result = organisationService.insert(admOrganisation);
				LOGGER.debug("Query, for creating organization,has ended...");
//				String url = UploadUtils.downloadHistory(admOrganisation.getImagePath(), DOWNLOAD_IMAGE, request);
//				// admOrganisation.setImagePath(uploadImage.get("url"));
//				admOrganisation.setImagePath(url);
				if (result != 0) {
					appResponse = new AppResponse.Builder()
							.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, admOrganisation)
									.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.ORG_SUCCESS_MSG))
							.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				} else {
					appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
							AppModuleErrors.ORG_CREATE_ERROR, appStatus.getMessage(AppModuleErrors.ORG_CREATE_ERROR));
				}
			} else {
				String email = admOrganisation.getContEmail();
				boolean isContainingEmail = validation.parallelStream().allMatch(i -> i.getContEmail().equals(email));

				if (isContainingEmail) {
					appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
							AppStatusMsg.EXIST_ORGANISATION_EMAIL);
				} else {
					appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
							AppStatusMsg.EXIST_ORGANISATION_MOBILE);
				}
			}
		} catch (CannotGetJdbcConnectionException ex) {
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.ORG_CREATE_ERROR,
					AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.ORG_CREATE_ERROR,
					appStatus.getMessage(AppModuleErrors.ORG_CREATE_ERROR));
		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}

	}

	@SuppressWarnings({ "finally", "unused" })
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseEntity<AppResponse> update(@RequestBody ADMOrganisation admOrganisation, ServletRequest request)
			throws SupplyMintException {
		AppResponse appResponse = null;
		Map<String, String> uploadImage = null;
		try {
			LOGGER.debug("Query,for validating existing data, is going to start... ");
			List<ADMOrganisation> validation = organisationService.checkOrgUpdateExistance(admOrganisation);
			LOGGER.debug("Query,for validating existing data,has ended... ");
			if (validation.size() == 0) {
				OffsetDateTime dateTime = OffsetDateTime.now();
				String ipAddress = InetAddress.getLocalHost().getHostAddress().trim().toString();
				admOrganisation.setIpAddress(ipAddress);
				admOrganisation.setUpdatedBy("");
				admOrganisation.setUpdationTime(dateTime);

				/*
				 * if (admOrganisation.getIsDefault().equalsIgnoreCase("FALSE")) {
				 * LOGGER.debug("Query,for uploading data, is going to start... "); uploadImage
				 * = organisationService.uploadImage(admOrganisation);
				 * LOGGER.debug("Query,for uploading data, has ended... ");
				 * admOrganisation.setImagePath(uploadImage.get("bucketKey")); } else if
				 * (admOrganisation.getIsDefault().equalsIgnoreCase("TRUE")) {
				 * admOrganisation.setImagePath(defaultUrl); }
				 */
				LOGGER.debug("Query,for updating data, is going to start...");
				int result = organisationService.update(admOrganisation);
				LOGGER.debug("Query,for updating data, has ended...");

				// String url = UploadUtils.downloadHistory(admOrganisation.getImagePath(),
				// DOWNLOAD_IMAGE, request);
				// admOrganisation.setImagePath(url);

				// admOrganisation.setImagePath(uploadImage.get("url"));

				if (result == 1) {
					appResponse = new AppResponse.Builder()
							.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, admOrganisation)
									.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.ORG_UPDATE_MSG))
							.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				} else {
					appResponse = buildErrorResponse(AppErrorCodes.GENERIC_EVENT_NOT_AVAILABLE_ERROR_CODE,
							ClientErrors.NOT_FOUND, AppStatusMsg.BLANK);
				}
			} else {

				String email = admOrganisation.getContEmail();
				boolean isContainingEmail = validation.parallelStream().allMatch(i -> i.getContEmail().equals(email));

				if (isContainingEmail) {
					appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
							AppStatusMsg.EXIST_ORGANISATION_EMAIL);
				} else {
					appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
							AppStatusMsg.EXIST_ORGANISATION_MOBILE);
				}
			}
		} catch (CannotGetJdbcConnectionException ex) {
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.ORG_UPDATE_ERROR,
					AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.ORG_UPDATE_ERROR,
					appStatus.getMessage(AppModuleErrors.ORG_UPDATE_ERROR));
		}

		catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}

	}

	@SuppressWarnings("finally")
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> deleteById(@PathVariable String id) {
		AppResponse appResponse = null;
		try {
			LOGGER.debug("Query,for deleting all details,is going to start...");
			int result = organisationService.deleteById(Integer.parseInt(id));
			LOGGER.debug("Query,for updating data, has ended...");
			if (result != 0) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = buildErrorResponse(AppErrorCodes.GENERIC_EVENT_NOT_AVAILABLE_ERROR_CODE,
						ClientErrors.NOT_FOUND, AppStatusMsg.BLANK);
			}
		} catch (CannotGetJdbcConnectionException ex) {
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.ORG_DELETE_ERROR,
					AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.ORG_DELETE_ERROR,
					appStatus.getMessage(AppModuleErrors.ORG_DELETE_ERROR));
		} catch (NumberFormatException ex) {
			LOGGER.debug(String.format("Error Occoured From NumberFormatException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.NUMBER_FORMAT_MSG);
		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	// merge get all
	@SuppressWarnings({ "finally", "unused" })
	@RequestMapping(value = "/get/all", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getAll(@RequestParam("pageNo") Integer pageNo,
			@RequestParam("type") Integer type, @RequestParam("search") String search,
			@RequestParam("orgName") String orgName, @RequestParam("displayName") String displayName,
			@RequestParam("status") String status, HttpServletRequest request) {

		AppResponse appResponse = null;
		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;
		// ObjectNode objectNode;
		int flag = 0;
		flag = type;
		List<ADMOrganisation> data = null;
		ADMOrganisation admOrganisation = null;

		String userName = "";
		try {
			if (flag == 1) {
				LOGGER.debug("Query,for checking all record,is going to start...");
				totalRecord = organisationService.record();
				LOGGER.debug("Query,for checking all record,has ended...");
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
				LOGGER.debug("Query,for getting data,is going to start...");
				data = organisationService.getAll(offset, pageSize);
				LOGGER.debug("Query,for getting data,has ended..");
				if (!CodeUtils.isEmpty(data)) {
					// appResponse = orgSuccessResponse(pageNo, previousPage, maxPage, objectNode,
					// data, request);
					appResponse = successResponse(pageNo, previousPage, maxPage, buildJson(data));
				} else {
					appResponse = blankResponse();
				}
			} else if (flag == 2) {
				admOrganisation = new ADMOrganisation();
				admOrganisation.setOrgName(orgName);
				admOrganisation.setDisplayName(displayName);
				admOrganisation.setStatus(status);
				LOGGER.debug("Query,for filtering all record,is going to start...");
				totalRecord = organisationService.filterRecord(admOrganisation);
				LOGGER.debug("Query,for filtering all record,has ended...");
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
				LOGGER.debug("Query,for getting all filter record,is going to start...");
				data = organisationService.filterOrganisation(offset, pageSize, admOrganisation);
				LOGGER.debug("Query,for getting all filter record,has ended...");
				if (!CodeUtils.isEmpty(data)) {
					// objectNode = buildJson(data);
					// appResponse = orgSuccessResponse(pageNo, previousPage, maxPage, objectNode,
					// data, request);
					appResponse = successResponse(pageNo, previousPage, maxPage, buildJson(data));

				} else {
					appResponse = blankResponse();
				}
			} else {
				if (flag == 3 && !search.isEmpty()) {
					// data = searchAll(pageNo, search);
					LOGGER.debug("Query,for searching all record,is going to start...");
					totalRecord = organisationService.searchRecord(search);
					LOGGER.debug("Query,for searching all record,has ended...");
					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo - 1) * pageSize;
					LOGGER.debug("Query,for getting all searched record,is going to start...");
					data = organisationService.searchAll(offset, pageSize, search);
					LOGGER.debug("Query,for getting all searched record,has ended...");
					if (!CodeUtils.isEmpty(data)) {
						// appResponse = orgSuccessResponse(pageNo, previousPage, maxPage, objectNode,
						// data, request);
						appResponse = successResponse(pageNo, previousPage, maxPage, buildJson(data));

					} else {
						appResponse = blankResponse();
					}
				} else {
					appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
							AppStatusMsg.FAILURE_MSG);
				}
			}
		} catch (CannotGetJdbcConnectionException ex) {
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.ORG_READER_ERROR,
					AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.ORG_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.ORG_READER_ERROR));

		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings("deprecation")
	ObjectNode buildJson(List<ADMOrganisation> data) {
		ObjectNode objectNode = getMapper().createObjectNode();
		ArrayNode array = getMapper().createArrayNode();
		data.forEach(e -> {

			ObjectNode node = getMapper().valueToTree(e);
			array.add(node);

		});
		objectNode.put(CodeUtils.RESPONSE, array);
		return objectNode;
	}

	AppResponse notFoundResponse() {
		AppResponse appResponse = buildErrorResponse(AppErrorCodes.GENERIC_EVENT_NOT_AVAILABLE_ERROR_CODE,
				ClientErrors.NOT_FOUND, AppStatusMsg.BLANK);
		return appResponse;
	}

	@SuppressWarnings("deprecation")
	public static AppResponse orgSuccessResponse(int pageNo, int previousPage, int maxPage, ObjectNode objectNode,
			List<ADMOrganisation> data, HttpServletRequest request) {
		ArrayNode arrNode = getMapper().createArrayNode();
		String nullValue = null;
		data.stream().forEach(e -> {
			String url = null;
			ObjectNode tempNode = getMapper().createObjectNode();
			tempNode.put("orgID", e.getOrgID());
			tempNode.put("orgName", e.getOrgName());
			tempNode.put("displayName", e.getDisplayName());
			tempNode.put("description", e.getDescription());
			tempNode.put("billToAddress", e.getBillToAddress());
			tempNode.put("contPerson", e.getContPerson());
			tempNode.put("contNumber", e.getContNumber());
			tempNode.put("contEmail", e.getContEmail());
			tempNode.put("active", e.getActive());
			tempNode.put("status", e.getStatus());
			tempNode.put("ipAddress", e.getIpAddress());
			tempNode.put("createdBy", e.getCreatedBy());
			tempNode.put("enterpriseGSTN", e.getEnterpriseGSTN());
			if (e.getCreatedTime() != null) {
				tempNode.put("createdTime", e.getCreatedTime().format(DateTimeFormatter.ofPattern("dd MMM yyy HH:mm")));
			} else {
				tempNode.put("createdTime", nullValue);
			}
			tempNode.put("updatedBy", e.getUpdatedBy());
			if (e.getUpdationTime() != null) {
				tempNode.put("updationTime",
						e.getUpdationTime().format(DateTimeFormatter.ofPattern("dd MMM yyy HH:mm")));
			} else {
				tempNode.put("updationTime", nullValue);
			}
			tempNode.put("additional", e.getAdditional());

			// String path =;
			String path = e.getImagePath();
			// downloadUrl = UploadUtils.downloadHistory(bucketLink, Generate_ExcelCSV,
			// request);
			url = UploadUtils.downloadHistory(path, DOWNLOAD_IMAGE, request);
			tempNode.put("imagePath", url);
			tempNode.put("isDefault", e.getIsDefault());
			arrNode.add(tempNode);
		});

		objectNode.put(CodeUtils.RESPONSE, arrNode);

		AppResponse appResponse = new AppResponse.Builder()
				.data(getMapper().createObjectNode().put("currPage", pageNo).put("prePage", previousPage)
						.put("maxPage", maxPage).put(CodeUtils.RESPONSE_MSG, "").putAll(objectNode))
				.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
		return appResponse;
	}

}
