package com.supplymint.layer.web.endpoint.tenant.demand;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.CannotGetJdbcConnectionException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.supplymint.config.application.AppCodeConfig;
import com.supplymint.exception.StatusCodes.AppErrorCodes;
import com.supplymint.exception.StatusCodes.AppModuleErrors;
import com.supplymint.exception.StatusCodes.AppStatusCodes;
import com.supplymint.exception.StatusCodes.AppStatusMsg;
import com.supplymint.exception.StatusCodes.ClientErrors;
import com.supplymint.exception.SupplyMintException;
import com.supplymint.layer.business.contract.demand.OTBService;
import com.supplymint.layer.data.config.ThreadLocalStorage;
import com.supplymint.layer.data.tenant.demand.entity.OTBPlanASD;
import com.supplymint.layer.data.tenant.demand.entity.OTBPlanDFD;
import com.supplymint.layer.data.tenant.demand.entity.OpenToBuy;
import com.supplymint.layer.web.endpoint.abstracts.AbstractEndpoint;
import com.supplymint.layer.web.model.AppResponse;
import com.supplymint.util.ApplicationUtils.CustomRunState;
import com.supplymint.util.ApplicationUtils.Quarter;
import com.supplymint.util.ApplicationUtils.TenantHash;
import com.supplymint.util.CodeUtils;
import com.supplymint.util.JsonUtils;

@RestController
@RequestMapping(path = OTBEndpoint.PATH, produces = { MediaType.APPLICATION_JSON_VALUE })
public class OTBEndpoint extends AbstractEndpoint {

	public static final String PATH = "demand/otb";

	private static final Logger LOGGER = LoggerFactory.getLogger(OTBEndpoint.class);

	private OTBService otbService;

	@Autowired
	private AppCodeConfig appStatus;

	@Autowired
	public OTBEndpoint(OTBService otbService) {
		this.otbService = otbService;
	}

	@SuppressWarnings("finally")
	@RequestMapping(value = "/get/planName", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getByPlanName(@RequestParam(value = "planName") String planName) {
		AppResponse appResponse = null;

		try {
			LOGGER.info("retrive OTB Plan by planName.");
			OpenToBuy data = otbService.getByPlanName(planName);
			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, data)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = buildErrorResponse(AppErrorCodes.GENERIC_EVENT_NOT_AVAILABLE_ERROR_CODE, ClientErrors.NOT_FOUND,
						AppStatusMsg.BLANK);
			}
		} catch (NumberFormatException ex) {
			LOGGER.debug(String.format("Error Occoured From NumberFormatException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.NUMBER_FORMAT_MSG);

		} catch (CannotGetJdbcConnectionException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.OTB_READER_ERROR,
					AppStatusMsg.FAILED_JDBC_CONNECTION);
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.OTB_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.OTB_READER_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings("finally")
	@RequestMapping(value = "/get/planId", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getByPlanId(@RequestParam(value = "planId") String planId) {
		AppResponse appResponse = null;

		try {
			LOGGER.info("retrive OTB Plan by planId");
			OpenToBuy data = otbService.getByPlanId(planId);
			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, data)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = buildErrorResponse(AppErrorCodes.GENERIC_EVENT_NOT_AVAILABLE_ERROR_CODE, ClientErrors.NOT_FOUND,
						AppStatusMsg.BLANK);
			}
		} catch (NumberFormatException ex) {
			LOGGER.debug(String.format("Error Occoured From NumberFormatException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.NUMBER_FORMAT_MSG);

		} catch (CannotGetJdbcConnectionException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.OTB_READER_ERROR,
					AppStatusMsg.FAILED_JDBC_CONNECTION);
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.OTB_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.OTB_READER_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings("finally")
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> createOTBPlanName(@RequestBody OpenToBuy openToBuy) {

		AppResponse appResponse = null;
		int result = 0;
		try {
			result = otbService.create(openToBuy);
			if (result != 0) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, openToBuy)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.OTB_SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {

				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.OTB_CREATOR_ERROR,
						appStatus.getMessage(AppModuleErrors.OTB_CREATOR_ERROR));
			}
		} catch (CannotGetJdbcConnectionException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.OTB_CREATOR_ERROR,
					AppStatusMsg.FAILED_JDBC_CONNECTION);
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.OTB_CREATOR_ERROR,
					appStatus.getMessage(AppModuleErrors.OTB_CREATOR_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (SupplyMintException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.OTB_PLANNAME_EXIST);
			LOGGER.debug(String.format("Error Occoured From SupplyMintException : %s", ex));
		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings("finally")
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> updateOTBPlan(@RequestBody OpenToBuy openToBuy) {

		AppResponse appResponse = null;
		int result = 0;
		try {

			result = otbService.updateASDAndDFD(openToBuy);

			if (result != 0) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, openToBuy)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.OTB_UPDATE_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.OTB_UPDATE_ERROR,
						appStatus.getMessage(AppModuleErrors.OTB_UPDATE_ERROR));
			}
		} catch (CannotGetJdbcConnectionException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.OTB_UPDATE_ERROR,
					AppStatusMsg.FAILED_JDBC_CONNECTION);
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.OTB_UPDATE_ERROR,
					appStatus.getMessage(AppModuleErrors.OTB_UPDATE_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings("finally")
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> deleteOTBPlan(@PathVariable(value = "id") String sid) {

		AppResponse appResponse = null;
		int id = 0;
		try {
			id = Integer.parseInt(sid);

			int result = otbService.delete(id);
			if (result != 0) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.OTB_DELETE_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.OTB_DELETE_ERROR,
						appStatus.getMessage(AppModuleErrors.OTB_DELETE_ERROR));
				LOGGER.info(appStatus.getMessage(AppModuleErrors.OTB_DELETE_ERROR));
			}
		} catch (CannotGetJdbcConnectionException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.OTB_DELETE_ERROR,
					AppStatusMsg.FAILED_JDBC_CONNECTION);
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.OTB_DELETE_ERROR,
					appStatus.getMessage(AppModuleErrors.OTB_DELETE_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (NumberFormatException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.NUMBER_FORMAT_MSG);
			LOGGER.debug(String.format("Error Occoured From NumberFormatException : %s", ex));
		} catch (Exception ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally" })
	@RequestMapping(value = "/get/activePlan", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getActiveOTBPlan() {

		AppResponse appResponse = null;
		try {
			List<OpenToBuy> getActiveOTBPlan = otbService.getActiveOTBPlan();
			
			String isShowOption=otbService.getConfig("OTB_FILTER_OPTION");
			
			Map<String,Object> mapData=JsonUtils.toMap(JsonUtils.toJsonObject(isShowOption));
			
			if (!CodeUtils.isEmpty(getActiveOTBPlan)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO("otbFilter", mapData.get("otbFilter")).
								putPOJO(CodeUtils.RESPONSE, getActiveOTBPlan)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}
		} catch (DataAccessException ex) {
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.OTB_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.OTB_READER_ERROR));
		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally", "unused" })
	@RequestMapping(value = "/update/activePlan", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> updateActivePlan(@RequestBody OpenToBuy openToBuy) {

		AppResponse appResponse = null;
		int result = 0;
		int updatePreviousActivePlan = 0;
		try {
			updatePreviousActivePlan = otbService.updatePreviousActivePlan(openToBuy);
			result = otbService.updateActivePlan(openToBuy);
			if (result != 0) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, openToBuy)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.OTB_UPDATE_ERROR,
						appStatus.getMessage(AppModuleErrors.OTB_UPDATE_ERROR));
			}
		} catch (CannotGetJdbcConnectionException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.OTB_UPDATE_ERROR,
					AppStatusMsg.FAILED_JDBC_CONNECTION);
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.OTB_UPDATE_ERROR,
					appStatus.getMessage(AppModuleErrors.OTB_UPDATE_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally" })
	@RequestMapping(value = "/remove/activePlan", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> removeActivePlan(@RequestBody OpenToBuy openToBuy) {

		AppResponse appResponse = null;
		int result = 0;

		try {
			result = otbService.removeActivePlan(openToBuy);

			if (result != 0) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.OTB_DELETE_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.OTB_DELETE_ERROR,
						appStatus.getMessage(AppModuleErrors.OTB_DELETE_ERROR));
			}
		} catch (CannotGetJdbcConnectionException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.OTB_UPDATE_ERROR,
					AppStatusMsg.FAILED_JDBC_CONNECTION);
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.OTB_UPDATE_ERROR,
					appStatus.getMessage(AppModuleErrors.OTB_UPDATE_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings("finally")
	@RequestMapping(value = "/create/user/otbplan", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> createUserOTBPlan(@RequestBody OpenToBuy openToBuy) {

		AppResponse appResponse = null;
		int result = 0;
		try {
			result = otbService.create(openToBuy);
			if (result != 0) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, openToBuy)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {

				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.OTB_CREATOR_ERROR,
						appStatus.getMessage(AppModuleErrors.OTB_CREATOR_ERROR));
			}
		} catch (CannotGetJdbcConnectionException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.OTB_CREATOR_ERROR,
					AppStatusMsg.FAILED_JDBC_CONNECTION);
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.OTB_CREATOR_ERROR,
					appStatus.getMessage(AppModuleErrors.OTB_CREATOR_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (SupplyMintException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.OTB_PLANNAME_EXIST);
			LOGGER.debug(String.format("Error Occoured From SupplyMintException : %s", ex));
		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "deprecation", "finally" })
	@RequestMapping(value = "/get/otbplan", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getOTBPlan(@RequestParam("pageNo") Integer pageNo,
			@RequestParam("activePlan") String activePlan, @RequestParam("frequency") String frequency,
			@RequestParam("startDate") String startDate, @RequestParam("endDate") String endDate,
			@RequestParam("planId") String planId) {

		AppResponse appResponse = null;
		try {
			ObjectNode objectNode = otbService.getOTBPlan(pageNo, activePlan, frequency, startDate, endDate, planId);
			if (objectNode.get(CodeUtils.RESPONSE).toString().length() > 2) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(objectNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}
		} catch (DataAccessException ex) {
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.OTB_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.OTB_READER_ERROR));
		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "deprecation", "finally" })
	@RequestMapping(value = "/get/total/otbplan", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getTotalOTBPlan(@RequestParam("activePlan") String activePlan,
			@RequestParam("frequency") String frequency, @RequestParam("startDate") String startDate,
			@RequestParam("endDate") String endDate, @RequestParam("planId") String planId) {

		AppResponse appResponse = null;
		try {
			ObjectNode objectNode = otbService.getTotalOTBPlan(activePlan, frequency, startDate, endDate, planId);
			if (objectNode.get(CodeUtils.RESPONSE).toString().length() > 2) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(objectNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}
		} catch (DataAccessException ex) {
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.OTB_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.OTB_READER_ERROR));
		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally" })
	@RequestMapping(value = "export/otbplan", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> exportOTBPlan(@RequestParam("planId") String planId,
			@RequestParam("activePlan") String activePlan,@RequestParam("frequency") String frequency, @RequestParam("startDate") String startDate,
			@RequestParam("endDate") String endDate, HttpServletRequest request) {

		AppResponse appResponse = null;

		try {
			String url = otbService.exportOTBPlan(planId, activePlan,frequency,startDate,endDate, request);
			ObjectNode objectNode = getMapper().createObjectNode();
			objectNode.put("url", url);
			if (!CodeUtils.isEmpty(url)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, objectNode)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, AppModuleErrors.OTB_CREATOR_ERROR,
						appStatus.getMessage(AppStatusMsg.FILE_NOT_UPLOADED));
			}

		} catch (ArrayIndexOutOfBoundsException e) {
			LOGGER.debug(String.format("Error Occoured From ArrayIndexOutOfBoundsException : %s", e));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, AppModuleErrors.OTB_CREATOR_ERROR,
					appStatus.getMessage(AppStatusMsg.FILE_NOT_UPLOADED));
		} catch (Exception e) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", e));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}

	}

	@SuppressWarnings({ "finally" })
	@RequestMapping(value = "/update/modification", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> updateAssortmentDetail(@RequestBody JsonNode jsonNode ) {

		AppResponse appResponse = null;
		OTBPlanASD otbPlanASD=null;
		OTBPlanDFD otbPlanDFD=null;
		Integer result = 0;

		try {
			String tenantHashkey=ThreadLocalStorage.getTenantHashKey();
			String billDate=jsonNode.get("billDate").asText();
			result = otbService.modificationDetails(jsonNode);
			if (result != 0) {
				String planId = jsonNode.get("planId").asText();
				if (!tenantHashkey.equalsIgnoreCase(TenantHash.valueOf("STYLE_BAAZAR").getHashKey())) {
					billDate = CodeUtils.convertOTBMonthlyFormat(billDate);
				} else {
					billDate = Quarter.valueOf(billDate).getQtr();
				}
				String assortmentCode = jsonNode.get("assortmentCode").asText();
				String activePlan = jsonNode.get("activePlan").asText();
				if (activePlan.equalsIgnoreCase("ASD")) {
					otbPlanASD = otbService.getByPlanIdAndAssortmentcodeASD(planId, assortmentCode, billDate);
					if (!tenantHashkey.equalsIgnoreCase(TenantHash.valueOf("STYLE_BAAZAR").getHashKey())) {
						otbPlanASD.setBillDate(CodeUtils.convertOTBMonthlyDateFormat(otbPlanASD.getFutureBillDate()));
					} else {
						otbPlanASD.setBillDate(otbPlanASD.getFutureBillDate());
					}
					appResponse = new AppResponse.Builder()
							.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, otbPlanASD)
									.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
							.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				} else {
					otbPlanDFD = otbService.getByPlanIdAndAssortmentcodeDFD(planId, assortmentCode, billDate);
					if (!tenantHashkey.equalsIgnoreCase(TenantHash.valueOf("STYLE_BAAZAR").getHashKey())) {
						otbPlanDFD.setBillDate(CodeUtils.convertOTBMonthlyDateFormat(otbPlanDFD.getBillDate()));
					} else {
						otbPlanDFD.setBillDate(otbPlanDFD.getBillDate());
					}
					appResponse = new AppResponse.Builder()
							.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, otbPlanDFD)
									.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
							.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				}
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}

		} catch (CannotGetJdbcConnectionException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.OTB_UPDATE_ERROR,
					AppStatusMsg.FAILED_JDBC_CONNECTION);
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.OTB_UPDATE_ERROR,
					appStatus.getMessage(AppModuleErrors.OTB_UPDATE_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
		} finally {
			return ResponseEntity.ok(appResponse);
		}

	}
	
	@SuppressWarnings({ "finally" })
	@RequestMapping(value = "/get/otbStatus", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getOTBStatus() {

		AppResponse appResponse = null;
		try {
			String status= otbService.getOTBCreateStatus();
			ObjectNode objectNode = getMapper().createObjectNode();
			if (!CodeUtils.isEmpty(status)) {
				objectNode.put("status", status);
				if(!status.equalsIgnoreCase(CustomRunState.FAILED.toString())) {
						appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
								.putPOJO(CodeUtils.RESPONSE, objectNode).put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
								.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				}else {
					appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
							.putPOJO(CodeUtils.RESPONSE, objectNode).put(CodeUtils.RESPONSE_MSG, AppStatusMsg.OTB_PLAN_ERROR))
							.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}
			}else {
				status="SUCCEEDED";
				objectNode.put("status", status);
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, objectNode)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}
		} catch (CannotGetJdbcConnectionException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.OTB_READER_ERROR,
					AppStatusMsg.FAILED_JDBC_CONNECTION);
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.OTB_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.OTB_READER_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}
}
