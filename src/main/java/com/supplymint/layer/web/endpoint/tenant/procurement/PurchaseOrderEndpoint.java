package com.supplymint.layer.web.endpoint.tenant.procurement;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

import javax.servlet.http.HttpServletRequest;

import org.apache.ibatis.exceptions.PersistenceException;
import org.mybatis.spring.MyBatisSystemException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.core.env.Environment;
import org.springframework.dao.DataAccessException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.CannotGetJdbcConnectionException;
import org.springframework.transaction.CannotCreateTransactionException;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.services.s3.model.S3Object;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.supplymint.config.application.AppCodeConfig;
import com.supplymint.config.aws.config.AmazonS3Client;
import com.supplymint.config.aws.utils.AWSUtils.Status;
import com.supplymint.exception.StatusCodes.AppModuleErrors;
import com.supplymint.exception.StatusCodes.AppModuleMsg;
import com.supplymint.exception.StatusCodes.AppStatusCodes;
import com.supplymint.exception.StatusCodes.AppStatusMsg;
import com.supplymint.exception.StatusCodes.ClientErrorMessage;
import com.supplymint.exception.StatusCodes.ClientErrors;
import com.supplymint.exception.SupplyMintException;
import com.supplymint.layer.business.contract.procurement.PurchaseIndentService;
import com.supplymint.layer.business.contract.procurement.PurchaseOrderService;
import com.supplymint.layer.data.config.ThreadLocalStorage;
import com.supplymint.layer.data.metadata.PurchaseIndentMetaData;
import com.supplymint.layer.data.metadata.PurchaseOrderMetaData;
import com.supplymint.layer.data.tenant.administration.entity.ADMItem;
import com.supplymint.layer.data.tenant.administration.entity.PurchaseOrderPricePoint;
import com.supplymint.layer.data.tenant.entity.procurement.config.DeptItemUDFMappings;
import com.supplymint.layer.data.tenant.procurement.entity.Category;
import com.supplymint.layer.data.tenant.procurement.entity.DeptItemUDFSettings;
import com.supplymint.layer.data.tenant.procurement.entity.DeptSizeData;
import com.supplymint.layer.data.tenant.procurement.entity.FinCharge;
import com.supplymint.layer.data.tenant.procurement.entity.IndtCatDescUDFList;
import com.supplymint.layer.data.tenant.procurement.entity.InvSetUDF;
import com.supplymint.layer.data.tenant.procurement.entity.ProcurementUploadSummary;
import com.supplymint.layer.data.tenant.procurement.entity.PurchaseIndent;
import com.supplymint.layer.data.tenant.procurement.entity.PurchaseIndentDetail;
import com.supplymint.layer.data.tenant.procurement.entity.PurchaseIndentHistory;
import com.supplymint.layer.data.tenant.procurement.entity.PurchaseOrder;
import com.supplymint.layer.data.tenant.procurement.entity.PurchaseOrderChild;
import com.supplymint.layer.data.tenant.procurement.entity.PurchaseOrderConfiguration;
import com.supplymint.layer.data.tenant.procurement.entity.PurchaseOrderParent;
import com.supplymint.layer.data.tenant.procurement.entity.RatioList;
import com.supplymint.layer.data.tenant.procurement.entity.TempPurchaseOrderChild;
import com.supplymint.layer.data.tenant.procurement.entity.UDFMaster;
import com.supplymint.layer.data.tenant.procurement.entity.UDF_SettingsMaster;
import com.supplymint.layer.web.endpoint.abstracts.AbstractEndpoint;
import com.supplymint.layer.web.model.AppResponse;
import com.supplymint.util.ApplicationUtils.DefaultParameter;
import com.supplymint.util.ApplicationUtils.TenantHash;
import com.supplymint.util.CodeUtils;
import com.supplymint.util.JsonUtils;
import com.supplymint.util.UploadUtils;

@RestController
@RequestMapping(path = PurchaseOrderEndpoint.PATH)
public class PurchaseOrderEndpoint extends AbstractEndpoint {

	public static final String PATH = "admin/po";

	private static final Logger log = LoggerFactory.getLogger(PurchaseOrderEndpoint.class);

	private String tenantHashkey;

	@Autowired
	private PurchaseOrderService poService;

	@Autowired
	private PlatformTransactionManager txManager;

	@Autowired
	private PurchaseIndentService piService;

	@Autowired
	private HttpServletRequest request;

	@Autowired
	private AppCodeConfig appStatus;

	@Autowired
	private Environment env;

	@Autowired
	private AmazonS3Client amazonS3Client;

	@Autowired
	public PurchaseOrderEndpoint(PurchaseOrderService poService) {
		this.poService = poService;
	}

	/*
	 * @SuppressWarnings({ "deprecated", "finally" })
	 * 
	 * @RequestMapping(method = RequestMethod.GET, value = "/get/pattern")
	 * 
	 * @ResponseBody public ResponseEntity<AppResponse>
	 * getAllPIPattern(@RequestParam("pageNo") Integer pageNo,
	 * 
	 * @RequestParam("type") Integer type, @RequestParam("search") String search,
	 * 
	 * @RequestParam("pattern") String pattern) { AppResponse appResponse = null;
	 * List<String> patternList = null; ObjectNode node =
	 * getMapper().createObjectNode(); ArrayNode arrNode =
	 * getMapper().createArrayNode(); int previousPage = 0; int pageSize = 10; int
	 * totalRecord = 0; int offset = 0; int maxPage = 0;
	 * 
	 * try {
	 * 
	 * if (type == 1) { totalRecord = poService.getCountAllPIPattern(); maxPage =
	 * (totalRecord + pageSize - 1) / pageSize; previousPage = pageNo - 1; offset =
	 * (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
	 * 
	 * patternList = poService.getAllPIPattern(offset, pageSize); }
	 * 
	 * else if (type == 2) { totalRecord =
	 * poService.countFilterAllPIPattern(pattern); maxPage = (totalRecord + pageSize
	 * - 1) / pageSize; previousPage = pageNo - 1; offset = (pageNo == 1) ? pageNo -
	 * 1 : (pageNo - 1) * pageSize;
	 * 
	 * patternList = poService.filterAllPIPattern(offset, pageSize, pattern); }
	 * 
	 * else {
	 * 
	 * if (type == 3 && !CodeUtils.isEmpty(search)) { totalRecord =
	 * poService.countSearchAllPIPattern(search); maxPage = (totalRecord + pageSize
	 * - 1) / pageSize; previousPage = pageNo - 1; offset = (pageNo == 1) ? pageNo -
	 * 1 : (pageNo - 1) * pageSize;
	 * 
	 * patternList = poService.searchAllPIPattern(offset, pageSize, search); } }
	 * 
	 * if (!CodeUtils.isEmpty(patternList)) {
	 * 
	 * patternList.stream().forEach(e -> { arrNode.add(e); });
	 * 
	 * node.put(CodeUtils.RESPONSE, arrNode);
	 * 
	 * appResponse = new AppResponse.Builder()
	 * .data(getMapper().createObjectNode().put("currPage", pageNo).put("prePage",
	 * previousPage) .put("maxPage", maxPage).put(CodeUtils.RESPONSE_MSG,
	 * "").putAll(node)) .status(AppStatusCodes.genericSuccessCode).build();
	 * 
	 * } else { appResponse = new AppResponse.Builder()
	 * .data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null))
	 * .status(AppStatusCodes.genericSuccessCode).build();
	 * 
	 * } } catch (PersistenceException e) { log.info(e.getMessage()); appResponse =
	 * buildErrorResponse(AppStatusCodes.genericServerErrorCode,
	 * AppModuleErrors.poGetAllPatternError, ClientErrorMessage.badRequest); } catch
	 * (DataAccessException ex) { ex.printStackTrace(); } catch (Exception e) {
	 * e.printStackTrace(); appResponse =
	 * buildErrorResponse(AppStatusCodes.genericClientErrorCode,
	 * ClientErrors.badRequest, AppStatusMsg.FAILURE_MSG);
	 * 
	 * log.info(AppStatusMsg.FAILURE_MSG); } finally { return
	 * ResponseEntity.ok(appResponse); } }
	 */
	@SuppressWarnings({ "deprecated", "finally" })
	@RequestMapping(method = RequestMethod.GET, value = "/get/article/hierarchy/data")
	public ResponseEntity<AppResponse> getArticleHierarchyLevelData(@RequestParam("pageNo") Integer pageNo,
			@RequestParam("type") Integer type, @RequestParam("search") String search,
			@RequestParam("hl4Code") String hl4Code, @RequestParam("hl4Name") String hl4Name,
			@RequestParam("hl1Name") String hl1Name, @RequestParam("hl2Name") String hl2Name,
			@RequestParam("hl3Name") String hl3Name, @RequestParam("orgId") String orgId) {

		log.info("getArticleHierarchyLevelData method starts here....");

		AppResponse appResponse = null;
		ObjectNode node = getMapper().createObjectNode();
		ArrayNode arrNode = getMapper().createArrayNode();
		List<PurchaseOrderPricePoint> articleHierarchyList = null;
		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;

		try {

			if (type == 1) {
				totalRecord = poService.getRecordArticleHierarchyLevelData();
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;

				articleHierarchyList = poService.getArticleHierarchyLevelData(offset, pageSize);
			}

			else if (type == 2) {

				totalRecord = poService.filterRecordArticleHierarchyLevelData(hl4Code, hl4Name, hl1Name, hl2Name,
						hl3Name);

				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;

				articleHierarchyList = poService.filterArticleHierarchyLevelData(offset, pageSize, hl4Code, hl4Name,
						hl1Name, hl2Name, hl3Name);
			}

			else {
				if (type == 3 && !search.isEmpty()) {

					totalRecord = poService.getRecordForSearchAllArticleHierarchyLevelData(search);
					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
					articleHierarchyList = poService.searchAllArticleHierarchyLevelData(offset, pageSize, search);

				}
			}

			if (!CodeUtils.isEmpty(articleHierarchyList)) {

				AtomicInteger count = new AtomicInteger(1);
				count.set(((pageNo - 1) * 10) + 1);

				articleHierarchyList.stream().forEach(articleHierarchyData -> {

					ObjectNode tempNode = getMapper().createObjectNode();

					tempNode.put(PurchaseOrderMetaData.Article.ID, count.intValue());
					tempNode.put(PurchaseOrderMetaData.Article.HL1CODE, articleHierarchyData.gethL1Code());
					tempNode.put(PurchaseOrderMetaData.Article.HL1NAME, articleHierarchyData.gethL1Name());
					tempNode.put(PurchaseOrderMetaData.Article.HL2CODE, articleHierarchyData.gethL2Code());
					tempNode.put(PurchaseOrderMetaData.Article.HL2NAME, articleHierarchyData.gethL2Name());
					tempNode.put(PurchaseOrderMetaData.Article.HL3CODE, articleHierarchyData.gethL3Code());
					tempNode.put(PurchaseOrderMetaData.Article.HL3NAME, articleHierarchyData.gethL3Name());
					tempNode.put(PurchaseOrderMetaData.Article.HL4CODE, articleHierarchyData.gethL4Code());
					tempNode.put(PurchaseOrderMetaData.Article.HL4NAME, articleHierarchyData.gethL4Name());
					tempNode.put(PurchaseOrderMetaData.Article.MRPSTART, articleHierarchyData.getMRPRangeFrom());
					tempNode.put(PurchaseOrderMetaData.Article.MRPEND, articleHierarchyData.getMRPRangeTo());
//					tempNode.put(PurchaseOrderMetaData.Article.HSN_SAC_CODE, articleHierarchyData.getHsnSacCode());

					arrNode.add(tempNode);
					count.getAndIncrement();
				});

//				@Author Prabhakar Srivastava Start for checking site and udf existence for generics
				String udfExistenceStatus = piService.getUDFExistenceStatus(orgId);

				Map<String, Object> proMap = JsonUtils.toMap(JsonUtils.toJsonObject(udfExistenceStatus));
				String udfExist = proMap.get("UDF").toString();
				String siteExist = proMap.get("SITE").toString();
				String otbValidation = proMap.get("OTB_VALIDATION").toString();

				String customPO = piService.getSystemDefaultConfig("CUSTOM_PO", orgId);
				Map<String, Object> customProMap = JsonUtils.toMap(JsonUtils.toJsonObject(customPO));
				String copyColor = customProMap.get("copyColor").toString();
				// End

				if (CodeUtils.isEmpty(udfExistenceStatus)) {
					throw new SupplyMintException("System default config reader error");
				}

				log.info("final Object is : {} ", node);

				node.put(CodeUtils.RESPONSE, arrNode);

				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put("currPage", pageNo).put("prePage", previousPage)
								.put("maxPage", maxPage).put("isUDFExist", udfExist).put("isSiteExist", siteExist)
								.put("otbValidation", otbValidation).put("copyColor", copyColor)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG).setAll(node))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

				log.info(AppStatusMsg.SUCCESS_MSG);
			} else {

				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			}

		} catch (MyBatisSystemException ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CONNECTION_ERROR_CODE,
					AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, AppStatusMsg.CONNECTION_NOT_AVAILABLE);
		} catch (NullPointerException ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			log.info(e.getMessage());
		} finally {
			log.info("getArticleHierarchyLevelData method starts here....");
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally" })
	@RequestMapping(method = RequestMethod.GET, value = "/get/item/details/{hl4Code}")
	public ResponseEntity<AppResponse> getItemsByArtCode(@RequestParam("pageNo") Integer pageNo,
			@RequestParam("type") Integer type, @RequestParam("search") String search,
			@PathVariable("hl4Code") String hl4Code, @RequestParam("itemCode") String itemCode,
			@RequestParam("itemName") String itemName, @RequestParam("mrp") String mrp,
			@RequestParam("rsp") String rsp) {

		log.info("getItemsByArtCode method starts here....");

		AppResponse appResponse = null;
		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;
		List<ADMItem> itemsList = null;

		ObjectNode node = getMapper().createObjectNode();
		ArrayNode arrNode = getMapper().createArrayNode();

		try {
			if (type == 1) {

				totalRecord = poService.getCountItemsByArtCode(hl4Code);
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;

				itemsList = poService.getItemsByArtCode(offset, pageSize, hl4Code);

			}
			if (type == 2) {

				totalRecord = 10;// poService.filterCountItemsByArtCode(hl4Code, itemCode, itemName, mrp, rsp);
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;

				itemsList = poService.filterItemsByArtCode(offset, pageSize, hl4Code, itemCode, itemName, mrp, rsp);

			}

			else {
				if (type == 3 & !search.isEmpty()) {

					totalRecord = poService.searchCountItemsByArtCode(hl4Code, search);
					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;

					itemsList = poService.searchItemsByArtCode(offset, pageSize, hl4Code, search);

				}
			}

			if (!itemsList.isEmpty() && itemsList.size() > 0) {
				AtomicInteger count = new AtomicInteger(1);
				itemsList.forEach(item -> {

					ObjectNode tempItemNode = getMapper().createObjectNode();

					tempItemNode.put("id", count.get());
					tempItemNode.put(PurchaseOrderMetaData.Item.ITEM_CODE, item.getiCode());
					tempItemNode.put(PurchaseOrderMetaData.Item.ITEM_NAME, item.getItemName());
					tempItemNode.put(PurchaseOrderMetaData.Item.MRP, item.getCostPrice());
					tempItemNode.put(PurchaseOrderMetaData.Item.RSP, item.getSellPrice());
					tempItemNode.put(PurchaseOrderMetaData.Item.INVHSNSACMAIN_CODE,
							Integer.parseInt(item.getHsnSacCode()));

					arrNode.add(tempItemNode);
					count.getAndIncrement();
				});

				node.put(CodeUtils.RESPONSE, arrNode);

				log.info("Final object node data is {} ", node);

				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
						.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG).put("currPage", pageNo)
						.put("prePage", previousPage).put("maxPage", maxPage).putAll(node))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

				log.info(AppStatusMsg.SUCCESS_MSG);
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			}
		} catch (MyBatisSystemException e) {
			log.info(e.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CONNECTION_ERROR_CODE,
					AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppStatusMsg.CONNECTION_NOT_AVAILABLE);
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.PO_ITEM_READER_ERROR, appStatus.getMessage(AppModuleErrors.PO_ITEM_READER_ERROR));

			log.info(ex.getMessage());

		} catch (NullPointerException e) {
			log.info(e.getMessage());

			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppStatusMsg.FAILURE_MSG);
		} catch (Exception e) {

			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			log.info(e.getMessage());

		} finally {
			log.info("getItemsByArtCode method ends here....");

			return ResponseEntity.ok(appResponse);

		}
	}

	@SuppressWarnings("finally")
	@RequestMapping(value = "/get", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getPIByPattern(@RequestParam(value = "orderId") String orderId,
			@RequestParam(value = "orderDetailId") String orderDetailId, @RequestParam(value = "orgId") String orgId) {
		AppResponse appResponse = null;
		try {
			String tenantHashKey = ThreadLocalStorage.getTenantHashKey();
//			String piJson = poService.getPI(orderId, orderDetailId);
			PurchaseIndent pi = new PurchaseIndent();
			List<PurchaseIndentDetail> piDetail = new ArrayList<PurchaseIndentDetail>();

			pi = piService.getPIMain(Integer.parseInt(orderId));

			if (tenantHashKey.equalsIgnoreCase(TenantHash.valueOf("VMART").getHashKey())) {
				piDetail = piService.getPIDetail(Integer.parseInt(orderId), orderDetailId);
			} else {
				piDetail = piService.getPIDetailForOthers(Integer.parseInt(orderId), orderDetailId);
			}

			if (!CodeUtils.isEmpty(pi)) {
				pi.setPiDetails(new ArrayList<PurchaseIndentDetail>());
				pi.getPiDetails().addAll(piDetail);
			}

			if (!CodeUtils.isEmpty(pi) && !CodeUtils.isEmpty(piDetail)) {

//				@Author Prabhakar Srivastava Start for checking site and udf existence for generics
				String udfExistenceStatus = piService.getUDFExistenceStatus(orgId);
				Map<String, Object> proMap = JsonUtils.toMap(JsonUtils.toJsonObject(udfExistenceStatus));
				String catDescModification = proMap.get("CAT_DESC_MODIFICATION").toString();
				String editDisplayName = proMap.get("EDIT_DISPLAY_NAME").toString();
				String otbValidation = proMap.get("OTB_VALIDATION").toString();

				ObjectNode piNode = buildIndentJson(pi);

				piNode.put("catDescModification", catDescModification);
				piNode.put("editDisplayName", editDisplayName);
				piNode.put("otbValidation", otbValidation);
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, piNode)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SUCCESS_CODE, ClientErrors.NOT_FOUND,
						AppStatusMsg.BLANK);
				log.info(AppStatusMsg.BLANK);
			}
		} catch (CannotGetJdbcConnectionException ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.PI_READER_ERROR,
					AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.PI_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.PI_READER_ERROR));

			log.info(ex.getMessage());

		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			log.info(e.getMessage());
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings("deprecation")
	ObjectNode buildIndentJson(PurchaseIndent pi) throws JsonParseException, JsonMappingException, IOException {
		ArrayNode detailArrayNode = getMapper().createArrayNode();

		String token = request.getHeader("X-Auth-Token");
		JsonArray orgDetails = CodeUtils.decode(token).get("ORG_DETAILS").getAsJsonArray();
		JsonObject jNode = orgDetails.get(0).getAsJsonObject();
		String orgId = jNode.get("orgId").getAsString();

		ObjectNode piMainNode = getMapper().createObjectNode();
		ObjectNode piNode = getMapper().createObjectNode();
		piNode.put("hl1Name", pi.getHl1Name());
		piNode.put("hl1Code", pi.getHl1Code());
		piNode.put("hl2Name", pi.getHl2Name());
		piNode.put("hl2Code", pi.getHl2Code());
		piNode.put("hl3Name", pi.getHl3Name());
		piNode.put("hl3Code", pi.getHl3Code());
		piNode.put("slCode", pi.getSlCode());
		piNode.put("slName", pi.getSlName());
		piNode.put("slCityName", pi.getSlCityName());
		piNode.put("slAddr", pi.getSlAddr());
		piNode.put("leadTime", pi.getLeadTime());
		piNode.put("termCode", pi.getTermCode());
		piNode.put("termName", pi.getTermName());
		piNode.put("transporterCode", pi.getTransporterCode());
		piNode.put("transporterName", pi.getTransporterName());
		piNode.put("poQuantity", pi.getPoQuantity());
		piNode.put("poAmount", pi.getPoAmount());
		piNode.put("stateCode", pi.getStateCode());
		piNode.put("isUDFExist", pi.getIsUDFExist());
		piNode.put("siteCode", pi.getSiteCode());
		piNode.put("siteName", pi.getSiteName());
		piNode.put("isSiteExist", pi.getIsSiteExist());

		log.info("detail size :" + pi.getPiDetails().size());

		for (int i = 0; i < pi.getPiDetails().size(); i++) {

			ArrayNode sizeArray = getMapper().createArrayNode();
			ArrayNode ratioArray = getMapper().createArrayNode();
			ArrayNode colorArray = getMapper().createArrayNode();
			ArrayNode finArray = getMapper().createArrayNode();
			ArrayNode imageArray = getMapper().createArrayNode();

			PurchaseIndentDetail detail = pi.getPiDetails().get(i);

			ObjectNode detailNode = getMapper().createObjectNode();

			detailNode.put("hl4Code", detail.getHl4Code());
			detailNode.put("hl4Name", detail.getHl4Name());
			detailNode.put("hsnSacCode", detail.getHsnSacCode());
			detailNode.put("hsnCode", detail.getHsnCode());
			detailNode.put("design", detail.getDesign());
			detailNode.put("containsImage", detail.getContainsImage());
			detailNode.put("rsp", detail.getRsp());
			detailNode.put("mrp", detail.getMrp());
			detailNode.put("rate", detail.getRate());

			// discount added
			detailNode.put("discountType", detail.getDiscountType());
			detailNode.put("discountValue", detail.getDiscountValue());
			detailNode.put("finalRate", detail.getFinalRate());

			//
			detailNode.put("noOfSets", detail.getNoOfSets());
			detailNode.put("deliveryDate",
					(!CodeUtils.isEmpty(detail.getDeliveryDate()) ? detail.getDeliveryDate().toString() : ""));
			detailNode.put("remarks", (!CodeUtils.isEmpty(detail.getRemarks()) ? detail.getRemarks() : ""));
			detailNode.put("quantity", detail.getQuantity());
			detailNode.put("netAmountTotal", detail.getNetAmountTotal());
			detailNode.put("calculatedMargin", detail.getCalculatedMargin());
			detailNode.put("gst", detail.getGst());
			detailNode.put("otb", detail.getOtb());
			detailNode.put("intakeMargin", detail.getIntakeMargin());
			detailNode.put("marginRule", detail.getMarginRule());
			detailNode.put("typeOfBuying", detail.getTypeOfBuying());
			detailNode.put("totalAmount", detail.getTotalAmount());
			detailNode.put("colorName", "");
			detailNode.put("sizeName", "");
			detailNode.put("tax", detail.getTax());
			detailNode.put("mrpStart", detail.getMrpStart());
			detailNode.put("mrpEnd", detail.getMrpEnd());
			detailNode.put("cat1Code", (!CodeUtils.isEmpty(detail.getCat1Code()) ? detail.getCat1Code() : ""));
			detailNode.put("cat1Name", (!CodeUtils.isEmpty(detail.getCat1Name()) ? detail.getCat1Name() : ""));
			detailNode.put("cat2Code", (!CodeUtils.isEmpty(detail.getCat2Code()) ? detail.getCat2Code() : ""));
			detailNode.put("cat2Name", (!CodeUtils.isEmpty(detail.getCat2Name()) ? detail.getCat2Name() : ""));
			detailNode.put("cat3Code", (!CodeUtils.isEmpty(detail.getCat3Code()) ? detail.getCat3Code() : ""));
			detailNode.put("cat3Name", (!CodeUtils.isEmpty(detail.getCat3Name()) ? detail.getCat3Name() : ""));
			detailNode.put("cat4Code", (!CodeUtils.isEmpty(detail.getCat4Code()) ? detail.getCat4Code() : ""));
			detailNode.put("cat4Name", (!CodeUtils.isEmpty(detail.getCat4Name()) ? detail.getCat4Name() : ""));
			detailNode.put("desc2Code", (!CodeUtils.isEmpty(detail.getDesc2Code()) ? detail.getDesc2Code() : ""));
			detailNode.put("desc2Name", (!CodeUtils.isEmpty(detail.getDesc2Name()) ? detail.getDesc2Name() : ""));
			detailNode.put("desc3Code", (!CodeUtils.isEmpty(detail.getDesc3Code()) ? detail.getDesc3Code() : ""));
			detailNode.put("desc3Name", (!CodeUtils.isEmpty(detail.getDesc3Name()) ? detail.getDesc3Name() : ""));
			detailNode.put("desc4Code", (!CodeUtils.isEmpty(detail.getDesc4Code()) ? detail.getDesc4Code() : ""));
			detailNode.put("desc4Name", (!CodeUtils.isEmpty(detail.getDesc4Name()) ? detail.getDesc4Name() : ""));
			detailNode.put("desc5Code", (!CodeUtils.isEmpty(detail.getDesc5Code()) ? detail.getDesc5Code() : ""));
			detailNode.put("desc5Name", (!CodeUtils.isEmpty(detail.getDesc5Name()) ? detail.getDesc5Name() : ""));

			ObjectNode udfNewNode = getMapper().createObjectNode();
			if (!CodeUtils.isEmpty(detail.getUdfString())) {

				UDFMaster udfMaster = getMapper().readValue(detail.getUdfString(), UDFMaster.class);

				if (!CodeUtils.isEmpty(udfMaster)) {
					udfNewNode.put("itemudf1",
							(!CodeUtils.isEmpty(udfMaster.getItemudf1()) ? udfMaster.getItemudf1() : ""));
					udfNewNode.put("itemudf2",
							(!CodeUtils.isEmpty(udfMaster.getItemudf2()) ? udfMaster.getItemudf2() : ""));
					udfNewNode.put("itemudf3",
							(!CodeUtils.isEmpty(udfMaster.getItemudf3()) ? udfMaster.getItemudf3() : ""));
					udfNewNode.put("itemudf4",
							(!CodeUtils.isEmpty(udfMaster.getItemudf4()) ? udfMaster.getItemudf4() : ""));
					udfNewNode.put("itemudf5",
							(!CodeUtils.isEmpty(udfMaster.getItemudf5()) ? udfMaster.getItemudf5() : ""));
					udfNewNode.put("itemudf6",
							(!CodeUtils.isEmpty(udfMaster.getItemudf6()) ? udfMaster.getItemudf6() : ""));
					udfNewNode.put("itemudf7",
							(!CodeUtils.isEmpty(udfMaster.getItemudf7()) ? udfMaster.getItemudf7() : ""));
					udfNewNode.put("itemudf8",
							(!CodeUtils.isEmpty(udfMaster.getItemudf8()) ? udfMaster.getItemudf8() : ""));
					udfNewNode.put("itemudf9",
							(!CodeUtils.isEmpty(udfMaster.getItemudf9()) ? udfMaster.getItemudf9() : ""));
					udfNewNode.put("itemudf10",
							(!CodeUtils.isEmpty(udfMaster.getItemudf10()) ? udfMaster.getItemudf10() : ""));
					udfNewNode.put("itemudf11",
							(!CodeUtils.isEmpty(udfMaster.getItemudf11()) ? udfMaster.getItemudf11() : ""));
					udfNewNode.put("itemudf12",
							(!CodeUtils.isEmpty(udfMaster.getItemudf12()) ? udfMaster.getItemudf12() : ""));
					udfNewNode.put("itemudf13",
							(!CodeUtils.isEmpty(udfMaster.getItemudf13()) ? udfMaster.getItemudf13() : ""));
					udfNewNode.put("itemudf14",
							(!CodeUtils.isEmpty(udfMaster.getItemudf14()) ? udfMaster.getItemudf14() : ""));
					udfNewNode.put("itemudf15",
							(!CodeUtils.isEmpty(udfMaster.getItemudf15()) ? udfMaster.getItemudf15() : ""));
					udfNewNode.put("itemudf16",
							(!CodeUtils.isEmpty(udfMaster.getItemudf16()) ? udfMaster.getItemudf16().toString() : ""));
					udfNewNode.put("itemudf17",
							(!CodeUtils.isEmpty(udfMaster.getItemudf17()) ? udfMaster.getItemudf17().toString() : ""));
					udfNewNode.put("itemudf18",
							(!CodeUtils.isEmpty(udfMaster.getItemudf18()) ? udfMaster.getItemudf18().toString() : ""));
					udfNewNode.put("itemudf19",
							(!CodeUtils.isEmpty(udfMaster.getItemudf19()) ? udfMaster.getItemudf19().toString() : ""));
					udfNewNode.put("itemudf20",
							(!CodeUtils.isEmpty(udfMaster.getItemudf20()) ? udfMaster.getItemudf20().toString() : ""));
					udfNewNode.put("udf1", (!CodeUtils.isEmpty(udfMaster.getUdf1()) ? udfMaster.getUdf1() : ""));
					udfNewNode.put("udf2", (!CodeUtils.isEmpty(udfMaster.getUdf2()) ? udfMaster.getUdf2() : ""));
					udfNewNode.put("udf3", (!CodeUtils.isEmpty(udfMaster.getUdf3()) ? udfMaster.getUdf3() : ""));
					udfNewNode.put("udf4", (!CodeUtils.isEmpty(udfMaster.getUdf4()) ? udfMaster.getUdf4() : ""));
					udfNewNode.put("udf5", (!CodeUtils.isEmpty(udfMaster.getUdf5()) ? udfMaster.getUdf5() : ""));
					udfNewNode.put("udf6", (!CodeUtils.isEmpty(udfMaster.getUdf6()) ? udfMaster.getUdf6() : ""));
					udfNewNode.put("udf7", (!CodeUtils.isEmpty(udfMaster.getUdf7()) ? udfMaster.getUdf7() : ""));
					udfNewNode.put("udf8", (!CodeUtils.isEmpty(udfMaster.getUdf8()) ? udfMaster.getUdf8() : ""));
					udfNewNode.put("udf9", (!CodeUtils.isEmpty(udfMaster.getUdf9()) ? udfMaster.getUdf9() : ""));
					udfNewNode.put("udf10", (!CodeUtils.isEmpty(udfMaster.getUdf10()) ? udfMaster.getUdf10() : ""));
					udfNewNode.put("udf11", (!CodeUtils.isEmpty(udfMaster.getUdf11()) ? udfMaster.getUdf11() : ""));
					udfNewNode.put("udf12", (!CodeUtils.isEmpty(udfMaster.getUdf12()) ? udfMaster.getUdf12() : ""));
					udfNewNode.put("udf13", (!CodeUtils.isEmpty(udfMaster.getUdf13()) ? udfMaster.getUdf13() : ""));
					udfNewNode.put("udf14", (!CodeUtils.isEmpty(udfMaster.getUdf14()) ? udfMaster.getUdf14() : ""));
					udfNewNode.put("udf15", (!CodeUtils.isEmpty(udfMaster.getUdf15()) ? udfMaster.getUdf15() : ""));
					udfNewNode.put("udf16",
							(!CodeUtils.isEmpty(udfMaster.getUdf16()) ? udfMaster.getUdf16().toString() : ""));
					udfNewNode.put("udf17",
							(!CodeUtils.isEmpty(udfMaster.getUdf17()) ? udfMaster.getUdf17().toString() : ""));
					udfNewNode.put("udf18",
							(!CodeUtils.isEmpty(udfMaster.getUdf18()) ? udfMaster.getUdf18().toString() : ""));
					udfNewNode.put("udf19",
							(!CodeUtils.isEmpty(udfMaster.getUdf19()) ? udfMaster.getUdf19().toString() : ""));
					udfNewNode.put("udf20",
							(!CodeUtils.isEmpty(udfMaster.getUdf20()) ? udfMaster.getUdf20().toString() : ""));

				}
			}

			detailNode.put("udf", udfNewNode);
			List<Category> sizeList = null;
			List<Category> colorList = null;
			List<RatioList> ratioList = null;

			try {
				sizeList = getMapper().readValue(detail.getSize(),
						getMapper().getTypeFactory().constructCollectionType(List.class, Category.class));

				colorList = getMapper().readValue(detail.getColor(),
						getMapper().getTypeFactory().constructCollectionType(List.class, Category.class));

				ratioList = getMapper().readValue(detail.getRatios(),
						getMapper().getTypeFactory().constructCollectionType(List.class, RatioList.class));

			} catch (IOException e) {
				e.printStackTrace();
			}
			sizeList.forEach(e -> {
				ObjectNode node = getMapper().valueToTree(e);
				sizeArray.add(node);
			});

			colorList.forEach(e -> {
				ObjectNode node = getMapper().valueToTree(e);
				colorArray.add(node);
			});

			ratioList.forEach(e -> {
				ObjectNode node = getMapper().valueToTree(e);
				ratioArray.add(node);
			});

			try {
				if (!CodeUtils.isEmpty(detail.getFinCharge()))
					finArray = buildFinChargeNode(detail.getFinCharge());

			} catch (IOException e1) {
				e1.printStackTrace();
			}

			List<String> imagesArray = new ArrayList<String>();

			if (!CodeUtils.isEmpty(detail.getImage1()))
				imagesArray.add(detail.getImage1());
			else if (!CodeUtils.isEmpty(detail.getImage1()))
				imagesArray.add(detail.getImage2());
			else if (!CodeUtils.isEmpty(detail.getImage3()))
				imagesArray.add(detail.getImage3());
			else if (!CodeUtils.isEmpty(detail.getImage4()))
				imagesArray.add(detail.getImage4());
			else if (!CodeUtils.isEmpty(detail.getImage5()))
				imagesArray.add(detail.getImage5());

			imagesArray.stream().forEach(e -> {
//				ObjectNode node = getMapper().valueToTree(e);
				imageArray.add(e);
			});

			detailNode.put("sizeList", sizeArray);
			detailNode.put("ratioList", ratioArray);
			detailNode.put("colorList", colorArray);
			detailNode.put("finCharge", finArray);
			detailNode.put("image", imageArray);

			detailArrayNode.add(detailNode);
		}
//		});

		String customPO = piService.getSystemDefaultConfig("CUSTOM_PO", orgId);
		Map<String, Object> customProMap = null;
		try {
			customProMap = JsonUtils.toMap(JsonUtils.toJsonObject(customPO));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		String copyColor = customProMap.get("copyColor").toString();

		piNode.put("piDetails", detailArrayNode);
		piNode.put("copyColor", copyColor);

		piMainNode.put("piKey", piNode);

		return piMainNode;
	}

//
//    {  
//        "chgCode":1,
//        "chgName":"Discount [Before Tax]",
//        "seq":1,
//        "finFormula":"B",
//        "finChgRate":0,
//        "finSource":"F",
//        "gstComponent":null,
//        "isTax":"N",
//        "sign":"-",
//        "finChgOperationLevel":"L",
//        "calculationBasis":"A",
//        "subTotal": 3345.8, 
//        "rates":null,
//        "charges": {
//              "seq": 2,
//              "chargeCode": 1003,
//              "rate": 0,
//              "formula": "B+1",
//              "sign": "+",
//              "operationLevel": "L",
//              "chargeAmount": 3355.8,
//              "subTotal": 3345.8
//          }
//          
//        
//     }

	ArrayNode buildFinChargeNode(List<FinCharge> detail) throws JsonMappingException, JsonParseException, IOException {

		ArrayNode finArray = getMapper().createArrayNode();

		detail.forEach(e -> {
//			ObjectNode node = getMapper().valueToTree(e);
			ObjectNode finNode = getMapper().createObjectNode();

			finNode.put("chgCode", e.getChgCode());
			finNode.put("chgName", e.getChgName());
			finNode.put("seq", e.getSeq());
			finNode.put("finFormula", e.getFinFormula());
			finNode.put("finChgRate", e.getFinChgRate());
			finNode.put("finSource", e.getFinSource());
			finNode.put("gstComponent", e.getGstComponent());
			finNode.put("isTax", e.getIsTax());
			finNode.put("sign", e.getSign());
			finNode.put("finChgOperationLevel", e.getFinChgOperationLevel());
			finNode.put("calculationBasis", e.getCalculationBasis());
			finNode.put("subTotal", e.getSubTotal());

			if (!CodeUtils.isEmpty(e.getChargesString())) {

//				FinChargeDetail finDetail = null;
				JsonNode finChargeDetailNode = null;
				JsonNode finChargeRateDetailNode = null;
				try {

					finChargeDetailNode = getMapper().readTree(e.getChargesString());
					if (!CodeUtils.isEmpty(e.getRatesString())) {
						finChargeRateDetailNode = getMapper().readTree(e.getRatesString());
					}
//					finDetail = getMapper().readValue(e.getChargesString(), FinChargeDetail.class);

				} catch (IOException e1) {
					e1.printStackTrace();
				}

				if (!CodeUtils.isEmpty(finChargeDetailNode)) {
//					ObjectNode finDetailNode = getMapper().createObjectNode();
//
//					finDetailNode.put("seq", finDetail.getSeq());
//					finDetailNode.put("chargeCode", finDetail.getChgCode());
//					finDetailNode.put("rate", finDetail.getRate());
//					finDetailNode.put("formula", finDetail.getFormula());
//					finDetailNode.put("sign", finDetail.getSign());
//					finDetailNode.put("operationLevel", finDetail.getOperationLevel());
//					finDetailNode.put("chargeAmount", finDetail.getChargeAmount());
//					finDetailNode.put("subTotal", finDetail.getSubTotal());

					log.info("fin charge Detail Node:" + finChargeDetailNode);

//					finNode.put("rates", e.getRates());
					finNode.put("rates", finChargeRateDetailNode);
					finNode.put("charges", finChargeDetailNode);

				}
			}

			finArray.add(finNode);
		});

		return finArray;

	}

	@SuppressWarnings({ "deprecated", "finally" })
	@RequestMapping(method = RequestMethod.GET, value = "/get/load/indent")
	@ResponseBody
	public ResponseEntity<AppResponse> getPIData(@RequestParam("pageNo") Integer pageNo,
			@RequestParam("type") Integer type, @RequestParam("search") String search,
			@RequestParam("orderId") String orderId, @RequestParam("orderId") String orderDetailId,
			@RequestParam("supplier") String supplier, @RequestParam("cityName") String cityName,
			@RequestParam("piFromDate") String piFromDate, @RequestParam("piToDate") String piToDate) {

		AppResponse appResponse = null;
		List<PurchaseIndentHistory> data = null;
		ObjectNode node = getMapper().createObjectNode();
		ArrayNode arrNode = getMapper().createArrayNode();
		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;

		try {

			if (type == 1) {
				totalRecord = poService.countLoadIndent();
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
				data = poService.getLoadIndent(offset, pageSize);
			}

			else if (type == 2) {
				totalRecord = poService.countLoadIndentFilter(orderId, supplier, cityName, piFromDate, piToDate);

				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
				data = poService.getLoadIndentFilter(orderId, supplier, cityName, piFromDate, piToDate, offset,
						pageSize);
			}

			else {

				if (type == 3 && !CodeUtils.isEmpty(search)) {
//					totalRecord = poService.searchCountPIData(search);
					totalRecord = poService.countLoadIndentSearch(search);

					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;

//					data = poService.searchPIData(offset, pageSize, search);
					data = poService.getLoadIndentSearch(search, offset, pageSize);
				}
			}

			AtomicInteger count = new AtomicInteger(1);
			count.set(((pageNo - 1) * 10) + 1);

			if (!CodeUtils.isEmpty(data)) {

				data.stream().forEach(e -> {
					ObjectNode tempItemNode = getMapper().createObjectNode();
					tempItemNode.put("id", count.get());
					tempItemNode.put("orderId", e.getOrderId());
					tempItemNode.put("orderDetailId", e.getOrderDetailId());
					tempItemNode.put("supplierName", e.getSlName());
					tempItemNode.put("cityName", e.getSlCityName());
					tempItemNode.put("generatedDate", e.getCreatedTime().toString());
					tempItemNode.put("articleCode", e.getHl4Code());

					arrNode.add(tempItemNode);
					count.getAndIncrement();

				});

				node.put(CodeUtils.RESPONSE, arrNode);

				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode().put("currPage", pageNo)
						.put("prePage", previousPage).put("maxPage", maxPage)
						.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG).putAll(node))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				log.info(AppStatusMsg.BLANK);

			}
		} catch (PersistenceException e) {
			log.info(e.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.PO_GET_PI_DATA,
					ClientErrorMessage.BAD_REQUEST);
		} catch (DataAccessException ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.PO_GET_PI_DATA,
					ClientErrorMessage.BAD_REQUEST);
		} catch (Exception e) {
			log.info(e.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);

		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "deprecation", "finally" })
	@RequestMapping(method = RequestMethod.GET, value = "/get/all/setudfsetting")
	@ResponseBody
	public ResponseEntity<AppResponse> getUDFMappingData(@RequestParam("type") Integer type,
			@RequestParam(value = "udfType") String udfType, @RequestParam(value = "isCompulsary") String isCompulsary,
			@RequestParam("displayName") String displayName, @RequestParam("search") String search,
			@RequestParam("ispo") String ispo) {

		log.info("PurchaseIndent getUDFMappingData() method starts here....");
		AppResponse appResponse = null;
		ObjectNode node = getMapper().createObjectNode();
		ArrayNode arrNode = getMapper().createArrayNode();
		List<UDF_SettingsMaster> udfMappingData = null;

		AtomicInteger count = new AtomicInteger(1);
		try {
			if (ispo.toLowerCase().contains("true")) {
				udfMappingData = poService.getPOUDFMappingData();
			} else {
				if (type == 1)
					udfMappingData = poService.getUDFMappingData();

				else if (type == 2)
					udfMappingData = poService.filterUDFMappingData(udfType, isCompulsary, displayName);

				else if (type == 3 && !search.isEmpty()) {
					udfMappingData = poService.searchUDFMappingData(search);

				}
			}

			log.info("PurchaseOrder data present in the list getUDFMappingData is {} : " + udfMappingData);

			if (udfMappingData.size() > 0 && !udfMappingData.isEmpty()) {

				udfMappingData.stream().forEach(e -> {

					boolean valueChecked = true;
//					if (e.getIsCompulsary() == 'N') {
//						valueChecked = false;
//					}
					ObjectNode tempNode = getMapper().createObjectNode();
					tempNode.put(PurchaseIndentMetaData.SetUDF.ID, count.get());
					tempNode.put(PurchaseIndentMetaData.SetUDF.UDF_TYPE, e.getUdfType());
					tempNode.put(PurchaseIndentMetaData.SetUDF.DISPAYNAME, e.getDisplayName());
					tempNode.put(PurchaseIndentMetaData.SetUDF.IS_COMPULSARY, String.valueOf(e.getIsCompulsary()));
					tempNode.put(PurchaseIndentMetaData.SetUDF.ORDER_BY, e.getOrderBy());
//					tempNode.put(PurchaseIndentMetaData.SetUDF.CHECKED, valueChecked);
					tempNode.put(PurchaseIndentMetaData.SetUDF.IS_LOV, String.valueOf(e.getIsLov()));

					arrNode.add(tempNode);
					count.getAndIncrement();
				});

				node.put(CodeUtils.RESPONSE, arrNode);

				log.info("Final object node data is  {}", node);

				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
						.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG).putAll(node))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

				log.info(AppStatusMsg.BLANK);
			}
		} catch (MyBatisSystemException ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CONNECTION_ERROR_CODE,
					AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, AppStatusMsg.CONNECTION_NOT_AVAILABLE);

		} catch (DataAccessException ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.PO_UDF_MAPPING,
					appStatus.getMessage(AppModuleErrors.PO_UDF_MAPPING));

		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			log.info(e.getMessage());
		} finally {

			log.info("PurchaseOrder getUDFMappingData() method ends here....");
			log.info(String.format("Current Tenant HashKey : %s", ThreadLocalStorage.getTenantHashKey()));
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "deprecation", "finally" })
	@RequestMapping(method = RequestMethod.GET, value = "/get/all/setudfmapping")
	@ResponseBody
	public ResponseEntity<AppResponse> getInvSetUDFData(@RequestParam("type") Integer type,
			@RequestParam("pageNo") Integer pageNo, @RequestParam(value = "udfType") String udfType,
			@RequestParam(value = "code") String code, @RequestParam("name") String name,
			@RequestParam("search") String search, @RequestParam("ispo") String ispo) {

		log.info("PurchaseIndent getInvSetUDFData() method starts here....");
		AppResponse appResponse = null;
		ObjectNode node = getMapper().createObjectNode();
		ArrayNode arrNode = getMapper().createArrayNode();
		List<InvSetUDF> invUDFData = null;
		AtomicInteger count = new AtomicInteger(1);
		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;

		try {
			if (CodeUtils.isEmpty(ispo) || !ispo.equalsIgnoreCase("true")) {
				if (type == 1) {
					totalRecord = poService.getInvSetUDFDataByUDFTypeRecord(udfType);

					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
					invUDFData = poService.getInvSetUDFDataByUDFType(offset, pageSize, udfType);
				} else if (type == 2) {
					totalRecord = poService.filterInvSetUDFDataByUDFTypeRecord(udfType, code, name);
					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
					invUDFData = poService.filterInvSetUDFDataByUDFType(offset, pageSize, udfType, code, name);
				}

				else if (type == 3 && !search.isEmpty()) {
					totalRecord = poService.searchInvSetUDFDataByUDFTypeRecord(udfType, search);
					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
					invUDFData = poService.searchInvSetUDFDataByUDFType(offset, pageSize, udfType, search);
				}

			} else if (ispo.equalsIgnoreCase("true")) {

				if (type == 1) {
					totalRecord = poService.getInvSetUDFDataByUDFTypeExtRecord(udfType);

					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
					invUDFData = poService.getInvSetUDFDataByUDFTypeExt(offset, pageSize, udfType);
				} else if (type == 2) {
					totalRecord = poService.filterInvSetUDFDataByUDFTypeExtRecord(udfType, code, name);
					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
					invUDFData = poService.filterInvSetUDFDataByUDFTypeExt(offset, pageSize, udfType, code, name);
				}

				else if (type == 3 && !search.isEmpty()) {
					totalRecord = poService.searchInvSetUDFDataByUDFTypeExtRecord(udfType, search);
					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
					invUDFData = poService.searchInvSetUDFDataByUDFTypeExt(offset, pageSize, udfType, search);
				}
			}

			log.info("PurchaseOrder data present in the list getInvSetUDFData is {} : " + invUDFData);

			if (invUDFData.size() > 0 && !invUDFData.isEmpty()) {

				invUDFData.stream().forEach(e -> {

					ObjectNode tempNode = getMapper().createObjectNode();
					tempNode.put("id", count.get());
					tempNode.put("code", e.getCode());
					tempNode.put("name", e.getName());

					arrNode.add(tempNode);
					count.getAndIncrement();
				});

				node.put(CodeUtils.RESPONSE, arrNode);

				log.info("Final object node data is  {}", node);

				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
						.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG).put("currPage", pageNo)
						.put("prePage", previousPage).put("maxPage", maxPage).putAll(node))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

				log.info(AppStatusMsg.BLANK);
			}
		} catch (MyBatisSystemException ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CONNECTION_ERROR_CODE,
					AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, AppStatusMsg.CONNECTION_NOT_AVAILABLE);

		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.PO_INV_UDF_MAPPING, appStatus.getMessage(AppModuleErrors.PO_INV_UDF_MAPPING));

			log.info(ex.getMessage());

		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			log.info(e.getMessage());
		} finally {

			log.info("PurchaseOrder getUDFMappingData() method ends here....");

			return ResponseEntity.ok(appResponse);
		}
	}

	/*
	 * @RequestMapping(value = "/create", method = RequestMethod.POST) public
	 * ResponseEntity<AppResponse> createPO(@RequestBody JsonNode poItem) {
	 * 
	 * AppResponse appResponse = null; try { log.info("Initiate PO create");
	 * PurchaseOrderTemp pot = getMapper().treeToValue(poItem,
	 * PurchaseOrderTemp.class);
	 * 
	 * if (!CodeUtils.isEmpty(pot.getOrderDate()) &&
	 * !CodeUtils.isEmpty(pot.getValidFrom()) &&
	 * !CodeUtils.isEmpty(pot.getValidTo()) && pot.getArticleId() > 0.0) {
	 * 
	 * String orderNoResult = poService.createPO(pot); String result =
	 * orderNoResult.substring(orderNoResult.indexOf(',')); String orderNo =
	 * orderNoResult.substring(0, orderNoResult.indexOf(',')) + " "; if
	 * (result.contains("0")) { log.info("PO creation unsuccessful"); appResponse =
	 * buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
	 * AppModuleErrors.PO_CREATOR_ERROR,
	 * appStatus.getMessage(AppModuleErrors.PO_CREATOR_ERROR)); } else { appResponse
	 * = new AppResponse.Builder() .data(getMapper().createObjectNode()
	 * .put(CodeUtils.RESPONSE_MSG, AppModuleMsg.PO_SUCCESS + orderNo)
	 * .putPOJO(CodeUtils.RESPONSE, pot))
	 * .status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
	 * log.info("PO creation success "); } } else { appResponse =
	 * buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE,
	 * ClientErrors.BAD_REQUEST, AppStatusMsg.PO_INPUTS); }
	 * 
	 * } catch (MyBatisSystemException ex) { log.info(ex.getMessage()); appResponse
	 * = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
	 * AppModuleErrors.PO_CREATOR_ERROR,
	 * appStatus.getMessage(AppModuleErrors.PO_CREATOR_ERROR)); } catch
	 * (DataAccessException ex) { log.info(ex.getMessage()); appResponse =
	 * buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
	 * AppModuleErrors.PO_CREATOR_ERROR,
	 * appStatus.getMessage(AppModuleErrors.PO_CREATOR_ERROR)); } catch
	 * (NumberFormatException ex) { log.info(ex.getMessage()); appResponse =
	 * buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE,
	 * ClientErrors.BAD_REQUEST, AppStatusMsg.NUMBER_FORMAT_MSG);
	 * 
	 * } catch (JsonMappingException jsonException) {
	 * log.info(jsonException.getMessage()); appResponse =
	 * buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE,
	 * ClientErrors.BAD_REQUEST, AppStatusMsg.INVALID_JSON);
	 * 
	 * } catch (Exception e) { log.info(e.getMessage()); appResponse =
	 * buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
	 * ClientErrors.BAD_REQUEST, AppStatusMsg.FAILURE_MSG); } finally { return
	 * ResponseEntity.ok(appResponse); }
	 * 
	 * }
	 */

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseEntity<AppResponse> createPO(@RequestBody JsonNode poItem) {

		TransactionDefinition defaultTxn = new DefaultTransactionDefinition();
		TransactionStatus ts = txManager.getTransaction(defaultTxn);
		String orderNo = "";
		AppResponse appResponse = null;
		try {
			String token = request.getHeader("X-Auth-Token");
			JsonArray orgDetails = CodeUtils.decode(token).get("ORG_DETAILS").getAsJsonArray();
			JsonObject jNode = orgDetails.get(0).getAsJsonObject();
			String orgId = jNode.get("orgId").getAsString();

			String tenantHashKey = ThreadLocalStorage.getTenantHashKey();
			log.info("Initiate PO create");
			PurchaseOrderParent poMain = getMapper().treeToValue(poItem, PurchaseOrderParent.class);
			poMain.setOrgId(orgId);

			if (tenantHashKey.equalsIgnoreCase(TenantHash.valueOf("VMART").getHashKey()))
				poService.validateMarginRule(poMain.getPol());

//			orderNo = CodeUtils.getPOStringOrderDate() + "/" + CodeUtils.getEightDigitRandomNumber();

			orderNo = CodeUtils.getPOStringOrderDate();

			poMain.setOrderNo(orderNo);
			poMain.setIntGHeaderId(orderNo);
//			poMain.setIntGLineId(intGLineId);

			OffsetDateTime currentTime = OffsetDateTime.now();
			currentTime = currentTime.plusHours(new Long(5));
			currentTime = currentTime.plusMinutes(new Long(30));
			String ipAddress = InetAddress.getLocalHost().getHostAddress().trim().toString();

			String userName = "";
			if (!CodeUtils.isEmpty(request)) {
				userName = request.getAttribute("name").toString();
				poMain.setCreatedBy(userName);
			}
			poMain.setCreatedTime(currentTime);
			poMain.setIpAddress(ipAddress);
			poMain.setActive(PurchaseOrderMetaData.POStatus.ACTIVE);
			poMain.setStatus(PurchaseOrderMetaData.POStatus.APPROVED_STATUS);
			
			String availableKey=piService.getSystemDefaultConfig("ESSENTIAL_PRO_PARAM", orgId);
			poMain.setAvailableKeys(availableKey);

			int poMainResult = poService.createParentPO(poMain);

			if (poMainResult > 0) {

				String detailResult = poService.createDetailPO(poMain, currentTime, ipAddress, userName);
				if (!detailResult.contains("0")) {
					txManager.commit(ts);
					int restTemplateResult = poService.sendSMPartentPO(poMain);

					if (restTemplateResult > 0) {
						log.info("PO request sent to Ginesys");
					} else {
						log.info("PO request didn't send to Ginesys");
					}
					appResponse = new AppResponse.Builder()
							.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG,
									AppModuleMsg.PO_SUCCESS + " " + poMain.getOrderNo()))
							.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
					log.info("PO created success ");

				} else {
					txManager.rollback(ts);

					appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
							AppModuleErrors.PO_CREATOR_ERROR, appStatus.getMessage(AppModuleErrors.PO_CREATOR_ERROR));
				}
			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
						AppModuleErrors.PO_CREATOR_ERROR, appStatus.getMessage(AppModuleErrors.PO_CREATOR_ERROR));
			}
		} catch (CannotCreateTransactionException ex) {
			log.info(ex.getMessage());
			txManager.rollback(ts);
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CONNECTION_ERROR_CODE,
					AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, AppStatusMsg.CONNECTION_NOT_AVAILABLE);
		} catch (MyBatisSystemException ex) {
			log.info(ex.getMessage());

			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.PO_CREATOR_ERROR,
					appStatus.getMessage(AppModuleErrors.PO_CREATOR_ERROR));
			txManager.rollback(ts);
		} catch (DataAccessException ex) {
			log.info(ex.getMessage());

			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.PO_CREATOR_ERROR,
					appStatus.getMessage(AppModuleErrors.PO_CREATOR_ERROR));
			txManager.rollback(ts);
		} catch (NumberFormatException ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.NUMBER_FORMAT_MSG);
			txManager.rollback(ts);
		} catch (SupplyMintException ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					ex.getMessage());
			txManager.rollback(ts);
		} catch (JsonMappingException jsonException) {
			log.info(jsonException.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.INVALID_JSON);

		} catch (Exception e) {

			log.info(e.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			txManager.rollback(ts);
		} finally {
			return ResponseEntity.ok(appResponse);
		}

	}

	@SuppressWarnings({ "deprecation", "finally" })
	@RequestMapping(method = RequestMethod.GET, value = "/history")
	@ResponseBody
	public ResponseEntity<AppResponse> getHistoryData(@RequestParam("type") Integer type,
			@RequestParam("pageNo") Integer pageNo, @RequestParam("orderDate") String orderDate,
			@RequestParam("orderNo") String orderNo, @RequestParam("validFrom") String validFrom,
			@RequestParam("validTo") String validTo, @RequestParam("slCode") String slCode,
			@RequestParam("slName") String slName, @RequestParam("slCityName") String slCityName,
			@RequestParam("hl1Name") String hl1Name, @RequestParam("hl2Name") String hl2Name,
			@RequestParam("hl3Name") String hl3Name, @RequestParam("search") String search) {
		log.info("PurchaseOrder getHistoryData() method starts here....");
		AppResponse appResponse = null;
		ObjectNode node = getMapper().createObjectNode();
		ArrayNode arrNode = getMapper().createArrayNode();
		List<PurchaseOrderParent> poHistoryData = null;

		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;

		try {
			String token = request.getHeader("X-Auth-Token");
			JsonArray orgDetails = CodeUtils.decode(token).get("ORG_DETAILS").getAsJsonArray();
			JsonObject jNode = orgDetails.get(0).getAsJsonObject();
			String orgId = jNode.get("orgId").getAsString();

			if (type == 1) {
				totalRecord = poService.count(orgId);

				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;

				poHistoryData = poService.findAll(offset, pageSize, orgId);
				log.info("PO history");
			}

			else if (type == 2) {

				totalRecord = poService.filterCount(orderNo, orderDate, validFrom, validTo, slCode, slName, slCityName,
						hl1Name, hl2Name, hl3Name, orgId);

				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;

				poHistoryData = poService.filter(offset, pageSize, orderNo, orderDate, validFrom, validTo, slCode,
						slName, slCityName, hl1Name, hl2Name, hl3Name, orgId);

			}

			else {

				if (type == 3 && !search.isEmpty()) {

					totalRecord = poService.searchCount(search, orgId);

					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;

					poHistoryData = poService.search(offset, pageSize, search, orgId);

				}

			}

			log.info("PurchaseOrder data present in the list getHistoryData is {} : " + poHistoryData);
			if (!CodeUtils.isEmpty(poHistoryData)) {

				poHistoryData.stream().forEach(e -> {
					ObjectNode tempNode = getMapper().createObjectNode();
					tempNode.put(PurchaseIndentMetaData.PurchaseOrder.PO_TYPE, e.getPoType());
					tempNode.put(PurchaseIndentMetaData.PurchaseOrder.HL1CODE, e.getHl1Code());
					tempNode.put(PurchaseIndentMetaData.PurchaseOrder.HL1NAME, e.getHl1Name());
					tempNode.put(PurchaseIndentMetaData.PurchaseOrder.HL2CODE, e.getHl2Code());
					tempNode.put(PurchaseIndentMetaData.PurchaseOrder.HL2NAME, e.getHl2Name());
					tempNode.put(PurchaseIndentMetaData.PurchaseOrder.HL3CODE, e.getHl3Code());
					tempNode.put(PurchaseIndentMetaData.PurchaseOrder.HL3NAME, e.getHl3Name());
					tempNode.put(PurchaseIndentMetaData.PurchaseOrder.ORDER_NO, e.getOrderNo());
					tempNode.put(PurchaseIndentMetaData.PurchaseOrder.ORDER_DATE, e.getOrderDate().toString());
					tempNode.put(PurchaseIndentMetaData.PurchaseOrder.VALID_FROM, e.getValidFrom().toString());
					tempNode.put(PurchaseIndentMetaData.PurchaseOrder.VALID_TO, e.getValidTo().toString());
					tempNode.put(PurchaseIndentMetaData.PurchaseOrder.SUPPLIER_NAME, e.getSlName());
					tempNode.put(PurchaseIndentMetaData.PurchaseOrder.SUPPLIER_CITYNAME, e.getSlCityName());
					tempNode.put(PurchaseIndentMetaData.PurchaseOrder.SLCODE, e.getSlCode());
					tempNode.put(PurchaseIndentMetaData.PurchaseOrder.SUPPLIER_ADDRESS, e.getSlAddr());
					arrNode.add(tempNode);

				});

				node.put(CodeUtils.RESPONSE, arrNode);

				log.info("Final object node data is  {}", node);

				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
						.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG).put("currPage", pageNo)
						.put("prePage", previousPage).put("maxPage", maxPage).putAll(node))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

				log.info(AppStatusMsg.BLANK);
			}
		} catch (MyBatisSystemException ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CONNECTION_ERROR_CODE,
					AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, AppStatusMsg.CONNECTION_NOT_AVAILABLE);

		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.PO_HISTORY,
					appStatus.getMessage(AppModuleErrors.PO_HISTORY));

			log.info(ex.getMessage());
		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			log.info(e.getMessage());
		} finally {
			log.info("PurchaseOrder getHistoryData() method ends here....");

			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "deprecation", "finally" })
	@RequestMapping(method = RequestMethod.POST, value = "/get/all/sizemapping")
	@ResponseBody
	public ResponseEntity<AppResponse> getSizeByDepartment(@RequestBody ObjectNode requestData) {

		log.info("PurchaseOrder getSizeByDepartment() method starts here....");
		AppResponse appResponse = null;
		ObjectNode node = getMapper().createObjectNode();
		ArrayNode arrNode = getMapper().createArrayNode();
		List<DeptSizeData> sizeData = null;
		String tenantHashKey = "";
		try {
			if (!requestData.isNull())
				tenantHashKey = ThreadLocalStorage.getTenantHashKey();
			sizeData = this.getSizesByDepartment(requestData.get("deptCode").asText(),
					requestData.get("deptName").asText(), tenantHashKey);

			log.info("PurchaseOrder data present in the list getSizeByDepartment is {} : " + sizeData);

			if (sizeData.size() > 0 && !sizeData.isEmpty()) {

				sizeData.stream().forEach(e -> {

					ObjectNode tempNode = getMapper().createObjectNode();
					tempNode.put(PurchaseIndentMetaData.Department.ORDERBY, e.getOrderBy());
					tempNode.put(PurchaseIndentMetaData.Department.CODE, e.getCode());
					tempNode.put(PurchaseIndentMetaData.Department.CNAME, e.getCname());

					arrNode.add(tempNode);
				});

				node.put(CodeUtils.RESPONSE, arrNode);

				log.info("Final object node data is  {}", node);

				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
						.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG).putAll(node))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

				log.info(AppStatusMsg.BLANK);
			}
		} catch (MyBatisSystemException ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CONNECTION_ERROR_CODE,
					AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, AppStatusMsg.CONNECTION_NOT_AVAILABLE);

		} catch (DataAccessException ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.PROCUREMENT_DEPT_SIZE, appStatus.getMessage(AppModuleErrors.PROCUREMENT_DEPT_SIZE));

		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			log.info(e.getMessage());
		} finally {

			log.info("PurchaseOrder getSizeByDepartment() method ends here....");

			return ResponseEntity.ok(appResponse);
		}
	}

	public List<DeptSizeData> getSizesByDepartment(String deptCode, String deptName, String tenantHashKey) {
		if (tenantHashKey.equalsIgnoreCase(TenantHash.valueOf("VMART").getHashKey()))
			return poService.getSizeByDept(deptCode);
		else
			return poService.getOtherSizeByDept(deptName);

	}

	@SuppressWarnings({ "deprecation", "finally" })
	@RequestMapping(method = RequestMethod.POST, value = "update/setudfsetting")
	@ResponseBody
	public ResponseEntity<AppResponse> updateUDFMapping(@RequestBody JsonNode udfMappingNode) {
		AppResponse appResponse = null;
		com.fasterxml.jackson.databind.ObjectMapper mapper = getMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

		try {
			List<UDF_SettingsMaster> udfMappingList = null;

			// JsonNode udfMappingNode
			JsonNode udfList = udfMappingNode.get("udfKey");

			String result = poService.updateUDFMapping(udfList);

			if (!result.contains("0")) {
				log.info("UDF Mapping update successful");

//				ObjectNode udfNode = poService.getUDFMappingNode(udfMappingList);
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode()
								.put(CodeUtils.RESPONSE_MSG, AppModuleMsg.PO_UDFSETTING_UPDATE)
								.putAll((ObjectNode) udfMappingNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				log.info("UDF Mapping update unsuccessful");
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
						AppStatusMsg.INVALID_JSON);

			}
		} catch (MyBatisSystemException ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CONNECTION_ERROR_CODE,
					AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, AppStatusMsg.CONNECTION_NOT_AVAILABLE);

		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.PROCUREMENT_DEPT_SIZE, appStatus.getMessage(AppModuleErrors.PROCUREMENT_DEPT_SIZE));

			log.info(ex.getMessage());
		} catch (SupplyMintException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					ex.getMessage());
		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			log.info(e.getMessage());
		} finally {

			log.info("PurchaseOrder getSizeByDepartment() method ends here....");

			return ResponseEntity.ok(appResponse);
		}

	}

	@RequestMapping(method = RequestMethod.POST, value = "update/tempsetudfsetting")
	@ResponseBody
	public ResponseEntity<AppResponse> tempUpdateUDFMapping(@RequestBody JsonNode udfMappingNode) {
		AppResponse appResponse = null;
		String result = "";

		try {

			JsonNode udfList = udfMappingNode.get("udfKey");

			result = poService.updateUDFMapping(udfList);

			if (!result.contains("0")) {
				log.info("UDF Mapping update successful");

				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode()
								.put(CodeUtils.RESPONSE_MSG, AppModuleMsg.PO_UDFSETTING_UPDATE)
								.putAll((ObjectNode) udfMappingNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				log.info("UDF Mapping update unsuccessful");
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
						AppStatusMsg.INVALID_JSON);

			}
		} catch (MyBatisSystemException ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CONNECTION_ERROR_CODE,
					AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, AppStatusMsg.CONNECTION_NOT_AVAILABLE);

		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.PROCUREMENT_DEPT_SIZE, appStatus.getMessage(AppModuleErrors.PROCUREMENT_DEPT_SIZE));

			log.info(ex.getMessage());
		} catch (SupplyMintException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					ex.getMessage());
		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			log.info(e.getMessage());
		} finally {

			log.info("PurchaseOrder getSizeByDepartment() method ends here....");

			return ResponseEntity.ok(appResponse);
		}

	}

	@SuppressWarnings("finally")
	@RequestMapping(method = RequestMethod.POST, path = "/upload/podata")
	// @Consumes(MediaType.APPLICATION_JSON)
	public ResponseEntity<AppResponse> uploadPoData(@RequestBody JsonNode data) {
		log.info("uploadPoData method starts here....");
		AppResponse appResponse = null;
		log.info("Request payload : {} ", data);
		try {
			if (!CodeUtils.isEmpty(data)) {
				// PurchaseOrderNode po = getMapper().treeToValue(data.get("poData"),
				// PurchaseOrderNode.class);

				JsonNode poJsonList = data.get("poData");

				for (JsonNode poJson : poJsonList) {
					PurchaseOrder po = getMapper().convertValue(poJson, PurchaseOrder.class);

					if (!CodeUtils.isEmpty(po)) {
						log.info("success");
					} else {
						log.info("unsuccess");
					}
				}

				// log.info("Purchase Order Node :" + po);
//				String[] payload = data.split("=");
//
//				if (payload.length > 1) {

				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
						.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG).putPOJO(CodeUtils.RESPONSE, null))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

//				} else {
//					appResponse = buildErrorResponse(AppStatusCodes.genericClientErrorCode, ClientErrors.badRequest,
//							AppStatusMsg.FAILURE_MSG);
//
//					log.info(AppStatusMsg.BLANK);
//				}
			}
		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			log.info("Third api :" + e.getMessage());
			e.printStackTrace();
		} finally {

			log.info("uploadPoData method ends here....");
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "deprecation", "finally" })
	@RequestMapping(method = RequestMethod.GET, value = "/get/active/itemcatdescmapping")
	@ResponseBody
	public ResponseEntity<AppResponse> getIndtCatDescUDFListData() {

		log.info("PurchaseOrder getIndtCatDescUDFListData() method starts here....");
		AppResponse appResponse = null;
		ObjectNode node = getMapper().createObjectNode();
		ArrayNode arrNode = getMapper().createArrayNode();
		List<IndtCatDescUDFList> data = null;

		try {

			data = poService.getIndtCatDescUDFListData();

			log.info("PurchaseOrder data present in the list getIndtCatDescUDFListData is {} : " + data);
			AtomicInteger count = new AtomicInteger(1);

			if (data.size() > 0 && !data.isEmpty()) {

				data.stream().forEach(e -> {

					ObjectNode tempNode = getMapper().createObjectNode();
					tempNode.put(PurchaseIndentMetaData.IndtCatDescUDF.ID, count.get());
					tempNode.put(PurchaseIndentMetaData.IndtCatDescUDF.CODE, e.getCode());
					tempNode.put(PurchaseIndentMetaData.IndtCatDescUDF.NAME, e.getName());
					tempNode.put(PurchaseIndentMetaData.IndtCatDescUDF.IS_SHOW, e.getIsShow());
					tempNode.put(PurchaseIndentMetaData.IndtCatDescUDF.CAT_TYPE, e.getCatType());

					arrNode.add(tempNode);
					count.getAndIncrement();
				});

				node.put(CodeUtils.RESPONSE, arrNode);

				log.info("Final object node data is  {}", node);

				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
						.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG).putAll(node))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

				log.info(AppStatusMsg.BLANK);
			}
		} catch (MyBatisSystemException ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CONNECTION_ERROR_CODE,
					AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, AppStatusMsg.CONNECTION_NOT_AVAILABLE);

		} catch (DataAccessException ex) {

			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.INDT_CAT_DESC_UDF_LIST,
					appStatus.getMessage(AppModuleErrors.INDT_CAT_DESC_UDF_LIST));

			log.info(ex.getMessage());

		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			log.info(e.getMessage());
		} finally {

			log.info("PurchaseOrder getIndtCatDescUDFListData() method ends here....");

			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "deprecation", "finally" })
	@RequestMapping(method = RequestMethod.GET, value = "/get/active/itemudfmapping")
	@ResponseBody
	public ResponseEntity<AppResponse> getIndtCatActiveUDFData() {

		log.info("PurchaseOrder getIndtCatActiveUDFData() method starts here....");
		AppResponse appResponse = null;
		ObjectNode node = getMapper().createObjectNode();
		ArrayNode arrNode = getMapper().createArrayNode();
		List<IndtCatDescUDFList> data = null;

		try {

			data = poService.getIndtCatDescUDFListActiveData();

			log.info("PurchaseOrder data present in the list getIndtCatActiveUDFData is {} : " + data);
			AtomicInteger count = new AtomicInteger(1);

			if (data.size() > 0 && !data.isEmpty()) {

				data.stream().forEach(e -> {

					ObjectNode tempNode = getMapper().createObjectNode();

					tempNode.put(PurchaseIndentMetaData.IndtCatDescUDF.ID, count.get());
					tempNode.put(PurchaseIndentMetaData.IndtCatDescUDF.CODE, e.getCode());
					tempNode.put(PurchaseIndentMetaData.IndtCatDescUDF.NAME, e.getName());
					tempNode.put(PurchaseIndentMetaData.IndtCatDescUDF.IS_SHOW, e.getIsShow());
					tempNode.put(PurchaseIndentMetaData.IndtCatDescUDF.CAT_TYPE, e.getCatType());

					arrNode.add(tempNode);
					count.getAndIncrement();

				});

				node.put(CodeUtils.RESPONSE, arrNode);

				log.info("Final object node data is  {}", node);

				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
						.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG).putAll(node))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

				log.info(AppStatusMsg.BLANK);
			}
		} catch (MyBatisSystemException ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CONNECTION_ERROR_CODE,
					AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, AppStatusMsg.CONNECTION_NOT_AVAILABLE);

		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.INDT_CAT_DESC_UDF_LIST,
					appStatus.getMessage(AppModuleErrors.INDT_CAT_DESC_UDF_LIST));

			log.info(ex.getMessage());

		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			log.info(e.getMessage());
		} finally {

			log.info("PurchaseOrder getIndtCatActiveUDFData() method ends here....");
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally" })
	@RequestMapping(method = RequestMethod.GET, value = "/get/itemudfsetting")
	@ResponseBody
	public ResponseEntity<AppResponse> getDeptItemUDFData(@RequestParam("hl3Code") String hl3code,
			@RequestParam("ispo") String ispo) {

		log.info("PurchaseOrder getDeptItemUDFData() method starts here....");
		AppResponse appResponse = null;
		List<DeptItemUDFSettings> data = null;

		try {

			if (ispo.toLowerCase().contains("true")) {
				data = poService.getPODepartmentItemUDFByHl3Name(hl3code);
			} else {
//				if (CodeUtils.isEmpty(hl4Code)) {
				data = poService.getDepartmentItemUDFByHl3Name(hl3code);
//				} else {
//					data = poService.getItemHeaderForPOCreate(hl3Name, hl4Code);
//				}				
			}
			if (!CodeUtils.isEmpty(data)) {
				ObjectNode dataNode = poService.getItemUDFSettingNode(data);

				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
						.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG).putAll(dataNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				ObjectNode dataNode = poService.getItemUDFSettingMaster(hl3code);
				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
						.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG).putAll(dataNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}

		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.INDT_CAT_DESC_UDF_LIST,
					appStatus.getMessage(AppModuleErrors.INDT_CAT_DESC_UDF_LIST));

			log.info(ex.getMessage());

		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			log.info(e.getMessage());
		} finally {
			log.info("PurchaseOrder getDeptItemUDFData() method ends here....");
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally" })
	@RequestMapping(method = RequestMethod.GET, value = "/get/itemudfmapping")
	@ResponseBody
	public ResponseEntity<AppResponse> getDeptItemUDFMappingsData(@RequestParam("type") Integer type,
			@RequestParam("pageNo") Integer pageNo, @RequestParam("search") String search,
			@RequestParam("hl3code") String hl3code, @RequestParam("description") String description,
			@RequestParam("ispo") String ispo, @RequestParam("cat_desc_udf") String cat_desc_udf,
			@RequestParam("hl4code") String hl4code) {

		log.info("PurchaseOrder getDeptItemUDFMappingsData() method starts here....");
		AppResponse appResponse = null;
		List<DeptItemUDFMappings> data = null;
		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;

		try {
			if (CodeUtils.isEmpty(ispo) || !ispo.equalsIgnoreCase("true")) {

				if (type == 1) {
					totalRecord = this.countItemUdfMapping(hl3code, cat_desc_udf, description, hl4code);

					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
					data = this.getAllItemUdfMappingData(offset, pageSize, hl3code, cat_desc_udf, description, hl4code);

				} else if (type == 3 && !search.isEmpty()) {

					totalRecord = this.countSearchedItemUdfMapping(hl3code, cat_desc_udf, search, hl4code);

					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
					data = this.getAllSearchedDataItemUdfMapping(offset, pageSize, hl3code, cat_desc_udf, search,
							hl4code);

				}
			} else if (ispo.equalsIgnoreCase("true")) {
				if (type == 1) {
					totalRecord = poService.countItemUDFMappingByHl3NameUDFEXT(hl3code, cat_desc_udf);
					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
					data = poService.getItemUDFMappingByHl3NameUDFEXT(offset, pageSize, hl3code, cat_desc_udf);
				} else if (type == 3 && !search.isEmpty()) {

					totalRecord = poService.searchCountPOItemUdfMappingByHl3NameAndUDF(hl3code, cat_desc_udf, search);
					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
					data = poService.searchItemUdfMappingByHl3NameAndUDFEXT(offset, pageSize, hl3code, cat_desc_udf,
							search);
				}
			}
			if (!CodeUtils.isEmpty(data)) {
				ObjectNode mappingNode = poService.getItemUDFMappingNode(data, hl3code);
				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
						.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG).put("currPage", pageNo)
						.put("prePage", previousPage).put("maxPage", maxPage).putAll(mappingNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

				log.info(AppStatusMsg.BLANK);
			}
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.DEP_ITEM_UDF_MAPPING, appStatus.getMessage(AppModuleErrors.DEP_ITEM_UDF_MAPPING));

			log.info(ex.getMessage());
		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			log.info(e.getMessage());
		} finally {
			log.info("PurchaseOrder getItemUDFMappingsData() method ends here....");
			return ResponseEntity.ok(appResponse);
		}
	}

	int countItemUdfMapping(String hl3code, String cat_desc_udf, String description, String hl4code) {
		String tenantHashKey = ThreadLocalStorage.getTenantHashKey();
		if (tenantHashKey.equalsIgnoreCase(TenantHash.valueOf("VMART").getHashKey())) {
			return poService.countItemUdfMappingByHl3NameAndUDF(hl3code, cat_desc_udf, description, hl4code);
		} else {
			return poService.countItemUdfMappingByHl3NameAndUDFForOthers(hl3code, cat_desc_udf, description, hl4code);
		}
	}

	List<DeptItemUDFMappings> getAllItemUdfMappingData(int offset, int pageSize, String hl3code, String cat_desc_udf,
			String description, String hl4code) {
		List<DeptItemUDFMappings> data = null;
		String tenantHashKey = ThreadLocalStorage.getTenantHashKey();
		if (tenantHashKey.equalsIgnoreCase(TenantHash.valueOf("VMART").getHashKey())) {
			data = poService.itemUdfMappingByHl3NameAndUDF(offset, pageSize, hl3code, cat_desc_udf, description,
					hl4code);
		} else {
			data = poService.itemUdfMappingByHl3NameAndUDFForOthers(offset, pageSize, hl3code, cat_desc_udf,
					description, hl4code);
		}
		return data;
	}

	int countSearchedItemUdfMapping(String hl3code, String cat_desc_udf, String search, String hl4code) {
		String tenantHashKey = ThreadLocalStorage.getTenantHashKey();
		if (tenantHashKey.equalsIgnoreCase(TenantHash.valueOf("VMART").getHashKey())) {
			return poService.searchCountItemUdfMappingByHl3NameAndUDF(hl3code, cat_desc_udf, search, hl4code);
		} else {
			return poService.searchCountItemUdfMappingByHl3NameAndUDFforOther(hl3code, cat_desc_udf, search, hl4code);
		}
	}

	List<DeptItemUDFMappings> getAllSearchedDataItemUdfMapping(int offset, int pageSize, String hl3code,
			String cat_desc_udf, String search, String hl4code) {
		List<DeptItemUDFMappings> data = null;
		String tenantHashKey = ThreadLocalStorage.getTenantHashKey();
		if (tenantHashKey.equalsIgnoreCase(TenantHash.valueOf("VMART").getHashKey())) {
			data = poService.searchItemUdfMappingByHl3NameAndUDF(offset, pageSize, hl3code, cat_desc_udf, search,
					hl4code);
		} else {
			data = poService.searchItemUdfMappingByHl3NameAndUDFforOthers(offset, pageSize, hl3code, cat_desc_udf,
					search, hl4code);
		}
		return data;
	}

	@SuppressWarnings({ "deprecation", "finally" })
	@RequestMapping(method = RequestMethod.POST, value = "update/itemudfsetting")
	@ResponseBody
	public ResponseEntity<AppResponse> updateCatDescUDF(@RequestBody JsonNode catDescNode) {
		AppResponse appResponse = null;
		com.fasterxml.jackson.databind.ObjectMapper mapper = getMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

		try {
			List<UDF_SettingsMaster> udfMappingList = null;

			// JsonNode udfMappingNode
			// JsonNode udfList = catDescNode.get("catDescKey").get("categories");

			// udfMappingList = getMapper().treeToValue(udfMappingNode.get("udfKey"),
			// ArrayList.class);
			String result = poService.updateItemCatDescUDF(catDescNode);

			if (!result.contains("0") && !CodeUtils.isEmpty(result)) {
				log.info("UDF Mapping update successful");

//				ObjectNode udfNode = poService.getUDFMappingNode(udfMappingList);
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode()
								.put(CodeUtils.RESPONSE_MSG, AppModuleMsg.PO_ITEMUDFSETTING_UPDATE)
								.putPOJO(CodeUtils.RESPONSE, catDescNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				log.info("UDF Mapping update unsuccessful");
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
						AppStatusMsg.INVALID_JSON);

			}
		} catch (MyBatisSystemException ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CONNECTION_ERROR_CODE,
					AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, AppStatusMsg.CONNECTION_NOT_AVAILABLE);

		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.PROCUREMENT_DEPT_SIZE, appStatus.getMessage(AppModuleErrors.PROCUREMENT_DEPT_SIZE));

			log.info(ex.getMessage());

		} catch (SupplyMintException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					ex.getMessage());
		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			log.info(e.getMessage());
		} finally {

			log.info("PurchaseOrder getDeptItemUDFMappingsData() method ends here....");
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "deprecation", "finally" })
	@RequestMapping(method = RequestMethod.GET, value = "/get/setudfmapping")
	@ResponseBody
	public ResponseEntity<AppResponse> getInvSetPOItemUDFData(@RequestParam("type") Integer type,
			@RequestParam(value = "udfType") String udfType, @RequestParam(value = "code") String code,
			@RequestParam("name") String name, @RequestParam("search") String search) {

		log.info("PurchaseOrder getInvSetPOItemUDFData() method starts here....");
		AppResponse appResponse = null;
		ObjectNode node = getMapper().createObjectNode();
		ArrayNode arrNode = getMapper().createArrayNode();
		List<InvSetUDF> invUDFData = null;
		AtomicInteger count = new AtomicInteger(1);
		try {

			if (type == 1) {
				invUDFData = poService.getInvSetPOItemUDFData(udfType);
			} else if (type == 2) {
				invUDFData = poService.filterInvSetPOItemUDFData(udfType, code, name);
			} else if (type == 3 && !search.isEmpty()) {
				invUDFData = poService.searchInvSetPOItemUDFData(udfType, search);
			}

			log.info("PurchaseOrder data present in the list getInvSetPOItemUDFData is {} : " + invUDFData);

			if (invUDFData.size() > 0 && !invUDFData.isEmpty()) {

				invUDFData.stream().forEach(e -> {

					ObjectNode tempNode = getMapper().createObjectNode();
					tempNode.put("code", e.getCode());
					tempNode.put("value", e.getName());
					tempNode.put("udfType", e.getUdfType());
					tempNode.put("ext", String.valueOf(e.getExt()));

					arrNode.add(tempNode);
					count.getAndIncrement();
				});

				node.put(CodeUtils.RESPONSE, arrNode);

				log.info("Final object node data is  {}", node);

				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
						.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG).putAll(node))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

				log.info(AppStatusMsg.BLANK);
			}
		} catch (MyBatisSystemException ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CONNECTION_ERROR_CODE,
					AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, AppStatusMsg.CONNECTION_NOT_AVAILABLE);

		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.PO_INV_UDF_MAPPING, appStatus.getMessage(AppModuleErrors.PO_INV_UDF_MAPPING));

			log.info(ex.getMessage());

		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			log.info(e.getMessage());
		} finally {

			log.info("PurchaseOrder getInvSetPOItemUDFData() method ends here....");

			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "deprecation", "finally" })
	@RequestMapping(method = RequestMethod.POST, value = "/insertupdate/setudfmapping")
	@ResponseBody
	public ResponseEntity<AppResponse> createAndUpdatePOItemUDF(@RequestBody JsonNode node) {
		AppResponse appResponse = null;
		int result = 0;
		String errorMessage = "";
		String successMessage = "";
		String isInsert = "";
		try {
			isInsert = node.get("isInsert").asText();

			// JsonNode categoryList = node.get("isInsert").get("setUDFType");
			result = poService.createAndUpdatePOItemUDF(node);

			if (result != 0) {

				log.info("UDF Mapping update successful");
				if (isInsert.equalsIgnoreCase("true")) {
					successMessage = AppModuleMsg.PO_SETUDFMAPPING_INSERT;
				} else {
					successMessage = AppModuleMsg.PO_SETUDFMAPPING_UPDATE;
				}

//				ObjectNode udfNode = poService.getUDFMappingNode(udfMappingList);
				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
						.put(CodeUtils.RESPONSE_MSG, successMessage).putPOJO(CodeUtils.RESPONSE, node))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				if (isInsert.equalsIgnoreCase("true")) {
					errorMessage = AppModuleErrors.INSERT_SET_UDF_SETTING;
				} else {
					errorMessage = AppModuleErrors.UPDATE_SET_UDF_SETTING;
				}

				log.info("UDF Mapping update unsuccessful");
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
						AppStatusMsg.INVALID_JSON);

			}
		} catch (SupplyMintException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					ex.getMessage());
			log.info(ex.getMessage());
		} catch (DataAccessException ex) {
			if (isInsert.equalsIgnoreCase("true")) {
				errorMessage = AppModuleErrors.INSERT_SET_UDF_SETTING;
			} else {
				errorMessage = AppModuleErrors.UPDATE_SET_UDF_SETTING;
			}
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, errorMessage,
					appStatus.getMessage(errorMessage));

			log.info(ex.getMessage());
		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			log.info(e.getMessage());
		} finally {

			log.info("PurchaseOrder getDeptItemUDFMappingsData() method ends here....");
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "deprecation", "finally" })
	@RequestMapping(method = RequestMethod.GET, value = "/get/itemudfsetting/hl4code")
	@ResponseBody
	public ResponseEntity<AppResponse> getdepartmentItemUDFByHl3Name(@RequestParam(value = "hl3code") String hl3code) {

		log.info("PurchaseOrder getDeptItemUDFData() method starts here....");
		AppResponse appResponse = null;
		List<DeptItemUDFSettings> data = null;

		try {

			data = poService.getItemHeaderForPOCreate(hl3code);
			if (!CodeUtils.isEmpty(data)) {
				ObjectNode dataNode = poService.getItemUDFSettingNode(data);

				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
						.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG).putAll(dataNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

				log.info(AppStatusMsg.BLANK);
			}
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.INDT_CAT_DESC_UDF_LIST,
					appStatus.getMessage(AppModuleErrors.INDT_CAT_DESC_UDF_LIST));

			log.info(ex.getMessage());
		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			log.info(e.getMessage());
		} finally {
			log.info("PurchaseOrder getDeptItemUDFData() method ends here....");
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally" })
	@RequestMapping(method = RequestMethod.POST, value = "/insertupdate/itemudfmapping")
	@ResponseBody
	public ResponseEntity<AppResponse> createAndUpdatePODeptItemUDFMappings(@RequestBody JsonNode node) {
		// ,ServletRequest request

		log.info("PurchaseOrder createAndUpdatePODeptItemUDFMappings() method start here....");
		AppResponse appResponse = null;
		int result = 0;
		String errorMessage = "";
		String isInsert = "";
		String successMessage = "";
		try {
			isInsert = node.get("isInsert").asText();
			result = poService.createAndUpdatePODeptItemUDFMappings(node, request);

			if (result != 0) {
				log.info("UDF Mapping update successful");
				successMessage = (isInsert.equalsIgnoreCase("true")) ? AppModuleMsg.PO_ITEMUDFMAPPING_INSERT
						: AppModuleMsg.PO_ITEMUDFMAPPING_UPDATE;
				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
						.put(CodeUtils.RESPONSE_MSG, successMessage).putPOJO(CodeUtils.RESPONSE, node))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				if (isInsert.equalsIgnoreCase("true")) {
					errorMessage = AppModuleErrors.INSERT_ITEM_UDF_MAPPING;
				} else {
					errorMessage = AppModuleErrors.UPDATE_ITEM_UDF_MAPPING;
				}
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, errorMessage,
						appStatus.getMessage(errorMessage));
			}
		} catch (SupplyMintException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					ex.getMessage());
			log.info(ex.getMessage());

		} catch (DataAccessException ex) {
			if (isInsert.equalsIgnoreCase("true")) {
				errorMessage = AppModuleErrors.INSERT_ITEM_UDF_MAPPING;
			} else {
				errorMessage = AppModuleErrors.UPDATE_ITEM_UDF_MAPPING;
			}
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, errorMessage,
					appStatus.getMessage(errorMessage));
			log.info(ex.getMessage());
		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			log.info(e.getMessage());
		} finally {

			log.info("PurchaseOrder createAndUpdatePODeptItemUDFMappings() method ends here....");
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally" })
	@RequestMapping(method = RequestMethod.POST, value = "/udpate/sizemapping")
	@ResponseBody
	public ResponseEntity<AppResponse> udpateSizeMapping(@RequestBody ObjectNode sizes) {

		log.info("PurchaseOrder updatesizemapping() method starts here....");
		AppResponse appResponse = null;
		String sizeResult = "";

		try {
			String tenantHashKey=ThreadLocalStorage.getTenantHashKey();
			sizeResult = poService.updateSizeMapping(sizes,tenantHashKey);

			if (!sizeResult.contains("0") && !CodeUtils.isEmpty(sizeResult)) {
				log.info("UDF size Mapping update successful");
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode()
								.put(CodeUtils.RESPONSE_MSG, AppModuleMsg.PO_SIZEMAPPING_UPDATE)
								.putPOJO(CodeUtils.RESPONSE, sizes))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
						AppStatusMsg.BLANK);

			}
		} catch (SupplyMintException ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					ex.getMessage());
		} catch (MyBatisSystemException ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CONNECTION_ERROR_CODE,
					AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, AppStatusMsg.CONNECTION_NOT_AVAILABLE);

		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.PO_SIZE_MAPPING_UPDATE_ERROR,
					appStatus.getMessage(AppModuleErrors.PO_SIZE_MAPPING_UPDATE_ERROR));
			log.info(ex.getMessage());
		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			log.info(e.getMessage());
		} finally {
			log.info("PurchaseOrder updatesizemapping() method ends here....");
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "deprecation", "finally" })
	@RequestMapping(method = RequestMethod.GET, value = "/get/catdescudfname")
	@ResponseBody
	public ResponseEntity<AppResponse> getCatDescUDf(@RequestParam("type") Integer type,
			@RequestParam("pageNo") Integer pageNo, @RequestParam("search") String search,
			@RequestParam("header") String header) {
		AppResponse appResponse = null;
		ObjectNode node = getMapper().createObjectNode();
		ArrayNode arrNode = getMapper().createArrayNode();
		List<Map<String, String>> catDescUDFList = null;

		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;

		try {
			if (type == 1) {
				totalRecord = poService.countCatDescUdf(header);
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
				catDescUDFList = poService.findAllCatDescUdf(offset, pageSize, header);

			} else if (type == 3 && !search.isEmpty()) {

				totalRecord = poService.searchCatDescUDFCount(search, header);
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
				catDescUDFList = poService.searchCatDescUDFName(offset, pageSize, search, header);

			}

			if (catDescUDFList.size() > 0 && !catDescUDFList.isEmpty()) {

				AtomicInteger count = new AtomicInteger(1);
				catDescUDFList.stream().forEach(e -> {
					ObjectNode tempNode = getMapper().createObjectNode();
					tempNode.put("id", count.get());
					tempNode.put("code", e.get("CODE"));
					tempNode.put("header", e.get("DESCRIPTION"));
					tempNode.put("ext", e.get("EXT"));
					arrNode.add(tempNode);
					count.getAndIncrement();
				});
				node.put(CodeUtils.RESPONSE, arrNode);

				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
						.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG).put("currPage", pageNo)
						.put("prePage", previousPage).put("maxPage", maxPage).putAll(node))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			}
		} catch (MyBatisSystemException ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CONNECTION_ERROR_CODE,
					AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, AppStatusMsg.CONNECTION_NOT_AVAILABLE);

		} catch (DataAccessException ex) {

			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.GET_PO_ITEM_UDF_MAPPING_CAT_DESCRIPTION,
					appStatus.getMessage(AppModuleErrors.GET_PO_ITEM_UDF_MAPPING_CAT_DESCRIPTION));

			log.info(ex.getMessage());

		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			log.info(e.getMessage());
		} finally {

			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "deprecation", "finally" })
	@RequestMapping(method = RequestMethod.GET, value = "/get/article")
	@ResponseBody
	public ResponseEntity<AppResponse> getdepartmentItemUDFByHl3Name(@RequestParam("type") Integer type,
			@RequestParam("pageNo") Integer pageNo, @RequestParam("search") String search,
			@RequestParam(value = "hl3Code") String hl3Code, @RequestParam(value = "catDescUDF") String catDescUDF) {

		log.info("PurchaseOrder itemudfmapping article method starts here....");
		AppResponse appResponse = null;
		List<Map<String, String>> data = null;
		ObjectNode node = getMapper().createObjectNode();
		ArrayNode arrNode = getMapper().createArrayNode();
		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;

		try {
			if (type == 1) {
				totalRecord = poService.countArticleByDept(hl3Code, catDescUDF);
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
				data = poService.getArticleByDept(offset, pageSize, hl3Code, catDescUDF);

			} else if (type == 3 && !search.isEmpty()) {
				totalRecord = poService.searchArticleCount(search, hl3Code, catDescUDF);

				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;

				data = poService.searchArticle(offset, pageSize, search, hl3Code, catDescUDF);

			}

			if (!CodeUtils.isEmpty(data)) {
				ObjectNode dataNode = getArticleObjectNode(data, pageNo);

				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
						.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG).put("currPage", pageNo)
						.put("prePage", previousPage).put("maxPage", maxPage).putAll(dataNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

				log.info(AppStatusMsg.BLANK);
			}
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.ITEM_UDF_MAPPING_ARTICLE,
					appStatus.getMessage(AppModuleErrors.ITEM_UDF_MAPPING_ARTICLE));

			log.info(ex.getMessage());
		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			log.info(e.getMessage());
		} finally {
			log.info("PurchaseOrder itemudfmapping article method ends here....");
			return ResponseEntity.ok(appResponse);
		}
	}

	public ObjectNode getArticleObjectNode(List<Map<String, String>> articles, int pageNo) {
		ObjectNode node = getMapper().createObjectNode();
		ArrayNode arrNode = getMapper().createArrayNode();
		AtomicInteger count = new AtomicInteger(1);
		count.set(((pageNo - 1) * 10) + 1);
		articles.stream().forEach(e -> {
			ObjectNode tempNode = getMapper().createObjectNode();
			tempNode.put("hl4Code", e.get("HL4CODE"));
			tempNode.put("hl4Name", e.get("HL4NAME"));
			arrNode.add(tempNode);
			count.getAndIncrement();

		});

		node.put(CodeUtils.RESPONSE, arrNode);

		return node;
	}

	@SuppressWarnings({ "deprecation", "finally" })
	@RequestMapping(method = RequestMethod.GET, value = "/testpdf")
	@ResponseBody
	public ResponseEntity<AppResponse> getPDF() {
		String orderNo = "20190311/87227130";

		AppResponse appResponse = null;
		try {

		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.ITEM_UDF_MAPPING_ARTICLE,
					appStatus.getMessage(AppModuleErrors.ITEM_UDF_MAPPING_ARTICLE));

			log.info(appStatus.getMessage(AppModuleErrors.ITEM_UDF_MAPPING_ARTICLE));

		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			log.info(e.getMessage());
		} finally {
			log.info("PurchaseOrder itemudfmapping article method ends here....");
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "deprecated", "finally" })
	@RequestMapping(method = RequestMethod.GET, value = "/get/desc6")
	@ResponseBody
	public ResponseEntity<AppResponse> getDesc6Values(@RequestParam("pageNo") Integer pageNo,
			@RequestParam("type") Integer type, @RequestParam("search") String search,
			@RequestParam("articleCode") String articleCode, @RequestParam("mrpRangeFrom") String mrpRangeFrom,
			@RequestParam("mrpRangeTo") String mrpRangeTo) {

		AppResponse appResponse = null;
		List<String> data = null;
		ObjectNode node = getMapper().createObjectNode();
		ArrayNode arrNode = getMapper().createArrayNode();
		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;
		String tenantHashKey = "";

		try {
			tenantHashKey = ThreadLocalStorage.getTenantHashKey();
			if (type == 1) {

				totalRecord = this.countDesc6Values(articleCode, mrpRangeFrom, mrpRangeTo, tenantHashKey);
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
				data = this.getDesc6Values(articleCode, mrpRangeFrom, mrpRangeTo, offset, pageSize, tenantHashKey);
			} else if (type == 2) {
			}

			else {

				if (type == 3 && !CodeUtils.isEmpty(search)) {
					totalRecord = this.countSearchDesc6Values(articleCode, mrpRangeFrom, mrpRangeTo, search,
							tenantHashKey);
					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;

					data = this.getPOSearchDesc6Values(articleCode, mrpRangeFrom, mrpRangeTo, offset, pageSize, search,
							tenantHashKey);

				} else {
					throw new SupplyMintException("");
				}
			}

			AtomicInteger count = new AtomicInteger(1);
			count.set(((pageNo - 1) * 10) + 1);

			if (!CodeUtils.isEmpty(data)) {

				data.stream().forEach(e -> {
					ObjectNode tempItemNode = getMapper().createObjectNode();
					tempItemNode.put("id", count.get());
					tempItemNode.put(PurchaseIndentMetaData.Article.MRP, Double.valueOf(e));

					arrNode.add(tempItemNode);
					count.getAndIncrement();

				});

				node.put(CodeUtils.RESPONSE, arrNode);

				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode().put("currPage", pageNo)
						.put("prePage", previousPage).put("maxPage", maxPage)
						.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG).putAll(node))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				log.info(AppStatusMsg.BLANK);

			}
		} catch (DataAccessException ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.PO_DESC6_READER_ERROR, ClientErrorMessage.BAD_REQUEST);
		} catch (SupplyMintException ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.SEARCH_NOT_AVAILABLE_MSG);
		} catch (Exception e) {
			log.info(e.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);

		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	Integer countDesc6Values(String articleCode, String mrpRangeFrom, String mrpRangeTo, String tenantHashKey) {
		if (tenantHashKey.equalsIgnoreCase(TenantHash.valueOf("VMART").getHashKey()))
			return poService.getPOCountDesc6Values(articleCode, mrpRangeFrom, mrpRangeTo);
		else
			return poService.getOtherPOCountDesc6Values(articleCode, mrpRangeFrom, mrpRangeTo);

	}

	List<String> getDesc6Values(String articleCode, String mrpRangeFrom, String mrpRangeTo, int offset, int pagesize,
			String tenantHashKey) {
		if (tenantHashKey.equalsIgnoreCase(TenantHash.valueOf("VMART").getHashKey()))
			return poService.getPODesc6Values(articleCode, mrpRangeFrom, mrpRangeTo, offset, pagesize);
		else
			return poService.getOtherPODesc6Values(articleCode, mrpRangeFrom, mrpRangeTo, offset, pagesize);

	}

	public Integer countSearchDesc6Values(String hl4Code, String mrpRangeFrom, String mrpRangeTo, String search,
			String tenantHashKey) {
		if (tenantHashKey.equalsIgnoreCase(TenantHash.valueOf("VMART").getHashKey()))
			return poService.countSearchDesc6Values(hl4Code, mrpRangeFrom, mrpRangeTo, search);
		else
			return poService.countOtherSearchDesc6Values(hl4Code, mrpRangeFrom, mrpRangeTo, search);

	}

	List<String> getPOSearchDesc6Values(String hl4Code, String mrpRangeFrom, String mrpRangeTo, Integer offset,
			Integer pagesize, String search, String tenantHashKey) {
		if (tenantHashKey.equalsIgnoreCase(TenantHash.valueOf("VMART").getHashKey()))
			return poService.getPOAdhocSearchDesc6Values(hl4Code, mrpRangeFrom, mrpRangeTo, offset, pagesize, search);
		else
			return poService.getOtherPOAdhocSearchDesc6Values(hl4Code, mrpRangeFrom, mrpRangeTo, offset, pagesize,
					search);

	}

	@SuppressWarnings({ "deprecated", "finally" })
	@RequestMapping(method = RequestMethod.GET, value = "/get/department")
	@ResponseBody
	public ResponseEntity<AppResponse> getPODepartments(@RequestParam("pageNo") Integer pageNo,
			@RequestParam("type") Integer type, @RequestParam("search") String search) {

		AppResponse appResponse = null;
		List<Map<String, String>> data = null;
		ObjectNode node = getMapper().createObjectNode();
		ArrayNode arrNode = getMapper().createArrayNode();
		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;

		try {

			if (type == 1) {
				totalRecord = poService.countPOSavedDepartment();
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
				data = poService.getPOSavedDepartment(offset, pageSize);
			} else if (type == 2) {
			}

			else {

				if (type == 3 && !CodeUtils.isEmpty(search)) {
					totalRecord = poService.countPOSavedDepartmentsSearch(search);
					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
					data = poService.getPOSavedDepartmentSearch(offset, pageSize, search);
				}
			}

			AtomicInteger count = new AtomicInteger(1);
			count.set(((pageNo - 1) * 10) + 1);

			if (!CodeUtils.isEmpty(data) && !data.contains(null)) {

				data.stream().forEach(e -> {
					ObjectNode tempItemNode = getMapper().createObjectNode();
					tempItemNode.put("id", count.get());
					tempItemNode.put("hl3Code", e.get("HL3CODE"));
					tempItemNode.put("hl3Name", e.get("HL3NAME"));

					arrNode.add(tempItemNode);
					count.getAndIncrement();

				});

				node.put(CodeUtils.RESPONSE, arrNode);

				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode().put("currPage", pageNo)
						.put("prePage", previousPage).put("maxPage", maxPage)
						.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG).putAll(node))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				log.info(AppStatusMsg.BLANK);

			}
		} catch (DataAccessException ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.PO_DEPT_READER_ERROR, ClientErrorMessage.BAD_REQUEST);
		} catch (Exception e) {
			log.info(e.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);

		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "deprecated", "finally" })
	@RequestMapping(method = RequestMethod.GET, value = "/get/supplier")
	@ResponseBody
	public ResponseEntity<AppResponse> getPOSupplier(@RequestParam("pageNo") Integer pageNo,
			@RequestParam("type") Integer type, @RequestParam("search") String search,
			@RequestParam("hl3code") String hl3code) {

		AppResponse appResponse = null;
		List<Map<String, String>> data = null;
		ObjectNode node = getMapper().createObjectNode();
		ArrayNode arrNode = getMapper().createArrayNode();
		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;

		try {

			if (type == 1) {
				totalRecord = poService.countPOSavedSupplier(hl3code);
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
				data = poService.getPOSavedSupplier(offset, pageSize, hl3code);
			} else if (type == 2) {
			}

			else {

				if (type == 3 && !CodeUtils.isEmpty(search)) {
					totalRecord = poService.countPOSavedSupplierSearch(search, hl3code);
					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
					data = poService.getPOSavedSupplierSearch(offset, pageSize, search, hl3code);
				}
			}

			AtomicInteger count = new AtomicInteger(1);
			count.set(((pageNo - 1) * 10) + 1);

			if (!CodeUtils.isEmpty(data) && !data.contains(null)) {

				data.stream().forEach(e -> {
					ObjectNode tempItemNode = getMapper().createObjectNode();
					tempItemNode.put("id", count.get());
					tempItemNode.put("supplierCode", e.get("SUPPLIER_CODE"));
					tempItemNode.put("supplierName", e.get("SUPPLIER_NAME"));

					arrNode.add(tempItemNode);
					count.getAndIncrement();

				});

				node.put(CodeUtils.RESPONSE, arrNode);

				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode().put("currPage", pageNo)
						.put("prePage", previousPage).put("maxPage", maxPage)
						.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG).putAll(node))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				log.info(AppStatusMsg.BLANK);

			}
		} catch (DataAccessException ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.PO_SUPPLIER_READER_ERROR, ClientErrorMessage.BAD_REQUEST);
		} catch (Exception e) {
			log.info(e.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);

		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "deprecated", "finally" })
	@RequestMapping(method = RequestMethod.GET, value = "/get/orderno")
	@ResponseBody
	public ResponseEntity<AppResponse> getPOOrderNo(@RequestParam("pageNo") Integer pageNo,
			@RequestParam("type") Integer type, @RequestParam("search") String search,
			@RequestParam("hl3code") String hl3code, @RequestParam("supplierCode") String supplierCode) {

		AppResponse appResponse = null;
		List<Map<String, String>> data = null;
		ObjectNode node = getMapper().createObjectNode();
		ArrayNode arrNode = getMapper().createArrayNode();
		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;

		try {

			if (type == 1) {
				totalRecord = poService.countPOSavedOrderNo(hl3code, supplierCode);
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
				data = poService.getPOSavedOrderNo(offset, pageSize, hl3code, supplierCode);
			} else if (type == 2) {
			}

			else {

				if (type == 3 && !CodeUtils.isEmpty(search)) {
					totalRecord = poService.countPOSavedOrderNoSearch(search, hl3code, supplierCode);
					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
					data = poService.getPOSavedOrderNoSearch(offset, pageSize, search, hl3code, supplierCode);

				}
			}

			AtomicInteger count = new AtomicInteger(1);
			count.set(((pageNo - 1) * 10) + 1);

			if (!CodeUtils.isEmpty(data) && !data.contains(null)) {

				data.stream().forEach(e -> {
					ObjectNode tempItemNode = getMapper().createObjectNode();
					tempItemNode.put("id", count.get());
					tempItemNode.put("orderNo", e.get("ORDER_NO"));

					arrNode.add(tempItemNode);
					count.getAndIncrement();

				});

				node.put(CodeUtils.RESPONSE, arrNode);

				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode().put("currPage", pageNo)
						.put("prePage", previousPage).put("maxPage", maxPage)
						.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG).putAll(node))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				log.info(AppStatusMsg.BLANK);

			}
		} catch (DataAccessException ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.PO_SUPPLIER_READER_ERROR, ClientErrorMessage.BAD_REQUEST);
		} catch (Exception e) {
			log.info(e.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);

		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "deprecated", "finally" })
	@RequestMapping(method = RequestMethod.GET, value = "/get/setbased")
	@ResponseBody
	public ResponseEntity<AppResponse> getPOSetBased(@RequestParam("orderNo") String orderNo,
			@RequestParam("validTo") String validTo, @RequestParam("orgId") String orgId,
			@RequestParam("poType") String poType) {

		AppResponse appResponse = null;
		PurchaseOrderParent parentPO = null;

		String otbValue = "";

		List<PurchaseOrderChild> childPO = null;
		try {
			String tenantHashKey = ThreadLocalStorage.getTenantHashKey();
			parentPO = poService.getSetBasedPO(orderNo);

			if (!CodeUtils.isEmpty(parentPO)) {

				parentPO.setPoType(poType);
				if (tenantHashKey.equalsIgnoreCase(TenantHash.valueOf("VMART").getHashKey())) {
					childPO = poService.getSetBasedPOChild(orderNo);
				} else {
					childPO = poService.getSetBasedPOChildForOthers(orderNo);
				}

				if (!CodeUtils.isEmpty(childPO)) {

					log.info("valid to month : " + parentPO.getValidTo().getMonth().toString().substring(0, 3));
					log.info("valid to year :" + parentPO.getValidTo().getYear());

					ObjectNode otbRequest = getMapper().createObjectNode();

//					String month = parentPO.getValidTo().getMonth().toString().substring(0, 3);
//					int year = parentPO.getValidTo().getYear();
//					String monthYear = month + "-" + year;

					String monthYear = "";
					if (!CodeUtils.isEmpty(validTo)) {
						monthYear = validTo.substring(3).toUpperCase();
					} else {

						String month = parentPO.getValidTo().getMonth().toString().substring(0, 3);
						int year = parentPO.getValidTo().getYear();
						monthYear = month + "-" + year;
					}

					otbRequest.put("articleCode", parentPO.getHl4Code());
					otbRequest.put("monthYear", monthYear);

					for (PurchaseOrderChild po : childPO) {

						otbValue = "";
						otbRequest.put("desc6", "" + (int) po.getRsp());

						PurchaseOrderConfiguration poc = poService.getPOConfiguration(2);
						JsonNode headerJson = getMapper().readTree(poc.getHeaders());
						String headerType = getMapper().treeToValue(headerJson.get("headerType"), String.class);
						ResponseEntity<AppResponse> responseEntityData = null;
						responseEntityData = poService.sendPODetailsOnRestAPI(headerType, otbRequest, poc);

						if (!CodeUtils.isEmpty(responseEntityData)) {
							String responseEntityStatus = responseEntityData.getBody().getStatus();
							log.info("response entity message :"
									+ responseEntityData.getBody().getData().get(CodeUtils.RESPONSE_MSG));
							log.info("error code response entity message :"
									+ responseEntityData.getBody().getError().get("errorCode"));
							log.info("error message response entity message :"
									+ responseEntityData.getBody().getError().get("errorMessage"));
							log.info("response calling :" + responseEntityStatus);
							if (responseEntityStatus.contains(CodeUtils.RESPONSE_SUCCESS_CODE)) {
								log.info("response true");
								otbValue = responseEntityData.getBody().getData().get("otb").toString();
							}
						} else {
//							throw new SupplyMintException("");
//							@Author Prabhakar Srivastava Due To Gynesis Rest Template and make code generics
							if (tenantHashKey.equalsIgnoreCase(TenantHash.valueOf("VMART").getHashKey())) {
								throw new SupplyMintException("");
							} else {
								otbValue = DefaultParameter.OTB_VALUE;
							}

						}

						if (CodeUtils.isEmpty(otbValue) || otbValue == "null") {
							otbValue = "0";

						} else {
							if (otbValue.contains("\""))
								otbValue = otbValue.substring(1, otbValue.length() - 1);

						}

						childPO.get(childPO.indexOf(po)).setOtb(otbValue);
						childPO.get(childPO.indexOf(po)).setDesignWiseTotalOtb(otbValue);

						if (!CodeUtils.isEmpty(poType) && poType.equalsIgnoreCase("holdpo")) {
							if (!CodeUtils.isEmpty(po.getUdfString())) {
								childPO.get(childPO.indexOf(po))
										.setUdf(getMapper().readValue(po.getUdfString(), UDFMaster.class));
							}
							Map<String, String> descUDF = null;
							if (!CodeUtils.isEmpty(po.getUdfString())) {
								descUDF = poService.getHoldDescUDFData(po, parentPO.getHl3Code());
							}

							if (!CodeUtils.isEmpty(descUDF)) {
								childPO.get(childPO.indexOf(po)).setDesc2Name(descUDF.get("DESC2"));
								childPO.get(childPO.indexOf(po)).setDesc3Name(descUDF.get("DESC3"));
								childPO.get(childPO.indexOf(po)).setDesc4Name(descUDF.get("DESC4"));
								childPO.get(childPO.indexOf(po)).setDesc5Name(descUDF.get("DESC5"));
								childPO.get(childPO.indexOf(po)).getUdf().setItemudf1(descUDF.get("ITEMUDF1"));
								childPO.get(childPO.indexOf(po)).getUdf().setItemudf2(descUDF.get("ITEMUDF2"));
								childPO.get(childPO.indexOf(po)).getUdf().setItemudf3(descUDF.get("ITEMUDF3"));
								childPO.get(childPO.indexOf(po)).getUdf().setItemudf4(descUDF.get("ITEMUDF4"));
								childPO.get(childPO.indexOf(po)).getUdf().setItemudf5(descUDF.get("ITEMUDF5"));
								childPO.get(childPO.indexOf(po)).getUdf().setItemudf6(descUDF.get("ITEMUDF6"));
								childPO.get(childPO.indexOf(po)).getUdf().setItemudf7(descUDF.get("ITEMUDF7"));
								childPO.get(childPO.indexOf(po)).getUdf().setItemudf8(descUDF.get("ITEMUDF8"));
								childPO.get(childPO.indexOf(po)).getUdf().setItemudf9(descUDF.get("ITEMUDF9"));
								childPO.get(childPO.indexOf(po)).getUdf().setItemudf10(descUDF.get("ITEMUDF10"));
								childPO.get(childPO.indexOf(po)).getUdf().setItemudf11(descUDF.get("ITEMUDF11"));
								childPO.get(childPO.indexOf(po)).getUdf().setItemudf12(descUDF.get("ITEMUDF12"));
								childPO.get(childPO.indexOf(po)).getUdf().setItemudf13(descUDF.get("ITEMUDF13"));
								childPO.get(childPO.indexOf(po)).getUdf().setItemudf14(descUDF.get("ITEMUDF14"));
								childPO.get(childPO.indexOf(po)).getUdf().setItemudf15(descUDF.get("ITEMUDF15"));

							}

							Map<String, String> holdCategories = poService.getHoldCategoriesData(parentPO.getHl3Code(),
									po.getCat1Code(), po.getCat2Code(), po.getCat3Code(), po.getCat4Code());

							if (!CodeUtils.isEmpty(holdCategories)) {
								childPO.get(childPO.indexOf(po)).setCat1Name(holdCategories.get("CAT1"));
								childPO.get(childPO.indexOf(po)).setCat2Name(holdCategories.get("CAT2"));
								childPO.get(childPO.indexOf(po)).setCat3Name(holdCategories.get("CAT3"));
								childPO.get(childPO.indexOf(po)).setCat4Name(holdCategories.get("CAT4"));

								if (CodeUtils.isEmpty(holdCategories.get("CAT1")))
									childPO.get(childPO.indexOf(po)).setCat1Code(holdCategories.get("CAT1"));

								if (CodeUtils.isEmpty(holdCategories.get("CAT2")))
									childPO.get(childPO.indexOf(po)).setCat2Code(holdCategories.get("CAT2"));

								if (CodeUtils.isEmpty(holdCategories.get("CAT3")))
									childPO.get(childPO.indexOf(po)).setCat3Code(holdCategories.get("CAT3"));

								if (CodeUtils.isEmpty(holdCategories.get("CAT4")))
									childPO.get(childPO.indexOf(po)).setCat4Code(holdCategories.get("CAT4"));

							}

						}

					}

					if (!CodeUtils.isEmpty(poType) && poType.equalsIgnoreCase("holdpo")) {

						for (int i = 0; i < childPO.size(); i++) {

							List<Category> sizeList = getMapper().readValue(childPO.get(i).getSize(),
									getMapper().getTypeFactory().constructCollectionType(List.class, Category.class));

							List<Category> colorList = getMapper().readValue(childPO.get(i).getColor(),
									getMapper().getTypeFactory().constructCollectionType(List.class, Category.class));

							childPO.get(i).setColors(colorList);
							childPO.get(i).setSizes(sizeList);

							List<String> sizeColorList = new ArrayList<String>();
							for (Category cat : childPO.get(i).getSizes()) {
								sizeColorList.add(cat.getCode());
							}

							List<Category> sizeData = poService.getHoldSizeData(sizeColorList, parentPO.getHl3Code());

							if (!CodeUtils.isEmpty(sizeData)) {

								for (int j = 0; j < childPO.get(i).getSizes().size(); j++) {
									for (int k = 0; k < sizeData.size(); k++) {
										if (childPO.get(i).getSizes().get(j).getCname()
												.equals(sizeData.get(k).getCname())) {
											childPO.get(i).getSizes().get(j).setCname(sizeData.get(k).getCname());
											break;
										}
									}
								}
							}

							sizeColorList = null;
							sizeColorList = new ArrayList<String>();
							for (Category cat : childPO.get(i).getColors()) {
								sizeColorList.add(cat.getCode());
							}

							List<Category> colorData = poService.getHoldColorData(sizeColorList, parentPO.getHl3Code());

							if (!CodeUtils.isEmpty(colorData)) {
								for (int j = 0; j < childPO.get(i).getColors().size(); j++) {
									for (int k = 0; k < colorData.size(); k++) {
										if (childPO.get(i).getColors().get(j).getCname()
												.equals(colorData.get(k).getCname())) {
											childPO.get(i).getColors().get(j).setCname(colorData.get(k).getCname());
											break;
										}
									}
								}

							}
						}
					}

					parentPO.setPol(null);
					parentPO.setPol(new ArrayList<PurchaseOrderChild>());

					parentPO.setPol(childPO);

//					@Author Prabhakar Srivastava Start for checking site and udf existence for generics
					String udfExistenceStatus = piService.getUDFExistenceStatus(orgId);
					Map<String, Object> proMap = JsonUtils.toMap(JsonUtils.toJsonObject(udfExistenceStatus));
					String catDescModification = proMap.get("CAT_DESC_MODIFICATION").toString();
					String editDisplayName = proMap.get("EDIT_DISPLAY_NAME").toString();
					String otbValidation = proMap.get("OTB_VALIDATION").toString();

					ObjectNode poNode = buildOrderJson(parentPO);

					poNode.put("catDescModification", catDescModification);
					poNode.put("editDisplayName", editDisplayName);
					poNode.put("otbValidation", otbValidation);

					appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
							.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG).putAll(poNode))
							.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

					log.info("Loaded set based Purchase Order ");
				} else {
					appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
							AppStatusMsg.BLANK);
				}
			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
						AppStatusMsg.BLANK);
			}
		} catch (DataAccessException ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.PO_SUPPLIER_READER_ERROR, ClientErrorMessage.BAD_REQUEST);
		} catch (SupplyMintException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.PI_SMPARTNER_OTB_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.PI_SMPARTNER_OTB_READER_ERROR));
			log.info(appStatus.getMessage(AppModuleErrors.PI_SMPARTNER_OTB_READER_ERROR));
		} catch (Exception e) {
			log.info(e.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings("deprecation")
	ObjectNode buildOrderJson(PurchaseOrderParent po) throws JsonParseException, JsonMappingException, IOException {
		ArrayNode detailArrayNode = getMapper().createArrayNode();

		String token = request.getHeader("X-Auth-Token");
		JsonArray orgDetails = CodeUtils.decode(token).get("ORG_DETAILS").getAsJsonArray();
		JsonObject jNode = orgDetails.get(0).getAsJsonObject();
		String orgId = jNode.get("orgId").getAsString();

		ObjectNode piMainNode = getMapper().createObjectNode();
		ObjectNode poNode = getMapper().createObjectNode();
		poNode.put("orderNo", po.getOrderNo());
		poNode.put("orderDate", (!CodeUtils.isEmpty(po.getOrderDate()) ? po.getOrderDate().toString() : ""));
		poNode.put("validFrom", (!CodeUtils.isEmpty(po.getValidFrom()) ? po.getValidFrom().toString() : ""));
		poNode.put("validTo", (!CodeUtils.isEmpty(po.getValidTo()) ? po.getValidTo().toString() : ""));
		poNode.put("hl1Name", po.getHl1Name());
		poNode.put("hl1Code", po.getHl1Code());
		poNode.put("hl2Name", po.getHl2Name());
		poNode.put("hl2Code", po.getHl2Code());
		poNode.put("hl3Name", po.getHl3Name());
		poNode.put("hl3Code", po.getHl3Code());
		poNode.put("hl4Code", po.getHl4Code());
		poNode.put("hl4Name", po.getHl4Name());
		poNode.put("slCode", po.getSlCode());
		poNode.put("slName", po.getSlName());
		poNode.put("slCityName", po.getSlCityName());
		poNode.put("slAddr", po.getSlAddr());
		poNode.put("leadTime", po.getLeadTime());
		poNode.put("termCode", po.getTermCode());
		poNode.put("termName", po.getTermName());
		poNode.put("transporterCode", po.getTransporterCode());
		poNode.put("transporterName", po.getTransporterName());
		poNode.put("stateCode", po.getStateCode());
		poNode.put("mrpStart", po.getMrpStart());
		poNode.put("mrpEnd", po.getMrpEnd());
		poNode.put("poQuantity", po.getPoQuantity());
		poNode.put("poAmount", po.getPoAmount());
		poNode.put("hsnSacCode", po.getHsnSacCode());
		poNode.put("isHoldPO", po.getIsHoldPO());
		poNode.put("hsnCode", po.getHsnCode());
		poNode.put("isUDFExist", po.getIsUDFExist());
		poNode.put("siteCode", po.getSiteCode());
		poNode.put("siteName", po.getSiteName());
		poNode.put("isSiteExist", po.getIsSiteExist());

		log.info("detail size :" + po.getPol().size());

		for (int i = 0; i < po.getPol().size(); i++) {

			ArrayNode sizeArray = getMapper().createArrayNode();
			ArrayNode ratioArray = getMapper().createArrayNode();
			ArrayNode colorArray = getMapper().createArrayNode();
			ArrayNode finArray = getMapper().createArrayNode();
			ArrayNode imageArray = getMapper().createArrayNode();

			ArrayNode calculatedMarginArray = getMapper().createArrayNode();
			ArrayNode totalGSTArray = getMapper().createArrayNode();
			ArrayNode totalIntakeMarginArray = getMapper().createArrayNode();
			ArrayNode totalTaxArray = getMapper().createArrayNode();
			ArrayNode totalFinChargeArray = getMapper().createArrayNode();

			PurchaseOrderChild detail = po.getPol().get(i);

			ObjectNode detailNode = getMapper().createObjectNode();

			detailNode.put("setHeaderId", detail.getSetHeaderId());
			detailNode.put("design", detail.getDesign());
			detailNode.put("rate", detail.getRate());

			// discount Added
			detailNode.put("discountType", detail.getDiscountType());
			detailNode.put("discountValue", detail.getDiscountValue());
			detailNode.put("finalRate", detail.getFinalRate());
			//

			detailNode.put("qty", detail.getQty());
			detailNode.put("sumOfRatio", detail.getSumOfRatio());
			detailNode.put("noOfSets", detail.getNoOfSets());
			detailNode.put("rsp", detail.getRsp());
			detailNode.put("mrp", detail.getMrp());
			detailNode.put("typeOfBuying", detail.getTypeOfBuying());
			detailNode.put("marginRule", detail.getMarginRule());
			detailNode.put("totalAmount", detail.getTotalAmount());
			detailNode.put("tax", detail.getTax());
			detailNode.put("gst", detail.getGst());
			detailNode.put("otb", detail.getOtb());
			detailNode.put("netAmountTotal", detail.getNetAmountTotal());
			detailNode.put("calculatedMargin", detail.getCalculatedMargin());
			detailNode.put("intakeMargin", detail.getIntakeMargin());
			detailNode.put("deliveryDate",
					(!CodeUtils.isEmpty(detail.getDeliveryDate()) ? detail.getDeliveryDate().toString() : ""));
			detailNode.put("remarks", detail.getRemarks());

			detailNode.put("cat1Code", (!CodeUtils.isEmpty(detail.getCat1Code()) ? detail.getCat1Code() : ""));
			detailNode.put("cat1Name", (!CodeUtils.isEmpty(detail.getCat1Name()) ? detail.getCat1Name() : ""));
			detailNode.put("cat2Code", (!CodeUtils.isEmpty(detail.getCat2Code()) ? detail.getCat2Code() : ""));
			detailNode.put("cat2Name", (!CodeUtils.isEmpty(detail.getCat2Name()) ? detail.getCat2Name() : ""));
			detailNode.put("cat3Code", (!CodeUtils.isEmpty(detail.getCat3Code()) ? detail.getCat3Code() : ""));
			detailNode.put("cat3Name", (!CodeUtils.isEmpty(detail.getCat3Name()) ? detail.getCat3Name() : ""));
			detailNode.put("cat4Code", (!CodeUtils.isEmpty(detail.getCat4Code()) ? detail.getCat4Code() : ""));
			detailNode.put("cat4Name", (!CodeUtils.isEmpty(detail.getCat4Name()) ? detail.getCat4Name() : ""));
			detailNode.put("desc2Code", (!CodeUtils.isEmpty(detail.getDesc2Code()) ? detail.getDesc2Code() : ""));
			detailNode.put("desc2Name", (!CodeUtils.isEmpty(detail.getDesc2Name()) ? detail.getDesc2Name() : ""));
			detailNode.put("desc3Code", (!CodeUtils.isEmpty(detail.getDesc3Code()) ? detail.getDesc3Code() : ""));
			detailNode.put("desc3Name", (!CodeUtils.isEmpty(detail.getDesc3Name()) ? detail.getDesc3Name() : ""));
			detailNode.put("desc4Code", (!CodeUtils.isEmpty(detail.getDesc4Code()) ? detail.getDesc4Code() : ""));
			detailNode.put("desc4Name", (!CodeUtils.isEmpty(detail.getDesc4Name()) ? detail.getDesc4Name() : ""));
			detailNode.put("desc5Code", (!CodeUtils.isEmpty(detail.getDesc5Code()) ? detail.getDesc5Code() : ""));
			detailNode.put("desc5Name", (!CodeUtils.isEmpty(detail.getDesc5Name()) ? detail.getDesc5Name() : ""));

			ObjectNode udfNewNode = getMapper().createObjectNode();
			if (!CodeUtils.isEmpty(detail.getUdfString())) {
				UDFMaster udfMaster = null;
				if (po.getPoType().equalsIgnoreCase("holdpo")) {
					udfMaster = po.getPol().get(i).getUdf();
				} else {
					udfMaster = getMapper().readValue(detail.getUdfString(), UDFMaster.class);
				}
				if (!CodeUtils.isEmpty(udfMaster)) {
					udfNewNode.put("itemudf1",
							(!CodeUtils.isEmpty(udfMaster.getItemudf1()) ? udfMaster.getItemudf1() : ""));
					udfNewNode.put("itemudf2",
							(!CodeUtils.isEmpty(udfMaster.getItemudf2()) ? udfMaster.getItemudf2() : ""));
					udfNewNode.put("itemudf3",
							(!CodeUtils.isEmpty(udfMaster.getItemudf3()) ? udfMaster.getItemudf3() : ""));
					udfNewNode.put("itemudf4",
							(!CodeUtils.isEmpty(udfMaster.getItemudf4()) ? udfMaster.getItemudf4() : ""));
					udfNewNode.put("itemudf5",
							(!CodeUtils.isEmpty(udfMaster.getItemudf5()) ? udfMaster.getItemudf5() : ""));
					udfNewNode.put("itemudf6",
							(!CodeUtils.isEmpty(udfMaster.getItemudf6()) ? udfMaster.getItemudf6() : ""));
					udfNewNode.put("itemudf7",
							(!CodeUtils.isEmpty(udfMaster.getItemudf7()) ? udfMaster.getItemudf7() : ""));
					udfNewNode.put("itemudf8",
							(!CodeUtils.isEmpty(udfMaster.getItemudf8()) ? udfMaster.getItemudf8() : ""));
					udfNewNode.put("itemudf9",
							(!CodeUtils.isEmpty(udfMaster.getItemudf9()) ? udfMaster.getItemudf9() : ""));
					udfNewNode.put("itemudf10",
							(!CodeUtils.isEmpty(udfMaster.getItemudf10()) ? udfMaster.getItemudf10() : ""));
					udfNewNode.put("itemudf11",
							(!CodeUtils.isEmpty(udfMaster.getItemudf11()) ? udfMaster.getItemudf11() : ""));
					udfNewNode.put("itemudf12",
							(!CodeUtils.isEmpty(udfMaster.getItemudf12()) ? udfMaster.getItemudf12() : ""));
					udfNewNode.put("itemudf13",
							(!CodeUtils.isEmpty(udfMaster.getItemudf13()) ? udfMaster.getItemudf13() : ""));
					udfNewNode.put("itemudf14",
							(!CodeUtils.isEmpty(udfMaster.getItemudf14()) ? udfMaster.getItemudf14() : ""));
					udfNewNode.put("itemudf15",
							(!CodeUtils.isEmpty(udfMaster.getItemudf15()) ? udfMaster.getItemudf15() : ""));
					udfNewNode.put("itemudf16",
							(!CodeUtils.isEmpty(udfMaster.getItemudf16()) ? udfMaster.getItemudf16().toString() : ""));
					udfNewNode.put("itemudf17",
							(!CodeUtils.isEmpty(udfMaster.getItemudf17()) ? udfMaster.getItemudf17().toString() : ""));
					udfNewNode.put("itemudf18",
							(!CodeUtils.isEmpty(udfMaster.getItemudf18()) ? udfMaster.getItemudf18().toString() : ""));
					udfNewNode.put("itemudf19",
							(!CodeUtils.isEmpty(udfMaster.getItemudf19()) ? udfMaster.getItemudf19().toString() : ""));
					udfNewNode.put("itemudf20",
							(!CodeUtils.isEmpty(udfMaster.getItemudf20()) ? udfMaster.getItemudf20().toString() : ""));
					udfNewNode.put("udf1", (!CodeUtils.isEmpty(udfMaster.getUdf1()) ? udfMaster.getUdf1() : ""));
					udfNewNode.put("udf2", (!CodeUtils.isEmpty(udfMaster.getUdf2()) ? udfMaster.getUdf2() : ""));
					udfNewNode.put("udf3", (!CodeUtils.isEmpty(udfMaster.getUdf3()) ? udfMaster.getUdf3() : ""));
					udfNewNode.put("udf4", (!CodeUtils.isEmpty(udfMaster.getUdf4()) ? udfMaster.getUdf4() : ""));
					udfNewNode.put("udf5", (!CodeUtils.isEmpty(udfMaster.getUdf5()) ? udfMaster.getUdf5() : ""));
					udfNewNode.put("udf6", (!CodeUtils.isEmpty(udfMaster.getUdf6()) ? udfMaster.getUdf6() : ""));
					udfNewNode.put("udf7", (!CodeUtils.isEmpty(udfMaster.getUdf7()) ? udfMaster.getUdf7() : ""));
					udfNewNode.put("udf8", (!CodeUtils.isEmpty(udfMaster.getUdf8()) ? udfMaster.getUdf8() : ""));
					udfNewNode.put("udf9", (!CodeUtils.isEmpty(udfMaster.getUdf9()) ? udfMaster.getUdf9() : ""));
					udfNewNode.put("udf10", (!CodeUtils.isEmpty(udfMaster.getUdf10()) ? udfMaster.getUdf10() : ""));
					udfNewNode.put("udf11", (!CodeUtils.isEmpty(udfMaster.getUdf11()) ? udfMaster.getUdf11() : ""));
					udfNewNode.put("udf12", (!CodeUtils.isEmpty(udfMaster.getUdf12()) ? udfMaster.getUdf12() : ""));
					udfNewNode.put("udf13", (!CodeUtils.isEmpty(udfMaster.getUdf13()) ? udfMaster.getUdf13() : ""));
					udfNewNode.put("udf14", (!CodeUtils.isEmpty(udfMaster.getUdf14()) ? udfMaster.getUdf14() : ""));
					udfNewNode.put("udf15", (!CodeUtils.isEmpty(udfMaster.getUdf15()) ? udfMaster.getUdf15() : ""));
					udfNewNode.put("udf16",
							(!CodeUtils.isEmpty(udfMaster.getUdf16()) ? udfMaster.getUdf16().toString() : ""));
					udfNewNode.put("udf17",
							(!CodeUtils.isEmpty(udfMaster.getUdf17()) ? udfMaster.getUdf17().toString() : ""));
					udfNewNode.put("udf18",
							(!CodeUtils.isEmpty(udfMaster.getUdf18()) ? udfMaster.getUdf18().toString() : ""));
					udfNewNode.put("udf19",
							(!CodeUtils.isEmpty(udfMaster.getUdf19()) ? udfMaster.getUdf19().toString() : ""));
					udfNewNode.put("udf20",
							(!CodeUtils.isEmpty(udfMaster.getUdf20()) ? udfMaster.getUdf20().toString() : ""));

				}
			}

			detailNode.put("udf", udfNewNode);

			List<Category> sizeList = null;
			List<Category> colorList = null;
			List<RatioList> ratioList = null;
			List<String> totalTaxList = null;
			List<String> totalCalculatedMarginList = null;
			List<String> totalGSTList = null;
			List<String> totalIntakeMargin = null;
			List<FinCharge> totalFinCharge = null;

			try {
				if (po.getPoType().equalsIgnoreCase("holdpo")) {
					sizeList = po.getPol().get(i).getSizes();
					colorList = po.getPol().get(i).getColors();
				} else {
					sizeList = getMapper().readValue(detail.getSize(),
							getMapper().getTypeFactory().constructCollectionType(List.class, Category.class));

					colorList = getMapper().readValue(detail.getColor(),
							getMapper().getTypeFactory().constructCollectionType(List.class, Category.class));
				}

				ratioList = getMapper().readValue(detail.getRatio(),
						getMapper().getTypeFactory().constructCollectionType(List.class, RatioList.class));

				totalTaxList = getMapper().readValue(detail.getDesignWiseTotalTaxString(),
						getMapper().getTypeFactory().constructCollectionType(List.class, String.class));

				totalCalculatedMarginList = getMapper().readValue(detail.getDesignWiseTotalCalculatedMargingString(),
						getMapper().getTypeFactory().constructCollectionType(List.class, String.class));

				totalGSTList = getMapper().readValue(detail.getDesignWiseTotalGSTString(),
						getMapper().getTypeFactory().constructCollectionType(List.class, String.class));

				totalIntakeMargin = getMapper().readValue(detail.getDesignWiseTotalIntakeMarginString(),
						getMapper().getTypeFactory().constructCollectionType(List.class, String.class));

				totalFinCharge = getMapper().readValue(detail.getDesignWiseTotalFinChargesString(),
						getMapper().getTypeFactory().constructCollectionType(List.class, FinCharge.class));

			} catch (IOException e) {
				e.printStackTrace();
			}
			sizeList.forEach(e -> {
				ObjectNode node = getMapper().valueToTree(e);
				sizeArray.add(node);
			});

			colorList.forEach(e -> {
				ObjectNode node = getMapper().valueToTree(e);
				colorArray.add(node);
			});

			ratioList.forEach(e -> {
				ObjectNode node = getMapper().valueToTree(e);
				ratioArray.add(node);
			});

			totalTaxList.forEach(e -> {
//				ObjectNode node = getMapper().valueToTree(e);
				totalTaxArray.add(e);
			});

			totalCalculatedMarginList.forEach(e -> {
//				ObjectNode node = getMapper().valueToTree(e);
				calculatedMarginArray.add(e);
			});

			totalGSTList.forEach(e -> {
//				ObjectNode node = getMapper().valueToTree(e);
				totalGSTArray.add(e);
			});

			totalIntakeMargin.forEach(e -> {
//				ObjectNode node = getMapper().valueToTree(e);
				totalIntakeMarginArray.add(e);
			});

			/*
			 * if (!CodeUtils.isEmpty(totalFinCharge)) { totalFinCharge.forEach(e -> {
			 * ObjectNode node = getMapper().valueToTree(e); totalFinChargeArray.add(node);
			 * }); }
			 */

			try {
				if (!CodeUtils.isEmpty(detail.getFinCharge()) && !CodeUtils.isEmpty(totalFinCharge))
					finArray = buildFinChargeNode(detail.getFinCharge());
				totalFinChargeArray = buildFinChargeNode(totalFinCharge);

			} catch (IOException e1) {
				e1.printStackTrace();
			}

			List<String> imagesArray = new ArrayList<String>();

			if (!CodeUtils.isEmpty(detail.getImage1()))
				imagesArray.add(detail.getImage1());
			else if (!CodeUtils.isEmpty(detail.getImage1()))
				imagesArray.add(detail.getImage2());
			else if (!CodeUtils.isEmpty(detail.getImage3()))
				imagesArray.add(detail.getImage3());
			else if (!CodeUtils.isEmpty(detail.getImage4()))
				imagesArray.add(detail.getImage4());
			else if (!CodeUtils.isEmpty(detail.getImage5()))
				imagesArray.add(detail.getImage5());

			imagesArray.stream().forEach(e -> {
				imageArray.add(e);
			});

			detailNode.put("sizes", sizeArray);
			detailNode.put("ratios", ratioArray);
			detailNode.put("colors", colorArray);
			detailNode.put("finCharge", finArray);
			detailNode.put("images", imageArray);
			detailNode.put("containsImage",
					(!CodeUtils.isEmpty(detail.getContainsImage()) ? detail.getContainsImage() : ""));

			detailNode.put("designWiseTotalTax", totalTaxArray);
			detailNode.put("designWiseTotalCalculatedMargin", calculatedMarginArray);
			detailNode.put("designWiseTotalGST", totalGSTArray);
			detailNode.put("designWiseTotalIntakeMargin", totalIntakeMarginArray);
			detailNode.put("designWiseTotalFinCharges", totalFinChargeArray);

			detailNode.put("designWiseRowId", detail.getDesignWiseRowId());
			detailNode.put("designWiseTotalQty", detail.getDesignWiseTotalQty());
			detailNode.put("designWiseNetAmountTotal", detail.getDesignWiseNetAmountTotal());

			detailNode.put("designWiseTotalBasic", detail.getDesignWiseTotalBasic());
			detailNode.put("designWiseTotalOtb", detail.getDesignWiseTotalOtb());

			detailArrayNode.add(detailNode);
		}

		poNode.put("pol", detailArrayNode);

//		piMainNode.put("piKey", poNode);

		String customPO = piService.getSystemDefaultConfig("CUSTOM_PO", orgId);
		Map<String, Object> customProMap = null;
		try {
			customProMap = JsonUtils.toMap(JsonUtils.toJsonObject(customPO));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		String copyColor = customProMap.get("copyColor").toString();

		poNode.put("copyColor", copyColor);

		ObjectNode mainNode = getMapper().createObjectNode();
		mainNode.put(CodeUtils.RESPONSE, poNode);

		return mainNode;

	}

	@SuppressWarnings({ "finally", "deprecation", "unchecked", "rawtypes" })
	@RequestMapping(value = "/upload/data", method = RequestMethod.POST, consumes = {
			MediaType.MULTIPART_FORM_DATA_VALUE })
	@ResponseBody
	public ResponseEntity<AppResponse> uploadProcurementData(@RequestParam("fileData") MultipartFile fileData,
			@RequestParam("type") String type, HttpServletRequest request) {

		AppResponse appResponse = null;

		String token = request.getHeader("X-Auth-Token");
		tenantHashkey = ThreadLocalStorage.getTenantHashKey();
		try {
			JsonArray jOrgDetail = CodeUtils.decode(token).get("ORG_DETAILS").getAsJsonArray();
			String orgId = jOrgDetail.get(0).getAsJsonObject().get("orgId").getAsString();
			String orgName = jOrgDetail.get(0).getAsJsonObject().get("orgName").getAsString();
			String userName = CodeUtils.decode(token).get("prn").getAsString();

			String fileName = fileData.getOriginalFilename();
			OffsetDateTime currentTime = OffsetDateTime.now();
			currentTime = currentTime.plusHours(5);
			currentTime = currentTime.plusMinutes(30);
			String name = fileData.getOriginalFilename();
			String uuId = UUID.randomUUID().toString();
			String ipAddress = InetAddress.getLocalHost().getHostAddress().trim().toString();
			ProcurementUploadSummary summary = new ProcurementUploadSummary();
			summary.setUuId(uuId);
			summary.setProcurementType(type);
			summary.setLastSummary("TRUE");
			summary.setFileName(name);
			summary.setUploadStatus(Status.INPROGRESS.toString());
			summary.setUserName(userName);
			summary.setOrgId(orgId);
			summary.setOrgName(orgName);
			summary.setStatus("Active");
			summary.setActive("1");
			summary.setCreatedTime(currentTime);
			summary.setIpAddress(ipAddress);
			poService.insertUploadSummary(summary);
			int totalRec = poService.getUploadSummaryCount();

			if (totalRec != 0)
				poService.updateLastRecord(userName, uuId);

			InputStream inputStream = fileData.getInputStream();

			ExecutorService executorService = Executors.newSingleThreadExecutor();
			executorService.execute(new Runnable() {
				@Override
				public void run() {
					ThreadLocalStorage.setTenantHashKey(tenantHashkey);
					Map<String, String> map = null;
					String url = null;
					String userFileName = null;
					try {

						if (CodeUtils.getExtensionByApacheCommonLib(fileName).equalsIgnoreCase("XLS")
								|| CodeUtils.getExtensionByApacheCommonLib(fileName).equalsIgnoreCase("XLSX")) {
							userFileName = fileName.substring(0, fileName.lastIndexOf(".") + 1) + "csv";
						} else {
							userFileName = fileName;
						}

						map = poService.uploadFileOnS3(inputStream, userName, fileName, type, orgId);
						url = map.get("bucketKey");

						String bucketName = map.get("bucketName");
						String bucketPathWithFileName = map.get("bucketPath") + "/" + userFileName;
						S3Object s3Object = null;
						s3Object = amazonS3Client.getObjectRequest(bucketName, bucketPathWithFileName);

						int dataLength = Integer.parseInt(map.get("totalHeaders"));
						BufferedReader bufferedReader = new BufferedReader(
								new InputStreamReader(s3Object.getObjectContent()));

//						poService.insertDataFromFile(bufferedReader, dataLength, url, uuId, userName, orgId, bucketName,
//								map.get("bucketPath"), token);

						poService.importDataFromFile(bufferedReader, dataLength, url, uuId, userName, orgId, bucketName,
								map.get("bucketPath"), token);

					} catch (Exception e) {
						log.debug(String.format("Error Occoured From Exception : %s--", e));

					}
				}
			});

			ObjectNode objectNode = getMapper().createObjectNode();

			objectNode.put("id", uuId);
			objectNode.put("frequency", type);
			objectNode.put("userName", userName);
			ObjectNode node = getMapper().createObjectNode();
			node.put(CodeUtils.RESPONSE, objectNode);

			appResponse = new AppResponse.Builder().data(
					getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, AppStatusMsg.FILE_UPLOAD).putAll(node))
					.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

		} catch (CannotGetJdbcConnectionException ex) {
			log.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.PO_HISTORY,
					AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			log.debug(String.format("Error Occoured From DataAccessException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.DEMAND_BUDGETED_SALES_CREATOR_ERROR,
					appStatus.getMessage(AppModuleErrors.PO_HISTORY));

		} catch (Exception ex) {
			log.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "deprecated", "finally" })
	@RequestMapping(method = RequestMethod.GET, value = "/get/itembarcode")
	@ResponseBody
	public ResponseEntity<AppResponse> getItemBarcode(@RequestParam("type") Integer type,
			@RequestParam("pageNo") Integer pageNo, @RequestParam("search") String search,
			@RequestParam("articleCode") String articleCode, @RequestParam("supplierCode") String supplierCode,
			@RequestParam("siteId") String siteId) {

		AppResponse appResponse = null;
		List<Map<String, String>> data = null;
		ObjectNode itemParameterNode = getMapper().createObjectNode();
		ObjectNode itemNode = null;

		try {

			itemParameterNode.put("type", "" + type);
			itemParameterNode.put("pageNo", "" + pageNo);
			itemParameterNode.put("search", "" + search);
			itemParameterNode.put("articleCode", articleCode);
			itemParameterNode.put("supplierCode", supplierCode);
			itemParameterNode.put("siteId", siteId);

			itemNode = (ObjectNode) poService.getItemBarcode(itemParameterNode);

			if (!CodeUtils.isEmpty(itemNode) && itemNode.size() > 0) {

				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode().putAll(itemNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				log.info(AppStatusMsg.BLANK);

			}
		} catch (DataAccessException ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.PO_SUPPLIER_READER_ERROR, ClientErrorMessage.BAD_REQUEST);
		} catch (Exception e) {
			log.info(e.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);

		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "deprecated", "finally" })
	@RequestMapping(method = RequestMethod.POST, value = "/find/itembarcode/detail")
	@ResponseBody
	public ResponseEntity<AppResponse> getItemBarcodeDetail(@RequestBody ObjectNode itemRequestNode) {

		AppResponse appResponse = null;
		List<Map<String, String>> data = null;
		ObjectNode node = getMapper().createObjectNode();
		ObjectNode itemNode = null;

		try {

			itemNode = (ObjectNode) poService.getItemBarcodeDetail(itemRequestNode);

			if (!CodeUtils.isEmpty(itemNode) && itemNode.size() > 0) {
				node.put(CodeUtils.RESPONSE, itemNode);
				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
						.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG).putAll(node))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				log.info(AppStatusMsg.BLANK);

			}
		} catch (DataAccessException ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.PO_SUPPLIER_READER_ERROR, ClientErrorMessage.BAD_REQUEST);
		} catch (Exception e) {
			log.info(e.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);

		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally" })
	@RequestMapping(method = RequestMethod.GET, value = "/get/availablekeys")
	@ResponseBody
	public ResponseEntity<AppResponse> getAvailableButton(@RequestParam("orgId") String orgId) {

		log.info("PurchaseOrder present keys starts here....");
		AppResponse appResponse = null;
		ObjectNode data = null;

		try {

			data = poService.getAvailablePOButtonKeys(orgId);

			if (!CodeUtils.isEmpty(data)) {

				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
						.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG).putAll(data))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				log.info(AppStatusMsg.BLANK);
			}

		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.INDT_CAT_DESC_UDF_LIST,
					appStatus.getMessage(AppModuleErrors.INDT_CAT_DESC_UDF_LIST));

			log.info(ex.getMessage());

		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			log.info(e.getMessage());
		} finally {
			log.info("PurchaseOrder getDeptItemUDFData() method ends here....");
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally", "unused", "deprecation" })
	@RequestMapping(value = "/fmcg/get/history", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getFMCGHistory(@RequestParam("pageNo") Integer pageNo,
			@RequestParam("type") Integer type, @RequestParam("search") String search,
			@RequestParam("uploadedDate") String uploadedDate, @RequestParam("fileName") String fileName,
			@RequestParam("fileRecord") String fileRecord, @RequestParam("status") String status,
			HttpServletRequest request) {

		AppResponse appResponse = null;

		try {

			String token = request.getHeader("X-Auth-Token");
			JsonArray jOrgDetail = CodeUtils.decode(token).get("ORG_DETAILS").getAsJsonArray();
			String orgId = jOrgDetail.get(0).getAsJsonObject().get("orgId").getAsString();

			Map<String, Object> data = poService.getAllFCGHistory(pageNo, type, search, uploadedDate, fileName,
					fileRecord, status, orgId);

			ArrayNode array = (ArrayNode) data.get("data");
			if (array.size() != 0) {
				ObjectNode objectNode = getMapper().createObjectNode();
				objectNode.put(CodeUtils.RESPONSE, array);
				int previousPage = (int) data.get("prePage");
				int maxPage = (int) data.get("maxPage");

				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").put("currPage", pageNo)
								.put("prePage", previousPage).put("maxPage", maxPage).putAll(objectNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}
		}

		catch (CannotGetJdbcConnectionException ex) {
			log.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.FMCG_READER_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			log.debug(String.format("Error Occoured From DataAccessException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.FMCG_READER_ERROR, appStatus.getMessage(AppModuleErrors.FMCG_READER_ERROR));

		} catch (Exception ex) {
			log.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally" })
	@RequestMapping(method = RequestMethod.GET, value = "/get/last/status")
	@ResponseBody
	public ResponseEntity<AppResponse> getLastRecordStatus(HttpServletRequest request) {

		log.info("PurchaseOrder present keys starts here....");
		AppResponse appResponse = null;
		ProcurementUploadSummary data = null;

		try {

			String token = request.getHeader("X-Auth-Token");
			String userName = CodeUtils.decode(token).get("prn").getAsString();
			JsonArray jOrgDetail = CodeUtils.decode(token).get("ORG_DETAILS").getAsJsonArray();
			String orgId = jOrgDetail.get(0).getAsJsonObject().get("orgId").getAsString();

			data = poService.getLastRecord(userName, orgId);

			if (!CodeUtils.isEmpty(data)) {

				ObjectNode node = getMapper().createObjectNode();

				node.put("id", data.getUuId());
				node.put("fileName", data.getFileName());
				node.put("status", data.getUploadStatus());

				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, node)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				log.info(AppStatusMsg.BLANK);
			}

		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.FMCG_READER_ERROR, appStatus.getMessage(AppModuleErrors.FMCG_READER_ERROR));

			log.info(ex.getMessage());

		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			log.info(e.getMessage());
		} finally {
			log.info("PurchaseOrder getDeptItemUDFData() method ends here....");
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally" })
	@RequestMapping(value = "/export/fmcg/error/data/{uuId}", method = RequestMethod.GET)
	public ResponseEntity<AppResponse> exportFMCGData(@PathVariable(value = "uuId") String uuId,
			HttpServletRequest request) {
		AppResponse appResponse = null;
		String url = null;
		ProcurementUploadSummary uploadSummary = null;
		try {

			String token = request.getHeader("X-Auth-Token");
			String userName = CodeUtils.decode(token).get("prn").getAsString();
			JsonArray jOrgDetail = CodeUtils.decode(token).get("ORG_DETAILS").getAsJsonArray();
			String orgId = jOrgDetail.get(0).getAsJsonObject().get("orgId").getAsString();
			uploadSummary = poService.getSingleRecord(userName, orgId, uuId);

			if (!CodeUtils.isEmpty(uploadSummary)) {
				url = UploadUtils.downloadHistory(uploadSummary.getDownloadBucketKey(), "PURCHASE_ORDER", orgId);

				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, url)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}

		} catch (CannotGetJdbcConnectionException ex) {
			log.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.FMCG_DOWNLOAD_ERROR_FILE, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			log.debug(String.format("Error Occoured From DataAccessException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.FMCG_DOWNLOAD_ERROR_FILE,
					appStatus.getMessage(AppModuleErrors.FMCG_DOWNLOAD_ERROR_FILE));

		} catch (Exception ex) {
			log.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally" })
	@RequestMapping(method = RequestMethod.GET, value = "/repair/nullsize")
	@ResponseBody
	public ResponseEntity<AppResponse> repairNullSizes() {

		log.info("PurchaseOrder present keys starts here....");
		AppResponse appResponse = null;
		ObjectNode data = null;
		List<TempPurchaseOrderChild> tpocList = null;
		try {

			tpocList = poService.getNullSizeData();

			boolean result = poService.repairNullSizes(tpocList);

			if (!CodeUtils.isEmpty(data)) {

				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
						.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG).putAll(data))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}

		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.INDT_CAT_DESC_UDF_LIST,
					appStatus.getMessage(AppModuleErrors.INDT_CAT_DESC_UDF_LIST));

			log.info(ex.getMessage());

		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			log.info(e.getMessage());
		} finally {
			log.info("PurchaseOrder getDeptItemUDFData() method ends here....");
			return ResponseEntity.ok(appResponse);

		}
	}

	@SuppressWarnings({ "finally", "deprecation" })
	@RequestMapping(method = RequestMethod.POST, value = "/config/prodate")
	@ResponseBody
	public ResponseEntity<AppResponse> configProcurementDate(@RequestBody ObjectNode dateNode) {

		AppResponse appResponse = null;

		ObjectNode node = getMapper().createObjectNode();
		int result = 0;

		try {

			String token = request.getHeader("X-Auth-Token");
			JsonArray orgDetails = CodeUtils.decode(token).get("ORG_DETAILS").getAsJsonArray();
			JsonObject jNode = orgDetails.get(0).getAsJsonObject();
			String orgId = jNode.get("orgId").getAsString();
			result = poService.configProcurementDate(dateNode, orgId);

			if (result > 0) {
				node.put(CodeUtils.RESPONSE, dateNode);
				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
						.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG).putAll(node))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				log.info(AppStatusMsg.BLANK);

			}
		} catch (DataAccessException ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.PO_SUPPLIER_READER_ERROR, ClientErrorMessage.BAD_REQUEST);
		} catch (Exception e) {
			log.info(e.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);

		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally" })
	@RequestMapping(method = RequestMethod.GET, value = "/repair/nullcolor")
	@ResponseBody
	public ResponseEntity<AppResponse> repairNullColors() {

		log.info("PurchaseOrder present keys starts here....");
		AppResponse appResponse = null;
		ObjectNode data = null;
		List<TempPurchaseOrderChild> tpocList = null;
		try {

			tpocList = poService.getNullColorData();

			boolean result = poService.repairNullColors(tpocList);

			if (result == true) {

				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
						.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG).putAll(data))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.INDT_CAT_DESC_UDF_LIST,
					appStatus.getMessage(AppModuleErrors.INDT_CAT_DESC_UDF_LIST));

			log.info(ex.getMessage());

		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			log.info(e.getMessage());
		} finally {
			log.info("PurchaseOrder getDeptItemUDFData() method ends here....");
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "deprecated", "finally", "deprecation" })
	@RequestMapping(method = RequestMethod.GET, value = "/get/cities")
	@ResponseBody
	public ResponseEntity<AppResponse> getAllCitiesDetail(@RequestParam("pageNo") Integer pageNo,
			@RequestParam("type") Integer type, @RequestParam("search") String search) {

		AppResponse appResponse = null;
		List<String> data = null;
//		ObjectNode data = getMapper().createObjectNode();
		ObjectNode node = getMapper().createObjectNode();
		ArrayNode arrNode = getMapper().createArrayNode();
		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;

		try {

			if (type == 1) {
				totalRecord = poService.countTotalDistinctCities();
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
				data = poService.getAllCitiesDetails(offset, pageSize);
			} else if (type == 2) {
			}

			else {

				if (type == 3 && !CodeUtils.isEmpty(search)) {
					totalRecord = poService.countAllSearchedCities(search);
					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
					data = poService.getAllSearchedCities(offset, pageSize, search);
				}
			}

//			AtomicInteger count = new AtomicInteger(1);
//			count.set(((pageNo - 1) * 10) + 1);
//
			if (!CodeUtils.isEmpty(data)) {
//
//				data.stream().forEach(e -> {
//					ObjectNode tempItemNode = getMapper().createObjectNode();
//					tempItemNode.put("id", count.get());
//					tempItemNode.put("hl3Code", e.get("HL3CODE"));
//					tempItemNode.put("hl3Name", e.get("HL3NAME"));
//
//					arrNode.add(tempItemNode);
//					count.getAndIncrement();
//
//				});

				node.putPOJO(CodeUtils.RESPONSE, data);

				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode().put("currPage", pageNo)
						.put("prePage", previousPage).put("maxPage", maxPage)
						.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG).putAll(node))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				log.info(AppStatusMsg.BLANK);
			}
		} catch (DataAccessException ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.PO_DEPT_READER_ERROR, ClientErrorMessage.BAD_REQUEST);
		} catch (Exception e) {
			log.info(e.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);

		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally" })
	@RequestMapping(method = RequestMethod.GET, value = "/get/discount")
	@ResponseBody
	public ResponseEntity<AppResponse> getPODiscount(@RequestParam("articleCode") String articleCode,
			@RequestParam("mrp") String mrp, @RequestParam("discountType") String discountType) {

		log.info("PurchaseOrder discount starts here....");
		AppResponse appResponse = null;
		List<Map<String, String>> poDiscountMap = null;
		ArrayNode arrNode = getMapper().createArrayNode();
		try {
			poDiscountMap = poService.getPODiscount(articleCode, mrp, discountType);

			if (!CodeUtils.isEmpty(poDiscountMap)) {

			}

			AtomicInteger count = new AtomicInteger(1);

			if (!CodeUtils.isEmpty(poDiscountMap) && !poDiscountMap.contains(null)) {

				poDiscountMap.stream().forEach(e -> {
					ObjectNode tempItemNode = getMapper().createObjectNode();
					tempItemNode.put("id", count.get());
					tempItemNode.put("discount", e.get("DISCOUNT"));
					tempItemNode.put("discountValue", e.get("DISCOUNT_VALUE"));
					arrNode.add(tempItemNode);
					count.getAndIncrement();

				});

				ObjectNode mainNode = getMapper().createObjectNode();
				mainNode.put(CodeUtils.RESPONSE, arrNode);
				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
						.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG).putAll(mainNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				log.info(AppStatusMsg.BLANK);
			}
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.INDT_CAT_DESC_UDF_LIST,
					appStatus.getMessage(AppModuleErrors.INDT_CAT_DESC_UDF_LIST));

			log.info(ex.getMessage());

		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			log.info(e.getMessage());
		} finally {
			log.info("PurchaseOrder getDeptItemUDFData() method ends here....");
			return ResponseEntity.ok(appResponse);
		}
	}

}
