package com.supplymint.layer.web.endpoint.setting;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.CannotGetJdbcConnectionException;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.supplymint.config.application.AppCodeConfig;
import com.supplymint.exception.StatusCodes.AppModuleErrors;
import com.supplymint.exception.StatusCodes.AppStatusCodes;
import com.supplymint.exception.StatusCodes.AppStatusMsg;
import com.supplymint.exception.StatusCodes.ClientErrors;
import com.supplymint.layer.business.contract.setting.OTBSettingService;
import com.supplymint.layer.web.endpoint.abstracts.AbstractEndpoint;
import com.supplymint.layer.web.model.AppResponse;
import com.supplymint.util.CodeUtils;

/**
 * This Controller describes all services for OTB and HLEVEL Configuration
 * @author Prabhakar Srivastava 
 * @author Varun Sharma
 * @Date   10 May 2019
 * @version 1.0
 */
@RestController
@RequestMapping(path = OTBSettingEndpoint.PATH, produces = { MediaType.APPLICATION_JSON_VALUE })
public class OTBSettingEndpoint extends AbstractEndpoint {

	public static final String PATH = "setting/otb";

	private static final Logger LOGGER = LoggerFactory.getLogger(OTBSettingEndpoint.class);

	private OTBSettingService otbSettingService;

	@Autowired
	private AppCodeConfig appStatus;

	@Autowired
	public OTBSettingEndpoint(OTBSettingService otbSettingService) {
		this.otbSettingService = otbSettingService;
	}

	/**
	 * 
	 * @param Hlevel
	 * @return all HLEVEL list from table SYSTEM_DEFAULT_CONFIG
	 */
	@SuppressWarnings({ "deprecation", "finally" })
	@RequestMapping(value = "/get/hlevel", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getAllHLevel(@RequestParam("HLEVEL") String hlevel) throws Exception {

		AppResponse appResponse = null;
		try {
			LOGGER.debug("Query,for getting all HLEVEL,is going to start...");
			ObjectNode data = otbSettingService.getHLevel(hlevel);
			LOGGER.debug("Query,for gettting all HLEVEL,has ended...");
			if (!CodeUtils.isEmpty(data) || !CodeUtils.isEmpty(data.get(CodeUtils.RESPONSE))) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(data))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}
		} catch (DataAccessException ex) {
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.OTB_SETTING_READER_ERROR, appStatus.getMessage(AppModuleErrors.OTB_SETTING_READER_ERROR));
		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	/**
	 * 
	 * @param node
	 * @return create OTB assortment
	 */
	@SuppressWarnings("finally")
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> createConfiguration(@RequestBody JsonNode otbSetting) {

		AppResponse appResponse = null;

		int result = 0;
		try {
			LOGGER.debug("Query,for creating assortment,is going to start...");
			result = otbSettingService.create(otbSetting);
			LOGGER.debug("Query,for creating assortment,has ended...");
			if (result != 0) {
				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
						.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.CONFIG_SUCCESS_MSG).putPOJO(CodeUtils.RESPONSE, ""))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
						AppModuleErrors.OTB_SETTING_CREATOR_ERROR,
						appStatus.getMessage(AppModuleErrors.OTB_SETTING_CREATOR_ERROR));
			}
		} catch (CannotGetJdbcConnectionException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.OTB_SETTING_CREATOR_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.OTB_SETTING_CREATOR_ERROR,
					appStatus.getMessage(AppModuleErrors.OTB_SETTING_CREATOR_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	/**
	 * This API is used to get all DEFAULT_ASSORTMENT from table
	 * SYSTEM_DEFAULT_CONFIG
	 * 
	 * @return
	 */
	@SuppressWarnings({ "deprecation", "finally" })
	@RequestMapping(value = "/get/default", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getDefaultAssortment() throws Exception{

		AppResponse appResponse = null;
		try {
			LOGGER.debug("Query,for getting data,is going to start...");
			ObjectNode data = otbSettingService.getDefaultAssort();
			LOGGER.debug("Query,for getting data,has ended...");
			if (!CodeUtils.isEmpty(data.get(CodeUtils.RESPONSE))) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(data))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}
		} catch (DataAccessException ex) {
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.OTB_SETTING_READER_ERROR, appStatus.getMessage(AppModuleErrors.OTB_SETTING_READER_ERROR));
		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally" })
	@RequestMapping(value = "/create/otbassort", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> createOTBAssortment(@RequestBody JsonNode node) throws Exception{

		AppResponse appResponse = null;
		int result = 0;
		try {
			LOGGER.debug("Query,for creating otb_assortment,is going to start...");
			result = otbSettingService.createAssortment(node);
			LOGGER.debug("Query,for creating otb_assortment,has ended...");
			if (result != 0) {
				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
						.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.OTB_SETTING_SUCCESS_MSG).putPOJO(CodeUtils.RESPONSE, ""))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
						AppModuleErrors.OTB_SETTING_CREATOR_ERROR,
						appStatus.getMessage(AppModuleErrors.OTB_SETTING_CREATOR_ERROR));
			}
		} catch (CannotGetJdbcConnectionException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.OTB_SETTING_CREATOR_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.OTB_SETTING_CREATOR_ERROR,
					appStatus.getMessage(AppModuleErrors.OTB_SETTING_CREATOR_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}
	
	@SuppressWarnings({ "deprecation", "finally" })
	@RequestMapping(value = "/get/all/config", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getAllHlevelConfig() throws Exception{

		AppResponse appResponse = null;
		try {
			LOGGER.debug("Query,for getting all config data,is going to start...");
			ObjectNode data = otbSettingService.getAllConfig();
			LOGGER.debug("Query,for getting all config data,has ended...");
			if (!CodeUtils.isEmpty(data.get(CodeUtils.RESPONSE))) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(data))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}
		} catch (DataAccessException ex) {
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.OTB_SETTING_READER_ERROR, appStatus.getMessage(AppModuleErrors.OTB_SETTING_READER_ERROR));
		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}
	
	@SuppressWarnings({ "deprecation", "finally" })
	@RequestMapping(value = "/get/all/otb", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getAllOTBConfig() throws Exception{

		AppResponse appResponse = null;
		try {
			LOGGER.debug("Query,for getting all config data,is going to start...");
			ObjectNode data = otbSettingService.getAllOTBConfig();
			LOGGER.debug("Query,for getting all config data,has ended...");
			if (!CodeUtils.isEmpty(data.get(CodeUtils.RESPONSE))) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(data))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}
		} catch (DataAccessException ex) {
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.OTB_SETTING_READER_ERROR, appStatus.getMessage(AppModuleErrors.OTB_SETTING_READER_ERROR));
		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}
}
