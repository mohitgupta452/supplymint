package com.supplymint.layer.web.endpoint.core;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.supplymint.exception.StatusCodes.AppErrorCodes;
import com.supplymint.exception.StatusCodes.AppException;
import com.supplymint.exception.StatusCodes.AppStatusCodes;
import com.supplymint.exception.StatusCodes.AppStatusMsg;
import com.supplymint.exception.StatusCodes.ClientErrorMessage;
import com.supplymint.exception.StatusCodes.ClientErrors;
import com.supplymint.exception.StatusCodes.ServerErrors;
import com.supplymint.exception.SupplyMintException;
import com.supplymint.layer.business.service.core.RoleService;
import com.supplymint.layer.data.core.entity.Role;
import com.supplymint.layer.web.endpoint.abstracts.AbstractEndpoint;
import com.supplymint.layer.web.model.AppResponse;
import com.supplymint.util.CodeUtils;

@RestController
@RequestMapping(path = RolesEndpoint.PATH, produces = { MediaType.APPLICATION_JSON_VALUE })
public class RolesEndpoint extends AbstractEndpoint{
	
	public static final String PATH = "core/role";
	private RoleService roleService;

	@Autowired
	public RolesEndpoint(RoleService roleService) {
		this.roleService = roleService;
	}
	
	@SuppressWarnings("finally")
	@RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getByIdentifierAndModule(@PathVariable(value = "id") Integer id) {

		AppResponse appResponse = null;
		try {

			Role data = roleService.findById(id);

			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode().putPOJO("resource", data)
						.put("message", AppStatusMsg.SUCCESS_MSG)).status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
						ClientErrorMessage.BAD_REQUEST);
			}
		} catch (NumberFormatException ex) {

			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.NUMBER_FORMAT_MSG);
			throw new SupplyMintException(AppException.NUMBER_FORMAT_EXCEPTION);

		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, ServerErrors.INTERNAL_SERVER_ERROR,
					AppStatusMsg.FAILURE_MSG);
			throw new SupplyMintException(AppStatusMsg.FAILURE_MSG);
		} finally {

			return ResponseEntity.ok(appResponse);
		}

	}

	@SuppressWarnings({ "deprecation", "finally" })
	@RequestMapping(value = "/get/all", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getAll() throws SupplyMintException {

		AppResponse appResponse = null;
		try {

			List<Role> data = roleService.roleByName();

			ObjectNode objectNode = getMapper().createObjectNode();
			ArrayNode array = getMapper().createArrayNode();
			data.forEach(e -> {
//				ObjectNode node = getMapper().valueToTree(e);
				ObjectNode node = getMapper().createObjectNode();
				
				node.put("id",e.getId() );
				node.put("code",e.getCode() );
				node.put("name", e.getName());
				array.add(node);
			});
			objectNode.put(CodeUtils.RESPONSE, array);

			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(objectNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = buildErrorResponse(AppErrorCodes.GENERIC_EVENT_NOT_AVAILABLE_ERROR_CODE, ClientErrors.NOT_FOUND,
						AppStatusMsg.BLANK);
			}

		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, ServerErrors.INTERNAL_SERVER_ERROR,
					AppStatusMsg.FAILURE_MSG);
			throw new SupplyMintException(AppStatusMsg.FAILURE_MSG);
		} finally {

			return ResponseEntity.ok(appResponse);
		}
	}

}
