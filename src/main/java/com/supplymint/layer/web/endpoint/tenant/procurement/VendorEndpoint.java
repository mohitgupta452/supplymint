package com.supplymint.layer.web.endpoint.tenant.procurement;

import java.net.InetAddress;
import java.time.OffsetDateTime;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.supplymint.config.application.AppCodeConfig;
import com.supplymint.exception.StatusCodes.AppErrorCodes;
import com.supplymint.exception.StatusCodes.AppModuleErrors;
import com.supplymint.exception.StatusCodes.AppStatusCodes;
import com.supplymint.exception.StatusCodes.AppStatusMsg;
import com.supplymint.exception.StatusCodes.ClientErrors;
import com.supplymint.exception.SupplyMintException;
import com.supplymint.layer.business.contract.procurement.VendorService;
import com.supplymint.layer.data.tenant.procurement.entity.Vendor;
import com.supplymint.layer.web.endpoint.abstracts.AbstractEndpoint;
import com.supplymint.layer.web.model.AppResponse;
import com.supplymint.util.CodeUtils;

@RestController
@RequestMapping(path = VendorEndpoint.PATH, produces = { MediaType.APPLICATION_JSON_VALUE })
public class VendorEndpoint extends AbstractEndpoint {

	public static final String PATH = "tenant/vendor";

	private static final Logger LOGGER = LoggerFactory.getLogger(VendorEndpoint.class);

	private VendorService vendorService;

	@Autowired
	private AppCodeConfig appStatus;
	
	@Autowired
	private HttpServletRequest request;

	@Autowired
	public VendorEndpoint(VendorService vendorService) {
		this.vendorService = vendorService;
	}

	@SuppressWarnings("finally")
	@RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getByIdentifierAndModule(@PathVariable(value = "id") String sid) {
		AppResponse appResponse = null;
		int id = 0;

		try {
			id = Integer.parseInt(sid);
			Vendor data = vendorService.getById(id);
			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, data)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}
			else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			LOGGER.info(AppStatusMsg.BLANK);
			}
		} catch (NumberFormatException ex) {

			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.NUMBER_FORMAT_MSG);

		} catch (DataAccessException ex) {

			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.VENDOR_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.VENDOR_READER_ERROR));
			LOGGER.info(appStatus.getMessage(AppModuleErrors.VENDOR_READER_ERROR));
		} catch (Exception ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			throw new SupplyMintException(AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "unchecked", "deprecation", "rawtypes", "finally" })
	@RequestMapping(value = "/get/all", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getAllSites(@RequestParam("pageno") Integer pageNo) {

		AppResponse appResponse = null;
		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;
		try {
			totalRecord = vendorService.record();
			maxPage = (totalRecord + pageSize - 1) / pageSize;
			previousPage = pageNo - 1;
			offset = (pageNo - 1) * pageSize;
			List data = vendorService.getAll(offset, pageSize);

			ObjectNode objectNode = getMapper().createObjectNode();
			ArrayNode array = getMapper().createArrayNode();
			data.forEach(e -> {
				ObjectNode node = getMapper().valueToTree(e);
				array.add(node);
			});
			objectNode.put(CodeUtils.RESPONSE, array);
			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put("currPage", pageNo).put("prePage", previousPage)
								.put("maxPage", maxPage).put(CodeUtils.RESPONSE_MSG, "").putAll(objectNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				LOGGER.info(AppStatusMsg.BLANK);
			}
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.VENDOR_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.VENDOR_READER_ERROR));

			LOGGER.info(appStatus.getMessage(AppModuleErrors.VENDOR_READER_ERROR));

		} catch (Exception ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			throw new SupplyMintException(AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings("finally")
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> saveSite(@RequestBody Vendor vendor) {

		AppResponse appResponse = null;
		int result = 0;
		try {
			int isExist = vendorService.isValid(vendor);
			if (isExist == 0) {
				OffsetDateTime creationTime = OffsetDateTime.now();
				vendor.setCreatedTime(creationTime);
				vendor.setCreatedBy(request.getAttribute("email").toString());
				String ipAddress = InetAddress.getLocalHost().getHostAddress().trim().toString();
				vendor.setIpAddress(ipAddress);
				vendor.setActive('1');
				result = vendorService.create(vendor);
				Vendor data = vendorService.getById(vendor.getId());
				if (result != 0) {
					appResponse = new AppResponse.Builder()
							.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, data)
									.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
							.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

				} else {
					appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
							AppModuleErrors.VENDOR_CREATOR_ERROR,
							appStatus.getMessage(AppModuleErrors.VENDOR_CREATOR_ERROR));
				}
			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
						AppStatusMsg.EXIST_VENDOR);
			}
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.VENDOR_CREATOR_ERROR,
					appStatus.getMessage(AppModuleErrors.VENDOR_CREATOR_ERROR));

			LOGGER.info(appStatus.getMessage(AppModuleErrors.VENDOR_CREATOR_ERROR));

		} catch (Exception ex) {
			ex.printStackTrace();
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			throw new SupplyMintException(AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings("finally")
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> updateSite(@RequestBody Vendor vendor) {

		AppResponse appResponse = null;
		int result = 0;
		try {
			OffsetDateTime updationTime = OffsetDateTime.now();
			vendor.setUpdationTime(updationTime);
			vendor.setUpdatedBy(request.getAttribute("email").toString());
			String ipAddress = InetAddress.getLocalHost().getHostAddress().trim().toString();
			vendor.setIpAddress(ipAddress);
			result = vendorService.update(vendor);
			Vendor data = vendorService.getById(vendor.getId());

			if (result != 0) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, data)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
						AppModuleErrors.VENDOR_UPDATE_ERROR, appStatus.getMessage(AppModuleErrors.VENDOR_UPDATE_ERROR));
			}

		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.VENDOR_UPDATE_ERROR,
					appStatus.getMessage(AppModuleErrors.VENDOR_UPDATE_ERROR));

			LOGGER.info(appStatus.getMessage(AppModuleErrors.VENDOR_UPDATE_ERROR));

		} catch (Exception ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			throw new SupplyMintException(AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings("finally")
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> deleteSite(@PathVariable(value = "id") String sid) {

		AppResponse appResponse = null;
		int id = 0;
		try {
			id = Integer.parseInt(sid);

			int result = vendorService.delete(id);
			if (result != 0) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
						AppModuleErrors.VENDOR_DELETE_ERROR, appStatus.getMessage(AppModuleErrors.VENDOR_DELETE_ERROR));
				LOGGER.info(appStatus.getMessage(AppModuleErrors.VENDOR_DELETE_ERROR));
			}
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.VENDOR_DELETE_ERROR,
					appStatus.getMessage(AppModuleErrors.VENDOR_DELETE_ERROR));

			LOGGER.info(appStatus.getMessage(AppModuleErrors.VENDOR_DELETE_ERROR));

		} catch (NumberFormatException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.NUMBER_FORMAT_MSG);

		} catch (Exception ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			throw new SupplyMintException(AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings("finally")
	@RequestMapping(value = "/get/vendorname/{name}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getVendorName(@PathVariable("name") String name) {

		AppResponse appResponse = null;
		try {
			Vendor data = vendorService.getVendorName(name);

			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, data)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = buildErrorResponse(AppErrorCodes.GENERIC_EVENT_NOT_AVAILABLE_ERROR_CODE, ClientErrors.NOT_FOUND,
						AppStatusMsg.BLANK);
				LOGGER.info(AppStatusMsg.BLANK);
			}
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.VENDOR_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.VENDOR_READER_ERROR));
			LOGGER.info(appStatus.getMessage(AppModuleErrors.VENDOR_READER_ERROR));
		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			throw new SupplyMintException(AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally", "rawtypes", "unchecked", "deprecation" })
	@RequestMapping(value = "/get/orgname/{name}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getByOrganisationName(@PathVariable("name") String name) {

		AppResponse appResponse = null;
		try {
			List data = vendorService.getByOrganisationName(name);

			if (!CodeUtils.isEmpty(data)) {

				ObjectNode objectNode = getMapper().createObjectNode();
				ArrayNode array = getMapper().createArrayNode();
				data.forEach(e -> {
					ObjectNode node = getMapper().valueToTree(e);
					array.add(node);
				});

				objectNode.put(CodeUtils.RESPONSE, array);
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(objectNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else
				appResponse = buildErrorResponse(AppErrorCodes.GENERIC_EVENT_NOT_AVAILABLE_ERROR_CODE, ClientErrors.NOT_FOUND,
						AppStatusMsg.BLANK);
			LOGGER.info(AppStatusMsg.BLANK);
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.VENDOR_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.VENDOR_READER_ERROR));
			LOGGER.info(appStatus.getMessage(AppModuleErrors.VENDOR_READER_ERROR));
		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			throw new SupplyMintException(AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings("finally")
	@RequestMapping(value = "/get/vendorcode/{code}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getVendorCode(@PathVariable("code") String code) {

		AppResponse appResponse = null;
		try {

			Vendor data = vendorService.getVendorCode(code);

			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, data)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = buildErrorResponse(AppErrorCodes.GENERIC_EVENT_NOT_AVAILABLE_ERROR_CODE, ClientErrors.NOT_FOUND,
						AppStatusMsg.BLANK);
				LOGGER.info(AppStatusMsg.BLANK);
			}
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.VENDOR_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.VENDOR_READER_ERROR));

			LOGGER.info(appStatus.getMessage(AppModuleErrors.VENDOR_READER_ERROR));

		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

}
