
package com.supplymint.layer.web.endpoint.aws;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.AmazonClientException;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.fasterxml.jackson.databind.JsonNode;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.supplymint.config.aws.config.AmazonS3Client;
import com.supplymint.config.aws.utils.AWSUtils;
import com.supplymint.config.aws.utils.AWSUtils.RunState;
import com.supplymint.exception.StatusCodes.AppStatusCodes;
import com.supplymint.exception.StatusCodes.AppStatusMsg;
import com.supplymint.exception.StatusCodes.ClientErrors;
import com.supplymint.layer.business.contract.inventory.JobDetailService;
import com.supplymint.layer.business.contract.s3Bucket.S3BucketInfoService;
import com.supplymint.layer.business.service.s3bucketService.S3WrapperService;
import com.supplymint.layer.data.tenant.inventory.entity.JobDetail;
import com.supplymint.layer.data.tenant.inventory.entity.S3BucketInfo;
import com.supplymint.layer.web.endpoint.abstracts.AbstractEndpoint;
import com.supplymint.layer.web.model.AppResponse;
import com.supplymint.util.CodeUtils;
import com.supplymint.util.FileUtils;

/**
 * @author Manoj Singh
 * @since 10 NOV 2018
 * @version 1.0
 */
@RestController
@RequestMapping(path = AmazonController.PATH, produces = { MediaType.APPLICATION_JSON_VALUE })
public class AmazonController extends AbstractEndpoint {

	private final static Logger LOGGER = LoggerFactory.getLogger(AmazonController.class);

	public static final String PATH = "aws/s3bucket";

	public static final String USER_ENTERPRISE = "eid";

	public static final String USER_BUCKET = "BUCKET";

	public static final String BUCKET = "com-supplymint-glue";

	public static final String BUCKETSCRIPTPATH = "_etl";

	public static final String OUTPUTFOLDER = "output";

	public static final String Bucket = "bucket";

	public static final String Template = "template";

	public static final String BucketKey = "bucketKey";

	public static final String Expiration = "expire";

	public static final String FileName = "fileName";

	public static final String FileType = "fileType";

	public static final String Ecode = "ent_code";

	public static final String Authorization = "X-Auth-Token";

	public static final String ENV = "dev";

	static String delimeter = "/";

	@Autowired
	private S3WrapperService s3Wrapper;

	private S3BucketInfoService s3BucketInfoService;

	private JobDetailService jobDetailService;

	@Autowired
	private AmazonS3Client amazonS3Client;

	@Autowired
	public AmazonController(JobDetailService jobDetailService, S3BucketInfoService s3BucketInfoService) {
		this.s3BucketInfoService = s3BucketInfoService;
		this.jobDetailService = jobDetailService;
	}

	@SuppressWarnings("deprecation")
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public ResponseEntity<AppResponse> upload(@RequestPart("file") MultipartFile[] multipartFiles,
			@RequestPart("template") String template, ServletRequest request) {
		String token = ((HttpServletRequest) request).getHeader("X-Auth-Token");
		String bucketName = CodeUtils.decode(token).get(USER_BUCKET).getAsString();
		JsonArray orgDetails=CodeUtils.decode(token).get("ORG_DETAILS").getAsJsonArray();
		
		JsonObject jNode=orgDetails.get(0).getAsJsonObject();
		String orgId = jNode.get("orgId").getAsString();
		S3BucketInfo s3BucketInfo = s3BucketInfoService.getBucketByType(bucketName, FileUtils.FileUpload,orgId);
		AppResponse appResponse = null;
		ObjectNode objectNode = getMapper().createObjectNode();
		ObjectNode node = getMapper().createObjectNode();

		try {
			if (!CodeUtils.isEmpty(s3BucketInfo.getS3bucketName())) {
				String fileName = s3BucketInfo.getBucketPath() + delimeter + template + FileUtils.CSVExtension;
				String etag = s3Wrapper.upload(multipartFiles, bucketName, fileName).get(0).getMetadata().getETag();
				String filePath = bucketName + delimeter + fileName;
				if (!CodeUtils.isEmpty(etag)) {
					node.put("progress", AWSUtils.Percentage.COMPLETED);
					node.put("filePath", filePath);
					objectNode.put(CodeUtils.RESPONSE, node);
					appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
							.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG).putAll(objectNode))
							.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				}
			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SECURITY_ERROR_CODE, ClientErrors.NOT_FOUND,
						AppStatusMsg.NO_LOCATION_FOUND);
			}

		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SECURITY_ERROR_CODE, ClientErrors.NOT_FOUND,
					AppStatusMsg.BLANK);
		}
		return ResponseEntity.status(HttpStatus.OK).body(appResponse);
	}

	@RequestMapping(value = "/uploads", method = RequestMethod.POST)
	public ResponseEntity<AppResponse> upload(@RequestBody JsonNode payload) {
		AppResponse appResponse;
		try {
			String filepath = payload.get(FileName).asText();
			String bucket = payload.get(Bucket).asText();
			String enterprise = "";
			String progress = s3Wrapper.uploads(filepath, bucket, enterprise);

			appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
					.put(CodeUtils.RESPONSE, progress).put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
					.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
		} catch (AmazonClientException | InterruptedException e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.info(e.getMessage());
		}
		return ResponseEntity.status(HttpStatus.OK).body(appResponse);

	}

	@RequestMapping(value = "/downloads/{filetype}", method = RequestMethod.GET)
	public ResponseEntity<AppResponse> download(@PathVariable(value = "filetype") String fileType,
			ServletRequest request) {
		String token = ((HttpServletRequest) request).getHeader("X-Auth-Token");
		String bucketName = CodeUtils.decode(token).get(USER_BUCKET).getAsString();
		JsonArray orgDetails=CodeUtils.decode(token).get("ORG_DETAILS").getAsJsonArray();
		JsonObject jNode=orgDetails.get(0).getAsJsonObject();
		String orgId = jNode.get("orgId").getAsString();
		S3BucketInfo s3BucketInfo = s3BucketInfoService.getBucketByType(bucketName, fileType,orgId);
		AppResponse appResponse = null;
		try {
			if (!CodeUtils.isEmpty(s3BucketInfo.getS3bucketName())) {
				String key = s3BucketInfo.getBucketPath() + delimeter + s3BucketInfo.getOutputFileName();
				System.out.print("key :" + key);
				String keys = amazonS3Client.getObjectSummaries(bucketName, key).get(0).getKey();
				if (true) {
					String expire = s3BucketInfo.getTime();
					String url = s3Wrapper.downloadPreSignedURL(bucketName, keys, Integer.parseInt(expire));
					appResponse = new AppResponse.Builder()
							.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE, url)
									.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
							.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				}

			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SUCCESS_CODE, ClientErrors.NOT_FOUND,
						AppStatusMsg.NO_LOCATION_FOUND);
			}

		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SECURITY_ERROR_CODE, ClientErrors.NOT_FOUND,
					AppStatusMsg.BLANK);
			LOGGER.info(e.getMessage());
		}
		return ResponseEntity.status(HttpStatus.OK).body(appResponse);
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "deprecation" })
	@RequestMapping(value = "/download/{filetype}", method = RequestMethod.GET)
	public ResponseEntity<AppResponse> downloadSummary(@PathVariable(value = "filetype") String fileType,
			ServletRequest request) {
		
		AppResponse appResponse = null;
		String url = null;
		boolean isPopup = false;
		try {
			String token = ((HttpServletRequest) request).getHeader("X-Auth-Token");
			String bucketName = CodeUtils.decode(token).get(USER_BUCKET).getAsString();
			JsonArray orgDetails=CodeUtils.decode(token).get("ORG_DETAILS").getAsJsonArray();
			
			JsonObject jNode=orgDetails.get(0).getAsJsonObject();
			String orgId = jNode.get("orgId").getAsString();
			
			S3BucketInfo s3BucketInfo = s3BucketInfoService.getBucketByType(bucketName, fileType,orgId);
			ObjectNode objectNode = getMapper().createObjectNode();
			ObjectNode node = getMapper().createObjectNode();
			if (!CodeUtils.isEmpty(bucketName) && !CodeUtils.isEmpty(s3BucketInfo.getJobName())) {
				String keys = null;
				if (fileType.equalsIgnoreCase(OUTPUTFOLDER))
					keys = jobDetailService.getLastSummary(s3BucketInfo.getJobName(),s3BucketInfo.getOrgId()).getBucketKey();
				else
					keys = jobDetailService.getLastSummary(s3BucketInfo.getJobName(),s3BucketInfo.getOrgId()).getGeneratedTransferOrder();
				if (keys != null) {
					String expire = s3BucketInfo.getTime();
					keys = keys.split("\\//")[1].replaceAll(bucketName + delimeter, "") + delimeter.trim().toString();
					Map<String, String> urlwithFileName = s3Wrapper.downloadListOfPreSignedURL(bucketName, keys,
							Integer.parseInt(expire));
					if (urlwithFileName.size() == 1) {
						List<String> urlList = new ArrayList(urlwithFileName.values());
						url = urlList.get(0);
						objectNode.putPOJO("link", null);
					} else if (urlwithFileName.size() > 1) {
						isPopup = true;
						if (!urlwithFileName.toString().contains("FILE")) {
							List convertedUrlList = CodeUtils.convertMaptoList(urlwithFileName);
							objectNode.putPOJO("link", convertedUrlList);
						} else {
							List convertedUrlList = CodeUtils.genericsMaptoList(urlwithFileName);
							objectNode.putPOJO("link", convertedUrlList);
						}

					} else {
						isPopup = false;
						objectNode.putPOJO("link", null);
					}

					objectNode.put("URL", url);
					objectNode.put("isPopup", isPopup);
					node.putPOJO(CodeUtils.RESPONSE, objectNode);
					appResponse = new AppResponse.Builder()
							.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(node))
							.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				}

			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.NOT_FOUND,
						AppStatusMsg.JOB_FAILED);
			}

		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.NOT_FOUND,
					AppStatusMsg.BLANK);
			LOGGER.info(e.getMessage());
		}
		return ResponseEntity.status(HttpStatus.OK).body(appResponse);
	}

	@RequestMapping(value = "/download/history/{filetype}/{jid}", method = RequestMethod.GET)
	public ResponseEntity<AppResponse> downloadHistory(@PathVariable(value = "filetype") String fileType,
			@PathVariable(value = "jid") String jobID, ServletRequest request) {
		String token = ((HttpServletRequest) request).getHeader("X-Auth-Token");
		String bucketName = CodeUtils.decode(token).get(USER_BUCKET).getAsString();
		JsonArray orgDetails=CodeUtils.decode(token).get("ORG_DETAILS").getAsJsonArray();
		
		JsonObject jNode=orgDetails.get(0).getAsJsonObject();
		String orgId = jNode.get("orgId").getAsString();
		S3BucketInfo s3BucketInfo = s3BucketInfoService.getBucketByType(bucketName, fileType,orgId);
		AppResponse appResponse = null;

		try {
			if (!CodeUtils.isEmpty(bucketName) && !CodeUtils.isEmpty(s3BucketInfo.getJobName())) {
				JobDetail jobDetail = jobDetailService.getByJobId(jobID);
				String keys = jobDetailService.getByJobId(jobID).getBucketKey();
				if (keys != null && jobDetail.getJobRunState().equalsIgnoreCase(RunState.SUCCEEDED.toString())) {

					String expire = s3BucketInfo.getTime();
					keys = keys.split("\\//")[1].replaceAll(bucketName + delimeter, "") + delimeter.trim().toString();
					String url = s3Wrapper.downloadPreSignedURLOnHistory(bucketName, keys, Integer.parseInt(expire));
					appResponse = new AppResponse.Builder()
							.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE, url)
									.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
							.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				} else {
					appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.NOT_FOUND,
							AppStatusMsg.JOB_FAILED);
				}

			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SUCCESS_CODE, ClientErrors.NOT_FOUND,
						AppStatusMsg.NO_LOCATION_FOUND);
			}

		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SECURITY_ERROR_CODE, ClientErrors.NOT_FOUND,
					AppStatusMsg.BLANK);
			LOGGER.info(e.getMessage());
		}
		return ResponseEntity.status(HttpStatus.OK).body(appResponse);
	}

	@SuppressWarnings({"deprecation" })
	@RequestMapping(value = "/download/csv/{template}", method = RequestMethod.GET)
	public ResponseEntity<AppResponse> downloadCSVUploadData(@PathVariable(value = "template") String template,
			ServletRequest request) {
		String token = ((HttpServletRequest) request).getHeader("X-Auth-Token");
		String bucketName = CodeUtils.decode(token).get(USER_BUCKET).getAsString();
		JsonArray orgDetails=CodeUtils.decode(token).get("ORG_DETAILS").getAsJsonArray();
		
		JsonObject jNode=orgDetails.get(0).getAsJsonObject();
		String orgId = jNode.get("orgId").getAsString();
		S3BucketInfo s3BucketInfo = s3BucketInfoService.getBucketByType(bucketName, FileUtils.FileUpload,orgId);
		AppResponse appResponse = null;
		ObjectNode node = getMapper().createObjectNode();
		ObjectNode objectNode = getMapper().createObjectNode();
		try {
			if (!CodeUtils.isEmpty(s3BucketInfo.getS3bucketName())) {
				String key = s3BucketInfo.getBucketPath() + delimeter + template + FileUtils.CSVExtension;
				if (key.contains(FileUtils.CSVExtension.toLowerCase()) || key.contains(FileUtils.CSVExtension)) {
					String expire = s3BucketInfo.getTime();
					@SuppressWarnings("unchecked")
					Map<String, String> preSignedURLWithMetaData = s3Wrapper.getPreSignedURLWithMetaData(bucketName,
							key, Integer.parseInt(expire));
					node.put("url", preSignedURLWithMetaData.get("url"));
					node.put("modifiedOn", preSignedURLWithMetaData.get("modifiedOn"));
					node.put("fileName", template + FileUtils.CSVExtension);
					objectNode.put(CodeUtils.RESPONSE, node);

					appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
							.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG).putAll(objectNode))
							.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				}

			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SECURITY_ERROR_CODE, ClientErrors.NOT_FOUND,
						AppStatusMsg.NO_LOCATION_FOUND);
			}

		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SECURITY_ERROR_CODE, ClientErrors.NOT_FOUND,
					AppStatusMsg.BLANK);
			LOGGER.info(e.getMessage());
		}
		return ResponseEntity.status(HttpStatus.OK).body(appResponse);
	}

	@RequestMapping(value = "/downloads", method = RequestMethod.GET)
	public ResponseEntity<byte[]> downloads(@RequestParam String key) {
		String bucketKey = ENV + BUCKETSCRIPTPATH + delimeter + "citylife" + delimeter + OUTPUTFOLDER;
		try {
			Date date = CodeUtils.______dateTimeFormat.parse(key);
			return s3Wrapper.download(BUCKET, bucketKey, CodeUtils.convertDateOnTimeZonePluseForAWS(date, 0, 0));
		} catch (ParseException | IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value = "/presigned/{key}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> downloadPreSigned(@PathVariable(value = "key") String key) {
		String bucketKey = ENV + BUCKETSCRIPTPATH + delimeter + "citylife" + delimeter + OUTPUTFOLDER;
		AppResponse appResponse = null;
		try {
			Date date = CodeUtils.______dateTimeFormat.parse(key);
//			String URL = s3Wrapper.downloadPreSigned(BUCKET, BUCKETKEY,
//					CodeUtils.convertDateOnTimeZoneForAWS(date, 5, 30));
			String url = s3Wrapper.downloadPreSigned(BUCKET, bucketKey, date);
			appResponse = new AppResponse.Builder().data(getMapper().createObjectNode().put(CodeUtils.RESPONSE, url)
					.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG)).status(AppStatusCodes.GENERIC_SUCCESS_CODE)
					.build();
		} catch (ParseException e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.info(e.getMessage());
		}
		return ResponseEntity.status(HttpStatus.OK).body(appResponse);

	}

	@RequestMapping(value = "/list/{bucket}", method = RequestMethod.GET)
	public List<S3ObjectSummary> list(@PathVariable(value = "bucket") String bucket) throws IOException {
		return s3Wrapper.list(bucket);
	}

	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public ResponseEntity<AppResponse> getObject(@RequestBody JsonNode payload) {

		AppResponse appResponse = null;
		try {
			String bucket = payload.get(Bucket).asText();
			String key = payload.get(BucketKey).asText();
			String url = amazonS3Client.getObjectSummary(bucket, key).getBucketName().toString();
			appResponse = new AppResponse.Builder().data(getMapper().createObjectNode().put(CodeUtils.RESPONSE, url)
					.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG)).status(AppStatusCodes.GENERIC_SUCCESS_CODE)
					.build();
		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.info(e.getMessage());
		}
		return ResponseEntity.status(HttpStatus.OK).body(appResponse);
	}

	@RequestMapping(value = "/presigned/url", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> getPreSigned(@RequestBody JsonNode payload) {
		AppResponse appResponse = null;
		try {
			String bucket = payload.get(Bucket).asText();
			String bucketKey = payload.get(BucketKey).asText() + delimeter + payload.get(FileName).asText();
			int expire = payload.get(Expiration).asInt();
			String url = s3Wrapper.downloadPreSignedURL(bucket, bucketKey, expire);
			appResponse = new AppResponse.Builder().data(getMapper().createObjectNode().put(CodeUtils.RESPONSE, url)
					.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG)).status(AppStatusCodes.GENERIC_SUCCESS_CODE)
					.build();
		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.info(e.getMessage());
		}
		return ResponseEntity.status(HttpStatus.OK).body(appResponse);

	}

	@RequestMapping(value = "/upload/file", method = RequestMethod.POST)
	public List<PutObjectResult> upload(@RequestParam("file") MultipartFile[] multipartFiles) {
		String bucket = "tcloud-supplymint-develop-com";
		return s3Wrapper.upload(multipartFiles, bucket, "/TEST.CSV");

	}

	@RequestMapping(value = "/create/bucket/{bucketName}", method = RequestMethod.GET)
	public com.amazonaws.services.s3.model.Bucket createBucket(@PathVariable(value = "bucket") String bucket) {
		return s3Wrapper.createBucket(bucket);
	}

	@RequestMapping(value = "/read/s3data", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> getDataFromS3(@RequestBody JsonNode payload) {
		AppResponse appResponse = null;
		try {
			String bucket = payload.get(Bucket).asText();
			String bucketKey = payload.get(BucketKey).asText().toString();
			ArrayNode arrayNode = s3Wrapper.readDataFromS3(bucket, bucketKey);
			appResponse = new AppResponse.Builder()
					.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE, arrayNode.toString())
							.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))

					.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.info(e.getMessage());
		}
		return ResponseEntity.status(HttpStatus.OK).body(appResponse);
	}

}
