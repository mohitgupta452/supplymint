package com.supplymint.layer.web.endpoint.tenant.administration;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.InetAddress;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.dao.DataAccessException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.supplymint.config.application.AppCodeConfig;
import com.supplymint.config.aws.utils.AWSUtils;
import com.supplymint.exception.StatusCodes.AppModuleErrors;
import com.supplymint.exception.StatusCodes.AppStatusCodes;
import com.supplymint.exception.StatusCodes.AppStatusMsg;
import com.supplymint.exception.StatusCodes.ClientErrors;
import com.supplymint.exception.SupplyMintException;
import com.supplymint.layer.business.contract.administration.CustomService;
import com.supplymint.layer.data.tenant.administration.entity.CustomFileUpload;
import com.supplymint.layer.data.tenant.administration.entity.CustomMaster;
import com.supplymint.layer.data.tenant.administration.entity.DataReference;
import com.supplymint.layer.data.tenant.administration.entity.EventMaster;
import com.supplymint.layer.data.tenant.administration.entity.FMCGStores;
import com.supplymint.layer.web.endpoint.abstracts.AbstractEndpoint;
import com.supplymint.layer.web.model.AppResponse;
import com.supplymint.util.CodeUtils;
import com.supplymint.util.FileUtils;
import com.supplymint.util.JsonUtils;

import oracle.net.aso.s;

@RestController
@RequestMapping(path = CustomEndpoint.PATH, produces = { MediaType.APPLICATION_JSON_VALUE })
public class CustomEndpoint extends AbstractEndpoint {

	public static final String PATH = "admin/custom";

	private static final Logger LOGGER = LoggerFactory.getLogger(CustomEndpoint.class);

	private CustomService customService;

	@Autowired
	private AppCodeConfig appStatus;

	@Autowired
	public CustomEndpoint(CustomService customService) {
		this.customService = customService;
	}

	@SuppressWarnings("finally")
	@RequestMapping(value = "/file/upload", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> customeFileUpload(@RequestBody JsonNode payload) {
		AppResponse appResponse = null;
		CustomFileUpload customFileUpload = new CustomFileUpload();
		int result = 0;
		String activeFile = "FALSE";
		try {
			if (!CodeUtils.isEmpty(payload.get("channel").asText())) {
				activeFile = "TRUE";
				customFileUpload.setChannel(payload.get("channel").asText());
				customFileUpload.setTemplate(payload.get("template").asText());
				customFileUpload.setActiveFile(activeFile);
				customFileUpload.setFilePath(payload.get("filepath").asText());
				customFileUpload.setBucket(payload.get("bucket").asText());
				customFileUpload.setCreatedTime(new Date());
				String ipAddress = InetAddress.getLocalHost().getHostAddress().trim().toString();
				customFileUpload.setIpAddress(ipAddress);
				LOGGER.debug("Query,for creating,is going to start...");
				result = customService.createCustomFileUpload(customFileUpload);
				LOGGER.debug("Query,for creating,has ended...");
			}
			if (result != 0) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, customFileUpload)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.DATA_SYNC_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
						AppModuleErrors.CUSTOM_CREATOR_ERROR,
						appStatus.getMessage(AppModuleErrors.CUSTOM_CREATOR_ERROR));
			}
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.CUSTOM_CREATOR_ERROR, appStatus.getMessage(AppModuleErrors.CUSTOM_CREATOR_ERROR));

			LOGGER.info(appStatus.getMessage(AppModuleErrors.CUSTOM_CREATOR_ERROR));

		} catch (Exception ex) {
			ex.printStackTrace();
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			throw new SupplyMintException(AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "deprecation", "finally" })
	@RequestMapping(value = "/get/all", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getAllCustomFileUpload(@RequestParam("pageno") Integer pageNo) {

		AppResponse appResponse = null;
		List<CustomFileUpload> data = null;
		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;

		try {
			LOGGER.debug("Query,for counting data,is going to start...");
			totalRecord = customService.getAllCustomRecord();
			LOGGER.debug("Query,for counting data,has ended...");
			maxPage = (totalRecord + pageSize - 1) / pageSize;
			previousPage = pageNo - 1;
			offset = (pageNo - 1) * pageSize;
			LOGGER.debug("Query,for getting all counted data,is going to start...");
			data = customService.getAllCustomFileUpload(offset, pageSize);
			LOGGER.debug("Query,for getting all counted data,has ended...");

//			LOGGER.debug("Query,for getting all data,is going to start...");
//			List<CustomFileUpload> data = customService.getAllCustomFileUpload();
//			LOGGER.debug("Query,for getting all data,has ended...");
			ObjectNode objectNode = getMapper().createObjectNode();
//			ArrayNode array = getMapper().createArrayNode();
//			data.forEach(e -> {
//				ObjectNode node = getMapper().valueToTree(e);
//				array.add(node);
//			});
			objectNode.putPOJO(CodeUtils.RESPONSE, data);
			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put("currPage", pageNo).put("prePage", previousPage)
								.put("maxPage", maxPage).put(CodeUtils.RESPONSE_MSG, "").putAll(objectNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				LOGGER.info(AppStatusMsg.BLANK);
			}
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.CUSTOM_READER_ERROR, appStatus.getMessage(AppModuleErrors.CUSTOM_READER_ERROR));
			LOGGER.info(appStatus.getMessage(AppModuleErrors.CUSTOM_READER_ERROR));
		} catch (Exception ex) {
			ex.printStackTrace();
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "deprecation", "finally" })
	@RequestMapping(value = "/get/partner", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getAllPartnerByChannel(@RequestParam("channel") String channel) {

		AppResponse appResponse = null;

		try {
			List<CustomMaster> data = customService.getAllPartnerByChannel(channel.toUpperCase());
			if (data.isEmpty()) {
				data = customService.getAllPartnerByChannel(channel.toLowerCase());
			}
			Set<String> partnerSet = new HashSet<String>();
			List<String> partner = data.stream().filter(e -> e.getPartner() != null && partnerSet.add(e.getPartner()))
					.map(m -> m.getPartner()).collect(Collectors.toList());

			ObjectNode objectNode = getMapper().createObjectNode();
			ArrayNode array = getMapper().createArrayNode();
			data.forEach(e -> {
				ObjectNode node = getMapper().valueToTree(e);
				array.add(node);
			});
			ObjectNode node = getMapper().createObjectNode();
			objectNode.putPOJO("partner", partner);
			objectNode.putPOJO("store", array);
			node.put(CodeUtils.RESPONSE, objectNode);
			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(node))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				LOGGER.info(AppStatusMsg.BLANK);
			}
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.CUSTOM_READER_ERROR, appStatus.getMessage(AppModuleErrors.CUSTOM_READER_ERROR));
			LOGGER.info(appStatus.getMessage(AppModuleErrors.CUSTOM_READER_ERROR));
		} catch (Exception ex) {
			ex.printStackTrace();
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "deprecation", "finally" })
	@RequestMapping(value = "/get/zone", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getAllZoneByChannel(@RequestParam("channel") String channel,
			@RequestParam("partner") String partner) {

		AppResponse appResponse = null;

		try {
			LOGGER.debug("Query,for getting data,is going to start...");
			List<CustomMaster> data = customService.getAllZoneByChannel(partner, channel.toUpperCase());
			LOGGER.debug("Query,for getting data,has ended...");
			if (data.isEmpty()) {
				data = customService.getAllZoneByChannel(partner, channel.toLowerCase());
			}
			Set<String> zoneSet = new HashSet<String>();
			List<String> zone = data.stream().filter(e -> e.getZone() != null && zoneSet.add(e.getZone()))
					.map(m -> m.getZone()).collect(Collectors.toList());

			ObjectNode objectNode = getMapper().createObjectNode();
			ArrayNode array = getMapper().createArrayNode();
			data.forEach(e -> {
				ObjectNode node = getMapper().valueToTree(e);
				array.add(node);
			});
			ObjectNode node = getMapper().createObjectNode();
			objectNode.putPOJO("zone", zone);
			objectNode.putPOJO("store", array);
			node.put(CodeUtils.RESPONSE, objectNode);
			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(node))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				LOGGER.info(AppStatusMsg.BLANK);
			}
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.CUSTOM_READER_ERROR, appStatus.getMessage(AppModuleErrors.CUSTOM_READER_ERROR));
			LOGGER.info(appStatus.getMessage(AppModuleErrors.CUSTOM_READER_ERROR));
		} catch (Exception ex) {
			ex.printStackTrace();
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "deprecation", "finally" })
	@RequestMapping(value = "/get/grade", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getAllGradeByZone(@RequestParam("channel") String channel,
			@RequestParam("partner") String partner, @RequestParam("zone") String zone) {

		AppResponse appResponse = null;

		try {

			List<CustomMaster> data = null;
			LOGGER.debug("Query for getting data is going to start...");
			data = customService.getAllGradeByZone(partner, channel.toUpperCase(), zone);
			LOGGER.debug("Query for getting data has ended...");
			if (CodeUtils.isEmpty(data))
				data = customService.getAllGradeByZone(partner, channel.toLowerCase(), zone);
			Set<String> gradeSet = new HashSet<String>();
			List<String> grade = data.stream().filter(e -> e.getStoreGrade() != null && gradeSet.add(e.getStoreGrade()))
					.map(m -> m.getStoreGrade()).collect(Collectors.toList());

			ObjectNode objectNode = getMapper().createObjectNode();
			ArrayNode array = getMapper().createArrayNode();
			data.forEach(e -> {
				ObjectNode node = getMapper().valueToTree(e);
				array.add(node);
			});
			ObjectNode node = getMapper().createObjectNode();
			objectNode.putPOJO("grade", grade);
			objectNode.putPOJO("store", array);
			node.put(CodeUtils.RESPONSE, objectNode);
			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(node))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				LOGGER.info(AppStatusMsg.BLANK);
			}
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.CUSTOM_READER_ERROR, appStatus.getMessage(AppModuleErrors.CUSTOM_READER_ERROR));
			LOGGER.info(appStatus.getMessage(AppModuleErrors.CUSTOM_READER_ERROR));
		} catch (Exception ex) {
			ex.printStackTrace();
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "deprecation", "finally", "unchecked", "rawtypes" })
	@RequestMapping(value = "/get/storeCode", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> getAllStoreCode(@RequestBody JsonNode payload) {

		AppResponse appResponse = null;

		try {
			String channel = payload.get("channel").asText();
			String partner = payload.get("partner").asText();
			String zone = payload.get("zone").asText();
			String grade = payload.get("grade").asText();
			LOGGER.debug("Query,for getting data,is going to start...");
			List data = customService.getAllStoreCode(partner, channel.toUpperCase(), zone, grade);
			LOGGER.debug("Query,for getting data,has ended...");
			ObjectNode objectNode = getMapper().createObjectNode();
			ArrayNode array = getMapper().createArrayNode();
			data.forEach(e -> {
				ObjectNode node = getMapper().valueToTree(e);
				array.add(node);
			});
			ObjectNode node = getMapper().createObjectNode();
			objectNode.putPOJO("store", array);
			node.put(CodeUtils.RESPONSE, objectNode);
			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(node))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				LOGGER.info(AppStatusMsg.BLANK);
			}
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.CUSTOM_READER_ERROR, appStatus.getMessage(AppModuleErrors.CUSTOM_READER_ERROR));
			LOGGER.info(appStatus.getMessage(AppModuleErrors.CUSTOM_READER_ERROR));
		} catch (Exception ex) {
			ex.printStackTrace();
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings("finally")
	@RequestMapping(value = "/upload", method = RequestMethod.POST, consumes = { MediaType.MULTIPART_FORM_DATA_VALUE })
	@ResponseBody
	public ResponseEntity<AppResponse> custFileUpload(@RequestParam("file") MultipartFile file) {
		AppResponse appResponse = null;
		String path = FileUtils.getPlatformBasedParentDir().toString() + "/upload/";
		File desinationFile;
		File createClientFolder;
		BufferedOutputStream stream;
		String realFileName = null;
		String url = null;
		int progress = 0;
		try {

			if (!file.isEmpty()) {

				progress = 100;
				realFileName = file.getOriginalFilename();

				url = path + realFileName;// +extension;
				desinationFile = new File(url);
				createClientFolder = new File(desinationFile.getParent());
				if (!createClientFolder.exists())
					createClientFolder.mkdirs();
				stream = new BufferedOutputStream(new FileOutputStream(desinationFile));
				stream.write(file.getBytes());
				stream.close();
			}
			ObjectNode objectNode = getMapper().createObjectNode();

			objectNode.put("filePath", url);
			objectNode.put("progress", progress);

			if (!CodeUtils.isEmpty(url)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, objectNode)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
						AppModuleErrors.SITEMAP_CREATOR_ERROR,
						appStatus.getMessage(AppModuleErrors.SITEMAP_CREATOR_ERROR));
			}
		} catch (DataAccessException ex) {
			ex.printStackTrace();
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.SITEMAP_CREATOR_ERROR, appStatus.getMessage(AppModuleErrors.SITEMAP_CREATOR_ERROR));

			LOGGER.info(appStatus.getMessage(AppModuleErrors.SITEMAP_CREATOR_ERROR));

		} catch (Exception ex) {
			ex.printStackTrace();
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "deprecation", "finally", "unchecked", "rawtypes" })
	@RequestMapping(value = "/get/template", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getAllTemplate(@RequestParam("channel") String channel) {

		AppResponse appResponse = null;

		try {
			LOGGER.debug("Query,for getting data,is going to start...");
			List data = customService.getAllTemplate(channel);
			LOGGER.debug("Query,for getting data,has ended...");
			ObjectNode objectNode = getMapper().createObjectNode();
			ArrayNode array = getMapper().createArrayNode();
			data.forEach(e -> {
				ObjectNode node = getMapper().valueToTree(e);
				array.add(node);
			});
			objectNode.put(CodeUtils.RESPONSE, array);
			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(objectNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				LOGGER.info(AppStatusMsg.BLANK);
			}
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.CUSTOM_READER_ERROR, appStatus.getMessage(AppModuleErrors.CUSTOM_READER_ERROR));
			LOGGER.info(appStatus.getMessage(AppModuleErrors.CUSTOM_READER_ERROR));
		} catch (Exception ex) {
			ex.printStackTrace();
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "deprecation", "finally" })
	@RequestMapping(value = "/get/all/reference", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getAllDataReference(@RequestParam("pageno") Integer pageNo,
			@RequestParam("type") Integer type, @RequestParam("search") String search,
			@RequestParam("name") String name, @RequestParam("key") String key,
			@RequestParam("eventStart") String eventS, @RequestParam("eventEnd") String eventE,
			@RequestParam("status") String status, @RequestParam("dataCount") Integer dataCount,
			@RequestParam("channel") String channel) {

		AppResponse appResponse = null;
		List<DataReference> data = null;
		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;
		String eventStart = eventS;
		String eventEnd = eventE;
		try {
			if (type == 1) {
				LOGGER.debug("Query,for counting data,is going to start...");
				totalRecord = customService.refRecordCount(channel.toUpperCase());
				LOGGER.debug("Query,for counting data,has ended...");
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo - 1) * pageSize;
				LOGGER.debug("Query,for getting all counted data,is going to start...");
				data = customService.getAllDataReference(offset, pageSize, channel.toUpperCase());
				LOGGER.debug("Query,for getting all counted data,has ended...");
			} else if (type == 2) {
				if (!CodeUtils.isEmpty(eventStart)) {
					eventStart = CodeUtils.convertDataReferenceDateFormat(eventStart);
				}
				if (!CodeUtils.isEmpty(eventEnd)) {
					eventEnd = CodeUtils.convertDataReferenceDateFormat(eventEnd);
				}
				LOGGER.debug("Query,for filerting data,is going to start...");
				totalRecord = customService.searchDataReferenceRecord(name, key, eventStart, eventEnd, status,
						dataCount, channel.toUpperCase());
				LOGGER.debug("Query,for filerting data,has ended...");
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo - 1) * pageSize;
				LOGGER.debug("Query,for getting all filtered record,is going to start...");
				data = customService.searchDataReference(pageSize, offset, name, key, eventStart, eventEnd, status,
						dataCount, channel.toUpperCase());
				LOGGER.debug("Query,for getting all filtered record,has ended...");
			} else if (type == 3) {

				if (!search.isEmpty()) {
					LOGGER.debug("Query,for getting data,is going to start...");
					totalRecord = customService.searchAllRecordDataReference(search, channel.toUpperCase());
					LOGGER.debug("Query,for getting data,has ended...");
					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo - 1) * pageSize;
					LOGGER.debug("Query,for getting all searched record,is going to start...");
					data = customService.searchAllDataReference(offset, pageSize, search, channel.toUpperCase());
					LOGGER.debug("Query,for getting all searched record,has ended...");
				}
			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
						AppStatusMsg.FAILURE_MSG);
			}

			ObjectNode objectNode = getMapper().createObjectNode();
			ArrayNode array = getMapper().createArrayNode();
			data.forEach(e -> {
				ObjectNode node = getMapper().valueToTree(e);
				array.add(node);
			});
			objectNode.put(CodeUtils.RESPONSE, array);
			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put("currPage", pageNo).put("prePage", previousPage)
								.put("maxPage", maxPage).put(CodeUtils.RESPONSE_MSG, "").putAll(objectNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.CUSTOM_READER_ERROR, appStatus.getMessage(AppModuleErrors.CUSTOM_READER_ERROR));
		} catch (Exception ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "deprecation", "finally" })
	@RequestMapping(value = "/get/top/reference", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getTop5DataReference() {

		AppResponse appResponse = null;
		List<DataReference> data = null;
		try {
			LOGGER.debug("Query,for getting data,is going to start...");
			data = customService.getTop5DataReference();
			LOGGER.debug("Query,for getting data,has ended...");
			ObjectNode objectNode = getMapper().createObjectNode();
			ArrayNode array = getMapper().createArrayNode();
			data.forEach(e -> {
				ObjectNode node = getMapper().valueToTree(e);
				array.add(node);
			});
			objectNode.put(CodeUtils.RESPONSE, array);
			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(objectNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.CUSTOM_READER_ERROR, appStatus.getMessage(AppModuleErrors.CUSTOM_READER_ERROR));
			LOGGER.info(appStatus.getMessage(AppModuleErrors.CUSTOM_READER_ERROR));
		} catch (Exception ex) {
			ex.printStackTrace();
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "deprecation", "finally" })
	@RequestMapping(value = "/filter/storeCode", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> filterStoreCode(@RequestBody JsonNode payload) {

		AppResponse appResponse = null;
		List<CustomMaster> data = null;
		String store = "";

		try {
			String channel = payload.get("channel").asText();
			String partner = payload.get("partner").asText();
			String zone = payload.get("zone").asText();
			String grade = payload.get("grade").asText();
			String storeCode = payload.get("storeCode").asText();
			if (!partner.equalsIgnoreCase(AWSUtils.CustomParam.NOT_APPLICABLE)
					|| !zone.equalsIgnoreCase(AWSUtils.CustomParam.NOT_APPLICABLE)
					|| !grade.equalsIgnoreCase(AWSUtils.CustomParam.NOT_APPLICABLE)) {
				data = customService.filterStoreCode(channel, partner, zone, grade);
				Set<String> storeSet = new HashSet<String>();
				List<String> allStore = data.stream()
						.filter(e -> e.getStoreCode() != null && storeSet.add(e.getStoreCode()))
						.map(m -> m.getStoreCode()).collect(Collectors.toList());

				for (int i = 0; i < allStore.size(); i++) {
					store += allStore.get(i) + "|";
				}
				if (!CodeUtils.isEmpty(data))
					store = store.substring(0, store.length() - 1);
			} else if (partner.equalsIgnoreCase(AWSUtils.CustomParam.NOT_APPLICABLE)
					&& storeCode.equalsIgnoreCase(AWSUtils.CustomParam.NOT_APPLICABLE)
					&& zone.equalsIgnoreCase(AWSUtils.CustomParam.NOT_APPLICABLE)
					&& grade.equalsIgnoreCase(AWSUtils.CustomParam.NOT_APPLICABLE)) {
				store = "ALL_STORES";
			} else if (!storeCode.equalsIgnoreCase(AWSUtils.CustomParam.NOT_APPLICABLE)) {
				store = storeCode;
			}
			ObjectNode objectNode = getMapper().createObjectNode();
			ObjectNode node = getMapper().createObjectNode();
			objectNode.putPOJO("store", store);
			node.put(CodeUtils.RESPONSE, objectNode);
			if (!CodeUtils.isEmpty(data) || !store.equalsIgnoreCase(AWSUtils.CustomParam.NOT_APPLICABLE)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(node))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.CUSTOM_READER_ERROR, appStatus.getMessage(AppModuleErrors.CUSTOM_READER_ERROR));
		} catch (Exception ex) {
			ex.printStackTrace();
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "deprecation", "finally" })
	@RequestMapping(value = "/paramsetting/{type}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getParamSetting(@PathVariable("type") String type) {

		AppResponse appResponse = null;
		JSONObject data = null;
		Map<String, Object> d = null;
		try {
			ObjectNode node = getMapper().createObjectNode();
			if (type.equalsIgnoreCase("ONLOAD")) {
				String paramSetting = customService.getCustomByKey("CUSTOM_PARAMETER_SETTING");
				data = JsonUtils.toJsonObject(paramSetting);
				d = JsonUtils.toMap(data);
				node.putPOJO(CodeUtils.RESPONSE, d);
			} else {
				String paramSetting = customService.getCustomByKey("CUSTOM_PARAMETER_FILE");
				data = JsonUtils.toJsonObject(paramSetting);
				d = JsonUtils.toMap(data);
				Set keys = d.keySet();
				Iterator itr = keys.iterator();
				String key11;
				List<Map<String, Object>> value11;
				String keyvalue11;
				String valueOf11;
				while (itr.hasNext()) {
					key11 = (String) itr.next();
					value11 = (List<Map<String, Object>>) d.get(key11);
					for (int i = 0; i < value11.size(); i++) {
						Object templateValue = value11.get(i);
						String result = templateValue + "";
						if (result.contains(type)) {
							node.putPOJO(CodeUtils.RESPONSE, value11.get(i).get(type));
						}
					}
				}
			}
			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(node))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}
		} catch (DataAccessException ex) {
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.CUSTOM_READER_ERROR, appStatus.getMessage(AppModuleErrors.CUSTOM_READER_ERROR));
		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings("finally")
	@RequestMapping(value = "/master/create", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> customMasterSave(@RequestBody JsonNode payload, HttpServletRequest request) {
		AppResponse appResponse = null;
		int result = 0;
		try {
			if (!CodeUtils.isEmpty(payload)) {
				result = customService.createCustomMaster(payload, request);
			}
			if (result != 0) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, payload)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
						AppModuleErrors.CUSTOM_CREATOR_ERROR,
						appStatus.getMessage(AppModuleErrors.CUSTOM_CREATOR_ERROR));
			}
		} catch (SupplyMintException ex) {
			LOGGER.debug(String.format("Error Occoured From SupplyMintException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					ex.getMessage());
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.CUSTOM_CREATOR_ERROR, appStatus.getMessage(AppModuleErrors.CUSTOM_CREATOR_ERROR));

			LOGGER.info(appStatus.getMessage(AppModuleErrors.CUSTOM_CREATOR_ERROR));

		} catch (Exception ex) {
			ex.printStackTrace();
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			throw new SupplyMintException(AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "deprecation", "finally" })
	@RequestMapping(value = "/get/all/event", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getAllEventMasterData(@RequestParam("pageno") Integer pageNo,
			@RequestParam("type") Integer type, @RequestParam("search") String search,
			@RequestParam("eventName") String eventName, @RequestParam("startDate") String startDate,
			@RequestParam("endDate") String endDate, @RequestParam("multiplierROS") String multiplierROS,
			@RequestParam("stockDays") String stockDays, @RequestParam("grade") String grade,
			@RequestParam("partner") String partner, @RequestParam("zone") String zone,
			@RequestParam("storeCode") String storeCode, HttpServletRequest request) {

		AppResponse appResponse = null;
//		List<EventMaster> data = null;
		ObjectNode dataNode = getMapper().createObjectNode();
		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;
		try {

			String token = ((HttpServletRequest) request).getHeader("X-Auth-Token");
//			int eid = CodeUtils.decode(token).get("eid").getAsInt();
			JsonArray orgDetails = CodeUtils.decode(token).get("ORG_DETAILS").getAsJsonArray();

			JsonObject jNode = orgDetails.get(0).getAsJsonObject();
			String orgId = jNode.get("orgId").getAsString();
//			String orgId = "201";
			if (type == 1) {
				LOGGER.debug("Query,for counting data,is going to start...");
				totalRecord = customService.getAllCountEventMaster(orgId);
				LOGGER.debug("Query,for counting data,has ended...");
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo - 1) * pageSize;
				LOGGER.debug("Query,for getting all counted data,is going to start...");
				dataNode = customService.getAllEventMasterData(offset, pageSize, orgId);
				LOGGER.debug("Query,for getting all counted data,has ended...");
			} else if (type == 2) {

				LOGGER.debug("Query,for filerting data,is going to start...");
				totalRecord = customService.filterCountEventMasterRecord(eventName, startDate, endDate, stockDays,
						multiplierROS, partner, grade, storeCode, zone, orgId);
				LOGGER.debug("Query,for filerting data,has ended...");
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo - 1) * pageSize;
				LOGGER.debug("Query,for getting all filtered record,is going to start...");
				dataNode = customService.getFilterCustomEventMaster(pageSize, offset, eventName, startDate, endDate,
						stockDays, multiplierROS, partner, grade, storeCode, zone, orgId);
				LOGGER.debug("Query,for getting all filtered record,has ended...");
			} else if (type == 3) {

				if (!search.isEmpty()) {
					LOGGER.debug("Query,for getting data,is going to start...");
					totalRecord = customService.searchCountCustomMasterData(search, orgId);
					LOGGER.debug("Query,for getting data,has ended...");
					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo - 1) * pageSize;
					LOGGER.debug("Query,for getting all searched record,is going to start...");
					dataNode = customService.searchAllCustomMasterData(offset, pageSize, search, orgId);
					LOGGER.debug("Query,for getting all searched record,has ended...");
				}
			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
						AppStatusMsg.FAILURE_MSG);
			}

			if (!CodeUtils.isEmpty(dataNode)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put("currPage", pageNo).put("prePage", previousPage)
								.put("maxPage", maxPage).put(CodeUtils.RESPONSE_MSG, "").putAll(dataNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.CUSTOM_READER_ERROR, appStatus.getMessage(AppModuleErrors.CUSTOM_READER_ERROR));
		} catch (Exception ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings("finally")
	@RequestMapping(value = "/delete/event", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> delete(@RequestBody JsonNode payload, HttpServletRequest request) {
		AppResponse appResponse = null;

		int result = 0;
		try {
			String token = ((HttpServletRequest) request).getHeader("X-Auth-Token");
//			int eid = CodeUtils.decode(token).get("eid").getAsInt();
			JsonArray orgDetails = CodeUtils.decode(token).get("ORG_DETAILS").getAsJsonArray();

			JsonObject jNode = orgDetails.get(0).getAsJsonObject();
			String orgId = jNode.get("orgId").getAsString();

			result = customService.deleteEventMaster(payload, orgId);

			if (!CodeUtils.isEmpty(result)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, payload)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
						AppModuleErrors.SITEMAP_CREATOR_ERROR,
						appStatus.getMessage(AppModuleErrors.SITEMAP_CREATOR_ERROR));
			}
		} catch (DataAccessException ex) {
			ex.printStackTrace();
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.SITEMAP_CREATOR_ERROR, appStatus.getMessage(AppModuleErrors.SITEMAP_CREATOR_ERROR));

			LOGGER.info(appStatus.getMessage(AppModuleErrors.SITEMAP_CREATOR_ERROR));

		} catch (Exception ex) {
			ex.printStackTrace();
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally", "unchecked" })
	@RequestMapping(value = "/get/fmcg/mother/comp", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getAllFMCGMotherComp(@RequestParam("pageno") int pageNo,
			@RequestParam("type") Integer type, @RequestParam("search") String search) {

		AppResponse appResponse = null;
		int previousPage = 0;
		int maxPage = 0;
		List<String> motherComData = null;
		try {

			Map<String, Object> data = customService.getAllMotherComp(pageNo, type, search);
			LOGGER.debug("Query for getting data is going to start...");
			maxPage = (int) data.get("maxPage");
			previousPage = (int) data.get("previousPage");
			motherComData = (List<String>) data.get("motherCom");

			LOGGER.debug("Query for getting data has ended...");
			if (!CodeUtils.isEmpty(motherComData)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put("currPage", pageNo).put("prePage", previousPage)
								.put("maxPage", maxPage).put(CodeUtils.RESPONSE_MSG, "")
								.putPOJO(CodeUtils.RESPONSE, motherComData))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				LOGGER.info(AppStatusMsg.BLANK);
			}
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.CUSTOM_READER_ERROR, appStatus.getMessage(AppModuleErrors.CUSTOM_READER_ERROR));
			LOGGER.info(appStatus.getMessage(AppModuleErrors.CUSTOM_READER_ERROR));
		} catch (Exception ex) {
			ex.printStackTrace();
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally", "unchecked" })
	@RequestMapping(value = "/get/fmcg/article", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getAllFMCGArticle(@RequestParam("pageno") int pageNo,
			@RequestParam("type") Integer type, @RequestParam("motherComp") String motherComp,
			@RequestParam("search") String search) {

		AppResponse appResponse = null;
		int previousPage = 0;
		int maxPage = 0;
		List<FMCGStores> articleData = null;

		try {
			LOGGER.debug("Query for getting data is going to start...");
			Map<String, Object> data = customService.getAllArticleByMotherComp(pageNo, type, motherComp, search);
			LOGGER.debug("Query for getting data has ended...");
			previousPage = (int) data.get("previousPage");
			maxPage = (int) data.get("maxPage");
			articleData = (List<FMCGStores>) data.get("articleData");
			
			if (!CodeUtils.isEmpty(articleData)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put("currPage", pageNo).put("prePage", previousPage)
								.put("maxPage", maxPage).put(CodeUtils.RESPONSE_MSG, "")
								.putPOJO(CodeUtils.RESPONSE, articleData))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				LOGGER.info(AppStatusMsg.BLANK);
			}
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.CUSTOM_READER_ERROR, appStatus.getMessage(AppModuleErrors.CUSTOM_READER_ERROR));
			LOGGER.info(appStatus.getMessage(AppModuleErrors.CUSTOM_READER_ERROR));
		} catch (Exception ex) {
			ex.printStackTrace();
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally", "unchecked" })
	@RequestMapping(value = "/get/fmcg/vendor", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getAllFMCGVendor(@RequestParam("pageno") int pageNo,
			@RequestParam("type") Integer type, @RequestParam("motherComp") String motherComp,
			@RequestParam("articleCode") String articleCode, @RequestParam("search") String search) {

		AppResponse appResponse = null;
		int previousPage = 0;
		int maxPage = 0;
		List<FMCGStores> vendorData = null;

		try {

			Map<String, Object> data = customService.getAllVendor(pageNo, type,motherComp, articleCode,search);
			LOGGER.debug("Query for getting data is going to start...");
			previousPage = (int) data.get("previousPage");
			maxPage = (int) data.get("maxPage");
			vendorData = (List<FMCGStores>) data.get("vendorData");
			LOGGER.debug("Query for getting data has ended...");
			if (!CodeUtils.isEmpty(vendorData)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put("currPage", pageNo).put("prePage", previousPage)
								.put("maxPage", maxPage).put(CodeUtils.RESPONSE_MSG, "")
								.putPOJO(CodeUtils.RESPONSE, vendorData))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				LOGGER.info(AppStatusMsg.BLANK);
			}
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.CUSTOM_READER_ERROR, appStatus.getMessage(AppModuleErrors.CUSTOM_READER_ERROR));
			LOGGER.info(appStatus.getMessage(AppModuleErrors.CUSTOM_READER_ERROR));
		} catch (Exception ex) {
			ex.printStackTrace();
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally", "unchecked" })
	@RequestMapping(value = "/get/fmcg/store", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getAllFMCGStore(@RequestParam("pageno") int pageNo,@RequestParam("type") Integer type,
			@RequestParam("motherComp") String motherComp, @RequestParam("articleCode") String articleCode,
			@RequestParam("vendorCode") String vendorCode,@RequestParam("search") String search) {

		AppResponse appResponse = null;
		int previousPage = 0;
		int maxPage = 0;
		List<FMCGStores> storeData = null;

		try {

			Map<String, Object> data = customService.getAllStore(pageNo,type, motherComp, articleCode, vendorCode,search);
			LOGGER.debug("Query for getting data is going to start...");
			previousPage = (int) data.get("previousPage");
			maxPage = (int) data.get("maxPage");
			storeData = (List<FMCGStores>) data.get("storeData");
			LOGGER.debug("Query for getting data has ended...");
			if (!CodeUtils.isEmpty(storeData)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put("currPage", pageNo).put("prePage", previousPage)
								.put("maxPage", maxPage).put(CodeUtils.RESPONSE_MSG, "")
								.putPOJO(CodeUtils.RESPONSE, storeData))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				LOGGER.info(AppStatusMsg.BLANK);
			}
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.CUSTOM_READER_ERROR, appStatus.getMessage(AppModuleErrors.CUSTOM_READER_ERROR));
			LOGGER.info(appStatus.getMessage(AppModuleErrors.CUSTOM_READER_ERROR));
		} catch (Exception ex) {
			ex.printStackTrace();
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally", "deprecation" })
	@RequestMapping(value = "/get/fmcg/filter/store", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> getAllFMCGFilterStore(@RequestBody JsonNode payload) {

		AppResponse appResponse = null;
		String storeCodeData = null;
		try {
			storeCodeData = customService.getAllFMCGFilterStore(payload);
			LOGGER.debug("Query for getting data has ended...");
			ObjectNode objectNode = getMapper().createObjectNode();
			ObjectNode node = getMapper().createObjectNode();
			objectNode.putPOJO("store", storeCodeData);
			node.put(CodeUtils.RESPONSE, objectNode);

			if (!CodeUtils.isEmpty(storeCodeData)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(node))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				LOGGER.info(AppStatusMsg.BLANK);
			}
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.CUSTOM_READER_ERROR, appStatus.getMessage(AppModuleErrors.CUSTOM_READER_ERROR));
			LOGGER.info(appStatus.getMessage(AppModuleErrors.CUSTOM_READER_ERROR));
		} catch (Exception ex) {
			ex.printStackTrace();
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

}
