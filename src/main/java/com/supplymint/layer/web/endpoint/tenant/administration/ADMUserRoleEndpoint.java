package com.supplymint.layer.web.endpoint.tenant.administration;

import java.net.InetAddress;
import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.dao.DataAccessException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.CannotGetJdbcConnectionException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.supplymint.config.application.AppCodeConfig;
import com.supplymint.exception.StatusCodes.AppErrorCodes;
import com.supplymint.exception.StatusCodes.AppModuleErrors;
import com.supplymint.exception.StatusCodes.AppStatusCodes;
import com.supplymint.exception.StatusCodes.AppStatusMsg;
import com.supplymint.exception.StatusCodes.ClientErrors;
import com.supplymint.exception.StatusCodes.ServerErrors;
import com.supplymint.exception.SupplyMintException;
import com.supplymint.layer.business.contract.administration.ADMUserRoleService;
import com.supplymint.layer.business.contract.administration.ADMUserService;
import com.supplymint.layer.data.config.ThreadLocalStorage;
import com.supplymint.layer.data.tenant.administration.entity.ADMUser;
import com.supplymint.layer.data.tenant.administration.entity.ADMUserRole;
import com.supplymint.layer.web.endpoint.abstracts.AbstractEndpoint;
import com.supplymint.layer.web.model.AppResponse;
import com.supplymint.util.ApplicationUtils.AppEnvironment;
import com.supplymint.util.CodeUtils;

@RestController
@RequestMapping(path = ADMUserRoleEndpoint.PATH, produces = { MediaType.APPLICATION_JSON_VALUE })
public class ADMUserRoleEndpoint extends AbstractEndpoint {
	public static final String PATH = "admin/role";
	private static final Logger LOGGER = LoggerFactory.getLogger(ADMUserEndpoint.class);
	private ADMUserRoleService admUserRoleService;
	private static final String URI_UPDATEPATH_DEV = "https://b7rav73vr7.execute-api.ap-south-1.amazonaws.com/dev/user/auth/update/user";
	private static final String URI_UPDATEPATH_PROD = "https://b7rav73vr7.execute-api.ap-south-1.amazonaws.com/prod/user/auth/update/user";

	@Autowired
	private Environment env;

	@Autowired
	private ADMUserService admUserService;

	@Autowired
	private AppCodeConfig appStatus;

	@Autowired
	private HttpServletRequest request;

	@Autowired
	public ADMUserRoleEndpoint(ADMUserRoleService admUserRoleService) {
		this.admUserRoleService = admUserRoleService;
	}

	@SuppressWarnings("finally")
	@RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getByIdentifierAndModule(@PathVariable(value = "id") String sid) {
		AppResponse appResponse = null;
		int id = 0;
		try {
			id = Integer.parseInt(sid);
//			LOGGER.debug("Query,for getting all details by ID,is going to start...");
			ADMUserRole data = admUserRoleService.findById(id);
			LOGGER.debug("Query,for getting all details by ID,has ended...");
			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, data)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = buildErrorResponse(AppErrorCodes.GENERIC_EVENT_NOT_AVAILABLE_ERROR_CODE,
						ClientErrors.NOT_FOUND, AppStatusMsg.BLANK);
				LOGGER.info(AppStatusMsg.BLANK);
			}
		} catch (NumberFormatException ex) {
			LOGGER.debug(String.format("Error Occoured From NumberFormatException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.NUMBER_FORMAT_MSG);

		} catch (CannotGetJdbcConnectionException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.USER_ROLE_READER_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
		} catch (DataAccessException ex) {

			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.USER_ROLE_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.USER_ROLE_READER_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
		} finally {
			return ResponseEntity.ok(appResponse);
		}

	}

	@SuppressWarnings("finally")
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> create(@RequestBody JsonNode userNode) throws SupplyMintException {
		int result = 0;
		AppResponse appResponse = null;
		ADMUserRole admUser = null;
		try {
			admUser = getMapper().treeToValue(userNode, ADMUserRole.class);
			OffsetDateTime currentTime = OffsetDateTime.now();
			currentTime = currentTime.plusHours(5);
			currentTime = currentTime.plusMinutes(30);
			admUser.setCreatedTime(currentTime);
			if (!CodeUtils.isEmpty(request)) {
				admUser.setCreatedBy(request.getAttribute("email").toString());
			}
			admUser.setStatus("active");
			String ipAddress = InetAddress.getLocalHost().getHostAddress().trim().toString();
			admUser.setIpAddress(ipAddress);
			LOGGER.debug("Query,for creating, is going to start...");
			result = admUserRoleService.create(admUser);
			LOGGER.debug("Query,for creating,has ended...");
			if (result != 0) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, admUser)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
						AppModuleErrors.USER_ROLE_CREATOR_ERROR,
						appStatus.getMessage(AppModuleErrors.USER_ROLE_CREATOR_ERROR));
			}

		} catch (CannotGetJdbcConnectionException ex) {
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.USER_ROLE_CREATOR_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.USER_ROLE_CREATOR_ERROR,
					appStatus.getMessage(AppModuleErrors.USER_ROLE_CREATOR_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (JsonMappingException jsonException) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.INVALID_JSON);
			LOGGER.debug(String.format("Error Occoured From JsonMappingException : %s", jsonException));
		} catch (Exception e) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", e));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings("finally")
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> update(@RequestBody JsonNode userNode) {

		AppResponse appResponse = null;
		int result = 0;
		ADMUserRole users = null;
		ResponseEntity<AppResponse> responseEntity = null;

		try {
			users = getMapper().treeToValue(userNode, ADMUserRole.class);
			LOGGER.debug("Query,for getting all details by user name,is going to start...");
			ADMUser admUser = admUserService.getByUserName(users.getUserName());
			LOGGER.debug("Query,for getting all details by user name,has ended...");
			users.setEid(admUser.getPartnerEnterpriseId());
			users.setActive(admUser.getActive());
			String newRole = Arrays.toString(users.getUserRoles()).substring(1);
			newRole = newRole.substring(0, newRole.length() - 1);
			users.setName(newRole);
			LOGGER.debug("user roles : %s", users.getName());
			OffsetDateTime updationTime = OffsetDateTime.now();
			updationTime = updationTime.plusHours(5);
			updationTime = updationTime.plusMinutes(30);
			users.setUpdationTime(updationTime);
			if (!CodeUtils.isEmpty(request)) {
				users.setCreatedBy(request.getAttribute("email").toString());
			}
			String ipAddress = InetAddress.getLocalHost().getHostAddress().trim().toString();
			users.setIpAddress(ipAddress);
			LOGGER.debug("Query,for updating all details,is going to start...");
			result = admUserRoleService.update(users);
			LOGGER.debug("Query,for updating all details,has ended...");
			if (result != 0) {
				responseEntity = admUserRoleService.sendUserRolesDetailsOnRestAPI(users,
						env.getActiveProfiles().length == 0
								|| env.getActiveProfiles()[0].equalsIgnoreCase(AppEnvironment.Profiles.DEV.toString())
										? URI_UPDATEPATH_DEV
										: env.getActiveProfiles()[0].equalsIgnoreCase(
												AppEnvironment.Profiles.PROD.toString()) ? URI_UPDATEPATH_PROD
														: URI_UPDATEPATH_DEV);
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, users)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.USER_ROLE_UPDATE_SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
						AppModuleErrors.USER_ROLE_UPDATE_ERROR,
						appStatus.getMessage(AppModuleErrors.USER_ROLE_UPDATE_ERROR));
			}

		} catch (CannotGetJdbcConnectionException ex) {
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.USER_ROLE_UPDATE_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.USER_ROLE_UPDATE_ERROR,
					appStatus.getMessage(AppModuleErrors.USER_ROLE_UPDATE_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings("finally")
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> delete(@PathVariable(value = "id") String sid) {
		AppResponse appResponse = null;
		int id = 0;
		try {
			id = Integer.parseInt(sid);
			LOGGER.debug("Query,for deleting data by specific ID,is going to start...");
			int result = admUserRoleService.delete(id);
			LOGGER.debug("Query,for deleting data by specific ID,has ended...");
			if (result != 0) {
				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode().putPOJO("resource", null)
						.put("message", AppStatusMsg.SUCCESS_MSG)).status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
						AppModuleErrors.USER_ROLE_DELETE_ERROR,
						appStatus.getMessage(AppModuleErrors.USER_ROLE_DELETE_ERROR));
				LOGGER.info(appStatus.getMessage(AppModuleErrors.USER_ROLE_DELETE_ERROR));
			}

		} catch (CannotGetJdbcConnectionException ex) {
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.USER_ROLE_DELETE_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.USER_ROLE_DELETE_ERROR,
					appStatus.getMessage(AppModuleErrors.USER_ROLE_DELETE_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (NumberFormatException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.NUMBER_FORMAT_MSG);
			LOGGER.debug(String.format("Error Occoured From NumberFormatException : %s", ex));
		} catch (Exception ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "deprecation", "finally" })
	@RequestMapping(value = "/get/onstatus/{status}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getStatus(@PathVariable("status") String status) {

		AppResponse appResponse = null;
		try {
			LOGGER.debug("Query,for getting data by specific status,is going to start...");
			List data = admUserRoleService.getStatus(status);
			LOGGER.debug("Query,for getting data by specific status,has ended...");
			ObjectNode objectNode = getMapper().createObjectNode();
			ArrayNode array = getMapper().createArrayNode();
			data.forEach(e -> {
				ObjectNode node = getMapper().valueToTree(e);
				array.add(node);
			});
			objectNode.put(CodeUtils.RESPONSE, array);
			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(objectNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = buildErrorResponse(AppErrorCodes.GENERIC_EVENT_NOT_AVAILABLE_ERROR_CODE,
						ClientErrors.NOT_FOUND, AppStatusMsg.BLANK);
				LOGGER.info(AppStatusMsg.BLANK);
			}
		} catch (CannotGetJdbcConnectionException ex) {
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.USER_ROLE_READER_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.USER_ROLE_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.USER_ROLE_READER_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.debug(String.format("Error Occoured From Exception : %s", e));
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings("finally")
	@RequestMapping(value = "/update/status/{status}", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> updateStatus(@PathVariable(value = "status") String status,
			@RequestParam(value = "urid") String urid) {
		AppResponse appResponse = null;
		int result = 0;
		int id = 0;
		try {
			id = Integer.parseInt(urid);
			OffsetDateTime updationTime = OffsetDateTime.now();
			updationTime = updationTime.plusHours(5);
			updationTime = updationTime.plusMinutes(30);
			String ipAddress = InetAddress.getLocalHost().getHostAddress().trim().toString();
			LOGGER.debug("Query,for updating data,is going to start...");
			result = admUserRoleService.updateStatus(status, id, ipAddress, updationTime);
			LOGGER.debug("Query,for updating data,has ended...");
			if (result != 0) {
				LOGGER.debug("Query,for getting data by specific ID,is going to start...");
				ADMUserRole data = admUserRoleService.findById(id);
				LOGGER.debug("Query,for getting data by specific ID,has ended...");
				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode().putPOJO("resource", data)
						.put("message", AppStatusMsg.SUCCESS_MSG)).status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
						AppModuleErrors.USER_ROLE_UPDATE_ERROR,
						appStatus.getMessage(AppModuleErrors.USER_ROLE_UPDATE_ERROR));
				LOGGER.info(appStatus.getMessage(AppModuleErrors.USER_ROLE_UPDATE_ERROR));
			}
		} catch (CannotGetJdbcConnectionException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.USER_ROLE_UPDATE_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.USER_ROLE_UPDATE_ERROR,
					appStatus.getMessage(AppModuleErrors.USER_ROLE_UPDATE_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (NumberFormatException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.NUMBER_FORMAT_MSG);
			LOGGER.debug(String.format("Error Occoured From NumberFormatException : %s", ex));
		} catch (Exception ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
		} finally {
			return ResponseEntity.ok(appResponse);
		}

	}

	@SuppressWarnings({ "unchecked", "deprecation", "finally", "rawtypes" })
	@RequestMapping(value = "/get/user/{userid}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getUserId(@PathVariable("userid") String userid) {

		AppResponse appResponse = null;
		int id = 0;
		try {

			id = Integer.parseInt(userid);
			LOGGER.debug("Query,for getting data by specific ID,is going to start...");
			List data = admUserRoleService.getUserById(id);
			LOGGER.debug("Query,for getting data by specific ID,has ended...");
			ObjectNode objectNode = getMapper().createObjectNode();
			ArrayNode array = getMapper().createArrayNode();
			data.forEach(e -> {
				ObjectNode node = getMapper().valueToTree(e);
				array.add(node);
			});
			objectNode.put(CodeUtils.RESPONSE, array);
			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(objectNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = buildErrorResponse(AppErrorCodes.GENERIC_EVENT_NOT_AVAILABLE_ERROR_CODE,
						ClientErrors.NOT_FOUND, AppStatusMsg.BLANK);
				LOGGER.info(AppStatusMsg.BLANK);
			}
		} catch (CannotGetJdbcConnectionException ex) {
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.USER_ROLE_READER_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.USER_ROLE_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.USER_ROLE_READER_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (NumberFormatException ex) {

			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.NUMBER_FORMAT_MSG);
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));

		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					ServerErrors.INTERNAL_SERVER_ERROR, AppStatusMsg.FAILURE_MSG);
			LOGGER.info(e.getMessage());
		} finally {
			return ResponseEntity.ok(appResponse);
		}

	}

	@SuppressWarnings({ "finally", "unchecked", "rawtypes", "deprecation" })
	@RequestMapping(value = "/get/onrole/{name}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getUserRole(@PathVariable("name") String name) {

		AppResponse appResponse = null;
		try {
			LOGGER.debug("Query,for getting data,is going to start...");
			List data = admUserRoleService.getOnRole(name);
			LOGGER.debug("Query,for getting data,has ended...");
			ObjectNode objectNode = getMapper().createObjectNode();
			ArrayNode array = getMapper().createArrayNode();
			data.forEach(e -> {
				ObjectNode node = getMapper().valueToTree(e);
				array.add(node);
			});
			objectNode.put(CodeUtils.RESPONSE, array);
			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(objectNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = buildErrorResponse(AppErrorCodes.GENERIC_EVENT_NOT_AVAILABLE_ERROR_CODE,
						ClientErrors.NOT_FOUND, AppStatusMsg.BLANK);
				LOGGER.info(AppStatusMsg.BLANK);
			}
		} catch (CannotGetJdbcConnectionException ex) {
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.USER_ROLE_READER_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.USER_ROLE_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.USER_ROLE_READER_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.debug(String.format("Error Occoured From Exception : %s", e));
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "unchecked", "deprecation", "rawtypes", "finally" })
	@RequestMapping(value = "/get/filter", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> filter(@RequestParam("pageno") Integer pageNo,
			@RequestBody ADMUserRole admUserRole) throws SupplyMintException {

		AppResponse appResponse = null;
		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;

		try {
			admUserRole.setUserName(admUserRole.getUserName().trim());
			admUserRole.setName(admUserRole.getName().trim());
			admUserRole.setCreatedBy(admUserRole.getCreatedBy().trim());
			admUserRole.setStatus(admUserRole.getStatus().trim());
			LOGGER.debug("Query,for getting all record,is going to start...");
			totalRecord = admUserRoleService.record();
			LOGGER.debug("Query,for getting all record,has ended...");
			if (totalRecord != 0) {
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo - 1) * pageSize;
				admUserRole.setOffset(offset);
				admUserRole.setPagesize(pageSize);
				LOGGER.debug("Query,for filtering all record,is going to start...");
				List data = admUserRoleService.filter(admUserRole);
				LOGGER.debug("Query,for filtering all record,has ended...");
				ObjectNode objectNode = getMapper().createObjectNode();
				ArrayNode array = getMapper().createArrayNode();
				data.forEach(e -> {
					ObjectNode node = getMapper().valueToTree(e);
					array.add(node);
				});
				objectNode.put(CodeUtils.RESPONSE, array);

				if (!CodeUtils.isEmpty(data)) {
					appResponse = new AppResponse.Builder()
							.data(getMapper().createObjectNode().put("currPage", pageNo).put("prePage", previousPage)
									.put("maxPage", maxPage).put(CodeUtils.RESPONSE_MSG, "").putAll(objectNode))
							.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				} else {
					appResponse = new AppResponse.Builder()
							.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
									.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
							.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
					LOGGER.info(AppStatusMsg.BLANK);
				}
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

				LOGGER.info(AppStatusMsg.BLANK);
			}
		} catch (CannotGetJdbcConnectionException ex) {
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.USER_ROLE_READER_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.USER_ROLE_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.USER_ROLE_READER_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally" })
	@RequestMapping(value = "/get/all", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getAll(@RequestParam("pageNo") Integer pageNo,
			@RequestParam("type") Integer type, @RequestParam("search") String search,
			@RequestParam("userName") String userName, @RequestParam("name") String name,
			@RequestParam("createdBy") String createdBy, @RequestParam("status") String status) {

		AppResponse appResponse = null;
		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;
		ObjectNode objectNode;
		List<ADMUserRole> data = null;
		ADMUserRole admUserRole = null;
		int flag = 0;

		try {

			flag = type;
			LOGGER.debug(String.format("TenantHashKey : %s", ThreadLocalStorage.getTenantHashKey()));
			String token = ((HttpServletRequest) request).getHeader("X-Auth-Token");
			String enterpriseUserName = CodeUtils.decode(token).get("prn").getAsString();
			if (flag == 1) {
				LOGGER.debug("Query,for getting all record,is going to start...");
				totalRecord = admUserRoleService.record();
				LOGGER.debug("Query,for getting all record,has ended...");
				totalRecord = totalRecord - 1;
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo - 1) * pageSize;
				LOGGER.debug("Query,for finding all record,is going to start...");
				data = admUserRoleService.findAll(offset, pageSize, enterpriseUserName);
				LOGGER.debug("Query,for finding all record,has ended...");
				if (!CodeUtils.isEmpty(data)) {
					for (int i = 0; i < data.size(); i++) {
						if (!CodeUtils.isEmpty(data.get(i))) {
							LOGGER.info(data.get(i).getName());
							if (data.get(i).getName().contains(",")) {
								String[] roles = data.get(i).getName().split(",");
								data.get(i).setUserRoles(roles);
							} else {
								data.get(i).setUserRoles(new String[] { data.get(i).getName() });
							}
						}
					}

					objectNode = buildJson(data);
					appResponse = successResponse(pageNo, previousPage, maxPage, objectNode);
					LOGGER.info(AppStatusMsg.SUCCESS_MSG);

				} else {
					appResponse = blankResponse();
					LOGGER.info(AppStatusMsg.BLANK);
				}
			} else if (flag == 2) {
				admUserRole = new ADMUserRole();
				admUserRole.setUserName(userName);
				admUserRole.setName(name);
				admUserRole.setCreatedBy(createdBy);
				admUserRole.setStatus(status);
				LOGGER.debug("Query,for getting all record,is going to start...");
				totalRecord = admUserRoleService.filterRecord(admUserRole);
				LOGGER.debug("Query,for getting all record,has ended...");
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo - 1) * pageSize;
				LOGGER.debug("Query,for getting all filtered record,is going to start...");
				data = admUserRoleService.filterUserRole(offset, pageSize, admUserRole);
				LOGGER.debug("Query,for getting all filtered record,has ended...");
				if (!CodeUtils.isEmpty(data)) {
					for (int i = 0; i < data.size(); i++) {
						if (!CodeUtils.isEmpty(data.get(i))) {
							LOGGER.info(data.get(i).getName());
							if (data.get(i).getName().contains(",")) {
								String[] roles = data.get(i).getName().split(",");
								data.get(i).setUserRoles(roles);
							} else {
								data.get(i).setUserRoles(new String[] { data.get(i).getName() });
							}
						}
					}
					objectNode = buildJson(data);
					appResponse = successResponse(pageNo, previousPage, maxPage, objectNode);

				} else {
					appResponse = blankResponse();
					LOGGER.info(AppStatusMsg.BLANK);
				}
			} else {
				if (flag == 3 && !search.isEmpty()) {
					// data = searchAll(pageNo, search);
					LOGGER.debug("Query,for getting all record,is going to start...");
					totalRecord = admUserRoleService.searchRecord(search);
					LOGGER.debug("Query,for getting all record,has ended...");
					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo - 1) * pageSize;
					LOGGER.debug("Query,for getting all searched record,is going to start...");
					data = admUserRoleService.searchAll(offset, pageSize, search);
					LOGGER.debug("Query,for getting all searched record,has ended...");
					if (!CodeUtils.isEmpty(data)) {

						for (int i = 0; i < data.size(); i++) {
							if (!CodeUtils.isEmpty(data.get(i))) {
								LOGGER.info(data.get(i).getName());
								if (data.get(i).getName().contains(",")) {
									String[] roles = data.get(i).getName().split(",");
									data.get(i).setUserRoles(roles);
								} else {
									data.get(i).setUserRoles(new String[] { data.get(i).getName() });
								}
							}
						}

						objectNode = buildJson(data);
						appResponse = successResponse(pageNo, previousPage, maxPage, objectNode);

					} else {
						appResponse = blankResponse();
					}
				} else {
					appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
							AppStatusMsg.FAILURE_MSG);
				}
			}
		} catch (CannotGetJdbcConnectionException ex) {
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.USER_ROLE_READER_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.USER_ROLE_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.USER_ROLE_READER_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
		} finally {

			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings("deprecation")
	ObjectNode buildJson(List<ADMUserRole> data) {
		ObjectNode objectNode = getMapper().createObjectNode();
		ArrayNode array = getMapper().createArrayNode();
		data.forEach(e -> {
			ObjectNode node = getMapper().valueToTree(e);
			array.add(node);
		});
		objectNode.put(CodeUtils.RESPONSE, array);

		return objectNode;
	}

	AppResponse notFoundResponse() {
		AppResponse appResponse = buildErrorResponse(AppErrorCodes.GENERIC_EVENT_NOT_AVAILABLE_ERROR_CODE,
				ClientErrors.NOT_FOUND, AppStatusMsg.BLANK);
		return appResponse;
	}

	public List<ADMUserRole> searchWord(String str, List<ADMUserRole> data) {
		List<ADMUserRole> record = null;
		record = data.stream()
				.filter(userRole -> (userRole.getUserName() != null ? userRole.getUserName().toLowerCase().contains(str)
						: false)
						|| (userRole.getName() != null ? userRole.getName().toLowerCase().contains(str) : false))
				.collect(Collectors.toList());

		return record;
	}

}
