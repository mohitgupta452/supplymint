package com.supplymint.layer.web.endpoint.tenant.inventory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.OffsetDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.dao.DataAccessException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.CannotGetJdbcConnectionException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.glue.model.BatchStopJobRunResult;
import com.amazonaws.services.glue.model.ConcurrentRunsExceededException;
import com.amazonaws.services.glue.model.CreateTriggerResult;
import com.amazonaws.services.glue.model.DeleteTriggerResult;
import com.amazonaws.services.glue.model.EntityNotFoundException;
import com.amazonaws.services.glue.model.GetJobResult;
import com.amazonaws.services.glue.model.GetJobRunResult;
import com.amazonaws.services.glue.model.IdempotentParameterMismatchException;
import com.amazonaws.services.glue.model.StopTriggerResult;
import com.amazonaws.services.glue.model.UpdateTriggerResult;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.opencsv.CSVReader;
import com.supplymint.config.application.AppCodeConfig;
import com.supplymint.config.aws.config.AWSGlueConfig;
import com.supplymint.config.aws.config.AmazonS3Client;
import com.supplymint.config.aws.utils.AWSUtils;
import com.supplymint.config.aws.utils.AWSUtils.Conditions;
import com.supplymint.config.aws.utils.AWSUtils.RunState;
import com.supplymint.config.aws.utils.AWSUtils.Status;
import com.supplymint.exception.StatusCodes.AppErrorCodes;
import com.supplymint.exception.StatusCodes.AppModuleErrors;
import com.supplymint.exception.StatusCodes.AppStatusCodes;
import com.supplymint.exception.StatusCodes.AppStatusMsg;
import com.supplymint.exception.StatusCodes.ClientErrors;
import com.supplymint.exception.StatusCodes.FileStatusCode;
import com.supplymint.exception.StatusCodes.Pagination;
import com.supplymint.exception.SupplyMintException;
import com.supplymint.layer.business.contract.inventory.JobDetailService;
import com.supplymint.layer.business.contract.notification.EmailService;
import com.supplymint.layer.business.contract.notification.MailManagerService;
import com.supplymint.layer.business.contract.procurement.PurchaseOrderService;
import com.supplymint.layer.business.contract.s3Bucket.S3BucketInfoService;
import com.supplymint.layer.business.service.core.HeaderConfigService;
import com.supplymint.layer.business.service.s3bucketService.S3WrapperService;
import com.supplymint.layer.data.config.ThreadLocalStorage;
import com.supplymint.layer.data.notification.entity.MailManager;
import com.supplymint.layer.data.notification.entity.MailManagerViewLog;
import com.supplymint.layer.data.tenant.inventory.entity.HeaderConfigLog;
import com.supplymint.layer.data.tenant.inventory.entity.HeadersSpecificOutput;
import com.supplymint.layer.data.tenant.inventory.entity.JobDetail;
import com.supplymint.layer.data.tenant.inventory.entity.JobTrigger;
import com.supplymint.layer.data.tenant.inventory.entity.S3BucketInfo;
import com.supplymint.layer.data.tenant.procurement.entity.ProcurementUploadSummary;
import com.supplymint.layer.web.endpoint.abstracts.AbstractEndpoint;
import com.supplymint.layer.web.model.AppResponse;
import com.supplymint.util.ApplicationUtils.AppEnvironment.Profiles;
import com.supplymint.util.ApplicationUtils.BucketParameter;
import com.supplymint.util.ApplicationUtils.CustomParameter;
import com.supplymint.util.ApplicationUtils.CustomRunState;
import com.supplymint.util.ApplicationUtils.EmailTemplateName;
import com.supplymint.util.ApplicationUtils.Module;
import com.supplymint.util.ApplicationUtils.SubModule;
import com.supplymint.util.CodeUtils;
import com.supplymint.util.ExcelUtils;
import com.supplymint.util.FileUtils;

/**
 * @author Prabhakar Srivastava
 * @since 20 OCT 2018
 */
@RestController
@RequestMapping(path = JobDetailEndpoint.PATH, produces = { MediaType.APPLICATION_JSON_VALUE })
public class JobDetailEndpoint extends AbstractEndpoint {

	public static final String PATH = "rule/engine";

	private static final Logger LOGGER = LoggerFactory.getLogger(JobDetailEndpoint.class);

	public static final String GENERATE_TRANSFERORDER = "GENERATE_TRANSFER_ORDER";

	public static final String CHANELPARTNER = "TRANSFER_ORDER";

	static String delimeter = "/";

	@Autowired
	private AWSGlueConfig awsGlueConfig;

	@Autowired
	private JobDetailService jobDetailService;

	@Autowired
	private MailManagerService mailManagerService;

	@Autowired
	private PurchaseOrderService poService;

	@Autowired
	private AmazonS3Client amazonS3Client;

	@Autowired
	private Environment environment;

	@Autowired
	private EmailService emailService;

	@Autowired
	private S3BucketInfoService s3BucketInfoService;

	@Autowired
	private AppCodeConfig appStatus;

	@Autowired
	private S3WrapperService s3Wrapper;

	@Autowired
	private HeaderConfigService configService;

	@Autowired
	public JobDetailEndpoint(JobDetailService jobDetailService, S3BucketInfoService s3BucketInfoService) {
		this.jobDetailService = jobDetailService;
		this.s3BucketInfoService = s3BucketInfoService;
	}

	@SuppressWarnings({ "finally" })
	@RequestMapping(value = "/job/get", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getByIdentifierAndModule(@RequestParam(value = "jobId") String jobId) {

		AppResponse appResponse = null;
		JobDetail data = null;
		try {

			LOGGER.info("Retrive the jobdetails by job id");
			data = jobDetailService.getByJobId(jobId);
			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, data)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}
		} catch (NumberFormatException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.NUMBER_FORMAT_MSG);
			LOGGER.debug(String.format("Error Occoured From NumberFormatException : %s", ex));
		} catch (CannotGetJdbcConnectionException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.RULE_ENGINE_READER_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.RULE_ENGINE_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.RULE_ENGINE_READER_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));

		} catch (Exception ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
		} finally {
			return ResponseEntity.ok(appResponse);
		}

	}

	@SuppressWarnings({ "deprecation", "finally" })
	@RequestMapping(value = "/job/get/all", method = RequestMethod.GET)
	@ResponseBody

	public ResponseEntity<AppResponse> getAll(@RequestParam("pageno") Integer pageNo,
			@RequestParam("type") Integer type, @RequestParam("from") String from, @RequestParam("to") String to,
			HttpServletRequest request) {

		AppResponse appResponse = null;
		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;

		int flag = 0;
		List<JobDetail> data = null;
		try {
			flag = type;
			String token = ((HttpServletRequest) request).getHeader("X-Auth-Token");
			String enterpriseCode = CodeUtils.decode(token).get("ecode").getAsString();
			JsonArray orgDetails = CodeUtils.decode(token).get("ORG_DETAILS").getAsJsonArray();

			JsonObject jNode = orgDetails.get(0).getAsJsonObject();
			String orgId = jNode.get("orgId").getAsString();
			String pattern = jNode.get("ip_prefix").getAsString();
			String enviroment = environment.getActiveProfiles()[0].equalsIgnoreCase(Profiles.DEV.name()) ? "DEV"
					: "PROD";

			String jobName = "SMGLUE-" + enterpriseCode + "-" + enviroment + "-" + pattern + "-JOB";

			if (flag == 1) {
				LOGGER.info("Retrive the job details summary get all");
				totalRecord = jobDetailService.record(orgId, jobName);
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo - 1) * pageSize;
				data = jobDetailService.getAll(offset, pageSize, orgId, jobName);
				LOGGER.info("Obtaining the job details summary get all");
			} else if (flag == 2) {
				LOGGER.info("Retrive the job details summary filter");
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				Date fromDate = new SimpleDateFormat("yyyy-MM-dd").parse(from);
				String fromConvertDateFormate = dateFormat.format(fromDate);

				Date toDate = new SimpleDateFormat("yyyy-MM-dd").parse(to);
				String toConvertDateFormate = dateFormat.format(toDate);

				long diff = toDate.getTime() - fromDate.getTime();
				int daysBetween = (int) (diff / (1000 * 60 * 60 * 24));

				if (daysBetween == 0) {
					totalRecord = jobDetailService.filterSameDateRecord(toConvertDateFormate, orgId, jobName);
					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo - 1) * pageSize;
					data = jobDetailService.filterSameDate(offset, pageSize, toConvertDateFormate, orgId, jobName);

				} else {
					totalRecord = jobDetailService.filterRecord(fromConvertDateFormate, toConvertDateFormate, orgId,
							jobName);
					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo - 1) * pageSize;
					data = jobDetailService.filter(offset, pageSize, fromConvertDateFormate, toConvertDateFormate,
							orgId, jobName);
				}
				LOGGER.info("Obtaining the job details summary filter");
			}
			ObjectNode objectNode = getMapper().createObjectNode();
			ArrayNode array = getMapper().createArrayNode();
			data.forEach(e -> {
				ObjectNode node = getMapper().valueToTree(e);
				array.add(node);
			});
			objectNode.put(CodeUtils.RESPONSE, array);

			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put("currPage", pageNo).put("prePage", previousPage)
								.put("maxPage", maxPage).put(CodeUtils.RESPONSE_MSG, "").putAll(objectNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}
		} catch (CannotGetJdbcConnectionException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.RULE_ENGINE_READER_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
		} catch (DataAccessException ex) {
			ex.printStackTrace();
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.RULE_ENGINE_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.RULE_ENGINE_READER_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception ex) {
			ex.printStackTrace();
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings("finally")
	@RequestMapping(value = "/job/createJob", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> createJob(@RequestBody JobDetail jDetail, HttpServletRequest request) {

		AppResponse appResponse = null;
		GetJobResult data = null;
		int result = 0;
		JobDetail jobDetail = jDetail;
		try {
			String token = ((HttpServletRequest) request).getHeader("X-Auth-Token");
			String enterpriseCode = CodeUtils.decode(token).get("ecode").getAsString();
			JsonArray orgDetails = CodeUtils.decode(token).get("ORG_DETAILS").getAsJsonArray();

			JsonObject jNode = orgDetails.get(0).getAsJsonObject();
			String orgId = jNode.get("orgId").getAsString();
			String pattern = jNode.get("ip_prefix").getAsString();
			String enviroment = environment.getActiveProfiles()[0].equalsIgnoreCase(Profiles.DEV.name()) ? "DEV"
					: "PROD";

			String jobName = "SMGLUE-" + enterpriseCode + "-" + enviroment + "-" + pattern + "-JOB";
			jobDetail.setJobName(jobName);

			// int isValid = jobDetailService.validate(jobDetail.getJobName());
			@SuppressWarnings("unchecked")
			List<JobDetail> jobDetails = jobDetailService.validatCreateJob(jobDetail.getJobName());
			LOGGER.info("Checking validation for only one job is created with job id is null");
			if (CodeUtils.isEmpty(jobDetails) || jobDetails.size() == 0) {
				jobDetail = jobDetails.get(0);
				data = awsGlueConfig.getJobDetails(jobDetail.getJobName());
				jobDetail.setJobName(data.getJob().getName());
				jobDetail.setRole(data.getJob().getRole());
				Date jobCreatedOn = data.getJob().getCreatedOn();
				Date jobModifiedOn = data.getJob().getLastModifiedOn();
				jobDetail.setJobCreatedOn(CodeUtils.convertDateOnTimeZonePluseForAWS(jobCreatedOn, 5, 30));
				jobDetail.setJobModifiedOn(CodeUtils.convertDateOnTimeZonePluseForAWS(jobModifiedOn, 5, 30));
				jobDetail.setScriptLocationOn(data.getJob().getCommand().getScriptLocation());
				String ipAddress = InetAddress.getLocalHost().getHostAddress().trim().toString();
				jobDetail.setIpAddress(ipAddress);
				Date creationTime = new Date();
				jobDetail.setCreatedOn(CodeUtils.convertDateOnTimeZonePluseForAWS(creationTime, 5, 30));
				jobDetail.setCreatedBy("");
				LOGGER.info("Creating a job with job id is null");
				result = jobDetailService.createJob(jobDetail);
				// jobDetail = jobDetailService.getById(jobDetail.getId());
				if (!CodeUtils.isEmpty(result)) {
					appResponse = new AppResponse.Builder()
							.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, jobDetail)
									.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
							.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				} else {
					appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
							AppModuleErrors.RULE_ENGINE_CREATOR_ERROR,
							appStatus.getMessage(AppModuleErrors.RULE_ENGINE_CREATOR_ERROR));
				}
			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
						AppStatusMsg.EXIST_JOB);
			}
		} catch (AmazonServiceException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, ClientErrors.BAD_REQUEST,
					ex.getErrorMessage());
			LOGGER.debug(String.format("Error Occoured From AmazonServiceException : %s", ex));
		} catch (CannotGetJdbcConnectionException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.RULE_ENGINE_CREATOR_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.RULE_ENGINE_CREATOR_ERROR,
					appStatus.getMessage(AppModuleErrors.RULE_ENGINE_CREATOR_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.debug(String.format("Error Occoured From Exception : %s", e));
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally", "unused", "deprecation" })
	@RequestMapping(value = "/job/create/trigger", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> createTrigger(@RequestBody JobTrigger jTrigger, ServletRequest request) {

		JobTrigger jobTrigger = jTrigger;
		String jobName = jobTrigger.getJobName();
		String timeZone = jobTrigger.getTimeZone();
		String sch = CodeUtils.convertStandardDateFormat(jobTrigger.getSchedule());
		Date awsschedule = CodeUtils.convertDateOnTimeZonePluseForAWS(new Date(sch), 5, 30);
		String convertSchedule = CodeUtils.convertISTtoNormalDateFormat(awsschedule.toString()).toString();
		String schedule = CodeUtils.convertStandardToNormalDateFormat(convertSchedule);
		String frequency = jobTrigger.getFrequency();
		String triggerName = jobTrigger.getTriggerName();
		String type = jobTrigger.getType();
		AppResponse appResponse = null;
		CreateTriggerResult createTriggerResult = null;
		UpdateTriggerResult updateTriggerResult = null;
		int result = 0;
		LOGGER.info("Checking validation for unique job trigger");
		int isValid = jobDetailService.validateTrigger(jobTrigger.getTriggerName());
		try {
			if (jobTrigger.getFrequency().equals("WEEKLY")) {
				jobTrigger.setDayMonth("NA");
			} else if (jobTrigger.getFrequency().equals("MONTHLY")) {
				jobTrigger.setDayWeek("NA");
			} else if (jobTrigger.getFrequency().equals("DAILY")) {
				jobTrigger.setDayWeek("NA");
				jobTrigger.setDayMonth("NA");
			}
			// @Author Manoj Singh -> Line of code allows user to set the action with
			// arguments while creating trigger .

			String token = ((HttpServletRequest) request).getHeader("X-Auth-Token");
			String enterpriseCode = CodeUtils.decode(token).get("ecode").getAsString();
			JsonArray orgDetails = CodeUtils.decode(token).get("ORG_DETAILS").getAsJsonArray();

			JsonObject jNode = orgDetails.get(0).getAsJsonObject();
			String orgId = jNode.get("orgId").getAsString();
			String pattern = jNode.get("ip_prefix").getAsString();
			String bucketName = CodeUtils.decode(token).get(FileUtils.BucketName).getAsString();
			jobName = jobName.subSequence(0, jobName.lastIndexOf("-") + 1) + pattern + "-JOB";
			jobTrigger.setJobName(jobName);
			jobTrigger.setOrgId(orgId);
			S3BucketInfo s3BucketInfo = s3BucketInfoService.getBucketByType(bucketName, FileUtils.FileDownload, orgId);

			JobTrigger trigger = jobDetailService.getByTriggerName(triggerName, orgId);
			String triggerNextSchedule = trigger != null ? trigger.getNextSchedule()
					: AWSUtils.CustomParam.NOT_APPLICABLE;
			String folderName = triggerNextSchedule != AWSUtils.CustomParam.NOT_APPLICABLE
					? CodeUtils.________dateTimeFormat.format(CodeUtils._____dateFormat.parse(triggerNextSchedule))
					: AWSUtils.CustomParam.NOT_APPLICABLE;
			s3BucketInfo.setOutputFileName(folderName);
			s3BucketInfo.setJobName(jobName);
			String path = BucketParameter.BUCKETPREFIX + bucketName + "/" + s3BucketInfo.getBucketPath();
			s3BucketInfo.setBucketKey(path);
			s3BucketInfo.setType(FileUtils.FileDownload);
			s3BucketInfo.setOrgId(orgId);

			LOGGER.info("Update bucket by type in create trigger api");
			s3BucketInfoService.updateBucketByType(s3BucketInfo);
			Map<String, String> arguments = new HashMap<>();
			Map<String, String> tempArg = new HashMap<>();
			tempArg.put(AWSUtils.CustomParam.STORE, AWSUtils.CustomParam.NOT_APPLICABLE);
			tempArg.put(AWSUtils.CustomParam.STORECODE, AWSUtils.CustomParam.NOT_APPLICABLE);
			tempArg.put(AWSUtils.CustomParam.TRIGGER, AWSUtils.CustomParam.SCHEDULED);
			tempArg.put(AWSUtils.CustomParam.PATH, path);
			tempArg.put(AWSUtils.CustomParam.STOCKPOINT, "ALL");
			tempArg.put(AWSUtils.CustomParam.PREFIX, pattern);

			String[] jobArguments = s3BucketInfo.getJobArguments().split(",");

			for (int i = 0; i < jobArguments.length; i++) {
				arguments.put(jobArguments[i], tempArg.get(jobArguments[i]));
			}

			// End

			LOGGER.info("Retrive next scheduled time in create trigger api");
			String schduleTriggerResult = awsGlueConfig.retrieveNextScheduled(frequency, schedule, timeZone,
					jobTrigger.getDayWeek(), jobTrigger.getDayMonth());
			jobTrigger.setNextSchedule(schduleTriggerResult);
			if (isValid == 0) {
				LOGGER.info("Create job trigger");
				createTriggerResult = awsGlueConfig.createTrigger(triggerName, jobName, schedule, timeZone, frequency,
						type, jobTrigger.getDayWeek(), jobTrigger.getDayMonth(), arguments);
				jobTrigger.setJobName(jobName);
				jobTrigger.setTriggerName(triggerName);
				String ipAddress = InetAddress.getLocalHost().getHostAddress().trim().toString();
				Date triggercreationTime = new Date();
				Date creationTime = CodeUtils.convertDateOnTimeZonePluseForAWS(triggercreationTime, 5, 30);
				jobTrigger.setIpAddress(ipAddress);
				jobTrigger.setCreatedBy("");
				jobTrigger.setCreatedOn(creationTime);
				// jobTrigger.setSchedule(CodeUtils.convertStandardDateFormat(schedule));
				Date correctScheduleTime = CodeUtils
						.convertDateOnTimeZoneForAWS(new Date(CodeUtils.convertStandardDateFormat(schedule)), 5, 30);
				jobTrigger
						.setSchedule(CodeUtils.convertISTtoNormalDateFormat(correctScheduleTime.toString()).toString());
				result = jobDetailService.createTrigger(jobTrigger);
				jobTrigger = jobDetailService.getByTriggerId(jobTrigger.getId());
			} else {
				// int updateSchedule=jobDetailService.updateNextSchedule(schduleTriggerResult,
				// triggerName);
				LOGGER.info("Update job trigger");
				updateTriggerResult = awsGlueConfig.updateTrigger(triggerName, jobName, schedule, timeZone, frequency,
						type, jobTrigger.getDayWeek(), jobTrigger.getDayMonth(), arguments);
				jobTrigger.setJobName(jobName);
				jobTrigger.setTriggerName(triggerName);
				String ipAddress = InetAddress.getLocalHost().getHostAddress().trim().toString();
				Date triggerupdationTime = new Date();
				Date updationTime = CodeUtils.convertDateOnTimeZonePluseForAWS(triggerupdationTime, 5, 30);
				jobTrigger.setIpAddress(ipAddress);
				jobTrigger.setUpdatedBy("");
				jobTrigger.setUpdatedOn(updationTime);
				// jobTrigger.setSchedule(CodeUtils.convertStandardDateFormat(schedule));
				Date correctScheduleTime = CodeUtils
						.convertDateOnTimeZoneForAWS(new Date(CodeUtils.convertStandardDateFormat(schedule)), 5, 30);
				jobTrigger
						.setSchedule(CodeUtils.convertISTtoNormalDateFormat(correctScheduleTime.toString()).toString());
				result = jobDetailService.uTrigger(jobTrigger);
				jobTrigger = jobDetailService.getByTriggerName(jobTrigger.getTriggerName(), orgId);
			}
			if (!CodeUtils.isEmpty(result)) {
				if (isValid == 0) {
					appResponse = new AppResponse.Builder()
							.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, jobTrigger)
									.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
							.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				} else {
					appResponse = new AppResponse.Builder()
							.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, jobTrigger)
									.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.TRIGGER_MSG))
							.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				}
			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
						AppModuleErrors.RULE_ENGINE_CREATOR_ERROR,
						appStatus.getMessage(AppModuleErrors.RULE_ENGINE_CREATOR_ERROR));
			}
		} catch (IdempotentParameterMismatchException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.EXIST_TRIGGER);
			LOGGER.debug(String.format("Error Occoured From IdempotentParameterMismatchException : %s", ex));
		} catch (AmazonServiceException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, ClientErrors.BAD_REQUEST,
					ex.getErrorMessage());
			LOGGER.debug(String.format("Error Occoured From AmazonServiceException : %s", ex));
		} catch (CannotGetJdbcConnectionException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.RULE_ENGINE_CREATOR_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.RULE_ENGINE_CREATOR_ERROR,
					appStatus.getMessage(AppModuleErrors.RULE_ENGINE_CREATOR_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.debug(String.format("Error Occoured From Exception : %s", e));
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally", "unused" })
	@RequestMapping(value = "job/run/ondemand", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> runOnDemand(@RequestBody JobDetail jDetail, ServletRequest request) {

		AppResponse appResponse = null;
		GetJobRunResult data = null;
		String startedOn = null;
		String lastEngineRun = null;
		S3BucketInfo s3BucketInfo = null;
		int result = 0;
		int throwError = 0;
		JobDetail jobDetail = jDetail;

		try {
			String token = ((HttpServletRequest) request).getHeader("X-Auth-Token");
			String enterpriseCode = CodeUtils.decode(token).get("ecode").getAsString();
			String userName = CodeUtils.decode(token).get("prn").getAsString();
			JsonArray orgDetails = CodeUtils.decode(token).get("ORG_DETAILS").getAsJsonArray();

			JsonObject jNode = orgDetails.get(0).getAsJsonObject();
			String orgId = jNode.get("orgId").getAsString();
			String orgCode = jNode.get("orgCode").getAsString();
			String pattern = jNode.get("ip_prefix").getAsString();
			String jobName = jobDetail.getJobName().subSequence(0, jobDetail.getJobName().lastIndexOf("-") + 1)
					+ pattern + "-JOB";
			jobDetail.setOrgId(orgId);
			jobDetail.setJobName(jobName);
			jobDetail.setUpdatedBy(userName);
			// Job run based on custom parameters
			// ----*START CODE *-----

			String folderName = UUID.randomUUID().toString();
			String bucketName = CodeUtils.decode(token).get(FileUtils.BucketName).getAsString();
			LOGGER.info("Retrive s3 Bucket Info for run on demand");
			for (int i = 0;; i++) {
				s3BucketInfo = s3BucketInfoService.getBucketByType(bucketName, FileUtils.FileDownload, orgId);
				if (!CodeUtils.isEmpty(s3BucketInfo)) {
					break;
				} else {
					try {
						Thread.sleep(2000);
						throwError += 2;
						if (throwError == 60) {
							throw new SupplyMintException();
						}
					} catch (Exception ex) {
						throw new SupplyMintException();
					}
				}
			}
			s3BucketInfo.setOutputFileName(folderName);
			String path = BucketParameter.BUCKETPREFIX + bucketName + "/" + s3BucketInfo.getBucketPath() + "/"
					+ s3BucketInfo.getOutputFileName();
			s3BucketInfo.setBucketKey(path);
			s3BucketInfo.setType(FileUtils.FileDownload);
			s3BucketInfo.setManualTransferPath(path + "/MANUAL_TRANSFER_ORDER");
			LOGGER.info("Obtaining s3 Bucket Info for run on demand");
			s3BucketInfo.setOrgId(orgId);
			s3BucketInfoService.updateBucketByType(s3BucketInfo);
			jobDetail.setFileName(folderName);

			LOGGER.info("Update s3 Bucket Info for run on demand");
			Map<String, String> arguments = new HashMap<>();
			Map<String, String> temArg = new HashMap<>();

			temArg.put(AWSUtils.CustomParam.STORE, jobDetail.getStore());
			temArg.put(AWSUtils.CustomParam.TRIGGER, AWSUtils.CustomParam.ONDEMAND);
			temArg.put(AWSUtils.CustomParam.STORECODE, jobDetail.getStoreCode());
			temArg.put(AWSUtils.CustomParam.PATH, path);
			temArg.put(AWSUtils.CustomParam.STOCKPOINT, jobDetail.getStockPoint());
			temArg.put(AWSUtils.CustomParam.PREFIX, pattern);

			String[] jobArguments = s3BucketInfo.getJobArguments().split(",");

			for (int i = 0; i < jobArguments.length; i++) {
				arguments.put(jobArguments[i], temArg.get(jobArguments[i]));
			}

			String glueAgruments = arguments.toString();
			LOGGER.debug("Putting arguments from the end user to map %s", glueAgruments);
			LOGGER.info("start run on demand on aws glue");
			data = awsGlueConfig.jobRunRequestWithCustomFields(jobDetail.getJobName(), arguments);

			LOGGER.info("Obtaining run on demand details from aws glue");
			// ----*END CODE *-----
			// data = awsGlueConfig.jobRunResult(jobDetail.getJobName());

			jobDetail.setJobId(data.getJobRun().getId());
			jobDetail.setJobName(data.getJobRun().getJobName());

			LOGGER.info("Validate job name with job name and last engine run calculate count");
			int validateJobName = jobDetailService.validateJobName(jobDetail.getJobName());
			if (validateJobName > 1) {
				String awsstartedOn = jobDetailService.getLastEngineRun(data.getJobRun().getId());
				startedOn = CodeUtils.convertISTtoNormalDateFormat(awsstartedOn);
				// lastEngineRun = CodeUtils._____dateFormat.format(new Date(startedOn));
				lastEngineRun = startedOn;
			} else {
				Date jobstartedOn = data.getJobRun().getStartedOn();
				String awsstartedOn = CodeUtils.convertDateOnTimeZonePluseForAWS(jobstartedOn, 5, 30).toString();
				startedOn = CodeUtils.convertISTtoNormalDateFormat(awsstartedOn);
				// lastEngineRun = CodeUtils._____dateFormat.format(new Date(startedOn));
				lastEngineRun = startedOn;
			}
			LOGGER.info("Update meta data ie:- update job id with remaining parameter");
			result = jobDetailService.updateMetaData(jobDetailService.storeJobRunMetaData(data));
			int updateFileName = jobDetailService.updateFileName(folderName, glueAgruments, data.getJobRun().getId());
			validateJobName = jobDetailService.validateJobName(jobDetail.getJobName());
			int updateOrgId = jobDetailService.updateOrgId(jobDetail.getOrgId(), jobDetail.getJobId());
			if (validateJobName == 2) {
				LOGGER.info("Update status last engine run to passed");
				int updateStatus = jobDetailService.updateStatus(jobDetail);
			}
			if (result != 0) {
				jobDetail = jobDetailService.getByJobId(data.getJobRun().getId());
				if (!CodeUtils.isEmpty(jobDetail)) {
					appResponse = new AppResponse.Builder()
							.data(getMapper().createObjectNode().put("lastEngineRun", lastEngineRun)
									.putPOJO(CodeUtils.RESPONSE, jobDetail)
									.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
							.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				} else {
					appResponse = buildErrorResponse(AppErrorCodes.GENERIC_EVENT_NOT_AVAILABLE_ERROR_CODE,
							ClientErrors.NOT_FOUND, AppStatusMsg.BLANK);
					LOGGER.info(data.getJobRun().getId());
				}
			}
		} catch (SupplyMintException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_JOB_MSG);
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
		} catch (ConcurrentRunsExceededException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.CONCURRENT_EXIST);
			LOGGER.debug(String.format("Error Occoured From ConcurrentRunsExceededException : %s", ex));
		} catch (AmazonServiceException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, ClientErrors.BAD_REQUEST,
					ex.getErrorMessage());
			LOGGER.debug(String.format("Error Occoured From AmazonServiceException : %s", ex));
		} catch (CannotGetJdbcConnectionException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.RULE_ENGINE_UPDATE_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.RULE_ENGINE_UPDATE_ERROR,
					appStatus.getMessage(AppModuleErrors.RULE_ENGINE_UPDATE_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_JOB_MSG);
			LOGGER.debug(String.format("Error Occoured From Exception : %s", e));
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally", "deprecation", "unused" })
	@RequestMapping(value = "job/stop/ondemand", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> stopOnDemand(@RequestBody JobDetail jobDetail, HttpServletRequest request) {

		AppResponse appResponse = null;
		BatchStopJobRunResult data = null;
		GetJobRunResult result = null;
		String state = null;
		int updateState = 0;
		try {
			String token = ((HttpServletRequest) request).getHeader("X-Auth-Token");
			String enterpriseCode = CodeUtils.decode(token).get("ecode").getAsString();
			JsonArray orgDetails = CodeUtils.decode(token).get("ORG_DETAILS").getAsJsonArray();

			JsonObject jNode = orgDetails.get(0).getAsJsonObject();
			String orgId = jNode.get("orgId").getAsString();
			String pattern = jNode.get("ip_prefix").getAsString();

			String jobName = jobDetail.getJobName().subSequence(0, jobDetail.getJobName().lastIndexOf("-") + 1)
					+ pattern + "-JOB";
			jobDetail.setJobName(jobName);

			LOGGER.info("Stop run on demand on aws glue");
			data = awsGlueConfig.stopJobRunRequest(jobDetail.getJobName(), jobDetail.getJobId());
			LOGGER.info("Retrive current state of a job from aws glue");
			result = awsGlueConfig.getJobRunResult(jobDetail.getJobId(), jobDetail.getJobName());
			state = result.getJobRun().getJobRunState();
			Date endOn = new Date();
			Date started = result.getJobRun().getStartedOn();
			String duration = CodeUtils.elapsedTime(started, endOn);
			int updateEndTimeWithRepDate = jobDetailService.updateEndTimeWithRepDate(jobDetail.getJobId(), endOn,
					duration);
			LOGGER.debug("Stop the job with status :-%s", state);
			if (!state.equals("RUNNING")) {
				state = "STOPPED";
				updateState = jobDetailService.updateState(jobDetail.getJobId(), jobDetail.getJobName(), state);
			}
			if (!CodeUtils.isEmpty(data)) {
				ObjectNode objectNode = getMapper().createObjectNode();
				ObjectNode node = getMapper().createObjectNode();
				node.put("state", state);

				objectNode.put(CodeUtils.RESPONSE, node);
				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
						.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG).putAll(objectNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
						AppModuleErrors.RULE_ENGINE_CREATOR_ERROR,
						appStatus.getMessage(AppModuleErrors.RULE_ENGINE_CREATOR_ERROR));
			}
		} catch (AmazonServiceException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, ClientErrors.BAD_REQUEST,
					ex.getErrorMessage());
			LOGGER.debug(String.format("Error Occoured From AmazonServiceException : %s", ex));
		} catch (CannotGetJdbcConnectionException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.RULE_ENGINE_CREATOR_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.RULE_ENGINE_CREATOR_ERROR,
					appStatus.getMessage(AppModuleErrors.RULE_ENGINE_CREATOR_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.debug(String.format("Error Occoured From Exception : %s", e));
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally" })
	@RequestMapping(value = "/job/get/jobName", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getByJobName(@RequestParam("jobName") String jName, HttpServletRequest request) {

		AppResponse appResponse = null;
		JobDetail data = null;
		try {
			String token = ((HttpServletRequest) request).getHeader("X-Auth-Token");
			JsonArray orgDetails = CodeUtils.decode(token).get("ORG_DETAILS").getAsJsonArray();

			JsonObject jNode = orgDetails.get(0).getAsJsonObject();
			String orgId = jNode.get("orgId").getAsString();
			String pattern = jNode.get("ip_prefix").getAsString();
			String jobName = jName.subSequence(0, jName.lastIndexOf("-") + 1) + pattern + "-JOB";
//			String previousJobId=jobDetailService.getPreviousJobId();
//			int updatePreviousLastEngine=jobDetailService.updatePreviousLastEngine(previousJobId);
			LOGGER.info("Get jobdetails by job name..");
			data = jobDetailService.getByJobName(jobName, orgId);
			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, data)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}
		} catch (CannotGetJdbcConnectionException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.RULE_ENGINE_READER_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.RULE_ENGINE_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.RULE_ENGINE_READER_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.debug(String.format("Error Occoured From Exception : %s", e));
		} finally {
			return ResponseEntity.ok(appResponse);
		}

	}

	@SuppressWarnings({ "finally", "deprecation", "unused" })
	@RequestMapping(value = "job/get/rundetail", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> getByState(@RequestBody JobDetail jobDetail, HttpServletRequest request) {
		// JobDetail result = null;
		AppResponse appResponse = null;
		int mailCounter = 0;
		int updateState = 0;
		int updateCount = 0;
		int updateSummary = 0;
		int updateTriggerName = 0;
		int percentage = 0;
		long diffMinutes = 1L;
		GetJobRunResult jobRunResult = null;
		GetJobResult jobResult = null;
		Date startedOn = null;
		Date lastModifiedOn = null;
		Date completedOn = null;
		String state = null;
		String lastEngineRun = null;
		String getlastEngineRun = null;
		String istDate = null;
		String elapsedTime = null;
		String triggerName = "RUN ON DEMAND";
		String jobId = jobDetail.getJobId();
		String jobName = jobDetail.getJobName();
		String bucketKey = null;
		ObjectNode objectNode = getMapper().createObjectNode();
		ObjectNode node = getMapper().createObjectNode();
		int response = 0;
		int maxTime = 0;
		String total = null;
		List<String> avarageTime = null;

		try {

			String token = ((HttpServletRequest) request).getHeader("X-Auth-Token");
			String enterpriseCode = CodeUtils.decode(token).get("ecode").getAsString();
			String enterpriseName = CodeUtils.decode(token).get("ename").getAsString();
			String userName = CodeUtils.decode(token).get("prn").getAsString();
			JsonArray orgDetails = CodeUtils.decode(token).get("ORG_DETAILS").getAsJsonArray();
			JsonObject jNode = orgDetails.get(0).getAsJsonObject();
			String orgId = jNode.get("orgId").getAsString();
			String orgCode = jNode.get("orgCode").getAsString();
			String pattern = jNode.get("ip_prefix").getAsString();
			jobName = jobName.subSequence(0, jobName.lastIndexOf("-") + 1) + pattern + "-JOB";
			jobDetail.setOrgId(orgId);

			String tenantHashkey = ThreadLocalStorage.getTenantHashKey();

			// @Author Manoj -- START , It will check for running jobID by any other user .
			JobDetail createJobDetail = new JobDetail();
			createJobDetail.setJobName(jobName);
			createJobDetail.setCreatedBy(userName);
			boolean createJob = jobDetailService.createJobs(createJobDetail);
			boolean checkRunByUser = false;
			LOGGER.info("Checking condition for active running job");
			List<JobDetail> activeJob = jobDetailService.getByState(jobName, RunState.RUNNING.toString());
			if (activeJob.size() != 0) {
				jobId = activeJob.get(0).getJobId();
				LOGGER.debug(String.format("Active JodID : %s", jobId));
				checkRunByUser = true;
			}
			// ---END --
			if (jobId.equals("NA")) {
				jobResult = awsGlueConfig.getJobDetails(jobName);
				lastModifiedOn = jobResult.getJob().getLastModifiedOn();
				// diffMinutes += 4L;
				elapsedTime = "0";
				state = "NOT STARTED";
			} else {
				// Author Manoj Singh --Start Code
				LOGGER.info("Retrive the running job details from glue...");
				jobRunResult = awsGlueConfig.getJobRunResult(jobId, jobName);
				JobDetail jobDetails = jobDetailService.storeJobRunMetaData(jobRunResult);
				LOGGER.info("Validate job Name where status last engine run count...");
				jobDetail.setJobName(jobName);
				int validateJobName = jobDetailService.validateJobName(jobDetail.getJobName());
				state = jobRunResult.getJobRun().getJobRunState();
				String previousJobId = jobDetailService.getPreviousJobId();
				LOGGER.info("Validate job Name where status last engine run count...");
				int updatePreviousLastEngine = jobDetailService.updatePreviousLastEngine(previousJobId,
						jobDetail.getJobName());
				JobDetail previousJobDetails = jobDetailService.getByJobName(jobName, jobDetail.getOrgId());
				boolean checkActiveEngine = false;
				LOGGER.info("Validate state is Running or not for run details api");
				int updateOrgId = jobDetailService.updateOrgId(jobDetail.getOrgId(), jobDetail.getJobId());
				if (!state.equalsIgnoreCase(RunState.RUNNING.toString())) {
					if (state.equalsIgnoreCase(RunState.STOPPING.toString())) {
						state = RunState.STOPPED.toString();
						Date endOn = new Date();
						Date started = jobRunResult.getJobRun().getStartedOn();
						String duration = CodeUtils.elapsedTime(started, endOn);
						int updateEndTimeWithRepDate = jobDetailService.updateEndTimeWithRepDate(jobId, endOn,
								duration);
					} else if (state.equalsIgnoreCase(RunState.FAILED.toString())) {
						try {
							int result = mailManagerService.getEmailStatus(jobDetail.getOrgId());
							S3BucketInfo s3BucketInfo = s3BucketInfoService.getBucketByType(GENERATE_TRANSFERORDER,
									orgId);

							int reqFileCount = s3BucketInfo.getReqMailCount();
							JobDetail jDetail = jobDetailService.getJobDetailByJobId(jobId, orgId);
							mailCounter = jDetail.getMailCounter();

							if (result != 0) {
								if (mailCounter < reqFileCount && mailCounter == 0) {
									jobDetailService.updateMailCounter(jobId, mailCounter + 1, orgId);
									jobDetailService.sendMail(EmailTemplateName.TO_FAILED_GENERATION.toString(), state,
											enterpriseName, jobId, orgId);
								}
							}
						} catch (Exception ex) {
							LOGGER.info(String.format("unable to generate email:-%s", ex));
						}
					}
					int updateCurrentLastEngine = jobDetailService.updateCurrentLastEngine(jobId);
					updateState = jobDetailService.updateState(jobId, jobName, state);
					if (validateJobName == 1) {
						istDate = jobDetailService.getLastEngineRun(jobId);
						if (istDate != null && istDate.contains("IST")) {
							Date date = CodeUtils._dateFormatIST.parse(istDate);
							lastEngineRun = CodeUtils._____dateFormat.format(date);
						}
					} else {
						if (validateJobName > 1) {
							int updateStatus = jobDetailService.updateStatus(jobDetail);
						}
						istDate = jobRunResult.getJobRun().getStartedOn().toString();
						if (istDate.contains("IST")) {
							Date date = CodeUtils._dateFormatIST.parse(istDate);
							lastEngineRun = CodeUtils._____dateFormat.format(date);
						}
					}
					checkActiveEngine = true;
					if (previousJobDetails != null) {
						if (state.equalsIgnoreCase(RunState.STOPPED.toString())
								|| state.equalsIgnoreCase(RunState.STOPPING.toString())
								|| state.equalsIgnoreCase(RunState.SUCCEEDED.toString())) {
							jobDetailService.updatePreviousEngine(previousJobDetails.getJobId());
						}
					}
					int updateLastEngine = jobDetailService.updateLastEngine(jobId, jobName);
				}
				// End Code
				LOGGER.info("Checking condition job details contains Trigger Name");
				String containsTrigger = jobRunResult.toString();
				if (containsTrigger.contains("TriggerName")) {
					validateJobName = jobDetailService.validateJobName(jobDetail.getJobName());
					if (validateJobName > 1) {
						int updateStatus = jobDetailService.updateStatus(jobDetail);
					}
					if (response != 0) {
						LOGGER.debug("JobRunDetails metaData Saved");
						node.put("scheduledAt", jobDetails.getSchedule());
					}
					JobTrigger trigger = jobDetailService.getByTriggerName(jobRunResult.getJobRun().getTriggerName(),
							jobDetail.getOrgId());

					Date awsschedule = CodeUtils.convertDateOnTimeZonePluseForAWS(new Date(trigger.getSchedule()), 5,
							30);
					String convertSchedule = CodeUtils.convertISTtoNormalDateFormat(awsschedule.toString()).toString();

					String schedule = CodeUtils.convertStandardToNormalDateFormat(convertSchedule);
					String schduleTriggerResult = awsGlueConfig.retrieveNextScheduled(trigger.getFrequency(), schedule,
							trigger.getTimeZone(), trigger.getDayWeek(), trigger.getDayMonth());

					trigger.setNextSchedule(schduleTriggerResult);
					int updateSchedule = jobDetailService.updateNextSchedule(schduleTriggerResult,
							jobRunResult.getJobRun().getTriggerName());
					triggerName = jobRunResult.getJobRun().getTriggerName();
					node.putPOJO("nextSchedule", trigger.getNextSchedule());
					updateTriggerName = jobDetailService.updateTriggerName(jobId, jobName, triggerName);

				} else {
					LOGGER.info("Update trigger name");
					updateTriggerName = jobDetailService.updateTriggerName(jobId, jobName, triggerName);
				}

				LOGGER.debug(String.format("JOB API Response : %s", jobRunResult));
				state = jobRunResult.getJobRun().getJobRunState();
				startedOn = jobRunResult.getJobRun().getStartedOn();
				LOGGER.info(jobRunResult.getJobRun().toString());
				lastModifiedOn = CodeUtils
						.convertDateOnTimeZonePluseForAWS(jobRunResult.getJobRun().getLastModifiedOn(), 5, 30);
				if (jobRunResult.getJobRun().getCompletedOn() != null) {
					completedOn = CodeUtils.convertDateOnTimeZonePluseForAWS(jobRunResult.getJobRun().getCompletedOn(),
							5, 30);
					elapsedTime = CodeUtils.elapsedTime(
							CodeUtils.convertDateOnTimeZonePluseForAWS(jobRunResult.getJobRun().getStartedOn(), 5, 30),
							completedOn);
				} else {
					elapsedTime = CodeUtils.elapsedTime(startedOn, completedOn);
				}
				if (Integer.parseInt(elapsedTime) > 0) {
					avarageTime = jobDetailService.fiveSucceededTime();
					for (String avgSum : avarageTime) {
						if (!CodeUtils.isEmpty(avgSum))
							maxTime += Integer.parseInt(avgSum);
					}
					int calculatedTime = maxTime / 5;
					if (calculatedTime != 0) {
						total = calculatedTime + "";
					} else {
						total = "10";
					}
					percentage = CodeUtils.calculatePercentage(elapsedTime, total);
					if (percentage > 95 && state.equals("RUNNING")) {
						percentage = 95;
					} else if (state.equals("SUCCEEDED")) {
						percentage = 100;
					}
				}

				/*
				 * Calendar calendar = null; String temp_duration = null; Date dd = null;
				 * JobDetail lastjobDetails = jobDetailService.getByJobId(jobId); if
				 * (lastjobDetails != null) { if (lastjobDetails.getTemp_duration() != null) {
				 * temp_duration = lastjobDetails.getTemp_duration(); String temp_dateTime =
				 * CodeUtils.convertISTtoNormalDateFormat(temp_duration); dd = new
				 * Date(temp_dateTime); } } if (temp_duration == null) { calendar = new
				 * GregorianCalendar(2018, Calendar.JANUARY, 1, 1, 3); int t_duration =
				 * jobDetailService.updateTemp_duration(jobId, jobName, "" +
				 * calendar.getTime()); } else { calendar = new GregorianCalendar(2018,
				 * Calendar.JANUARY, 1, 1, dd.getMinutes(), dd.getSeconds());
				 * calendar.add(Calendar.SECOND, -5); LOGGER.info(calendar.get(Calendar.MINUTE)
				 * + ""); if (calendar.get(Calendar.MINUTE) > 0) { String date =
				 * CodeUtils.____dateTimeFormat.format(calendar.getTime()); int t_duration =
				 * jobDetailService.updateTemp_duration(jobId, jobName, "" +
				 * calendar.getTime()); } else if (calendar.get(Calendar.MINUTE) == 0) {
				 * List<JobDetail> leftTime = jobDetailService.getLastFiveRecord(jobName); int
				 * num1 = 0; int count2 = 1; for (int i = 0; i < leftTime.size(); i++) { if
				 * (leftTime.get(i) != null) { String sum_dateTime = CodeUtils
				 * .convertISTtoNormalDateFormat(leftTime.get(i).getTemp_duration()); Date sum =
				 * new Date(sum_dateTime); num1 = num1 + sum.getMinutes();
				 * 
				 * int avgTime = num1 / count2; LOGGER.info(avgTime + ""); count2++; } } }
				 * 
				 * }
				 */

				LOGGER.info("Condition if state succeeded for run details api");
				if (state.equals("SUCCEEDED")) {

					pattern = jobName.substring(jobName.indexOf("IP"), jobName.lastIndexOf("-"));
					String tableName = "REP_ALLOC_OUTPUT_" + pattern;

					long difference = completedOn.getTime() - CodeUtils
							.convertDateOnTimeZonePluseForAWS(jobRunResult.getJobRun().getStartedOn(), 5, 30).getTime();
					diffMinutes = difference / (60 * 1000) % 60;
					String duration = Long.toString(diffMinutes);
					jobDetail.setDuration(duration);
					jobDetail.setCompleteOn(completedOn);

					/*
					 * @Production code for global job
					 */

					int storeCount = jobDetailService.globalStoreCount(tableName);
					jobDetail.setStoreCount(storeCount);
					int itemCount = jobDetailService.globalitemCount(tableName);
					jobDetail.setItemCount(itemCount);
					String tCount = jobDetailService.globaltotalCount(tableName);

					if (tCount == null) {
						int totalCount = 0;
						jobDetail.setTotalCount(totalCount);
					} else {
						jobDetail.setTotalCount(Integer.parseInt(tCount));
					}
					Date updationTime = new Date();
					jobDetail.setRepDate(CodeUtils.convertDateOnTimeZonePluseForAWS(updationTime, 5, 30));
					jobDetail.setJobId(jobId);
					int validateSummaryUpdate = jobDetailService.validateSummaryUpdate(jobDetail);
					if (validateSummaryUpdate > 1) {
						updateSummary = jobDetailService.updateSummary(jobDetail);
					} else {
						jobDetail.setSummary("TRUE");
						updateSummary = jobDetailService.updateSummary(jobDetail);
					}
					updateCount = jobDetailService.globalupdateCount(jobDetail);

					/*
					 * End @production code
					 */

				}

			}
			LOGGER.info("Conditions if jobId is equals to NA for run details api");
			if (jobId.equalsIgnoreCase("NA") || checkRunByUser) {
				// @Author Manoj Singh
				// Retrieve JobID and JobName
				Map<String, String> jobRunDetailsMap = awsGlueConfig.getJobRunsResult(jobName);
				if (jobRunDetailsMap.containsKey(jobName)) {
					jobRunResult = awsGlueConfig.getJobRunResult(jobRunDetailsMap.get(jobName), jobName);
					startedOn = CodeUtils.convertDateOnTimeZonePluseForAWS(jobRunResult.getJobRun().getStartedOn(), 5,
							30);
					istDate = startedOn.toString();
					state = jobRunResult.getJobRun().getJobRunState();

					lastEngineRun = CodeUtils._____dateFormat.format(startedOn);
					node.putPOJO("jobId", jobRunDetailsMap.get(jobName));

					String containsTrigger = jobRunResult.toString();
					if (containsTrigger.contains("TriggerName")) {
						triggerName = jobRunResult.getJobRun().getTriggerName();
						JobDetail jobDetails = jobDetailService.storeJobRunMetaData(jobRunResult);
						jobDetail.setJobName(jobName);
//						int updateLastEngineRun=jobDetailService.updateLastEngineRun();
						int validateRow = jobDetailService.validateRow(jobRunDetailsMap.get(jobName));
						if (validateRow == 0) {
							String configDate = CodeUtils.________dateTimeFormat.format(new Date());
							String Key = jobDetails.getBucketKey() + CustomParameter.DELIMETER + configDate;
							jobDetails.setBucketKey(Key);
							String manualTransferOrder = Key + "/MANUAL_TRANSFER_ORDER";
							updateState = jobDetailService.updateStatus(jobDetails);
							jobDetails.setUpdatedBy(userName);
							response = jobDetailService.updateMetaData(jobDetails);
							int updateManualTransferOrder = s3BucketInfoService
									.updateManualTransferOrder(manualTransferOrder, FileUtils.FileDownload);
						}
					}

				} else {
					node.putPOJO("jobId", jobId);
					// End
				}

			} else {
				jobRunResult = awsGlueConfig.getJobRunResult(jobDetail.getJobId(), jobName);
				startedOn = CodeUtils.convertDateOnTimeZonePluseForAWS(jobRunResult.getJobRun().getStartedOn(), 5, 30);
				state = jobRunResult.getJobRun().getJobRunState();

				lastEngineRun = CodeUtils._____dateFormat.format(startedOn);
				node.put("jobId", jobDetail.getJobId());
			}
			node.put("lastEngineRun", lastEngineRun);
			node.put("timeTaken", elapsedTime);
			node.put("state", state);
			node.put("triggerName", triggerName);
			node.put("percentage", percentage);
			objectNode.put(CodeUtils.RESPONSE, node);
			if (!CodeUtils.isEmpty(jobRunResult)) {
				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
						.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG).putAll(objectNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else if (!CodeUtils.isEmpty(jobResult)) {
				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
						.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG).putAll(objectNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
						AppModuleErrors.RULE_ENGINE_READER_ERROR,
						appStatus.getMessage(AppModuleErrors.RULE_ENGINE_READER_ERROR));
			}

		} catch (ConcurrentRunsExceededException ex) {
			ex.printStackTrace();
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.CONCURRENT_EXIST);
			LOGGER.debug(String.format("Error Occoured From ConcurrentRunsExceededException : %s", ex));
		} catch (EntityNotFoundException ex) {
			ex.printStackTrace();
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.ENTITY_NOT_FOUND);
			LOGGER.debug(String.format("Error Occoured From EntityNotFoundException : %s", ex));
		} catch (AmazonServiceException ex) {
			ex.printStackTrace();
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, ClientErrors.BAD_REQUEST,
					ex.getErrorMessage());
			LOGGER.debug(String.format("Error Occoured From AmazonServiceException : %s", ex));
		} catch (CannotGetJdbcConnectionException ex) {
			ex.printStackTrace();
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.RULE_ENGINE_READER_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
		} catch (DataAccessException ex) {
			ex.printStackTrace();
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.RULE_ENGINE_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.RULE_ENGINE_READER_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception ex) {
			ex.printStackTrace();
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally", "deprecation" })
	@RequestMapping(value = "job/stop/trigger", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> stopTrigger(@RequestBody JobTrigger jobTrigger) {

		AppResponse appResponse = null;
		StopTriggerResult stopTriggerResult = null;

		try {
			LOGGER.debug("Stop scheduled trigger with name :%s", jobTrigger.getTriggerName());
			stopTriggerResult = awsGlueConfig.stopTrigger(jobTrigger.getTriggerName());
			if (!CodeUtils.isEmpty(stopTriggerResult)) {
				ObjectNode objectNode = getMapper().createObjectNode();
				ObjectNode node = getMapper().createObjectNode();

				node.put("triggerName", stopTriggerResult.getName());
				objectNode.put(CodeUtils.RESPONSE, node);
				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
						.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG).putAll(objectNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
						AppModuleErrors.RULE_ENGINE_READER_ERROR,
						appStatus.getMessage(AppModuleErrors.RULE_ENGINE_READER_ERROR));
			}

		} catch (AmazonServiceException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, ClientErrors.BAD_REQUEST,
					ex.getErrorMessage());
			LOGGER.debug(String.format("Error Occoured From AmazonServiceException : %s", ex));
		} catch (CannotGetJdbcConnectionException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.RULE_ENGINE_READER_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
		} catch (DataAccessException ex) {
			ex.printStackTrace();
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.RULE_ENGINE_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.RULE_ENGINE_READER_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception e) {
			e.printStackTrace();
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.debug(String.format("Error Occoured From Exception : %s", e));
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally" })
	@RequestMapping(value = "/job/get/triggerDetail", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> getTriggerDetails(@RequestBody JobTrigger jobTrigger,
			HttpServletRequest request) {

		// get triggerDetails(including next schedule) by triggerName
		AppResponse appResponse = null;
		JobTrigger data = null;
		try {
			String token = ((HttpServletRequest) request).getHeader("X-Auth-Token");
			JsonArray orgDetails = CodeUtils.decode(token).get("ORG_DETAILS").getAsJsonArray();

			JsonObject jNode = orgDetails.get(0).getAsJsonObject();
			String orgId = jNode.get("orgId").getAsString();
			LOGGER.debug("Get trigger details with name :%s", jobTrigger.getTriggerName());
			data = jobDetailService.getByTriggerName(jobTrigger.getTriggerName(), orgId);
			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, data)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}
		} catch (CannotGetJdbcConnectionException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.RULE_ENGINE_READER_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.RULE_ENGINE_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.RULE_ENGINE_READER_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.debug(String.format("Error Occoured From Exception : %s", e));
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally" })
	@RequestMapping(value = "/job/getLastJob/summary", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> getLastSummary(@RequestBody JobDetail jobDetail, HttpServletRequest request) {

		AppResponse appResponse = null;
		JobDetail data = null;
		try {
			String token = ((HttpServletRequest) request).getHeader("X-Auth-Token");
			String enterpriseCode = CodeUtils.decode(token).get("ecode").getAsString();
			JsonArray orgDetails = CodeUtils.decode(token).get("ORG_DETAILS").getAsJsonArray();
			JsonObject jNode = orgDetails.get(0).getAsJsonObject();
			String orgId = jNode.get("orgId").getAsString();
			String pattern = jNode.get("ip_prefix").getAsString();
			String jobName = jobDetail.getJobName().subSequence(0, jobDetail.getJobName().lastIndexOf("-") + 1)
					+ pattern + "-JOB";
			LOGGER.debug("Get job details with name :%s", jobDetail.getJobName());
			data = jobDetailService.getLastSummary(jobName, orgId);
			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, data)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SUCCESS_CODE, ClientErrors.NOT_FOUND,
						AppStatusMsg.BLANK);
			}
		} catch (CannotGetJdbcConnectionException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.RULE_ENGINE_READER_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.RULE_ENGINE_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.RULE_ENGINE_READER_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.debug(String.format("Error Occoured From Exception : %s", e));
		} finally {

			return ResponseEntity.ok(appResponse);
		}
	}
	// get all

	@SuppressWarnings({ "deprecation", "finally" })
	@RequestMapping(value = "/job/getLastJob/Sdetails", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getAllSummary(@RequestParam("pageno") Integer pageNo,
			@RequestParam("type") Integer type, @RequestParam("storecode") String storeCode,
			HttpServletRequest request) {
		AppResponse appResponse = null;
		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;
		int flag = 0;
		List<HeadersSpecificOutput> data = null;
		String tableName = null;
		try {

			String token = ((HttpServletRequest) request).getHeader("X-Auth-Token");
			String enterpriseCode = CodeUtils.decode(token).get("ecode").getAsString();
			JsonArray orgDetails = CodeUtils.decode(token).get("ORG_DETAILS").getAsJsonArray();

			JsonObject jNode = orgDetails.get(0).getAsJsonObject();
			String orgId = jNode.get("orgId").getAsString();
			String pattern = jNode.get("ip_prefix").getAsString();
			String enviroment = environment.getActiveProfiles()[0].equalsIgnoreCase(Profiles.DEV.name()) ? "DEV"
					: "PROD";

			flag = type;
			String displayName = "ALLOCATION SUMMARY";

			String jobName = "SMGLUE-" + enterpriseCode + "-" + enviroment + "-" + pattern + "-JOB";

			if (displayName.equalsIgnoreCase("ALLOCATION SUMMARY")) {
				tableName = "REP_ALLOC_OUTPUT_" + pattern;
				if (flag == 1) {
					LOGGER.debug("Retrive job summary details get all");
					totalRecord = jobDetailService.globalrecordRAO(tableName);
					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo - 1) * pageSize;
					data = jobDetailService.globalAllRAO(offset, pageSize, tableName);
					LOGGER.debug("Obtaining job summary details get all:-%d", data.size());
				} else if (flag == 2) {
					LOGGER.debug("Retrive job summary details filter");
					totalRecord = jobDetailService.globalSummaryFilterRecord(storeCode, tableName);
					// totalRecord=10;
					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo - 1) * pageSize;
					data = jobDetailService.globalSummaryFilter(offset, pageSize, storeCode, tableName);
					LOGGER.debug("Obtaining job summary details filter:-", data.size());
				}
			} else if (displayName.equalsIgnoreCase("REQUIREMENT SUMMARY")) {
				tableName = "REP_REQ_OUTPUT_" + pattern;
				if (flag == 1) {
					LOGGER.debug("Retrive job summary details get all");
					totalRecord = jobDetailService.globalrecordReq(tableName);
					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo - 1) * pageSize;
					data = jobDetailService.globalAllReq(offset, pageSize, tableName);
					LOGGER.debug("Obtaining job summary details get all:-%d", data.size());
				} else if (flag == 2) {
					LOGGER.debug("Retrive job summary details filter");
					totalRecord = jobDetailService.globalSummaryFilterRecordReq(storeCode, tableName);
					// totalRecord=10;
					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo - 1) * pageSize;
					data = jobDetailService.globalSummaryFilterReq(offset, pageSize, storeCode, tableName);
					LOGGER.debug("Obtaining job summary details filter:-", data.size());
				}
			}
			ObjectNode objectNode = getMapper().createObjectNode();
			ArrayNode array = getMapper().createArrayNode();
			data.forEach(e -> {
				ObjectNode node = getMapper().valueToTree(e);
				array.add(node);
			});

			objectNode.put(CodeUtils.RESPONSE, array);
			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(Pagination.CURRENT_PAGE, pageNo)
								.put(Pagination.PREVIOUS_PAGE, previousPage).put(Pagination.MAXIMUM_PAGE, maxPage)
								.put(CodeUtils.RESPONSE_MSG, "").putAll(objectNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				LOGGER.info(AppStatusMsg.BLANK);
			}
		} catch (CannotGetJdbcConnectionException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.RULE_ENGINE_READER_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.RULE_ENGINE_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.RULE_ENGINE_READER_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception ex) {
			ex.printStackTrace();
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally" })
	@RequestMapping(value = "/job/get/storecode/name", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getAllStoreCode(HttpServletRequest request) {

		AppResponse appResponse = null;
		List<String> data = null;
		try {

			String token = ((HttpServletRequest) request).getHeader("X-Auth-Token");
			JsonArray orgDetails = CodeUtils.decode(token).get("ORG_DETAILS").getAsJsonArray();

			JsonObject jNode = orgDetails.get(0).getAsJsonObject();
			String pattern = jNode.get("ip_prefix").getAsString();

//			String jobName="SMGLUE-"+enterpriseCode+"-"+enviroment+"-"+pattern+"-JOB";
			String tableName = "REP_ALLOC_OUTPUT_" + pattern;

			LOGGER.debug("Retrive job history store code get all");
			data = jobDetailService.globalAllStoreCode(tableName);
			LOGGER.debug("Obtaining job history store code get all :-%d", data.size());
			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, data)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = buildErrorResponse(AppErrorCodes.GENERIC_EVENT_NOT_AVAILABLE_ERROR_CODE,
						ClientErrors.NOT_FOUND, AppStatusMsg.BLANK);
				LOGGER.info(AppStatusMsg.BLANK);
			}
		} catch (CannotGetJdbcConnectionException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.RULE_ENGINE_READER_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.RULE_ENGINE_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.RULE_ENGINE_READER_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.debug(String.format("Error Occoured From Exception : %s", e));
		} finally {
			return ResponseEntity.ok(appResponse);
		}

	}

	@SuppressWarnings({ "finally", "deprecation" })
	@RequestMapping(value = "job/delete/trigger", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> deleteTrigger(@RequestBody JobTrigger jobTrigger) {

		AppResponse appResponse = null;
		DeleteTriggerResult deleteTriggerResult = null;
		try {
			LOGGER.debug("Delete trigger with job trigger name is - : %s", jobTrigger.getTriggerName());
			deleteTriggerResult = awsGlueConfig.deleteTriggerResult(jobTrigger.getTriggerName());
			if (!CodeUtils.isEmpty(deleteTriggerResult)) {
				ObjectNode objectNode = getMapper().createObjectNode();
				ObjectNode node = getMapper().createObjectNode();
				node.put("triggerName", deleteTriggerResult.getName());
				objectNode.put(CodeUtils.RESPONSE, node);
				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
						.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG).putAll(objectNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
						AppModuleErrors.RULE_ENGINE_READER_ERROR,
						appStatus.getMessage(AppModuleErrors.RULE_ENGINE_READER_ERROR));
			}
		} catch (AmazonServiceException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, ClientErrors.BAD_REQUEST,
					ex.getErrorMessage());
			LOGGER.debug(String.format("Error Occoured From AmazonServiceException : %s", ex));
		} catch (CannotGetJdbcConnectionException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.RULE_ENGINE_DELETE_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.RULE_ENGINE_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.RULE_ENGINE_READER_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.debug(String.format("Error Occoured From Exception : %s", e));
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	// @Author : Manoj Singh
	@SuppressWarnings("unused")
	@RequestMapping(value = "/generate/transfer/order/test/{tenant_hashkey}", method = RequestMethod.GET)
	public ResponseEntity<AppResponse> uploadSummaryZipTest(
			@PathVariable(value = "tenant_hashkey") String tenantHashKey, HttpServletRequest request)
			throws UnknownHostException {
		AppResponse appResponse = null;
		MailManagerViewLog mailManagerViewLog = new MailManagerViewLog();
		String ipAddress = InetAddress.getLocalHost().getHostAddress().trim().toString();
		ExecutorService executorService = Executors.newSingleThreadExecutor();
		executorService.execute(new Runnable() {
			@SuppressWarnings({ "unchecked", "rawtypes" })
			@Override
			public void run() {
				AppResponse appResponse = null;
				ThreadLocalStorage.setTenantHashKey(tenantHashKey);
				LOGGER.info("Genarate transfer Order method initiated ....");
				List<String> whData = null;
				File zipFile = null;
				File file = null;
				String fileName = null;
				String filePath = null;
				String filePath1 = null;
				String filePath2 = null;
				String etag = null;
				String[] cc = null;
				String[] to = null;
				String isZipped = null;
				boolean isSendEmail = false;
				int threadPoolCount = 1;
				String randomFolder = UUID.randomUUID().toString();

				String token = ((HttpServletRequest) request).getHeader("X-Auth-Token");
				JsonArray orgDetails = CodeUtils.decode(token).get("ORG_DETAILS").getAsJsonArray();

				JsonObject jNode = orgDetails.get(0).getAsJsonObject();
				String orgId = jNode.get("orgId").getAsString();

				S3BucketInfo s3BucketInfo = s3BucketInfoService.getBucketByType(GENERATE_TRANSFERORDER, orgId);

//				List<JobRun> jobRunsResults = awsGlueConfig.listAllJobRunsResult(s3BucketInfo.getJobName());
//				String getLastAwsJobId=jobRunsResults.get(0).getId();

				for (int threadCount = 0; threadCount < threadPoolCount; threadCount++) {

					JobDetail jobDetail = jobDetailService.getLastEngineRunDetails(s3BucketInfo.getJobName());
					int mailCounter = jobDetail.getMailCounter();
					boolean isSucceeded = jobDetail.getJobRunState().equalsIgnoreCase(RunState.RUNNING.toString())
							? false
							: jobDetail.getJobRunState().equalsIgnoreCase(RunState.SUCCEEDED.toString()) ? true : false;
					LOGGER.debug(String.format("Current JobID : %s  and JobRunState : %s", jobDetail.getJobId(),
							jobDetail.getJobRunState()));
					try {
						if (isSucceeded) {
							String jobName = jobDetail.getJobName();
							String pattern = jobName.substring(jobName.indexOf("IP"), jobName.lastIndexOf("-"));
							String tableName = "REP_ALLOC_OUTPUT_" + pattern;
//							if (jobDetail.getJobId().equals(getLastAwsJobId)) {
							LOGGER.debug("Running job is SUCCEEDED");
							if (!CodeUtils.isEmpty(s3BucketInfo.getS3bucketName())) {
								String s3BucketFilePath = s3BucketInfo.getBucketPath() + CustomParameter.DELIMETER
										+ randomFolder;
								LOGGER.info("Bucket info : {} ", s3BucketInfo);
								try {

									// Code for Nysaa Start
									if (tenantHashKey.equalsIgnoreCase(
											"5b72fb9c68fc88b920c03ed4e701eb9642deeecb34e456b8badc11b54a164ba2")) {

										List downloadExcelSummaryNysaa1 = null;
										List downloadExcelSummaryNysaa2 = null;
										fileName = s3BucketInfo.getEnterprise() + CustomParameter.SEPARATOR
												+ FileUtils.GenerateTransferOrder + CustomParameter.SEPARATOR
												+ CodeUtils.convertDateToString(new Date());
										File[] excelFiles = new File[2];

										String savedPath1 = File.separator + s3BucketInfo.getEnterprise()
												+ File.separator + "EXCEL" + File.separator + "REQUIREMENT"
												+ FileUtils.EXCELExtension;

										String savedPath2 = File.separator + s3BucketInfo.getEnterprise()
												+ File.separator + "EXCEL" + File.separator + "OLD_BARCODE_ALLOCATION"
												+ FileUtils.EXCELExtension;

										filePath1 = FileUtils.createFilePath(FileUtils.getPlatformBasedParentDir(),
												savedPath1);
										filePath2 = FileUtils.createFilePath(FileUtils.getPlatformBasedParentDir(),
												savedPath2);
										downloadExcelSummaryNysaa1 = jobDetailService.downloadExcelSummaryForNysaa1();
										ExcelUtils.writeToExcel(filePath1, downloadExcelSummaryNysaa1);

										downloadExcelSummaryNysaa2 = jobDetailService.downloadExcelSummaryForNysaa2();
										ExcelUtils.writeToExcel(filePath2, downloadExcelSummaryNysaa2);
										excelFiles[0] = new File(filePath1);
										excelFiles[1] = new File(filePath2);

										isZipped = s3BucketInfo.getCreateZip();
										if (isZipped.equalsIgnoreCase(Conditions.TRUE.toString())) {

											file = new File(filePath1.replace(FileUtils.EXCELExtension,
													FileUtils.ZIPExtension));
											CodeUtils.createZip(file, excelFiles);
											etag = s3Wrapper.upload(file,
													s3BucketFilePath + CustomParameter.DELIMETER + fileName
															+ FileUtils.ZIPExtension,
													s3BucketInfo.getS3bucketName()).getETag();
										}
										// Code for Nysaa End

										// code for megashop
									} else if (tenantHashKey.equalsIgnoreCase(
											"fd40841a596821e2b0d5efff6b7a69f6bf8fe5bdbb525fbac609c22d2dfd93f3")) {
										s3BucketInfo = s3BucketInfoService.getBucketByType(FileUtils.FileDownload,
												orgId);
										String manualPath = s3BucketInfo.getManualTransferPath();
										manualPath = manualPath.split("\\//")[1].replaceAll(
												s3BucketInfo.getS3bucketName() + CustomParameter.DELIMETER, "")
												+ CustomParameter.DELIMETER.trim().toString();
										manualPath = manualPath.substring(0, manualPath.length() - 1);
										file = s3Wrapper.getListFileFromS3Bucket(s3BucketInfo.getS3bucketName(),
												manualPath);

										etag = s3Wrapper.upload(file,
												s3BucketFilePath + CustomParameter.DELIMETER + "TRANSFER_ORDER"
														+ FileUtils.ZIPExtension,
												s3BucketInfo.getS3bucketName()).getETag();
										s3BucketInfo = s3BucketInfoService.getBucketByType(GENERATE_TRANSFERORDER,
												orgId);

										// code for citykart
									} else if (tenantHashKey.equalsIgnoreCase(
											"2f197d441a3e6aa97a736485efdb4c7810b20ccafdac5340f34064113f2ae120")) {
										s3BucketInfo = s3BucketInfoService.getBucketByType(FileUtils.FileDownload,
												orgId);
										String manualPath = s3BucketInfo.getManualTransferPath();
										manualPath = manualPath.split("\\//")[1].replaceAll(
												s3BucketInfo.getS3bucketName() + CustomParameter.DELIMETER, "")
												+ CustomParameter.DELIMETER.trim().toString();
										manualPath = manualPath.substring(0, manualPath.length() - 1);
										file = s3Wrapper.getListFileFromS3Bucket(s3BucketInfo.getS3bucketName(),
												manualPath);

										etag = s3Wrapper.upload(file,
												s3BucketFilePath + CustomParameter.DELIMETER + "TRANSFER_ORDER"
														+ FileUtils.ZIPExtension,
												s3BucketInfo.getS3bucketName()).getETag();
										s3BucketInfo = s3BucketInfoService.getBucketByType(GENERATE_TRANSFERORDER,
												orgId);

									} else {
										// Previous Code start
										whData = jobDetailService.getWhCode(tableName);
										if (!CodeUtils.isEmpty(whData)) {
											File[] excelFiles = new File[whData.size()];
											for (int i = 0; i < whData.size(); i++) {
												fileName = s3BucketInfo.getAdditional() + CustomParameter.SEPARATOR
														+ CHANELPARTNER + CustomParameter.SEPARATOR
														+ CodeUtils.___________dateTimeFormat.format(new Date());
												List downloadExcelSummary = null;
												if (tenantHashKey.equalsIgnoreCase(
														"c319c3c2c08b3ad130d6f25c41343d26d9f98e8dff5662d79b19877d3305b9f4"))
													downloadExcelSummary = jobDetailService
															.downloadExcelSummaryForSkecters(whData.get(i));
												else
													downloadExcelSummary = jobDetailService
															.downloadExcelSummary(whData.get(i));
												String savedPath = File.separator + s3BucketInfo.getEnterprise()
														+ File.separator + "EXCEL" + File.separator + fileName
														+ FileUtils.EXCELExtension;
												filePath = FileUtils.createFilePath(
														FileUtils.getPlatformBasedParentDir(), savedPath);
												ExcelUtils.writeToExcel(filePath, downloadExcelSummary);
												excelFiles[i] = new File(filePath);
											}
											isZipped = s3BucketInfo.getCreateZip();
											if (isZipped.equalsIgnoreCase(Conditions.TRUE.toString())) {
												file = new File(filePath.replace(FileUtils.EXCELExtension,
														FileUtils.ZIPExtension));
												CodeUtils.createZip(file, excelFiles);
												etag = s3Wrapper.upload(file,
														s3BucketFilePath + CustomParameter.DELIMETER + fileName
																+ FileUtils.ZIPExtension,
														s3BucketInfo.getS3bucketName()).getETag();
											} else {
												file = new File(filePath);
												etag = s3Wrapper.upload(file,
														s3BucketFilePath + CustomParameter.DELIMETER + fileName
																+ FileUtils.EXCELExtension,
														s3BucketInfo.getS3bucketName()).getETag();
											}
										}
										// Previous Code end
									}
									// Previous Code out of the block common for all case

									if (!CodeUtils.isEmpty(etag)) {
										JobDetail jobDetails = jobDetailService
												.getLastSummary(s3BucketInfo.getJobName(), jobDetail.getOrgId());
										s3BucketFilePath = BucketParameter.BUCKETPREFIX + s3BucketInfo.getS3bucketName()
												+ CustomParameter.DELIMETER + s3BucketFilePath;
										int respCode = jobDetailService.updateTransferOrderKey(jobDetails.getJobId(),
												s3BucketFilePath, mailCounter + 1);
										if (respCode == 1) {
											appResponse = new AppResponse.Builder()
													.data(getMapper().createObjectNode()
															.putPOJO(CodeUtils.RESPONSE, s3BucketFilePath)
															.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
													.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
											LOGGER.info(
													"zip is created and successfully updated on defined bucket with bucket key  : {} ",
													s3BucketFilePath);
										}
										int result = mailManagerService.getEmailStatus(jobDetail.getOrgId());
										int reqFileCount = s3BucketInfo.getReqMailCount();
										if (result != 0 && mailCounter + 1 == reqFileCount) {

											MailManager mailManager = mailManagerService.getByStatus(
													EmailTemplateName.TO_SUCCESS_GENERATION.toString(),
													CustomRunState.SUCCESS.toString(), jobDetail.getOrgId());
											MailManagerViewLog mailManagerViewLog = new MailManagerViewLog();
											if (!CodeUtils.isEmpty(mailManager)) {
												if (mailManager.getCc() != null) {
													cc = mailManager.getCc().trim().split(",");
												}
												if (mailManager.getTo() != null) {
													to = mailManager.getTo().trim().split(",");
												}
												mailManagerViewLog.setCc(mailManager.getCc());
												mailManagerViewLog.setBcc(mailManager.getBcc());
												mailManagerViewLog.setTo(mailManager.getTo());
												mailManagerViewLog.setCreatedOn(
														CodeUtils.convertDateOnTimeZonePluseForAWS(new Date(), 5, 30));
												mailManagerViewLog.setDeliveredOn(
														CodeUtils.convertDateOnTimeZonePluseForAWS(new Date(), 5, 30));
												mailManagerViewLog
														.setBody(EmailTemplateName.TO_SUCCESS_GENERATION.toString());
												mailManagerViewLog.setModule(Module.INVENTRY_PLANNING.toString());
												mailManagerViewLog.setSubModule(SubModule.RUN_ON_DEMAND.toString());
												mailManagerViewLog.setIpAddress(ipAddress);
												if (!CodeUtils.isEmpty(mailManager)) {
													SimpleMailMessage mailMessage = new SimpleMailMessage();
													mailMessage.setCc(cc);
													mailMessage.setTo(to);
													mailMessage.setBcc(mailManager.getBcc());
													mailMessage.setSubject(mailManager.getSubject());
													Date updationTime = new Date();
													String updatedOn = CodeUtils._____dateFormat.format(CodeUtils
															.convertDateOnTimeZonePluseForAWS(updationTime, 5, 30));

													s3BucketFilePath = s3BucketFilePath.split("\\//")[1].replaceAll(
															s3BucketInfo.getS3bucketName() + CustomParameter.DELIMETER,
															"") + CustomParameter.DELIMETER.trim().toString();
													String url = s3Wrapper.downloadPreSignedURLOnHistory(
															s3BucketInfo.getS3bucketName(), s3BucketFilePath,
															Integer.parseInt(s3BucketInfo.getTime()));
													emailService.sendEmailUsingMailTemplate(mailMessage,
															mailManager.getTemplateName(), s3BucketInfo.getEnterprise(),
															jobDetail.getJobId(), updatedOn, url);
													isSendEmail = true;
												}
												if (isSendEmail) {
													mailManagerViewLog.setStatus(RunState.SUCCEEDED.toString());
													int createLog = mailManagerService.createEmailActivityLog(
															mailManagerViewLog, jobDetail.getOrgId());
													jobDetailService.updateMailStatus(jobDetail.getJobId());
												}
											}

										}

									}
									// End

								} catch (Exception e) {
									int result = mailManagerService.getEmailStatus(jobDetail.getOrgId());
									if (result != 0) {
										MailManager mailManager = mailManagerService.getByStatus(
												EmailTemplateName.TO_FAILED_GENERATION.toString(),
												CustomRunState.FAILED.toString(), jobDetail.getOrgId());
//										MailManagerViewLog mailManagerViewLog = new MailManagerViewLog();
										if (!CodeUtils.isEmpty(mailManager)) {
											if (mailManager.getCc() != null) {
												cc = mailManager.getCc().trim().split(",");
											}
											if (mailManager.getTo() != null) {
												to = mailManager.getTo().trim().split(",");
											}
											mailManagerViewLog.setCc(mailManager.getCc());
											mailManagerViewLog.setBcc(mailManager.getBcc());
											mailManagerViewLog.setTo(mailManager.getTo());
											mailManagerViewLog.setCreatedOn(
													CodeUtils.convertDateOnTimeZonePluseForAWS(new Date(), 5, 30));
											mailManagerViewLog.setDeliveredOn(
													CodeUtils.convertDateOnTimeZonePluseForAWS(new Date(), 5, 30));
											mailManagerViewLog
													.setBody(EmailTemplateName.TO_FAILED_GENERATION.toString());
											mailManagerViewLog.setModule(Module.INVENTRY_PLANNING.toString());
											mailManagerViewLog.setSubModule(SubModule.RUN_ON_DEMAND.toString());
											mailManagerViewLog.setIpAddress(ipAddress);
											if (!CodeUtils.isEmpty(mailManager)) {
												SimpleMailMessage mailMessage = new SimpleMailMessage();
												mailMessage.setCc(cc);
												mailMessage.setTo(to);
												mailMessage.setBcc(mailManager.getBcc());
												mailMessage.setSubject(mailManager.getSubject());
												Date updationTime = new Date();
												String updatedOn = CodeUtils._____dateFormat.format(CodeUtils
														.convertDateOnTimeZonePluseForAWS(updationTime, 5, 30));

												String url = null;
												emailService.sendEmailUsingMailTemplate(mailMessage,
														mailManager.getTemplateName(), s3BucketInfo.getEnterprise(),
														jobDetail.getJobId(), updatedOn, url);
												isSendEmail = true;
											}
											if (isSendEmail) {
												mailManagerViewLog.setStatus(RunState.SUCCEEDED.toString());
												int createLog = mailManagerService.createEmailActivityLog(
														mailManagerViewLog, jobDetail.getOrgId());
												jobDetailService.updateMailStatus(jobDetail.getJobId());
											}
										}
									}
									e.printStackTrace();
								}
							} else {
								LOGGER.debug("Bucket info not defined ..");
								appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
										AppModuleErrors.RULE_ENGINE_CREATOR_ERROR, AppStatusMsg.JOB_FAILED);
							}
							break;
//							}

						} else {
							threadPoolCount++;
							LOGGER.debug(String.format("JobRunState : %s , setting thread pool to sleep for 60 Sec ",
									jobDetail.getJobRunState()));
							Thread.sleep(60 * 1000);
							LOGGER.debug("Thread waked up after 60 sec !!");
						}

					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}

			}
		});
		appResponse = new AppResponse.Builder()
				.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, AppStatusMsg.SUCCESS_MSG)
						.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
				.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
		return ResponseEntity.ok(appResponse);
	}

	@SuppressWarnings({ "finally", "deprecation", "unused", "rawtypes" })
	@RequestMapping(value = "/get/transferOrder", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getTransferOrder(String jobName, HttpServletRequest request) {

		AppResponse appResponse = null;
		JobDetail data = null;
		ObjectNode objectNode = null;
		ObjectNode node = null;
		S3BucketInfo s3BucketInfo = null;
		List repAllocData = null;
		try {
			String token = ((HttpServletRequest) request).getHeader("X-Auth-Token");
			JsonArray orgDetails = CodeUtils.decode(token).get("ORG_DETAILS").getAsJsonArray();

			JsonObject jNode = orgDetails.get(0).getAsJsonObject();
			String orgId = jNode.get("orgId").getAsString();
			String pattern = jNode.get("ip_prefix").getAsString();
			jobName = jobName.subSequence(0, jobName.lastIndexOf("-") + 1) + pattern + "-JOB";
			LOGGER.info("get job last summary details to retrive transfer order status");
			data = jobDetailService.getLastSummary(jobName, orgId);
			String bucketName = CodeUtils.decode(token).get(FileUtils.BucketName).getAsString();
			LOGGER.info("Retrive s3 Bucket Info for download transfer order");
			s3BucketInfo = s3BucketInfoService.getBucketByType(bucketName, FileUtils.FileDownload, orgId);
			if (!CodeUtils.isEmpty(data)) {
				String transferOrder = data.getGeneratedTransferOrder();
				if (transferOrder != null) {
					objectNode = getMapper().createObjectNode();
					objectNode.put("fileInfo", FileUtils.TransferOrder_Success);
					objectNode.put("code", FileStatusCode.FILE_SUCCESS);
				} else if (transferOrder == null) {
					objectNode = getMapper().createObjectNode();
					objectNode.put("fileInfo", FileUtils.TransferOrder_Inprogress);
					objectNode.put("code", FileStatusCode.FILE_INPROGRESS);
				}
			} else {
				objectNode = getMapper().createObjectNode();
				objectNode.put("fileInfo", FileUtils.Allocation_Not_Found);
				objectNode.put("code", FileStatusCode.FILE_INPROGRESS);
			}
			node = getMapper().createObjectNode();
			node.put(CodeUtils.RESPONSE, objectNode);
			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(node))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}
		} catch (CannotGetJdbcConnectionException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.RULE_ENGINE_READER_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.RULE_ENGINE_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.RULE_ENGINE_READER_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
		} finally {

			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally", "deprecation" })
	@RequestMapping(value = "/get/customheader", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getFixedHeaders(@RequestParam("isDefault") String isDefault,
			@RequestParam("attributeType") String attributeType, @RequestParam("displayName") String displayName,
			HttpServletRequest request) {
		AppResponse appResponse = null;
		try {
			String token = ((HttpServletRequest) request).getHeader("X-Auth-Token");
			JsonArray orgDetails = CodeUtils.decode(token).get("ORG_DETAILS").getAsJsonArray();

			JsonObject jNode = orgDetails.get(0).getAsJsonObject();
			String orgId = jNode.get("orgId").getAsString();
			LOGGER.info("query for fetching header has started...");
			ObjectNode data = jobDetailService.getCustomHeaders(isDefault.toUpperCase(), attributeType.toUpperCase(),
					displayName.toUpperCase(), orgId);
			LOGGER.info("query for fetching header has ended...");
			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(data))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}
		} catch (CannotGetJdbcConnectionException ex) {
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.HEADER_CONFIG_CREATE_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.HEADER_CONFIG_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.HEADER_CONFIG_READER_ERROR));
		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally", "deprecation" })
	@RequestMapping(value = "/get/header", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getHeaders(@RequestParam("active") String active,
			@RequestParam("attributeType") String attributeType, @RequestParam("displayName") String displayName) {
		AppResponse appResponse = null;
		try {
			LOGGER.info("Query for fetching headers, started....");
			ObjectNode data = configService.getHeaders(active.toUpperCase(), attributeType.toUpperCase(),
					displayName.toUpperCase());
			LOGGER.info("Query for fetching headers, ended....");
			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(data))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}
		} catch (CannotGetJdbcConnectionException ex) {
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.HEADER_CONFIG_CREATE_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.HEADER_CONFIG_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.HEADER_CONFIG_READER_ERROR));
		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally" })
	@RequestMapping(value = "/change/headers", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> changeHeaders(@RequestBody HeaderConfigLog headerConfigLog) {
		LOGGER.info("JobDetailEndpoint changeHeaders() method starts here....");
		AppResponse appResponse = null;
		int result = 0;

		try {
			LOGGER.info("Query for changing headers started....");
			result = configService.changeHeaders(headerConfigLog);
			LOGGER.info("Query for changing headers ended....");
			if (result != 0) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, headerConfigLog)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
						AppModuleErrors.HEADER_CONFIG_CREATE_ERROR,
						appStatus.getMessage(AppModuleErrors.HEADER_CONFIG_CREATE_ERROR));
			}
		} catch (CannotGetJdbcConnectionException ex) {
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.HEADER_CONFIG_CREATE_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.HEADER_CONFIG_CREATE_ERROR,
					appStatus.getMessage(AppModuleErrors.HEADER_CONFIG_CREATE_ERROR));
		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

//	@Author : Manoj Singh
	@SuppressWarnings("unused")
	@RequestMapping(value = "/generate/transfer/order/{tenant_hashkey}", method = RequestMethod.GET)
	public ResponseEntity<AppResponse> uploadSummaryZip(@PathVariable(value = "tenant_hashkey") String tenantHashKey)
			throws UnknownHostException {
		AppResponse appResponse = null;
		MailManagerViewLog mailManagerViewLog = new MailManagerViewLog();
		String ipAddress = InetAddress.getLocalHost().getHostAddress().trim().toString();
		ExecutorService executorService = Executors.newSingleThreadExecutor();
		executorService.execute(new Runnable() {
			@SuppressWarnings("unchecked")
			@Override
			public void run() {
				AppResponse appResponse = null;
				ThreadLocalStorage.setTenantHashKey(tenantHashKey);
				LOGGER.info("Genarate transfer Order method initiated ....");
				List<String> whData = null;
				File zipFile = null;
				File file = null;
				String fileName = null;
				String filePath = null;
				String filePath1 = null;
				String filePath2 = null;
				String etag = null;
				String[] cc = null;
				String[] to = null;
				String isZipped = null;
				boolean isSendEmail = false;
				int threadPoolCount = 1;

				String randomFolder = UUID.randomUUID().toString();
				List<S3BucketInfo> s3BucketInfoList = s3BucketInfoService.getBucketByType(GENERATE_TRANSFERORDER);
				for (S3BucketInfo s3BucketInfo : s3BucketInfoList) {
					boolean isTO_Generated = false;
					LOGGER.debug(String.format("S3 Bucket Information : %s and time : %s", s3BucketInfo, new Date()));
					for (int threadCount = 0; threadCount < threadPoolCount; threadCount++) {
						try {
							JobDetail jobDetail = jobDetailService.getLastEngineRunDetails(s3BucketInfo.getJobName());
							int mailCounter = jobDetail.getMailCounter();
							String jobRunState = jobDetail.getJobRunState();
							boolean isSucceeded = jobRunState.equalsIgnoreCase(RunState.RUNNING.toString()) ? false
									: jobRunState.equalsIgnoreCase(RunState.SUCCEEDED.toString()) ? true : false;

							boolean isFailed_Stopped_Stopping = jobRunState.equalsIgnoreCase(RunState.FAILED.toString())
									? true
									: jobRunState.equalsIgnoreCase(RunState.STOPPED.toString()) ? true
											: jobRunState.equalsIgnoreCase(RunState.STOPPING.toString()) ? true : false;
							try {
								isTO_Generated = jobDetail.getGeneratedTransferOrder().isEmpty() ? false : true;
							} catch (Exception e1) {
								LOGGER.debug("No Transfer Order is generated yet !!: %s");
							}
							LOGGER.debug(String.format("Current JobID : %s  and JobRunState : %s", jobDetail.getJobId(),
									jobDetail.getJobRunState()));
							try {
								if (isSucceeded && !isTO_Generated) {
									String jobName = jobDetail.getJobName();
									String pattern = jobName.substring(jobName.indexOf("IP"), jobName.lastIndexOf("-"));
									String tableName = "REP_ALLOC_OUTPUT_" + pattern;
									LOGGER.debug("Running job is SUCCEEDED");
									if (!CodeUtils.isEmpty(s3BucketInfo.getS3bucketName())) {
										String s3BucketFilePath = s3BucketInfo.getBucketPath()
												+ CustomParameter.DELIMETER + randomFolder;
										LOGGER.info("Bucket info : {} ", s3BucketInfo);
										try {
											if (tenantHashKey.equalsIgnoreCase(
													"5b72fb9c68fc88b920c03ed4e701eb9642deeecb34e456b8badc11b54a164ba2")) {

												List downloadExcelSummaryNysaa1 = null;
												List downloadExcelSummaryNysaa2 = null;
												fileName = s3BucketInfo.getEnterprise() + CustomParameter.SEPARATOR
														+ FileUtils.GenerateTransferOrder + CustomParameter.SEPARATOR
														+ CodeUtils.convertDateToString(new Date());
												File[] excelFiles = new File[2];

												String savedPath1 = File.separator + s3BucketInfo.getEnterprise()
														+ File.separator + "EXCEL" + File.separator + "REQUIREMENT"
														+ FileUtils.EXCELExtension;

												String savedPath2 = File.separator + s3BucketInfo.getEnterprise()
														+ File.separator + "EXCEL" + File.separator
														+ "OLD_BARCODE_ALLOCATION" + FileUtils.EXCELExtension;

												filePath1 = FileUtils.createFilePath(
														FileUtils.getPlatformBasedParentDir(), savedPath1);
												filePath2 = FileUtils.createFilePath(
														FileUtils.getPlatformBasedParentDir(), savedPath2);
												LOGGER.debug(String.format(
														"Retrieving rep alloc output table1 data for nysaa :%s",
														jobDetail.getJobId()));
												downloadExcelSummaryNysaa1 = jobDetailService
														.downloadExcelSummaryForNysaa1();
												LOGGER.debug(String.format(
														"Obtaining rep alloc output table1 data for nysaa :%s",
														jobDetail.getJobId()));
												ExcelUtils.writeToExcel(filePath1, downloadExcelSummaryNysaa1);
												LOGGER.debug(String.format(
														"Excel Write Completed for output table1 data for nysaa :%s",
														jobDetail.getJobId()));
												LOGGER.debug(String.format(
														"Retrieving rep alloc output table2 data for nysaa :%s",
														jobDetail.getJobId()));
												downloadExcelSummaryNysaa2 = jobDetailService
														.downloadExcelSummaryForNysaa2();
												LOGGER.debug(String.format(
														"Obtaining rep alloc output table1 data for nysaa",
														jobDetail.getJobId()));
												ExcelUtils.writeToExcel(filePath2, downloadExcelSummaryNysaa2);
												LOGGER.debug(String.format(
														"Excel Write Completed for output table1 data for nysaa %s",
														jobDetail.getJobId()));
												excelFiles[0] = new File(filePath1);
												excelFiles[1] = new File(filePath2);

												isZipped = s3BucketInfo.getCreateZip();
												if (isZipped.equalsIgnoreCase(Conditions.TRUE.toString())) {

													file = new File(filePath1.replace(FileUtils.EXCELExtension,
															FileUtils.ZIPExtension));
													LOGGER.debug(String.format("Create Zip Files for nysaa",
															jobDetail.getJobId()));
													CodeUtils.createZip(file, excelFiles);
													etag = s3Wrapper.upload(file,
															s3BucketFilePath + CustomParameter.DELIMETER + fileName
																	+ FileUtils.ZIPExtension,
															s3BucketInfo.getS3bucketName()).getETag();
													LOGGER.debug(String.format(
															"Zip Files for nysaa uploaded Successfully!! :%s",
															jobDetail.getJobId()));
												}
											}

											else if (tenantHashKey.equalsIgnoreCase(
													"f71b609842ca103b916ceec05d9cc8ff8a354b77da12e425f49166dc1cf41c5e")) {
												LOGGER.info("Upload FMCG data start now....");
												List<String> getJobIdOfUplodedFile = poService
														.getJobIdFCGFile(jobDetail.getOrgId());
												LOGGER.debug(String.format("upload data for JobId is :  %s---",
														jobDetail.getJobId()));
												if (!getJobIdOfUplodedFile.contains(jobDetail.getJobId())) {
													String bucketPathWithFileName = null;
													OffsetDateTime currentTime = OffsetDateTime.now();
													currentTime = currentTime.plusHours(5);
													currentTime = currentTime.plusMinutes(30);
													ProcurementUploadSummary summary = new ProcurementUploadSummary();
													summary.setUuId(randomFolder);
													summary.setJobId(jobDetail.getJobId());
													summary.setProcurementType("FPO");
													summary.setLastSummary("TRUE");
													summary.setUploadStatus(Status.INPROGRESS.toString());
													summary.setUserName(jobDetail.getCreatedBy());
													summary.setOrgId(jobDetail.getOrgId());
													summary.setStatus("Active");
													summary.setActive("1");
													summary.setCreatedTime(currentTime);
													summary.setIpAddress(ipAddress);

													String bucketName = s3BucketInfo.getS3bucketName();
													s3BucketInfo = s3BucketInfoService.getBucketByType(bucketName,
															"OUTPUT", jobDetail.getOrgId());
//													String bucketKey = s3BucketInfo.getBucketPath() + "/"
//															+ s3BucketInfo.getOutputFileName();

													String bucketKey = jobDetailService
															.getLastSummary(s3BucketInfo.getJobName(),
																	s3BucketInfo.getOrgId())
															.getBucketKey();
													bucketKey = bucketKey.split("\\//")[1].replaceAll(
															bucketName + delimeter, "") + delimeter.trim().toString();

													List<S3ObjectSummary> objectSummary = amazonS3Client
															.getObjectSummaries(bucketName, bucketKey);

//
//													List<S3ObjectSummary> s3ObjectSummary = amazonS3Client
//															.getObjectSummaries(bucketName, bucketKey);
													for (S3ObjectSummary s3Summary : objectSummary) {
														if (s3Summary.getKey().contains("PO_REQ"))
															bucketPathWithFileName = s3Summary.getKey();
													}
													LOGGER.debug(String.format("upload data of bucket key is :  %s---",
															bucketPathWithFileName));
													String arrBucketPathWithFileName[] = bucketPathWithFileName
															.split("/");
													String getFileName = arrBucketPathWithFileName[arrBucketPathWithFileName.length
															- 1];
													summary.setFileName(getFileName);
													poService.insertUploadSummary(summary);
													LOGGER.debug(String.format(
															"successfully inserted summary record for JobId is :%s--",
															jobDetail.getJobId()));
													poService.updateLastRecord(jobDetail.getOrgId(), randomFolder);

													S3Object s3Object = null;
													s3Object = amazonS3Client.getObjectRequest(bucketName,
															bucketPathWithFileName);
													LOGGER.debug(String.format("Get file from s3 bucket...."));
													InputStream inputStream = s3Object.getObjectContent();
													filePath = FileUtils.createFilePath(
															FileUtils.getPlatformBasedParentDir(),
															bucketPathWithFileName + ".csv");
													Files.copy(inputStream, Paths.get(filePath));
													@SuppressWarnings("resource")
													CSVReader reader = new CSVReader(new FileReader(filePath));
													String str = String.join("", reader.readNext());
													String[] header = str.split("\\" + "|");
													int dataLength = header.length;
													File fileData = new File(filePath);
													InputStream in = new FileInputStream(fileData);
													BufferedReader bufferedReader = new BufferedReader(
															new InputStreamReader(in));
													poService.importDataFromFile(bufferedReader, dataLength, bucketKey,
															randomFolder, jobDetail.getUserName(), jobDetail.getOrgId(),
															bucketName, bucketKey, "null");
												}

											} else {
												if (s3BucketInfo.getIsManualTransfer().equalsIgnoreCase("TRUE")) {
													s3BucketInfo = s3BucketInfoService.getBucketByType(
															FileUtils.FileDownload, s3BucketInfo.getOrgId());
													String manualPath = s3BucketInfo.getManualTransferPath();
													manualPath = manualPath.split("\\//")[1].replaceAll(
															s3BucketInfo.getS3bucketName() + CustomParameter.DELIMETER,
															"") + CustomParameter.DELIMETER.trim().toString();
													manualPath = manualPath.substring(0, manualPath.length() - 1);
													LOGGER.debug(String.format(
															"Retrieving Manual Transfer Order from S3 to local :%s",
															jobDetail.getJobId()));
													file = s3Wrapper.getListFileFromS3Bucket(
															s3BucketInfo.getS3bucketName(), manualPath);
													LOGGER.debug(String.format(
															"Obtaining Manual Transfer Order from S3 to local :%s",
															jobDetail.getJobId()));
													etag = s3Wrapper.upload(file,
															s3BucketFilePath + CustomParameter.DELIMETER
																	+ "TRANSFER_ORDER" + FileUtils.ZIPExtension,
															s3BucketInfo.getS3bucketName()).getETag();
													s3BucketInfo = s3BucketInfoService.getBucketByType(
															GENERATE_TRANSFERORDER, s3BucketInfo.getOrgId());
													LOGGER.debug(String.format(
															"Manual Transfer Order Zip file uploaded successfully!! :%s",
															jobDetail.getJobId()));
												} else {
													List<String> headerKey = jobDetailService.getHeadersData()
															.get("key");
													List<String> headerValue = jobDetailService.getHeadersData()
															.get("value");
													whData = jobDetailService.getWhCode(tableName);
													if (!CodeUtils.isEmpty(whData)) {
														File[] excelFiles = new File[whData.size()];
														for (int i = 0; i < whData.size(); i++) {
															fileName = s3BucketInfo.getAdditional()
																	+ CustomParameter.SEPARATOR + CHANELPARTNER
																	+ CustomParameter.SEPARATOR
																	+ CodeUtils.___________dateTimeFormat
																			.format(new Date());
															List<HeadersSpecificOutput> downloadExcelSummary = null;
															LOGGER.debug(String.format(
																	"Retrieving rep alloc output data :%s",
																	jobDetail.getJobId()));
															downloadExcelSummary = jobDetailService
																	.getDownloadExcelSummaryData(whData.get(i),
																			tableName);
															LOGGER.debug(
																	String.format("Obtaining rep alloc output data :%s",
																			jobDetail.getJobId()));
															String savedPath = File.separator
																	+ s3BucketInfo.getEnterprise() + File.separator
																	+ "EXCEL" + File.separator + fileName
																	+ FileUtils.EXCELExtension;
															filePath = FileUtils.createFilePath(
																	FileUtils.getPlatformBasedParentDir(), savedPath);
															LOGGER.debug(String.format(
																	"Excel Write start from Rep Alloc Output :%s",
																	jobDetail.getJobId()));
															ExcelUtils.customizedHeaderWriteToExcel(headerKey, filePath,
																	downloadExcelSummary, headerValue);
															LOGGER.debug(String.format(
																	"Excel Write Completed from Rep Alloc Output :%s",
																	jobDetail.getJobId()));
															excelFiles[i] = new File(filePath);
														}
														isZipped = s3BucketInfo.getCreateZip();
														if (isZipped.equalsIgnoreCase(Conditions.TRUE.toString())) {
															file = new File(filePath.replace(FileUtils.EXCELExtension,
																	FileUtils.ZIPExtension));
															LOGGER.debug(String.format("Create Zip Files Start... :%s",
																	jobDetail.getJobId()));
															CodeUtils.createZip(file, excelFiles);
															LOGGER.debug(String.format("Create Zip Files completed :%s",
																	jobDetail.getJobId()));
															etag = s3Wrapper.upload(file,
																	s3BucketFilePath + CustomParameter.DELIMETER
																			+ fileName + FileUtils.ZIPExtension,
																	s3BucketInfo.getS3bucketName()).getETag();
															LOGGER.debug(String.format(
																	"Zip Files uploaded successfully!! :%s",
																	jobDetail.getJobId()));
														} else {
															file = new File(filePath);
															etag = s3Wrapper.upload(file,
																	s3BucketFilePath + CustomParameter.DELIMETER
																			+ fileName + FileUtils.EXCELExtension,
																	s3BucketInfo.getS3bucketName()).getETag();
															LOGGER.debug(String.format(
																	"Excel Files uploaded successfully!! :%s",
																	jobDetail.getJobId()));
														}
													}
												}
											}
											// Previous Code out of the block common for all case
											if (!CodeUtils.isEmpty(etag)) {
												JobDetail jobDetails = jobDetailService.getLastSummary(
														s3BucketInfo.getJobName(), jobDetail.getOrgId());
												s3BucketFilePath = BucketParameter.BUCKETPREFIX
														+ s3BucketInfo.getS3bucketName() + CustomParameter.DELIMETER
														+ s3BucketFilePath;
												LOGGER.debug(
														String.format("Current JobID : %s  and Transfer Order : %s",
																jobDetail.getJobId(), s3BucketFilePath));
												int respCode = jobDetailService.updateTransferOrderKey(
														jobDetails.getJobId(), s3BucketFilePath, mailCounter + 1);
												if (respCode == 1) {
													appResponse = new AppResponse.Builder()
															.data(getMapper().createObjectNode()
																	.putPOJO(CodeUtils.RESPONSE, s3BucketFilePath)
																	.put(CodeUtils.RESPONSE_MSG,
																			AppStatusMsg.SUCCESS_MSG))
															.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
													LOGGER.info(
															"zip is created and successfully updated on defined bucket with bucket key  : {} ",
															s3BucketFilePath);
												}

												int result = mailManagerService.getEmailStatus(jobDetail.getOrgId());
												int reqFileCount = s3BucketInfo.getReqMailCount();

												if (result != 0 && mailCounter + 1 == reqFileCount) {
													LOGGER.debug(String.format(
															"Email Process Start...Req File Count: %s and jobId :%s",
															reqFileCount, jobDetail.getJobId()));
													MailManager mailManager = mailManagerService.getByStatus(
															EmailTemplateName.TO_SUCCESS_GENERATION.toString(),
															CustomRunState.SUCCESS.toString(), jobDetail.getOrgId());
													MailManagerViewLog mailManagerViewLog = new MailManagerViewLog();
													if (!CodeUtils.isEmpty(mailManager)) {
														if (mailManager.getCc() != null) {
															cc = mailManager.getCc().trim().split(",");
														}
														if (mailManager.getTo() != null) {
															to = mailManager.getTo().trim().split(",");
														}
														mailManagerViewLog.setCc(mailManager.getCc());
														mailManagerViewLog.setBcc(mailManager.getBcc());
														mailManagerViewLog.setTo(mailManager.getTo());
														mailManagerViewLog.setCreatedOn(CodeUtils
																.convertDateOnTimeZonePluseForAWS(new Date(), 5, 30));
														mailManagerViewLog.setDeliveredOn(CodeUtils
																.convertDateOnTimeZonePluseForAWS(new Date(), 5, 30));
														mailManagerViewLog.setBody(
																EmailTemplateName.TO_SUCCESS_GENERATION.toString());
														mailManagerViewLog
																.setModule(Module.INVENTRY_PLANNING.toString());
														mailManagerViewLog
																.setSubModule(SubModule.RUN_ON_DEMAND.toString());
														mailManagerViewLog.setIpAddress(ipAddress);
														if (!CodeUtils.isEmpty(mailManager)) {
															SimpleMailMessage mailMessage = new SimpleMailMessage();
															mailMessage.setCc(cc);
															mailMessage.setTo(to);
															mailMessage.setBcc(mailManager.getBcc());
															mailMessage.setSubject(mailManager.getSubject());
															Date updationTime = new Date();
															String updatedOn = CodeUtils._____dateFormat
																	.format(CodeUtils.convertDateOnTimeZonePluseForAWS(
																			updationTime, 5, 30));

															s3BucketFilePath = s3BucketFilePath.split("\\//")[1]
																	.replaceAll(s3BucketInfo.getS3bucketName()
																			+ CustomParameter.DELIMETER, "")
																	+ CustomParameter.DELIMETER.trim().toString();
															String url = s3Wrapper.downloadPreSignedURLOnHistory(
																	s3BucketInfo.getS3bucketName(), s3BucketFilePath,
																	Integer.parseInt(s3BucketInfo.getTime()));
															emailService.sendEmailUsingMailTemplate(mailMessage,
																	mailManager.getTemplateName(),
																	s3BucketInfo.getEnterprise(), jobDetail.getJobId(),
																	updatedOn, url);
															LOGGER.debug("Email Send successfully!!");
															isSendEmail = true;
														}
														if (isSendEmail) {
															mailManagerViewLog.setStatus(RunState.SUCCEEDED.toString());
															int createLog = mailManagerService.createEmailActivityLog(
																	mailManagerViewLog, jobDetail.getOrgId());
															jobDetailService.updateMailStatus(jobDetail.getJobId());
														}
													}
												}
											}
										} catch (Exception e) {
											LOGGER.debug(String.format("Error occoured From Job Transfer Order", e));
											int result = mailManagerService.getEmailStatus(jobDetail.getOrgId());
											if (result != 0) {
												MailManager mailManager = mailManagerService.getByStatus(
														EmailTemplateName.TO_FAILED_GENERATION.toString(),
														CustomRunState.FAILED.toString(), jobDetail.getOrgId());
												if (!CodeUtils.isEmpty(mailManager)) {
													if (mailManager.getCc() != null) {
														cc = mailManager.getCc().trim().split(",");
													}
													if (mailManager.getTo() != null) {
														to = mailManager.getTo().trim().split(",");
													}
													mailManagerViewLog.setCc(mailManager.getCc());
													mailManagerViewLog.setBcc(mailManager.getBcc());
													mailManagerViewLog.setTo(mailManager.getTo());
													mailManagerViewLog.setCreatedOn(CodeUtils
															.convertDateOnTimeZonePluseForAWS(new Date(), 5, 30));
													mailManagerViewLog.setDeliveredOn(CodeUtils
															.convertDateOnTimeZonePluseForAWS(new Date(), 5, 30));
													mailManagerViewLog
															.setBody(EmailTemplateName.TO_FAILED_GENERATION.toString());
													mailManagerViewLog.setModule(Module.INVENTRY_PLANNING.toString());
													mailManagerViewLog.setSubModule(SubModule.RUN_ON_DEMAND.toString());
													mailManagerViewLog.setIpAddress(ipAddress);
													if (!CodeUtils.isEmpty(mailManager)) {
														SimpleMailMessage mailMessage = new SimpleMailMessage();
														mailMessage.setCc(cc);
														mailMessage.setTo(to);
														mailMessage.setBcc(mailManager.getBcc());
														mailMessage.setSubject(mailManager.getSubject());
														Date updationTime = new Date();
														String updatedOn = CodeUtils._____dateFormat.format(CodeUtils
																.convertDateOnTimeZonePluseForAWS(updationTime, 5, 30));

														String url = null;
														emailService.sendEmailUsingMailTemplate(mailMessage,
																mailManager.getTemplateName(),
																s3BucketInfo.getEnterprise(), jobDetail.getJobId(),
																updatedOn, url);
														isSendEmail = true;
													}
													if (isSendEmail) {
														mailManagerViewLog.setStatus(RunState.SUCCEEDED.toString());
														int createLog = mailManagerService.createEmailActivityLog(
																mailManagerViewLog, jobDetail.getOrgId());
														jobDetailService.updateMailStatus(jobDetail.getJobId());
													}
												}
											}
											e.printStackTrace();
										}
									} else {
										LOGGER.debug("Bucket info not defined ..");
										appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
												AppModuleErrors.RULE_ENGINE_CREATOR_ERROR, AppStatusMsg.JOB_FAILED);
									}
									break;
								} else {
									if (isFailed_Stopped_Stopping) {
										LOGGER.debug("Escaping from sleep !!");
									} else if (isTO_Generated) {
										LOGGER.debug("Escaping from sleep !!");
									} else {
										threadPoolCount++;
										LOGGER.debug(String.format(
												"JobRunState : %s , setting thread pool to sleep for 60 Sec ",
												jobDetail.getJobRunState()));
										Thread.sleep(60 * 1000);
										LOGGER.debug("Thread waked up after 60 sec !!");
									}

								}

							} catch (InterruptedException e) {
								LOGGER.debug("Error occoured From Job Transfer Order", e);
								e.printStackTrace();
							}
						} catch (Exception ex) {
							LOGGER.debug("JOB Name not found", ex);
							ex.printStackTrace();
						}
					}

				}

			}
		});
		appResponse = new AppResponse.Builder()
				.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, AppStatusMsg.SUCCESS_MSG)
						.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
				.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
		return ResponseEntity.ok(appResponse);
	}

}