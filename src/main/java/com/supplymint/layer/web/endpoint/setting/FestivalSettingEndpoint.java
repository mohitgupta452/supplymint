package com.supplymint.layer.web.endpoint.setting;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.CannotGetJdbcConnectionException;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.supplymint.config.application.AppCodeConfig;
import com.supplymint.exception.StatusCodes.AppModuleErrors;
import com.supplymint.exception.StatusCodes.AppModuleMsg;
import com.supplymint.exception.StatusCodes.AppStatusCodes;
import com.supplymint.exception.StatusCodes.AppStatusMsg;
import com.supplymint.exception.StatusCodes.ClientErrors;
import com.supplymint.layer.business.contract.setting.FestivalSettingService;
import com.supplymint.layer.data.setting.entity.FestivalSetting;
import com.supplymint.layer.data.setting.entity.FestivalSettingMaster;
import com.supplymint.layer.web.endpoint.abstracts.AbstractEndpoint;
import com.supplymint.layer.web.model.AppResponse;
import com.supplymint.util.CodeUtils;

import oracle.jdbc.internal.ObjectData;

@RestController
@RequestMapping(path = FestivalSettingEndpoint.PATH, produces = { MediaType.APPLICATION_JSON_VALUE })
public class FestivalSettingEndpoint extends AbstractEndpoint {

	public static final String PATH = "setting/festival";

	private static final Logger LOGGER = LoggerFactory.getLogger(FestivalSettingEndpoint.class);

	private FestivalSettingService festivalSettingService;

	@Autowired
	private AppCodeConfig appStatus;

	@Autowired
	public FestivalSettingEndpoint(FestivalSettingService festivalSettingService) {
		this.festivalSettingService = festivalSettingService;
	}

	@SuppressWarnings({ "finally", "unchecked" })
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseEntity<AppResponse> createGeneralSetting(@RequestBody JsonNode festivalNode,
			HttpServletRequest request) {
		FestivalSettingMaster festivals = null;
		List<FestivalSetting> defaultFestivalSetting = null;
		List<FestivalSetting> customFestivalSetting = null;

		AppResponse appResponse = null;
		int result = 0;
		try {
			festivals = getMapper().treeToValue(festivalNode, FestivalSettingMaster.class);
			defaultFestivalSetting = festivals.getDefaultFestival();
			customFestivalSetting = festivals.getCustomFestival();
			LOGGER.info("creating new festival list with startDate & endDate...");
			result = festivalSettingService.createNew(defaultFestivalSetting, customFestivalSetting, request);
			LOGGER.info("created new festival list with startDate & endDate...");
			if (result != 0) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode()
								.put(CodeUtils.RESPONSE_MSG, AppModuleMsg.FESTIVAL_SETTING_UPDATION)
								.putPOJO(CodeUtils.RESPONSE, festivalNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
						AppModuleErrors.FESTIVAL_SETTING_CREATOR_ERROR,
						appStatus.getMessage(AppModuleErrors.FESTIVAL_SETTING_CREATOR_ERROR));
			}
		} catch (CannotGetJdbcConnectionException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.FESTIVAL_SETTING_CREATOR_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.FESTIVAL_SETTING_CREATOR_ERROR,
					appStatus.getMessage(AppModuleErrors.FESTIVAL_SETTING_CREATOR_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}


	@SuppressWarnings({ "deprecation", "finally" })
	@RequestMapping(value = "/get/all/defaultlist", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getAllDefaultList() {

		AppResponse appResponse = null;
		try {
			LOGGER.info("starting method/query to get all default list...");
			ObjectNode data = festivalSettingService.getAllDefaultList();
//			ObjectNode data = festivalSettingService.testDefaultList();
			LOGGER.info("ended method/query to get all default list...");
			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(data))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}
		} catch (DataAccessException ex) {
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.FESTIVAL_SETTING_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.FESTIVAL_SETTING_READER_ERROR));
		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "deprecation", "finally" })
	@RequestMapping(value = "/get/all/customlist", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getAllCustomList(@RequestParam("pageno") Integer pageNo) {

		AppResponse appResponse = null;
		int previousPage = 0;
		int pageSize = 9;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;
		try {
			LOGGER.info("starting method/query to get all custom list...");
			totalRecord = festivalSettingService.countCustomList();
			LOGGER.info("ended method/query to get all custom list...");
			maxPage = (totalRecord + pageSize - 1) / pageSize;
			previousPage = pageNo - 1;
			offset = (pageNo - 1) * pageSize;
			LOGGER.info("starting method/query to get all custom list according pageNo...");
			ObjectNode data = festivalSettingService.getAllCustomList(offset, pageSize);
			LOGGER.info("ended method/query to get all custom list according pageNo...");
			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").put("currPage", pageNo)
								.put("prePage", previousPage).put("maxPage", maxPage).putAll(data))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				LOGGER.info(AppStatusMsg.SUCCESS_MSG);
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				LOGGER.info(AppStatusMsg.BLANK);
			}
		} catch (DataAccessException ex) {
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.FESTIVAL_SETTING_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.FESTIVAL_SETTING_READER_ERROR));
		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "deprecation", "finally" })
	@RequestMapping(value = "/get/checkedlist", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getCheckedFestival() {

		AppResponse appResponse = null;
		try {
			LOGGER.info("starting method/query to get all checked list...");
			ObjectNode data = festivalSettingService.getAllNew();
			LOGGER.info("ended method/query to get all checked list...");
			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(data))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}
		} catch (DataAccessException ex) {
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.FESTIVAL_SETTING_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.FESTIVAL_SETTING_READER_ERROR));
		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

}
