
package com.supplymint.layer.web.endpoint.tenant.dashboard;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import org.mybatis.spring.MyBatisSystemException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.supplymint.config.application.AppCodeConfig;
import com.supplymint.exception.StatusCodes.AppModuleErrors;
import com.supplymint.exception.StatusCodes.AppStatusCodes;
import com.supplymint.exception.StatusCodes.AppStatusMsg;
import com.supplymint.exception.StatusCodes.ClientErrors;
import com.supplymint.exception.StatusCodes.DashboardMsg;
import com.supplymint.exception.SupplyMintException;
import com.supplymint.layer.business.contract.dashboard.DashboardService;
import com.supplymint.layer.data.config.ThreadLocalStorage;
import com.supplymint.layer.data.tenant.dashboard.entity.ArticleMoving;
import com.supplymint.layer.data.tenant.dashboard.entity.SalesTrend;
import com.supplymint.layer.data.tenant.dashboard.entity.StoreArticleRank;
import com.supplymint.layer.web.endpoint.abstracts.AbstractEndpoint;
import com.supplymint.layer.web.model.AppResponse;
import com.supplymint.util.CodeUtils;
import com.supplymint.util.DateUtils;
import com.supplymint.util.FiscalDate;

@RestController
@RequestMapping(path = DashboardEndpoint.PATH, produces = { MediaType.APPLICATION_JSON_VALUE })
public class DashboardEndpoint extends AbstractEndpoint {

	public static final String PATH = "dashboard";

	private static final Logger LOGGER = LoggerFactory.getLogger(DashboardEndpoint.class);

	private DashboardService dashboardService;

	@Autowired
	private AppCodeConfig appStatus;

	@Autowired
	public DashboardEndpoint(DashboardService dashboardService) {
		this.dashboardService = dashboardService;
	}

	@SuppressWarnings({ "deprecation", "finally" })
	@RequestMapping(method = RequestMethod.GET, value = "get/windows")
	@ResponseBody
	public ResponseEntity<AppResponse> getDashboardWindows() {

		LOGGER.info("DashboardEndpoint getDashboardWindows() method starts here...");
		AppResponse appResponse = null;

		try {

			LOGGER.debug(ThreadLocalStorage.getTenantHashKey());
			Map<String, String> windowValues = dashboardService.getDashboardWindowValues();

			if (windowValues.get("isValid").contains("true")) {
				ObjectNode dashboardNode = getDashboardWindowNode(windowValues);
				
				

				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
						.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG).putAll(dashboardNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

				LOGGER.info(AppStatusMsg.SUCCESS_MSG);
			} else {
				throw new SupplyMintException("Database does not have values");
			}

		} catch (MyBatisSystemException ex) {
			LOGGER.info(ex.getMessage());
//			appResponse = buildErrorResponse(AppStatusCodes.genericConnectionErrorCode,
//					AppStatusCodes.genericClientErrorCode, AppStatusMsg.CONNECTION_NOT_AVAILABLE);

			appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
					.putPOJO(CodeUtils.RESPONSE, null).put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
					.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

		} catch (DataAccessException ex) {
			LOGGER.info(ex.getMessage());
//			appResponse = buildErrorResponse(AppStatusCodes.genericServerErrorCode, AppModuleErrors.dashboardError,
//					appStatus.getMessage(AppModuleErrors.dashboardError));
			appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
					.putPOJO(CodeUtils.RESPONSE, null).put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
					.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

		} catch (NullPointerException ex) {
			LOGGER.info(ex.getMessage());
//			appResponse = buildErrorResponse(AppStatusCodes.genericServerErrorCode, AppModuleErrors.dashboardError,
//					appStatus.getMessage(AppModuleErrors.dashboardError));
			appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
					.putPOJO(CodeUtils.RESPONSE, null).put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
					.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

		} catch (SupplyMintException ex) {
			LOGGER.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.DASHBOARD_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.DASHBOARD_READER_ERROR));
		} catch (Exception e) {
			LOGGER.info(e.getMessage());
//			appResponse = buildErrorResponse(AppStatusCodes.genericServerErrorCode, ClientErrors.badRequest,
//					AppStatusMsg.FAILURE_MSG);
			appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
					.putPOJO(CodeUtils.RESPONSE, null).put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
					.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

		} finally {
			return ResponseEntity.ok(appResponse);
		}

	}

	@SuppressWarnings({ "finally", "deprecation" })
	@RequestMapping(method = RequestMethod.GET, value = "get/toparticlesstores")
	@ResponseBody
	public ResponseEntity<AppResponse> getTopArticlesStores() {

		LOGGER.info("DashboardEndpoint getTopArticlesStores() method starts here...");
		AppResponse appResponse = null;

		try {
			Map<String, List<StoreArticleRank>> topStoreArticle = dashboardService.getTopStoreArticleRankNode();

			if (!CodeUtils.isEmpty(topStoreArticle)) {
				ObjectNode topStoreArticleNode = getTopStoreArticleNode(topStoreArticle);
				
				
				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
						.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG).putAll(topStoreArticleNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				LOGGER.info(AppStatusMsg.SUCCESS_MSG);
			} else {
				//throw new SupplyMintException("Database does not have values");
				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
						.putPOJO(CodeUtils.RESPONSE, null).put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}

		} catch (MyBatisSystemException ex) {
			LOGGER.info(ex.getMessage());
//			appResponse = buildErrorResponse(AppStatusCodes.genericConnectionErrorCode,
//					AppStatusCodes.genericClientErrorCode, AppStatusMsg.CONNECTION_NOT_AVAILABLE);
			appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
					.putPOJO(CodeUtils.RESPONSE, null).put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
					.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

		} catch (DataAccessException ex) {
			LOGGER.info(ex.getMessage());
//			appResponse = buildErrorResponse(AppStatusCodes.genericServerErrorCode, AppModuleErrors.dashboardError,
//					appStatus.getMessage(AppModuleErrors.dashboardError));
			appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
					.putPOJO(CodeUtils.RESPONSE, null).put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
					.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
		} /*
			 * catch (SupplyMintException ex) { LOGGER.info(ex.getMessage()); appResponse =
			 * buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
			 * AppModuleErrors.DASHBOARD_READER_ERROR,
			 * appStatus.getMessage(AppModuleErrors.DASHBOARD_READER_ERROR)); }
			 */
		catch (Exception e) {
			LOGGER.info(e.getMessage());
//			appResponse = buildErrorResponse(AppStatusCodes.genericServerErrorCode, ClientErrors.badRequest,
//					AppStatusMsg.FAILURE_MSG);
			appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
					.putPOJO(CodeUtils.RESPONSE, null).put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
					.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

		} finally {
			return ResponseEntity.ok(appResponse);
		}

	}

	@SuppressWarnings({ "deprecation", "finally" })
	@RequestMapping(method = RequestMethod.GET, value = "get/salestrend")
	@ResponseBody
	public ResponseEntity<AppResponse> getSalesTrendGraph(@RequestParam("type") String type) {

		LOGGER.info("DashboardEndpoint getSalesTrendGraph() method starts here...");
		AppResponse appResponse = null;

		try {

			Map<String, Object> topStoreArticle = dashboardService.getSalesTrend(type);
			if (!CodeUtils.isEmpty(topStoreArticle)) {

				ObjectNode topStoreArticleNode = getSalesTrendNode(topStoreArticle);

				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
						.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG).putAll(topStoreArticleNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				//throw new SupplyMintException("Database does not have values");
				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
						.putPOJO(CodeUtils.RESPONSE, null).put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			}
			LOGGER.info(AppStatusMsg.SUCCESS_MSG);
		} catch (MyBatisSystemException ex) {
			LOGGER.info(ex.getMessage());
//			appResponse = buildErrorResponse(AppStatusCodes.genericConnectionErrorCode,
//					AppStatusCodes.genericClientErrorCode, AppStatusMsg.CONNECTION_NOT_AVAILABLE);

			appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
					.putPOJO(CodeUtils.RESPONSE, null).put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
					.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

		} catch (DataAccessException ex) {
			LOGGER.info(ex.getMessage());
//			appResponse = buildErrorResponse(AppStatusCodes.genericServerErrorCode, AppModuleErrors.dashboardError,
//					appStatus.getMessage(AppModuleErrors.dashboardError));
			appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
					.putPOJO(CodeUtils.RESPONSE, null).put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
					.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

		}
		/*
		 * catch (SupplyMintException ex) { LOGGER.info(ex.getMessage()); appResponse =
		 * buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
		 * AppModuleErrors.DASHBOARD_READER_ERROR,
		 * appStatus.getMessage(AppModuleErrors.DASHBOARD_READER_ERROR)); }
		 */ catch (Exception e) {
			LOGGER.info(e.getMessage());
//			appResponse = buildErrorResponse(AppStatusCodes.genericServerErrorCode, ClientErrors.badRequest,
//					AppStatusMsg.FAILURE_MSG);
			appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
					.putPOJO(CodeUtils.RESPONSE, null).put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
					.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

		} finally {
			return ResponseEntity.ok(appResponse);
		}

	}

	@SuppressWarnings("finally")
	@RequestMapping(method = RequestMethod.GET, value = "shcheduler/store")
	@ResponseBody
	public ResponseEntity<AppResponse> updateStoreArticleRank() {

		LOGGER.info("DashboardEndpoint updateStoreArticleRank() method starts here...");
		AppResponse appResponse = null;

		try {

//			List store = dashboardService.getStoreArticleRank();
			dashboardService.createStoreArticleRank();

			if (!CodeUtils.isEmpty(null)) {

//				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
//						.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG).putAll(storeRank))
//						.status(AppStatusCodes.genericSuccessCode).build();
			} else {
				throw new SupplyMintException("Database does not have values");

			}
			LOGGER.info(AppStatusMsg.SUCCESS_MSG);
		} catch (MyBatisSystemException ex) {
			LOGGER.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CONNECTION_ERROR_CODE,
					AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, AppStatusMsg.CONNECTION_NOT_AVAILABLE);

		} catch (DataAccessException ex) {
			LOGGER.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.DASHBOARD_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.DASHBOARD_READER_ERROR));
		} catch (SupplyMintException ex) {
			LOGGER.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.DASHBOARD_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.DASHBOARD_READER_ERROR));
		} catch (Exception e) {
			LOGGER.info(e.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally", "deprecation" })
	@RequestMapping(method = RequestMethod.GET, value = "get/articlemoving")
	@ResponseBody
	public ResponseEntity<AppResponse> getArticlesMoving() {

		LOGGER.info("DashboardEndpoint getArticlesMoving() method starts here...");
		AppResponse appResponse = null;

		try {
			Map<String, List<ArticleMoving>> articleMoving = dashboardService.getArticleMoving();
			if (!CodeUtils.isEmpty(articleMoving)) {
				ObjectNode articleMovindNode = getArticleMovingNode(articleMoving);

				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
						.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG).putAll(articleMovindNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				LOGGER.info(AppStatusMsg.SUCCESS_MSG);
			} else {
				throw new SupplyMintException("Database does not have values");
			}

		} catch (MyBatisSystemException ex) {
			LOGGER.info(ex.getMessage());
//			appResponse = buildErrorResponse(AppStatusCodes.genericConnectionErrorCode,
//					AppStatusCodes.genericClientErrorCode, AppStatusMsg.CONNECTION_NOT_AVAILABLE);
			appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
					.putPOJO(CodeUtils.RESPONSE, null).put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
					.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

		} catch (DataAccessException ex) {
			LOGGER.info(ex.getMessage());
//			appResponse = buildErrorResponse(AppStatusCodes.genericServerErrorCode, AppModuleErrors.dashboardError,
//					appStatus.getMessage(AppModuleErrors.dashboardError));
			appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
					.putPOJO(CodeUtils.RESPONSE, null).put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
					.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

		} catch (SupplyMintException ex) {
			LOGGER.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.DASHBOARD_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.DASHBOARD_READER_ERROR));
		} catch (Exception e) {
			LOGGER.info(e.getMessage());
//			appResponse = buildErrorResponse(AppStatusCodes.genericServerErrorCode, ClientErrors.badRequest,
//					AppStatusMsg.FAILURE_MSG);
			appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
					.putPOJO(CodeUtils.RESPONSE, null).put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
					.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

		} finally {
			return ResponseEntity.ok(appResponse);
		}

	}

	@SuppressWarnings("deprecation")
	public ObjectNode getDashboardWindowNode(Map<String, String> map) throws Exception {
		ObjectNode tempNode = getMapper().createObjectNode();
		ObjectNode mainNode = getMapper().createObjectNode();

		tempNode.put(DashboardMsg.TOTAL_SALES, map.get("totalSales"));
		tempNode.put(DashboardMsg.TOTAL_UNITS, map.get("totalUnits"));
		tempNode.put(DashboardMsg.TOTAL_AVGSALESUNIT, map.get("avgSalesUnit"));
		tempNode.put(DashboardMsg.TOTAL_AVGSALESASSORTMENT, map.get("avgAssortmentUnit"));
		
		Date endTime=CodeUtils.getCurrentTime();
		Date startTime=CodeUtils.addYear(endTime, -1);
		Date weekStartTime=CodeUtils.addDay(endTime, -7);
		Date lastMonthStartTime=CodeUtils.addMonth(endTime, -1);
		String lastYear = CodeUtils.dateFormat.format(startTime) + " - "
				+ CodeUtils.dateFormat.format(endTime);
		
		String lastWeek = CodeUtils.dateFormat.format(weekStartTime) + " - "
				+ CodeUtils.dateFormat.format(endTime);
		
		String lastMonth = CodeUtils.dateFormat.format(lastMonthStartTime) + " - "
				+ CodeUtils.dateFormat.format(endTime);
		tempNode.put("lastYear", lastYear);
		tempNode.put("lastWeek", lastWeek);
		tempNode.put("lastMonth", lastMonth);

		mainNode.put(CodeUtils.RESPONSE, tempNode);
		return mainNode;
	}

	@SuppressWarnings("deprecation")
	public ObjectNode getTopStoreArticleNode(Map<String, List<StoreArticleRank>> map) throws Exception {

		ObjectNode mainNode = getMapper().createObjectNode();
		ObjectNode arrayMainNode = getMapper().createObjectNode();
		ArrayNode topStoreArrayNode = getMapper().createArrayNode();
		ArrayNode bottomStoreArrayNode = getMapper().createArrayNode();
		ArrayNode topArticleArrayNode = getMapper().createArrayNode();
		ArrayNode bottomArticleArrayNode = getMapper().createArrayNode();

		map.get("topStores").stream().forEach(store -> {
			ObjectNode topObjectNode = getMapper().createObjectNode();
			topObjectNode.put(DashboardMsg.STORES, store.getCode());
			topObjectNode.put(DashboardMsg.STORES_NAME, store.getName());
			topObjectNode.put(DashboardMsg.LASTYEAR, store.getLastYearSV());
			topObjectNode.put(DashboardMsg.LASTYEARSIGN, store.getLastYearSign());
			topObjectNode.put(DashboardMsg.THISYEAR, store.getCurrentYearSV());
			topObjectNode.put(DashboardMsg.THISYEARSIGN, store.getCurrentYearSign());
			topObjectNode.put(DashboardMsg.LASTMONTH, store.getLastMonthSV());
			topObjectNode.put(DashboardMsg.LASTMONTHSIGN, store.getLastMonthSign());
			topObjectNode.put(DashboardMsg.PREVYEARLASTMONTH, store.getPyLastMonthSV());
			topObjectNode.put(DashboardMsg.PREVYEARLASTMONTHSIGN, store.getPrevYearLastMonthSign());
			topStoreArrayNode.add(topObjectNode);
		});

		map.get("bottomStores").stream().forEach(store -> {
			ObjectNode topObjectNode = getMapper().createObjectNode();
			topObjectNode.put(DashboardMsg.STORES, store.getCode());
			topObjectNode.put(DashboardMsg.STORES_NAME, store.getName());
			topObjectNode.put(DashboardMsg.LASTYEAR, store.getLastYearSV());
			topObjectNode.put(DashboardMsg.LASTYEARSIGN, store.getLastYearSign());
			topObjectNode.put(DashboardMsg.THISYEAR, store.getCurrentYearSV());
			topObjectNode.put(DashboardMsg.THISYEARSIGN, store.getCurrentYearSign());
			topObjectNode.put(DashboardMsg.LASTMONTH, store.getLastMonthSV());
			topObjectNode.put(DashboardMsg.LASTMONTHSIGN, store.getLastMonthSign());
			topObjectNode.put(DashboardMsg.PREVYEARLASTMONTH, store.getPyLastMonthSV());
			topObjectNode.put(DashboardMsg.PREVYEARLASTMONTHSIGN, store.getPrevYearLastMonthSign());
			bottomStoreArrayNode.add(topObjectNode);
		});

		map.get("topArticles").stream().forEach(store -> {
			ObjectNode topObjectNode = getMapper().createObjectNode();
			topObjectNode.put(DashboardMsg.STORES, store.getCode());
			topObjectNode.put(DashboardMsg.ARTICLES_NAME, store.getName());
			topObjectNode.put(DashboardMsg.LASTYEAR, store.getLastYearSV());
			topObjectNode.put(DashboardMsg.LASTYEARSIGN, store.getLastYearSign());
			topObjectNode.put(DashboardMsg.THISYEAR, store.getCurrentYearSV());
			topObjectNode.put(DashboardMsg.THISYEARSIGN, store.getCurrentYearSign());
			topObjectNode.put(DashboardMsg.LASTMONTH, store.getLastMonthSV());
			topObjectNode.put(DashboardMsg.LASTMONTHSIGN, store.getLastMonthSign());
			topObjectNode.put(DashboardMsg.PREVYEARLASTMONTH, store.getPyLastMonthSV());
			topObjectNode.put(DashboardMsg.PREVYEARLASTMONTHSIGN, store.getPrevYearLastMonthSign());
			topArticleArrayNode.add(topObjectNode);
		});

		map.get("bottomArticles").stream().forEach(store -> {
			ObjectNode topObjectNode = getMapper().createObjectNode();
			topObjectNode.put(DashboardMsg.STORES, store.getCode());
			topObjectNode.put(DashboardMsg.ARTICLES_NAME, store.getName());
			topObjectNode.put(DashboardMsg.LASTYEAR, store.getLastYearSV());
			topObjectNode.put(DashboardMsg.LASTYEARSIGN, store.getLastYearSign());
			topObjectNode.put(DashboardMsg.THISYEAR, store.getCurrentYearSV());
			topObjectNode.put(DashboardMsg.THISYEARSIGN, store.getCurrentYearSign());
			topObjectNode.put(DashboardMsg.LASTMONTH, store.getLastMonthSV());
			topObjectNode.put(DashboardMsg.LASTMONTHSIGN, store.getLastMonthSign());
			topObjectNode.put(DashboardMsg.PREVYEARLASTMONTH, store.getPyLastMonthSV());
			topObjectNode.put(DashboardMsg.PREVYEARLASTMONTHSIGN, store.getPrevYearLastMonthSign());
			bottomArticleArrayNode.add(topObjectNode);
		});

		arrayMainNode.put(DashboardMsg.TOPARTICLES, topArticleArrayNode);
		arrayMainNode.put(DashboardMsg.BOTTOMARTICLES, bottomArticleArrayNode);
		arrayMainNode.put(DashboardMsg.TOPSTORES, topStoreArrayNode);
		arrayMainNode.put(DashboardMsg.BOTTOMSTORES, bottomStoreArrayNode);
		
		
		Date endTime=CodeUtils.getCurrentTime();
		FiscalDate fiscalDate=new FiscalDate(endTime);
		
		String previousFiscalYear = "01 Apr " + (fiscalDate.getFiscalYear() - 1) + " - 31 Mar " + fiscalDate.getFiscalYear();
		String currentFiscalYear = "01 Apr "+fiscalDate.getFiscalYear() + " - " + CodeUtils.dateFormat.format(endTime);
		
		Date lastMonthStartTime=CodeUtils.addMonth(endTime, -1);
		
		Date preLastMonthStartTime=CodeUtils.addYear(lastMonthStartTime, -1);
		
		String lastMonth = CodeUtils.convertDemandForecastDateFormat(CodeUtils.getFirstDayOfTheMonth(CodeUtils.__dateFormat.format(lastMonthStartTime))) + " - "
				+ CodeUtils.convertDemandForecastDateFormat(CodeUtils.getLastDayOfTheMonth(CodeUtils.__dateFormat.format(lastMonthStartTime)));
		
		String preLastMonth = CodeUtils.convertDemandForecastDateFormat(CodeUtils.getFirstDayOfTheMonth(CodeUtils.__dateFormat.format(preLastMonthStartTime))) + " - "
				+ CodeUtils.convertDemandForecastDateFormat(CodeUtils.getLastDayOfTheMonth(CodeUtils.__dateFormat.format(preLastMonthStartTime)));
		arrayMainNode.put("lastYear", previousFiscalYear);
		arrayMainNode.put("thisYear", currentFiscalYear);
		arrayMainNode.put("lastMonth", lastMonth);
		arrayMainNode.put("preLastMonth", preLastMonth);

		mainNode.put(CodeUtils.RESPONSE, arrayMainNode);

		return mainNode;
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	public ObjectNode getSalesTrendNode(Map<String, Object> map) {

		ObjectNode mainNode = getMapper().createObjectNode();
		ObjectNode salesTrendNode = getMapper().createObjectNode();

		ArrayNode unitSales = null;
		ArrayNode salesValue = null;

		ObjectNode arrayMainNode = getMapper().createObjectNode();

		Map<String, List<SalesTrend>> salesTrendMap = (Map<String, List<SalesTrend>>) map.get("sales");
		Map<String, String> yearsMap = (Map<String, String>) map.get("year");

		unitSales = getMapper().convertValue(salesTrendMap.get("unitsTrend"), ArrayNode.class);
		salesValue = getMapper().convertValue(salesTrendMap.get("salesTrend"), ArrayNode.class);

		salesTrendNode.put(DashboardMsg.UNITSALES, unitSales);
		salesTrendNode.put(DashboardMsg.SALESVALUE, salesValue);
		salesTrendNode.put(DashboardMsg.YEARS, yearsMap.get("years"));

		arrayMainNode.put(DashboardMsg.SALESTREND, salesTrendNode);
		mainNode.put(CodeUtils.RESPONSE, arrayMainNode);

		return mainNode;
	}

	@SuppressWarnings("deprecation")
	public ObjectNode getArticleMovingNode(Map<String, List<ArticleMoving>> articleMap) {

		ObjectNode mainNode = getMapper().createObjectNode();

		ObjectNode fastThirty = getMapper().createObjectNode();
		ObjectNode fastFourty = getMapper().createObjectNode();
		ObjectNode fastNinety = getMapper().createObjectNode();

		ArrayNode threeFastThirtyArrayNode = getMapper().createArrayNode();
		ArrayNode fiveFastThirtyArrayNode = getMapper().createArrayNode();
		ArrayNode tenFastThirtyArrayNode = getMapper().createArrayNode();

		ArrayNode threeFastFourtyArrayNode = getMapper().createArrayNode();
		ArrayNode fiveFastFourtyArrayNode = getMapper().createArrayNode();
		ArrayNode tenFastFourtyArrayNode = getMapper().createArrayNode();

		ArrayNode threeFastNinetyArrayNode = getMapper().createArrayNode();
		ArrayNode fiveFastNinetyArrayNode = getMapper().createArrayNode();
		ArrayNode tenFastNinetyArrayNode = getMapper().createArrayNode();
		AtomicInteger fastCount = new AtomicInteger(1);

		ObjectNode slowThirty = getMapper().createObjectNode();
		ObjectNode slowFourty = getMapper().createObjectNode();
		ObjectNode slowNinety = getMapper().createObjectNode();

		ArrayNode threeslowThirtyArrayNode = getMapper().createArrayNode();
		ArrayNode fiveslowThirtyArrayNode = getMapper().createArrayNode();
		ArrayNode tenslowThirtyArrayNode = getMapper().createArrayNode();

		ArrayNode threeslowFourtyArrayNode = getMapper().createArrayNode();
		ArrayNode fiveslowFourtyArrayNode = getMapper().createArrayNode();
		ArrayNode tenslowFourtyArrayNode = getMapper().createArrayNode();

		ArrayNode threeslowNinetyArrayNode = getMapper().createArrayNode();
		ArrayNode fiveslowNinetyArrayNode = getMapper().createArrayNode();
		ArrayNode tenslowNinetyArrayNode = getMapper().createArrayNode();
		AtomicInteger slowCount = new AtomicInteger(1);

		articleMap.get("fastMoving").stream().forEach(article -> {
			ObjectNode thirtyObjectNode = getMapper().createObjectNode();
			ObjectNode fourtyObjectNode = getMapper().createObjectNode();
			ObjectNode ninetyObjectNode = getMapper().createObjectNode();

			thirtyObjectNode.put(DashboardMsg.ARTICLECODE, article.getArticleCode());
			thirtyObjectNode.put(DashboardMsg.ARTICLENAME, article.getArticleName());
			thirtyObjectNode.put(DashboardMsg.DAYS, article.getThirtyDays());
			thirtyObjectNode.put(DashboardMsg.DAYSSIGN, article.getThirtyDaysSign());

			fourtyObjectNode.put(DashboardMsg.ARTICLECODE, article.getArticleCode());
			fourtyObjectNode.put(DashboardMsg.ARTICLENAME, article.getArticleName());
			fourtyObjectNode.put(DashboardMsg.DAYS, article.getFourtyFiveDays());
			fourtyObjectNode.put(DashboardMsg.DAYSSIGN, article.getFourtyFiveDaysSign());

			ninetyObjectNode.put(DashboardMsg.ARTICLECODE, article.getArticleCode());
			ninetyObjectNode.put(DashboardMsg.ARTICLENAME, article.getArticleName());
			ninetyObjectNode.put(DashboardMsg.DAYS, article.getNinetyDays());
			ninetyObjectNode.put(DashboardMsg.DAYSSIGN, article.getNinetyDaysSign());

			if (fastCount.get() < 4) {
				threeFastThirtyArrayNode.add(thirtyObjectNode);
				threeFastFourtyArrayNode.add(fourtyObjectNode);
				threeFastNinetyArrayNode.add(ninetyObjectNode);

				fiveFastThirtyArrayNode.add(thirtyObjectNode);
				fiveFastFourtyArrayNode.add(fourtyObjectNode);
				fiveFastNinetyArrayNode.add(ninetyObjectNode);
			}

			if (fastCount.get() > 3 && fastCount.get() < 6) {
				fiveFastThirtyArrayNode.add(thirtyObjectNode);
				fiveFastFourtyArrayNode.add(fourtyObjectNode);
				fiveFastNinetyArrayNode.add(ninetyObjectNode);
			}

			tenFastThirtyArrayNode.add(thirtyObjectNode);
			tenFastFourtyArrayNode.add(fourtyObjectNode);
			tenFastNinetyArrayNode.add(ninetyObjectNode);

			fastCount.getAndIncrement();
		});

		fastThirty.put(DashboardMsg.TOPTHREE, threeFastThirtyArrayNode);
		fastThirty.put(DashboardMsg.TOPFIVE, fiveFastThirtyArrayNode);
		fastThirty.put(DashboardMsg.TOPTEN, tenFastThirtyArrayNode);

		fastFourty.put(DashboardMsg.TOPTHREE, threeFastFourtyArrayNode);
		fastFourty.put(DashboardMsg.TOPFIVE, fiveFastFourtyArrayNode);
		fastFourty.put(DashboardMsg.TOPTEN, tenFastFourtyArrayNode);

		fastNinety.put(DashboardMsg.TOPTHREE, threeFastNinetyArrayNode);
		fastNinety.put(DashboardMsg.TOPFIVE, fiveFastNinetyArrayNode);
		fastNinety.put(DashboardMsg.TOPTEN, tenFastNinetyArrayNode);

		ObjectNode fastDaysNode = getMapper().createObjectNode();

		fastDaysNode.put(DashboardMsg.THIRTY, fastThirty);
		fastDaysNode.put(DashboardMsg.FOURTY, fastFourty);
		fastDaysNode.put(DashboardMsg.NINETY, fastNinety);

		articleMap.get("slowMoving").stream().forEach(article -> {
			ObjectNode thirtyObjectNode = getMapper().createObjectNode();
			ObjectNode fourtyObjectNode = getMapper().createObjectNode();
			ObjectNode ninetyObjectNode = getMapper().createObjectNode();

			thirtyObjectNode.put(DashboardMsg.ARTICLECODE, article.getArticleCode());
			thirtyObjectNode.put(DashboardMsg.ARTICLENAME, article.getArticleName());
			thirtyObjectNode.put(DashboardMsg.DAYS, article.getThirtyDays());
			thirtyObjectNode.put(DashboardMsg.DAYSSIGN, article.getThirtyDaysSign());

			fourtyObjectNode.put(DashboardMsg.ARTICLECODE, article.getArticleCode());
			fourtyObjectNode.put(DashboardMsg.ARTICLENAME, article.getArticleName());
			fourtyObjectNode.put(DashboardMsg.DAYS, article.getFourtyFiveDays());
			fourtyObjectNode.put(DashboardMsg.DAYSSIGN, article.getFourtyFiveDaysSign());

			ninetyObjectNode.put(DashboardMsg.ARTICLECODE, article.getArticleCode());
			ninetyObjectNode.put(DashboardMsg.ARTICLENAME, article.getArticleName());
			ninetyObjectNode.put(DashboardMsg.DAYS, article.getNinetyDays());
			ninetyObjectNode.put(DashboardMsg.DAYSSIGN, article.getNinetyDaysSign());

			if (slowCount.get() < 4) {
				threeslowThirtyArrayNode.add(thirtyObjectNode);
				threeslowFourtyArrayNode.add(fourtyObjectNode);
				threeslowNinetyArrayNode.add(ninetyObjectNode);

				fiveslowThirtyArrayNode.add(thirtyObjectNode);
				fiveslowFourtyArrayNode.add(fourtyObjectNode);
				fiveslowNinetyArrayNode.add(ninetyObjectNode);
			}

			if (slowCount.get() > 3 && slowCount.get() < 6) {
				fiveslowThirtyArrayNode.add(thirtyObjectNode);
				fiveslowFourtyArrayNode.add(fourtyObjectNode);
				fiveslowNinetyArrayNode.add(ninetyObjectNode);
			}

			tenslowThirtyArrayNode.add(thirtyObjectNode);
			tenslowFourtyArrayNode.add(fourtyObjectNode);
			tenslowNinetyArrayNode.add(ninetyObjectNode);

			slowCount.getAndIncrement();
		});

		slowThirty.put(DashboardMsg.TOPTHREE, threeslowThirtyArrayNode);
		slowThirty.put(DashboardMsg.TOPFIVE, fiveslowThirtyArrayNode);
		slowThirty.put(DashboardMsg.TOPTEN, tenslowThirtyArrayNode);

		slowFourty.put(DashboardMsg.TOPTHREE, threeslowFourtyArrayNode);
		slowFourty.put(DashboardMsg.TOPFIVE, fiveslowFourtyArrayNode);
		slowFourty.put(DashboardMsg.TOPTEN, tenslowFourtyArrayNode);

		slowNinety.put(DashboardMsg.TOPTHREE, threeslowNinetyArrayNode);
		slowNinety.put(DashboardMsg.TOPFIVE, fiveslowNinetyArrayNode);
		slowNinety.put(DashboardMsg.TOPTEN, tenslowNinetyArrayNode);

		ObjectNode slowDaysNode = getMapper().createObjectNode();

		slowDaysNode.put(DashboardMsg.THIRTY, slowThirty);
		slowDaysNode.put(DashboardMsg.FOURTY, slowFourty);
		slowDaysNode.put(DashboardMsg.NINETY, slowNinety);

		ObjectNode movingNode = getMapper().createObjectNode();
		movingNode.put(DashboardMsg.SLOWMOVING, slowDaysNode);
		movingNode.put(DashboardMsg.FASTMOVING, fastDaysNode);

		mainNode.put(CodeUtils.RESPONSE, movingNode);

		return mainNode;
	}

}
