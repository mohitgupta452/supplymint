package com.supplymint.layer.web.endpoint.s3Bucket;

import java.net.InetAddress;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.CannotGetJdbcConnectionException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.supplymint.config.application.AppCodeConfig;
import com.supplymint.exception.StatusCodes.AppErrorCodes;
import com.supplymint.exception.StatusCodes.AppModuleErrors;
import com.supplymint.exception.StatusCodes.AppStatusCodes;
import com.supplymint.exception.StatusCodes.AppStatusMsg;
import com.supplymint.exception.StatusCodes.ClientErrors;
import com.supplymint.exception.SupplyMintException;
import com.supplymint.layer.business.contract.s3Bucket.S3BucketInfoService;
import com.supplymint.layer.data.tenant.inventory.entity.S3BucketInfo;
import com.supplymint.layer.web.endpoint.abstracts.AbstractEndpoint;
import com.supplymint.layer.web.endpoint.tenant.administration.CustomEndpoint;
import com.supplymint.layer.web.model.AppResponse;
import com.supplymint.util.CodeUtils;

/**
 * @author Manoj Singh
 * @since 20 OCT 2018
 * @version 1.0
 */

@SuppressWarnings("finally")
@RestController
@RequestMapping(path = S3BucketInfoEndpoint.PATH, produces = { MediaType.APPLICATION_JSON_VALUE })
public class S3BucketInfoEndpoint extends AbstractEndpoint {

	public static final String PATH = "aws/bucket";

	private static final Logger LOGGER = LoggerFactory.getLogger(CustomEndpoint.class);

	private S3BucketInfoService s3BucketInfoService;

	@Autowired
	private AppCodeConfig appStatus;

	@Autowired
	public S3BucketInfoEndpoint(S3BucketInfoService s3BucketInfoService) {
		this.s3BucketInfoService = s3BucketInfoService;
	}

	@SuppressWarnings({ "unchecked", "deprecation", "rawtypes" })
	@RequestMapping(value = "/bucketName", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getBucketByName(@RequestParam("bucketName") String bucketName) {

		AppResponse appResponse = null;
		try {
			List data = s3BucketInfoService.getBucketByName(bucketName);

			ObjectNode objectNode = getMapper().createObjectNode();
			ArrayNode array = getMapper().createArrayNode();
			data.forEach(e -> {
				ObjectNode node = getMapper().valueToTree(e);
				array.add(node);
			});
			objectNode.put(CodeUtils.RESPONSE, array);
			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(objectNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				LOGGER.info(AppStatusMsg.BLANK);
			}
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.RULE_ENGINE_READER_ERROR, appStatus.getMessage(AppModuleErrors.RULE_ENGINE_READER_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "unchecked", "deprecation", "rawtypes" })
	@RequestMapping(value = "/get/status/{status}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getBucketStatus(@PathVariable("status") String status) {

		AppResponse appResponse = null;
		try {
			List data = s3BucketInfoService.getBucketStatus(status);

			ObjectNode objectNode = getMapper().createObjectNode();
			ArrayNode array = getMapper().createArrayNode();
			data.forEach(e -> {
				ObjectNode node = getMapper().valueToTree(e);
				array.add(node);
			});
			objectNode.put(CodeUtils.RESPONSE, array);
			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(objectNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				LOGGER.info(AppStatusMsg.BLANK);
			}
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.RULE_ENGINE_READER_ERROR, appStatus.getMessage(AppModuleErrors.RULE_ENGINE_READER_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "unchecked", "deprecation", "rawtypes" })
	@RequestMapping(value = "/getAllBucket", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getAllBucket() {

		AppResponse appResponse = null;
		try {
			List data = s3BucketInfoService.getAllBucket();

			ObjectNode objectNode = getMapper().createObjectNode();
			ArrayNode array = getMapper().createArrayNode();
			data.forEach(e -> {
				ObjectNode node = getMapper().valueToTree(e);
				array.add(node);
			});
			objectNode.put(CodeUtils.RESPONSE, array);
			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(objectNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				LOGGER.info(AppStatusMsg.BLANK);
			}
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.RULE_ENGINE_READER_ERROR, appStatus.getMessage(AppModuleErrors.RULE_ENGINE_READER_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@RequestMapping(value = "/insert/fileName", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> s3InsertFileName(@RequestBody S3BucketInfo s3BucketInfo)
			throws SupplyMintException {

		AppResponse appResponse = null;
		int result = 0;
		List<S3BucketInfo> isExistFileName = s3BucketInfoService.isExistS3FileName(s3BucketInfo.getOutputFileName());
		try {
			if (isExistFileName.size() == 0) {
				result = s3BucketInfoService.s3InsertFileName(s3BucketInfo);
				if (result != 0) {
					appResponse = new AppResponse.Builder()
							.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, s3BucketInfo)
									.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
							.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				} else {
					appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
							AppStatusMsg.FAILURE_MSG);

				}
			} else {
				result = s3BucketInfoService.s3UpdateFileName(s3BucketInfo);

				if (result == 1) {
					appResponse = new AppResponse.Builder()
							.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, s3BucketInfo)
									.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
							.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				} else {
					appResponse = buildErrorResponse(AppErrorCodes.GENERIC_EVENT_NOT_AVAILABLE_ERROR_CODE,
							ClientErrors.NOT_FOUND, AppStatusMsg.BLANK);
				}
			}
		} catch (Exception e) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", e));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@RequestMapping(value = "/getBucket/type", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getBucketByType(@RequestParam("bucketName") String bucketName,
			@RequestParam("type") String type) {

		AppResponse appResponse = null;
		S3BucketInfo data = null;
		try {
			data = s3BucketInfoService.getBucketByType(bucketName, type);
			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, data)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = buildErrorResponse(AppErrorCodes.GENERIC_EVENT_NOT_AVAILABLE_ERROR_CODE, ClientErrors.NOT_FOUND,
						AppStatusMsg.BLANK);
				LOGGER.info(AppStatusMsg.BLANK);
			}
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.RULE_ENGINE_READER_ERROR, appStatus.getMessage(AppModuleErrors.RULE_ENGINE_READER_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.debug(String.format("Error Occoured From Exception : %s", e));
		} finally {
			return ResponseEntity.ok(appResponse);
		}

	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> updateBucketByType(@RequestBody S3BucketInfo s3BucketInfo) {

		AppResponse appResponse = null;
		int result = 0;
		try {
			Date updationTime = new Date();
			updationTime = CodeUtils.convertDateOnTimeZonePluseForAWS(updationTime, 5, 30);
			s3BucketInfo.setUpdationTime(updationTime);
			// s3BucketInfo.setUpdatedBy(userName);
			String ipAddress = InetAddress.getLocalHost().getHostAddress().trim().toString();
			s3BucketInfo.setIpAddress(ipAddress);
			result = s3BucketInfoService.updateBucketByType(s3BucketInfo);
			if (result != 0) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, s3BucketInfo)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
						AppModuleErrors.RULE_ENGINE_UPDATE_ERROR,
						appStatus.getMessage(AppModuleErrors.RULE_ENGINE_UPDATE_ERROR));
			}

		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.RULE_ENGINE_UPDATE_ERROR, appStatus.getMessage(AppModuleErrors.RULE_ENGINE_UPDATE_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}
	
	@SuppressWarnings({ "deprecation"})
	@RequestMapping(value = "/get/all/fileName", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getAllFileName() {

		AppResponse appResponse = null;
		ObjectNode node = getMapper().createObjectNode();
		ArrayNode arrNode = getMapper().createArrayNode();

		try {
			List<S3BucketInfo> list = s3BucketInfoService.getAllFileName();
			if (list.size() > 0 && !list.isEmpty()) {
				list.stream().forEach(data -> {
					ObjectNode objectNode = getMapper().createObjectNode();

					objectNode.put("outputFileName", data.getOutputFileName());
					arrNode.add(objectNode);

				});
				node.put(CodeUtils.RESPONSE, arrNode);

				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, node)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			}
		} catch (CannotGetJdbcConnectionException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.RULE_ENGINE_READER_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.RULE_ENGINE_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.RULE_ENGINE_READER_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}
	
	@RequestMapping(value = "/s3/insert/bucket/name", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> insertBucketName(@RequestBody S3BucketInfo s3BucketInfo)
			throws SupplyMintException {

		AppResponse appResponse = null;
		int result = 0;
		LOGGER.info("validation for existence of bucket name");
		List<S3BucketInfo> isExistBucketName = s3BucketInfoService.isExistBucketName(s3BucketInfo.getS3bucketName());
		try {
			if (isExistBucketName.size() == 0) {
				String url = "http://s3.ap-south-1.amazonaws.com/web-supplymint-com-dev/index.html";
				s3BucketInfo.setUrl(url);
				LOGGER.info("create s3 bucket info if not exists");
				result = s3BucketInfoService.insertBucketName(s3BucketInfo);
				if (result != 0) {
					appResponse = new AppResponse.Builder()
							.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, s3BucketInfo)
									.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
							.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				} else {
					appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
							AppStatusMsg.FAILURE_MSG);
				}
			} else {
				String url = "http://s3.ap-south-1.amazonaws.com/web-supplymint-com-dev/index.html";
				s3BucketInfo.setUrl(url);
				LOGGER.info("update s3 bucket info if exists");
				result = s3BucketInfoService.updateBucketName(s3BucketInfo);
				if (result == 1) {
					appResponse = new AppResponse.Builder()
							.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, s3BucketInfo)
									.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
							.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				} else {
					appResponse = buildErrorResponse(AppErrorCodes.GENERIC_EVENT_NOT_AVAILABLE_ERROR_CODE,
							ClientErrors.NOT_FOUND, AppStatusMsg.BLANK);
				}
			}

		} catch (CannotGetJdbcConnectionException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.RULE_ENGINE_CREATOR_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.RULE_ENGINE_CREATOR_ERROR,
					appStatus.getMessage(AppModuleErrors.RULE_ENGINE_CREATOR_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}
	
	@SuppressWarnings({ "deprecation"})
	@RequestMapping(value = "/get/by/url", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getAllByURL(@RequestParam("url") String url) {

		AppResponse appResponse = null;
		ObjectNode node = getMapper().createObjectNode();
		ArrayNode arrNode = getMapper().createArrayNode();
		try {
			LOGGER.info("retrive the s3 bucket info by url...");
			List<S3BucketInfo> list = s3BucketInfoService.getByURL(url);
			if (list.size() > 0 && !list.isEmpty()) {
				list.stream().forEach(data -> {
					ObjectNode objectNode = getMapper().createObjectNode();

					objectNode.put("s3bucketName", data.getS3bucketName());
					objectNode.put("bucketKey", data.getBucketKey());
					objectNode.put("time", data.getTime());
					arrNode.add(objectNode);
				});
				node.put(CodeUtils.RESPONSE, arrNode);
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, node)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}
		} catch (CannotGetJdbcConnectionException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.RULE_ENGINE_READER_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.RULE_ENGINE_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.RULE_ENGINE_READER_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

}
