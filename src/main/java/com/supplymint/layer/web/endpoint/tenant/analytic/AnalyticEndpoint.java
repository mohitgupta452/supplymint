package com.supplymint.layer.web.endpoint.tenant.analytic;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.supplymint.config.application.AppCodeConfig;
import com.supplymint.exception.StatusCodes.AppModuleErrors;
import com.supplymint.exception.StatusCodes.AppStatusCodes;
import com.supplymint.exception.StatusCodes.AppStatusMsg;
import com.supplymint.exception.StatusCodes.ClientErrors;
import com.supplymint.exception.SupplyMintException;
import com.supplymint.layer.business.contract.analytic.AnalyticsService;
import com.supplymint.layer.data.tenant.analytic.entity.AnalyticStoreProfile;
import com.supplymint.layer.data.tenant.analytic.entity.StoreProfileActualSaleData;
import com.supplymint.layer.data.tenant.analytic.entity.StoreProfileGraph;
import com.supplymint.layer.web.endpoint.abstracts.AbstractEndpoint;
import com.supplymint.layer.web.model.AppResponse;
import com.supplymint.util.CodeUtils;

@RestController
@RequestMapping(path = AnalyticEndpoint.PATH, produces = { MediaType.APPLICATION_JSON_VALUE })
public class AnalyticEndpoint extends AbstractEndpoint {

	public static final String PATH = "analytics/storeprofile";
	private static final Logger LOGGER = LoggerFactory.getLogger(AnalyticEndpoint.class);

	private AnalyticsService analyticsService;

	@Autowired
	private AppCodeConfig appStatus;

	@Autowired
	public AnalyticEndpoint(AnalyticsService analyticsService) {
		this.analyticsService = analyticsService;
	}

	@SuppressWarnings({ "deprecation", "finally" })
	@RequestMapping(value = "/get/store/code", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getAllStorCode() throws SupplyMintException {
		LOGGER.info("analytics storeprofile getAllStorCode() method starts here....");
		AppResponse appResponse = null;
		List<Map<String, String>> data = null;
		try {
			data = analyticsService.getSiteCode();
			ObjectNode objectNode = getMapper().createObjectNode();
			objectNode.putPOJO(CodeUtils.RESPONSE, data);
			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(objectNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.STORE_PROFILE_BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}
		} catch (DataAccessException ex) {
			LOGGER.debug(AppModuleErrors.ANALYTIC_READER_ERROR, ex);
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.ANALYTIC_READER_ERROR, appStatus.getMessage(AppModuleErrors.ANALYTIC_READER_ERROR));
		} catch (Exception ex) {
			LOGGER.debug(AppModuleErrors.ANALYTIC_READER_ERROR, ex);
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			LOGGER.info("Analytics storeprofile getAllStorCode() method ends here....");
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally" })
	@RequestMapping(value = "/get/store/info", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getStoreInfoData(@RequestParam("storeCode") String storeCode) throws Exception {

		LOGGER.info("Analytics storeprofile  getStoreInfoData() method starts here....");
		AppResponse appResponse = null;
		AnalyticStoreProfile data = null;
		try {
			data = analyticsService.getStoreInfo(storeCode);
			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
						.put(CodeUtils.RESPONSE_MSG, "").putPOJO(CodeUtils.RESPONSE, data))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}
		} catch (SupplyMintException ex) {
			LOGGER.debug(AppModuleErrors.ANALYTIC_READER_ERROR, ex);
			appResponse = new AppResponse.Builder()
					.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null).put(CodeUtils.RESPONSE_MSG,
							AppStatusMsg.ANALYTICS_STORE_NOT_FOUND))
					.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
		} catch (DataAccessException ex) {
			LOGGER.debug(AppModuleErrors.ANALYTIC_READER_ERROR, ex);
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.ANALYTIC_READER_ERROR, appStatus.getMessage(AppModuleErrors.ANALYTIC_READER_ERROR));
		} catch (Exception ex) {
			LOGGER.debug(AppModuleErrors.ANALYTIC_READER_ERROR, ex);
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally", "deprecation" })
	@RequestMapping(value = "/get/sell/thru", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getSellThrough(@RequestParam("sellThroghType") String sellThroughType,
			@RequestParam("storeCode") String storeCode) {

		LOGGER.info("Analytics storeprofile  getStoreInfoData() method starts here....");
		AppResponse appResponse = null;
		ObjectNode data = null;
		try {
			data = analyticsService.getSellThrough(sellThroughType, storeCode);
			ObjectNode objectNode = getMapper().createObjectNode();
			objectNode.putPOJO(CodeUtils.RESPONSE, data);
			if (objectNode.get(CodeUtils.RESPONSE).toString().length() > 2) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(objectNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}
		} catch (DataAccessException ex) {
			LOGGER.debug(AppModuleErrors.ANALYTIC_READER_ERROR, ex);
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.ANALYTIC_READER_ERROR, appStatus.getMessage(AppModuleErrors.ANALYTIC_READER_ERROR));
		} catch (Exception ex) {
			LOGGER.debug(AppModuleErrors.ANALYTIC_READER_ERROR, ex);
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally" })
	@RequestMapping(value = "/get/top/article", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getTopArticle() {

		LOGGER.info("Analytics storeprofile  getTopArticle() method starts here....");
		AppResponse appResponse = null;
		ObjectNode data = null;
		try {
			data = analyticsService.getTopArticle();
			ObjectNode objectNode = getMapper().createObjectNode();
			objectNode.putPOJO(CodeUtils.RESPONSE, data);
			if (objectNode.get(CodeUtils.RESPONSE).toString().length() > 2) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(objectNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}
		} catch (NumberFormatException ex) {
			LOGGER.debug(AppModuleErrors.ANALYTIC_READER_ERROR, ex);
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.NUMBER_FORMAT_MSG);
		} catch (DataAccessException ex) {
			LOGGER.debug(AppModuleErrors.ANALYTIC_READER_ERROR, ex);
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.ANALYTIC_READER_ERROR, appStatus.getMessage(AppModuleErrors.ANALYTIC_READER_ERROR));
		} catch (Exception ex) {
			LOGGER.debug(AppModuleErrors.ANALYTIC_READER_ERROR, ex);
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally", "deprecation" })
	@RequestMapping(value = "/sales/trend/graph", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getSalesTrendGraph() {

		LOGGER.info("Analytics storeprofile  getSalesTrendGraph() method starts here....");
		AppResponse appResponse = null;
		Map<String, ArrayNode> data = null;
		try {
			data = analyticsService.getFiveTopRankStoresArticleData();
			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(data))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}
		} catch (DataAccessException ex) {
			LOGGER.debug(AppModuleErrors.ANALYTIC_READER_ERROR, ex);
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.ANALYTIC_READER_ERROR, appStatus.getMessage(AppModuleErrors.ANALYTIC_READER_ERROR));
		} catch (Exception ex) {
			LOGGER.debug(AppModuleErrors.ANALYTIC_READER_ERROR, ex);
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			LOGGER.info("Analytics storeprofile  getSalesTrendGraph() method ends here....");
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally", "deprecation" })
	@RequestMapping(value = "/get/sell/profit/trends", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getSellProfitTrends() {

		LOGGER.info("Analytics storeprofile  getStoreInfoData() method starts here....");
		AppResponse appResponse = null;
		List<StoreProfileActualSaleData> data = null;
		try {
			data = analyticsService.getSellProfitTrends();
			ObjectNode objectNode = getMapper().createObjectNode();
			objectNode.putPOJO(CodeUtils.RESPONSE, data);
			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(objectNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}
		} catch (DataAccessException ex) {
			LOGGER.debug(AppModuleErrors.ANALYTIC_READER_ERROR, ex);
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.ANALYTIC_READER_ERROR, appStatus.getMessage(AppModuleErrors.ANALYTIC_READER_ERROR));
		} catch (Exception ex) {
			LOGGER.debug(AppModuleErrors.ANALYTIC_READER_ERROR, ex);
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally", "deprecation" })
	@RequestMapping(value = "/get/sell/qty/graph", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getSellData(@RequestParam("storeCode") String storeCode) {

		LOGGER.info("Analytics storeprofile  getSellData() method starts here....");
		AppResponse appResponse = null;
		List<StoreProfileGraph> data = null;
		try {
			data = analyticsService.getSellData(storeCode);
			ObjectNode objectNode = getMapper().createObjectNode();
			objectNode.putPOJO(CodeUtils.RESPONSE, data);
			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(objectNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}
		} catch (NumberFormatException ex) {
			LOGGER.debug(AppModuleErrors.ANALYTIC_READER_ERROR, ex);
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.ANALYTIC_READER_ERROR, appStatus.getMessage(AppModuleErrors.ANALYTIC_READER_ERROR));
		} catch (DataAccessException ex) {
			LOGGER.debug(AppModuleErrors.ANALYTIC_READER_ERROR, ex);
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.ANALYTIC_READER_ERROR, appStatus.getMessage(AppModuleErrors.ANALYTIC_READER_ERROR));
		} catch (Exception ex) {
			LOGGER.debug(AppModuleErrors.ANALYTIC_READER_ERROR, ex);
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			LOGGER.info("Analytics storeprofile  getSellData() method ends here....");
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally", "deprecation" })
	@RequestMapping(value = "/get/stock/hand", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getStockInHand(@RequestParam("storeCode") String storeCode) throws Exception {

		LOGGER.info("Analytics storeprofile  getStoreInfoData() method starts here....");
		AppResponse appResponse = null;
		List<StoreProfileActualSaleData> data = null;
		try {
			data = analyticsService.getStockInHand(storeCode);
			ObjectNode objectNode = getMapper().createObjectNode();
			objectNode.putPOJO(CodeUtils.RESPONSE, data);
			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(objectNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}
		} catch (DataAccessException ex) {
			LOGGER.debug(AppModuleErrors.ANALYTIC_READER_ERROR, ex);
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.ANALYTIC_READER_ERROR, appStatus.getMessage(AppModuleErrors.ANALYTIC_READER_ERROR));
		} catch (Exception ex) {
			LOGGER.debug(AppModuleErrors.ANALYTIC_READER_ERROR, ex);
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

}
