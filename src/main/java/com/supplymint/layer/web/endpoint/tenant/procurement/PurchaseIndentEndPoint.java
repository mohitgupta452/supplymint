package com.supplymint.layer.web.endpoint.tenant.procurement;

/***
 * 
@author Sachin Mittal
Dated : 25 Oct, 2018
 * 
 * 
 */

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

import javax.servlet.http.HttpServletRequest;

import org.mybatis.spring.MyBatisSystemException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.CannotGetJdbcConnectionException;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.supplymint.config.application.AppCodeConfig;
import com.supplymint.exception.StatusCodes.AppErrorCodes;
import com.supplymint.exception.StatusCodes.AppModuleErrors;
import com.supplymint.exception.StatusCodes.AppModuleMsg;
import com.supplymint.exception.StatusCodes.AppStatusCodes;
import com.supplymint.exception.StatusCodes.AppStatusMsg;
import com.supplymint.exception.StatusCodes.ClientErrorMessage;
import com.supplymint.exception.StatusCodes.ClientErrors;
import com.supplymint.exception.SupplyMintException;
import com.supplymint.layer.business.contract.procurement.PurchaseIndentService;
import com.supplymint.layer.business.contract.procurement.PurchaseOrderService;
import com.supplymint.layer.business.contract.s3Bucket.S3BucketInfoService;
import com.supplymint.layer.business.service.s3bucketService.S3WrapperService;
import com.supplymint.layer.data.config.ThreadLocalStorage;
import com.supplymint.layer.data.downloads.entity.MultiFileUpload;
import com.supplymint.layer.data.metadata.PurchaseIndentMetaData;
import com.supplymint.layer.data.tenant.administration.entity.ADMItem;
import com.supplymint.layer.data.tenant.inventory.entity.S3BucketInfo;
import com.supplymint.layer.data.tenant.procurement.entity.CatMaster;
import com.supplymint.layer.data.tenant.procurement.entity.DeptItemUDFSettings;
import com.supplymint.layer.data.tenant.procurement.entity.DescriptionMaster;
import com.supplymint.layer.data.tenant.procurement.entity.FinChargeDetail;
import com.supplymint.layer.data.tenant.procurement.entity.HLevelData;
import com.supplymint.layer.data.tenant.procurement.entity.HsnData;
import com.supplymint.layer.data.tenant.procurement.entity.HsnGstData;
import com.supplymint.layer.data.tenant.procurement.entity.PurchaseIndent;
import com.supplymint.layer.data.tenant.procurement.entity.PurchaseIndentDetail;
import com.supplymint.layer.data.tenant.procurement.entity.PurchaseIndentHistory;
import com.supplymint.layer.data.tenant.procurement.entity.PurchaseIndentMaster;
import com.supplymint.layer.data.tenant.procurement.entity.PurchaseOrderConfiguration;
import com.supplymint.layer.data.tenant.procurement.entity.PurchaseTermData;
import com.supplymint.layer.data.tenant.procurement.entity.PurtermCharge;
import com.supplymint.layer.data.tenant.procurement.entity.PurtermDet;
import com.supplymint.layer.data.tenant.procurement.entity.PurtermMaster;
import com.supplymint.layer.data.tenant.procurement.entity.SupplierData;
import com.supplymint.layer.data.tenant.procurement.entity.Transporters;
import com.supplymint.layer.web.endpoint.abstracts.AbstractEndpoint;
import com.supplymint.layer.web.model.AppResponse;
import com.supplymint.util.ApplicationUtils.Conditions;
import com.supplymint.util.ApplicationUtils.DefaultParameter;
import com.supplymint.util.ApplicationUtils.TenantHash;
import com.supplymint.util.CodeUtils;
import com.supplymint.util.CodeUtils.PIValidation;
import com.supplymint.util.FileUtils;
import com.supplymint.util.JsonUtils;
import com.supplymint.util.UploadUtils;

@RestController
@RequestMapping(path = PurchaseIndentEndPoint.PATH, produces = { MediaType.APPLICATION_JSON_VALUE })
public class PurchaseIndentEndPoint extends AbstractEndpoint {

	public static final String PATH = "admin/pi";

	private static final Logger log = LoggerFactory.getLogger(PurchaseIndentEndPoint.class);

	private PurchaseIndentService piService;

	@Autowired
	private PurchaseOrderService poService;

	@Autowired
	private PlatformTransactionManager txManager;

	static String delimeter = "/";

	static String separator = "_";

	public static final String UPLOAD_Image_Article = "UPLOAD_ARTICLE";

	public static final String BucketPrefix = "s3://";

	@Autowired
	private AppCodeConfig appStatus;

	@Autowired
	private HttpServletRequest request;

	@Autowired
	private S3BucketInfoService s3BucketInfoService;

	@Autowired
	private S3WrapperService s3Wrapper;

	@Autowired
	public PurchaseIndentEndPoint(PurchaseIndentService piService) {
		this.piService = piService;
	}

	/**
	 * @return all hierarchy levels present for item master
	 */
	@SuppressWarnings({ "unchecked", "deprecation", "rawtypes", "finally" })
	@RequestMapping(method = RequestMethod.GET, value = "/get/hierarachylevels")
	@ResponseBody
	public ResponseEntity<AppResponse> getDistinctHierarachyLevels(@RequestParam("type") Integer type,
			@RequestParam("pageNo") Integer pageNo, @RequestParam("hl1name") String hl1name,
			@RequestParam("hl2name") String hl2name, @RequestParam("hl3name") String hl3name,
			@RequestParam("orgId") String orgId, @RequestParam("search") String search) {

		log.info("PurchaseIndentEndPoint getDistinctHierarachyLevels() method starts here...");

		AppResponse appResponse = null;
		ObjectNode node = getMapper().createObjectNode();
		ArrayNode arrNode = getMapper().createArrayNode();
		List<HLevelData> hLevelDataServiceList = null;

		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;

		try {

			if (type == 1) {
				totalRecord = piService.recordDistinctHierarachyLevels();

				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;

				hLevelDataServiceList = piService.getDistinctHierarachyLevels(offset, pageSize);
			} else if (type == 2) {

				totalRecord = piService.recordSearchByDistinctHierarachyLevels(hl1name, hl2name, hl3name);

				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;

				hl1name = hl1name.trim();
				hl2name = hl2name.trim();
				hl3name = hl3name.trim();
				hLevelDataServiceList = piService.searchByDistinctHierarachyLevels(offset, pageSize, hl1name, hl2name,
						hl3name);
			}

			else {
				if (type == 3 && !search.isEmpty()) {

					totalRecord = piService.recordALLSearchDistinctHierarachyLevels(search);

					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;

					search = search.trim();
					hLevelDataServiceList = piService.searchAllDistinctHierarachyLevels(offset, pageSize, search);

				}

			}
			log.info("data present in the list hLevelDataServiceList is {} : " + hLevelDataServiceList);

			AtomicInteger count = new AtomicInteger(1);

			if (hLevelDataServiceList.size() > 0 && !hLevelDataServiceList.isEmpty()) {

				hLevelDataServiceList.stream().forEach(hLevelData -> {

					ObjectNode tempNode = getMapper().createObjectNode();
					tempNode.put(PurchaseIndentMetaData.HierarchyLevel.ID, count.get());
					tempNode.put(PurchaseIndentMetaData.HierarchyLevel.HL1NAME, hLevelData.getHl1name());
					tempNode.put(PurchaseIndentMetaData.HierarchyLevel.HL1CODE, hLevelData.getHl1code());
					tempNode.put(PurchaseIndentMetaData.HierarchyLevel.HL2NAME, hLevelData.getHl2name());
					tempNode.put(PurchaseIndentMetaData.HierarchyLevel.HL2CODE, hLevelData.getHl2code());
					tempNode.put(PurchaseIndentMetaData.HierarchyLevel.HL3NAME, hLevelData.getHl3name());
					tempNode.put(PurchaseIndentMetaData.HierarchyLevel.HL3CODE, hLevelData.getHl3code());

					arrNode.add(tempNode);
					count.getAndIncrement();

				});

//				@Author Prabhakar Srivastava Start for checking site and udf existence for generics
				String udfExistenceStatus = piService.getUDFExistenceStatus(orgId);
				Map<String, Object> proMap = JsonUtils.toMap(JsonUtils.toJsonObject(udfExistenceStatus));
				String udfExist = proMap.get("UDF").toString();
				String siteExist = proMap.get("SITE").toString();
				String catDescModification = proMap.get("CAT_DESC_MODIFICATION").toString();
				String editDisplayName = proMap.get("EDIT_DISPLAY_NAME").toString();

				// End

				if (CodeUtils.isEmpty(udfExistenceStatus)) {
					throw new SupplyMintException("System default config reader error");
				}

				node.put(CodeUtils.RESPONSE, arrNode);
				log.info("Final Object Node data is {} : " + node);

				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").put("currPage", pageNo)
								.put("prePage", previousPage).put("maxPage", maxPage).put("isUDFExist", udfExist)
								.put("isSiteExist", siteExist).put("catDescModification", catDescModification)
								.put("editDisplayName", editDisplayName).putAll(node))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				log.info(AppStatusMsg.SUCCESS_MSG);

			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

				log.info(AppStatusMsg.BLANK);
			}
		} catch (MyBatisSystemException ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CONNECTION_ERROR_CODE,
					AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, AppStatusMsg.CONNECTION_NOT_AVAILABLE);

		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.HIERARCHY_LEVEL_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.HIERARCHY_LEVEL_READER_ERROR));
			log.info(ex.getMessage());

		} catch (SupplyMintException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.SYSTEM_DEFAULT_CONFIG_ERROR,
					appStatus.getMessage(AppModuleErrors.SYSTEM_DEFAULT_CONFIG_ERROR));
			log.info(ex.getMessage());

		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			log.info(e.getMessage());
		} finally {
			log.info("PurchaseIndentEndPoint getDistinctHierarachyLevels() method ends here...");
			return ResponseEntity.ok(appResponse);
		}
	}

	/**
	 * @param slCode is the supplier code
	 * @return list of transporters which are mapped to a particular supplier
	 */

	@SuppressWarnings({ "unchecked", "deprecation", "rawtypes", "finally" })
	@RequestMapping(method = RequestMethod.GET, value = "/get/transporter/supplier/{slcode}")
	@ResponseBody
	public ResponseEntity<AppResponse> getTransporterDataBySupplierCode(@PathVariable(value = "slcode") String slCode,
			@RequestParam("type") Integer type, @RequestParam("pageNo") Integer pageNo,
			@RequestParam("transporterName") String transporterName) {

		log.info("PurchaseIndent getTransporterDataBySupplierCode() method starts here....");
		AppResponse appResponse = null;
		ObjectNode node = getMapper().createObjectNode();
		ArrayNode arrNode = getMapper().createArrayNode();
		List<Transporters> transportersList = null;

		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;

		AtomicInteger count = new AtomicInteger(1);

		try {
			if (type == 1) {
				totalRecord = piService.getRecordTransportersBySlcode(slCode);

				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;

				transportersList = piService.getTransportersBySlcode(slCode, offset, pageSize);

			} else {

				if (type == 3 && !transporterName.isEmpty()) {

					transporterName = transporterName.trim();
					totalRecord = piService.recordTransportersBySlcode(slCode, transporterName);

					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;

					transportersList = piService.searchTransportersBySlcode(offset, pageSize, slCode, transporterName);

				}

			}

			log.info("data present in the list transportersList is {} : " + transportersList);

			if (transportersList.size() > 0 && !transportersList.isEmpty()) {

				transportersList.stream().forEach(transporter -> {

					ObjectNode tempNode = getMapper().createObjectNode();
					tempNode.put(PurchaseIndentMetaData.Transporter.ID, count.get());
					tempNode.put(PurchaseIndentMetaData.Transporter.TRANSPORTER_CODE, transporter.getTransporterCode());
					tempNode.put(PurchaseIndentMetaData.Transporter.TRANSPORTER_NAME, transporter.getTransporterName());

					arrNode.add(tempNode);
					count.getAndDecrement();
				});

				node.put(CodeUtils.RESPONSE, arrNode);

				log.info("Final object node data is  {}", node);

				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").put("currPage", pageNo)
								.put("prePage", previousPage).put("maxPage", maxPage).putAll(node))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

				log.info(AppStatusMsg.BLANK);
			}
		} catch (MyBatisSystemException ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CONNECTION_ERROR_CODE,
					AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, AppStatusMsg.CONNECTION_NOT_AVAILABLE);

		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.TRANSPORTER_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.TRANSPORTER_READER_ERROR));

			log.info(ex.getMessage());
		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			log.info(e.getMessage());
		} finally {
			log.info("PurchaseIndent getTransporterDataBySupplierCode() method ends here....");

			return ResponseEntity.ok(appResponse);
		}
	}

	/**
	 * @return list of all suppliers
	 */
	@SuppressWarnings({ "finally", "deprecation" })
	@RequestMapping(method = RequestMethod.GET, value = "/get/supplierdata")
	@ResponseBody
	public ResponseEntity<AppResponse> getSupplierData(@RequestParam("type") Integer type,
			@RequestParam("pageNo") Integer pageNo, @RequestParam("name") String name,
			@RequestParam("code") String code, @RequestParam("address") String address,
			@RequestParam("search") String search, @RequestParam("departmentName") String departmentName,
			@RequestParam("departmentCode") String departmentCode, @RequestParam("siteCode") String siteCode,
			@RequestParam("cityName") String cityName) {

		log.info("PurchaseIndent getSupplierData() method starts here....");

		AppResponse appResponse = null;
		ObjectNode node = getMapper().createObjectNode();
		ArrayNode arrNode = getMapper().createArrayNode();
		List<SupplierData> supplierList = null;

		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;

		try {
			String tenantHashKey = ThreadLocalStorage.getTenantHashKey();
			if (type == 1) {
				totalRecord = this.getRecordSupplierData(tenantHashKey, departmentName, departmentCode, siteCode,
						cityName);

				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;

				supplierList = this.getSupplierData(tenantHashKey, departmentName, departmentCode, siteCode, offset,
						pageSize, cityName);

			} else if (type == 2) {
				String slCode = code.trim();
				String slName = name.trim();
				String slAddr = address.trim();

				totalRecord = this.recordsearchBySupplierData(tenantHashKey, slCode, slName, slAddr, departmentName,
						cityName);

				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;

				supplierList = this.searchBySupplierData(tenantHashKey, offset, pageSize, slCode, slName, slAddr,
						departmentName, cityName);

			}

			else {

				if (type == 3 && !search.isEmpty()) {

					search = search.trim();

					totalRecord = this.recordSearchSupplierData(tenantHashKey, departmentCode, departmentName, siteCode,
							search, cityName);

					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;

					supplierList = this.searchSupplierData(tenantHashKey, departmentCode, departmentName, siteCode,
							offset, pageSize, search, cityName);

				}

			}

			if (supplierList.size() > 0 && !supplierList.isEmpty()) {

				supplierList.stream().forEach(supplier -> {
					ObjectNode tempNode = getMapper().createObjectNode();

					tempNode.put(PurchaseIndentMetaData.Supplier.CODE, supplier.getSlCode());
					tempNode.put(PurchaseIndentMetaData.Supplier.SLID, supplier.getSlId());
					tempNode.put(PurchaseIndentMetaData.Supplier.NAME, supplier.getSlName());
					tempNode.put(PurchaseIndentMetaData.Supplier.ADDRESS, supplier.getSlAddr());
					tempNode.put(PurchaseIndentMetaData.Supplier.CITY, supplier.getCity());
					tempNode.put(PurchaseIndentMetaData.Supplier.GSTIN_STATECODE,
							CodeUtils.isEmpty(supplier.getGstInStateCode()) ? 0
									: Integer.parseInt(supplier.getGstInStateCode()));
					tempNode.put(PurchaseIndentMetaData.Supplier.GSTIN_NO, supplier.getGstinNo());
					tempNode.put(PurchaseIndentMetaData.Supplier.TRANSPORTER_CODE, supplier.getTransporterCode());
					tempNode.put(PurchaseIndentMetaData.Supplier.TRANSPORTER_NAME, supplier.getTransporterName());
					tempNode.put(PurchaseIndentMetaData.Supplier.TRADEGRP_CODE,
							CodeUtils.isEmpty(supplier.getTradeGrpCode()) ? 0
									: Integer.parseInt(supplier.getTradeGrpCode()));
					tempNode.put(PurchaseIndentMetaData.Supplier.PURCHASETERM_CODE,
							CodeUtils.isEmpty(supplier.getPurchaseTermCode()) ? 0
									: Integer.parseInt(supplier.getPurchaseTermCode()));
					tempNode.put(PurchaseIndentMetaData.Supplier.PURCHASETERM_NAME, supplier.getPurchaseTermName());

					arrNode.add(tempNode);
				});

				node.put(CodeUtils.RESPONSE, arrNode);
				log.info("Final Object node data is {} ", node);

				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").put("currPage", pageNo)
								.put("prePage", previousPage).put("maxPage", maxPage).putAll(node))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				log.info(AppStatusMsg.SUCCESS_MSG);

			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

				log.info(AppStatusMsg.BLANK);
			}
		} catch (MyBatisSystemException ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CONNECTION_ERROR_CODE,
					AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, AppStatusMsg.CONNECTION_NOT_AVAILABLE);

		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.SUPPLIER_READER_ERROR, appStatus.getMessage(AppModuleErrors.SUPPLIER_READER_ERROR));

			log.info(ex.getMessage());
		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			log.info(e.getMessage());
		} finally {
			log.info("PurchaseIndentEndPoint getSupplierData() method ends here...");
			return ResponseEntity.ok(appResponse);
		}
	}

	private int getRecordSupplierData(String tenantHashKey, String departmentName, String departmentCode,
			String siteCode, String cityName) {
		if (CodeUtils.isEmpty(cityName)) {
			if (tenantHashKey.equalsIgnoreCase(TenantHash.valueOf("VMART").getHashKey()))
				return piService.getRecordSupplierData(departmentCode, siteCode);
			else
				return piService.getRecordCustomSupplierData(departmentName);
		} else {
			return piService.getRecordSuppliersByCity(cityName);
		}
	}

	private List<SupplierData> searchBySupplierData(String tenantHashKey, int offset, int pageSize, String slCode,
			String slName, String slAddr, String department, String cityName) {
		if (CodeUtils.isEmpty(cityName)) {
			if (tenantHashKey.equalsIgnoreCase(TenantHash.valueOf("VMART").getHashKey()))
				return piService.searchBySupplierData(offset, pageSize, slCode, slName, slAddr, department);
			else
				return piService.searchByCustomSupplierData(offset, pageSize, slCode, slName, slAddr, department);
		} else {
			return piService.searchByCustomSuppliersByCity(offset, pageSize, slCode, slName, slAddr, cityName);
		}

	}

	private int recordsearchBySupplierData(String tenantHashKey, String slCode, String slName, String slAddr,
			String departmentName, String cityName) {
		if (CodeUtils.isEmpty(cityName)) {
			if (tenantHashKey.equalsIgnoreCase(TenantHash.valueOf("VMART").getHashKey()))
				return piService.recordsearchBySupplierData(slCode, slName, slAddr, departmentName);
			else
				return piService.recordsearchByCustomSupplierData(slCode, slName, slAddr, departmentName);
		} else {
			return piService.recordsearchByCustomSuppliersByCity(slCode, slName, slAddr, cityName);
		}

	}

	private List<SupplierData> getSupplierData(String tenantHashKey, String departmentName, String departmentCode,
			String siteCode, int offset, int pageSize, String cityName) {
		if (CodeUtils.isEmpty(cityName)) {
			if (tenantHashKey.equalsIgnoreCase(TenantHash.valueOf("VMART").getHashKey()))
				return piService.getSupplierData(departmentCode, siteCode, offset, pageSize);
			else
				return piService.getCustomSupplierData(departmentName, offset, pageSize);
		} else {
			return piService.getCustomSuppliersByCity(offset, pageSize, cityName);
		}
	}

	private int recordSearchSupplierData(String tenantHashKey, String departmentCode, String departmentName,
			String siteCode, String search, String cityName) {
		if (CodeUtils.isEmpty(cityName)) {
			if (tenantHashKey.equalsIgnoreCase(TenantHash.valueOf("VMART").getHashKey()))
				return piService.recordSearchSupplierData(departmentCode, siteCode, search);
			else
				return piService.recordSearchCustomSupplierData(departmentName, search);
		} else {
			return piService.recordSearchCustomSuppliersByCity(cityName, search);
		}
	}

	private List<SupplierData> searchSupplierData(String tenantHashKey, String departmentCode, String departmentName,
			String siteCode, int offset, int pageSize, String search, String cityName) {
		if (CodeUtils.isEmpty(cityName)) {
			if (tenantHashKey.equalsIgnoreCase(TenantHash.valueOf("VMART").getHashKey()))
				return piService.searchSupplierData(departmentCode, siteCode, offset, pageSize, search);
			else
				return piService.searchCustomSupplierData(departmentName, offset, pageSize, search);
		} else {
			return piService.searchCustomSuppliersByCity(cityName, offset, pageSize, search);
		}

	}

	@SuppressWarnings({ "finally", "deprecation" })
	@RequestMapping(method = RequestMethod.GET, value = "/get/leadtime/supplier/{slcode}")
	@ResponseBody
	public ResponseEntity<AppResponse> getSupplierLeadTime(@PathVariable(value = "slcode") String slCode) {

		log.info("PurchaseIndent getSupplierLeadTime() method starts here....");

		AppResponse appResponse = null;
		ObjectNode node = getMapper().createObjectNode();
		ObjectNode tempNode = getMapper().createObjectNode();

		try {
			Integer leadTime = piService.getSupplierLeadTime(slCode);
			tempNode.put(PurchaseIndentMetaData.Supplier.LEADTIME, leadTime);
			node.put(CodeUtils.RESPONSE, tempNode);

			log.info("Supplier lead time is : {} ", leadTime);

			appResponse = new AppResponse.Builder()
					.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(node))
					.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			log.info(AppStatusMsg.SUCCESS_MSG);

		} catch (MyBatisSystemException ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CONNECTION_ERROR_CODE,
					AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, AppStatusMsg.CONNECTION_NOT_AVAILABLE);

		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.LEADTIME_READER_ERROR, appStatus.getMessage(AppModuleErrors.LEADTIME_READER_ERROR));

			log.info(ex.getMessage());
		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			log.info(e.getMessage());
		} finally {
			log.info("PurchaseIndent getSupplierLeadTime() method ends here....");
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "deprecation", "finally" })
	@RequestMapping(value = "/get/all/pricepoint/{slcode}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getArticle(@PathVariable(value = "slcode") String slcode,
			@RequestParam("type") Integer type, @RequestParam("pageNo") Integer pageNo,
			@RequestParam("articleName") String articleName, @RequestParam("costPrice") String costPrice,
			@RequestParam("sellPrice") String sellPrice, @RequestParam("search") String search,
			@RequestParam("department") String department) {

		AppResponse appResponse = null;
		List<ADMItem> articleData = null;
		ObjectNode objectNode = null;

		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;
		// int count =1;
		try {
			String tenantHashKey = ThreadLocalStorage.getTenantHashKey();

			if (type == 1) {
				totalRecord = this.getRecordPricePoint(tenantHashKey, slcode, department);

				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;

				articleData = this.getPricePoint(tenantHashKey, slcode, department, offset, pageSize);

			} else if (type == 2) {

				totalRecord = this.recordSearchByArticlePricePoint(tenantHashKey, slcode, articleName, department,
						costPrice, sellPrice);
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;

				articleData = this.searchByArticlePricePoint(tenantHashKey, offset, pageSize, slcode, articleName,
						department, costPrice, sellPrice);
			}

			else {

				if (type == 3 && !search.isEmpty()) {
					totalRecord = this.recordArticlePricePoint(tenantHashKey, slcode, department, search);
					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;

					articleData = this.searchArticlePricePoint(tenantHashKey, offset, pageSize, slcode, department,
							search);
				}

			}
			AtomicInteger count = new AtomicInteger(1);
			count.set(((pageNo - 1) * 10) + 1);

			if (!CodeUtils.isEmpty(articleData)) {
				objectNode = getMapper().createObjectNode();
				ArrayNode array = getMapper().createArrayNode();
				articleData.stream().forEach(article -> {
					// ObjectNode node = getMapper().valueToTree(e);
					ObjectNode articleNode = getMapper().createObjectNode();
					articleNode.put(PurchaseIndentMetaData.Article.ID, count.get());
					articleNode.put(PurchaseIndentMetaData.Article.ARTICLE_CODE, article.gethL4Code());
					articleNode.put(PurchaseIndentMetaData.Article.ARTICLE_NAME, article.gethL4Name());
					articleNode.put(PurchaseIndentMetaData.Article.MRP, article.getListedMrp());
					// articleNode.put(PurchaseIndentMetaData.Article.RSP, article.getSellPrice());
					articleNode.put(PurchaseIndentMetaData.Article.MRPSTART, article.getMRPRangeFrom());
					articleNode.put(PurchaseIndentMetaData.Article.MRPEND, article.getMRPRangeTo());
					// articleNode.put(PurchaseIndentMetaData.Article.ITEMCODE, article.getiCode());
					// articleNode.put(PurchaseIndentMetaData.Article.ITEMNAME,
					// article.getItemName());
//					articleNode.put(PurchaseIndentMetaData.Article.HSNSACCODE, article.getHsnSacCode());

					array.add(articleNode);
					count.getAndIncrement();
					// count++;
				});
				objectNode.put(CodeUtils.RESPONSE, array);
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").put("currPage", pageNo)
								.put("prePage", previousPage).put("maxPage", maxPage).putAll(objectNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				log.info(AppStatusMsg.SUCCESS_MSG);

			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				log.info(AppStatusMsg.BLANK);
			}
		} catch (MyBatisSystemException ex) {
			ex.printStackTrace();
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CONNECTION_ERROR_CODE,
					AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, AppStatusMsg.CONNECTION_NOT_AVAILABLE);

		} catch (DataAccessException ex) {
			ex.printStackTrace();
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.ARTICLE_READER_ERROR, appStatus.getMessage(AppModuleErrors.ARTICLE_READER_ERROR));

			log.info(ex.getMessage());

		} catch (Exception ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	private int getRecordPricePoint(String tenantHashKey, String slCode, String department) {

		int totalRecord = 0;
		if (tenantHashKey.equalsIgnoreCase(TenantHash.valueOf("VMART").getHashKey()))
			totalRecord = piService.getRecordPricePoint(slCode, department);
		else
			totalRecord = piService.getCustomRecordPricePoint(department);
		return totalRecord;
	}

	private List<ADMItem> getPricePoint(String tenantHashKey, String slcode, String department, int offset,
			int pageSize) {
		List<ADMItem> items = null;

		if (tenantHashKey.equalsIgnoreCase(TenantHash.valueOf("VMART").getHashKey()))
			items = piService.getPricePoint(slcode, department, offset, pageSize);
		else
			items = piService.getCustomPricePoint(department, offset, pageSize);

		return items;
	}

	private int recordSearchByArticlePricePoint(String tenantHashKey, String slcode, String articleName,
			String department, String costPrice, String sellPrice) {

		int totalRecord = 0;
		if (tenantHashKey.equalsIgnoreCase(TenantHash.valueOf("VMART").getHashKey()))
			totalRecord = piService.recordSearchByArticlePricePoint(slcode, articleName, department, costPrice,
					sellPrice);
		else
			totalRecord = piService.recordCustomSearchByArticlePricePoint(articleName, department, costPrice,
					sellPrice);
		return totalRecord;
	}

	private List<ADMItem> searchByArticlePricePoint(String tenantHashKey, int offset, int pageSize, String slcode,
			String articleName, String department, String costPrice, String sellPrice) {
		List<ADMItem> items = null;

		if (tenantHashKey.equalsIgnoreCase(TenantHash.valueOf("VMART").getHashKey()))
			items = piService.searchByArticlePricePoint(offset, pageSize, slcode, articleName, department, costPrice,
					sellPrice);
		else
			items = piService.searchByCustomArticlePricePoint(offset, pageSize, articleName, department, costPrice,
					sellPrice);

		return items;
	}

	private int recordArticlePricePoint(String tenantHashKey, String slcode, String department, String search) {

		int totalRecord = 0;
		if (tenantHashKey.equalsIgnoreCase(TenantHash.valueOf("VMART").getHashKey()))
			totalRecord = piService.recordArticlePricePoint(slcode, department, search);
		else
			totalRecord = piService.recordCustomArticlePricePoint(department, search);
		return totalRecord;
	}

	private List<ADMItem> searchArticlePricePoint(String tenantHashKey, int offset, int pageSize, String slcode,
			String department, String search) {
		List<ADMItem> items = null;

		if (tenantHashKey.equalsIgnoreCase(TenantHash.valueOf("VMART").getHashKey()))
			items = piService.searchArticlePricePoint(offset, pageSize, slcode, department, search);
		else
			items = piService.searchCustomArticlePricePoint(offset, pageSize, department, search);

		return items;
	}

	@SuppressWarnings({ "deprecation", "finally" })
	@RequestMapping(method = RequestMethod.GET, value = "/get/all/color/{articlecode}")
	@ResponseBody
	public ResponseEntity<AppResponse> getAllColors(@PathVariable(value = "articlecode") String articlecode,
			@RequestParam("type") Integer type, @RequestParam("pageNo") Integer pageNo,
			@RequestParam("cat6") String cat6) {

		log.info("PurchaseIndent getAllColorsByArticleCode() method starts here....");
		List<CatMaster> colorList = null;
		AppResponse appResponse = null;
		ObjectNode node = getMapper().createObjectNode();
		ArrayNode arrNode = getMapper().createArrayNode();

		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;

		try {
			if (type == 1) {

				totalRecord = piService.getRecordAllColors(articlecode);
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;

				colorList = piService.getAllColors(offset, pageSize, articlecode);
			} else {

				if (type == 3 && (!articlecode.isEmpty() || !cat6.isEmpty())) {

					totalRecord = piService.recordAllColors(articlecode, cat6);

					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;

					colorList = piService.searchAllColors(offset, pageSize, articlecode, cat6);
				}
			}

			log.info("Data present in the brandList is : {}" + colorList);

			AtomicInteger count = new AtomicInteger(1);

			if (colorList.size() > 0 && !colorList.isEmpty()) {

				colorList.stream().forEach(color -> {
					ObjectNode tempNode = getMapper().createObjectNode();
					tempNode.put(PurchaseIndentMetaData.Item.ID, count.get());
					tempNode.put(PurchaseIndentMetaData.Item.CODE, color.getCode());
					tempNode.put(PurchaseIndentMetaData.Item.CAT6, color.getCname());

					arrNode.add(tempNode);
					count.getAndIncrement();
				});

				node.put(CodeUtils.RESPONSE, arrNode);

				log.info("Final Object node data is {} ", node);

				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").put("currPage", pageNo)
								.put("prePage", previousPage).put("maxPage", maxPage).putAll(node))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

				log.info(AppStatusMsg.SUCCESS_MSG);
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

				log.info(AppStatusMsg.BLANK);
			}

		} catch (MyBatisSystemException ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CONNECTION_ERROR_CODE,
					AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, AppStatusMsg.CONNECTION_NOT_AVAILABLE);

		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			log.info(e.getMessage());
		} finally {
			log.info("PurchaseIndent getAllBrandsByArticleCode() method ends here....");

			return ResponseEntity.ok(appResponse);
		}

	}

	/***
	 * 
	 * @param hl4Code is article code
	 * @return response which contains the all unique codes i.e. cat1
	 */
	@SuppressWarnings({ "deprecation", "finally" })
	@RequestMapping(value = "/get/all/uniquecode/{hl4code}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getAllUniqueCodeByArticleCode(@PathVariable(value = "hl4code") String hl4Code,
			@RequestParam("type") Integer type, @RequestParam("pageNo") Integer pageNo,
			@RequestParam("cat1") String cat1) {

		log.info("PurchaseIndent getAllUniqueCodeByArticleCode() method starts here....");

		AppResponse appResponse = null;
		ObjectNode node = getMapper().createObjectNode();
		ArrayNode arrNode = getMapper().createArrayNode();
		List<CatMaster> uniqueCodeList = null;

		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;

		try {
			if (type == 1) {
				totalRecord = piService.getRecordAllUniqueCodeByArticleCode(hl4Code);

				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;

				uniqueCodeList = piService.getAllUniqueCodeByArticleCode(offset, pageSize, hl4Code);

			} else {

				if (type == 3 && (!hl4Code.isEmpty() || !cat1.isEmpty())) {

					totalRecord = piService.recordAllUniqueCodeByArticleCode(hl4Code, cat1);

					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;

					uniqueCodeList = piService.searchAllUniqueCodeByArticleCode(offset, pageSize, hl4Code, cat1);

				}
			}
			log.info("Data present in the uniqueCodeList is : {}" + uniqueCodeList);

			AtomicInteger count = new AtomicInteger(1);

			if (uniqueCodeList.size() > 0 && !uniqueCodeList.isEmpty()) {

				uniqueCodeList.stream().forEach(code -> {
					ObjectNode tempNode = getMapper().createObjectNode();
					tempNode.put(PurchaseIndentMetaData.Item.ID, count.get());
					tempNode.put(PurchaseIndentMetaData.Item.CODE, code.getCode());
					tempNode.put(PurchaseIndentMetaData.Item.CAT1, code.getCname());

					arrNode.add(tempNode);
					count.getAndIncrement();
				});

				node.put(CodeUtils.RESPONSE, arrNode);

				log.info("Final Object node data is {} ", node);

				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").put("currPage", pageNo)
								.put("prePage", previousPage).put("maxPage", maxPage).putAll(node))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

				log.info(AppStatusMsg.SUCCESS_MSG);
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

				log.info(AppStatusMsg.BLANK);
			}

		} catch (MyBatisSystemException ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CONNECTION_ERROR_CODE,
					AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, AppStatusMsg.CONNECTION_NOT_AVAILABLE);

		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			log.info(e.getMessage());
		} finally {
			log.info("PurchaseIndent getAllUniqueCodeByArticleCode() method ends here....");

			return ResponseEntity.ok(appResponse);
		}

	}

	/**
	 * @param hl4Code is article code
	 * @return all brands which are mapped to that particular article code
	 */
	@SuppressWarnings({ "unchecked", "rawtypes", "deprecation", "finally" })
	@RequestMapping(value = "/get/all/brand/{hl4code}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getAllBrandsByArticleCode(@PathVariable(value = "hl4code") String hl4Code,
			@RequestParam("type") Integer type, @RequestParam("pageNo") Integer pageNo,
			@RequestParam("cat2") String cat2) {

		log.info("PurchaseIndent getAllBrandsByArticleCode() method starts here....");

		AppResponse appResponse = null;
		ObjectNode node = getMapper().createObjectNode();
		ArrayNode arrNode = getMapper().createArrayNode();

		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;
		List<CatMaster> brandList = null;

		try {
			if (type == 1) {

				totalRecord = piService.getRecordAllBrandsByArticleCode(hl4Code);
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;

				brandList = piService.getAllBrandsByArticleCode(offset, pageSize, hl4Code);

			} else {

				if (type == 3 & (!hl4Code.isEmpty() || !cat2.isEmpty())) {

					totalRecord = piService.recordAllBrandsByArticleCode(hl4Code, cat2);
					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
					brandList = piService.searchAllBrandsByArticleCode(offset, pageSize, hl4Code, cat2);

				}
			}

			log.info("Data present in the brandList is : {}" + brandList);

			AtomicInteger count = new AtomicInteger(1);

			if (brandList.size() > 0 && !brandList.isEmpty()) {

				brandList.stream().forEach(brand -> {
					ObjectNode tempNode = getMapper().createObjectNode();
					tempNode.put(PurchaseIndentMetaData.Item.ID, count.get());
					tempNode.put(PurchaseIndentMetaData.Item.CODE, brand.getCode());
					tempNode.put(PurchaseIndentMetaData.Item.CAT2, brand.getCname());

					arrNode.add(tempNode);
					count.getAndIncrement();
				});

				node.put(CodeUtils.RESPONSE, arrNode);

				log.info("Final Object node data is {} ", node);

				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").put("currPage", pageNo)
								.put("prePage", previousPage).put("maxPage", maxPage).putAll(node))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

				log.info(AppStatusMsg.SUCCESS_MSG);
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

				log.info(AppStatusMsg.BLANK);
			}

		} catch (MyBatisSystemException ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CONNECTION_ERROR_CODE,
					AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, AppStatusMsg.CONNECTION_NOT_AVAILABLE);

		} catch (DataAccessException e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			log.info(e.getMessage());
		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			log.info(e.getMessage());
		} finally {
			log.info("PurchaseIndent getAllBrandsByArticleCode() method ends here....");

			return ResponseEntity.ok(appResponse);
		}

	}

	/**
	 * @param hl4Code is article code
	 * @return all the patterns which are mapped to that article code
	 */
	@SuppressWarnings({ "deprecation", "finally" })
	@RequestMapping(value = "/get/all/pattern/{hl4code}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getAllPatternsByArticleCode(@PathVariable(value = "hl4code") String hl4Code,
			@RequestParam("type") Integer type, @RequestParam("pageNo") Integer pageNo,
			@RequestParam("cat3") String cat3) {

		log.info("PurchaseIndent getAllPatternsByArticleCode() method starts here....");

		AppResponse appResponse = null;
		ObjectNode node = getMapper().createObjectNode();
		ArrayNode arrNode = getMapper().createArrayNode();
		List<CatMaster> patternList = null;
		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;

		try {
			if (type == 1) {
				totalRecord = piService.getRecordAllPatternsByArticleCode(hl4Code);
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;

				patternList = piService.getAllPatternsByArticleCode(offset, pageSize, hl4Code);

			} else {

				if (type == 3 && (!hl4Code.isEmpty() || !cat3.isEmpty())) {

					totalRecord = piService.recordAllPatternsByArticleCode(hl4Code, cat3);

					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;

					patternList = piService.searchAllPatternsByArticleCode(offset, pageSize, hl4Code, cat3);

				}
			}
			log.info("Data present in the patternList is : {}" + patternList);

			AtomicInteger count = new AtomicInteger(1);

			if (patternList.size() > 0 && !patternList.isEmpty()) {

				patternList.stream().forEach(pattern -> {
					ObjectNode tempNode = getMapper().createObjectNode();
					tempNode.put(PurchaseIndentMetaData.Item.ID, count.get());
					tempNode.put(PurchaseIndentMetaData.Item.CODE, pattern.getCode());
					tempNode.put(PurchaseIndentMetaData.Item.CAT3, pattern.getCname());

					arrNode.add(tempNode);
					count.getAndIncrement();
				});

				node.put(CodeUtils.RESPONSE, arrNode);

				log.info("Final Object node data is {} ", node);

				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").put("currPage", pageNo)
								.put("prePage", previousPage).put("maxPage", maxPage).putAll(node))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

				log.info(AppStatusMsg.SUCCESS_MSG);
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

				log.info(AppStatusMsg.BLANK);
			}

		} catch (MyBatisSystemException ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CONNECTION_ERROR_CODE,
					AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, AppStatusMsg.CONNECTION_NOT_AVAILABLE);

		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			log.info(e.getMessage());
		} finally {

			log.info("PurchaseIndent getAllPatternsByArticleCode() method ends here....");

			return ResponseEntity.ok(appResponse);
		}

	}

	/**
	 * @param hl4Code is article code
	 * @return all the assortments which are mapped to that article code
	 */
	@SuppressWarnings({ "unchecked", "rawtypes", "deprecation", "finally" })
	@RequestMapping(value = "/get/all/assortment/{hl4code}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getAllAssortmentsByArticleCode(@PathVariable(value = "hl4code") String hl4Code,
			@RequestParam("type") Integer type, @RequestParam("pageNo") Integer pageNo,
			@RequestParam("cat4") String cat4) {

		log.info("PurchaseIndent getAllAssortmentsByArticleCode() method starts here....");

		AppResponse appResponse = null;
		ObjectNode node = getMapper().createObjectNode();
		ArrayNode arrNode = getMapper().createArrayNode();
		List<CatMaster> assortmentList = null;

		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;

		try {
			if (type == 1) {
				totalRecord = piService.getRecordAllAssortmentsByArticleCode(hl4Code);

				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;

				assortmentList = piService.getAllAssortmentsByArticleCode(offset, pageSize, hl4Code);

			} else {

				if (type == 3 && (!hl4Code.isEmpty() || !cat4.isEmpty())) {

					totalRecord = piService.recordAllAssortmentsByArticleCode(hl4Code, cat4);

					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;

					assortmentList = piService.searchAllAssortmentsByArticleCode(offset, pageSize, hl4Code, cat4);
				}
			}

			log.info("Data present in the assortmentList is : {}" + assortmentList);

			AtomicInteger count = new AtomicInteger(1);

			if (assortmentList.size() > 0 && !assortmentList.isEmpty()) {

				assortmentList.stream().forEach(assortment -> {
					ObjectNode tempNode = getMapper().createObjectNode();
					tempNode.put(PurchaseIndentMetaData.Item.ID, count.get());
					tempNode.put(PurchaseIndentMetaData.Item.CODE, assortment.getCode());
					tempNode.put(PurchaseIndentMetaData.Item.CAT4, assortment.getCname());

					arrNode.add(tempNode);
					count.getAndIncrement();
				});

				node.put(CodeUtils.RESPONSE, arrNode);

				log.info("Final Object node data is {} ", node);

				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").put("currPage", pageNo)
								.put("prePage", previousPage).put("maxPage", maxPage).putAll(node))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

				log.info(AppStatusMsg.SUCCESS_MSG);
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

				log.info(AppStatusMsg.BLANK);
			}

		} catch (MyBatisSystemException ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CONNECTION_ERROR_CODE,
					AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, AppStatusMsg.CONNECTION_NOT_AVAILABLE);

		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			log.info(e.getMessage());
		} finally {
			log.info("PurchaseIndent getAllAssortmentsByArticleCode() method ends here....");

			return ResponseEntity.ok(appResponse);
		}

	}

	/**
	 * 
	 * @param hl4code is the article code
	 * @return all size mapped to a particular article
	 */

	@SuppressWarnings({ "unchecked", "rawtypes", "deprecation", "finally" })
	@RequestMapping(value = "/get/all/size/{hl4code}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getAllSizeByArticleCode(@PathVariable(value = "hl4code") String hl4code,
			@RequestParam("type") Integer type, @RequestParam("pageNo") Integer pageNo,
			@RequestParam("cat5") String cat5) {

		log.info("PurchaseIndent getAllSizeByArticleCode() method starts here....");

		AppResponse appResponse = null;
		ObjectNode node = getMapper().createObjectNode();
		ArrayNode arrNode = getMapper().createArrayNode();
		List<CatMaster> sizeList = null;

		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;

		try {
			if (type == 1) {

				totalRecord = piService.getRecordAllSizeByArticleCode(hl4code);

				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;

				sizeList = piService.getAllSizeByArticleCode(offset, pageSize, hl4code);

			} else {

				if (type == 3 && (!hl4code.isEmpty() || !cat5.isEmpty())) {

					totalRecord = piService.recordAllSizeByArticleCode(hl4code, cat5);

					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;

					sizeList = piService.searchAllSizeByArticleCode(offset, pageSize, hl4code, cat5);

				}
			}

			log.info("Data present in the sizeList is : {}" + sizeList);

			AtomicInteger count = new AtomicInteger(1);

			if (sizeList.size() > 0 && !sizeList.isEmpty()) {

				sizeList.stream().forEach(size -> {
					ObjectNode tempNode = getMapper().createObjectNode();
					tempNode.put(PurchaseIndentMetaData.Item.ID, count.get());
					tempNode.put(PurchaseIndentMetaData.Item.CODE, size.getCode());
					tempNode.put(PurchaseIndentMetaData.Item.CAT5, size.getCname());

					arrNode.add(tempNode);
					count.getAndIncrement();
				});

				node.put(CodeUtils.RESPONSE, arrNode);

				log.info("Final Object node data is {} ", node);

				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").put("currPage", pageNo)
								.put("prePage", previousPage).put("maxPage", maxPage).putAll(node))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

				log.info(AppStatusMsg.SUCCESS_MSG);
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

				log.info(AppStatusMsg.BLANK);
			}

		} catch (MyBatisSystemException ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CONNECTION_ERROR_CODE,
					AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, AppStatusMsg.CONNECTION_NOT_AVAILABLE);

		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			log.info(e.getMessage());
		} finally {
			log.info("PurchaseIndent getAllSizeByArticleCode() method ends here....");

			return ResponseEntity.ok(appResponse);
		}

	}

	@SuppressWarnings({ "unchecked", "rawtypes", "deprecation", "finally" })
	@RequestMapping(value = "/get/article/data/{hl4code}", method = RequestMethod.GET)
	@ResponseBody
	public String getArticleData(@PathVariable(value = "hl4code") String hl4code) {
		return "Hello";
	}

	/**
	 * @param node json data which contains the ratios,number of colors and number
	 *             of sets of a line item
	 * @return qty of a line item
	 */
	@SuppressWarnings({ "deprecation", "finally" })
	@RequestMapping(value = "/calculate/qty/article/data", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> calculateQtyForArticleData(@RequestBody ObjectNode node) {

		log.info("PurchaseIndent calculateQtyForArticleData() method starts here....");
		log.info("JSON data recieved in the node is : {}", node);

		AppResponse appResponse = null;
		ObjectNode tempNode = getMapper().createObjectNode();
		boolean isQtyValue = true;
		try {

			isQtyValue = piService.isPIQuanityValues(node);

			if (isQtyValue == true) {

				Integer qty = piService.calculateQtyForArticleData(node);

				if (!CodeUtils.isEmpty(qty)) {
					ObjectNode objNode = getMapper().createObjectNode();
					objNode.put(PurchaseIndentMetaData.Item.QTY, qty);
					tempNode.put(CodeUtils.RESPONSE, objNode);

					log.info("Final Object Node is : {}", tempNode);

					appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
							.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG).putAll(tempNode))
							.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

					log.info(AppStatusMsg.SUCCESS_MSG);
				} else {
					appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE,
							AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, AppStatusMsg.PI_CALCULATE_QTY);

				}

			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE,
						AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, AppStatusMsg.PI_CALCULATE_QTY);

			}
		} catch (NullPointerException e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE,
					AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, AppStatusMsg.PI_CALCULATE_QTY);

			log.info(e.getMessage());
		} catch (MyBatisSystemException ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CONNECTION_ERROR_CODE,
					AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, AppStatusMsg.CONNECTION_NOT_AVAILABLE);

		} catch (Exception e) {

			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE,
					AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, AppStatusMsg.PI_CALCULATE_QTY);

			log.info(e.getMessage());
		} finally {
			log.info("PurchaseIndent calculateQtyForArticleData() method ends here....");

			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes", "deprecation", "finally" })
	@RequestMapping(value = "/get/all/season/{articlecode}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getAllSeason(@PathVariable(value = "articlecode") String articlecode,
			@RequestParam("type") Integer type, @RequestParam("pageNo") Integer pageNo,
			@RequestParam("desc2") String desc2) {

		log.info("PurchaseIndent getAllSeasonByArticleCode() method starts here....");
		List<DescriptionMaster> seasonList = null;
		AppResponse appResponse = null;
		ObjectNode node = getMapper().createObjectNode();
		ArrayNode arrNode = getMapper().createArrayNode();

		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;

		try {
			if (type == 1) {
				totalRecord = piService.getRecordAllSeasonByArticleCode(articlecode);

				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;

				seasonList = piService.getAllSeasonByArticleCode(offset, pageSize, articlecode);

			} else {

				if (type == 3 && (!articlecode.isEmpty() || !desc2.isEmpty())) {

					totalRecord = piService.recordAllSeasonByArticleCode(articlecode, desc2);

					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;

					seasonList = piService.searchAllSeasonByArticleCode(offset, pageSize, articlecode, desc2);
				}
			}
			log.info("Data present in the seasonList is : {}" + seasonList);

			AtomicInteger count = new AtomicInteger(1);

			if (seasonList.size() > 0 && !seasonList.isEmpty()) {

				seasonList.stream().forEach(season -> {
					ObjectNode tempNode = getMapper().createObjectNode();
					tempNode.put(PurchaseIndentMetaData.Item.ID, count.get());
					tempNode.put(PurchaseIndentMetaData.Item.CODE, season.getCode());
					tempNode.put(PurchaseIndentMetaData.Item.DESC2, season.getDescription());

					arrNode.add(tempNode);
					count.getAndIncrement();
				});

				node.put(CodeUtils.RESPONSE, arrNode);

				log.info("Final Object node data is {} ", node);

				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").put("currPage", pageNo)
								.put("prePage", previousPage).put("maxPage", maxPage).putAll(node))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

				log.info(AppStatusMsg.SUCCESS_MSG);
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

				log.info(AppStatusMsg.BLANK);
			}

		} catch (MyBatisSystemException ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CONNECTION_ERROR_CODE,
					AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, AppStatusMsg.CONNECTION_NOT_AVAILABLE);

		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			log.info(e.getMessage());
		} finally {
			log.info("PurchaseIndent getAllSeasonByArticleCode() method ends here....");

			return ResponseEntity.ok(appResponse);
		}

	}

	@SuppressWarnings({ "unchecked", "rawtypes", "deprecation", "finally" })
	@RequestMapping(value = "/get/all/fabric/{articlecode}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getAllFabric(@PathVariable(value = "articlecode") String articlecode,
			@RequestParam("type") Integer type, @RequestParam("pageNo") Integer pageNo,
			@RequestParam("desc3") String desc3) {

		log.info("PurchaseIndent getAllFabricByArticleCode() method starts here....");
		List<DescriptionMaster> fabricList = null;
		AppResponse appResponse = null;
		ObjectNode node = getMapper().createObjectNode();
		ArrayNode arrNode = getMapper().createArrayNode();
		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;

		try {
			if (type == 1) {
				totalRecord = piService.getRecordAllFabricByArticleCode(articlecode);

				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;

				fabricList = piService.getAllFabricByArticleCode(offset, pageSize, articlecode);

			} else {
				if (type == 3 && (!articlecode.isEmpty() || !desc3.isEmpty())) {

					totalRecord = piService.recordAllFabricByArticleCode(articlecode, desc3);

					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;

					fabricList = piService.searchAllFabricByArticleCode(offset, pageSize, articlecode, desc3);

				}
			}
			log.info("Data present in the fabricList is : {}" + fabricList);

			AtomicInteger count = new AtomicInteger(1);

			if (fabricList.size() > 0 && !fabricList.isEmpty()) {

				fabricList.stream().forEach(fabric -> {
					ObjectNode tempNode = getMapper().createObjectNode();
					tempNode.put(PurchaseIndentMetaData.Item.ID, count.get());
					tempNode.put(PurchaseIndentMetaData.Item.CODE, fabric.getCode());
					tempNode.put(PurchaseIndentMetaData.Item.DESC3, fabric.getDescription());

					arrNode.add(tempNode);
					count.getAndIncrement();
				});

				node.put(CodeUtils.RESPONSE, arrNode);

				log.info("Final Object node data is {} ", node);

				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").put("currPage", pageNo)
								.put("prePage", previousPage).put("maxPage", maxPage).putAll(node))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

				log.info(AppStatusMsg.SUCCESS_MSG);
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

				log.info(AppStatusMsg.BLANK);
			}

		} catch (MyBatisSystemException ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CONNECTION_ERROR_CODE,
					AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, AppStatusMsg.CONNECTION_NOT_AVAILABLE);

		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			log.info(e.getMessage());
		} finally {
			log.info("PurchaseIndent getAllSeasonByArticleCode() method ends here....");

			return ResponseEntity.ok(appResponse);
		}

	}

	@SuppressWarnings({ "unchecked", "rawtypes", "deprecation", "finally" })
	@RequestMapping(value = "/get/all/darwin/{articlecode}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getAllDarwin(@PathVariable(value = "articlecode") String articlecode,
			@RequestParam("type") Integer type, @RequestParam("pageNo") Integer pageNo,
			@RequestParam("desc4") String desc4) {

		log.info("PurchaseIndent getAllDarwinByArticleCode() method starts here....");
		List<DescriptionMaster> darwinList = null;
		AppResponse appResponse = null;
		ObjectNode node = getMapper().createObjectNode();
		ArrayNode arrNode = getMapper().createArrayNode();

		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;

		try {
			if (type == 1) {

				totalRecord = piService.getRecordAllDarwinByArticleCode(articlecode);

				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;

				darwinList = piService.getAllDarwinByArticleCode(offset, pageSize, articlecode);
			} else {

				if (type == 3 && (!articlecode.isEmpty() || !desc4.isEmpty())) {

					totalRecord = piService.recordAllDarwinByArticleCode(articlecode, desc4);

					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;

					darwinList = piService.searchAllDarwinByArticleCode(offset, pageSize, articlecode, desc4);
				}
			}
			log.info("Data present in the darwinList is : {}" + darwinList);

			AtomicInteger count = new AtomicInteger(1);

			if (darwinList.size() > 0 && !darwinList.isEmpty()) {

				darwinList.stream().forEach(darwin -> {
					ObjectNode tempNode = getMapper().createObjectNode();
					tempNode.put(PurchaseIndentMetaData.Item.ID, count.get());
					tempNode.put(PurchaseIndentMetaData.Item.CODE, darwin.getCode());
					tempNode.put(PurchaseIndentMetaData.Item.DESC4, darwin.getDescription());

					arrNode.add(tempNode);
					count.getAndIncrement();
				});

				node.put(CodeUtils.RESPONSE, arrNode);

				log.info("Final Object node data is {} ", node);

				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").put("currPage", pageNo)
								.put("prePage", previousPage).put("maxPage", maxPage).putAll(node))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

				log.info(AppStatusMsg.SUCCESS_MSG);
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

				log.info(AppStatusMsg.BLANK);
			}

		} catch (MyBatisSystemException ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CONNECTION_ERROR_CODE,
					AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, AppStatusMsg.CONNECTION_NOT_AVAILABLE);

		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			log.info(e.getMessage());
		} finally {
			log.info("PurchaseIndent getAllDarwinByArticleCode() method ends here....");

			return ResponseEntity.ok(appResponse);
		}

	}

	@SuppressWarnings({ "unchecked", "rawtypes", "deprecation", "finally" })
	@RequestMapping(value = "/get/all/weaved/{articlecode}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getAllWeaved(@PathVariable(value = "articlecode") String articlecode,
			@RequestParam("type") Integer type, @RequestParam("pageNo") Integer pageNo,
			@RequestParam("desc5") String desc5) {
		log.info("PurchaseIndent getAllWeavedByArticleCode() method starts here....");
		List<DescriptionMaster> weavedList = null;
		AppResponse appResponse = null;
		ObjectNode node = getMapper().createObjectNode();
		ArrayNode arrNode = getMapper().createArrayNode();

		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;

		try {

			if (type == 1) {

				totalRecord = piService.getRecordAllWeavedByArticleCode(articlecode);

				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;

				weavedList = piService.getAllWeavedByArticleCode(offset, pageSize, articlecode);

			} else {

				if (type == 3 && (!articlecode.isEmpty() || !desc5.isEmpty())) {

					totalRecord = piService.recordAllWeavedByArticleCode(articlecode, desc5);
					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
					weavedList = piService.searchAllWeavedByArticleCode(offset, pageSize, articlecode, desc5);

				}

			}
			log.info("Data present in the weavedList is : {}" + weavedList);

			AtomicInteger count = new AtomicInteger(1);

			if (weavedList.size() > 0 && !weavedList.isEmpty()) {

				weavedList.stream().forEach(weaved -> {
					ObjectNode tempNode = getMapper().createObjectNode();
					tempNode.put(PurchaseIndentMetaData.Item.ID, count.get());
					tempNode.put(PurchaseIndentMetaData.Item.CODE, weaved.getCode());
					tempNode.put(PurchaseIndentMetaData.Item.DESC5, weaved.getDescription());

					arrNode.add(tempNode);
					count.getAndIncrement();
				});

				node.put(CodeUtils.RESPONSE, arrNode);

				log.info("Final Object node data is {} ", node);

				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").put("currPage", pageNo)
								.put("prePage", previousPage).put("maxPage", maxPage).putAll(node))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

				log.info(AppStatusMsg.SUCCESS_MSG);
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

				log.info(AppStatusMsg.BLANK);
			}

		} catch (MyBatisSystemException ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CONNECTION_ERROR_CODE,
					AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, AppStatusMsg.CONNECTION_NOT_AVAILABLE);

		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			log.info(e.getMessage());
		} finally {
			log.info("PurchaseIndent getAllWeavedByArticleCode() method ends here....");

			return ResponseEntity.ok(appResponse);
		}

	}

	/*
	 * @SuppressWarnings("finally")
	 * 
	 * @RequestMapping(value = "/create", method = RequestMethod.POST)
	 * 
	 * @ResponseBody public ResponseEntity<AppResponse> createPI(@RequestBody
	 * JsonNode piItem) {
	 * 
	 * AppResponse appResponse = null; int result[] = null; List finResult = null;
	 * boolean itemInsert = true; PurchaseIndent data = null; PurchaseIndent
	 * foundData = null; PurchaseIndent item = null; PurchaseIndent tempData = null;
	 * PurchaseIndent pdfData = null; List<PurchaseIndentDetail> pid = null;
	 * List<PurchaseIndentDetail> fpid = null; List<PurchaseIndentDetail>
	 * tempDetail1 = null; List<PurchaseIndentDetail> tempDetail2 = null;
	 * PurchaseIndentDetail pDetail = null; ObjectNode objectNode = null;
	 * PurchaseIndentMaster dataKey = null; String firstPattern = "S/PR-PID/AA0001";
	 * List<String> _pathList = null; int count = 0; double tempPOAmount = 0; int
	 * tempPOQuantity = 0; TransactionDefinition defaultTxn = new
	 * DefaultTransactionDefinition(); TransactionStatus ts =
	 * txManager.getTransaction(defaultTxn); Set<String> article; Set<String>
	 * design; Set<OffsetDateTime> deliveryDate; int pdfIncremental = 0;
	 * 
	 * String orderDetailId = null; String ipAddress = ""; String indentOrderNo =
	 * ""; List<PurchaseIndent> indentPdf; double indentAmount = 0.0;
	 * 
	 * try {
	 * 
	 * dataKey = getMapper().treeToValue(piItem, PurchaseIndentMaster.class);
	 * 
	 * String tempPayload = piItem.toString();
	 * 
	 * item = dataKey.getPiKey(); log.info("pidetails :" + item.getPiDetails());
	 * indentPdf = new ArrayList<PurchaseIndent>();
	 * 
	 * // if (item.isInvItem() == false) { tempData =
	 * getMapper().treeToValue(piItem.get("piKey"), PurchaseIndent.class); pdfData =
	 * getMapper().treeToValue(piItem.get("piKey"), PurchaseIndent.class); data =
	 * getMapper().treeToValue(piItem.get("piKey"), PurchaseIndent.class); foundData
	 * = getMapper().treeToValue(piItem.get("piKey"), PurchaseIndent.class);
	 * 
	 * if (!CodeUtils.isEmpty(tempData.getHl1Name()) &&
	 * !CodeUtils.isEmpty(tempData.getHl2Name()) &&
	 * !CodeUtils.isEmpty(tempData.getHl3Name()) &&
	 * !CodeUtils.isEmpty(tempData.getSlName()) && (tempData.getPoAmount() > 0.0) &&
	 * (tempData.getPoQuantity() > 0.0)) {
	 * 
	 * pid = item.getPiDetails(); // fpid = item.getFpiDetails();
	 * 
	 * tempDetail1 = new ArrayList<PurchaseIndentDetail>(); tempDetail2 = new
	 * ArrayList<PurchaseIndentDetail>();
	 * 
	 * article = new HashSet<String>(); design = new HashSet<String>(); deliveryDate
	 * = new HashSet<OffsetDateTime>();
	 * 
	 * data.setPiDetails(null); data.setPiDetails(tempDetail2); //
	 * data.setFpiDetails(null); // data.setFpiDetails(new
	 * ArrayList<PurchaseIndentDetail>()); foundData.setPiDetails(null);
	 * foundData.setPiDetails(new ArrayList<PurchaseIndentDetail>());
	 * 
	 * if (!CodeUtils.isEmpty(tempData.getPiDetails().get(0).getSizeList())) {
	 * tempData.setPiDetails(null); tempData.setPiDetails(tempDetail1);
	 * 
	 * // set pidetails according to size and color combination
	 * List<PurchaseIndentDetail> detail = new ArrayList<PurchaseIndentDetail>();
	 * PurchaseIndentMaster purchaseIndentMaster =
	 * piService.getPurchaseIndentMaster(dataKey);
	 * 
	 * OffsetDateTime currentTime = OffsetDateTime.now(); currentTime =
	 * currentTime.plusHours(new Long(5)); currentTime = currentTime.plusMinutes(new
	 * Long(30)); ipAddress =
	 * InetAddress.getLocalHost().getHostAddress().trim().toString(); String email =
	 * ""; if (!CodeUtils.isEmpty(request)) { email =
	 * request.getAttribute("email").toString(); }
	 * 
	 * pdfData.setCreatedTime(currentTime); pdfData.setIpAddress(ipAddress);
	 * 
	 * pdfData.setActive(PurchaseIndentMetaData.PIStatus.INACTIVE);
	 * pdfData.setStatus(PurchaseIndentMetaData.PIStatus.STATUS);
	 * 
	 * tempData.setIpAddress(ipAddress); tempData.setCreatedTime(currentTime);
	 * tempData.setCreatedBy(email);
	 * tempData.setActive(PurchaseIndentMetaData.PIStatus.INACTIVE);
	 * tempData.setStatus(PurchaseIndentMetaData.PIStatus.STATUS);
	 * 
	 * for (int i = 0; i < pid.size(); i++) {
	 * 
	 * purchaseIndentMaster.getPiKey().setPiDetails(new
	 * ArrayList<PurchaseIndentDetail>()); log.info("Pi detail " +
	 * CodeUtils.isEmpty(purchaseIndentMaster.getPiKey().getPiDetails()));
	 * purchaseIndentMaster.getPiKey().getPiDetails().add(0, pid.get(i));
	 * 
	 * JsonNode nodeMaster = getMapper().convertValue(purchaseIndentMaster,
	 * JsonNode.class); pid.get(i).setDetailJsonNode(nodeMaster.toString());
	 * 
	 * pid.get(i).setCreatedTime(currentTime); pid.get(i).setCreatedBy(email);
	 * pid.get(i).setIpAddress(ipAddress);
	 * 
	 * log.info("before if condition :" + orderDetailId); log.info("delivery date :"
	 * + pid.get(i).getDeliveryDate()); log.info("design :" +
	 * pid.get(i).getDesign()); log.info("hl4code :" + pid.get(i).getHl4Code());
	 * 
	 * // log.info("delivery date :" +
	 * deliveryDate.add(pid.get(i).getDeliveryDate())); // log.info("design:" +
	 * design.add(pid.get(i).getDesign())); // log.info("hl4code" +
	 * article.add(pid.get(i).getHl4Code()));
	 * 
	 * boolean isDeliveryDate = deliveryDate.add(pid.get(i).getDeliveryDate());
	 * boolean isDesign = design.add(pid.get(i).getDesign().toLowerCase()); boolean
	 * isArticle = article.add(pid.get(i).getHl4Code());
	 * 
	 * log.info("delivery date set :" + deliveryDate.toString());
	 * log.info("design set :" + design.toString()); log.info("article set :" +
	 * article.toString());
	 * 
	 * if (isDeliveryDate || isDesign || isArticle) {
	 * 
	 * indentPdf.add(piService.setPurchaseIndent(tempData));
	 * 
	 * indentPdf.get(i).setCreatedTime(currentTime);
	 * indentPdf.get(i).setCreatedBy(email);
	 * 
	 * if (i == 0) { orderDetailId = piService.getLastPattern(); }
	 * 
	 * if (!CodeUtils.isEmpty(orderDetailId)) { orderDetailId =
	 * piService.createPattern(orderDetailId);
	 * pid.get(i).setOrderDetailId(orderDetailId); } else { orderDetailId =
	 * firstPattern; pid.get(i).setOrderDetailId(firstPattern); } indentOrderNo =
	 * indentOrderNo + ", " + orderDetailId; indentAmount = 0; indentAmount =
	 * pid.get(i).getNetAmountTotal();
	 * indentPdf.get(i).getPiDetails().add(pid.get(i));
	 * 
	 * indentPdf.get(i).setPoAmount(indentAmount); pdfIncremental = 1;
	 * 
	 * log.info("Inside if :" + orderDetailId); } else { // orderDetailId =
	 * piService.createPattern(orderDetailId);
	 * 
	 * log.info("else order detail :" + orderDetailId);
	 * pid.get(i).setOrderDetailId(orderDetailId);
	 * 
	 * indentAmount = indentAmount + pid.get(i).getNetAmountTotal();
	 * 
	 * indentPdf.get(i - pdfIncremental).setPoAmount(indentAmount); indentPdf.get(i
	 * - pdfIncremental).getPiDetails().add(pid.get(i));
	 * 
	 * ++pdfIncremental; }
	 * 
	 * log.info("detail Id :" + orderDetailId);
	 * 
	 * for (int j = 0; j < pid.get(i).getSizeList().size(); j++) { //
	 * log.info("size list :" + pid.get(i).getSizeList().size());
	 * 
	 * for (int k = 0; k < pid.get(i).getColorList().size(); k++) {
	 * 
	 * // log.info("color list :" + pid.get(i).getColorList().size()); pDetail = new
	 * PurchaseIndentDetail(); pDetail =
	 * piService.setPurchaseIndentDetail(pid.get(i));
	 * 
	 * pDetail.setSizeCode(pid.get(i).getSizeList().get(j).getCode());
	 * pDetail.setSizeName(pid.get(i).getSizeList().get(j).getCname()); //
	 * pDetail.setRatio(pid.get(i).getRatioList().get(j));
	 * 
	 * pDetail.setColorCode(pid.get(i).getColorList().get(k).getCode());
	 * pDetail.setColorName(pid.get(i).getColorList().get(k).getCname());
	 * detail.add(pDetail); } } purchaseIndentMaster.getPiKey().setPiDetails(null);
	 * 
	 * } tempData.getPiDetails().addAll(detail); log.info("permutations created"); }
	 * 
	 * result = new int[tempData.getPiDetails().size()];
	 * 
	 * for (PurchaseIndentDetail tempPID : tempData.getPiDetails()) { tempPOQuantity
	 * = tempPOQuantity + tempPID.getQuantity(); tempPOAmount = tempPOAmount +
	 * tempPID.getNetAmountTotal(); } tempData.setPoQuantity(tempPOQuantity);
	 * tempData.setPoAmount(tempPOAmount);
	 * 
	 * // tempData.setJsonPayload(tempPayload);
	 * 
	 * int n = piService.insertPI(tempData); if (n != 0) {
	 * log.info("PIMAIN table data created");
	 * 
	 * for (int i = 0; i < tempData.getPiDetails().size(); i++) {
	 * tempData.getPiDetails().get(i).setOrderId(tempData.getId());
	 * log.info(tempData.getPiDetails().get(i).toString()); result[i] =
	 * piService.insertPIDetails(tempData.getPiDetails().get(i)); }
	 * 
	 * finResult = new ArrayList<Integer>();
	 * 
	 * for (int i = 0; i < pid.size(); i++) { for (int j = 0; j <
	 * pid.get(i).getFinCharge().size(); j++) { //
	 * pid.get(i).getFinCharge().get(j).setDetailId(pid.get(i).getOrderDetailId());
	 * pid.get(i).getFinCharge().get(j).setCreatedBy(pid.get(i).getCreatedBy());
	 * pid.get(i).getFinCharge().get(j).setCreatedTime(pid.get(i).getCreatedTime());
	 * pid.get(i).getFinCharge().get(j).setIpAddress(ipAddress);
	 * pid.get(i).getFinCharge().get(j).setActive(PurchaseIndentMetaData.PIStatus.
	 * INACTIVE);
	 * pid.get(i).getFinCharge().get(j).setStatus(PurchaseIndentMetaData.PIStatus.
	 * STATUS);
	 * 
	 * finResult.add(piService.insertFinCharge(pid.get(i).getFinCharge().get(j))); }
	 * }
	 * 
	 * String strFin = finResult.toString();
	 * 
	 * for (int i : result) { if (strFin.contains("0")) { result[i] = 0;
	 * txManager.rollback(ts); break; } }
	 * 
	 * for (int i : result) { if (i == 0) { itemInsert = false; break; } }
	 * 
	 * if (itemInsert == true) { log.info("PIDET and PICHGDET tables created");
	 * txManager.commit(ts);
	 * 
	 * appResponse = new AppResponse.Builder() .data(getMapper().createObjectNode()
	 * .put(CodeUtils.RESPONSE_MSG, AppModuleMsg.PI_SUCCESS + " " +
	 * indentOrderNo.substring(2)) .putPOJO(CodeUtils.RESPONSE, null))
	 * .status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
	 * 
	 * for (PurchaseIndent pdf : indentPdf) {
	 * 
	 * for (int i = 0; i < pdf.getPiDetails().size(); i++) {
	 * 
	 * for (int k = 0; k < pdf.getPiDetails().get(i).getImage().size(); k++) { if (k
	 * == 0) { pdf.getPiDetails().get(i)
	 * .setImage1(pdf.getPiDetails().get(i).getImage().get(k)); } if (k == 1) {
	 * pdf.getPiDetails().get(i)
	 * .setImage2(pdf.getPiDetails().get(i).getImage().get(k)); } if (k == 2) {
	 * pdf.getPiDetails().get(i)
	 * .setImage3(pdf.getPiDetails().get(i).getImage().get(k)); } if (k == 3) {
	 * pdf.getPiDetails().get(i)
	 * .setImage4(pdf.getPiDetails().get(i).getImage().get(k)); } if (k == 4) {
	 * pdf.getPiDetails().get(i)
	 * .setImage5(pdf.getPiDetails().get(i).getImage().get(k)); } } }
	 * 
	 * String pdfPath = piService.buildReport(pdf);
	 * 
	 * log.info((!CodeUtils.isEmpty(pdfPath) ? "Purchase Indent PDF created" :
	 * "Purchase Indent PDF  not created")); }
	 * 
	 * } else { txManager.rollback(ts); appResponse =
	 * buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
	 * AppModuleErrors.PI_CREATOR_ERROR,
	 * appStatus.getMessage(AppModuleErrors.PI_CREATOR_ERROR)); } } else {
	 * txManager.rollback(ts);
	 * log.info(appStatus.getMessage(AppModuleErrors.PI_CREATOR_ERROR)); appResponse
	 * = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
	 * AppModuleErrors.PI_CREATOR_ERROR,
	 * appStatus.getMessage(AppModuleErrors.PI_CREATOR_ERROR)); }
	 * 
	 * } else { appResponse =
	 * buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE,
	 * ClientErrors.BAD_REQUEST, AppStatusMsg.PI_INPUTS);
	 * 
	 * } } catch (MyBatisSystemException ex) { log.info(ex.getMessage());
	 * 
	 * txManager.rollback(ts); appResponse =
	 * buildErrorResponse(AppStatusCodes.GENERIC_CONNECTION_ERROR_CODE,
	 * AppStatusCodes.GENERIC_CLIENT_ERROR_CODE,
	 * AppStatusMsg.CONNECTION_NOT_AVAILABLE);
	 * 
	 * } catch (DataAccessException ex) { log.info(ex.getMessage());
	 * txManager.rollback(ts); appResponse =
	 * buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
	 * AppModuleErrors.PI_CREATOR_ERROR,
	 * appStatus.getMessage(AppModuleErrors.PI_CREATOR_ERROR)); } catch
	 * (NumberFormatException ex) { log.info(ex.getMessage()); appResponse =
	 * buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE,
	 * ClientErrors.BAD_REQUEST, AppStatusMsg.NUMBER_FORMAT_MSG);
	 * 
	 * } catch (JsonMappingException jsonException) {
	 * log.info(jsonException.getMessage()); appResponse =
	 * buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE,
	 * ClientErrors.BAD_REQUEST, AppStatusMsg.INVALID_JSON);
	 * 
	 * } catch (SupplyMintException ex) { log.info(ex.getMessage());
	 * txManager.rollback(ts); appResponse =
	 * buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
	 * AppModuleErrors.PI_PDF_CREATOR_ERROR,
	 * appStatus.getMessage(AppModuleErrors.PI_PDF_CREATOR_ERROR));
	 * 
	 * } catch (Exception e) { log.info(e.getMessage()); appResponse =
	 * buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
	 * ClientErrors.BAD_REQUEST, AppStatusMsg.FAILURE_MSG); } finally { return
	 * ResponseEntity.ok(appResponse); }
	 * 
	 * }
	 */
	@SuppressWarnings({ "finally" })
	@RequestMapping(value = "/get/history", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getPurchaseIndentHistory(@RequestParam("pageno") Integer pageNo,
			@RequestParam("type") Integer type, @RequestParam("pattern") String pattern,
			@RequestParam("supplierName") String supplierName, @RequestParam("articleCode") String articleCode,
			@RequestParam("division") String division, @RequestParam("status") String status,
			@RequestParam("section") String section, @RequestParam("department") String department,
			@RequestParam("generatedDate") String generatedDate, @RequestParam("search") String search,
			@RequestParam("slCityName") String slCityName, @RequestParam("deliveryDateFrom") String deliveryDateFrom,
			@RequestParam("deliveryDateTo") String deliveryDateTo, @RequestParam("desc2Code") String desc2Code,
			HttpServletRequest request) {

		AppResponse appResponse = null;
		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;// ,
		int offset = 0;
		int maxPage = 0;
		ObjectNode objectNode;
		List<PurchaseIndentHistory> data = null;
		try {
			String token = request.getHeader("X-Auth-Token");
			JsonArray orgDetails = CodeUtils.decode(token).get("ORG_DETAILS").getAsJsonArray();
			JsonObject jNode = orgDetails.get(0).getAsJsonObject();
			String orgId = jNode.get("orgId").getAsString();

			if (type == 1) {
				totalRecord = piService.record(orgId);
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo - 1) * pageSize;
				data = piService.getIndentHistory(offset, pageSize, orgId);

			} else if (type == 2) {

				totalRecord = piService.countIndentHistoryFilter(pattern, supplierName, articleCode, division, status,
						section, department, generatedDate, slCityName, deliveryDateFrom, deliveryDateTo, desc2Code,
						orgId);

				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo - 1) * pageSize;

				data = piService.getIndentHistoryFilter(offset, pageSize, pattern, supplierName, articleCode, division,
						status, section, department, generatedDate, slCityName, deliveryDateFrom, deliveryDateTo,
						desc2Code, orgId);
			} else if (type == 3 && !CodeUtils.isEmpty(search)) {
				totalRecord = piService.countIndentHistorySearch(search, orgId);
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo - 1) * pageSize;
				data = piService.getIndentHistorySearch(offset, pageSize, search, orgId);

			}

			if (!CodeUtils.isEmpty(data)) {
				objectNode = buildJson(data);
				appResponse = successResponse(pageNo, previousPage, maxPage, objectNode);
				log.info(AppStatusMsg.SUCCESS_MSG);
			} else {
				appResponse = blankResponse();
				log.info(AppStatusMsg.BLANK);
			}
		} catch (IndexOutOfBoundsException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.PI_HISTORY_ERROR,
					appStatus.getMessage(AppModuleErrors.PI_HISTORY_ERROR));

			log.info(ex.getMessage());
		} catch (MyBatisSystemException ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CONNECTION_ERROR_CODE,
					AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, AppStatusMsg.CONNECTION_NOT_AVAILABLE);

		} catch (DataAccessException ex) {
			ex.printStackTrace();
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.PI_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.PI_READER_ERROR));

			log.info(ex.getMessage());
		} catch (Exception ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);

			log.info(ex.getMessage());
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings("finally")
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> createPITemp(@RequestBody JsonNode piItem, HttpServletRequest request) {

		AppResponse appResponse = null;
		List finResult = null;
		boolean itemInsert = true;
		PurchaseIndent item = null;
		List<PurchaseIndentDetail> pid = null;
		PurchaseIndentMaster dataKey = null;
		String firstPattern = "S/PR-PID/AA0001";
		TransactionDefinition defaultTxn = new DefaultTransactionDefinition();
		TransactionStatus ts = txManager.getTransaction(defaultTxn);
		Set<String> article;
		Set<String> design;
		Set<OffsetDateTime> deliveryDate;
		int pdfIncremental = 0;

		String orderDetailId = null;
		String ipAddress = "";
		String indentOrderNo = "";
		List<PurchaseIndent> indentPdf;
		PurchaseIndent indentData = null;
		double indentAmount = 0.0;
		int newOrderIndex = 0;
		FinChargeDetail chargeDetail = null;

		try {
			String tenantHashKey = ThreadLocalStorage.getTenantHashKey();
			String token = request.getHeader("X-Auth-Token");
			JsonArray orgDetails = CodeUtils.decode(token).get("ORG_DETAILS").getAsJsonArray();
			JsonObject jNode = orgDetails.get(0).getAsJsonObject();
			String orgId = jNode.get("orgId").getAsString();
			indentData = new PurchaseIndent();
			dataKey = getMapper().treeToValue(piItem, PurchaseIndentMaster.class);

//			item = dataKey.getPiKey();

			item = getMapper().treeToValue(piItem.get("piKey"), PurchaseIndent.class);

//			indentData = dataKey.getPiKey();
			indentData = getMapper().treeToValue(piItem.get("piKey"), PurchaseIndent.class);

			indentData.setPiDetails(null);
			indentData.setPiDetails(new ArrayList<PurchaseIndentDetail>());

			indentData.setOrgId(orgId);

			indentPdf = new ArrayList<PurchaseIndent>();

			if (!CodeUtils.isEmpty(item.getHl1Name()) && !CodeUtils.isEmpty(item.getHl2Name())
					&& !CodeUtils.isEmpty(item.getHl3Name()) && !CodeUtils.isEmpty(item.getSlName())
					&& (item.getPoAmount() > 0.0) && (item.getPoQuantity() > 0.0)) {

				pid = item.getPiDetails();

				article = new HashSet<String>();
				design = new HashSet<String>();
				deliveryDate = new HashSet<OffsetDateTime>();

				OffsetDateTime currentTime = OffsetDateTime.now();
				currentTime = currentTime.plusHours(new Long(5));
				currentTime = currentTime.plusMinutes(new Long(30));
				ipAddress = InetAddress.getLocalHost().getHostAddress().trim().toString();
				String email = "";
				if (!CodeUtils.isEmpty(request)) {
					email = request.getAttribute("name").toString();
				}

				indentData.setIpAddress(ipAddress);
				indentData.setCreatedTime(currentTime);
				indentData.setCreatedBy(email);
				indentData.setActive(PurchaseIndentMetaData.PIStatus.INACTIVE);
				indentData.setStatus(PurchaseIndentMetaData.PIStatus.STATUS);

				for (int i = 0; i < pid.size(); i++) {

					pid.get(i).setCreatedTime(currentTime);
					pid.get(i).setCreatedBy(email);
					pid.get(i).setIpAddress(ipAddress);
					pid.get(i).setActive(PurchaseIndentMetaData.PIStatus.INACTIVE);
					pid.get(i).setStatus(PurchaseIndentMetaData.PIStatus.STATUS);

//					log.info("before if condition :" + orderDetailId);
//					log.info("delivery date :" + pid.get(i).getDeliveryDate());
//					log.info("design :" + pid.get(i).getDesign());
//					log.info("hl4code :" + pid.get(i).getHl4Code());

					boolean isDeliveryDate = deliveryDate.add(pid.get(i).getDeliveryDate());
					boolean isDesign = design.add(pid.get(i).getDesign().toLowerCase());
					boolean isArticle = article.add(pid.get(i).getHl4Code());

					log.info("delivery date set :" + deliveryDate.toString());
					log.info("design set :" + design.toString());
					log.info("article set :" + article.toString());

					if (isDeliveryDate || isDesign || isArticle) {

						indentPdf.add(piService.setPurchaseIndent(item));
						indentPdf.get(newOrderIndex).setCreatedTime(currentTime);
						indentPdf.get(newOrderIndex).setCreatedBy(email);

						if (i == 0) {
							orderDetailId = piService.getLastPattern();
						}

						if (!CodeUtils.isEmpty(orderDetailId)) {
							orderDetailId = piService.createPattern(orderDetailId);
							pid.get(i).setOrderDetailId(orderDetailId);
						} else {
							orderDetailId = firstPattern;
							pid.get(i).setOrderDetailId(firstPattern);
						}
						indentOrderNo = indentOrderNo + ", " + orderDetailId;
						indentAmount = 0;
						indentAmount = pid.get(i).getNetAmountTotal();

						indentPdf.get(newOrderIndex).getPiDetails().add(pid.get(i));
						indentPdf.get(newOrderIndex).setPoAmount(indentAmount);
						pdfIncremental = 1;
						++newOrderIndex;

						log.info("Inside if :" + orderDetailId);
					} else {

						log.info("else order detail :" + orderDetailId);
						pid.get(i).setOrderDetailId(orderDetailId);

						indentAmount = indentAmount + pid.get(i).getNetAmountTotal();

						indentPdf.get(i - pdfIncremental).setPoAmount(indentAmount);
						indentPdf.get(i - pdfIncremental).getPiDetails().add(pid.get(i));

						++pdfIncremental;
					}
				}

				for (PurchaseIndent pi : indentPdf) {
					for (PurchaseIndentDetail piDetail : pi.getPiDetails()) {

						for (int k = 0; k < piDetail.getImage().size(); k++) {
							if (k == 0) {
								piDetail.setImage1(piDetail.getImage().get(k));
							}
							if (k == 1) {
								piDetail.setImage2(piDetail.getImage().get(k));

							}
							if (k == 2) {
								piDetail.setImage3(piDetail.getImage().get(k));

							}
							if (k == 3) {
								piDetail.setImage4(piDetail.getImage().get(k));

							}
							if (k == 4) {
								piDetail.setImage5(piDetail.getImage().get(k));

							}
						}

						ArrayNode colorArray = getMapper().valueToTree(piDetail.getColorList());
						ArrayNode sizeArray = getMapper().valueToTree(piDetail.getSizeList());
						ArrayNode ratioArray = getMapper().valueToTree(piDetail.getRatioList());

						piDetail.setColor(colorArray.toString());
						piDetail.setSize(sizeArray.toString());
						piDetail.setRatios(ratioArray.toString());

						if (!CodeUtils.isEmpty(piDetail.getUdf())) {
							JsonNode udfNode = getMapper().valueToTree(piDetail.getUdf());
							piDetail.setUdfString(udfNode.toString());
						}

						indentData.getPiDetails().add(piDetail);
					}
				}

				int detailResult = 0;
				int chgResult = 0;
				String piResult = "";
				int n = piService.insertPI(indentData);
				if (n != 0) {
					log.info("PIMAIN table data created");
					finResult = new ArrayList<Integer>();

					for (int i = 0; i < indentData.getPiDetails().size(); i++) {
						indentData.getPiDetails().get(i).setOrderId(indentData.getId());
						log.info(indentData.getPiDetails().get(i).toString());
						if (tenantHashKey.equalsIgnoreCase(TenantHash.valueOf("VMART").getHashKey())) {
							detailResult = piService.insertPIDetails(indentData.getPiDetails().get(i));
						} else {
							detailResult = piService.insertPIDetailsForOthers(indentData.getPiDetails().get(i));
						}

						piResult = piResult + detailResult + ",";

						if (!piResult.contains("0")) {
							for (int j = 0; j < indentData.getPiDetails().get(i).getFinCharge().size(); j++) {
								indentData.getPiDetails().get(i).getFinCharge().get(j)
										.setDetailId(indentData.getPiDetails().get(i).getId());
								indentData.getPiDetails().get(i).getFinCharge().get(j)
										.setCreatedBy(indentData.getPiDetails().get(i).getCreatedBy());
								indentData.getPiDetails().get(i).getFinCharge().get(j)
										.setCreatedTime(indentData.getPiDetails().get(i).getCreatedTime());
								indentData.getPiDetails().get(i).getFinCharge().get(j)
										.setIpAddress(indentData.getPiDetails().get(i).getIpAddress());
								indentData.getPiDetails().get(i).getFinCharge().get(j)
										.setActive(PurchaseIndentMetaData.PIStatus.INACTIVE);
								indentData.getPiDetails().get(i).getFinCharge().get(j)
										.setStatus(PurchaseIndentMetaData.PIStatus.STATUS);

//								log.info("charge Node : " + indentData.getPiDetails().get(i).getFinCharge().get(j)
//										.getCharges().toString());

								if (!CodeUtils
										.isEmpty(indentData.getPiDetails().get(i).getFinCharge().get(j).getCharges())) {

//									ObjectNode charges = getMapper().valueToTree(
//											indentData.getPiDetails().get(i).getFinCharge().get(j).getCharges());

									indentData.getPiDetails().get(i).getFinCharge().get(j).setChargesString(indentData
											.getPiDetails().get(i).getFinCharge().get(j).getCharges().toString());

									if (!CodeUtils.isEmpty(
											indentData.getPiDetails().get(i).getFinCharge().get(j).getRates())) {
										indentData.getPiDetails().get(i).getFinCharge().get(j).setRatesString(indentData
												.getPiDetails().get(i).getFinCharge().get(j).getRates().toString());

									}
									log.info("charge Node : " + indentData.getPiDetails().get(i).getFinCharge().get(j)
											.getCharges().toString());
								}
								
								if (tenantHashKey.equalsIgnoreCase(TenantHash.valueOf("VMART").getHashKey())) {
									chgResult = piService
											.insertFinCharge(indentData.getPiDetails().get(i).getFinCharge().get(j));
								} else {
									chgResult = piService
											.insertFinChargeForOthers(indentData.getPiDetails().get(i).getFinCharge().get(j));
								}
								
								piResult = piResult + chgResult + ",";
								if (piResult.contains("0")) {
									itemInsert = false;
									break;
								}
							}
						} else {
							itemInsert = false;
							break;
						}
					}

					if (itemInsert == true) {
						log.info("PIDET and PICHGDET tables created");
						txManager.commit(ts);

						appResponse = new AppResponse.Builder()
								.data(getMapper().createObjectNode()
										.put(CodeUtils.RESPONSE_MSG,
												AppModuleMsg.PI_SUCCESS + " " + indentOrderNo.substring(2))
										.putPOJO(CodeUtils.RESPONSE, null))
								.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

						for (PurchaseIndent pdf : indentPdf) {

							for (int i = 0; i < pdf.getPiDetails().size(); i++) {

								for (int k = 0; k < pdf.getPiDetails().get(i).getImage().size(); k++) {
									if (k == 0) {
										pdf.getPiDetails().get(i)
												.setImage1(pdf.getPiDetails().get(i).getImage().get(k));
									}
									if (k == 1) {
										pdf.getPiDetails().get(i)
												.setImage2(pdf.getPiDetails().get(i).getImage().get(k));
									}
									if (k == 2) {
										pdf.getPiDetails().get(i)
												.setImage3(pdf.getPiDetails().get(i).getImage().get(k));
									}
									if (k == 3) {
										pdf.getPiDetails().get(i)
												.setImage4(pdf.getPiDetails().get(i).getImage().get(k));
									}
									if (k == 4) {
										pdf.getPiDetails().get(i)
												.setImage5(pdf.getPiDetails().get(i).getImage().get(k));
									}
								}
							}

							String pdfPath = piService.buildReport(pdf, request);

							log.info((!CodeUtils.isEmpty(pdfPath) ? "Purchase Indent PDF created"
									: "Purchase Indent PDF  not created"));
						}

					} else {
						txManager.rollback(ts);
						appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
								AppModuleErrors.PI_CREATOR_ERROR,
								appStatus.getMessage(AppModuleErrors.PI_CREATOR_ERROR));
					}
				} else {
					txManager.rollback(ts);
					log.info(appStatus.getMessage(AppModuleErrors.PI_CREATOR_ERROR));
					appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
							AppModuleErrors.PI_CREATOR_ERROR, appStatus.getMessage(AppModuleErrors.PI_CREATOR_ERROR));
				}

			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
						AppStatusMsg.PI_INPUTS);

			}
		} catch (MyBatisSystemException ex) {
			log.info(ex.getMessage());

			txManager.rollback(ts);
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CONNECTION_ERROR_CODE,
					AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, AppStatusMsg.CONNECTION_NOT_AVAILABLE);

		} catch (DataAccessException ex) {
			log.info(ex.getMessage());
			txManager.rollback(ts);
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.PI_CREATOR_ERROR,
					appStatus.getMessage(AppModuleErrors.PI_CREATOR_ERROR));
		} catch (NumberFormatException ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.NUMBER_FORMAT_MSG);

		} catch (JsonMappingException jsonException) {
			log.info(jsonException.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.INVALID_JSON);

		} catch (SupplyMintException ex) {
			log.info(ex.getMessage());
//			txManager.rollback(ts);
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.PI_PDF_CREATOR_ERROR, appStatus.getMessage(AppModuleErrors.PI_PDF_CREATOR_ERROR));

		} catch (Exception e) {
			log.info(e.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}

	}

	@SuppressWarnings({ "unchecked", "rawtypes", "deprecation", "finally" })
	@RequestMapping(value = "/get/lineitem/charges/{data}", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> getLineItemChargesByPurchaseTermCode(@RequestBody ObjectNode node) {

		log.info("PurchaseIndent getLineItemChargesByPurchaseTermCode() method starts here....");
		log.info("JSON data recieved in the node is : {}", node);

		AppResponse appResponse = null;
		ObjectNode nodeObj = getMapper().createObjectNode();
		ArrayNode arrNode = getMapper().createArrayNode();
		String gstComponent = null;
		boolean isGSTApplicable = false;

		try {
			if (!node.isNull()) {
				Integer purtermMainCode = Integer.parseInt(node.get("purtermMainCode").asText());
				Double transactionPrice = Double.parseDouble(node.get("rate").asText());

				String date = node.get("piDate").asText();
				Date parsedDate = CodeUtils.dateTimeMonthFormat.parse(date);

				String piDate = new SimpleDateFormat("dd-MMM-yyyy").format(parsedDate);

				String clientGstInNo = node.get("clientGstIn").asText();
				Integer supplierGstinStateCode = Integer.parseInt(node.get("supplierGstinStateCode").asText());
				Double basicValue = Double.parseDouble(node.get("basic").asText());

				// compare supplier and enterpriseGstin state code
				boolean result = piService.compareSupplierAndClientGstin(clientGstInNo, supplierGstinStateCode);

				log.info("after comparing supplier and enterprise gstin, the result obtained is : {}", result);

				if (result) {
					// CGST, SGST and CESS Rate will apply on the line item

					log.info("calculating cgst, sgst and cess....");
					Double subTotal = 0.0;
					double grandTotal = 0.0;

					List<PurtermCharge> purTermChargelList = piService
							.getPurchaseTermChargeDetailsByPurchaseTermCode(purtermMainCode);

					log.info("Charges applicable for the purchase term are : {},{} ", purTermChargelList.size(),
							purTermChargelList);

					for (PurtermCharge purtermCharge : purTermChargelList) {

						ObjectNode tempNode = getMapper().createObjectNode();

						log.info("purtermCharge contains the following details : {}", purtermCharge);

						int seq = purtermCharge.getPtSeq().intValue();
						String operationLevel = purtermCharge.getPtOperationLevel();
						String formula = purtermCharge.getPtFormulae();
						Double finChgRate = purtermCharge.getRate().doubleValue();
						String source = purtermCharge.getSource();
						gstComponent = purtermCharge.getGstComponent();
						String isTax = purtermCharge.getIsTax();
						String sign = purtermCharge.getSign();
						String finChgOperationLevel = purtermCharge.getOperationLevel();
						String calculationBasis = purtermCharge.getBasis();
						BigDecimal chgCode = purtermCharge.getChgCode();
						String chgName = purtermCharge.getChgName();

						// fincharges
						tempNode.put(PurchaseIndentMetaData.PurtermCharge.CHARGE_CODE, chgCode);
						tempNode.put(PurchaseIndentMetaData.PurtermCharge.CHARGE_NAME, chgName);
						tempNode.put(PurchaseIndentMetaData.PurtermCharge.CHARGE_SEQ, seq);
						tempNode.put(PurchaseIndentMetaData.PurtermCharge.FIN_FORMULA, formula);
						tempNode.put(PurchaseIndentMetaData.PurtermCharge.FIN_CHARGE_RATE, finChgRate);
						tempNode.put(PurchaseIndentMetaData.PurtermCharge.FIN_SOURCE, source);
						tempNode.put(PurchaseIndentMetaData.PurtermCharge.GST_COMPONENT, gstComponent);
						tempNode.put(PurchaseIndentMetaData.PurtermCharge.IS_TAX, isTax);
						tempNode.put(PurchaseIndentMetaData.PurtermCharge.SIGN, sign);
						tempNode.put(PurchaseIndentMetaData.PurtermCharge.FIN_CHARGE_OPERATION_LEVEL,
								finChgOperationLevel);
						tempNode.put(PurchaseIndentMetaData.PurtermCharge.CALCULATION_BASIS, calculationBasis);

						if (gstComponent == null) {

							// When gst component is null and operation level is line level
							if (finChgOperationLevel.equals("L")) {

								// When gst component is null and operation level is line level and calculation
								// basis is Amount
								if (calculationBasis.equals("A")) {

									if (sign.equals("+")) {
										subTotal = subTotal + finChgRate;
										log.info("subtotal : {}", subTotal);
									} else {
										subTotal = subTotal - finChgRate;
										log.info("subtotal : {}", subTotal);
									}

								} else {
									// When gst component is null and operation level is line level and calculation
									// basis is Percentage

									if (sign.equals("+")) {
										double rateAmount = ((basicValue * finChgRate) / 100);
										subTotal = subTotal + rateAmount;
									} else {
										double rateAmount = ((basicValue * finChgRate) / 100);
										subTotal = subTotal - rateAmount;
									}

								}
							} else {
								// When gst component is null and operation level is Header level
							}
						}

						else if (!CodeUtils.isEmpty(gstComponent)) {
							isGSTApplicable = true;
							log.info("CGST and SGST calculation starts here.....");

							if (source.equals("G") && (gstComponent.equals("CGST") || gstComponent.equals("SGST")
									|| gstComponent.equals("CESS")) && isTax.equals("Y")) {

								if (operationLevel.equals("L")) {

									// calculating rate for charges starts here
									String hsnSacCode = node.get("hsnSacCode").asText();

									// In query, we have added to_char() with hsnsaccode becoz HSNSACCODE is in
									// number format in DB
									HsnGstData maxEffectiveDate = piService.getMaxEffectiveDateByHsnSacCode(hsnSacCode,
											piDate);

									Timestamp effectiveDate = maxEffectiveDate.getEffectiveDate();
									String slab = maxEffectiveDate.getSlab();

									if (slab.equals("Without Slab")) {
										String taxName = maxEffectiveDate.getTaxName();
										String gstSlab = maxEffectiveDate.getSlab();
										String slabEffectiveDate = maxEffectiveDate.getEffectiveDate().toString();

										Double cgstRate = 0.0, sgstRate = 0.0, cessRate = 0.0;

										if (gstComponent.equalsIgnoreCase("cgst")) {
											cgstRate = maxEffectiveDate.getCgstRate().doubleValue();
										} else if (gstComponent.equalsIgnoreCase("sgst")) {
											sgstRate = maxEffectiveDate.getSgstRate().doubleValue();
										} else {
											cessRate = maxEffectiveDate.getCessRate().doubleValue();
										}

										// rates
										tempNode.put("rates", piService.getCgstChargeRateNode(taxName, gstSlab,
												slabEffectiveDate, cgstRate, sgstRate, cessRate));

										nodeObj.put(PurchaseIndentMetaData.PILineItem.GST,
												piService.getGstRateByTaxName(taxName));

										Double ch1 = 0.0, ch2 = 0.0, ch3 = 0.0;

										PurtermDet purTermDet = piService
												.getPurchaseTermDetailsByChgCode(purtermMainCode, chgCode);

										// for charges
										ObjectNode chargeNode = getMapper().createObjectNode();

										String formulae = purTermDet.getFormulae();
										String lineItermSign = purTermDet.getSign();

										chargeNode.put("seq", purTermDet.getSeq());
										chargeNode.put("chgcode", purTermDet.getChgCode());
										chargeNode.put("rate", purTermDet.getRate());
										chargeNode.put("formula", formulae);
										chargeNode.put("sign", lineItermSign);
										chargeNode.put("operationLevel", purTermDet.getOperationLevel());

										Map<String, Double> subTotalMap = piService.calulcateTotalForCgstSgstRates(
												subTotal, basicValue, cgstRate, sgstRate, cessRate, ch1, ch2, ch3,
												lineItermSign, formulae);

										subTotal = subTotalMap.get("subTotal");
										chargeNode.put("chargeAmount", subTotalMap.get("chargeAmount"));
										chargeNode.put("subTotal", subTotal);

										tempNode.put("charges", chargeNode);

									} else {

										String piEffectiveDate = new SimpleDateFormat("dd-MMM-yyyy")
												.format(effectiveDate);
										log.info("Maximum effective date obtained is = " + piEffectiveDate);

										// get HSNGSTDATA using hsn sac code
										// List<HsnGstData> hsnGstDataList =
										// piService.getHsnGstDataByHsnSacCode(hsnSacCode,piEffectiveDate);

										HsnGstData maxAmountFromHsnGstData = piService.getMaxAmountFrom(hsnSacCode,
												piEffectiveDate);
										Double maxAmountFrom = maxAmountFromHsnGstData.getAmountFrom().doubleValue();

										HsnGstData minAmountFromHsnGstData = piService.getMinAmountFrom(hsnSacCode,
												piEffectiveDate);
										Double minAmountFrom = minAmountFromHsnGstData.getAmountFrom().doubleValue();

										log.info(
												"Minimum and Maximum AMOUNTFROM for GST Slab obtained are as follows : {}, {}",
												minAmountFrom, maxAmountFrom);

										log.info("Transaction Price for item is : {}", transactionPrice);
										if (transactionPrice > minAmountFrom && transactionPrice <= maxAmountFrom) {
											HsnGstData applicableGstSlab = piService
													.getApplicableGstSlabForTransactionPrice(hsnSacCode,
															piEffectiveDate, minAmountFrom);

											log.info("Applicable GST Slab is : {}", applicableGstSlab);
											String taxName = applicableGstSlab.getTaxName();
											String gstSlab = applicableGstSlab.getSlab();
											String slabEffectiveDate = applicableGstSlab.getEffectiveDate().toString();
											Double cgstRate = 0.0, sgstRate = 0.0, cessRate = 0.0;

											if (gstComponent.equalsIgnoreCase("cgst")) {
												cgstRate = applicableGstSlab.getCgstRate().doubleValue();
											} else if (gstComponent.equalsIgnoreCase("sgst")) {
												sgstRate = applicableGstSlab.getSgstRate().doubleValue();
											} else {
												cessRate = applicableGstSlab.getCessRate().doubleValue();
											}

											// for rates
											tempNode.put("rates", piService.getCgstChargeRateNode(taxName, gstSlab,
													slabEffectiveDate, cgstRate, sgstRate, cessRate));

											nodeObj.put(PurchaseIndentMetaData.PILineItem.GST,
													piService.getGstRateByTaxName(taxName));

											Double ch1 = 0.0, ch2 = 0.0, ch3 = 0.0;

											PurtermDet purTermDet = piService
													.getPurchaseTermDetailsByChgCode(purtermMainCode, chgCode);

											// for charges
											ObjectNode chargeNode = getMapper().createObjectNode();

											BigDecimal chargeCode = purTermDet.getChgCode();
											String formulae = purTermDet.getFormulae();
											String lineItermSign = purTermDet.getSign();
											BigDecimal rate = purTermDet.getRate();
											BigDecimal sequence = purTermDet.getSeq();
											String chargeOperationLevel = purTermDet.getOperationLevel();

											chargeNode.put("seq", sequence);
											chargeNode.put("chgcode", chargeCode);
											chargeNode.put("rate", rate);
											chargeNode.put("formula", formulae);
											chargeNode.put("sign", lineItermSign);
											chargeNode.put("operationLevel", chargeOperationLevel);

											Map<String, Double> subTotalMap = piService.calulcateTotalForCgstSgstRates(
													subTotal, basicValue, cgstRate, sgstRate, cessRate, ch1, ch2, ch3,
													lineItermSign, formulae);

											subTotal = subTotalMap.get("subTotal");
											chargeNode.put("chargeAmount", subTotalMap.get("chargeAmount"));
											chargeNode.put("subTotal", subTotal);

											tempNode.put("charges", chargeNode);
											// //calculate charges and basic value using formula ends here

										} else {

											log.info(
													"calculate line item charges for cgst,sgst,cess for maximum amount slab starts here....");

											if (source.equals("G") && (gstComponent.equals("CGST")
													|| gstComponent.equals("SGST") || gstComponent.equals("CESS"))
													&& isTax.equals("Y")) {

												HsnGstData applicableGstSlab = piService
														.getApplicableGstSlabForTransactionPrice(hsnSacCode, piDate,
																maxAmountFrom);

												log.info("obtained maximum gst slab data is as follows : {}",
														applicableGstSlab);

												String taxName = applicableGstSlab.getTaxName();
												String gstSlab = applicableGstSlab.getSlab();
												String slabEffectiveDate = applicableGstSlab.getEffectiveDate()
														.toString();
												Double cgstRate = 0.0, sgstRate = 0.0, cessRate = 0.0;

												if (gstComponent.equalsIgnoreCase("cgst")) {
													cgstRate = maxEffectiveDate.getCgstRate().doubleValue();
												} else if (gstComponent.equalsIgnoreCase("sgst")) {
													sgstRate = maxEffectiveDate.getSgstRate().doubleValue();
												} else {
													cessRate = maxEffectiveDate.getCessRate().doubleValue();
												}

												// for rates
												tempNode.put("rates", piService.getCgstChargeRateNode(taxName, gstSlab,
														slabEffectiveDate, cgstRate, sgstRate, cessRate));

												nodeObj.put(PurchaseIndentMetaData.PILineItem.GST,
														piService.getGstRateByTaxName(taxName));

												Double ch1 = 0.0, ch2 = 0.0, ch3 = 0.0;

												PurtermDet purTermDet = piService
														.getPurchaseTermDetailsByChgCode(purtermMainCode, chgCode);
												log.info("Charges obtained by list of charges is : {} ", purTermDet);

												// for charges
												ObjectNode chargeNode = getMapper().createObjectNode();

												BigDecimal chargeCode = purTermDet.getChgCode();
												String formulae = purTermDet.getFormulae();
												String lineItermSign = purTermDet.getSign();
												BigDecimal rate = purTermDet.getRate();
												BigDecimal sequence = purTermDet.getSeq();
												String chargeOperationLevel = purTermDet.getOperationLevel();

												chargeNode.put("seq", sequence);
												chargeNode.put("chgcode", chargeCode);
												chargeNode.put("rate", rate);
												chargeNode.put("formula", formulae);
												chargeNode.put("sign", lineItermSign);
												chargeNode.put("operationLevel", chargeOperationLevel);

												Map<String, Double> subTotalMap = piService
														.calulcateTotalForCgstSgstRates(subTotal, basicValue, cgstRate,
																sgstRate, cessRate, ch1, ch2, ch3, lineItermSign,
																formulae);

												subTotal = subTotalMap.get("subTotal");
												chargeNode.put("chargeAmount", subTotalMap.get("chargeAmount"));
												chargeNode.put("subTotal", subTotal);

												tempNode.put("charges", chargeNode);

												log.info(
														"calculate line item charges for cgst,sgst,cess slab ends here....");
												log.info("subtotal is : {}", subTotal);

												// //calculate charges and basic value using formula ends here
											}

										}
									}

								}
							}
						}

						tempNode.put(PurchaseIndentMetaData.PurtermCharge.SUBTOTAL, subTotal);
						arrNode.add(tempNode);

					}

					grandTotal = basicValue + subTotal;
					log.info("final subtotal for CGST,SGST and CESS obtained is = " + subTotal);

					// commented if condition by mohit on 12 Jul
//					if (subTotal == 0.0)
					if (isGSTApplicable == false)
						nodeObj.put(PurchaseIndentMetaData.PILineItem.GST, 0);

					nodeObj.put(PurchaseIndentMetaData.PILineItem.BASIC_VALUE, basicValue);
					nodeObj.put(PurchaseIndentMetaData.PILineItem.TAX, subTotal);
					nodeObj.put(PurchaseIndentMetaData.PILineItem.NET_AMOUNT, grandTotal);
					nodeObj.put(PurchaseIndentMetaData.PILineItem.FIN_CHARGES, arrNode);

					log.info("Final Object Node is : {}", nodeObj);

					appResponse = new AppResponse.Builder()
							.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, nodeObj)
									.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
							.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

					log.info(AppStatusMsg.SUCCESS_MSG);
				} else {
					// IGST and CESS Rate will be apply on the line item

					log.info("calculating igst and cess....");

					Double subTotal = 0.0;
					double grandTotal = 0.0;

					List<PurtermCharge> purTermChargelList = piService
							.getPurchaseTermChargeDetailsByPurchaseTermCode(purtermMainCode);

					log.info("Charges applicable when gst is IGST for the purchase term are : {},{} ",
							purTermChargelList.size(), purTermChargelList);

					for (PurtermCharge purtermCharge : purTermChargelList) {

						log.info("purtermCharge contains the following details : {}", purtermCharge);
						ObjectNode tempNode = getMapper().createObjectNode();

						int seq = purtermCharge.getPtSeq().intValue();
						Double finChgRate = purtermCharge.getRate().doubleValue();
						String source = purtermCharge.getSource();
						gstComponent = purtermCharge.getGstComponent();
						String isTax = purtermCharge.getIsTax();
						String sign = purtermCharge.getSign();
						String formula = purtermCharge.getPtFormulae();
						String finChgOperationLevel = purtermCharge.getOperationLevel();
						String calculationBasis = purtermCharge.getBasis();
						BigDecimal chgCode = purtermCharge.getChgCode();
						String chgName = purtermCharge.getChgName();

						tempNode.put(PurchaseIndentMetaData.PurtermCharge.CHARGE_CODE, chgCode);
						tempNode.put(PurchaseIndentMetaData.PurtermCharge.CHARGE_NAME, chgName);
						tempNode.put(PurchaseIndentMetaData.PurtermCharge.CHARGE_SEQ, seq);
						tempNode.put(PurchaseIndentMetaData.PurtermCharge.FIN_FORMULA, formula);
						tempNode.put(PurchaseIndentMetaData.PurtermCharge.FIN_CHARGE_RATE, finChgRate);
						tempNode.put(PurchaseIndentMetaData.PurtermCharge.FIN_SOURCE, source);
						tempNode.put(PurchaseIndentMetaData.PurtermCharge.GST_COMPONENT, gstComponent);
						tempNode.put(PurchaseIndentMetaData.PurtermCharge.IS_TAX, isTax);
						tempNode.put(PurchaseIndentMetaData.PurtermCharge.SIGN, sign);
						tempNode.put(PurchaseIndentMetaData.PurtermCharge.FIN_CHARGE_OPERATION_LEVEL,
								finChgOperationLevel);
						tempNode.put(PurchaseIndentMetaData.PurtermCharge.CALCULATION_BASIS, calculationBasis);

//						arrNode.add(tempNode);

						if (gstComponent == null) {

							if (finChgOperationLevel.equals("L")) {

								if (calculationBasis.equals("A")) {

									if (sign.equals("+")) {
										// subTotal = ((subTotal/articleQty) + finChgRate) * articleQty;

										subTotal = subTotal + finChgRate;
										log.info("subtotal : {}", subTotal);
									} else {
										subTotal = subTotal - finChgRate;
										log.info("subtotal : {}", subTotal);
									}

								} else {

									if (sign.equals("+")) {
										double rateAmount = ((basicValue * finChgRate) / 100);
										subTotal = subTotal + rateAmount;
									} else {
										double rateAmount = ((basicValue * finChgRate) / 100);
										subTotal = subTotal - rateAmount;
									}

								}
								tempNode.put("subTotal", subTotal);
								log.info("After applying fincharge rates subtotal is : {} ", subTotal);
							} else {
								log.info("When gst component is null and operation level is header level");
							}
						}

						else if (!CodeUtils.isEmpty(gstComponent)) {
							isGSTApplicable = true;

							if (finChgOperationLevel.equals("L")) {

								if (source.equals("G") && (gstComponent.equals("IGST") || gstComponent.equals("CESS"))
										&& isTax.equals("Y")) {

									// calculating rate for charges starts here
									String hsnSacCode = node.get("hsnSacCode").asText();

									HsnGstData maxEffectiveDate = piService.getMaxEffectiveDateByHsnSacCode(hsnSacCode,
											piDate);

									Timestamp effectiveDate = maxEffectiveDate.getEffectiveDate();
									String slab = maxEffectiveDate.getSlab();

									String piEffectiveDate = new SimpleDateFormat("dd-MMM-yyyy").format(effectiveDate);
									log.info("Maximum effective date obtained is = " + piEffectiveDate);

									if (slab.equals("Without Slab")) {

										String taxName = maxEffectiveDate.getTaxName();
										String gstSlab = maxEffectiveDate.getSlab();
										String slabEffectiveDate = maxEffectiveDate.getEffectiveDate().toString();

										Double igstRate = 0.0, cessRate = 0.0;

										if (gstComponent.equalsIgnoreCase("igst")) {
											igstRate = maxEffectiveDate.getIgstRate().doubleValue();
										} else {
											cessRate = maxEffectiveDate.getCessRate().doubleValue();
										}

										tempNode.put("rates", piService.getIgstChargeRateNode(taxName, gstSlab,
												slabEffectiveDate, igstRate, cessRate));

										nodeObj.put(PurchaseIndentMetaData.PILineItem.GST,
												piService.getGstRateByTaxName(taxName));

										Double ch1 = 0.0, ch2 = 0.0;

										PurtermDet purTermDet = piService
												.getPurchaseTermDetailsByChgCode(purtermMainCode, chgCode);

										ObjectNode chargeNode = getMapper().createObjectNode();

										BigDecimal chargeCode = purTermDet.getChgCode();
										String formulae = purTermDet.getFormulae();
										String lineItermSign = purTermDet.getSign();
										BigDecimal rate = purTermDet.getRate();
										BigDecimal sequence = purTermDet.getSeq();
										String chargeOperationLevel = purTermDet.getOperationLevel();

										chargeNode.put("seq", sequence);
										chargeNode.put("chgcode", chargeCode);
										chargeNode.put("rate", rate);
										chargeNode.put("formula", formulae);
										chargeNode.put("sign", lineItermSign);
										chargeNode.put("operationLevel", chargeOperationLevel);

										Map<String, Double> subTotalMap = piService.calulcateTotalForIgstRates(subTotal,
												basicValue, igstRate, cessRate, ch1, ch2, lineItermSign, formulae);

										subTotal = subTotalMap.get("subTotal");

										chargeNode.put("chargeAmount", subTotalMap.get("chargeAmount"));
										chargeNode.put("subTotal", subTotal);

										tempNode.put("charges", chargeNode);

									} else {
										// get HSNGSTDATA using hsn sac code
										List<HsnGstData> hsnGstDataList = piService
												.getHsnGstDataByHsnSacCode(hsnSacCode, piEffectiveDate);

										log.info("Calculating mininum and maxinum AMOUNTFROM for GST SLAB");

										HsnGstData maxAmountFromHsnGstData = piService.getMaxAmountFrom(hsnSacCode,
												piEffectiveDate);
										Double maxAmountFrom = maxAmountFromHsnGstData.getAmountFrom().doubleValue();

										HsnGstData minAmountFromHsnGstData = piService.getMinAmountFrom(hsnSacCode,
												piEffectiveDate);
										Double minAmountFrom = minAmountFromHsnGstData.getAmountFrom().doubleValue();

										log.info("Minimum and Maximum AMOUNTFROM obtained are as follows : {}, {} ",
												minAmountFrom, maxAmountFrom);

										log.info("Transaction Price for item is : {}", transactionPrice);

										if (transactionPrice > minAmountFrom && transactionPrice <= maxAmountFrom) {
											HsnGstData applicableGstSlab = piService
													.getApplicableGstSlabForTransactionPrice(hsnSacCode,
															piEffectiveDate, minAmountFrom);

											log.info("Gst Slab obtained for the minimum amount from is : {} ",
													applicableGstSlab);

											String taxName = applicableGstSlab.getTaxName();
											String gstSlab = applicableGstSlab.getSlab();
											String slabEffectiveDate = applicableGstSlab.getEffectiveDate().toString();
											Double igstRate = 0.0, cessRate = 0.0;

											if (gstComponent.equalsIgnoreCase("igst")) {
												igstRate = applicableGstSlab.getIgstRate().doubleValue();
											} else {
												cessRate = applicableGstSlab.getCessRate().doubleValue();
											}

											tempNode.put("rates",
													tempNode.put("rates", piService.getIgstChargeRateNode(taxName,
															gstSlab, slabEffectiveDate, igstRate, cessRate)));
											nodeObj.put(PurchaseIndentMetaData.PILineItem.GST,
													piService.getGstRateByTaxName(taxName));

											Double ch1 = 0.0, ch2 = 0.0;

											PurtermDet purTermDet = piService
													.getPurchaseTermDetailsByChgCode(purtermMainCode, chgCode);

											log.info("PurchaseTerm charges list is as follows : {}", purTermDet);

											ObjectNode chargeNode = getMapper().createObjectNode();

											BigDecimal chargeCode = purTermDet.getChgCode();
											String formulae = purTermDet.getFormulae();
											String lineItermSign = purTermDet.getSign();
											BigDecimal rate = purTermDet.getRate();
											BigDecimal sequence = purTermDet.getSeq();
											String chargeOperationLevel = purTermDet.getOperationLevel();

											chargeNode.put("seq", sequence);
											chargeNode.put("chgcode", chargeCode);
											chargeNode.put("rate", rate);
											chargeNode.put("formula", formulae);
											chargeNode.put("sign", lineItermSign);
											chargeNode.put("operationLevel", chargeOperationLevel);

											Map<String, Double> subTotalMap = piService.calulcateTotalForIgstRates(
													subTotal, basicValue, igstRate, cessRate, ch1, ch2, lineItermSign,
													formulae);

											subTotal = subTotalMap.get("subTotal");

											chargeNode.put("chargeAmount", subTotalMap.get("chargeAmount"));
											chargeNode.put("subTotal", subTotal);

											tempNode.put("charges", chargeNode);
											// End of switch case

											// //calculate charges and basic value using formula ends here

											// End of foreach loop
											log.info("subtotal is = {}", subTotal);

										} else {
											HsnGstData applicableGstSlab = piService
													.getApplicableGstSlabForTransactionPrice(hsnSacCode,
															piEffectiveDate, maxAmountFrom);

											log.info("Gst Slab obtained for the maximum amount from is : {} ",
													applicableGstSlab);

											String taxName = applicableGstSlab.getTaxName();
											String gstSlab = applicableGstSlab.getSlab();
											String slabEffectiveDate = applicableGstSlab.getEffectiveDate().toString();
											Double igstRate = 0.0, cessRate = 0.0;

											if (gstComponent.equalsIgnoreCase("igst")) {
												igstRate = maxEffectiveDate.getIgstRate().doubleValue();
											} else {
												cessRate = maxEffectiveDate.getCessRate().doubleValue();
											}

											tempNode.put("rates",
													tempNode.put("rates", piService.getIgstChargeRateNode(taxName,
															gstSlab, slabEffectiveDate, igstRate, cessRate)));

											nodeObj.put(PurchaseIndentMetaData.PILineItem.GST,
													piService.getGstRateByTaxName(taxName));

											Double ch1 = 0.0, ch2 = 0.0;

											PurtermDet purTermDet = piService
													.getPurchaseTermDetailsByChgCode(purtermMainCode, chgCode);

											ObjectNode chargeNode = getMapper().createObjectNode();

											BigDecimal chargeCode = purTermDet.getChgCode();
											String formulae = purTermDet.getFormulae();
											String lineItermSign = purTermDet.getSign();
											BigDecimal rate = purTermDet.getRate();
											BigDecimal sequence = purTermDet.getSeq();
											String chargeOperationLevel = purTermDet.getOperationLevel();

											chargeNode.put("seq", sequence);
											chargeNode.put("chgcode", chargeCode);
											chargeNode.put("rate", rate);
											chargeNode.put("formula", formulae);
											chargeNode.put("sign", lineItermSign);
											chargeNode.put("operationLevel", chargeOperationLevel);

											Map<String, Double> subTotalMap = piService.calulcateTotalForIgstRates(
													subTotal, basicValue, igstRate, cessRate, ch1, ch2, lineItermSign,
													formulae);

											subTotal = subTotalMap.get("subTotal");

											chargeNode.put("chargeAmount", subTotalMap.get("chargeAmount"));
											chargeNode.put("subTotal", subTotal);

											tempNode.put("charges", chargeNode);
											// End of switch case

											// End of purchaseTerm foreach loop

										}
									}
								}
							}
						}

						tempNode.put(PurchaseIndentMetaData.PurtermCharge.SUBTOTAL, subTotal);
						arrNode.add(tempNode);
					}

					grandTotal = basicValue + subTotal;
					log.info("final subtotal obtained is = " + subTotal);

					// commented if condition by mohit on 12 Jul
//					if (subTotal == 0.0)
					if (isGSTApplicable == false)
						nodeObj.put(PurchaseIndentMetaData.PILineItem.GST, 0);

					nodeObj.put(PurchaseIndentMetaData.PILineItem.BASIC_VALUE, basicValue);
					nodeObj.put(PurchaseIndentMetaData.PILineItem.TAX, subTotal);
					nodeObj.put(PurchaseIndentMetaData.PILineItem.NET_AMOUNT, grandTotal);
					nodeObj.put(PurchaseIndentMetaData.PILineItem.FIN_CHARGES, arrNode);

					log.info("Final Object Node is : {}", nodeObj);

					appResponse = new AppResponse.Builder()
							.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, nodeObj)
									.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
							.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

					log.info(AppStatusMsg.SUCCESS_MSG);

				}

			}
		} catch (MyBatisSystemException ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CONNECTION_ERROR_CODE,
					AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, AppStatusMsg.CONNECTION_NOT_AVAILABLE);

		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.PI_LINE_ITEM_ERROR, appStatus.getMessage(AppModuleErrors.PI_LINE_ITEM_ERROR));

			log.info(ex.getMessage());

		} catch (NullPointerException e) {

			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.PI_LINE_ITEM_ERROR, appStatus.getMessage(AppModuleErrors.PI_LINE_ITEM_ERROR));

			log.info(e.getMessage());
		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			log.info(e.getMessage());
		} finally {
			log.info("PurchaseIndent getLineItemChargesByPurchaseTermCode() method ends here....");
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes", "deprecation", "finally" })
	@RequestMapping(value = "/calculate/actual/markup/article/{data}", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> calculateInTakeMarginForArticleData(@RequestBody ObjectNode node) {

		log.info("PurchaseIndent calculateNetAmountForArticleData() method starts here....");
		log.info("JSON data recieved in the node is : {}", node);

		AppResponse appResponse = null;
		ObjectNode tempNode = getMapper().createObjectNode();

		try {

			Map<String, Double> map = piService.calculateInTakeMarginForArticleData(node);

			if (!CodeUtils.isEmpty(map)) {
				ObjectNode objNode = getMapper().createObjectNode();

				objNode.put(PurchaseIndentMetaData.Item.ACTUALMARKUP,
						map.get(PurchaseIndentMetaData.Item.ACTUALMARKUP));
				objNode.put(PurchaseIndentMetaData.Item.CALCULATEDMARGIN,
						map.get(PurchaseIndentMetaData.Item.CALCULATEDMARGIN));

				tempNode.put(CodeUtils.RESPONSE, objNode);

				log.info("Final Object Node is : {}", tempNode);

				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
						.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG).putAll(tempNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

				log.info(AppStatusMsg.SUCCESS_MSG);
			}
		} catch (NullPointerException e) {
			appResponse = new AppResponse.Builder().data(
					getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null).put(CodeUtils.RESPONSE_MSG, ""))
					.status(AppStatusMsg.BLANK).build();
			log.info(e.getMessage());
		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.PI_ACTUAL_MARKUP_FAILURE);
			log.info(e.getMessage());
		} finally {

			log.info("PurchaseIndent calculateNetAmountForArticleData() method ends here....");
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes", "finally", "deprecation" })
	@RequestMapping(method = RequestMethod.GET, value = "/get/purtermdata/{slCode}", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	public ResponseEntity<AppResponse> getPurchaseTermDataBySlCode(@PathVariable Integer slCode) {

		log.info("PurchaseIndent getPurchaseTermData() method starts here....");
		AppResponse appResponse = null;
		ObjectNode node = getMapper().createObjectNode();

		try {

			PurchaseTermData purchaseTermData = piService.getPurchaseTermDataBySlCode(slCode);

			if (!CodeUtils.isEmpty(purchaseTermData)) {

				ObjectNode tempNode = getMapper().createObjectNode();

				tempNode.put(PurchaseIndentMetaData.PurchaseTerm.CODE, purchaseTermData.getCode());
				tempNode.put(PurchaseIndentMetaData.PurchaseTerm.NAME, purchaseTermData.getName());

				node.put(CodeUtils.RESPONSE, tempNode);
				log.info("Final Object node data is {} ", node);

				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
						.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG).putAll(node))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				log.info(AppStatusMsg.SUCCESS_MSG);

			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

				log.info(AppStatusMsg.BLANK);
			}
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.SUPPLIER_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.PURCHASE_TERM_READER__ERROR));

			log.info(ex.getMessage());

		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			log.info(e.getMessage());
		} finally {
			log.info("PurchaseIndentEndPoint getPurchaseTermData() method ends here...");
			return ResponseEntity.ok(appResponse);
		}

	}

	@SuppressWarnings({ "unchecked", "rawtypes", "finally", "deprecation" })
	@RequestMapping(method = RequestMethod.GET, value = "/get/all/purchaseterms", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	public ResponseEntity<AppResponse> getAllPurchaseTerms() {

		log.info("PurchaseIndent getAllPurchaseTerms() method starts here....");
		AppResponse appResponse = null;
		ObjectNode node = getMapper().createObjectNode();
		ArrayNode arrNode = getMapper().createArrayNode();

		try {

			List<PurtermMaster> purchaseTermList = piService.getAllPurchaseTerms();

			AtomicInteger count = new AtomicInteger(1);

			if (!CodeUtils.isEmpty(purchaseTermList)) {

				purchaseTermList.forEach(purchaseTerm -> {
					ObjectNode tempNode = getMapper().createObjectNode();

					tempNode.put(PurchaseIndentMetaData.Transporter.ID, count.get());
					tempNode.put(PurchaseIndentMetaData.PurchaseTerm.CODE, purchaseTerm.getCode());
					tempNode.put(PurchaseIndentMetaData.PurchaseTerm.NAME, purchaseTerm.getName());

					count.getAndIncrement();
					arrNode.add(tempNode);

				});

				node.put(CodeUtils.RESPONSE, arrNode);
				log.info("Final Object node data is {} ", node);

				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
						.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG).putAll(node))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				log.info(AppStatusMsg.SUCCESS_MSG);

			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

				log.info(AppStatusMsg.BLANK);
			}
		} catch (NullPointerException e) {
			appResponse = new AppResponse.Builder().data(
					getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null).put(CodeUtils.RESPONSE_MSG, ""))
					.status(AppStatusMsg.BLANK).build();
			log.info(e.getMessage());
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.SUPPLIER_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.PURCHASE_TERM_READER__ERROR));

			log.info(ex.getMessage());

		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			log.info(e.getMessage());
		} finally {
			log.info("PurchaseIndentEndPoint getAllPurchaseTerms() method ends here...");
			return ResponseEntity.ok(appResponse);
		}

	}

	@SuppressWarnings({ "finally" })
	@RequestMapping(value = "/calculate/otb/articlecode/{article}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> calculateOtbArticalcode(@PathVariable("article") String article,
			@RequestParam("mrp") String mrp, @RequestParam("month") String month, @RequestParam("year") String year,
			@RequestParam("orgId") String orgId) {
		AppResponse appResponse = null;
		// String otb="50000.0";
		ObjectNode objectNode = getMapper().createObjectNode();
		String OtbValue = null;
		double value = 0;
		try {
			String tenantHashKey = ThreadLocalStorage.getTenantHashKey();
			if (!CodeUtils.isEmpty(article) && !CodeUtils.isEmpty(mrp) && !CodeUtils.isEmpty(month)
					&& !CodeUtils.isEmpty(year)) {

//				OtbValue = piService.getActualOtbData(article, mrp, month, year);

				ObjectNode otbRequest = getMapper().createObjectNode();

				String monthYear = month + "-" + year;
				otbRequest.put("articleCode", article);
				otbRequest.put("desc6", mrp);
				otbRequest.put("monthYear", monthYear);

				PurchaseOrderConfiguration poc = poService.getPOConfiguration(2);

				JsonNode headerJson = getMapper().readTree(poc.getHeaders());
				String headerType = getMapper().treeToValue(headerJson.get("headerType"), String.class);

				ResponseEntity<AppResponse> responseEntityData = null;

				responseEntityData = poService.sendPODetailsOnRestAPI(headerType, otbRequest, poc);

				if (!CodeUtils.isEmpty(responseEntityData)) {
					String responseEntityStatus = responseEntityData.getBody().getStatus();
					log.info("response entity message :"
							+ responseEntityData.getBody().getData().get(CodeUtils.RESPONSE_MSG));
					log.info("error code response entity message :"
							+ responseEntityData.getBody().getError().get("errorCode"));
					log.info("error message response entity message :"
							+ responseEntityData.getBody().getError().get("errorMessage"));
					log.info("response calling :" + responseEntityStatus);
					if (responseEntityStatus.contains(CodeUtils.RESPONSE_SUCCESS_CODE)) {
						log.info("response true");
						OtbValue = responseEntityData.getBody().getData().get("otb").toString();
//				poa.setHttpMessage(responseEntityData.getBody().getData().get(CodeUtils.RESPONSE_MSG).toString());
					}
				} else {
//					throw new SupplyMintException("");
					// @Author Prabhakar -- Start Comment Due to Gynesis Rest Template Specific
					if (tenantHashKey.equals("f71b609842ca103b916ceec05d9cc8ff8a354b77da12e425f49166dc1cf41c5e"))
						throw new SupplyMintException("Unable to push open purchase order in sm partner");
					else {
						String udfExistenceStatus = piService.getUDFExistenceStatus(orgId);
						Map<String, Object> proMap = JsonUtils.toMap(JsonUtils.toJsonObject(udfExistenceStatus));
						String isDefaultOtb = proMap.get("DEFAULT_OTB_VALUE").toString();
						if (isDefaultOtb.equalsIgnoreCase(Conditions.TRUE.toString())) {
							OtbValue = DefaultParameter.OTB_VALUE;
						} else {
							OtbValue = null;
						}
					}
					// end
				}

				if (CodeUtils.isEmpty(OtbValue) || OtbValue == "null") {
					OtbValue = "0";
					value = Double.parseDouble(OtbValue);

				} else {
					if (OtbValue.contains("\""))
						value = Double.parseDouble(OtbValue.substring(1, OtbValue.length() - 1));
					else
						value = Double.parseDouble(OtbValue);

				}
				objectNode.put("otb", value);
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, objectNode)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
						AppStatusMsg.PI_OTB_FAILURE);
			}
		} catch (NumberFormatException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.PI_OTB_READER_ERROR, appStatus.getMessage(AppModuleErrors.PI_OTB_READER_ERROR));
			log.info(ex.getMessage());
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.PI_OTB_READER_ERROR, appStatus.getMessage(AppModuleErrors.PI_OTB_READER_ERROR));
			log.info(ex.getMessage());
		} catch (SupplyMintException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.PI_SMPARTNER_OTB_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.PI_SMPARTNER_OTB_READER_ERROR));
			log.info(appStatus.getMessage(AppModuleErrors.PI_SMPARTNER_OTB_READER_ERROR));
		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			log.info(e.getMessage());
		} finally {
			return ResponseEntity.ok(appResponse);
		}

	}

	@RequestMapping(value = "/file/upload", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> uploadFile(@RequestBody MultiFileUpload fileUpload, HttpServletRequest request) {

		AppResponse appResponse = null;
		File destinationFile;
		File createClientFolder;
		BufferedOutputStream stream;
		String realFileName = null;
		ArrayNode arrNode = getMapper().createArrayNode();
		ArrayNode fileNameArrayNode = getMapper().createArrayNode();
		boolean isFileUpload = false;

		try {

			String token = request.getHeader("X-Auth-Token");
			JsonArray orgDetails = CodeUtils.decode(token).get("ORG_DETAILS").getAsJsonArray();

			JsonObject jNode = orgDetails.get(0).getAsJsonObject();
			String orgId = jNode.get("orgId").getAsString();

			for (int i = 0; i < fileUpload.getFile().size(); i++) {

				String file = fileUpload.getFile().get(i);
				String fileName = null;
				String filePath = null;
				String extension = CodeUtils.getExtensionByApacheCommonLib(fileUpload.getFileName().get(i));

				S3BucketInfo s3BucketInfo = s3BucketInfoService.getBucketByType(UPLOAD_Image_Article, orgId);
				if (!file.isEmpty() && !CodeUtils.isEmpty(s3BucketInfo)) {
					String randomFolder = UUID.randomUUID().toString();

					String s3BucketFilePath = s3BucketInfo.getBucketPath() + delimeter + randomFolder;
					fileName = s3BucketInfo.getEnterprise() + separator
							+ CodeUtils.__________dateTimeFormat.format(new Date());
					String savedPath = s3BucketFilePath + delimeter + fileName + "." + extension;

					if (!file.isEmpty()) {

						String base64Image = file.split(",")[1];

						byte[] imageBytes = javax.xml.bind.DatatypeConverter.parseBase64Binary(base64Image);
						String path = FileUtils.getPlatformBasedParentDir().toString() + "/"
								+ s3BucketInfo.getEnterprise();

						String url = path + "/" + savedPath;

						destinationFile = new File(url);
						createClientFolder = new File(destinationFile.getParent());

						if (!createClientFolder.exists())
							createClientFolder.mkdirs();

						stream = new BufferedOutputStream(new FileOutputStream(destinationFile));
						stream.write(imageBytes);
						stream.close();

						File saveBucket = new File(url);
						String etag = s3Wrapper.upload(saveBucket, savedPath, s3BucketInfo.getS3bucketName()).getETag();

						if (!CodeUtils.isEmpty(etag)) {
							isFileUpload = true;
							s3BucketFilePath = BucketPrefix + s3BucketInfo.getS3bucketName() + delimeter
									+ s3BucketFilePath;

							s3BucketFilePath = s3BucketFilePath.split("\\//")[1].replaceAll(
									s3BucketInfo.getS3bucketName() + delimeter, "") + delimeter.trim().toString();
							String URL = s3Wrapper.downloadPreSignedURLOnHistory(s3BucketInfo.getS3bucketName(),
									savedPath, Integer.parseInt(s3BucketInfo.getTime()));

							int updateBucketByProfile = s3BucketInfoService.updateBucketByProfile(savedPath);

							arrNode.add(URL);
							fileNameArrayNode.add(fileUpload.getFileName().get(i));

						} else {
							isFileUpload = false;
							break;
						}
					} else {

						appResponse = buildErrorResponse(AppErrorCodes.GENERIC_EVENT_NOT_AVAILABLE_ERROR_CODE,
								ClientErrors.NOT_FOUND, AppStatusMsg.FILE_NOT_UPLOADED);
						isFileUpload = false;
						break;
					}
				} else {

					appResponse = buildErrorResponse(AppErrorCodes.GENERIC_EVENT_NOT_AVAILABLE_ERROR_CODE,
							ClientErrors.NOT_FOUND, AppStatusMsg.ENTERPRISE_DATA);
					isFileUpload = false;
					break;
				}

			}

			if (isFileUpload == true) {
//				ObjectNode maindata = getMapper().createObjectNode();
//				maindata.put("path", arrNode);
//
//				appResponse = new AppResponse.Builder()
//						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG)
//								.putPojo(CodeUtils.RESPONSE, mainData))
//						.status(AppStatusCodes.genericSuccessCode).build();
//				
//				
				ObjectNode data = getMapper().createObjectNode();
				data.put("path", arrNode);
				data.put("fileName", fileNameArrayNode);

				String successMsg = (fileUpload.getFileName().size() > 1) ? "Images" : "Image";
				successMsg = successMsg + " uploaded successfully!!";

				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, data)
								.put(CodeUtils.RESPONSE_MSG, successMsg))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			}

		}

		catch (ArrayIndexOutOfBoundsException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			log.info(ex.getMessage());

		} catch (Exception e) {
			log.info(e.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.PI_UPDATE_ERROR,
					AppStatusMsg.FAILURE_MSG);

		} finally {

			return ResponseEntity.ok(appResponse);
		}

	}

	@SuppressWarnings("deprecation")
	ObjectNode buildArrayJson(List<ADMItem> data) {
		ObjectNode objectNode = getMapper().createObjectNode();
		ArrayNode array = getMapper().createArrayNode();
		data.forEach(e -> {
			ObjectNode node = getMapper().valueToTree(e);
			array.add(node);
		});
		objectNode.put(CodeUtils.RESPONSE, array);

		return objectNode;
	}

	@SuppressWarnings("deprecation")
	ObjectNode buildJson(List<PurchaseIndentHistory> data) {
		ObjectNode objectNode = getMapper().createObjectNode();
		ArrayNode array = getMapper().createArrayNode();
		data.forEach(e -> {
			ObjectNode node = getMapper().valueToTree(e);
			array.add(node);
		});
		objectNode.put(CodeUtils.RESPONSE, array);

		return objectNode;
	}

	@SuppressWarnings("deprecation")
	ObjectNode buildLoadIndentJson(List<PurchaseIndentHistory> data) {
		ObjectNode objectNode = getMapper().createObjectNode();
		ArrayNode array = getMapper().createArrayNode();
		data.forEach(e -> {

			ObjectNode node = getMapper().createObjectNode();

//			ObjectNode node = getMapper().valueToTree(e);
			array.add(node);
		});
		objectNode.put(CodeUtils.RESPONSE, array);

		return objectNode;
	}

	@RequestMapping(value = "/download/pdf", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> downloadPIPDF(@RequestParam(value = "savedPath") String savedPath,
			HttpServletRequest request) {

		AppResponse appResponse = null;
		String downloadPDF = "DOWNLOAD_PDF";

		try {
			String token = request.getHeader("X-Auth-Token");
			JsonArray orgDetails = CodeUtils.decode(token).get("ORG_DETAILS").getAsJsonArray();

			JsonObject jNode = orgDetails.get(0).getAsJsonObject();
			String orgId = jNode.get("orgId").getAsString();
			S3BucketInfo s3BucketInfo = s3BucketInfoService.getBucketByType(downloadPDF, orgId);
			// String savedPath = piService.getPDFPathByPattern(pattern);

			if (!CodeUtils.isEmpty(s3BucketInfo)) {
				String URL = s3Wrapper.downloadPreSignedURLOnHistory(s3BucketInfo.getS3bucketName(), savedPath,
						Integer.parseInt(s3BucketInfo.getTime()));
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, URL)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_API_NOT_FOUND, ClientErrors.NOT_FOUND,
						AppStatusMsg.PI_PDF_NOT_FOUND);

				log.info(AppStatusMsg.PI_PDF_NOT_FOUND);
			}
		} catch (CannotGetJdbcConnectionException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.PI_PDF_READER_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
			log.info(ex.getMessage());

		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.PI_PDF_READER_ERROR, appStatus.getMessage(AppModuleErrors.PI_PDF_READER_ERROR));
			log.info(ex.getMessage());

		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			log.info(e.getMessage());
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings("finally")
	@RequestMapping(value = "/update/status", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> updateStatus(@RequestParam(value = "status") String status,
			@RequestParam(value = "pattern") String pattern) {

		AppResponse appResponse = null;
		int result = 0;
		String updatedBy = "";
		try {
			OffsetDateTime updationTime = OffsetDateTime.now();
			updationTime = updationTime.plusHours(new Long(5));
			updationTime = updationTime.plusMinutes(new Long(30));

			String ipAddress = InetAddress.getLocalHost().getHostAddress().trim().toString();

			if (!CodeUtils.isEmpty(request)) {
				updatedBy = request.getAttribute("email").toString();
			}
			result = piService.updateStatus(status, pattern, ipAddress, updationTime, updatedBy);

			if (result != 0) {
				String message = "PENDING";
				if (status.contains("APPROVED")) {
					message = AppStatusMsg.PI_APPROVED;
				}
				if (status.contains("REJECTED")) {
					message = AppStatusMsg.PI_REJECTED;
				}
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, message))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
						AppModuleErrors.PI_UPDATE_ERROR, appStatus.getMessage(AppModuleErrors.PI_UPDATE_ERROR));
				log.info(appStatus.getMessage(AppModuleErrors.PI_UPDATE_ERROR));
			}

		} catch (CannotGetJdbcConnectionException ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.PI_UPDATE_ERROR,
					AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, AppModuleErrors.PI_UPDATE_ERROR,
					appStatus.getMessage(AppModuleErrors.PI_UPDATE_ERROR));
		} catch (NumberFormatException ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.NUMBER_FORMAT_MSG);

		} catch (Exception e) {
			log.info(e.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}

	}

	@RequestMapping(value = "/create/color", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> createColor(@RequestBody JsonNode piColor) {
		AppResponse appResponse = null;
		CatMaster catMaster = null;
		int colorSize = 0;
		try {
			catMaster = getMapper().treeToValue(piColor, CatMaster.class);
			// colorSize = piService.countColor(catMaster);
			if (colorSize == 0) {
				int result = piService.insertColor(catMaster);
				if (result != 0) {

					appResponse = new AppResponse.Builder()
							.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, catMaster)
									.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
							.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

				} else {
					appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
							AppModuleErrors.PI_COLOR_CREATOR_ERROR,
							appStatus.getMessage(AppModuleErrors.PI_COLOR_CREATOR_ERROR));

				}

			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
						AppStatusMsg.EXIST_COLOR);
			}

		} catch (JsonMappingException jsonException) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.INVALID_JSON);
			log.info(jsonException.getMessage());

		} catch (DataAccessException ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.PI_COLOR_CREATOR_ERROR,
					appStatus.getMessage(AppModuleErrors.PI_COLOR_CREATOR_ERROR));

		} catch (Exception ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@RequestMapping(value = "/create/brand", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> createBrand(@RequestBody JsonNode piBrand) {
		AppResponse appResponse = null;
		CatMaster catMaster = null;
		int brandSize = 0;
		try {
			catMaster = getMapper().treeToValue(piBrand, CatMaster.class);
			brandSize = piService.countBrand(catMaster);
			if (brandSize == 0) {
				int result = piService.insertBrand(catMaster);
				if (result != 0) {

					appResponse = new AppResponse.Builder()
							.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, catMaster)
									.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
							.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

				} else {
					appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
							AppModuleErrors.PI_BRAND_CREATOR_ERROR,
							appStatus.getMessage(AppModuleErrors.PI_BRAND_CREATOR_ERROR));

				}

			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
						AppStatusMsg.EXIST_BRAND);
			}

		} catch (JsonMappingException jsonException) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.INVALID_JSON);
			log.info(jsonException.getMessage());

		} catch (DataAccessException ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.PI_BRAND_CREATOR_ERROR,
					appStatus.getMessage(AppModuleErrors.PI_BRAND_CREATOR_ERROR));

		} catch (Exception ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@RequestMapping(value = "/create/size", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> createSize(@RequestBody JsonNode piSize) {
		AppResponse appResponse = null;
		CatMaster catMaster = null;
		int size = 0;
		try {
			catMaster = getMapper().treeToValue(piSize, CatMaster.class);
			// size = piService.countSize(catMaster);
			if (size == 0) {
				int result = piService.insertSize(catMaster);
				if (result != 0) {

					appResponse = new AppResponse.Builder()
							.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, catMaster)
									.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
							.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

				} else {
					appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
							AppModuleErrors.PI_SIZE_CREATOR_ERROR,
							appStatus.getMessage(AppModuleErrors.PI_SIZE_CREATOR_ERROR));

				}

			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
						AppStatusMsg.EXIST_SIZE);
			}

		} catch (JsonMappingException jsonException) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.INVALID_JSON);
			log.info(jsonException.getMessage());

		} catch (DataAccessException ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.PI_SIZE_CREATOR_ERROR, appStatus.getMessage(AppModuleErrors.PI_SIZE_CREATOR_ERROR));

		} catch (Exception ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@RequestMapping(value = "/create/uniquecode", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> createUniqueCode(@RequestBody JsonNode piUniqueCode) {
		AppResponse appResponse = null;
		CatMaster catMaster = null;
		int uniqueCodeSize = 0;
		try {
			catMaster = getMapper().treeToValue(piUniqueCode, CatMaster.class);
			uniqueCodeSize = piService.countUniqueCode(catMaster);
			if (uniqueCodeSize == 0) {
				int result = piService.insertUniqueCode(catMaster);
				if (result != 0) {

					appResponse = new AppResponse.Builder()
							.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, catMaster)
									.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
							.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

				} else {
					appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
							AppModuleErrors.PI_UNIQUE_CODE_CREATOR_ERROR,
							appStatus.getMessage(AppModuleErrors.PI_UNIQUE_CODE_CREATOR_ERROR));

				}

			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
						AppStatusMsg.EXIST_UNIQUECODE);
			}

		} catch (JsonMappingException jsonException) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.INVALID_JSON);
			log.info(jsonException.getMessage());

		} catch (DataAccessException ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.PI_UNIQUE_CODE_CREATOR_ERROR,
					appStatus.getMessage(AppModuleErrors.PI_UNIQUE_CODE_CREATOR_ERROR));

		} catch (Exception ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	/*
	 * @SuppressWarnings({ "finally" })
	 * 
	 * @RequestMapping(value = "/upload/article/image", method =
	 * RequestMethod.POST,consumes=MediaType.MULTIPART_FORM_DATA_VALUE)
	 * 
	 * @ResponseBody public ResponseEntity<AppResponse> uploadArticle(@RequestBody
	 * FileUpload fileUpload) {
	 * 
	 * AppResponse appResponse = null;
	 * 
	 * BufferedOutputStream stream; String file = fileUpload.getFile(); String
	 * fileName = null; String filePath = null; File saveBucket = null; String
	 * extension=CodeUtils.getExtensionByApacheCommonLib(fileUpload.getFileName());
	 * 
	 * try {
	 * 
	 * S3BucketInfo s3BucketInfo =
	 * s3BucketInfoService.getBucketByType(Download_Image_Article);
	 * 
	 * if (!file.isEmpty() && !CodeUtils.isEmpty(s3BucketInfo)) {
	 * 
	 * String s3BucketFilePath = s3BucketInfo.getBucketPath(); fileName =
	 * s3BucketInfo.getEnterprise() + separator +
	 * CodeUtils.______dateFormat.format(new Date());
	 * 
	 * String savedPath = s3BucketFilePath + delimeter + fileName +"."+ extension;
	 * filePath = FileUtils.createFilePath(FileUtils.getPlatformBasedParentDir(),
	 * savedPath);
	 * 
	 * String base64Image = file.split(",")[1];
	 * 
	 * byte[] imageBytes =
	 * javax.xml.bind.DatatypeConverter.parseBase64Binary(base64Image);
	 * 
	 * stream = new BufferedOutputStream(new FileOutputStream(filePath));
	 * stream.write(imageBytes); stream.close(); saveBucket = new File(filePath);
	 * String etag = s3Wrapper .upload(saveBucket,
	 * savedPath,s3BucketInfo.getS3bucketName()).getETag();
	 * 
	 * if(!CodeUtils.isEmpty(etag)) { s3BucketFilePath = BucketPrefix +
	 * s3BucketInfo.getS3bucketName() + delimeter + s3BucketFilePath;
	 * 
	 * s3BucketFilePath =
	 * s3BucketFilePath.split("\\//")[1].replaceAll(s3BucketInfo.getS3bucketName() +
	 * delimeter, "") + delimeter.trim().toString(); String URL =
	 * s3Wrapper.downloadPreSignedURLOnHistory(s3BucketInfo.getS3bucketName(),
	 * savedPath, Integer.parseInt(s3BucketInfo.getTime()));
	 * 
	 * ObjectNode objectNode = getMapper().createObjectNode();
	 * objectNode.put("path", URL); appResponse = new AppResponse.Builder()
	 * .data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, objectNode)
	 * .put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
	 * .status(AppStatusCodes.genericSuccessCode).build(); } } else { appResponse =
	 * buildErrorResponse(AppStatusCodes.genericClientErrorCode,
	 * AppModuleErrors.piUpdateError,
	 * appStatus.getMessage(AppStatusMsg.FILE_NOT_UPLOADED)); }
	 * 
	 * } catch (ArrayIndexOutOfBoundsException e) { e.printStackTrace(); appResponse
	 * = buildErrorResponse(AppStatusCodes.genericClientErrorCode,
	 * AppModuleErrors.piUpdateError,
	 * appStatus.getMessage(AppStatusMsg.FILE_NOT_UPLOADED));
	 * 
	 * } catch (Exception e) { e.printStackTrace(); appResponse =
	 * buildErrorResponse(AppStatusCodes.genericServerErrorCode,
	 * ClientErrors.badRequest, AppStatusMsg.FAILURE_MSG); } finally { return
	 * ResponseEntity.ok(appResponse); }
	 * 
	 * }
	 */

	@SuppressWarnings({ "unchecked", "deprecation", "rawtypes", "finally" })
	@RequestMapping(method = RequestMethod.GET, value = "/get/transporter")
	@ResponseBody
	public ResponseEntity<AppResponse> getTransporterData(
			@RequestParam(value = "transporterCode") String transporterCode, @RequestParam("type") Integer type,
			@RequestParam("pageNo") Integer pageNo, @RequestParam("transporterName") String transporterName,
			@RequestParam("search") String search, @RequestParam("cityName") String cityName) {

		log.info("PurchaseIndent getTransporterData() method starts here....");
		AppResponse appResponse = null;
		ObjectNode node = getMapper().createObjectNode();
		ArrayNode arrNode = getMapper().createArrayNode();
		List<Transporters> transportersList = null;

		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;

		AtomicInteger count = new AtomicInteger(1);

		try {
			if (type == 1) {
				totalRecord = this.getAllForTransporterRecord(cityName);
//				totalRecord = piService.getAllTransportersRecord();

				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
				transportersList = this.getAllForTransporters(offset, pageSize, cityName);
//				transportersList = piService.getAllTransporters(offset, pageSize);

			}

			else if (type == 2) {

				totalRecord = this.getAllFilterForTransportersRecord(transporterName, transporterCode, cityName);
//				totalRecord = piService.getAllFilterTransportersRecord(transporterName, transporterCode);

				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;

				transportersList = this.getAllFilterForTransporters(offset, pageSize, transporterName, transporterCode,
						cityName);
//				transportersList = piService.getAllFilterTransporters(offset, pageSize, transporterName,
//						transporterCode);

			}

			else {

				if (type == 3 && !search.isEmpty()) {

					transporterName = transporterName.trim();
					totalRecord = this.getAllSearchForTransportersRecord(search, cityName);
//					totalRecord = piService.getAllSearchTransportersRecord(search);

					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
					transportersList = this.getAllSearchForTransporters(offset, pageSize, search, cityName);
//					transportersList = piService.getAllSearchTransporters(offset, pageSize, search);

				}

			}

			log.info("data present in the list transportersList is {} : " + transportersList);

			if (transportersList.size() > 0 && !transportersList.isEmpty()) {

				transportersList.stream().forEach(transporter -> {

					ObjectNode tempNode = getMapper().createObjectNode();
					tempNode.put(PurchaseIndentMetaData.Transporter.ID, count.get());
					tempNode.put(PurchaseIndentMetaData.Transporter.TRANSPORTER_CODE, transporter.getTransporterCode());
					tempNode.put(PurchaseIndentMetaData.Transporter.TRANSPORTER_NAME, transporter.getTransporterName());

					arrNode.add(tempNode);
					count.getAndIncrement();
				});

				node.put(CodeUtils.RESPONSE, arrNode);

				log.info("Final object node data is  {}", node);

				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").put("currPage", pageNo)
								.put("prePage", previousPage).put("maxPage", maxPage).putAll(node))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			}
		} catch (MyBatisSystemException ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CONNECTION_ERROR_CODE,
					AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, AppStatusMsg.CONNECTION_NOT_AVAILABLE);

		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.TRANSPORTER_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.TRANSPORTER_READER_ERROR));

			log.info(ex.getMessage());

		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			log.info(e.getMessage());
		} finally {

			log.info("PurchaseIndent getTransporterData() method ends here....");

			return ResponseEntity.ok(appResponse);
		}
	}

	private List<Transporters> getAllSearchForTransporters(int offset, int pageSize, String search, String cityName) {
		if (CodeUtils.isEmpty(cityName)) {
			return piService.getAllSearchTransporters(offset, pageSize, search);
		} else {
			return piService.getAllSearchTransportersByCity(offset, pageSize, search, cityName);
		}
	}

	private int getAllSearchForTransportersRecord(String search, String cityName) {
		if (CodeUtils.isEmpty(cityName)) {
			return piService.getAllSearchTransportersRecord(search);
		} else {
			return piService.getAllSearchTransportersRecordByCity(search, cityName);
		}
	}

	private List<Transporters> getAllFilterForTransporters(int offset, int pageSize, String transporterName,
			String transporterCode, String cityName) {
		if (CodeUtils.isEmpty(cityName)) {
			return piService.getAllFilterTransporters(offset, pageSize, transporterName, transporterCode);
		} else {
			return piService.getAllFilterTransportersByCity(offset, pageSize, transporterName, transporterCode,
					cityName);
		}
	}

	private int getAllFilterForTransportersRecord(String transporterName, String transporterCode, String cityName) {
		if (CodeUtils.isEmpty(cityName)) {
			return piService.getAllFilterTransportersRecord(transporterName, transporterCode);
		} else {
			return piService.getAllFilterTransportersRecordByCity(transporterName, transporterCode, cityName);
		}
	}

	private List<Transporters> getAllForTransporters(int offset, int pageSize, String cityName) {
		if (CodeUtils.isEmpty(cityName)) {
			return piService.getAllTransporters(offset, pageSize);
		} else {
			return piService.getAllTransportersByCity(offset, pageSize, cityName);
		}

	}

	private int getAllForTransporterRecord(String cityName) {
		if (CodeUtils.isEmpty(cityName)) {
			return piService.getAllTransportersRecord();
		} else {
			return piService.getAllTransportersRecordByCity(cityName);
		}

	}

	@SuppressWarnings({ "finally" })
	@RequestMapping(method = RequestMethod.GET, value = "/validation")
	@ResponseBody
	public ResponseEntity<AppResponse> getValidation(@RequestParam("isExist") String isExist,
			@RequestParam("hl3Code") String hl3Code, @RequestParam("categoryName") String categoryName,
			@RequestParam("hl1name") String hl1name, @RequestParam("hl2name") String hl2name,
			@RequestParam("hl3name") String hl3name) {

		log.info("PurchaseIndent getValidation() method starts here....");
		AppResponse appResponse = null;
		int numberOfData = 0;
		String msg = "";

		try {
			/*
			 * if ((CodeUtils.isEmpty(hl1name) || CodeUtils.isEmpty(hl2name) ||
			 * CodeUtils.isEmpty(hl3name)) && (CodeUtils.isEmpty(hl4Code) &&
			 * (CodeUtils.isEmpty(categoryName) || CodeUtils.isEmpty(description)))) throw
			 * new SupplyMintException(AppModuleMsg.PI_CAT_DESC_VALIDATION);
			 */
			if (isExist.equalsIgnoreCase(PIValidation.UNIQUECODE.toString())) {
				numberOfData = piService.getExistanceUniqueCodeByArticleCode(hl3Code, categoryName);
				msg = AppStatusMsg.UNIQUE_CODE_VALIDATION_MSG;
			} else if (isExist.equalsIgnoreCase(PIValidation.BRAND.toString())) {
				numberOfData = piService.getExistanceBrandsByArticleCode(hl3Code, categoryName);
				msg = AppStatusMsg.BRAND_VALIDATION_MSG;
			} else if (isExist.equalsIgnoreCase(PIValidation.PATTERN.toString())) {
				numberOfData = piService.getExistancePatternsByArticleCode(hl3Code, categoryName);
				msg = AppStatusMsg.PATTERN_VALIDATION_MSG;
			} else if (isExist.equalsIgnoreCase(PIValidation.ASSORTMENT.toString())) {
				numberOfData = piService.getExistanceAssortmentsByArticleCode(hl3Code, categoryName);
				msg = AppStatusMsg.ASSORTMENT_VALIDATION_MSG;
			} else if (isExist.equalsIgnoreCase(PIValidation.SEASON.toString())) {
				numberOfData = piService.getExistanceSeasonByArticleCode(hl3Code, categoryName);
				msg = AppStatusMsg.SEASONS_VALIDATION_MSG;
			} else if (isExist.equalsIgnoreCase(PIValidation.FABRIC.toString())) {
				numberOfData = piService.getExistanceFabricByArticleCode(hl3Code, categoryName);
				msg = AppStatusMsg.FABRIC_VALIDATION_MSG;
			} else if (isExist.equalsIgnoreCase(PIValidation.DARWIN.toString())) {
				numberOfData = piService.getExistanceDarwinByArticleCode(hl3Code, categoryName);
				msg = AppStatusMsg.DARWIN_VALIDATION_MSG;
			} else if (isExist.equalsIgnoreCase(PIValidation.WEAVED.toString())) {
				numberOfData = piService.getExistanceWeavedByArticleCode(hl3Code, categoryName);
				msg = AppStatusMsg.WEAVED_VALIDATION_MSG;
			} else if (isExist.equalsIgnoreCase(PIValidation.SIZE.toString())) {
				numberOfData = piService.getExistanceSizeByArticleCode(hl3name, categoryName);
				msg = AppStatusMsg.SIZE_VALIDATION_MSG;
			} else if (isExist.equalsIgnoreCase(PIValidation.COLOR.toString())) {
				numberOfData = piService.getExistanceColorByArticleCode(hl3Code, categoryName);
				msg = AppStatusMsg.COLOR_VALIDATION_MSG;
			} else if (isExist.equalsIgnoreCase(PIValidation.HIERARACHY.toString())) {
				numberOfData = piService.getExistanceHierarachyLevels(hl1name, hl2name, hl3name);
				msg = AppStatusMsg.DEPARTMENT_VALIDATION_MSG;
			}

			if (numberOfData != 0) {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
						msg);
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}

		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.PI_VALIDATION_ERROR, appStatus.getMessage(AppModuleErrors.PI_VALIDATION_ERROR));

			log.info(ex.getMessage());

		} /*
			 * catch (SupplyMintException ex) { log.info(ex.getMessage()); appResponse =
			 * buildErrorResponse(AppStatusCodes.genericClientErrorCode,
			 * ClientErrors.badRequest, AppModuleMsg.PI_CAT_DESC_VALIDATION);
			 * 
			 * }
			 */catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			log.info(e.getMessage());
		} finally {

			log.info("PurchaseIndent getValidation() method ends here....");

			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "deprecation", "finally" })
	@RequestMapping(value = "/get/margin/rule/data/{slCode}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> marginRuleData(@PathVariable(value = "slCode") String slCode,
			@RequestParam("articleCode") String articleCode, @RequestParam("siteCode") String siteCode) {

		log.info("PurchaseIndent marginRuleData() method starts here....");

		AppResponse appResponse = null;
		String data = null;
		ObjectNode objNode = getMapper().createObjectNode();
		String tenantHashKey = "";

		try {
			tenantHashKey = ThreadLocalStorage.getTenantHashKey();

			data = piService.marginRule(slCode, articleCode, tenantHashKey, siteCode);

			if (!CodeUtils.isEmpty(data)) {
				objNode.putPOJO(CodeUtils.RESPONSE, data);

				log.info("Final Object Node is : {}", objNode);

				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
						.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG).putAll(objNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

				log.info(AppStatusMsg.SUCCESS_MSG);
			} else {
				if (tenantHashKey.equalsIgnoreCase(TenantHash.valueOf("VMART").getHashKey()))
					objNode.putPOJO(CodeUtils.RESPONSE, data);
				else
					objNode.putPOJO(CodeUtils.RESPONSE, "NA");
				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
						.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG).putAll(objNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}

		} catch (NullPointerException e) {
			appResponse = new AppResponse.Builder().data(
					getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null).put(CodeUtils.RESPONSE_MSG, ""))
					.status(AppStatusMsg.BLANK).build();
			log.info(e.getMessage());
		} catch (DataAccessException ex) {

			log.debug(AppModuleErrors.PI_MARGIN_RULE_READER_ERROR, ex);

			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.PI_MARGIN_RULE_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.PI_MARGIN_RULE_READER_ERROR));
		} catch (Exception ex) {
			ex.printStackTrace();
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			log.info(ex.getMessage());
		} finally {
			log.info("PurchaseIndent marginRuleData() method ends here....");

			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "deprecation", "finally" })
	@RequestMapping(value = "/multiple/file/upload", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	@ResponseBody
	public ResponseEntity<AppResponse> uploadFileData(@RequestPart("file") MultipartFile[] multipartFiles,
			HttpServletRequest request) {
		log.info("PurchaseIndent uploadFileData() method starts here....");
		AppResponse appResponse = null;
		Map<String, String> map = null;
		try {

			ObjectNode objNode = getMapper().createObjectNode();
			ArrayNode arrNode = getMapper().createArrayNode();
			for (MultipartFile upload : multipartFiles) {
				ObjectNode objTemp = getMapper().createObjectNode();
				map = UploadUtils.uploadImageFile(upload, UPLOAD_Image_Article, request);
				objTemp.put("url", map.get("url"));
				objTemp.put("fileName", upload.getOriginalFilename());
				arrNode.add(objTemp);
			}
			objNode.put(CodeUtils.RESPONSE, arrNode);

			if (!CodeUtils.isEmpty(map)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(objNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {

				appResponse = buildErrorResponse(AppErrorCodes.GENERIC_EVENT_NOT_AVAILABLE_ERROR_CODE,
						ClientErrors.NOT_FOUND, AppStatusMsg.ENTERPRISE_DATA);

			}

		} catch (NullPointerException e) {
			appResponse = new AppResponse.Builder().data(
					getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null).put(CodeUtils.RESPONSE_MSG, ""))
					.status(AppStatusMsg.BLANK).build();
			log.info(e.getMessage());
		} catch (DataAccessException ex) {
			log.debug(AppModuleErrors.PI_IMAGE_UPLOAD_ERROR, ex);
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.PI_IMAGE_UPLOAD_ERROR, appStatus.getMessage(AppModuleErrors.PI_IMAGE_UPLOAD_ERROR));
		} catch (Exception ex) {
			ex.printStackTrace();
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			log.info(ex.getMessage());
		} finally {
			log.info("PurchaseIndent uploadFileData() method ends here....");
			return ResponseEntity.ok(appResponse);
		}

	}

	@SuppressWarnings({ "unchecked", "rawtypes", "deprecation", "finally" })
	@RequestMapping(value = "/get/itemcatdesc/{hl3Code}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getItemCatDesc(@PathVariable(value = "hl3Code") String hl3Code,
			@RequestParam("type") Integer type, @RequestParam("pageNo") Integer pageNo,
			@RequestParam("catDescType") String catDescType, @RequestParam("hl3Name") String hl3Name,
			@RequestParam("search") String search) {

		log.info("PurchaseIndent getItemCatDesc() method starts here....");
		AppResponse appResponse = null;
		ObjectNode node = getMapper().createObjectNode();
		ObjectNode objectNode = null;
		try {
			String tenantHashKey = ThreadLocalStorage.getTenantHashKey();
			if (type == 1) {
				objectNode = piService.getItemCatDescData(pageNo, hl3Code, catDescType, hl3Name, tenantHashKey);

			} else if (type == 3) {
				objectNode = piService.searchItemCatDescData(pageNo, hl3Code, catDescType, hl3Name, search,
						tenantHashKey);
			}
			if (objectNode.size() != 0) {
				log.info("Final Object node data is {} ", node);
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(objectNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				log.info(AppStatusMsg.SUCCESS_MSG);
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

				log.info(AppStatusMsg.BLANK);
			}
		} catch (MyBatisSystemException ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CONNECTION_ERROR_CODE,
					AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, AppStatusMsg.CONNECTION_NOT_AVAILABLE);
		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			log.info(e.getMessage());
		} finally {
			log.info("PurchaseOrder getDeptItemUDFData() method ends here....");
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "deprecation", "finally" })
	@RequestMapping(method = RequestMethod.GET, value = "/get/catdesc/caption")
	@ResponseBody
	public ResponseEntity<AppResponse> getCatDescByHl3Name(@RequestParam(value = "hl3Code") String hl3Code,
			@RequestParam(value = "hl3Name") String hl3Name) {

		log.info("PurchaseIndent getCatDescByHl3Name() method starts here....");
		AppResponse appResponse = null;
		List<DeptItemUDFSettings> data = null;

		try {

			data = piService.getCatDescHeader(hl3Code);
			if (!CodeUtils.isEmpty(data)) {
				ObjectNode dataNode = piService.getCatDescHeaderNode(data);

				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
						.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG).putAll(dataNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

				log.info(AppStatusMsg.BLANK);
			}

		} catch (MyBatisSystemException ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CONNECTION_ERROR_CODE,
					AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, AppStatusMsg.CONNECTION_NOT_AVAILABLE);

		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.INDT_CAT_DESC_UDF_LIST, appStatus.getMessage(AppModuleErrors.CAT_DESC_HEADER));

			log.info(ex.getMessage());
		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			log.info(e.getMessage());
		} finally {
			log.info("PurchaseIndent getItemCatDesc() method ends here....");

			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "deprecated", "finally" })
	@RequestMapping(method = RequestMethod.GET, value = "/get/hsncode")
	@ResponseBody
	public ResponseEntity<AppResponse> getPODepartments(@RequestParam("pageNo") Integer pageNo,
			@RequestParam("type") Integer type, @RequestParam("search") String search,
			@RequestParam("departmentCode") String departmentCode) {

		AppResponse appResponse = null;
		List<HsnData> hsnData = null;
		ObjectNode node = getMapper().createObjectNode();
		ArrayNode arrNode = getMapper().createArrayNode();
		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;
		String hsnCode = "";
		String hsnSacCode = "";
		Map<String, String> smHSNCode = null;
		List hsnList = new ArrayList<HashMap<String, BigDecimal>>();
		String tenantHashKey = ThreadLocalStorage.getTenantHashKey();
		try {

			if (type == 1) {
				totalRecord = piService.countHSNCode();
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
				hsnData = piService.getHSNCode(offset, pageSize);
			} else if (type == 2) {
			}

			else {

				if (type == 3 && !CodeUtils.isEmpty(search)) {
					totalRecord = piService.countHSNCodeSearch(search);
					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
					hsnData = piService.getHSNCodeSearch(offset, pageSize, search);
				}
			}

//			AtomicInteger count = new AtomicInteger(1);
//			count.set(((pageNo - 1) * 10) + 1);

			if (!CodeUtils.isEmpty(hsnData)) {

				ObjectNode hsnRequest = getMapper().createObjectNode();
				hsnRequest.put("hl3Code", departmentCode);

				PurchaseOrderConfiguration poc = poService.getPOConfiguration(3);

				JsonNode headerJson = getMapper().readTree(poc.getHeaders());
				String headerType = getMapper().treeToValue(headerJson.get("headerType"), String.class);

				ResponseEntity<AppResponse> responseEntityData = null;
				responseEntityData = poService.sendPODetailsOnRestAPI(headerType, hsnRequest, poc);

				if (!CodeUtils.isEmpty(responseEntityData)) {
					String responseEntityStatus = responseEntityData.getBody().getStatus();
					log.info("response entity message :"
							+ responseEntityData.getBody().getData().get(CodeUtils.RESPONSE_MSG));
					log.info("error code response entity message :"
							+ responseEntityData.getBody().getError().get("errorCode"));
					log.info("error message response entity message :"
							+ responseEntityData.getBody().getError().get("errorMessage"));
					log.info("response calling :" + responseEntityStatus);
					if (responseEntityStatus.contains(CodeUtils.RESPONSE_SUCCESS_CODE)) {
						log.info("response true");
						hsnCode = responseEntityData.getBody().getData().get("hsnCode").toString();
						hsnSacCode = responseEntityData.getBody().getData().get("hsnSacCode").toString();
//				poa.setHttpMessage(responseEntityData.getBody().getData().get(CodeUtils.RESPONSE_MSG).toString());
						smHSNCode = new HashMap<String, String>();
						smHSNCode.put("code", hsnCode.substring(1, hsnCode.length() - 1));
						smHSNCode.put("hsnCode", hsnSacCode.substring(1, hsnSacCode.length() - 1));
//						hsnData.set(0, smHSNCode);
					}
				} else {
// 					previous code commented 					
//					throw new SupplyMintException("Unable to push open purchase order in sm partner");
					// @Author Prabhakar -- Start
					if (tenantHashKey.equalsIgnoreCase(TenantHash.valueOf("VMART").getHashKey()))
						throw new SupplyMintException("Unable to push open purchase order in sm partner");
					else {
						smHSNCode = new HashMap<String, String>();
						smHSNCode.put("code", "140");
						smHSNCode.put("hsnCode", "9503");
					}
					// end
				}
				/*
				 * hsnData.stream().forEach(e -> {
				 * 
				 * ObjectNode tempItemNode = getMapper().createObjectNode(); //
				 * tempItemNode.put("id", count.get());
				 * 
				 * tempItemNode.put("code", e.get("CODE")); tempItemNode.put("hsnSacCode",
				 * e.get("HSN_SAC_CODE"));
				 * 
				 * arrNode.add(tempItemNode); // count.getAndIncrement();
				 * 
				 * });
				 */

				for (int i = 0; i < hsnData.size(); i++) {
					ObjectNode tempItemNode = getMapper().createObjectNode();
					tempItemNode.put("code", hsnData.get(i).getCode().toString());
					tempItemNode.put("hsnSacCode", hsnData.get(i).getHsn_sac_code());
					arrNode.add(tempItemNode);
				}

				ObjectNode defaultNode = getMapper().createObjectNode();

				defaultNode.put("defaultHSNCode", smHSNCode.get("code"));
				defaultNode.put("defaultHSNSACCode", smHSNCode.get("hsnCode"));
				node.put("defaultHSNKEy", defaultNode);
				node.put(CodeUtils.RESPONSE, arrNode);

				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode().put("currPage", pageNo)
						.put("prePage", previousPage).put("maxPage", maxPage)
						.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG).putAll(node))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				log.info(AppStatusMsg.BLANK);

			}
		} catch (DataAccessException ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.PI_HSN_CODE_ERROR, ClientErrorMessage.BAD_REQUEST);
		} catch (SupplyMintException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.PI_SMPARTNER_HSN_CODE_ERROR,
					appStatus.getMessage(AppModuleErrors.PI_SMPARTNER_HSN_CODE_ERROR));
			log.info(appStatus.getMessage(AppModuleErrors.PI_SMPARTNER_HSN_CODE_ERROR));
		} catch (Exception e) {
			log.info(e.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);

		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes", "deprecation", "finally" })
	@RequestMapping(value = "/get/multiplelineitem/charges/{data}", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> getMultipleLineItemChargesByPurchaseTermCode(@RequestBody ObjectNode multiNode) {

		log.info("PurchaseIndent getMultipleLineItemChargesByPurchaseTermCode() method starts here....");
		log.info("JSON data recieved in the node is : {}", multiNode);

		AppResponse appResponse = null;
		ObjectNode nodeObj;
		ArrayNode arrNode;
		String gstComponent = null;
		boolean isGSTApplicable = false;

		ObjectNode mainObj = getMapper().createObjectNode();
		ArrayNode mainArrNode = getMapper().createArrayNode();

		try {

			for (JsonNode node : multiNode.get("multipleLineItem")) {

				nodeObj = getMapper().createObjectNode();
				arrNode = getMapper().createArrayNode();

				if (!node.isNull()) {
					Integer purtermMainCode = Integer.parseInt(node.get("purtermMainCode").asText());
					Double transactionPrice = Double.parseDouble(node.get("rate").asText());

					String date = node.get("piDate").asText();
					Date parsedDate = CodeUtils.dateTimeMonthFormat.parse(date);

					String piDate = new SimpleDateFormat("dd-MMM-yyyy").format(parsedDate);

					String clientGstInNo = node.get("clientGstIn").asText();
					Integer supplierGstinStateCode = Integer.parseInt(node.get("supplierGstinStateCode").asText());
					Double basicValue = Double.parseDouble(node.get("basic").asText());

					// compare supplier and enterpriseGstin state code
					boolean result = piService.compareSupplierAndClientGstin(clientGstInNo, supplierGstinStateCode);

					log.info("after comparing supplier and enterprise gstin, the result obtained is : {}", result);

					if (result) {
						// CGST, SGST and CESS Rate will apply on the line item

						log.info("calculating cgst, sgst and cess....");
						Double subTotal = 0.0;
						double grandTotal = 0.0;

						List<PurtermCharge> purTermChargelList = piService
								.getPurchaseTermChargeDetailsByPurchaseTermCode(purtermMainCode);

						log.info("Charges applicable for the purchase term are : {},{} ", purTermChargelList.size(),
								purTermChargelList);

						for (PurtermCharge purtermCharge : purTermChargelList) {

							ObjectNode tempNode = getMapper().createObjectNode();

							log.info("purtermCharge contains the following details : {}", purtermCharge);

							int seq = purtermCharge.getPtSeq().intValue();
							String operationLevel = purtermCharge.getPtOperationLevel();
							String formula = purtermCharge.getPtFormulae();
							Double finChgRate = purtermCharge.getRate().doubleValue();
							String source = purtermCharge.getSource();
							gstComponent = purtermCharge.getGstComponent();
							String isTax = purtermCharge.getIsTax();
							String sign = purtermCharge.getSign();
							String finChgOperationLevel = purtermCharge.getOperationLevel();
							String calculationBasis = purtermCharge.getBasis();
							BigDecimal chgCode = purtermCharge.getChgCode();
							String chgName = purtermCharge.getChgName();

							// fincharges
							tempNode.put(PurchaseIndentMetaData.PurtermCharge.CHARGE_CODE, chgCode);
							tempNode.put(PurchaseIndentMetaData.PurtermCharge.CHARGE_NAME, chgName);
							tempNode.put(PurchaseIndentMetaData.PurtermCharge.CHARGE_SEQ, seq);
							tempNode.put(PurchaseIndentMetaData.PurtermCharge.FIN_FORMULA, formula);
							tempNode.put(PurchaseIndentMetaData.PurtermCharge.FIN_CHARGE_RATE, finChgRate);
							tempNode.put(PurchaseIndentMetaData.PurtermCharge.FIN_SOURCE, source);
							tempNode.put(PurchaseIndentMetaData.PurtermCharge.GST_COMPONENT, gstComponent);
							tempNode.put(PurchaseIndentMetaData.PurtermCharge.IS_TAX, isTax);
							tempNode.put(PurchaseIndentMetaData.PurtermCharge.SIGN, sign);
							tempNode.put(PurchaseIndentMetaData.PurtermCharge.FIN_CHARGE_OPERATION_LEVEL,
									finChgOperationLevel);
							tempNode.put(PurchaseIndentMetaData.PurtermCharge.CALCULATION_BASIS, calculationBasis);

							if (gstComponent == null) {

								// When gst component is null and operation level is line level
								if (finChgOperationLevel.equals("L")) {

									// When gst component is null and operation level is line level and calculation
									// basis is Amount
									if (calculationBasis.equals("A")) {

										if (sign.equals("+")) {
											subTotal = subTotal + finChgRate;
											log.info("subtotal : {}", subTotal);
										} else {
											subTotal = subTotal - finChgRate;
											log.info("subtotal : {}", subTotal);
										}

									} else {
										// When gst component is null and operation level is line level and calculation
										// basis is Percentage

										if (sign.equals("+")) {
											double rateAmount = ((basicValue * finChgRate) / 100);
											subTotal = subTotal + rateAmount;
										} else {
											double rateAmount = ((basicValue * finChgRate) / 100);
											subTotal = subTotal - rateAmount;
										}

									}
								} else {
									// When gst component is null and operation level is Header level
								}
							}

							else if (!CodeUtils.isEmpty(gstComponent)) {
								isGSTApplicable = true;
								log.info("CGST and SGST calculation starts here.....");

								if (source.equals("G") && (gstComponent.equals("CGST") || gstComponent.equals("SGST")
										|| gstComponent.equals("CESS")) && isTax.equals("Y")) {

									if (operationLevel.equals("L")) {

										// calculating rate for charges starts here
										String hsnSacCode = node.get("hsnSacCode").asText();

										HsnGstData maxEffectiveDate = piService
												.getMaxEffectiveDateByHsnSacCode(hsnSacCode, piDate);

										Timestamp effectiveDate = maxEffectiveDate.getEffectiveDate();
										String slab = maxEffectiveDate.getSlab();

										if (slab.equals("Without Slab")) {
											String taxName = maxEffectiveDate.getTaxName();
											String gstSlab = maxEffectiveDate.getSlab();
											String slabEffectiveDate = maxEffectiveDate.getEffectiveDate().toString();

											Double cgstRate = 0.0, sgstRate = 0.0, cessRate = 0.0;

											if (gstComponent.equalsIgnoreCase("cgst")) {
												cgstRate = maxEffectiveDate.getCgstRate().doubleValue();
											} else if (gstComponent.equalsIgnoreCase("sgst")) {
												sgstRate = maxEffectiveDate.getSgstRate().doubleValue();
											} else {
												cessRate = maxEffectiveDate.getCessRate().doubleValue();
											}

											// rates
											tempNode.put("rates", piService.getCgstChargeRateNode(taxName, gstSlab,
													slabEffectiveDate, cgstRate, sgstRate, cessRate));

											nodeObj.put(PurchaseIndentMetaData.PILineItem.GST,
													piService.getGstRateByTaxName(taxName));

											Double ch1 = 0.0, ch2 = 0.0, ch3 = 0.0;

											PurtermDet purTermDet = piService
													.getPurchaseTermDetailsByChgCode(purtermMainCode, chgCode);

											// for charges
											ObjectNode chargeNode = getMapper().createObjectNode();

											String formulae = purTermDet.getFormulae();
											String lineItermSign = purTermDet.getSign();

											chargeNode.put("seq", purTermDet.getSeq());
											chargeNode.put("chgcode", purTermDet.getChgCode());
											chargeNode.put("rate", purTermDet.getRate());
											chargeNode.put("formula", formulae);
											chargeNode.put("sign", lineItermSign);
											chargeNode.put("operationLevel", purTermDet.getOperationLevel());

											Map<String, Double> subTotalMap = piService.calulcateTotalForCgstSgstRates(
													subTotal, basicValue, cgstRate, sgstRate, cessRate, ch1, ch2, ch3,
													lineItermSign, formulae);

											subTotal = subTotalMap.get("subTotal");
											chargeNode.put("chargeAmount", subTotalMap.get("chargeAmount"));
											chargeNode.put("subTotal", subTotal);

											tempNode.put("charges", chargeNode);

										} else {

											String piEffectiveDate = new SimpleDateFormat("dd-MMM-yyyy")
													.format(effectiveDate);
											log.info("Maximum effective date obtained is = " + piEffectiveDate);

											// get HSNGSTDATA using hsn sac code
											// List<HsnGstData> hsnGstDataList =
											// piService.getHsnGstDataByHsnSacCode(hsnSacCode,piEffectiveDate);

											HsnGstData maxAmountFromHsnGstData = piService.getMaxAmountFrom(hsnSacCode,
													piEffectiveDate);
											Double maxAmountFrom = maxAmountFromHsnGstData.getAmountFrom()
													.doubleValue();

											HsnGstData minAmountFromHsnGstData = piService.getMinAmountFrom(hsnSacCode,
													piEffectiveDate);
											Double minAmountFrom = minAmountFromHsnGstData.getAmountFrom()
													.doubleValue();

											log.info(
													"Minimum and Maximum AMOUNTFROM for GST Slab obtained are as follows : {}, {}",
													minAmountFrom, maxAmountFrom);

											log.info("Transaction Price for item is : {}", transactionPrice);
											if (transactionPrice > minAmountFrom && transactionPrice <= maxAmountFrom) {
												HsnGstData applicableGstSlab = piService
														.getApplicableGstSlabForTransactionPrice(hsnSacCode,
																piEffectiveDate, minAmountFrom);

												log.info("Applicable GST Slab is : {}", applicableGstSlab);
												String taxName = applicableGstSlab.getTaxName();
												String gstSlab = applicableGstSlab.getSlab();
												String slabEffectiveDate = applicableGstSlab.getEffectiveDate()
														.toString();
												Double cgstRate = 0.0, sgstRate = 0.0, cessRate = 0.0;

												if (gstComponent.equalsIgnoreCase("cgst")) {
													cgstRate = applicableGstSlab.getCgstRate().doubleValue();
												} else if (gstComponent.equalsIgnoreCase("sgst")) {
													sgstRate = applicableGstSlab.getSgstRate().doubleValue();
												} else {
													cessRate = applicableGstSlab.getCessRate().doubleValue();
												}

												// for rates
												tempNode.put("rates", piService.getCgstChargeRateNode(taxName, gstSlab,
														slabEffectiveDate, cgstRate, sgstRate, cessRate));

												nodeObj.put(PurchaseIndentMetaData.PILineItem.GST,
														piService.getGstRateByTaxName(taxName));

												Double ch1 = 0.0, ch2 = 0.0, ch3 = 0.0;

												PurtermDet purTermDet = piService
														.getPurchaseTermDetailsByChgCode(purtermMainCode, chgCode);

												// for charges
												ObjectNode chargeNode = getMapper().createObjectNode();

												BigDecimal chargeCode = purTermDet.getChgCode();
												String formulae = purTermDet.getFormulae();
												String lineItermSign = purTermDet.getSign();
												BigDecimal rate = purTermDet.getRate();
												BigDecimal sequence = purTermDet.getSeq();
												String chargeOperationLevel = purTermDet.getOperationLevel();

												chargeNode.put("seq", sequence);
												chargeNode.put("chgcode", chargeCode);
												chargeNode.put("rate", rate);
												chargeNode.put("formula", formulae);
												chargeNode.put("sign", lineItermSign);
												chargeNode.put("operationLevel", chargeOperationLevel);

												Map<String, Double> subTotalMap = piService
														.calulcateTotalForCgstSgstRates(subTotal, basicValue, cgstRate,
																sgstRate, cessRate, ch1, ch2, ch3, lineItermSign,
																formulae);

												subTotal = subTotalMap.get("subTotal");
												chargeNode.put("chargeAmount", subTotalMap.get("chargeAmount"));
												chargeNode.put("subTotal", subTotal);

												tempNode.put("charges", chargeNode);
												// //calculate charges and basic value using formula ends here

											} else {

												log.info(
														"calculate line item charges for cgst,sgst,cess for maximum amount slab starts here....");

												if (source.equals("G") && (gstComponent.equals("CGST")
														|| gstComponent.equals("SGST") || gstComponent.equals("CESS"))
														&& isTax.equals("Y")) {

													HsnGstData applicableGstSlab = piService
															.getApplicableGstSlabForTransactionPrice(hsnSacCode, piDate,
																	maxAmountFrom);

													log.info("obtained maximum gst slab data is as follows : {}",
															applicableGstSlab);

													String taxName = applicableGstSlab.getTaxName();
													String gstSlab = applicableGstSlab.getSlab();
													String slabEffectiveDate = applicableGstSlab.getEffectiveDate()
															.toString();
													Double cgstRate = 0.0, sgstRate = 0.0, cessRate = 0.0;

													if (gstComponent.equalsIgnoreCase("cgst")) {
														cgstRate = maxEffectiveDate.getCgstRate().doubleValue();
													} else if (gstComponent.equalsIgnoreCase("sgst")) {
														sgstRate = maxEffectiveDate.getSgstRate().doubleValue();
													} else {
														cessRate = maxEffectiveDate.getCessRate().doubleValue();
													}

													// for rates
													tempNode.put("rates", piService.getCgstChargeRateNode(taxName,
															gstSlab, slabEffectiveDate, cgstRate, sgstRate, cessRate));

													nodeObj.put(PurchaseIndentMetaData.PILineItem.GST,
															piService.getGstRateByTaxName(taxName));

													Double ch1 = 0.0, ch2 = 0.0, ch3 = 0.0;

													PurtermDet purTermDet = piService
															.getPurchaseTermDetailsByChgCode(purtermMainCode, chgCode);
													log.info("Charges obtained by list of charges is : {} ",
															purTermDet);

													// for charges
													ObjectNode chargeNode = getMapper().createObjectNode();

													BigDecimal chargeCode = purTermDet.getChgCode();
													String formulae = purTermDet.getFormulae();
													String lineItermSign = purTermDet.getSign();
													BigDecimal rate = purTermDet.getRate();
													BigDecimal sequence = purTermDet.getSeq();
													String chargeOperationLevel = purTermDet.getOperationLevel();

													chargeNode.put("seq", sequence);
													chargeNode.put("chgcode", chargeCode);
													chargeNode.put("rate", rate);
													chargeNode.put("formula", formulae);
													chargeNode.put("sign", lineItermSign);
													chargeNode.put("operationLevel", chargeOperationLevel);

													Map<String, Double> subTotalMap = piService
															.calulcateTotalForCgstSgstRates(subTotal, basicValue,
																	cgstRate, sgstRate, cessRate, ch1, ch2, ch3,
																	lineItermSign, formulae);

													subTotal = subTotalMap.get("subTotal");
													chargeNode.put("chargeAmount", subTotalMap.get("chargeAmount"));
													chargeNode.put("subTotal", subTotal);

													tempNode.put("charges", chargeNode);

													log.info(
															"calculate line item charges for cgst,sgst,cess slab ends here....");
													log.info("subtotal is : {}", subTotal);

													// //calculate charges and basic value using formula ends here
												}

											}
										}

									}
								}
							}

							tempNode.put(PurchaseIndentMetaData.PurtermCharge.SUBTOTAL, subTotal);
							arrNode.add(tempNode);

						}

						grandTotal = basicValue + subTotal;
						log.info("final subtotal for CGST,SGST and CESS obtained is = " + subTotal);

						// commented if condition by mohit on 12 Jul
//					if (subTotal == 0.0)
						if (isGSTApplicable == false)
							nodeObj.put(PurchaseIndentMetaData.PILineItem.GST, 0);

						nodeObj.put(PurchaseIndentMetaData.PILineItem.BASIC_VALUE, basicValue);
						nodeObj.put(PurchaseIndentMetaData.PILineItem.TAX, subTotal);
						nodeObj.put(PurchaseIndentMetaData.PILineItem.NET_AMOUNT, grandTotal);
						nodeObj.put(PurchaseIndentMetaData.PILineItem.FIN_CHARGES, arrNode);
						nodeObj.put("designRowid", node.get("designRowid"));
						nodeObj.put("rowId", node.get("rowId"));

						log.info("Final Object Node is : {}", nodeObj);

						mainArrNode.add(nodeObj);
//						appResponse = new AppResponse.Builder()
//								.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, nodeObj)
//										.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
//								.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

						log.info(AppStatusMsg.SUCCESS_MSG);
					} else {
						// IGST and CESS Rate will be apply on the line item

						log.info("calculating igst and cess....");

						Double subTotal = 0.0;
						double grandTotal = 0.0;

						List<PurtermCharge> purTermChargelList = piService
								.getPurchaseTermChargeDetailsByPurchaseTermCode(purtermMainCode);

						log.info("Charges applicable when gst is IGST for the purchase term are : {},{} ",
								purTermChargelList.size(), purTermChargelList);

						for (PurtermCharge purtermCharge : purTermChargelList) {

							log.info("purtermCharge contains the following details : {}", purtermCharge);
							ObjectNode tempNode = getMapper().createObjectNode();

							int seq = purtermCharge.getPtSeq().intValue();
							Double finChgRate = purtermCharge.getRate().doubleValue();
							String source = purtermCharge.getSource();
							gstComponent = purtermCharge.getGstComponent();
							String isTax = purtermCharge.getIsTax();
							String sign = purtermCharge.getSign();
							String formula = purtermCharge.getPtFormulae();
							String finChgOperationLevel = purtermCharge.getOperationLevel();
							String calculationBasis = purtermCharge.getBasis();
							BigDecimal chgCode = purtermCharge.getChgCode();
							String chgName = purtermCharge.getChgName();

							tempNode.put(PurchaseIndentMetaData.PurtermCharge.CHARGE_CODE, chgCode);
							tempNode.put(PurchaseIndentMetaData.PurtermCharge.CHARGE_NAME, chgName);
							tempNode.put(PurchaseIndentMetaData.PurtermCharge.CHARGE_SEQ, seq);
							tempNode.put(PurchaseIndentMetaData.PurtermCharge.FIN_FORMULA, formula);
							tempNode.put(PurchaseIndentMetaData.PurtermCharge.FIN_CHARGE_RATE, finChgRate);
							tempNode.put(PurchaseIndentMetaData.PurtermCharge.FIN_SOURCE, source);
							tempNode.put(PurchaseIndentMetaData.PurtermCharge.GST_COMPONENT, gstComponent);
							tempNode.put(PurchaseIndentMetaData.PurtermCharge.IS_TAX, isTax);
							tempNode.put(PurchaseIndentMetaData.PurtermCharge.SIGN, sign);
							tempNode.put(PurchaseIndentMetaData.PurtermCharge.FIN_CHARGE_OPERATION_LEVEL,
									finChgOperationLevel);
							tempNode.put(PurchaseIndentMetaData.PurtermCharge.CALCULATION_BASIS, calculationBasis);

//						arrNode.add(tempNode);

							if (gstComponent == null) {

								if (finChgOperationLevel.equals("L")) {

									if (calculationBasis.equals("A")) {

										if (sign.equals("+")) {
											// subTotal = ((subTotal/articleQty) + finChgRate) * articleQty;

											subTotal = subTotal + finChgRate;
											log.info("subtotal : {}", subTotal);
										} else {
											subTotal = subTotal - finChgRate;
											log.info("subtotal : {}", subTotal);
										}

									} else {

										if (sign.equals("+")) {
											double rateAmount = ((basicValue * finChgRate) / 100);
											subTotal = subTotal + rateAmount;
										} else {
											double rateAmount = ((basicValue * finChgRate) / 100);
											subTotal = subTotal - rateAmount;
										}

									}
									tempNode.put("subTotal", subTotal);
									log.info("After applying fincharge rates subtotal is : {} ", subTotal);
								} else {
									log.info("When gst component is null and operation level is header level");
								}
							}

							else if (!CodeUtils.isEmpty(gstComponent)) {
								isGSTApplicable = true;

								if (finChgOperationLevel.equals("L")) {

									if (source.equals("G")
											&& (gstComponent.equals("IGST") || gstComponent.equals("CESS"))
											&& isTax.equals("Y")) {

										// calculating rate for charges starts here
										String hsnSacCode = node.get("hsnSacCode").asText();

										HsnGstData maxEffectiveDate = piService
												.getMaxEffectiveDateByHsnSacCode(hsnSacCode, piDate);

										Timestamp effectiveDate = maxEffectiveDate.getEffectiveDate();
										String slab = maxEffectiveDate.getSlab();

										String piEffectiveDate = new SimpleDateFormat("dd-MMM-yyyy")
												.format(effectiveDate);
										log.info("Maximum effective date obtained is = " + piEffectiveDate);

										if (slab.equals("Without Slab")) {

											String taxName = maxEffectiveDate.getTaxName();
											String gstSlab = maxEffectiveDate.getSlab();
											String slabEffectiveDate = maxEffectiveDate.getEffectiveDate().toString();

											Double igstRate = 0.0, cessRate = 0.0;

											if (gstComponent.equalsIgnoreCase("igst")) {
												igstRate = maxEffectiveDate.getIgstRate().doubleValue();
											} else {
												cessRate = maxEffectiveDate.getCessRate().doubleValue();
											}

											tempNode.put("rates", piService.getIgstChargeRateNode(taxName, gstSlab,
													slabEffectiveDate, igstRate, cessRate));

											nodeObj.put(PurchaseIndentMetaData.PILineItem.GST,
													piService.getGstRateByTaxName(taxName));

											Double ch1 = 0.0, ch2 = 0.0;

											PurtermDet purTermDet = piService
													.getPurchaseTermDetailsByChgCode(purtermMainCode, chgCode);

											ObjectNode chargeNode = getMapper().createObjectNode();

											BigDecimal chargeCode = purTermDet.getChgCode();
											String formulae = purTermDet.getFormulae();
											String lineItermSign = purTermDet.getSign();
											BigDecimal rate = purTermDet.getRate();
											BigDecimal sequence = purTermDet.getSeq();
											String chargeOperationLevel = purTermDet.getOperationLevel();

											chargeNode.put("seq", sequence);
											chargeNode.put("chgcode", chargeCode);
											chargeNode.put("rate", rate);
											chargeNode.put("formula", formulae);
											chargeNode.put("sign", lineItermSign);
											chargeNode.put("operationLevel", chargeOperationLevel);

											Map<String, Double> subTotalMap = piService.calulcateTotalForIgstRates(
													subTotal, basicValue, igstRate, cessRate, ch1, ch2, lineItermSign,
													formulae);

											subTotal = subTotalMap.get("subTotal");

											chargeNode.put("chargeAmount", subTotalMap.get("chargeAmount"));
											chargeNode.put("subTotal", subTotal);

											tempNode.put("charges", chargeNode);

										} else {
											// get HSNGSTDATA using hsn sac code
											List<HsnGstData> hsnGstDataList = piService
													.getHsnGstDataByHsnSacCode(hsnSacCode, piEffectiveDate);

											log.info("Calculating mininum and maxinum AMOUNTFROM for GST SLAB");

											HsnGstData maxAmountFromHsnGstData = piService.getMaxAmountFrom(hsnSacCode,
													piEffectiveDate);
											Double maxAmountFrom = maxAmountFromHsnGstData.getAmountFrom()
													.doubleValue();

											HsnGstData minAmountFromHsnGstData = piService.getMinAmountFrom(hsnSacCode,
													piEffectiveDate);
											Double minAmountFrom = minAmountFromHsnGstData.getAmountFrom()
													.doubleValue();

											log.info("Minimum and Maximum AMOUNTFROM obtained are as follows : {}, {} ",
													minAmountFrom, maxAmountFrom);

											log.info("Transaction Price for item is : {}", transactionPrice);

											if (transactionPrice > minAmountFrom && transactionPrice <= maxAmountFrom) {
												HsnGstData applicableGstSlab = piService
														.getApplicableGstSlabForTransactionPrice(hsnSacCode,
																piEffectiveDate, minAmountFrom);

												log.info("Gst Slab obtained for the minimum amount from is : {} ",
														applicableGstSlab);

												String taxName = applicableGstSlab.getTaxName();
												String gstSlab = applicableGstSlab.getSlab();
												String slabEffectiveDate = applicableGstSlab.getEffectiveDate()
														.toString();
												Double igstRate = 0.0, cessRate = 0.0;

												if (gstComponent.equalsIgnoreCase("igst")) {
													igstRate = applicableGstSlab.getIgstRate().doubleValue();
												} else {
													cessRate = applicableGstSlab.getCessRate().doubleValue();
												}

												tempNode.put("rates",
														tempNode.put("rates", piService.getIgstChargeRateNode(taxName,
																gstSlab, slabEffectiveDate, igstRate, cessRate)));
												nodeObj.put(PurchaseIndentMetaData.PILineItem.GST,
														piService.getGstRateByTaxName(taxName));

												Double ch1 = 0.0, ch2 = 0.0;

												PurtermDet purTermDet = piService
														.getPurchaseTermDetailsByChgCode(purtermMainCode, chgCode);

												log.info("PurchaseTerm charges list is as follows : {}", purTermDet);

												ObjectNode chargeNode = getMapper().createObjectNode();

												BigDecimal chargeCode = purTermDet.getChgCode();
												String formulae = purTermDet.getFormulae();
												String lineItermSign = purTermDet.getSign();
												BigDecimal rate = purTermDet.getRate();
												BigDecimal sequence = purTermDet.getSeq();
												String chargeOperationLevel = purTermDet.getOperationLevel();

												chargeNode.put("seq", sequence);
												chargeNode.put("chgcode", chargeCode);
												chargeNode.put("rate", rate);
												chargeNode.put("formula", formulae);
												chargeNode.put("sign", lineItermSign);
												chargeNode.put("operationLevel", chargeOperationLevel);

												Map<String, Double> subTotalMap = piService.calulcateTotalForIgstRates(
														subTotal, basicValue, igstRate, cessRate, ch1, ch2,
														lineItermSign, formulae);

												subTotal = subTotalMap.get("subTotal");

												chargeNode.put("chargeAmount", subTotalMap.get("chargeAmount"));
												chargeNode.put("subTotal", subTotal);

												tempNode.put("charges", chargeNode);
												// End of switch case

												// //calculate charges and basic value using formula ends here

												// End of foreach loop
												log.info("subtotal is = {}", subTotal);

											} else {
												HsnGstData applicableGstSlab = piService
														.getApplicableGstSlabForTransactionPrice(hsnSacCode,
																piEffectiveDate, maxAmountFrom);

												log.info("Gst Slab obtained for the maximum amount from is : {} ",
														applicableGstSlab);

												String taxName = applicableGstSlab.getTaxName();
												String gstSlab = applicableGstSlab.getSlab();
												String slabEffectiveDate = applicableGstSlab.getEffectiveDate()
														.toString();
												Double igstRate = 0.0, cessRate = 0.0;

												if (gstComponent.equalsIgnoreCase("igst")) {
													igstRate = maxEffectiveDate.getIgstRate().doubleValue();
												} else {
													cessRate = maxEffectiveDate.getCessRate().doubleValue();
												}

												tempNode.put("rates",
														tempNode.put("rates", piService.getIgstChargeRateNode(taxName,
																gstSlab, slabEffectiveDate, igstRate, cessRate)));

												nodeObj.put(PurchaseIndentMetaData.PILineItem.GST,
														piService.getGstRateByTaxName(taxName));

												Double ch1 = 0.0, ch2 = 0.0;

												PurtermDet purTermDet = piService
														.getPurchaseTermDetailsByChgCode(purtermMainCode, chgCode);

												ObjectNode chargeNode = getMapper().createObjectNode();

												BigDecimal chargeCode = purTermDet.getChgCode();
												String formulae = purTermDet.getFormulae();
												String lineItermSign = purTermDet.getSign();
												BigDecimal rate = purTermDet.getRate();
												BigDecimal sequence = purTermDet.getSeq();
												String chargeOperationLevel = purTermDet.getOperationLevel();

												chargeNode.put("seq", sequence);
												chargeNode.put("chgcode", chargeCode);
												chargeNode.put("rate", rate);
												chargeNode.put("formula", formulae);
												chargeNode.put("sign", lineItermSign);
												chargeNode.put("operationLevel", chargeOperationLevel);

												Map<String, Double> subTotalMap = piService.calulcateTotalForIgstRates(
														subTotal, basicValue, igstRate, cessRate, ch1, ch2,
														lineItermSign, formulae);

												subTotal = subTotalMap.get("subTotal");

												chargeNode.put("chargeAmount", subTotalMap.get("chargeAmount"));
												chargeNode.put("subTotal", subTotal);

												tempNode.put("charges", chargeNode);
												// End of switch case

												// End of purchaseTerm foreach loop

											}
										}
									}
								}
							}

							tempNode.put(PurchaseIndentMetaData.PurtermCharge.SUBTOTAL, subTotal);
							arrNode.add(tempNode);
						}

						grandTotal = basicValue + subTotal;
						log.info("final subtotal obtained is = " + subTotal);

						// commented if condition by mohit on 12 Jul
//					if (subTotal == 0.0)
						if (isGSTApplicable == false)
							nodeObj.put(PurchaseIndentMetaData.PILineItem.GST, 0);

						nodeObj.put(PurchaseIndentMetaData.PILineItem.BASIC_VALUE, basicValue);
						nodeObj.put(PurchaseIndentMetaData.PILineItem.TAX, subTotal);
						nodeObj.put(PurchaseIndentMetaData.PILineItem.NET_AMOUNT, grandTotal);
						nodeObj.put(PurchaseIndentMetaData.PILineItem.FIN_CHARGES, arrNode);
						nodeObj.put("designRowid", node.get("designRowid"));
						nodeObj.put("rowId", node.get("rowId"));

						log.info("Final Object Node is : {}", nodeObj);

						mainArrNode.add(nodeObj);
						log.info(AppStatusMsg.SUCCESS_MSG);

					}

				}

			}

			mainObj.put("multipleLineItem", mainArrNode);
			ObjectNode resourceNode = getMapper().createObjectNode();

			resourceNode.put(CodeUtils.RESPONSE, mainObj);

			appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
					.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG).putAll(resourceNode))
					.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

		} catch (MyBatisSystemException ex) {
			log.info(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CONNECTION_ERROR_CODE,
					AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, AppStatusMsg.CONNECTION_NOT_AVAILABLE);

		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.PI_LINE_ITEM_ERROR, appStatus.getMessage(AppModuleErrors.PI_LINE_ITEM_ERROR));

			log.info(ex.getMessage());

		} catch (NullPointerException e) {

			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.PI_LINE_ITEM_ERROR, appStatus.getMessage(AppModuleErrors.PI_LINE_ITEM_ERROR));

			log.info(e.getMessage());
		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			log.info(e.getMessage());
		} finally {
			log.info("PurchaseIndent getLineItemChargesByPurchaseTermCode() method ends here....");
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes", "deprecation", "finally" })
	@RequestMapping(value = "/calculate/multipleactual/markup/article/{data}", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> calculateMultipleInTakeMarginForArticle(@RequestBody ObjectNode multipleNode) {

		AppResponse appResponse = null;
		ObjectNode mainNode;
		ArrayNode arrNode;
		ObjectNode resourceNode = getMapper().createObjectNode();
		boolean result = true;
		try {

			arrNode = getMapper().createArrayNode();
			mainNode = getMapper().createObjectNode();
			for (JsonNode node : multipleNode.get("multipleMarkup")) {

				Map<String, Double> map = piService.calculateInTakeMarginForArticleData((ObjectNode) node);

				if (!CodeUtils.isEmpty(map)) {
					ObjectNode objNode = getMapper().createObjectNode();
					objNode.put("rowId", node.get("rowId"));
					objNode.put("designRowid", node.get("designRowid"));

					objNode.put(PurchaseIndentMetaData.Item.ACTUALMARKUP,
							map.get(PurchaseIndentMetaData.Item.ACTUALMARKUP));
					objNode.put(PurchaseIndentMetaData.Item.CALCULATEDMARGIN,
							map.get(PurchaseIndentMetaData.Item.CALCULATEDMARGIN));

					arrNode.add(objNode);
				} else {
					result = false;
					break;
				}

			}

			if (result == true) {

				mainNode.put("multipleMarkup", arrNode);
				resourceNode.put(CodeUtils.RESPONSE, mainNode);

				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
						.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG).putAll(resourceNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

				log.info(AppStatusMsg.SUCCESS_MSG);
			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
						AppStatusMsg.FAILURE_MSG);
			}

		} catch (NullPointerException e) {
			appResponse = new AppResponse.Builder().data(
					getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null).put(CodeUtils.RESPONSE_MSG, ""))
					.status(AppStatusMsg.BLANK).build();
			log.info(e.getMessage());
		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.PI_ACTUAL_MARKUP_FAILURE);
			log.info(e.getMessage());
		} finally {

			log.info("PurchaseIndent calculateNetAmountForArticleData() method ends here....");
			return ResponseEntity.ok(appResponse);
		}
	}
	
	
	@SuppressWarnings("finally")
	@RequestMapping(value = "/get/all", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getArticleCode(@RequestParam("hl3Code") String hl3Code,@RequestParam("type") Integer type,
			@RequestParam("pageNo") Integer pageNo, @RequestParam("search") String search) {
		AppResponse appResponse = null;
		List<Map<String,String>> data =null;
		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;
		try {
			if (type == 1) {
				totalRecord = piService.recordDistinctArticle(hl3Code);

				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;

				 data = piService.getByDepartmentCode(hl3Code, offset, pageSize);
			}else {

				if (type == 3 && !search.isEmpty()) {

					totalRecord = piService.searchRecordDistinctArticle(search);

					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;

					 data = piService.searchByDepartmentCode(offset, pageSize, search);

				}
           	  }
			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put("currPage", pageNo)
								.put("prePage", previousPage).put("maxPage", maxPage)
								.putPOJO(CodeUtils.RESPONSE, data)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						        .status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				
			}
		} catch (NumberFormatException ex) {

			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.NUMBER_FORMAT_MSG);

		} catch (DataAccessException ex) {

			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.VENDOR_READER_ERROR, appStatus.getMessage(AppModuleErrors.VENDOR_READER_ERROR));
			
		} catch (Exception ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			throw new SupplyMintException(AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}


}
