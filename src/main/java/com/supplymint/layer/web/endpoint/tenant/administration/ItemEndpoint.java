
package com.supplymint.layer.web.endpoint.tenant.administration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.supplymint.exception.StatusCodes.AppStatusCodes;
import com.supplymint.layer.business.service.procurement.ItemService;
import com.supplymint.layer.data.tenant.administration.entity.ADMItem;
import com.supplymint.layer.web.endpoint.abstracts.AbstractEndpoint;
import com.supplymint.layer.web.model.AppResponse;

/**
 * @Authhor Manoj Singh
 * @Date 24-Sep-2018
 * @Version 1.0
 */

@RestController
@RequestMapping(path = ItemEndpoint.PATH, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {
		MediaType.APPLICATION_JSON_VALUE })
public class ItemEndpoint extends AbstractEndpoint {

	public static final String PATH = "admin/items";

	private ItemService itemService;

	@Autowired
	public ItemEndpoint(ItemService itemService) {
		this.itemService = itemService;
	}

	@RequestMapping(value = "id/{id}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getByIdentifierAndModule(@PathVariable(value = "id") Integer id) {

		ADMItem item = itemService.getItemById(id);
		AppResponse appResponse = new AppResponse.Builder()
				.data(getMapper().createArrayNode().add(getMapper().valueToTree(item)))
				.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

		return ResponseEntity.ok(appResponse);
	}
}
