package com.supplymint.layer.web.endpoint.tenant.demand;

import java.net.InetAddress;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import org.rosuda.REngine.REXPMismatchException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.CannotGetJdbcConnectionException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.supplymint.config.application.AppCodeConfig;
import com.supplymint.config.aws.utils.AWSUtils;
import com.supplymint.config.aws.utils.AWSUtils.Conditions;
import com.supplymint.config.aws.utils.AWSUtils.RunState;
import com.supplymint.config.aws.utils.AWSUtils.Status;
import com.supplymint.exception.StatusCodes.AppModuleErrors;
import com.supplymint.exception.StatusCodes.AppStatusCodes;
import com.supplymint.exception.StatusCodes.AppStatusMsg;
import com.supplymint.exception.StatusCodes.ClientErrors;
import com.supplymint.exception.StatusCodes.FileStatusCode;
import com.supplymint.exception.StatusCodes.Pagination;
import com.supplymint.exception.SupplyMintException;
import com.supplymint.layer.business.contract.demand.DemandPlanningService;
import com.supplymint.layer.business.contract.notification.EmailService;
import com.supplymint.layer.business.contract.notification.MailManagerService;
import com.supplymint.layer.business.contract.s3Bucket.S3BucketInfoService;
import com.supplymint.layer.business.contract.setting.GeneralSettingService;
import com.supplymint.layer.business.service.rconnection.RServeEngineProviderService;
import com.supplymint.layer.business.service.s3bucketService.S3WrapperService;
import com.supplymint.layer.data.config.ThreadLocalStorage;
import com.supplymint.layer.data.notification.entity.MailManager;
import com.supplymint.layer.data.notification.entity.MailManagerViewLog;
import com.supplymint.layer.data.tenant.demand.entity.BudgetedDemandPlanning;
import com.supplymint.layer.data.tenant.demand.entity.BudgetedTemporary;
import com.supplymint.layer.data.tenant.demand.entity.DemandForecastScheduler;
import com.supplymint.layer.data.tenant.demand.entity.DemandGraph;
import com.supplymint.layer.data.tenant.demand.entity.DemandPlanning;
import com.supplymint.layer.data.tenant.demand.entity.DemandPlanningViewLog;
import com.supplymint.layer.data.tenant.demand.entity.DpUploadSummary;
import com.supplymint.layer.data.tenant.inventory.entity.S3BucketInfo;
import com.supplymint.layer.web.endpoint.abstracts.AbstractEndpoint;
import com.supplymint.layer.web.filters.TenantFilter;
import com.supplymint.layer.web.model.AppResponse;
import com.supplymint.util.ApplicationUtils.BucketParameter;
import com.supplymint.util.ApplicationUtils.CustomParameter;
import com.supplymint.util.ApplicationUtils.CustomRunState;
import com.supplymint.util.ApplicationUtils.DemandPlanning.ChooseType;
import com.supplymint.util.ApplicationUtils.DemandPlanning.DemandPlanningGraph;
import com.supplymint.util.ApplicationUtils.DemandPlanningParameter;
import com.supplymint.util.ApplicationUtils.EmailTemplateName;
import com.supplymint.util.ApplicationUtils.FiscalYear;
import com.supplymint.util.ApplicationUtils.Module;
import com.supplymint.util.ApplicationUtils.SubModule;
import com.supplymint.util.CodeUtils;
import com.supplymint.util.CodeUtils.Demand;
import com.supplymint.util.FileUtils;
import com.supplymint.util.FiscalDate;
import com.supplymint.util.UploadUtils;

/**
 * @author Manoj Singh
 * @author Prabhakar Srivastava
 * @author Bishnu Dutta
 * @since 03 JAN 2019
 */
@RestController
@RequestMapping(path = DemandPlanningEndpoint.PATH, produces = { MediaType.APPLICATION_JSON_VALUE })
public class DemandPlanningEndpoint extends AbstractEndpoint {

	private final static Logger LOGGER = LoggerFactory.getLogger(DemandPlanningEndpoint.class);

	public static final String PATH = "demand/plan";

	private DemandPlanningService demandPlanningService;

	public String fileName = null;

	String newDateFormat = null;

	private String tenantHashkey;

	private String upnDc;

	private List<String> storeBudgetedSalesStatus;

	private int successCode;

	@Autowired
	private RServeEngineProviderService rServeEngineProviderService;

	@Autowired
	private EmailService emailService;

	@Autowired
	private MailManagerService mailManagerService;

	@Autowired
	private AppCodeConfig appStatus;

	@Autowired
	private HttpServletRequest servletRequest;

	@Autowired
	private TenantFilter tenantFilter;

	@Autowired
	private S3BucketInfoService s3BucketInfoService;

	@Autowired
	private GeneralSettingService generalSettingService;

	@Autowired
	private S3WrapperService s3Wrapper;

	@Autowired
	public DemandPlanningEndpoint(DemandPlanningService demandPlanningService) {
		this.demandPlanningService = demandPlanningService;
	}

	@SuppressWarnings({ "unused", "deprecation" })
	@RequestMapping(value = "/forecast/ondemand", method = RequestMethod.POST)
	public ResponseEntity<AppResponse> foreCastOnDemand(@RequestBody DemandPlanning demandPlanning,
			ServletRequest request) {
		String token = ((HttpServletRequest) request).getHeader("X-Auth-Token");
		tenantHashkey = ThreadLocalStorage.getTenantHashKey();

		String bucketKey = "/DEMAND_PLANNER/" + UUID.randomUUID().toString();
		String bucketName = CodeUtils.decode(token).get(FileUtils.BucketName).getAsString();
//		String enterpriseType = CodeUtils.decode(token).get("entType").getAsString();
		String enterpriseType = "RETAIL_MARKET";
		AppResponse appResponse = null;
		String duration = null;
		int result = 0;
		long diffMinutes = 0L;
		int updateSummary = 0;
		Map<String, Map<String, String>> assortmentPattern = new HashMap<String, Map<String, String>>();
		Map<String, String> forecastedAssortment = new HashMap<>();
		String startDate = CodeUtils.convertDemandForecastMonthlyDateFormat(
				demandPlanning.getChooseYear() + CustomParameter.HYPHAN + demandPlanning.getStartMonth());

		String startedOn = demandPlanning.getChooseYear() + CustomParameter.HYPHAN + demandPlanning.getStartMonth()
				+ CustomParameter.HYPHAN + "01";

		String forecastDate = CodeUtils.__dateFormat
				.format(CodeUtils.addMonth(new Date(CodeUtils.convertDataReferenceDateFormat(startedOn)),
						Integer.parseInt(demandPlanning.getPredictPeriod()) - 1));

		int validationCreate = 0;
		String statusMessage = null;
		try {
			JsonArray orgDetails=CodeUtils.decode(token).get("ORG_DETAILS").getAsJsonArray();
			JsonObject jNode=orgDetails.get(0).getAsJsonObject();
			String orgId = jNode.get("orgId").getAsString();
			
			MailManager mailManager = new MailManager();
			S3BucketInfo s3BucketInfo = s3BucketInfoService.getBucketByType(BucketParameter.GENERATE_EXCELCSV,orgId);
			Date createdTime = new Date();
			Date creationTime = CodeUtils.convertDateOnTimeZonePluseForAWS(createdTime, 5, 30);
			String ipAddress = InetAddress.getLocalHost().getHostAddress().trim().toString();
			String runId = null;
			String actualPattern = null;
			MailManagerViewLog mailManagerViewLog = new MailManagerViewLog();
			actualPattern = demandPlanningService.getRunIdPattern();
			if (actualPattern == null) {
				runId = DemandPlanningParameter.PATTERN;
			} else {
				runId = CodeUtils.createPattern(actualPattern);
			}
			LOGGER.debug("Successfully get runId is : %s", runId);

			demandPlanning.setStartedOn(CodeUtils.convertDemandForecastDateFormat(startedOn));
			demandPlanning.setDemandForecast(DemandPlanningGraph.DEMAND_ON_FORECAST.toString());
			demandPlanning.setIpAddress(ipAddress);
			demandPlanning.setCreatedOn(creationTime);
			demandPlanning.setRunId(runId);
			demandPlanning.setStatus(CustomRunState.Processing.toString());
			demandPlanning.setForecastCode(AppStatusCodes.GENERIC_PROCESSING_CODE);
			demandPlanning.setDbReadStatus(Conditions.FALSE.toString());
			demandPlanning.setSummary(Conditions.FALSE.toString());
			demandPlanning.setCreatedBy(servletRequest.getAttribute("email").toString());

			LOGGER.info("validate only 1 processing job is created...");
			validationCreate = demandPlanningService.validationCreate();
			if (validationCreate == 0) {
				LOGGER.info("creating a job with processing status");
				result = demandPlanningService.create(demandPlanning);
				LOGGER.info("update MVStatus with InProgress for demand Planning");
				int updateMVStatus = demandPlanningService.updateMVStatus(RunState.INPROGRESS.toString(), '0',
						Conditions.TRUE.toString());
			} else {
				throw new SupplyMintException();
			}
			upnDc = tenantFilter.getTenantConfigProperty().get(tenantHashkey);
			LOGGER.debug(String.format("Configuring script with datasource configuration for user : %s", upnDc));

			LOGGER.info("Calling executor service");
			ExecutorService executorService = Executors.newSingleThreadExecutor();
			executorService.execute(new Runnable() {
				@Override
				public void run() {
					try {
						String frequency = null;
						String period = null;
						String[] to = null;
						String[] cc = null;
						String forcastOnAssortmentCode = null;
						MailManager mailManager = new MailManager();
						if (!demandPlanning.getFrequency().equalsIgnoreCase("MONTHLY")) {
							frequency = "Weeks";
							period = "Weekly";

						} else {
							frequency = "Months";
							period = "Monthly";
						}

						String forecastFor = demandPlanning.getPredictPeriod() + " " + frequency;
						boolean isSendEmail = false;
						ThreadLocalStorage.setTenantHashKey(tenantHashkey);
						

						LOGGER.debug(
								"Passing down the configuration property to load the script with following attributes User :%s  Frequency :%s Predict Period :%s Start Pattern :%s",
								upnDc, demandPlanning.getFrequency(), demandPlanning.getPredictPeriod(), startedOn);
						successCode = rServeEngineProviderService.establishConnection(enterpriseType, upnDc,
								demandPlanning.getRunId(), demandPlanning.getFrequency(),
								demandPlanning.getPredictPeriod(), startedOn, bucketName, bucketKey);
						LOGGER.info("Waiting for forecast result ..");
						// Code to update the status based on success code after execution result of
						// rscript.
						LOGGER.debug(String.format("Get SUCCESS CODE is : %s", successCode));
						if (successCode == 200) {
							String runId = demandPlanningService.getRunIdPattern();
							DemandPlanning data = demandPlanningService.getByRunId(runId);
//							String url = demandPlanningService.getForecastUrl(startedOn, forecastDate,
//									demandPlanning.getFrequency(),
//									Integer.parseInt(demandPlanning.getPredictPeriod()) - 1, runId);
							int updateSummary = demandPlanningService.updateSummary(Conditions.TRUE.toString(), runId);
							int updatePreviousSummary = demandPlanningService.updatePreviousSummary(runId);
							Date createdTime = data.getCreatedOn();
							Date updateOn = new Date();
							Date updationTime = CodeUtils.convertDateOnTimeZonePluseForAWS(updateOn, 5, 30);
							long difference = updationTime.getTime() - createdTime.getTime();
							long diffMinutes = difference / (60 * 1000) % 60;
							String duration = Long.toString(diffMinutes);
							LOGGER.info("update download url with status");
							int updateDownloadUrl = demandPlanningService.updateDownloadUrl(bucketKey,
									BucketParameter.BUCKETPREFIX + bucketName + bucketKey + CustomParameter.DELIMETER,
									runId);
							forcastOnAssortmentCode = demandPlanningService.getForcastOnAssortmentCode();

							forecastedAssortment.put("assortmentCode", forcastOnAssortmentCode);
							forecastedAssortment.put("timeAgo", CodeUtils.getTimeAgo(updationTime.getTime()));
							forecastedAssortment.put("createdOn", CodeUtils._____dateFormat.format(updationTime));
							forecastedAssortment.put("startDate", startedOn);
							forecastedAssortment.put("endDate", forecastDate);

							assortmentPattern.put(runId, forecastedAssortment);
							int updateCodeWithStatus = demandPlanningService.updateCodeWithStatus(successCode,
									CustomRunState.Succeeded.toString(), runId, duration, updationTime,
									getMapper().writeValueAsString(assortmentPattern));
							
							LOGGER.info("Start refresh demand assortment monthly and weekly MV");
							demandPlanningService.refreshActualDataMView();
							int result = mailManagerService.getEmailStatus(orgId);
							if (result != 0) {
								mailManager = mailManagerService.getByStatus(
										EmailTemplateName.FORECAST_SUCCESS_GENERATION.toString(),
										CustomRunState.SUCCESS.toString(),orgId);
								SimpleMailMessage mailMessage = new SimpleMailMessage();
								if (!CodeUtils.isEmpty(mailManager)) {
									if (mailManager.getCc() != null) {
										cc = mailManager.getCc().trim().split(",");
									}
									if (mailManager.getTo() != null) {
										to = mailManager.getTo().trim().split(",");
									}
									mailMessage.setCc(cc);
									mailMessage.setTo(to);
									mailMessage.setBcc(mailManager.getBcc());
									mailMessage.setSubject(mailManager.getSubject());

									MailManagerViewLog mailManagerViewLog = new MailManagerViewLog();
									mailManagerViewLog.setCc(mailManager.getCc());
									mailManagerViewLog.setBcc(mailManager.getBcc());
									mailManagerViewLog.setTo(mailManager.getTo());
									mailManagerViewLog.setCreatedOn(updationTime);
									mailManagerViewLog.setDeliveredOn(updationTime);
									mailManagerViewLog
											.setTemplateName(EmailTemplateName.FORECAST_SUCCESS_GENERATION.toString());
									mailManagerViewLog.setModule(Module.DEMAND_PLANNING.toString());
									mailManagerViewLog.setSubModule(SubModule.FORECAST_ON_DEMAND.toString());
									mailManagerViewLog.setIpAddress(ipAddress);

									String updatedOn = CodeUtils._____dateFormat
											.format(CodeUtils.convertDateOnTimeZonePluseForAWS(updationTime, 5, 30));

									// arguments seprate with comma's like "string1","string2"
									if (!CodeUtils.isEmpty(mailManager)) {
										String savedPath = bucketKey + CustomParameter.DELIMETER;
										String url = s3Wrapper.downloadPreSignedURLOnHistory(
												s3BucketInfo.getS3bucketName(),
												savedPath.substring(1, savedPath.length()),
												Integer.parseInt(s3BucketInfo.getTime()));
										LOGGER.info("success email sending with is in process..");
										emailService.sendEmailUsingMailTemplate(mailMessage,
												mailManager.getTemplateName(), period, forecastFor, startDate,
												s3BucketInfo.getEnterprise(), runId, updatedOn, url);
										LOGGER.info("success email has been send");
										isSendEmail = true;
										if (isSendEmail) {
											mailManagerViewLog.setStatus(RunState.SUCCEEDED.toString());
											int createLog = mailManagerService
													.createEmailActivityLog(mailManagerViewLog,orgId);
										}
									}
								}
							}
							try {
								demandPlanningService.refreshMView();
							} catch (Exception ex) {

							}
						} else if (successCode == 500) {
							String runId = demandPlanningService.getRunIdPattern();
							DemandPlanning data = demandPlanningService.getByRunId(runId);
							Date createdTime = data.getCreatedOn();
							Date updateOn = new Date();
							Date updationTime = CodeUtils.convertDateOnTimeZonePluseForAWS(updateOn, 5, 30);
							long difference = updationTime.getTime() - createdTime.getTime();
							long diffMinutes = difference / (60 * 1000) % 60;
							String duration = Long.toString(diffMinutes);
							LOGGER.info("update MVStatus with Succeeded for demand Planning");
							int updateMVStatus = demandPlanningService.updateMVStatus(RunState.SUCCEEDED.toString(),
									'1', Conditions.TRUE.toString());
//							int updateDownloadKey=demandPlanningService.updateDownloadKey(bucketName,bucketKey);
							int updateCodeWithStatus = demandPlanningService.updateCodeWithStatus(successCode,
									CustomRunState.Failed.toString(), runId, duration, updationTime,
									forcastOnAssortmentCode);

							int result = mailManagerService.getEmailStatus(orgId);
							if (result != 0) {
								mailManager = mailManagerService.getByStatus(
										EmailTemplateName.FORECAST_FAILED_GENERATION.toString(),
										CustomRunState.FAILED.toString(),orgId);
								SimpleMailMessage mailMessage = new SimpleMailMessage();
								if (!CodeUtils.isEmpty(mailManager)) {
									if (mailManager.getCc() != null) {
										cc = mailManager.getCc().trim().split(",");
									}
									if (mailManager.getTo() != null) {
										to = mailManager.getTo().trim().split(",");
									}
									mailMessage.setCc(cc);
									mailMessage.setTo(to);
									mailMessage.setBcc(mailManager.getBcc());
									mailMessage.setSubject(mailManager.getSubject());

									MailManagerViewLog mailManagerViewLog = new MailManagerViewLog();
									mailManagerViewLog.setCc(mailManager.getCc());
									mailManagerViewLog.setBcc(mailManager.getBcc());
									mailManagerViewLog.setTo(mailManager.getTo());
									mailManagerViewLog.setCreatedOn(updationTime);
									mailManagerViewLog.setDeliveredOn(updationTime);
									mailManagerViewLog
											.setTemplateName(EmailTemplateName.FORECAST_FAILED_GENERATION.toString());
									mailManagerViewLog.setModule(Module.DEMAND_PLANNING.toString());
									mailManagerViewLog.setSubModule(SubModule.FORECAST_ON_DEMAND.toString());
									mailManagerViewLog.setIpAddress(ipAddress);

									String updatedOn = CodeUtils._____dateFormat
											.format(CodeUtils.convertDateOnTimeZonePluseForAWS(updationTime, 5, 30));

									// arguments seprate with comma's like "string1","string2"
									if (!CodeUtils.isEmpty(mailManager)) {
										String url = null;
										LOGGER.info("failed email sending with is in process..");
										emailService.sendEmailUsingMailTemplate(mailMessage,
												mailManager.getTemplateName(), period, forecastFor, startDate,
												s3BucketInfo.getEnterprise(), runId, updatedOn, url);
										LOGGER.info("failed email has been send");
										isSendEmail = true;
										if (isSendEmail) {
											mailManagerViewLog.setStatus(RunState.SUCCEEDED.toString());
											int createLog = mailManagerService
													.createEmailActivityLog(mailManagerViewLog,orgId);
										}
									}
								}
							}

						}
					} catch (REXPMismatchException e) {
						String forcastOnAssortmentCode = null;
						String runId = demandPlanningService.getRunIdPattern();
						DemandPlanning data = demandPlanningService.getByRunId(runId);
						Date createdTime = data.getCreatedOn();
						Date updateOn = new Date();
						Date updationTime = CodeUtils.convertDateOnTimeZonePluseForAWS(updateOn, 5, 30);
						long difference = updationTime.getTime() - createdTime.getTime();
						long diffMinutes = difference / (60 * 1000) % 60;
						String duration = Long.toString(diffMinutes);
						LOGGER.info("update status with mismatch Exception");
						int updateCodeWithStatus = demandPlanningService.updateCodeWithStatus(500,
								"REXPMISMATCHEXCEPTION", runId, duration, updationTime, forcastOnAssortmentCode);
						int updateMVStatus = demandPlanningService.updateMVStatus(RunState.SUCCEEDED.toString(), '1',
								Conditions.TRUE.toString());
						e.printStackTrace();
					} catch (Exception e) {
						String isFrequency = null;
						String isPeriod = null;
						String[] to = null;
						String[] cc = null;
						String forcastOnAssortmentCode = null;
						LOGGER.info("update status with Exception Block");
						int updateMVStatus = demandPlanningService.updateMVStatus("SUCCEEDED_VIEWED", '1',
								Conditions.TRUE.toString());
						MailManager mailManager = new MailManager();
						if (!demandPlanning.getFrequency().equalsIgnoreCase("MONTHLY")) {
							isFrequency = "Weeks";
							isPeriod = "Weekly";

						} else {
							isFrequency = "Months";
							isPeriod = "Monthly";
						}
						String forecastFor = demandPlanning.getPredictPeriod() + " " + isFrequency;
						boolean isSendEmail = false;

						String runId = demandPlanningService.getRunIdPattern();
						DemandPlanning data = demandPlanningService.getByRunId(runId);
						Date createdTime = data.getCreatedOn();
						Date updateOn = new Date();
						Date updationTime = CodeUtils.convertDateOnTimeZonePluseForAWS(updateOn, 5, 30);
						long difference = updationTime.getTime() - createdTime.getTime();
						long diffMinutes = difference / (60 * 1000) % 60;
						String duration = Long.toString(diffMinutes);
						OffsetDateTime updatedOn = OffsetDateTime.now();
						int updateCodeWithStatus = demandPlanningService.updateCodeWithStatus(500,
								CustomRunState.Failed.toString(), runId, duration, updationTime,
								forcastOnAssortmentCode);
						e.printStackTrace();

						int result = mailManagerService.getEmailStatus(orgId);
						if (result != 0) {
							mailManager = mailManagerService.getByStatus(
									EmailTemplateName.FORECAST_FAILED_GENERATION.toString(),
									CustomRunState.FAILED.toString(),orgId);
							SimpleMailMessage mailMessage = new SimpleMailMessage();
							if (!CodeUtils.isEmpty(mailManager)) {
								if (mailManager.getCc() != null) {
									cc = mailManager.getCc().trim().split(",");
								}
								if (mailManager.getTo() != null) {
									to = mailManager.getTo().trim().split(",");
								}
								mailMessage.setCc(cc);
								mailMessage.setTo(to);
								mailMessage.setBcc(mailManager.getBcc());
								mailMessage.setSubject(mailManager.getSubject());

								MailManagerViewLog mailManagerViewLog = new MailManagerViewLog();
								mailManagerViewLog.setCc(mailManager.getCc());
								mailManagerViewLog.setBcc(mailManager.getBcc());
								mailManagerViewLog.setTo(mailManager.getTo());
								mailManagerViewLog.setCreatedOn(updationTime);
								mailManagerViewLog.setDeliveredOn(updationTime);
								mailManagerViewLog
										.setTemplateName(EmailTemplateName.FORECAST_FAILED_GENERATION.toString());
								mailManagerViewLog.setModule(Module.DEMAND_PLANNING.toString());
								mailManagerViewLog.setSubModule(SubModule.FORECAST_ON_DEMAND.toString());
								mailManagerViewLog.setIpAddress(ipAddress);

								String updateTime = CodeUtils._____dateFormat
										.format(CodeUtils.convertDateOnTimeZonePluseForAWS(updateOn, 5, 30));

								// arguments seprate with comma's like "string1","string2"
								if (!CodeUtils.isEmpty(mailManager)) {
									String url = null;
									LOGGER.info(
											"failed forecast email sending with is in process from exception Block..");
									emailService.sendEmailUsingMailTemplate(mailMessage, mailManager.getTemplateName(),
											isPeriod, forecastFor, startDate, s3BucketInfo.getEnterprise(), runId,
											updateTime, url);
									isSendEmail = true;
									LOGGER.info("failed forecast email has been send");
									if (isSendEmail) {
										mailManagerViewLog.setStatus(RunState.SUCCEEDED.toString());
										int createLog = mailManagerService.createEmailActivityLog(mailManagerViewLog,orgId);
									}
								}
							}
						}
					}
				}
			});

			if (result != 0) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, demandPlanning)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.FORECAST_SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			}

		} catch (SupplyMintException ex) {
			LOGGER.debug(String.format("Error Occoured From SupplyMintException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FORECAST_ALREADY_QUEUE);
		} catch (Exception ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
		}
		return ResponseEntity.status(HttpStatus.OK).body(appResponse);
	}

	@SuppressWarnings({ "deprecation", "finally" })
	@RequestMapping(value = "/custom/get/all", method = RequestMethod.GET)
	@ResponseBody

	public ResponseEntity<AppResponse> getAll(@RequestParam("pageNo") Integer pageNo,
			@RequestParam("type") Integer type, @RequestParam("frequency") String frequency,
			@RequestParam("predictPeriod") String predictPeriod, @RequestParam("status") String status,
			@RequestParam("search") String search) {

		AppResponse appResponse = null;
		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;
		List<DemandPlanning> data = null;
		DemandPlanning demandPlanning = new DemandPlanning();
		try {

			if (type == 1) {
				LOGGER.info("retrive forecast history get all");
				totalRecord = demandPlanningService.record();
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo - 1) * pageSize;
				data = demandPlanningService.getAll(offset, pageSize);
				LOGGER.info("obtaining forecast history get all");
			} else if (type == 2) {
				LOGGER.info("retrive forecast history filter");
				demandPlanning.setOffset(offset);
				demandPlanning.setPageSize(pageSize);
				demandPlanning.setFrequency(frequency);
				demandPlanning.setPredictPeriod(predictPeriod);
				demandPlanning.setStatus(status);
				totalRecord = demandPlanningService.filterRecord(demandPlanning);
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo - 1) * pageSize;
				data = demandPlanningService.filter(demandPlanning);
				LOGGER.info("obtaining forecast history filter");
			} else if (type == 3) {
				if (!search.isEmpty()) {
					LOGGER.info("retrive forecast history search");
					totalRecord = demandPlanningService.countSearchRecord(search);
					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo - 1) * pageSize;
					data = demandPlanningService.search(offset, pageSize, search);
					LOGGER.info("obtaining forecast history search");
				}
			}

			ObjectNode objectNode = getMapper().createObjectNode();
			ArrayNode array = getMapper().createArrayNode();
			data.forEach(e -> {
				ObjectNode node = getMapper().valueToTree(e);
				array.add(node);
			});
			objectNode.put(CodeUtils.RESPONSE, array);

			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(Pagination.CURRENT_PAGE, pageNo)
								.put(Pagination.PREVIOUS_PAGE, previousPage).put(Pagination.MAXIMUM_PAGE, maxPage)
								.put(CodeUtils.RESPONSE_MSG, "").putAll(objectNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

				LOGGER.info(AppStatusMsg.BLANK);
			}

		} catch (CannotGetJdbcConnectionException ex) {
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.DEMAND_PLANNING_READER_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			ex.printStackTrace();
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.DEMAND_PLANNING_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.DEMAND_PLANNING_READER_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception ex) {
			ex.printStackTrace();
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "deprecation", "finally" })
	@RequestMapping(value = "/get/assortment", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getAssortment(@RequestParam("pageNo") Integer pageNo,
			@RequestParam("type") Integer type, @RequestParam("totalCount") Integer totalCount,
			@RequestParam("search") String search) {
		AppResponse appResponse = null;
		List<String> data = null;
		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;
		try {

			if (type == 1) {
				LOGGER.debug("retrive the distinct assortment get all");
				if (totalCount == 0) {
					totalRecord = demandPlanningService.assortmentRecord();
				} else {
					totalRecord = totalCount * 10;
				}
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo - 1) * pageSize;

				data = demandPlanningService.getAssortment(offset, pageSize);
				LOGGER.debug("obtaining the distinct assortment get all %d", data.size());
			} else if (type == 3) {
				if (!search.isEmpty()) {
					LOGGER.debug("retrive the distinct assortment search");
					if (totalCount == 0) {
						totalRecord = demandPlanningService.assortmentSearchRecord(search);
					} else {
						totalRecord = totalCount * 10;
					}
					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo - 1) * pageSize;
					data = demandPlanningService.assortmentSearch(offset, pageSize, search);
					LOGGER.debug("obtaining the distinct assortment search %d", data.size());
				}
			}

			ObjectNode objectNode = getMapper().createObjectNode();
			objectNode.putPOJO(CodeUtils.RESPONSE, data);

			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(Pagination.CURRENT_PAGE, pageNo)
								.put(Pagination.PREVIOUS_PAGE, previousPage).put(Pagination.MAXIMUM_PAGE, maxPage)
								.put(CodeUtils.RESPONSE_MSG, "").putAll(objectNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				LOGGER.info(AppStatusMsg.BLANK);
			}
		} catch (CannotGetJdbcConnectionException ex) {
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.DEMAND_PLANNING_READER_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.DEMAND_PLANNING_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.DEMAND_PLANNING_READER_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally", "unused", "deprecation" })
	@RequestMapping(value = "/upload/budgeted/data", method = RequestMethod.POST, consumes = {
			MediaType.MULTIPART_FORM_DATA_VALUE })
	@ResponseBody
	public ResponseEntity<AppResponse> graphBudgetedSale(@RequestParam("fileData") MultipartFile fileData,
			@RequestParam("type") String type, @RequestParam("userName") String userName, ServletRequest request) {

		AppResponse appResponse = null;
		long hours = 5;
		long minutes = 30;
		String token = ((HttpServletRequest) request).getHeader("X-Auth-Token");
		tenantHashkey = ThreadLocalStorage.getTenantHashKey();
		try {
			
			
			JsonArray orgDetails=CodeUtils.decode(token).get("ORG_DETAILS").getAsJsonArray();
			
			JsonObject jNode=orgDetails.get(0).getAsJsonObject();
			String orgId = jNode.get("orgId").getAsString();
			String fileLocation = null;
			storeBudgetedSalesStatus = new ArrayList<String>();
			// Previous Code

//			File tmpFile = new File(System.getProperty("java.io.tmpdir") + System.getProperty("file.separator")
//					+ fileData.getOriginalFilename());

//			fileData.transferTo(tmpFile);

//			fileName = tmpFile.getPath();

			LOGGER.debug(String.format("Reading file from file location : %s", fileData.getOriginalFilename()));
			List<BudgetedTemporary> getExcelData = demandPlanningService.validDataInExcelFile(fileData.getInputStream(),
					userName, type);
			LOGGER.debug("Returned Excel data will validation type");
			int rowCount = getExcelData.size();
			if (rowCount != 0) {
				OffsetDateTime currentTime = OffsetDateTime.now();
				currentTime = currentTime.plusHours(hours);
				currentTime = currentTime.plusMinutes(minutes);
				String name = fileData.getOriginalFilename();
				DpUploadSummary dpUploadSummary = new DpUploadSummary();
				String ipAddress = InetAddress.getLocalHost().getHostAddress().trim().toString();
				dpUploadSummary.setCreatedTime(currentTime);
				String id = UUID.randomUUID().toString();
				dpUploadSummary.setId(id);
				dpUploadSummary.setType(type);
				dpUploadSummary.setIpAddress(ipAddress);
				dpUploadSummary.setAttachUrl(fileLocation);
				dpUploadSummary.setFileName(name);
				dpUploadSummary.setDemandType(Demand.BUDGETED.toString());
				dpUploadSummary.setRowCount(rowCount);
				dpUploadSummary.setStatus(Status.UPLOADED.toString());
				dpUploadSummary.setCreatedBy(servletRequest.getAttribute("email").toString());
				dpUploadSummary.setLastSummary("true");
				dpUploadSummary.setUserName(userName);

				demandPlanningService.insertDpUploadSummary(dpUploadSummary);
				demandPlanningService.updateBudgetedSummary(userName, id);
				storeBudgetedSalesStatus.add(Status.UPLOADED.toString());
				ExecutorService executorService = Executors.newSingleThreadExecutor();
				executorService.execute(new Runnable() {
					@Override
					public void run() {
						ThreadLocalStorage.setTenantHashKey(tenantHashkey);
						List<BudgetedDemandPlanning> budgetedData = null;
						String status = null;
						try {

							if (type.equalsIgnoreCase(ChooseType.WEEKLY.toString())) {
								demandPlanningService.updateLastDemandBudgetedWeeklyData(userName);
							} else if (type.equalsIgnoreCase(ChooseType.MONTHLY.toString())) {
								demandPlanningService.updateLastDemandBudgeteMonthlyData(userName);
							}
							demandPlanningService.truncateTempBudgetedData(userName);

							LOGGER.info(String.format(
									"Inserting Budgeted data into demand planing budgeted sales table for %s",
									fileName));
							int size = 500;
							List<BudgetedTemporary> sublist = null;

							for (int start = 0; start < getExcelData.size(); start += size) {
								int end = Math.min(start + size, getExcelData.size());
								sublist = getExcelData.subList(start, end);
								demandPlanningService.insertFromExcelFileInList(sublist);
							}
							LOGGER.info(
									String.format("Successfully inserted into Temporary Budgeted data  %s", fileName));
							// update status after completing insert all data in Temporary Budgeted data
							status = demandPlanningService.getProgressStatus(userName);
							storeBudgetedSalesStatus.add(status);
							LOGGER.info(String.format("Your Inserting Budgeted data status is  %s", status));

							if (type.equalsIgnoreCase(ChooseType.WEEKLY.toString())) {
								demandPlanningService.deleteMatchedAssortmentCodeTempDataForWeeklyBudgeted(userName);
							} else if (type.equalsIgnoreCase(ChooseType.MONTHLY.toString())) {
								demandPlanningService.deleteMatchedAssortmentCodeTempDataForMonthlyBudgeted(userName);
							}

							if (type.equalsIgnoreCase(ChooseType.WEEKLY.toString())) {

								demandPlanningService.insertMatchBudgetedWeeklyData(userName);
								demandPlanningService.insertNotMatchBudgetedWeeklyData(userName);

							} else if (type.equalsIgnoreCase(ChooseType.MONTHLY.toString())) {

								demandPlanningService.insertMatchBudgetedMonthlyData(userName);
								demandPlanningService.insertNotMatchBudgetedMonthlyData(userName);

							}
							// update status after Verified and insert all Budgeted data
							status = demandPlanningService.getVerifiedAllData(userName);
							storeBudgetedSalesStatus.add(status);
							LOGGER.info(String.format("Your Inserting Budgeted data status is  %s", status));

							// update status after Verified and insert all Budgeted data With URL
							status = demandPlanningService.getFinalyData(type, userName,orgId);
							storeBudgetedSalesStatus.add(status);
							LOGGER.info(String.format("Your Inserting Budgeted data status is  %s", status));

						} catch (Exception e) {
							e.printStackTrace();
							demandPlanningService.updateDPUploadSummary(AWSUtils.Status.FAILED.toString(), 0, 0, null,
									id);

						}
					}
				});
				ObjectNode objectNode = getMapper().createObjectNode();
				objectNode.put("id", id);
				// objectNode.put("fileName", fileName);
				objectNode.put("frequency", type);
				objectNode.put("userName", userName);
				ObjectNode node = getMapper().createObjectNode();
				node.put(CodeUtils.RESPONSE, objectNode);
				// if (readDataFromFromExcel.size() != 0) {
				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
						.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.EXCEL_UPLOAD).putAll(node))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				demandPlanningService.insertDPUploadSummaryData(userName, type, userName, rowCount);
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE,
						AppModuleErrors.DEMAND_BUDGETED_SALES_EXCEL_READER_ERROR, AppStatusMsg.BLANK);
				LOGGER.info(AppStatusMsg.BLANK);

			}
		}
		catch (SupplyMintException ex) {
			LOGGER.debug(String.format("Error Occoured From SupplyMintException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE,
					AppModuleErrors.DEMAND_BUDGETED_SALES_EXCEL_READER_ERROR, ex.getMessage());

		} catch (CannotGetJdbcConnectionException ex) {
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.DEMAND_BUDGETED_SALES_CREATOR_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.DEMAND_BUDGETED_SALES_CREATOR_ERROR,
					appStatus.getMessage(AppModuleErrors.DEMAND_BUDGETED_SALES_CREATOR_ERROR));

		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings("finally")
	@RequestMapping(value = "/download/history", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getUrlResponse(@RequestParam(value = "runId") String runID,
			ServletRequest request) {

		AppResponse appResponse = null;
		
		try {
			String token = ((HttpServletRequest) request).getHeader("X-Auth-Token");
			JsonArray orgDetails=CodeUtils.decode(token).get("ORG_DETAILS").getAsJsonArray();
			
			JsonObject jNode=orgDetails.get(0).getAsJsonObject();
			String orgId = jNode.get("orgId").getAsString();
			
			DemandPlanning demandPlanning = demandPlanningService.getByRunId(runID);
			S3BucketInfo s3BucketInfo = s3BucketInfoService.getBucketByType(BucketParameter.GENERATE_EXCELCSV,orgId);
			
			// download for profile image url link
			if (!CodeUtils.isEmpty(demandPlanning.getDownloadUrl())) {
				LOGGER.info("retrive forecast on demand history download url");
				String url = s3Wrapper.downloadPreSignedURLOnHistory(s3BucketInfo.getS3bucketName(),
						demandPlanning.getSavedPath().substring(1, demandPlanning.getSavedPath().length()),
						Integer.parseInt(s3BucketInfo.getTime()));
				LOGGER.debug("forecast on demand history url :- %s", url);
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, url)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}
		} catch (CannotGetJdbcConnectionException ex) {
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.DEMAND_PLANNING_READER_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.DEMAND_PLANNING_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.DEMAND_PLANNING_READER_ERROR));

		} catch (Exception e) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", e));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally", "deprecation" })
	@RequestMapping(value = "/generate/monthly/forecast", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> testgenerateMonthlyForecast(
			@RequestBody DemandPlanningViewLog demandPlanningViewLog, HttpServletRequest request) {

		AppResponse appResponse = null;
		try {
			String assortment = demandPlanningViewLog.getAssortment();
			String startDate = demandPlanningViewLog.getStartDate();
			String endDate = demandPlanningViewLog.getEndDate();
			String uuid = UUID.randomUUID().toString();
			demandPlanningViewLog.setId(uuid);
			LOGGER.info("generate demand forecast monthly graph");
			Set<DemandGraph> finalMap = demandPlanningService.generateMonthlyForecast(demandPlanningViewLog, request);
			ObjectNode objectNode = getMapper().createObjectNode();
			ObjectNode node = getMapper().createObjectNode();
			node.putPOJO("uuid", uuid);
			node.putPOJO("status", "current");
			node.putPOJO("assortmentCode", assortment);
			node.putPOJO("startDate", CodeUtils.convertDemandForecastDateFormat(startDate));
			node.putPOJO("endDate", CodeUtils.convertDemandForecastDateFormat(endDate));
			node.putPOJO("monthlyGraph", finalMap);
			LOGGER.info(finalMap.toString());
			objectNode.putPOJO(CodeUtils.RESPONSE, node);
			if (!CodeUtils.isEmpty(finalMap)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(objectNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				LOGGER.info(AppStatusMsg.BLANK);
			}
		} catch (CannotGetJdbcConnectionException ex) {
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.DEMAND_PLANNING_READER_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.DEMAND_PLANNING_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.DEMAND_PLANNING_READER_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "deprecation", "finally" })
	@RequestMapping(value = "/generate/weekly/forecast", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> generateWeeklyForecast(@RequestBody DemandPlanningViewLog demandPlanningViewLog,
			HttpServletRequest request) {

		AppResponse appResponse = null;
		try {
			String assortment = demandPlanningViewLog.getAssortment();
			String startDate = demandPlanningViewLog.getStartDate();
			String endDate = demandPlanningViewLog.getEndDate();
			String uuid = UUID.randomUUID().toString();
			demandPlanningViewLog.setId(uuid);
			LOGGER.info("generate demand forecast weekly graph");
			Set<DemandGraph> finalMap = demandPlanningService.generateWeeklyForecast(demandPlanningViewLog, request);
			ObjectNode objectNode = getMapper().createObjectNode();
			ObjectNode node = getMapper().createObjectNode();
			node.putPOJO("uuid", uuid);
			node.putPOJO("status", "current");
			node.putPOJO("assortmentCode", assortment);
			node.putPOJO("startDate", CodeUtils.convertDemandForecastDateFormat(startDate));
			node.putPOJO("endDate", CodeUtils.convertDemandForecastDateFormat(endDate));
			node.putPOJO("weeklyGraph", finalMap);
			LOGGER.info(finalMap.toString());
			objectNode.putPOJO(CodeUtils.RESPONSE, node);
			if (!CodeUtils.isEmpty(finalMap)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(objectNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				LOGGER.info(AppStatusMsg.BLANK);
			}
		} catch (CannotGetJdbcConnectionException ex) {
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.DEMAND_PLANNING_READER_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.DEMAND_PLANNING_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.DEMAND_PLANNING_READER_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally", "deprecation", "rawtypes" })
	@RequestMapping(value = "/download/report", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> downloadForecastReport(@RequestParam("frequency") String frequency,
			@RequestParam("uuid") String uuid, @RequestParam("userName") String userName, ServletRequest request) {

		AppResponse appResponse = null;
		ObjectNode objectNode = null;
		ObjectNode node = null;
		String downloadUrl = null;
		try {
			LOGGER.info("retrive download Report for monthly or weekly graph");
			if (!uuid.equalsIgnoreCase("NA")) {
				DemandPlanningViewLog viewLog = demandPlanningService.getLastReportByFrequency(frequency, uuid);
				if (!CodeUtils.isEmpty(viewLog)) {
					downloadUrl = viewLog.getDownloadUrl();
					if (downloadUrl != null) {
						objectNode = getMapper().createObjectNode();
						objectNode.put("uuid", viewLog.getId());
						objectNode.put("status", viewLog.getAdditional());
						objectNode.put("downloadUrl", downloadUrl);
						objectNode.put("assortment", viewLog.getAssortment());
						objectNode.put("startDate", viewLog.getStartDate());
						objectNode.put("endDate", viewLog.getEndDate());
						ArrayList convertJsonObject = getMapper().readValue(viewLog.getGeneratedOutputJson(),
								ArrayList.class);
						objectNode.putPOJO("generatedJson", convertJsonObject);
					} else {
						objectNode = getMapper().createObjectNode();
						objectNode.put("uuid", viewLog.getId());
						objectNode.put("status", viewLog.getAdditional());
						objectNode.put("downloadUrl", FileStatusCode.FILE_INPROGRESS);
						objectNode.put("assortment", viewLog.getAssortment());
						objectNode.put("startDate", viewLog.getStartDate());
						objectNode.put("endDate", viewLog.getEndDate());
						ArrayList convertJsonObject = getMapper().readValue(viewLog.getGeneratedOutputJson(),
								ArrayList.class);
						objectNode.putPOJO("generatedJson", convertJsonObject);
					}
				}
			} else {
				DemandPlanningViewLog viewLog = demandPlanningService.getLastReport(frequency, userName);
				objectNode = getMapper().createObjectNode();
				objectNode.put("uuid", viewLog.getId());
				objectNode.put("status", viewLog.getAdditional());
				objectNode.put("assortment", viewLog.getAssortment());
				objectNode.put("startDate", viewLog.getStartDate());
				objectNode.put("endDate", viewLog.getEndDate());
				objectNode.put("downloadUrl", FileStatusCode.FILE_INPROGRESS);
				ArrayList convertJsonObject = getMapper().readValue(viewLog.getGeneratedOutputJson(), ArrayList.class);
				objectNode.putPOJO("generatedJson", convertJsonObject);
			}
			node = getMapper().createObjectNode();
			node.put(CodeUtils.RESPONSE, objectNode);
			appResponse = new AppResponse.Builder()
					.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(node))
					.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
		} catch (CannotGetJdbcConnectionException ex) {
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.DEMAND_PLANNING_READER_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.DEMAND_PLANNING_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.DEMAND_PLANNING_READER_ERROR));
		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally" })
	@RequestMapping(value = "/download/budgeted/data", method = RequestMethod.GET)
	public ResponseEntity<AppResponse> downloadDemandBudgeted(@RequestParam("frequency") String frequency,
			@RequestParam("fromDate") String fromDate, @RequestParam("toDate") String toDate,HttpServletRequest request) {

		AppResponse appResponse = null;
		String url = null;
		try {

			url = demandPlanningService.getBudgetedDownloadURL(frequency, fromDate, toDate,request);
			LOGGER.debug("obtaing download budgeted data url :- %s", url);
			if (!CodeUtils.isEmpty(url)) {

				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, url)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}

		}

		catch (CannotGetJdbcConnectionException ex) {
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.DEMAND_BUDGETED_SALES_READER_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.DEMAND_BUDGETED_SALES_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.DEMAND_BUDGETED_SALES_READER_ERROR));

		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally", "deprecation" })
	@RequestMapping(value = "/budgeted/sales/file/upload/status", method = RequestMethod.GET)
	public ResponseEntity<AppResponse> getBudgetedSalesFileUploadStaus(@RequestParam("userName") String userName) {

		AppResponse appResponse = null;
		DpUploadSummary dpUploadSummary = null;
		try {

			ObjectNode node = getMapper().createObjectNode();
			ObjectNode dataNode = getMapper().createObjectNode();
			LOGGER.info("get Last budgeted file uploaded status ");
			dpUploadSummary = demandPlanningService.getLastStatus(userName);
			if (!CodeUtils.isEmpty(dpUploadSummary)) {
				String lastStatus = dpUploadSummary.getStatus();
				if (lastStatus.equals("FAILED")) {
					node.put("status", lastStatus);
					node.put("lastStatus",
							!CodeUtils.isEmpty(storeBudgetedSalesStatus)
									? storeBudgetedSalesStatus.get(storeBudgetedSalesStatus.size() - 1)
									: null);
					if (dpUploadSummary.getRowCount() == 0) {
						node.put("message", AppStatusMsg.BLANK);
					} else {
						node.put("message", AppStatusMsg.BLANK);
					}
				} else {
					if (lastStatus.equalsIgnoreCase(AWSUtils.Status.SUCCEEDED.toString())) {
						demandPlanningService.truncateTempBudgetedData(userName);

						dataNode.put("total", dpUploadSummary.getRowCount());
						dataNode.put("valid", dpUploadSummary.getValidRow());
						dataNode.put("invalid", dpUploadSummary.getInvalidRow());
					}

					node.put("fileName", dpUploadSummary.getAttachUrl());
					node.put("frequency", dpUploadSummary.getType());
					node.put("status", dpUploadSummary.getStatus());
					node.put("data", dataNode);
				}
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, node)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}

		} catch (CannotGetJdbcConnectionException ex) {
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.DEMAND_BUDGETED_SALES_READER_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.DEMAND_BUDGETED_SALES_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.DEMAND_BUDGETED_SALES_READER_ERROR));

		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally", "deprecation" })
	@RequestMapping(value = "/budgeted/sales/summary", method = RequestMethod.GET)
	public ResponseEntity<AppResponse> getBudgetedSalesSummaryData(@RequestParam("pageNo") Integer pageNo,
			@RequestParam("type") Integer type, @RequestParam("uploadedDate") String uploadedDate,
			@RequestParam("fileName") String fileName, @RequestParam("totalData") String totalData,
			@RequestParam("valid") String valid, @RequestParam("invalid") String invalid,
			@RequestParam("search") String search) {

		AppResponse appResponse = null;
		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;
		List<DpUploadSummary> data = null;
		try {
			if (type == 1) {
				LOGGER.info("retrive budgeted sale summary get all");
				totalRecord = demandPlanningService.countBudgetedHistoryData();
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
				data = demandPlanningService.getBudgetedHistoryData(offset, pageSize);
				LOGGER.debug("obtaing budgeted sale summary get all :-%d", data.size());
			}

			if (type == 2) {
				LOGGER.info("retrive budgeted sale summary filter");
				totalRecord = demandPlanningService.countFilterBudgetedHistoryData(uploadedDate, fileName, totalData,
						valid, invalid);
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
				data = demandPlanningService.filterBudgetedHistoryData(offset, pageSize, uploadedDate, fileName,
						totalData, valid, invalid);
				LOGGER.debug("obtaing budgeted sale summary filter :-%d", data.size());
			}
			if (type == 3) {
				LOGGER.info("retrive budgeted sale summary search");
				totalRecord = demandPlanningService.countSearchBudgetedHistoryData(search);
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
				data = demandPlanningService.searchBudgetedHistoryData(offset, pageSize, search);
				LOGGER.debug("obtaing budgeted sale summary search :-%d", data.size());
			}

			ObjectNode objectNode = getMapper().createObjectNode();
			ArrayNode array = getMapper().createArrayNode();
			if (data.size() > 0 && !data.isEmpty()) {
				try {
					Map<String, String> generalSettingData = generalSettingService.getAll();

					String newDate = generalSettingData.get("dateFormat");
					String timeFormat = generalSettingData.get("timeFormat");
					String[] parts = new String[newDate.length()];
					if (newDate.contains("HH")) {
						if (timeFormat.equalsIgnoreCase("24 Hour")) {
							newDateFormat = newDate;
						}
						if (timeFormat.equalsIgnoreCase("12 Hour")) {
							parts = newDate.split("H");
							String h2r = "hh" + parts[2];
							newDateFormat = parts[0] + h2r;
						}
					} else {
						newDateFormat = newDate;
					}
				} catch (Exception ex) {
					newDateFormat = "DD MMM YYYY HH:mm";
				}

				data.stream().forEach(e -> {

					ObjectNode tempNode = getMapper().createObjectNode();

					tempNode.put("id", e.getId());
					tempNode.put("uploadedDate", e.getCreatedTime().format(DateTimeFormatter.ofPattern(newDateFormat)));
					tempNode.put("fileName", e.getFileName());
					tempNode.put("totalRecord", e.getRowCount());
					tempNode.put("valid", e.getValidRow());
					tempNode.put("invalid", e.getInvalidRow());
					tempNode.put("uploadedBy", e.getUserName());
					tempNode.put("url", e.getUrl());

					array.add(tempNode);

				});

				objectNode.put(CodeUtils.RESPONSE, array);
				LOGGER.debug(String.format("Final Object Node data is {} :  %s", objectNode));

				if (!CodeUtils.isEmpty(data)) {

					appResponse = new AppResponse.Builder()
							.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").put("currPage", pageNo)
									.put("prePage", previousPage).put("maxPage", maxPage).putAll(objectNode))
							.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

				} else {
					appResponse = new AppResponse.Builder()
							.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
									.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
							.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				}
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}
		} catch (CannotGetJdbcConnectionException ex) {
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.DEMAND_BUDGETED_SALES_READER_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.DEMAND_BUDGETED_SALES_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.DEMAND_BUDGETED_SALES_READER_ERROR));
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}

	}

	@SuppressWarnings({ "deprecation", "finally" })
	@RequestMapping(value = "/get/all/history", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getAllHistoty(@RequestParam("pageNo") Integer pageNo,
			@RequestParam("type") Integer type, @RequestParam("frequency") String frequency,
			@RequestParam("assortment") String assortment, @RequestParam("startDate") String sDate,
			@RequestParam("endDate") String eDate, @RequestParam("createdOn") String createOn,
			@RequestParam("search") String search) {

		AppResponse appResponse = null;
		List<DemandPlanningViewLog> data = null;
		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;
		String startDate =sDate;
		String endDate =eDate;
		String createdOn =createOn;
		
		try {
			if (type == 1) {
				LOGGER.info("retrive forecast weekly or monthly history get all..");
				totalRecord = demandPlanningService.getAllHistotyCount(frequency);
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo - 1) * pageSize;
				data = demandPlanningService.getAllHistory(offset, pageSize, frequency);
				LOGGER.debug("obtaining forecast weekly or monthly history get all :- %d", data.size());
			} else if (type == 2) {
				LOGGER.info("retrive forecast weekly or monthly history filter");
				if (!CodeUtils.isEmpty(createdOn)) {
					createdOn = CodeUtils.convertDataReferenceDateFormat(createdOn);
				}
				totalRecord = demandPlanningService.filterHistoryRecord(assortment, startDate, endDate, createdOn,
						frequency);
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo - 1) * pageSize;
				data = demandPlanningService.filterHistory(offset, pageSize, assortment, startDate, endDate, createdOn,
						frequency);
				LOGGER.debug("obtaining forecast weekly or monthly history filter :- %d", data.size());
			} else if (type == 3) {
				LOGGER.info("retrive forecast weekly or monthly history search..");
				if (!search.isEmpty()) {

					totalRecord = demandPlanningService.searchHistoryRecord(search, frequency);
					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo - 1) * pageSize;
					data = demandPlanningService.searchHistory(offset, pageSize, search, frequency);
					LOGGER.debug("obtaining forecast weekly or monthly history search :- %d", data.size());
				} else {
					appResponse = new AppResponse.Builder()
							.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
									.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
							.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				}
			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
						AppStatusMsg.FAILURE_MSG);
			}
			if (!CodeUtils.isEmpty(data)) {
				ObjectNode objectNode = getMapper().createObjectNode();
				ArrayNode array = getMapper().createArrayNode();
				data.forEach(e -> {
					ObjectNode node = getMapper().valueToTree(e);
					array.add(node);
				});
				objectNode.put(CodeUtils.RESPONSE, array);
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put("currPage", pageNo).put("prePage", previousPage)
								.put("maxPage", maxPage).put(CodeUtils.RESPONSE_MSG, "").putAll(objectNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.DEMAND_PLANNING_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.DEMAND_PLANNING_READER_ERROR));
		} catch (Exception ex) {
			ex.printStackTrace();
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally", "unused" })
	@RequestMapping(value = "/download/history/url", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getUrlResponse(@RequestParam("frequency") String frequency,
			@RequestParam("uuid") String uuid, ServletRequest request) {

		AppResponse appResponse = null;
		String downloadUrl = null;
		try {
			DemandPlanningViewLog viewLog = demandPlanningService.getLastReportByFrequency(frequency, uuid);
			String bucketLink = BucketParameter.BUCKETPREFIX + viewLog.getBucketPath() + viewLog.getBucketKey();
			downloadUrl = UploadUtils.downloadHistory(bucketLink, BucketParameter.GENERATE_EXCELCSV, request);
			LOGGER.debug("obtaing the forecast monthly or weekly download url :%s", downloadUrl);
			int updatedownloadUrlLogById = demandPlanningService.updatedownloadUrlLogById(downloadUrl, uuid);
			if (!CodeUtils.isEmpty(downloadUrl)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, downloadUrl)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}
		} catch (CannotGetJdbcConnectionException ex) {
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.DEMAND_PLANNING_READER_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.DEMAND_PLANNING_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.DEMAND_PLANNING_READER_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			LOGGER.debug(String.format("Error Occoured From Exception : %s", e));
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally", "deprecation", "unused" })
	@RequestMapping(value = "/get/forecast/status", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getStatus(@RequestParam(value = "runId") String runId, ServletRequest request) {

		AppResponse appResponse = null;
		int percentage = 0;
		String currentStatus = "NOT STARTED";
		String elapsedTime = "0";
		DemandPlanning lastSummaryData = null;
		DemandPlanning data = null;
		String forecastId = null;
		String duration = "0";
		int maxTime = 0;
		int calculatedTime = 0;
		String total = null;
		List<String> avarageTime = null;
		try {

			if (runId.equalsIgnoreCase("NA")) {
				lastSummaryData = demandPlanningService.getLastSummaryData();
				forecastId = demandPlanningService.getRunningId();
				if (lastSummaryData != null) {
					duration = lastSummaryData.getDuration();
				}
			} else {
				data = demandPlanningService.getByRunId(runId);
				currentStatus = data.getStatus();
				lastSummaryData = demandPlanningService.getLastSummaryData();
				if (lastSummaryData != null) {
					duration = lastSummaryData.getDuration();
				}
				forecastId = runId;
				if (data.getStatus().equalsIgnoreCase(CustomRunState.Processing.toString())) {
					Date currentTime = CodeUtils.convertDateOnTimeZonePluseForAWS(new Date(), 5, 30);
					elapsedTime = CodeUtils.elapsedTime(data.getCreatedOn(), currentTime);
					if (Integer.parseInt(elapsedTime) > 0) {
						avarageTime = demandPlanningService.fiveSucceededTime();
						int count = 1;
						for (String avgSum : avarageTime) {
							if (!CodeUtils.isEmpty(avgSum)) {
								maxTime += Integer.parseInt(avgSum);
								++count;
							}
						}
						if (count > 1) {
							calculatedTime = maxTime / (count - 1);
						}
						if (calculatedTime != 0) {
							total = calculatedTime + "";
						} else {
							total = "15";
						}
						percentage = CodeUtils.calculatePercentage(elapsedTime, total);

						if (percentage > 95 && currentStatus.equals(CustomRunState.Processing.toString())) {
							percentage = 95;
						}
						if (Integer.parseInt(elapsedTime) > 50 * 60) {
							int updateProcessingToFailed = demandPlanningService.updateProcessingToFailed();
						}
					}
				} else if (currentStatus.equalsIgnoreCase(CustomRunState.Succeeded.toString())) {
					percentage = 100;
				}
			}
			ObjectNode objectNode = getMapper().createObjectNode();
			objectNode.put("runId", forecastId);
			objectNode.put("status", currentStatus);
			objectNode.put("lastEngineRun", duration);
			objectNode.put("percentage", percentage);
			objectNode.put("timeTaken", elapsedTime);
			ObjectNode node = getMapper().createObjectNode();
			node.put(CodeUtils.RESPONSE, objectNode);
			if (!CodeUtils.isEmpty(objectNode)) {
				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
						.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG).putAll(node))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}
		} catch (CannotGetJdbcConnectionException ex) {
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.DEMAND_PLANNING_READER_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.DEMAND_PLANNING_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.DEMAND_PLANNING_READER_ERROR));

		} catch (Exception e) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", e));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally" })
	@RequestMapping(value = "/get/lastForecast/history", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getLastJobSummary() {

		AppResponse appResponse = null;
		DemandPlanning lastSummaryData = null;
		try {
			LOGGER.info("retrive the forecast last summary data");
			lastSummaryData = demandPlanningService.getLastSummaryData();
			if (!CodeUtils.isEmpty(lastSummaryData)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, lastSummaryData)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}
		} catch (CannotGetJdbcConnectionException ex) {
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.DEMAND_PLANNING_READER_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.DEMAND_PLANNING_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.DEMAND_PLANNING_READER_ERROR));

		} catch (Exception e) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", e));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "finally" })
	@RequestMapping(value = "/budgeted/history/url", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getUrlDemandBudgetedHistory(@RequestParam("uuid") String uuid,
			ServletRequest request) {

		AppResponse appResponse = null;
		String url = null;
		try {
			DpUploadSummary data = demandPlanningService.getBudgetedHistoryDataByID(uuid);

			url = UploadUtils.downloadHistory(data.getUrl(), BucketParameter.GENERATE_EXCELCSV, request);

			LOGGER.debug("obtaining the budgeted download url :-%s", url);
			if (!CodeUtils.isEmpty(url)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, url)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}
		} catch (CannotGetJdbcConnectionException ex) {
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.DEMAND_BUDGETED_SALES_READER_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.DEMAND_BUDGETED_SALES_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.DEMAND_BUDGETED_SALES_READER_ERROR));

		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		}

		finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings("finally")
	@RequestMapping(value = "/create/forecast/scheduler", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> insertData(@RequestBody DemandForecastScheduler forecastScheduler,
			ServletRequest request) throws SupplyMintException {

		AppResponse appResponse = null;
		int result = 0;
		try {
			result = demandPlanningService.createForecastSchedulerAutoConfig(forecastScheduler, request);
			if (result != 0) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, forecastScheduler)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
						AppModuleErrors.FORECAST_AUTOCONFIG_CREATOR_ERROR,
						appStatus.getMessage(AppModuleErrors.FORECAST_AUTOCONFIG_CREATOR_ERROR));

			}
		} catch (CannotGetJdbcConnectionException ex) {
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.FORECAST_AUTOCONFIG_CREATOR_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			ex.printStackTrace();
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.FORECAST_AUTOCONFIG_CREATOR_ERROR,
					appStatus.getMessage(AppModuleErrors.FORECAST_AUTOCONFIG_CREATOR_ERROR));
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}

	}

	@SuppressWarnings({ "deprecation", "finally", "rawtypes", "unchecked" })
	@RequestMapping(value = "/get/assortment/history", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getAssortmentHistory(@RequestParam("pageNo") Integer pageNo,
			@RequestParam("type") Integer type, @RequestParam("totalCount") Integer totalCount,
			@RequestParam("year") String year, @RequestParam("frequency") String frequency,
			@RequestParam("duration") String duration, @RequestParam("startDate") String sDate,
			@RequestParam("endDate") String eDate, @RequestParam("search") String search) {
		AppResponse appResponse = null;
		List<String> data = null;
		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;
		List array = new ArrayList<>();
		JsonNode jsonNode = null;
		try {
			String startDate=sDate;
			String endDate=eDate;
			Calendar calendar = Calendar.getInstance();
			FiscalDate fiscalDate = new FiscalDate(calendar);
			String previousFiscalYear = fiscalDate.getFiscalYear() - 1 + "-" + fiscalDate.getFiscalYear();
			String currentFiscalYear = fiscalDate.getFiscalYear() + "-" + fiscalDate.getFiscalYear() + 1;
//			String nextFiscalYear=fiscalDate.getFiscalYear()+1+"-"+fiscalDate.getFiscalYear()+2;

			if (year.equals(FiscalYear.PREVIOUS_FISCAL_YEAR.toString())) {
				startDate = CodeUtils.convertFiscalYearToFiscalDate(previousFiscalYear).get("startDate");
			} else if (year.equals(FiscalYear.CURRENT_FISCAL_YEAR.toString())) {
				startDate = CodeUtils.convertFiscalYearToFiscalDate(currentFiscalYear).get("startDate");
			}
//			else if(year.equals(FiscalYear.NEXT_FISCAL_YEAR.toString())) {
//				startDate=CodeUtils.convertFiscalYearToFiscalDate(nextFiscalYear).get("startDate");
//			}

			if (duration.equalsIgnoreCase("3_MONTHS")) {
				endDate = CodeUtils.__dateFormat.format(CodeUtils.addMonth(
						new Date(CodeUtils.convertDataReferenceDateFormat(CodeUtils.getLastDayOfTheMonth(startDate))),
						3));
			} else if (duration.equalsIgnoreCase("6_MONTHS")) {
				endDate = CodeUtils.__dateFormat.format(CodeUtils.addMonth(
						new Date(CodeUtils.convertDataReferenceDateFormat(CodeUtils.getLastDayOfTheMonth(startDate))),
						6));
			} else if (duration.equalsIgnoreCase("12_MONTHS")) {
				endDate = CodeUtils.__dateFormat.format(CodeUtils.addMonth(
						new Date(CodeUtils.convertDataReferenceDateFormat(CodeUtils.getLastDayOfTheMonth(startDate))),
						12));
			}
			String lastRunId = demandPlanningService.getLastRunId();
			if (type == 1) {
				LOGGER.debug("retrive the distinct assortment get all");
				if (totalCount == 0) {
					totalRecord = demandPlanningService.assortmentHistoryRecord(startDate, endDate, frequency);
				} else {
					totalRecord = totalCount * 10;
				}
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo - 1) * pageSize;

				data = demandPlanningService.getAssortmentHistory(offset, pageSize, startDate, endDate, frequency);
				LOGGER.debug("obtaining the distinct assortment get all %d", data.size());

				for (int i = 0; i < data.size(); i++) {
					jsonNode = getMapper().readTree(data.get(i));
					Iterator itr = jsonNode.iterator();
					while (itr.hasNext()) {
						JsonNode json = (JsonNode) itr.next();
						String createdOn = json.get("createdOn").asText();
						((ObjectNode) json).put("timeAgo", CodeUtils
								.getTimeAgo(new Date(CodeUtils.convertNormalDateFormatWithTime(createdOn)).getTime()));
					}
					array.add(jsonNode);
				}

			} else if (type == 3) {
				if (!search.isEmpty()) {
					LOGGER.debug("retrive the distinct assortment search");
					if (totalCount == 0) {
						totalRecord = demandPlanningService.assortmentSearchHistoryRecord(search, startDate, endDate,
								frequency);
					} else {
						totalRecord = totalCount * 10;
					}
					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo - 1) * pageSize;
					data = demandPlanningService.assortmentHistorySearch(offset, pageSize, search, startDate, endDate,
							frequency);
					LOGGER.debug("obtaining the distinct assortment search %d", data.size());

					for (int i = 0; i < data.size(); i++) {
						jsonNode = getMapper().readTree(data.get(i));
						Iterator itr = jsonNode.iterator();
						while (itr.hasNext()) {
							JsonNode json = (JsonNode) itr.next();
							String createdOn = json.get("createdOn").asText();
							((ObjectNode) json).put("timeAgo", CodeUtils.getTimeAgo(
									new Date(CodeUtils.convertNormalDateFormatWithTime(createdOn)).getTime()));
						}
						array.add(jsonNode);
					}
				}
			}

			ObjectNode objectNode = getMapper().createObjectNode();
			objectNode.putPOJO(CodeUtils.RESPONSE, array);

			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode().put("lastRunId", lastRunId)
						.put(Pagination.CURRENT_PAGE, pageNo).put(Pagination.PREVIOUS_PAGE, previousPage)
						.put(Pagination.MAXIMUM_PAGE, maxPage).put(CodeUtils.RESPONSE_MSG, "").putAll(objectNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				LOGGER.info(AppStatusMsg.BLANK);
			}
		} catch (CannotGetJdbcConnectionException ex) {
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.DEMAND_PLANNING_READER_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.DEMAND_PLANNING_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.DEMAND_PLANNING_READER_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "deprecation", "finally" })
	@RequestMapping(value = "/get/previous/assortment", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> getPreviousAssortmentCodeHistory(@RequestBody JsonNode payload,
			HttpServletRequest request) {
		AppResponse appResponse = null;
		try {

			int pageNo = payload.get("pageNo").asInt();
			int type = payload.get("type").asInt();
			int totalCount = payload.get("totalCount").asInt();
			String assortmentCode = payload.get("assortmentCode").asText();
			String runId = payload.get("runId").asText();
			String startDate = payload.get("startDate").asText();
			String endDate = payload.get("endDate").asText();
			String search = payload.get("search").asText();
			ObjectNode data = demandPlanningService.getPreviousAssortmentCodeHistory(pageNo, type, totalCount,
					assortmentCode, runId, startDate, endDate, search, request);
			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(data))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
				LOGGER.info(AppStatusMsg.BLANK);
			}
		} catch (CannotGetJdbcConnectionException ex) {
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.DEMAND_PLANNING_READER_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.DEMAND_PLANNING_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.DEMAND_PLANNING_READER_ERROR));
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings("finally")
	@RequestMapping(value = "/download/previous/forecast", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> downloadPreviousForecast(@RequestParam(value = "runId") String runId,
			@RequestParam(value = "startDate") String startDate, @RequestParam(value = "endDate") String endDate,
			@RequestParam(value = "frequency") String frequency, ServletRequest request) {

		AppResponse appResponse = null;

		String url = demandPlanningService.getForecastUrl(runId, startDate, endDate, frequency, request);
		try {
			if (!CodeUtils.isEmpty(url)) {
				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
						.putPOJO(CodeUtils.RESPONSE, "").put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}
		} catch (CannotGetJdbcConnectionException ex) {
			LOGGER.debug(String.format("Error Occoured From CannotGetJdbcConnectionException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.DEMAND_PLANNING_READER_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			LOGGER.debug(String.format("Error Occoured From DataAccessException : %s", ex));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.DEMAND_PLANNING_READER_ERROR,
					appStatus.getMessage(AppModuleErrors.DEMAND_PLANNING_READER_ERROR));

		} catch (Exception e) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", e));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
		} finally {
			return ResponseEntity.ok(appResponse);
		}
	}
}
