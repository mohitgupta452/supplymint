package com.supplymint.layer.web.endpoint.tenant.inventory;

import java.net.InetAddress;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.CannotGetJdbcConnectionException;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.supplymint.config.application.AppCodeConfig;
import com.supplymint.exception.StatusCodes.AppErrorCodes;
import com.supplymint.exception.StatusCodes.AppModuleErrors;
import com.supplymint.exception.StatusCodes.AppStatusCodes;
import com.supplymint.exception.StatusCodes.AppStatusMsg;
import com.supplymint.exception.StatusCodes.ClientErrors;
import com.supplymint.exception.SupplyMintException;
import com.supplymint.layer.business.contract.inventory.AdHocRequestService;
import com.supplymint.layer.business.contract.setting.GeneralSettingService;
import com.supplymint.layer.data.tenant.administration.entity.ADMItem;
import com.supplymint.layer.data.tenant.administration.entity.ADMSite;
import com.supplymint.layer.data.tenant.demand.entity.AdHocItemRequest;
import com.supplymint.layer.data.tenant.demand.entity.AdHocRequest;
import com.supplymint.layer.web.endpoint.abstracts.AbstractEndpoint;
import com.supplymint.layer.web.model.AppResponse;
import com.supplymint.util.CodeUtils;

@RestController
@RequestMapping(path = AdHocRequestEndpoint.PATH, produces = { MediaType.APPLICATION_JSON_VALUE })
public class AdHocRequestEndpoint extends AbstractEndpoint {

	public static final String PATH = "inv/plan/adhoc";
	public static final String DEFAULT_WORKORDER = "S/PR-AD-HOC/AA0001";
	@Autowired
	private AppCodeConfig appStatus;

	private static final Logger LOGGER = LoggerFactory.getLogger(AdHocRequestEndpoint.class);

	private AdHocRequestService adHocRequestService;

	String newDateFormat = null;

	@Autowired
	private GeneralSettingService generalSettingService;

	@Autowired
	public AdHocRequestEndpoint(AdHocRequestService adocRequestService) {
		this.adHocRequestService = adocRequestService;
	}

	@SuppressWarnings({ "finally" })
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<AppResponse> insertData(@RequestBody AdHocRequest adocRequest) throws SupplyMintException {
		LOGGER.info("Ad-HocRequest insertData() method starts here....");
		AppResponse appResponse = null;
		int result = 0;
		long hours = 5L;
		long minutes = 30L;
		List<AdHocItemRequest> itemRequests = adocRequest.getItem();
		// List<AdHocRequest> validation = null;
		String createNewWorkOrder;
		try {
			OffsetDateTime dateTime = OffsetDateTime.now();
			dateTime = dateTime.plusHours(hours);
			dateTime = dateTime.plusMinutes(minutes);
			OffsetDateTime requestedDate = adocRequest.getRequestedDate();
			requestedDate = requestedDate.plusHours(hours);
			requestedDate = requestedDate.plusMinutes(hours);
			String ipAddress = InetAddress.getLocalHost().getHostAddress().trim().toString();
			adocRequest.setCreatedTime(dateTime);
			adocRequest.setIpAddress(ipAddress);
			adocRequest.setItemStatus("Active");
			adocRequest.setReqStatus("Inprogress");
			adocRequest.setRequestedDate(requestedDate);
			String getLastWorkOrder = adHocRequestService.getLastWorkOrder();
			LOGGER.debug(String.format("Successfully get Last Work Order is : %s", getLastWorkOrder));
			if (!CodeUtils.isEmpty(getLastWorkOrder)) {
				createNewWorkOrder = CodeUtils.createPattern(getLastWorkOrder);
				adocRequest.setWorkOrder(createNewWorkOrder);
			} else {
				createNewWorkOrder = DEFAULT_WORKORDER;
				adocRequest.setWorkOrder(createNewWorkOrder);
			}

			LOGGER.debug(String.format("create new work order number is : %s", createNewWorkOrder));
			for (AdHocItemRequest request : itemRequests) {
				adocRequest.setItemCode(request.getItemCode());
				adocRequest.setQuantity(request.getQuantity());
				// validation = adHocRequestService.validation(adocRequest.getWorkOrder(),
				// request.getItemCode());
				// if (CodeUtils.isEmpty(validation)) {
				// }
				result = adHocRequestService.create(adocRequest);

			}
			LOGGER.info("Successfully Ad-Hoc data inserted.... ");

			if (result != 0) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, adocRequest)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.ADHOC_MSG + " " + createNewWorkOrder))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
						AppModuleErrors.ADHOC_CREATOR_ERROR, appStatus.getMessage(AppModuleErrors.ADHOC_CREATOR_ERROR));

			}

		} catch (DataAccessException ex) {

			LOGGER.debug(AppModuleErrors.ADHOC_CREATOR_ERROR, ex);

			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.ADHOC_CREATOR_ERROR, appStatus.getMessage(AppModuleErrors.ADHOC_CREATOR_ERROR));
		}

		catch (Exception ex) {
			LOGGER.debug(AppModuleErrors.ADHOC_CREATOR_ERROR, ex);
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			throw new SupplyMintException(AppStatusMsg.FAILURE_MSG);

		} finally {
			LOGGER.info("Ad-HocRequest insertData() method end here....");
			return ResponseEntity.ok(appResponse);
		}
	}

	@SuppressWarnings({ "deprecation", "finally" })

	@RequestMapping(value = "/get/itemcode", method = RequestMethod.GET)

	@ResponseBody
	public ResponseEntity<AppResponse> getAllItemCode(@RequestParam("pageNo") Integer pageNo,
			@RequestParam("type") Integer type, @RequestParam("search") String search) throws SupplyMintException {
		LOGGER.info("Ad-HocRequest getAllItemCode() method starts here....");
		AppResponse appResponse = null;
		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;
		// List<String> data = null;

		List<ADMItem> data = null;

		try {

			if (type == 1) {
				LOGGER.debug(
						String.format("Create Start Time at : %s", CodeUtils._24HourTimeFormat.format(new Date())));
				totalRecord = adHocRequestService.countItemCode();

				LOGGER.debug(String.format("Get total no. of Item Codes  : %s", totalRecord));
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
				// data = adHocRequestService.itemCode(offset, pageSize);
				data = adHocRequestService.itemCode(offset, pageSize);
				LOGGER.debug(String.format("Get List of Item Code data for type: %s", type));

			} else if (type == 3 && !CodeUtils.isEmpty(search)) {
				totalRecord = adHocRequestService.counterForSerarchItemCode(search);
				LOGGER.debug(String.format("Get total no. of Item Codes  : %s", totalRecord));
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
				// data = adHocRequestService.searchItemCode(offset, pageSize, search);
				data = adHocRequestService.searchItemCode(offset, pageSize, search);
				LOGGER.debug(String.format("Get List of Item Code data for type: %s", type));
			}

			ObjectNode objectNode = getMapper().createObjectNode();
			ArrayNode array = getMapper().createArrayNode();
			// AtomicInteger count = new AtomicInteger(1);

			if (data.size() > 0 && !data.isEmpty()) {

				data.stream().forEach(e -> {

					ObjectNode tempNode = getMapper().createObjectNode();
					tempNode.put("itemCode", e.getiCode());
					tempNode.put("itemName", e.getItemName());
					tempNode.put("articleName", e.gethL4Name());

					array.add(tempNode);

				});

				objectNode.put(CodeUtils.RESPONSE, array);
				// objectNode.putPOJO(CodeUtils.RESPONSE, data);
				LOGGER.debug("Final Object Node data is {} : %s", objectNode);

				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").put("currPage", pageNo)
								.put("prePage", previousPage).put("maxPage", maxPage).putAll(objectNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			}

			else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}
		}

		catch (DataAccessException ex) {
			LOGGER.debug(AppModuleErrors.ADHOC_READ_ICODE_ERROR, ex);
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.ADHOC_READ_ICODE_ERROR,
					appStatus.getMessage(AppModuleErrors.ADHOC_READ_ICODE_ERROR));

		} catch (Exception ex) {
			LOGGER.debug(AppModuleErrors.ADHOC_READ_ICODE_ERROR, ex);
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			throw new SupplyMintException(AppStatusMsg.FAILURE_MSG);
		} finally {
			LOGGER.info("Ad-HocRequest getAllItemCode() method end here....");
			return ResponseEntity.ok(appResponse);
		}

	}

	@SuppressWarnings({ "deprecation", "finally" })

	@RequestMapping(value = "/get/site", method = RequestMethod.GET)

	@ResponseBody
	public ResponseEntity<AppResponse> getAllSite() throws SupplyMintException {
		LOGGER.info("Ad-HocRequest getAllSite() method starts here....");
		AppResponse appResponse = null;
		List<ADMSite> data = null;
		try {
			data = adHocRequestService.getLocation();
			ObjectNode objectNode = getMapper().createObjectNode();
			ArrayNode array = getMapper().createArrayNode();

			if (data.size() > 0 && !data.isEmpty()) {

				data.stream().forEach(e -> {

					ObjectNode tempNode = getMapper().createObjectNode();

					if (CodeUtils.isEmpty(e.getCode()) && !CodeUtils.isEmpty(e.getName())) {
						tempNode.put("site", e.getName());
					} else if (CodeUtils.isEmpty(e.getName()) && !CodeUtils.isEmpty(e.getCode())) {
						tempNode.put("site", e.getCode());
					} else if (!CodeUtils.isEmpty(e.getName()) && !CodeUtils.isEmpty(e.getCode())) {
						tempNode.put("site", e.getCode() + " - " + e.getName());
					}

					array.add(tempNode);

				});

				objectNode.put(CodeUtils.RESPONSE, array);
				LOGGER.debug("Final Object Node data is {} : %s", objectNode);
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").putAll(objectNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			} else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}
		}

		catch (DataAccessException ex) {
			LOGGER.debug(AppModuleErrors.ADHOC_READ_SITE_ERROR, ex);
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.ADHOC_READ_SITE_ERROR, appStatus.getMessage(AppModuleErrors.ADHOC_READ_SITE_ERROR));

		} catch (Exception ex) {
			LOGGER.debug(AppModuleErrors.ADHOC_READ_SITE_ERROR, ex);

			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			throw new SupplyMintException(AppStatusMsg.FAILURE_MSG);
		} finally {
			LOGGER.info("Ad-HocRequest getAllSite() method end here....");
			return ResponseEntity.ok(appResponse);
		}

	}

	@SuppressWarnings({ "deprecation", "finally" })

	@RequestMapping(value = "/get/all", method = RequestMethod.GET)

	@ResponseBody
	public ResponseEntity<AppResponse> getAll(@RequestParam("pageNo") Integer pageNo,
			@RequestParam("type") Integer type, @RequestParam("search") String search,
			@RequestParam("status") String status, @RequestParam("site") String site,
			@RequestParam("workOrder") String workOrder, @RequestParam("createdOn") String createdOn)
			throws SupplyMintException {
		LOGGER.info("Ad-HocRequest getAll() method starts here....");
		AppResponse appResponse = null;
		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;
		List<AdHocRequest> data = null;

		try {

			if (type == 1) {
				totalRecord = adHocRequestService.count();
				LOGGER.debug(String.format("Get all total no.of record : %s", totalRecord));
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
				data = adHocRequestService.getAll(offset, pageSize);
				LOGGER.debug(String.format("Get List of data for type : %s", type));

			} else if (type == 2) {
				totalRecord = adHocRequestService.filterRecord(workOrder, status, site, createdOn);
				LOGGER.debug(String.format("Get total no.of filter record : %s", totalRecord));
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
				data = adHocRequestService.filter(offset, pageSize, workOrder, status, site, createdOn);
				LOGGER.debug(String.format("Get List of data for type: %s", type));

			} else {
				if (type == 3 && !CodeUtils.isEmpty(search)) {

					totalRecord = adHocRequestService.searchCount(search);
					LOGGER.debug(String.format("Get total no.of search record : %s", totalRecord));
					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
					data = adHocRequestService.searchAll(offset, pageSize, search);
					LOGGER.debug(String.format("Get List of data for type: %s", type));
				}

			}
			ObjectNode objectNode = getMapper().createObjectNode();
			ArrayNode array = getMapper().createArrayNode();

			if (data.size() != 0 && !data.isEmpty()) {

				try {
					Map<String, String> generalSettingData = generalSettingService.getAll();

					String newDate = generalSettingData.get("dateFormat");
					String timeFormat = generalSettingData.get("timeFormat");
					String[] parts = new String[newDate.length()];
					if (newDate.contains("HH")) {
						if (timeFormat.equalsIgnoreCase("24 Hour")) {
							newDateFormat = newDate;
						}
						if (timeFormat.equalsIgnoreCase("12 Hour")) {
							parts = newDate.split("H");
							String h2r = "hh" + parts[2];
							newDateFormat = parts[0] + h2r + " a";
						}
					} else {
						newDateFormat = newDate;
					}
				} catch (Exception ex) {
					newDateFormat = "DD MMM YYYY HH:mm";
				}

				data.stream().forEach(e -> {
					ObjectNode tempNode = getMapper().createObjectNode();
					ArrayNode itemArray = getMapper().createArrayNode();
					tempNode.put("site", e.getSite());
					tempNode.put("reqDate", e.getRequestedDate().format(DateTimeFormatter.ofPattern("dd MMM yyyy")));
					tempNode.put("workOrder", e.getWorkOrder());
					tempNode.put("createdOn", e.getCreatedTime().format(DateTimeFormatter.ofPattern(newDateFormat)));
					tempNode.put("status", e.getReqStatus());

					List<AdHocItemRequest> adHocItemData = adHocRequestService.getItem(e.getWorkOrder());
					adHocItemData.stream().forEach(itemData -> {

						ObjectNode itemNode = getMapper().createObjectNode();

						itemNode.put("itemData", itemData.getItemCode());
						itemNode.put("quantity", itemData.getQuantity());
						itemNode.put("status", itemData.getItemStatus());

						itemArray.add(itemNode);
					});

					tempNode.put("item", itemArray);

					array.add(tempNode);

				});

				objectNode.put(CodeUtils.RESPONSE, array);
				LOGGER.debug("Final Object Node data is {} : %s", objectNode);

				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, "").put("currPage", pageNo)
								.put("prePage", previousPage).put("maxPage", maxPage).putAll(objectNode))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}

			else {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();
			}
		}

		catch (DataAccessException ex) {
			LOGGER.debug(AppModuleErrors.ADHOC_READER_ERROR, ex);
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.ADHOC_READER_ERROR, appStatus.getMessage(AppModuleErrors.ADHOC_READER_ERROR));

		} catch (Exception ex) {
			LOGGER.debug(AppModuleErrors.ADHOC_READER_ERROR, ex);

			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			throw new SupplyMintException(AppStatusMsg.FAILURE_MSG);
		} finally {
			LOGGER.info("Ad-HocRequest getAll() method end here....");
			return ResponseEntity.ok(appResponse);
		}

	}

	@SuppressWarnings("finally")
	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> deleteByWorkOrderAndItemCode(@RequestParam(value = "workOrder") String workOrder,
			@RequestParam("itemData") String itemData) {

		LOGGER.info("deleteByWorkOrderAndItemCode() method starts here....");
		AppResponse appResponse = null;
		int result = 0;
		List<AdHocRequest> data = null;
		String successResponse;
		long hours = 5L;
		long minutes = 30L;
		try {
			data = adHocRequestService.getItemByworkOrder(workOrder);
			LOGGER.info("Reading List of Item with respect to work order");
			result = adHocRequestService.deleteByWorkOrderAndItemCode(workOrder, itemData);
			LOGGER.info("successfully remove Item");
			OffsetDateTime updationTime = OffsetDateTime.now();
			updationTime = updationTime.plusHours(hours);
			updationTime = updationTime.plusMinutes(minutes);
			String ipAddress = InetAddress.getLocalHost().getHostAddress().trim().toString();
			LOGGER.info("check no. of Items......");
			if (data.size() == 1) {
				adHocRequestService.removedWorkedOrder(workOrder, ipAddress, updationTime);
				successResponse = AppStatusMsg.ALL_ITEM_REMOVED_SUCCESS_MSG;
			} else {
				successResponse = AppStatusMsg.SINGLE_ITEM_REMOVED_SUCCESS_MSG;
			}
			if (result != 0) {
				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, successResponse))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = buildErrorResponse(AppErrorCodes.GENERIC_EVENT_NOT_AVAILABLE_ERROR_CODE,
						ClientErrors.NOT_FOUND, AppStatusMsg.BLANK);
			}
		} catch (CannotGetJdbcConnectionException ex) {
			ex.printStackTrace();
			LOGGER.debug(AppModuleErrors.ADHOC_DELETE_ERROR, ex);
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.ADHOC_DELETE_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			ex.printStackTrace();
			LOGGER.debug(AppModuleErrors.ADHOC_DELETE_ERROR, ex);
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.ADHOC_DELETE_ERROR, appStatus.getMessage(AppModuleErrors.ADHOC_DELETE_ERROR));

		} catch (NumberFormatException ex) {
			ex.printStackTrace();
			LOGGER.debug(AppModuleErrors.ADHOC_DELETE_ERROR, ex);
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.NUMBER_FORMAT_MSG);
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.debug(AppModuleErrors.ADHOC_DELETE_ERROR, ex);
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			throw new SupplyMintException(AppStatusMsg.FAILURE_MSG);
		} finally {
			LOGGER.info("deleteByWorkOrderAndItemCode() method ends here....");
			return ResponseEntity.ok(appResponse);
		}

	}

	@SuppressWarnings("finally")
	@RequestMapping(value = "/cancel/workorder", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> cancelWorkOrder(@RequestParam("workOrder") String workOrder) {
		LOGGER.info("cancelWorkOrder() method starts here....");
		AppResponse appResponse = null;
		int result = 0;
		long hours = 5L;
		long minutes = 30L;
		try {
			OffsetDateTime updationTime = OffsetDateTime.now();
			updationTime = updationTime.plusHours(hours);
			updationTime = updationTime.plusMinutes(minutes);

			String ipAddress = InetAddress.getLocalHost().getHostAddress().trim().toString();

			result = adHocRequestService.cancelWorkedOrder(workOrder, ipAddress, updationTime);
			LOGGER.info("Successfully canceled worked order");
			if (result != 0) {

				appResponse = new AppResponse.Builder()
						.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
								.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.CANCEL_WORK_ORDER_SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {
				appResponse = buildErrorResponse(AppErrorCodes.GENERIC_EVENT_NOT_AVAILABLE_ERROR_CODE,
						ClientErrors.NOT_FOUND, AppStatusMsg.BLANK);

			}
		} catch (CannotGetJdbcConnectionException ex) {
			ex.printStackTrace();
			LOGGER.debug(AppModuleErrors.ADHOC_DELETE_ERROR, ex);
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.ADHOC_DELETE_ERROR, AppStatusMsg.FAILED_JDBC_CONNECTION);
		} catch (DataAccessException ex) {
			ex.printStackTrace();
			LOGGER.debug(AppModuleErrors.ADHOC_DELETE_ERROR, ex);
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE,
					AppModuleErrors.ADHOC_DELETE_ERROR, appStatus.getMessage(AppModuleErrors.ADHOC_DELETE_ERROR));
		} catch (NumberFormatException ex) {
			ex.printStackTrace();
			LOGGER.debug(AppModuleErrors.ADHOC_DELETE_ERROR, ex);
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.NUMBER_FORMAT_MSG);

		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.debug(AppModuleErrors.ADHOC_DELETE_ERROR, ex);
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.FAILURE_MSG);
			throw new SupplyMintException(AppStatusMsg.FAILURE_MSG);
		} finally {
			LOGGER.info("cancelWorkOrder() method starts here....");
			return ResponseEntity.ok(appResponse);

		}

	}

}
