package com.supplymint.layer.web.endpoint.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.supplymint.exception.StatusCodes.AppException;
import com.supplymint.exception.StatusCodes.AppStatusCodes;
import com.supplymint.exception.StatusCodes.AppStatusMsg;
import com.supplymint.exception.StatusCodes.ClientErrorMessage;
import com.supplymint.exception.StatusCodes.ClientErrors;
import com.supplymint.exception.StatusCodes.ServerErrors;
import com.supplymint.exception.SupplyMintException;
import com.supplymint.layer.business.service.core.EnterpriseService;
import com.supplymint.layer.data.core.entity.Enterprise;
import com.supplymint.layer.web.endpoint.abstracts.AbstractEndpoint;
import com.supplymint.layer.web.model.AppResponse;
import com.supplymint.util.CodeUtils;

/**
 * @author Manoj Singh
 * @since 20 OCT 2018
 * @version 1.0
 */

@RestController
@RequestMapping(path = EnterpriseEndpoint.PATH, produces = { MediaType.APPLICATION_JSON_VALUE })
public class EnterpriseEndpoint extends AbstractEndpoint {

	public static final String PATH = "core/enterprise";

	private EnterpriseService enterpriseService;

	private static final Logger LOGGER = LoggerFactory.getLogger(EnterpriseEndpoint.class);

	@Autowired
	public EnterpriseEndpoint(EnterpriseService enterpriseService) {
		this.enterpriseService = enterpriseService;
	}

	@SuppressWarnings("finally")
	@RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<AppResponse> getByIdentifierAndModule(@PathVariable(value = "id") String eid) {

		AppResponse appResponse = null;
		int id = 0;
		try {

			id = Integer.parseInt(eid);
			Enterprise data = enterpriseService.findById(id);
			ObjectNode objectNode = getMapper().createObjectNode();

			objectNode.put("id", data.getId());
			objectNode.put("code", data.getCode());
			objectNode.put("name", data.getName());
			objectNode.put("partner", data.getPartner());
			objectNode.put("gstin", data.getGstin());

			if (!CodeUtils.isEmpty(data)) {
				appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
						.putPOJO("resource", objectNode).put("message", AppStatusMsg.SUCCESS_MSG))
						.status(AppStatusCodes.GENERIC_SUCCESS_CODE).build();

			} else {

				appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
						ClientErrorMessage.BAD_REQUEST);
			}
		} catch (NumberFormatException ex) {

			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_CLIENT_ERROR_CODE, ClientErrors.BAD_REQUEST,
					AppStatusMsg.NUMBER_FORMAT_MSG);
			throw new SupplyMintException(AppException.NUMBER_FORMAT_EXCEPTION);

		} catch (Exception e) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", e));
			appResponse = buildErrorResponse(AppStatusCodes.GENERIC_SERVER_ERROR_CODE, ServerErrors.INTERNAL_SERVER_ERROR,
					AppStatusMsg.FAILURE_MSG);
			throw new SupplyMintException(AppStatusMsg.FAILURE_MSG);
		} finally {

			return ResponseEntity.ok(appResponse);
		}

	}

}
