package com.supplymint.layer.data.typehandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.UUID;

import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import org.apache.ibatis.type.MappedTypes;
import org.apache.ibatis.type.TypeHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

@MappedJdbcTypes(value=JdbcType.BINARY)
@MappedTypes(UUID.class)
public class UuidTypeHandler implements TypeHandler<UUID> {

	private static final Logger LOGGER = LoggerFactory.getLogger(UuidTypeHandler.class);

	@Override
	public void setParameter(PreparedStatement ps, int i, UUID parameter, JdbcType jdbcType) throws SQLException {
		if (parameter == null) {
			ps.setObject(i, null, Types.OTHER);
		} else {
			ps.setObject(i, parameter.toString().getBytes(), Types.BINARY);
		}
	}

	@Override
	public UUID getResult(ResultSet rs, String columnName) throws SQLException {
		return toUUID(rs.getBytes(columnName));
	}

	@Override
	public UUID getResult(ResultSet rs, int columnIndex) throws SQLException {
		return toUUID(rs.getBytes(columnIndex));
	}

	@Override
	public UUID getResult(CallableStatement cs, int columnIndex) throws SQLException {
		return toUUID(cs.getBytes(columnIndex));
	}

	private static UUID toUUID(byte[] val) throws SQLException {
		if (StringUtils.isEmpty(val)) {
			return null;
		}
		try {
			return UUID.nameUUIDFromBytes(val);
			// return UUID.fromString(val);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			LOGGER.error(String.format("Can't convert UUID string to UUID Java Type. Error: %s", e.getMessage()));
		}
		return null;
	}

}