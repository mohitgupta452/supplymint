/**
 * 
 */
package com.supplymint.layer.data.metadata;

/**
 * @author tcemp0074
 *
 */
public interface ProcurementMetaData {

	public enum DeptItemUDFSettingsMetadeta {

		NAME("udf"), CATDESC("catdesc"), DISPLAY_NAME("displayName"), IS_COMPULSORY("isCompulsory"),
		ORDER_BY("orderBy"), IS_LOV("isLov"), CAT_DESC_UDF_TYPE("cat_desc_udf_type"), CATEGORIES("categories"),
		CAT_DESC_UDF("cat_desc_udf"), ID("id");

		private DeptItemUDFSettingsMetadeta(String udfSetting) {
			this.udfSetting = udfSetting;
		}

		private String udfSetting;

		public String getUdfSetting() {
			return udfSetting;
		}

		public void setUdfSetting(String udfSetting) {
			this.udfSetting = udfSetting;
		}

	}

	public enum CATDESCUDF {
		CAT("6"), DESC("6"), UDFSTRING("10"), UDFNUM("5"), UDFDATE("5");

		private CATDESCUDF(String limit) {
			this.limit = limit;
		}

		private String limit;

		public String getLimit() {
			return limit;
		}

		public void setLimit(String limit) {
			this.limit = limit;
		}

	}

	public interface UDFSetting {
		String[] udfSettingArray = { "CAT", "DESC", "UDFSTRING", "UDFNUM", "UDFDATE" };

	}
}
