package com.supplymint.layer.data.metadata;

public class PurchaseOrderMetaData {

	public interface Article {

		String ID = "id";
		String HL1CODE = "hl1Code";
		String HL1NAME = "hl1Name";
		String HL2CODE = "hl2Code";
		String HL2NAME = "hl2Name";
		String HL3CODE = "hl3Code";
		String HL3NAME = "hl3Name";
		String HL4CODE = "hl4Code";
		String HL4NAME = "hl4Name";
		String MRPSTART = "mrpStart";
		String MRPEND = "mrpEnd";
		String HSN_SAC_CODE = "hsnSacCode";
	}

	public interface Item {
		String ITEM_CODE = "itemCode";
		String ITEM_NAME = "itemName";
		String MRP = "mrp";
		String RSP = "rsp";
		String INVHSNSACMAIN_CODE = "invHsnSacMainCode";
	}

	public interface ItemUdfMapping {

		String ID = "id";
		String hl3Name = "hl3Name";
		String DISPLAY_NAME = "displayName";
		String DESCRIPTION = "description";
		String MAP = "map";
		String EXT = "ext";
		String hl3Code = "hl3Code";
	}

	public interface POStatus {

		String PENDING_STATUS = "PENDING";
		String APPROVED_STATUS = "APPROVED";

		String INACTIVE = "INACTIVE";
		String ACTIVE = "ACTIVE";

	}

	public interface SMPartnerValue {
		String ING_CODE = "1";
		String UOM = "PCS";
		String MATERIAL_TYPE = "F";
		String IS_IMPORTED = "N";
		String IS_VALIDATED = "Y";
		String DOC_CODE = "1298";
	}

}
