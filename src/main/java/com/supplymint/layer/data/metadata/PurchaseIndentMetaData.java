package com.supplymint.layer.data.metadata;

public class PurchaseIndentMetaData {

	public interface HierarchyLevel {
		String ID = "id";
		String HL1NAME = "hl1Name";
		String HL2NAME = "hl2Name";
		String HL3NAME = "hl3Name";
		String HL1CODE = "hl1Code";
		String HL2CODE = "hl2Code";
		String HL3CODE = "hl3Code";

	}

	public interface Transporter {
		String ID = "id";
		String TRANSPORTER_CODE = "transporterCode";
		String TRANSPORTER_NAME = "transporterName";
	}

	public interface Supplier {
		String CODE = "code";
		String SLID = "slId";
		String NAME = "name";
		String ADDRESS = "address";
		String LEADTIME = "leadTime";
		String CITY = "city";
		String GSTIN_STATECODE = "stateCode";
		String GSTIN_NO = "gstInNo";
		String TRANSPORTER_CODE = "transporterCode";
		String TRANSPORTER_NAME = "transporterName";
		String TRADEGRP_CODE = "tradeGrpCode";
		String PURCHASETERM_CODE = "purchaseTermCode";
		String PURCHASETERM_NAME = "purchaseTermName";
	}

	public interface Article {
		String ID = "id";
		String ARTICLE_CODE = "articleCode";
		String ARTICLE_NAME = "articleName";
		String MRP = "mrp";
		String RSP = "rsp";
		String MRPSTART = "mrpStart";
		String MRPEND = "mrpEnd";
		String ITEMCODE = "itemCode";
		String ITEMNAME = "itemName";
		String HSNSACCODE = "hsnSacCode";
	}

	public interface Item {
		String ID = "id";
		String CODE = "code";
		String CAT1 = "cat1";
		String CAT2 = "cat2";
		String CAT3 = "cat3";
		String CAT4 = "cat4";
		String CAT5 = "cat5";
		String CAT6 = "cat6";

		String DESC2 = "desc2";
		String DESC3 = "desc3";
		String DESC4 = "desc4";
		String DESC5 = "desc5";
		String QTY = "qty";

		String ITEMCODE = "iCode";
		String ITEMNAME = "itemName";
		String NETAMOUNT = "netAmount";
		String ACTUALMARKUP = "actualMarkUp";
		String CALCULATEDMARGIN = "calculatedMargin";

	}

	public interface PurchaseTerm {
		String CODE = "code";
		String NAME = "name";
	}

	public interface Formula {

		String B = "B";
		String B1 = "B+1";
		String B12 = "B+1+2";
		String B_12 = "B-1+2";
		String B123 = "B+1+2+3";
		String _B1 = "B-1";
		String B14="B+1+2+3+4";
		String B15="B+1+2+3+4+5";
	}

	public interface PIStatus {

		String STATUS = "PENDING";
		String INACTIVE = "INACTIVE";
	}

	public interface PurchaseOrder {

		String GENERATED_DATE = "generatedDate";
		String ORDER_NO = "orderNo";
		String QTY = "poQuantity";
		String AMOUNT = "poAmount";
		String SUPPLIER_NAME = "slName";
		String SUPPLIER_CODE = "slCode";
		String SUPPLIER_CITYNAME = "slCityName";
		String STATUS = "status";
		String PO_REMARKS = "poRemarks";
		String VALID_FROM = "validFrom";
		String VALID_TO = "validTO";
		String ORDER_DATE = "orderDate";

		String HL1CODE = "hl1Code";
		String HL1NAME = "hl1Name";
		String HL2CODE = "hl2Code";
		String HL2NAME = "hl2Name";
		String HL3CODE = "hl3Code";
		String HL3NAME = "hl3Name";
		String SLCODE = "slCode";
		String SUPPLIER_ADDRESS = "slAddr";
		String PO_TYPE = "poType";

	}

	public interface PILineItem {
		String GST = "gst";
		String BASIC_VALUE = "basic";
		String TAX = "tax";
		String NET_AMOUNT = "netAmount";
		String FIN_CHARGES = "finCharges";
	}

	public interface PurtermCharge {
		String CHARGE_CODE = "chgCode";
		String CHARGE_NAME = "chgName";
		String CHARGE_SEQ = "seq";
		String FIN_FORMULA = "finFormula";
		String FIN_CHARGE_RATE = "finChgRate";
		String FIN_SOURCE = "finSource";
		String GST_COMPONENT = "gstComponent";
		String IS_TAX = "isTax";
		String SIGN = "sign";
		String FIN_CHARGE_OPERATION_LEVEL = "finChgOperationLevel";
		String CALCULATION_BASIS = "calculationBasis";
		String SUBTOTAL = "subTotal";

	}

	public interface SetUDF {
		String ID = "id";
		String UDF_TYPE = "udfType";
		String DISPAYNAME = "displayName";
		String IS_COMPULSARY = "isCompulsary";
		String ORDER_BY = "orderBy";
		String CHECKED = "checked";
		String CODE = "code";
		String NAME = "name";
		String IS_LOV = "isLov";

	}

	public interface Department {

		String CODE = "code";
		String CNAME = "cname";
		String ORDERBY = "orderBy";
	}

	public interface IndtCatDescUDF {

		String ID = "id";
		String CODE = "code";
		String NAME = "name";
		String IS_SHOW = "isShow";
		String CAT_TYPE = "catType";
	}

	public interface DeptItemUDFSettingsMeta {

		String NAME = "udf";
		String CATDESC = "catdesc";
		String DISPLAY_NAME = "displayName";
		String IS_COMPULSORY = "isCompulsory";
		String ORDER_BY = "orderBy";
		String IS_LOV = "isLov";
		String CAT_DESC_UDF_TYPE = "cat_desc_udf_type";
		String CATEGORIES = "categories";
		String CAT_DESC_UDF = "cat_desc_udf";
		String ID = "id";
	}

}
