package com.supplymint.layer.data.setting.entity;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.supplymint.util.DateFormatSerializer;
import com.supplymint.util.StringDateSerializer;

public class FestivalSetting {

	private int id;
	private String name;
	private String festivalType;
	private String festivalList;
	private String isChecked;
	private String userName;
	private String status;
	private String active;
	private String ipAddress;
	private String createdBy;
	@JsonSerialize(using = DateFormatSerializer.class)
	private Date createdOn;
	private String updatedBy;
	@JsonSerialize(using = DateFormatSerializer.class)
	private Date updatedOn;
	private String additional;
//	@JsonSerialize(using = DateFormatSerializer.class)
	private String startDate;
//	@JsonSerialize(using = DateFormatSerializer.class)
	private String endDate;

	public FestivalSetting() {
		super();
	}

	public FestivalSetting(int id, String name, String festivalType, String festivalList, String isChecked,
			String userName, String status, String active, String ipAddress, String createdBy, Date createdOn,
			String updatedBy, Date updatedOn, String additional, String startDate, String endDate) {
		super();
		this.id = id;
		this.name = name;
		this.festivalType = festivalType;
		this.festivalList = festivalList;
		this.isChecked = isChecked;
		this.userName = userName;
		this.status = status;
		this.active = active;
		this.ipAddress = ipAddress;
		this.createdBy = createdBy;
		this.createdOn = createdOn;
		this.updatedBy = updatedBy;
		this.updatedOn = updatedOn;
		this.additional = additional;
		this.startDate = startDate;
		this.endDate = endDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFestivalType() {
		return festivalType;
	}

	public void setFestivalType(String festivalType) {
		this.festivalType = festivalType;
	}

	public String getFestivalList() {
		return festivalList;
	}

	public void setFestivalList(String festivalList) {
		this.festivalList = festivalList;
	}

	public String getIsChecked() {
		return isChecked;
	}

	public void setIsChecked(String isChecked) {
		this.isChecked = isChecked;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getAdditional() {
		return additional;
	}

	public void setAdditional(String additional) {
		this.additional = additional;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	@Override
	public String toString() {
		return "FestivalSetting [id=" + id + ", name=" + name + ", festivalType=" + festivalType + ", festivalList="
				+ festivalList + ", isChecked=" + isChecked + ", userName=" + userName + ", status=" + status
				+ ", active=" + active + ", ipAddress=" + ipAddress + ", createdBy=" + createdBy + ", createdOn="
				+ createdOn + ", updatedBy=" + updatedBy + ", updatedOn=" + updatedOn + ", additional=" + additional
				+ ", startDate=" + startDate + ", endDate=" + endDate + "]";
	}

	

	
	
	
}
