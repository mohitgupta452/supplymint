package com.supplymint.layer.data.setting.entity;

import java.util.List;

public class FestivalSettingMaster {

	List<FestivalSetting> defaultFestival;
	List<FestivalSetting> customFestival;

	public FestivalSettingMaster() {
		super();
		// TODO Auto-generated constructor stub
	}

	public FestivalSettingMaster(List<FestivalSetting> defaultFestival, List<FestivalSetting> customFestival) {
		super();
		this.defaultFestival = defaultFestival;
		this.customFestival = customFestival;
	}

	public List<FestivalSetting> getDefaultFestival() {
		return defaultFestival;
	}

	public void setDefaultFestival(List<FestivalSetting> defaultFestival) {
		this.defaultFestival = defaultFestival;
	}

	public List<FestivalSetting> getCustomFestival() {
		return customFestival;
	}

	public void setCustomFestival(List<FestivalSetting> customFestival) {
		this.customFestival = customFestival;
	}

}
