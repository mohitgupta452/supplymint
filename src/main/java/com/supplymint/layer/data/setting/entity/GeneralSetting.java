package com.supplymint.layer.data.setting.entity;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.supplymint.util.DateFormatSerializer;

public class GeneralSetting {

	private int id;
	private String setting;
	private String generalConfig;
	private String numberFormat;
	private String decimalPlaces;
	private String useSeperator;
	private String currency;
	private String useSymbol;
	private String timeFormat;
	private String dateFormat;
	private String setupValue;
	private String representation;
	private String userName;
	private String role;
	private String status;
	private char active;
	private String ipAddress;
	private String createdBy;
	@JsonSerialize(using = DateFormatSerializer.class)
	private Date createdOn;
	private String updatedBy;
	@JsonSerialize(using = DateFormatSerializer.class)
	private Date updatedOn;
	private String additional;

	public GeneralSetting() {

	}

	public GeneralSetting(int id, String setting, String generalConfig, String numberFormat, String decimalPlaces,
			String useSeperator, String currency, String useSymbol, String timeFormat, String dateFormat,
			String setupValue, String representation, String userName, String role, String status, char active,
			String ipAddress, String createdBy, Date createdOn, String updatedBy, Date updatedOn, String additional) {
		super();
		this.id = id;
		this.setting = setting;
		this.generalConfig = generalConfig;
		this.numberFormat = numberFormat;
		this.decimalPlaces = decimalPlaces;
		this.useSeperator = useSeperator;
		this.currency = currency;
		this.useSymbol = useSymbol;
		this.timeFormat = timeFormat;
		this.dateFormat = dateFormat;
		this.setupValue = setupValue;
		this.representation = representation;
		this.userName = userName;
		this.role = role;
		this.status = status;
		this.active = active;
		this.ipAddress = ipAddress;
		this.createdBy = createdBy;
		this.createdOn = createdOn;
		this.updatedBy = updatedBy;
		this.updatedOn = updatedOn;
		this.additional = additional;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSetting() {
		return setting;
	}

	public void setSetting(String setting) {
		this.setting = setting;
	}

	public String getGeneralConfig() {
		return generalConfig;
	}

	public void setGeneralConfig(String generalConfig) {
		this.generalConfig = generalConfig;
	}

	public String getNumberFormat() {
		return numberFormat;
	}

	public void setNumberFormat(String numberFormat) {
		this.numberFormat = numberFormat;
	}

	public String getDecimalPlaces() {
		return decimalPlaces;
	}

	public void setDecimalPlaces(String decimalPlaces) {
		this.decimalPlaces = decimalPlaces;
	}

	public String getUseSeperator() {
		return useSeperator;
	}

	public void setUseSeperator(String useSeperator) {
		this.useSeperator = useSeperator;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getUseSymbol() {
		return useSymbol;
	}

	public void setUseSymbol(String useSymbol) {
		this.useSymbol = useSymbol;
	}

	public String getTimeFormat() {
		return timeFormat;
	}

	public void setTimeFormat(String timeFormat) {
		this.timeFormat = timeFormat;
	}

	public String getDateFormat() {
		return dateFormat;
	}

	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}

	public String getSetupValue() {
		return setupValue;
	}

	public void setSetupValue(String setupValue) {
		this.setupValue = setupValue;
	}

	public String getRepresentation() {
		return representation;
	}

	public void setRepresentation(String representation) {
		this.representation = representation;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public char getActive() {
		return active;
	}

	public void setActive(char active) {
		this.active = active;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getAdditional() {
		return additional;
	}

	public void setAdditional(String additional) {
		this.additional = additional;
	}

	@Override
	public String toString() {
		return "GeneralSetting [id=" + id + ", setting=" + setting + ", generalConfig=" + generalConfig
				+ ", numberFormat=" + numberFormat + ", decimalPlaces=" + decimalPlaces + ", useSeperator="
				+ useSeperator + ", currency=" + currency + ", useSymbol=" + useSymbol + ", timeFormat=" + timeFormat
				+ ", dateFormat=" + dateFormat + ", setupValue=" + setupValue + ", representation=" + representation
				+ ", userName=" + userName + ", role=" + role + ", status=" + status + ", active=" + active
				+ ", ipAddress=" + ipAddress + ", createdBy=" + createdBy + ", createdOn=" + createdOn + ", updatedBy="
				+ updatedBy + ", updatedOn=" + updatedOn + ", additional=" + additional + "]";
	}
}
