package com.supplymint.layer.data.setting.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.supplymint.layer.data.setting.entity.FestivalSetting;

@Mapper
public interface FestivalSettingMapper {

	int create(FestivalSetting festival);

	int update(FestivalSetting festival);

	@Select("SELECT VALUE FROM SYSTEM_DEFAULT_CONFIG WHERE KEY = #{festivalkey}")
	String getFestivalName(@Param("festivalkey") String festivalkey);

	@Update("UPDATE FESTIVAL_SETTING SET STATUS='FALSE', ACTIVE ='0' where FESTIVAL_TYPE =#{festivalType}")
	int updateStatus(@Param("festivalType") String festivalType);

	@Select("SELECT FESTIVAL_LIST FROM FESTIVAL_SETTING WHERE STATUS = 'TRUE' AND ACTIVE = '1' ")
	String getCheckFestivalName();
}
