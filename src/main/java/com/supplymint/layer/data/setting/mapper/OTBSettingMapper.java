package com.supplymint.layer.data.setting.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.fasterxml.jackson.databind.JsonNode;

/**
 * This interface describes all service for OTB and HLEVEL Configuration
 * @author Prabhakar Srivastava 
 * @author Varun Sharma
 * @Date   10 May 2019
 * @version 1.0
 */
@Mapper
public interface OTBSettingMapper {

	/**
	 * This method describes getting HLEVEL and UDF Data
	 * @param HLEVEL Type
	 * @Table SYSTEM_DEFAULT_CONFIG
	 */
	@Select("SELECT VALUE FROM SYSTEM_DEFAULT_CONFIG WHERE KEY = #{hlevelConfig}")
	String getHPatternD(@Param("hlevelConfig")String hlevelConfig);

	/**
	 * This method describes create HLEVEL
	 * @param HLEVEL Pay load
	 * @Table SYSTEM_DEFAULT_CONFIG
	 */
	@Insert("INSERT INTO SYSTEM_DEFAULT_CONFIG (KEY,VALUE)" + " VALUES ('DEFAULT_HLEVEL_CONFIG',#{jsonResp})")
	int create(@Param("jsonResp") String jsonResp);
	
	/**
	 * This method describes create UDF
	 * @param UDF pay load
	 * @Table SYSTEM_DEFAULT_CONFIG
	 */
	@Insert("INSERT INTO SYSTEM_DEFAULT_CONFIG (KEY,VALUE)" + " VALUES ('DEFAULT_UDF_CONFIG',#{jsonudfResp})")
	int createUDF(@Param("jsonudfResp") String jsonudfResp);

	/**
	 * This method describes validate unique HLEVEL Key
	 * @Table SYSTEM_DEFAULT_CONFIG
	 */
	@Select("SELECT COUNT(KEY) FROM SYSTEM_DEFAULT_CONFIG WHERE KEY='DEFAULT_HLEVEL_CONFIG'")
	int isValidHlevel();
	
	/**
	 * This method describes validate unique UDF Key
	 * @Table SYSTEM_DEFAULT_CONFIG
	 */
	@Select("SELECT COUNT(KEY) FROM SYSTEM_DEFAULT_CONFIG WHERE KEY='DEFAULT_UDF_CONFIG'")
	int isValidUDFlevel();

	/**
	 * This method describes update HLEVEL Configuration
	 * @param DEFAULT_HLEVEL_CONFIG
	 * @Table SYSTEM_DEFAULT_CONFIG
	 */
	@Update("UPDATE SYSTEM_DEFAULT_CONFIG SET VALUE=#{jsonResp} WHERE KEY='DEFAULT_HLEVEL_CONFIG'")
	int update(String jsonResp);
	
	/**
	 * This method describes update UDF Configuration
	 * @param DEFAULT_UDF_CONFIG
	 * @Table SYSTEM_DEFAULT_CONFIG
	 */
	@Update("UPDATE SYSTEM_DEFAULT_CONFIG SET VALUE=#{jsonudfResp} WHERE KEY='DEFAULT_UDF_CONFIG'")
	int updateUDF(String jsonudfResp);

	/**
	 * This method describes get all HLEVEL and UDF Data
	 * @Table SYSTEM_DEFAULT_CONFIG
	 */
	@Select("SELECT VALUE FROM SYSTEM_DEFAULT_CONFIG WHERE KEY = #{defaultHlevelCConfig}")
	String getDefaultAssortment(@Param("defaultHlevelCConfig")String defaultHlevelCConfig);

	/**
	 * This method describes validate unique OTB_USER_ASSORTMENT Key
	 * @Table SYSTEM_DEFAULT_CONFIG
	 */
	@Select("SELECT COUNT(KEY) FROM SYSTEM_DEFAULT_CONFIG WHERE KEY='OTB_USER_ASSORTMENT'")
	int isValidAssortment(JsonNode node);

	/**
	 * This method describes create OTB Configuration
	 * @param OTB pay load
	 * @Table SYSTEM_DEFAULT_CONFIG
	 */
	@Insert("INSERT INTO SYSTEM_DEFAULT_CONFIG (KEY,VALUE)" + " VALUES ('OTB_USER_ASSORTMENT',#{jsonResp})")
	int createAssort(String jsonResp);

	/**
	 * This method describes update OTB Configuration
	 * @param OTB pay load
	 * @Table SYSTEM_DEFAULT_CONFIG
	 */
	@Update("UPDATE SYSTEM_DEFAULT_CONFIG SET VALUE=#{jsonResp} WHERE KEY='OTB_USER_ASSORTMENT'")
	int updateAssort(String jsonResp);

	/**
	 * This method describes get selected OTB Configuration
	 * @param OTB pay load
	 * @Table SYSTEM_DEFAULT_CONFIG
	 */
	@Select("SELECT VALUE FROM SYSTEM_DEFAULT_CONFIG WHERE KEY='OTB_USER_ASSORTMENT'")
	String getUserAssortment(@Param("key") String key);

}
