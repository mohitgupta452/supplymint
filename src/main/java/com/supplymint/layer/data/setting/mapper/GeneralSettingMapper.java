package com.supplymint.layer.data.setting.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.supplymint.layer.data.setting.entity.GeneralSetting;

@Mapper
public interface GeneralSettingMapper {
	
	int create(GeneralSetting generalSetting);
	
	List<GeneralSetting> getAll();
	
	int update(GeneralSetting generalSetting);
	
	@Update("UPDATE GENERAL_SETTING SET STATUS='FALSE' WHERE SETTING='NUMBER_FORMAT' OR SETTING='CURRENCY' OR SETTING='TIME_FORMAT' OR SETTING='DATE_FORMAT'")
	int updateStatus();

	@Select("SELECT SETUP_VALUE FROM GENERAL_SETTING WHERE SETTING='DATE_FORMAT' AND STATUS='TRUE'")
	String getdateFormat();
	
	@Update("UPDATE GENERAL_SETTING SET SETUP_VALUE=#{b} WHERE SETTING='DATE_FORMAT'")
	int updatevalue(Object b);

}
