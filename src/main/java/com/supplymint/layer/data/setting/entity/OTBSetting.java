package com.supplymint.layer.data.setting.entity;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.supplymint.util.DateFormatSerializer;

public class OTBSetting {

	private String HL1NAME;
	private String HL2NAME;
	private String HL3NAME;
	private String HL4NAME;
	private String HL5NAME;
	private String HL6NAME;
	private String UDF1;
	private String UDF2;
	private String UDF3;
	private String UDF4;
	private String UDF5;
	private String UDF6;
	private String status;
	private String active;
	private String ipAddress;
	private String createdBy;
	@JsonSerialize(using = DateFormatSerializer.class)
	private Date createdOn;
	private String updatedBy;
	@JsonSerialize(using = DateFormatSerializer.class)
	private Date updatedOn;
	private String additional;

	public OTBSetting() {

	}

	public OTBSetting(String hL1NAME, String hL2NAME, String hL3NAME, String hL4NAME, String hL5NAME, String hL6NAME,
			String uDF1, String uDF2, String uDF3, String uDF4, String uDF5, String uDF6, String status, String active,
			String ipAddress, String createdBy, Date createdOn, String updatedBy, Date updatedOn, String additional) {
		super();
		HL1NAME = hL1NAME;
		HL2NAME = hL2NAME;
		HL3NAME = hL3NAME;
		HL4NAME = hL4NAME;
		HL5NAME = hL5NAME;
		HL6NAME = hL6NAME;
		UDF1 = uDF1;
		UDF2 = uDF2;
		UDF3 = uDF3;
		UDF4 = uDF4;
		UDF5 = uDF5;
		UDF6 = uDF6;
		this.status = status;
		this.active = active;
		this.ipAddress = ipAddress;
		this.createdBy = createdBy;
		this.createdOn = createdOn;
		this.updatedBy = updatedBy;
		this.updatedOn = updatedOn;
		this.additional = additional;
	}

	public String getHL1NAME() {
		return HL1NAME;
	}

	public void setHL1NAME(String hL1NAME) {
		HL1NAME = hL1NAME;
	}

	public String getHL2NAME() {
		return HL2NAME;
	}

	public void setHL2NAME(String hL2NAME) {
		HL2NAME = hL2NAME;
	}

	public String getHL3NAME() {
		return HL3NAME;
	}

	public void setHL3NAME(String hL3NAME) {
		HL3NAME = hL3NAME;
	}

	public String getHL4NAME() {
		return HL4NAME;
	}

	public void setHL4NAME(String hL4NAME) {
		HL4NAME = hL4NAME;
	}

	public String getHL5NAME() {
		return HL5NAME;
	}

	public void setHL5NAME(String hL5NAME) {
		HL5NAME = hL5NAME;
	}

	public String getHL6NAME() {
		return HL6NAME;
	}

	public void setHL6NAME(String hL6NAME) {
		HL6NAME = hL6NAME;
	}

	public String getUDF1() {
		return UDF1;
	}

	public void setUDF1(String uDF1) {
		UDF1 = uDF1;
	}

	public String getUDF2() {
		return UDF2;
	}

	public void setUDF2(String uDF2) {
		UDF2 = uDF2;
	}

	public String getUDF3() {
		return UDF3;
	}

	public void setUDF3(String uDF3) {
		UDF3 = uDF3;
	}

	public String getUDF4() {
		return UDF4;
	}

	public void setUDF4(String uDF4) {
		UDF4 = uDF4;
	}

	public String getUDF5() {
		return UDF5;
	}

	public void setUDF5(String uDF5) {
		UDF5 = uDF5;
	}

	public String getUDF6() {
		return UDF6;
	}

	public void setUDF6(String uDF6) {
		UDF6 = uDF6;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getAdditional() {
		return additional;
	}

	public void setAdditional(String additional) {
		this.additional = additional;
	}

	@Override
	public String toString() {
		return "OTBSetting [HL1NAME=" + HL1NAME + ", HL2NAME=" + HL2NAME + ", HL3NAME=" + HL3NAME + ", HL4NAME="
				+ HL4NAME + ", HL5NAME=" + HL5NAME + ", HL6NAME=" + HL6NAME + ", UDF1=" + UDF1 + ", UDF2=" + UDF2
				+ ", UDF3=" + UDF3 + ", UDF4=" + UDF4 + ", UDF5=" + UDF5 + ", UDF6=" + UDF6 + ", status=" + status
				+ ", active=" + active + ", ipAddress=" + ipAddress + ", createdBy=" + createdBy + ", createdOn="
				+ createdOn + ", updatedBy=" + updatedBy + ", updatedOn=" + updatedOn + ", additional=" + additional
				+ "]";
	}

}
