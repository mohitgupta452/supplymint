package com.supplymint.layer.data.notification.entity;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.supplymint.util.DateFormatSerializer;

public class MailManager {
	private int id;
	private String module;
	private String subModule;
	private String type;
	private String fromName;
	private String from;
	private String subject;
	private String to;
	private String cc;
	private String bcc;
	private String channel;
	private String status;
	private char active;
	private String ipAddress;
	private String createdBy;
	@JsonSerialize(using = DateFormatSerializer.class)
	private Date createdOn;
	private String updatedBy;
	@JsonSerialize(using = DateFormatSerializer.class)
	private Date updatedOn;
	private String additional;
	private String templateName;
	private String arguments;
	private String orgId;

	public MailManager() {

	}

	public MailManager(int id, String module, String subModule, String type, String fromName, String from,
			String subject, String to, String cc, String bcc, String channel, String status, char active,
			String ipAddress, String createdBy, Date createdOn, String updatedBy, Date updatedOn, String additional,
			String templateName, String arguments, String orgId) {
		super();
		this.id = id;
		this.module = module;
		this.subModule = subModule;
		this.type = type;
		this.fromName = fromName;
		this.from = from;
		this.subject = subject;
		this.to = to;
		this.cc = cc;
		this.bcc = bcc;
		this.channel = channel;
		this.status = status;
		this.active = active;
		this.ipAddress = ipAddress;
		this.createdBy = createdBy;
		this.createdOn = createdOn;
		this.updatedBy = updatedBy;
		this.updatedOn = updatedOn;
		this.additional = additional;
		this.templateName = templateName;
		this.arguments = arguments;
		this.orgId = orgId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public String getSubModule() {
		return subModule;
	}

	public void setSubModule(String subModule) {
		this.subModule = subModule;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getFromName() {
		return fromName;
	}

	public void setFromName(String fromName) {
		this.fromName = fromName;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getCc() {
		return cc;
	}

	public void setCc(String cc) {
		this.cc = cc;
	}

	public String getBcc() {
		return bcc;
	}

	public void setBcc(String bcc) {
		this.bcc = bcc;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public char getActive() {
		return active;
	}

	public void setActive(char active) {
		this.active = active;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getAdditional() {
		return additional;
	}

	public void setAdditional(String additional) {
		this.additional = additional;
	}

	public String getTemplateName() {
		return templateName;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	public String getArguments() {
		return arguments;
	}

	public void setArguments(String arguments) {
		this.arguments = arguments;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	@Override
	public String toString() {
		return "MailManager [id=" + id + ", module=" + module + ", subModule=" + subModule + ", type=" + type
				+ ", fromName=" + fromName + ", from=" + from + ", subject=" + subject + ", to=" + to + ", cc=" + cc
				+ ", bcc=" + bcc + ", channel=" + channel + ", status=" + status + ", active=" + active + ", ipAddress="
				+ ipAddress + ", createdBy=" + createdBy + ", createdOn=" + createdOn + ", updatedBy=" + updatedBy
				+ ", updatedOn=" + updatedOn + ", additional=" + additional + ", templateName=" + templateName
				+ ", arguments=" + arguments + ", orgId=" + orgId + "]";
	}

}
