package com.supplymint.layer.data.notification.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.supplymint.layer.data.notification.entity.DropDown;
import com.supplymint.layer.data.notification.entity.MailManager;
import com.supplymint.layer.data.notification.entity.MailManagerViewLog;

@Mapper
public interface MailManagerMapper {

	MailManager getById(@Param("id") Integer id);

	@Select("SELECT COUNT(*) FROM EMAILCONFIG WHERE ORG_ID = #{orgId}")
	int getAllRecord(@Param("orgId") String orgId);

	List<MailManager> getAll(@Param("offset") Integer offset, @Param("pageSize") Integer pageSize,
			@Param("orgId") String orgId);

	Integer create(MailManager emailConfig);

	Integer update(MailManager emailConfig);

	@Delete("DELETE FROM EMAILCONFIG WHERE ID=#{id} ")
	Integer delete(@Param("id") int id);

	int filterRecord(@Param("module") String module, @Param("subModule") String subModule,
			@Param("configuration") String configuration, @Param("orgId") String orgId);

	List<MailManager> filter(@Param("offset") Integer offset, @Param("pageSize") Integer pageSize,
			@Param("module") String module, @Param("subModule") String subModule,
			@Param("configuration") String configuration, @Param("orgId") String orgId);

	int searchEmailConfigRecord(@Param("search") String search, @Param("orgId") String orgId);

	List<MailManager> searchEmailConfig(@Param("offset") Integer offset, @Param("pageSize") Integer pageSize,
			@Param("search") String search, @Param("orgId") String orgId);

	MailManager getByStatus(@Param("templateName") String templateName, @Param("status") String status,@Param("orgId") String orgId);

	int createEmailActivityLog(MailManagerViewLog mailManagerViewLog);

	DropDown getAllModule(@Param("moduleName") String moduleName);

	DropDown getAllSubModule(@Param("moduleName") String moduleName);

	DropDown getAllConfiguration(@Param("subModuleName") String subModuleName);

	@Select("SELECT COUNT(*) FROM EMAILCONFIG WHERE STATUS='TRUE' AND ACTIVE='1' And ORG_ID = #{orgId}")
	int getEmailStatus(@Param("orgId") String orgId);

	@Update("UPDATE EMAILCONFIG SET ACTIVE=#{active} WHERE STATUS='TRUE' AND ORG_ID =#{orgId}")
	int updateEmailStatus(@Param("active") char active, @Param("orgId") String orgId);

	MailManager getEmailConfiguration(@Param("moduleName") String moduleName,
			@Param("subModuleName") String subModuleName, @Param("configurationProperty") String configurationProperty,
			@Param("orgId") String orgId);

	@Select("SELECT COUNT(*) FROM EMAILCONFIG WHERE MODULE=#{moduleName} AND SUBMODULE=#{subModuleName} AND TYPE=#{configurationProperty} AND ORG_ID=#{orgId} ")
	int isExistModule(@Param("moduleName") String moduleName, @Param("subModuleName") String subModuleName,
			@Param("configurationProperty") String configurationProperty, @Param("orgId") String orgId);

	List<MailManager> getAllEmailData(@Param("orgId") String orgId);

}
