package com.supplymint.layer.data.notification.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.supplymint.layer.data.notification.entity.MailManager;

@Mapper
public interface EmailMapper {

	public MailManager getById(@Param("id") Integer id);

	@Select("SELECT COUNT(*) FROM EMAILCONFIG")
	public int getAllRecord();

	public List<MailManager> getAll(@Param("offset") Integer offset, @Param("pageSize") Integer pageSize);

	public Integer create(MailManager emailConfig);

	public Integer update(MailManager emailConfig);

	@Delete("DELETE FROM EMAILCONFIG WHERE ID=#{id}")
	public Integer delete(@Param("id") int id);

	public int filterRecord(@Param("module") String module, @Param("subModule") String subModule,
			@Param("subject") String subject);

	public List<MailManager> filter(@Param("offset") Integer offset, @Param("pageSize") Integer pageSize,
			@Param("module") String module, @Param("subModule") String subModule, @Param("subject") String subject);

	public int searchEmailConfigRecord(@Param("search") String search);

	public List<MailManager> searchEmailConfig(@Param("offset") Integer offset, @Param("pageSize") Integer pageSize,
			@Param("search") String search);

}
