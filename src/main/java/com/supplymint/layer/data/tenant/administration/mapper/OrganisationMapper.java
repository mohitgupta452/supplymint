package com.supplymint.layer.data.tenant.administration.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import com.supplymint.layer.data.tenant.administration.entity.ADMOrganisation;

@Mapper
public interface OrganisationMapper {

	Integer create(ADMOrganisation admOrganisation);

	ADMOrganisation findById(Integer orgId);

	List<ADMOrganisation> findAll(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize);

	Integer update(ADMOrganisation admOrganisation);

	@Delete("DELETE from ADM_ORGANISATION WHERE ORG_ID = #{id}")
	Integer removeById(Integer id);

	@Select("SELECT COUNT(*) FROM ADM_ORGANISATION")
	Integer record();

	List<ADMOrganisation> searchAll(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("search") String search);

	int searchRecord(@Param("search") String search);

	List<ADMOrganisation> getAllRecord();

	List<ADMOrganisation> filter(ADMOrganisation admOrganisation);

	@Select("SELECT COUNT(*) FROM ADM_ORGANISATION WHERE CONT_EMAIL = #{contEmail} OR CONT_NUMBER=#{contNumber}")
	Integer validationCheck(ADMOrganisation admOrganisation);

	@Select("SELECT CONT_EMAIL as contEmail, CONT_NUMBER as contNumber FROM ADM_ORGANISATION WHERE "
			+ "lower(CONT_EMAIL) = lower(#{contEmail}) OR CONT_NUMBER = #{contNumber}")
	List<ADMOrganisation> checkOrgExistance(ADMOrganisation admOrganisation);

	@Select("SELECT CONT_EMAIL as contEmail, CONT_NUMBER as contNumber FROM ADM_ORGANISATION"
			+ " WHERE (lower(CONT_EMAIL) = lower(#{contEmail}) OR CONT_NUMBER = #{contNumber}) AND ORG_ID != #{orgID}")
	List<ADMOrganisation> checkOrgUpdateExistance(ADMOrganisation admOrganisation);

	Integer filterRecord(ADMOrganisation admOrganisation);

	List<ADMOrganisation> getAllData();

	@Select("SELECT CONT_EMAIL as contEmail, CONT_NUMBER as contNumber FROM ADM_ORGANISATION WHERE "
			+ "lower(CONT_EMAIL) = lower(#{contEmail}) OR CONT_NUMBER = #{contNumber}")
	List<ADMOrganisation> checkOrgExistanceValidation(@Param("contEmail") String contEmail,
			@Param("contNumber") String contNumber);

}
