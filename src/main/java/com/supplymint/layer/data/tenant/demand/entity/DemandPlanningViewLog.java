package com.supplymint.layer.data.tenant.demand.entity;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.supplymint.util.DateFormatSerializer;
import com.supplymint.util.StringDateSerializer;

public class DemandPlanningViewLog {
	private String id;
	private String assortment;
	@JsonSerialize(using = StringDateSerializer.class)
	private String startDate;
	@JsonSerialize(using = StringDateSerializer.class)
	private String endDate;
	private String frequency;
	private String reportJson;
	@JsonSerialize(using = DateFormatSerializer.class)
	private Date generatedOn;
	private String generatedOutputJson;
	private String generatedBy;
	private String email;
	private String lastSummary;
	private String bucketPath;
	private String bucketKey;
	private String folderName;
	private String downloadUrl;
	private String isDownload;
	private String isViewedGraph;
	private String isViewedTabular;
	private String isActiveSession;
	private String repetition;
	private String userName;
	private String status;
	private char active;
	private String ipAddress;
	private String createdBy;
	@JsonSerialize(using = DateFormatSerializer.class)
	private Date createdOn;
	private String updatedBy;
	@JsonSerialize(using = DateFormatSerializer.class)
	private Date updatedOn;
	private String additional;

	public DemandPlanningViewLog() {

	}

	public DemandPlanningViewLog(String id, String assortment, String startDate, String endDate, String frequency,
			String reportJson, Date generatedOn, String generatedOutputJson, String generatedBy, String email,
			String lastSummary, String bucketPath, String bucketKey, String folderName, String downloadUrl,
			String isDownload, String isViewedGraph, String isViewedTabular, String isActiveSession, String repetition,
			String userName, String status, char active, String ipAddress, String createdBy, Date createdOn,
			String updatedBy, Date updatedOn, String additional) {
		super();
		this.id = id;
		this.assortment = assortment;
		this.startDate = startDate;
		this.endDate = endDate;
		this.frequency = frequency;
		this.reportJson = reportJson;
		this.generatedOn = generatedOn;
		this.generatedOutputJson = generatedOutputJson;
		this.generatedBy = generatedBy;
		this.email = email;
		this.lastSummary = lastSummary;
		this.bucketPath = bucketPath;
		this.bucketKey = bucketKey;
		this.folderName = folderName;
		this.downloadUrl = downloadUrl;
		this.isDownload = isDownload;
		this.isViewedGraph = isViewedGraph;
		this.isViewedTabular = isViewedTabular;
		this.isActiveSession = isActiveSession;
		this.repetition = repetition;
		this.userName = userName;
		this.status = status;
		this.active = active;
		this.ipAddress = ipAddress;
		this.createdBy = createdBy;
		this.createdOn = createdOn;
		this.updatedBy = updatedBy;
		this.updatedOn = updatedOn;
		this.additional = additional;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAssortment() {
		return assortment;
	}

	public void setAssortment(String assortment) {
		this.assortment = assortment;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getFrequency() {
		return frequency;
	}

	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}

	public String getReportJson() {
		return reportJson;
	}

	public void setReportJson(String reportJson) {
		this.reportJson = reportJson;
	}

	public Date getGeneratedOn() {
		return generatedOn;
	}

	public void setGeneratedOn(Date generatedOn) {
		this.generatedOn = generatedOn;
	}

	public String getGeneratedOutputJson() {
		return generatedOutputJson;
	}

	public void setGeneratedOutputJson(String generatedOutputJson) {
		this.generatedOutputJson = generatedOutputJson;
	}

	public String getGeneratedBy() {
		return generatedBy;
	}

	public void setGeneratedBy(String generatedBy) {
		this.generatedBy = generatedBy;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLastSummary() {
		return lastSummary;
	}

	public void setLastSummary(String lastSummary) {
		this.lastSummary = lastSummary;
	}

	public String getBucketPath() {
		return bucketPath;
	}

	public void setBucketPath(String bucketPath) {
		this.bucketPath = bucketPath;
	}

	public String getBucketKey() {
		return bucketKey;
	}

	public void setBucketKey(String bucketKey) {
		this.bucketKey = bucketKey;
	}

	public String getFolderName() {
		return folderName;
	}

	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}

	public String getDownloadUrl() {
		return downloadUrl;
	}

	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}

	public String getIsDownload() {
		return isDownload;
	}

	public void setIsDownload(String isDownload) {
		this.isDownload = isDownload;
	}

	public String getIsViewedGraph() {
		return isViewedGraph;
	}

	public void setIsViewedGraph(String isViewedGraph) {
		this.isViewedGraph = isViewedGraph;
	}

	public String getIsViewedTabular() {
		return isViewedTabular;
	}

	public void setIsViewedTabular(String isViewedTabular) {
		this.isViewedTabular = isViewedTabular;
	}

	public String getIsActiveSession() {
		return isActiveSession;
	}

	public void setIsActiveSession(String isActiveSession) {
		this.isActiveSession = isActiveSession;
	}

	public String getRepetition() {
		return repetition;
	}

	public void setRepetition(String repetition) {
		this.repetition = repetition;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public char getActive() {
		return active;
	}

	public void setActive(char active) {
		this.active = active;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getAdditional() {
		return additional;
	}

	public void setAdditional(String additional) {
		this.additional = additional;
	}

	@Override
	public String toString() {
		return "DemandPlanningViewLog [id=" + id + ", assortment=" + assortment + ", startDate=" + startDate
				+ ", endDate=" + endDate + ", frequency=" + frequency + ", reportJson=" + reportJson + ", generatedOn="
				+ generatedOn + ", generatedOutputJson=" + generatedOutputJson + ", generatedBy=" + generatedBy
				+ ", email=" + email + ", lastSummary=" + lastSummary + ", bucketPath=" + bucketPath + ", bucketKey="
				+ bucketKey + ", folderName=" + folderName + ", downloadUrl=" + downloadUrl + ", isDownload="
				+ isDownload + ", isViewedGraph=" + isViewedGraph + ", isViewedTabular=" + isViewedTabular
				+ ", isActiveSession=" + isActiveSession + ", repetition=" + repetition + ", userName=" + userName
				+ ", status=" + status + ", active=" + active + ", ipAddress=" + ipAddress + ", createdBy=" + createdBy
				+ ", createdOn=" + createdOn + ", updatedBy=" + updatedBy + ", updatedOn=" + updatedOn + ", additional="
				+ additional + "]";
	}

}
