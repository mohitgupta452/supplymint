package com.supplymint.layer.data.tenant.demand.entity;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.supplymint.util.DateFormatSerializer;

public class AssortmentViewLog {

	private int id;
	private String hierarchyPattern;
	private String pattern;
	private int itemCount;
	private String status;
	private char active;
	private String ipAddress;
	private String createdBy;
	@JsonSerialize(using = DateFormatSerializer.class)
	private Date createdOn;
	private String updatedBy;
	private String updatedOn;
	private String additional;
	private String hl1Name;
	private String hl2Name;
	private String hl3Name;
	private String hl4Name;
	private String assortmentCode;

	public AssortmentViewLog() {

	}

	public AssortmentViewLog(int id, String hierarchyPattern, String pattern, int itemCount, String status, char active,
			String ipAddress, String createdBy, Date createdOn, String updatedBy, String updatedOn, String additional,
			String hl1Name, String hl2Name, String hl3Name, String hl4Name, String assortmentCode) {
		super();
		this.id = id;
		this.hierarchyPattern = hierarchyPattern;
		this.pattern = pattern;
		this.itemCount = itemCount;
		this.status = status;
		this.active = active;
		this.ipAddress = ipAddress;
		this.createdBy = createdBy;
		this.createdOn = createdOn;
		this.updatedBy = updatedBy;
		this.updatedOn = updatedOn;
		this.additional = additional;
		this.hl1Name = hl1Name;
		this.hl2Name = hl2Name;
		this.hl3Name = hl3Name;
		this.hl4Name = hl4Name;
		this.assortmentCode = assortmentCode;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getHierarchyPattern() {
		return hierarchyPattern;
	}

	public void setHierarchyPattern(String hierarchyPattern) {
		this.hierarchyPattern = hierarchyPattern;
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	public int getItemCount() {
		return itemCount;
	}

	public void setItemCount(int itemCount) {
		this.itemCount = itemCount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public char getActive() {
		return active;
	}

	public void setActive(char active) {
		this.active = active;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(String updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getAdditional() {
		return additional;
	}

	public void setAdditional(String additional) {
		this.additional = additional;
	}

	public String getHl1Name() {
		return hl1Name;
	}

	public void setHl1Name(String hl1Name) {
		this.hl1Name = hl1Name;
	}

	public String getHl2Name() {
		return hl2Name;
	}

	public void setHl2Name(String hl2Name) {
		this.hl2Name = hl2Name;
	}

	public String getHl3Name() {
		return hl3Name;
	}

	public void setHl3Name(String hl3Name) {
		this.hl3Name = hl3Name;
	}

	public String getHl4Name() {
		return hl4Name;
	}

	public void setHl4Name(String hl4Name) {
		this.hl4Name = hl4Name;
	}

	public String getAssortmentCode() {
		return assortmentCode;
	}

	public void setAssortmentCode(String assortmentCode) {
		this.assortmentCode = assortmentCode;
	}

	@Override
	public String toString() {
		return "AssortmentViewLog [id=" + id + ", hierarchyPattern=" + hierarchyPattern + ", pattern=" + pattern
				+ ", itemCount=" + itemCount + ", status=" + status + ", active=" + active + ", ipAddress=" + ipAddress
				+ ", createdBy=" + createdBy + ", createdOn=" + createdOn + ", updatedBy=" + updatedBy + ", updatedOn="
				+ updatedOn + ", additional=" + additional + ", hl1Name=" + hl1Name + ", hl2Name=" + hl2Name
				+ ", hl3Name=" + hl3Name + ", hl4Name=" + hl4Name + ", assortmentCode=" + assortmentCode + "]";
	}

}
