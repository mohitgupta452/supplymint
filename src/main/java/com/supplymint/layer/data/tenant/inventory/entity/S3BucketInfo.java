package com.supplymint.layer.data.tenant.inventory.entity;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.supplymint.util.DateFormatSerializer;

public class S3BucketInfo {

	private int id;
	private String s3bucketName;
	private String bucketPath;
	private String accessKeyId;
	private String secureAccessKeyId;
	private String bucketKey;
	private String enterprise;
	private String outputFileName;
	private String url;
	private String time;
	private String status;
	private char active;
	private String ipAddress;
	private String createdBy;
	private String jobName;
	@JsonSerialize(using = DateFormatSerializer.class)
	private Date createdTime;
	private String updatedBy;
	@JsonSerialize(using = DateFormatSerializer.class)
	private Date updationTime;
	private String additional;
	private String type;
	private String createZip;
	private String manualTransferPath;
	private int reqMailCount;
	private String isManualTransfer;

	private String jobArguments;
	private String orgId;
	private int enterpriseId;

	public S3BucketInfo() {
		super();
	}

	public S3BucketInfo(int id, String s3bucketName, String bucketPath, String accessKeyId, String secureAccessKeyId,
			String bucketKey, String enterprise, String outputFileName, String url, String time, String status,
			char active, String ipAddress, String createdBy, String jobName, Date createdTime, String updatedBy,
			Date updationTime, String additional, String type, String createZip, String manualTransferPath,
			int reqMailCount, String isManualTransfer, String jobArguments, String orgId, int enterpriseId) {
		super();
		this.id = id;
		this.s3bucketName = s3bucketName;
		this.bucketPath = bucketPath;
		this.accessKeyId = accessKeyId;
		this.secureAccessKeyId = secureAccessKeyId;
		this.bucketKey = bucketKey;
		this.enterprise = enterprise;
		this.outputFileName = outputFileName;
		this.url = url;
		this.time = time;
		this.status = status;
		this.active = active;
		this.ipAddress = ipAddress;
		this.createdBy = createdBy;
		this.jobName = jobName;
		this.createdTime = createdTime;
		this.updatedBy = updatedBy;
		this.updationTime = updationTime;
		this.additional = additional;
		this.type = type;
		this.createZip = createZip;
		this.manualTransferPath = manualTransferPath;
		this.reqMailCount = reqMailCount;
		this.isManualTransfer = isManualTransfer;
		this.jobArguments = jobArguments;
		this.orgId = orgId;
		this.enterpriseId = enterpriseId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getS3bucketName() {
		return s3bucketName;
	}

	public void setS3bucketName(String s3bucketName) {
		this.s3bucketName = s3bucketName;
	}

	public String getBucketPath() {
		return bucketPath;
	}

	public void setBucketPath(String bucketPath) {
		this.bucketPath = bucketPath;
	}

	public String getAccessKeyId() {
		return accessKeyId;
	}

	public void setAccessKeyId(String accessKeyId) {
		this.accessKeyId = accessKeyId;
	}

	public String getSecureAccessKeyId() {
		return secureAccessKeyId;
	}

	public void setSecureAccessKeyId(String secureAccessKeyId) {
		this.secureAccessKeyId = secureAccessKeyId;
	}

	public String getBucketKey() {
		return bucketKey;
	}

	public void setBucketKey(String bucketKey) {
		this.bucketKey = bucketKey;
	}

	public String getEnterprise() {
		return enterprise;
	}

	public void setEnterprise(String enterprise) {
		this.enterprise = enterprise;
	}

	public String getOutputFileName() {
		return outputFileName;
	}

	public void setOutputFileName(String outputFileName) {
		this.outputFileName = outputFileName;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public char getActive() {
		return active;
	}

	public void setActive(char active) {
		this.active = active;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public Date getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdationTime() {
		return updationTime;
	}

	public void setUpdationTime(Date updationTime) {
		this.updationTime = updationTime;
	}

	public String getAdditional() {
		return additional;
	}

	public void setAdditional(String additional) {
		this.additional = additional;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCreateZip() {
		return createZip;
	}

	public void setCreateZip(String createZip) {
		this.createZip = createZip;
	}

	public String getManualTransferPath() {
		return manualTransferPath;
	}

	public void setManualTransferPath(String manualTransferPath) {
		this.manualTransferPath = manualTransferPath;
	}

	public int getReqMailCount() {
		return reqMailCount;
	}

	public void setReqMailCount(int reqMailCount) {
		this.reqMailCount = reqMailCount;
	}

	public String getIsManualTransfer() {
		return isManualTransfer;
	}

	public void setIsManualTransfer(String isManualTransfer) {
		this.isManualTransfer = isManualTransfer;
	}

	public String getJobArguments() {
		return jobArguments;
	}

	public void setJobArguments(String jobArguments) {
		this.jobArguments = jobArguments;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public int getEnterpriseId() {
		return enterpriseId;
	}

	public void setEnterpriseId(int enterpriseId) {
		this.enterpriseId = enterpriseId;
	}

	@Override
	public String toString() {
		return "S3BucketInfo [id=" + id + ", s3bucketName=" + s3bucketName + ", bucketPath=" + bucketPath
				+ ", accessKeyId=" + accessKeyId + ", secureAccessKeyId=" + secureAccessKeyId + ", bucketKey="
				+ bucketKey + ", enterprise=" + enterprise + ", outputFileName=" + outputFileName + ", url=" + url
				+ ", time=" + time + ", status=" + status + ", active=" + active + ", ipAddress=" + ipAddress
				+ ", createdBy=" + createdBy + ", jobName=" + jobName + ", createdTime=" + createdTime + ", updatedBy="
				+ updatedBy + ", updationTime=" + updationTime + ", additional=" + additional + ", type=" + type
				+ ", createZip=" + createZip + ", manualTransferPath=" + manualTransferPath + ", reqMailCount="
				+ reqMailCount + ", isManualTransfer=" + isManualTransfer + ", jobArguments=" + jobArguments
				+ ", orgId=" + orgId + ", enterpriseId=" + enterpriseId + "]";
	}

}
