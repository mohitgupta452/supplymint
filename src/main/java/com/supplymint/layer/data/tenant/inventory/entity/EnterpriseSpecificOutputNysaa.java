package com.supplymint.layer.data.tenant.inventory.entity;

public class EnterpriseSpecificOutputNysaa {

	private String SITEALIAS;
	private String DEPARTMENT;
	private String RSP;
	private String ASSORTMNT;
	private String WEEK_SL_Q;
	private String LM_SL_Q;
	private String MTD_SL_Q;
	private String AVERAGE_WEEK_SALE_QTY;
	private String AVERAGE_CURRENT_MONTH_SALE;
	private String AVERAGE_LAST_MONTH_SALE;
	private String MAX_OF_ABOVE_DATA_AVG_SALES;
	private String ST_Q;
	private String PACK_PEND_Q;
	private String IN_TRAN_Q;
	private String TOQTY;
	private String STR_PLAN;
	private String MBQ_QTY;
	private String AUTO_MBQ;
	private String ACTUAL_STOCK;
	private String ACTUAL_STR;
	private String REVISED_MBQ;
	private String REVISED_AUTO_MBQ;
	private String REQ1;
	private String REQ2;
	private String FINALREQUIREMENT;

	private String ICODE;
	private String ASSORTMNT1;
	private String WH_CODE;
	private String WHQTY;
	private String PACK_SIZE;
	private String _40_DAYS_MBQ;
	private String LAST_IN_RATE;
	private String SIZE;
	private String COLOR;
	private String VENDOR_NAME;
	private String SEG;
	private String OLD_BARCODE_RANK;
	private String FINALREQUIREMENT1;
	private String ACTUAL_STR1;
	private String FINALALLOCATIONQTY;
	private String UPDATEDREQUIREMENT;
	private String RANK;

	public EnterpriseSpecificOutputNysaa() {
		// TODO Auto-generated constructor stub
	}

	public EnterpriseSpecificOutputNysaa(String sITEALIAS, String dEPARTMENT, String rSP, String aSSORTMNT,
			String wEEK_SL_Q, String lM_SL_Q, String mTD_SL_Q, String aVERAGE_WEEK_SALE_QTY,
			String aVERAGE_CURRENT_MONTH_SALE, String aVERAGE_LAST_MONTH_SALE, String mAX_OF_ABOVE_DATA_AVG_SALES,
			String sT_Q, String pACK_PEND_Q, String iN_TRAN_Q, String tOQTY, String sTR_PLAN, String mBQ_QTY,
			String aUTO_MBQ, String aCTUAL_STOCK, String aCTUAL_STR, String rEVISED_MBQ, String rEVISED_AUTO_MBQ,
			String rEQ1, String rEQ2, String fINALREQUIREMENT, String iCODE, String aSSORTMNT1, String wH_CODE,
			String wHQTY, String pACK_SIZE, String _40_DAYS_MBQ, String lAST_IN_RATE, String sIZE, String cOLOR,
			String vENDOR_NAME, String sEG, String oLD_BARCODE_RANK, String fINALREQUIREMENT1, String aCTUAL_STR1,
			String fINALALLOCATIONQTY, String uPDATEDREQUIREMENT, String rANK) {
		super();
		SITEALIAS = sITEALIAS;
		DEPARTMENT = dEPARTMENT;
		RSP = rSP;
		ASSORTMNT = aSSORTMNT;
		WEEK_SL_Q = wEEK_SL_Q;
		LM_SL_Q = lM_SL_Q;
		MTD_SL_Q = mTD_SL_Q;
		AVERAGE_WEEK_SALE_QTY = aVERAGE_WEEK_SALE_QTY;
		AVERAGE_CURRENT_MONTH_SALE = aVERAGE_CURRENT_MONTH_SALE;
		AVERAGE_LAST_MONTH_SALE = aVERAGE_LAST_MONTH_SALE;
		MAX_OF_ABOVE_DATA_AVG_SALES = mAX_OF_ABOVE_DATA_AVG_SALES;
		ST_Q = sT_Q;
		PACK_PEND_Q = pACK_PEND_Q;
		IN_TRAN_Q = iN_TRAN_Q;
		TOQTY = tOQTY;
		STR_PLAN = sTR_PLAN;
		MBQ_QTY = mBQ_QTY;
		AUTO_MBQ = aUTO_MBQ;
		ACTUAL_STOCK = aCTUAL_STOCK;
		ACTUAL_STR = aCTUAL_STR;
		REVISED_MBQ = rEVISED_MBQ;
		REVISED_AUTO_MBQ = rEVISED_AUTO_MBQ;
		REQ1 = rEQ1;
		REQ2 = rEQ2;
		FINALREQUIREMENT = fINALREQUIREMENT;
		ICODE = iCODE;
		ASSORTMNT1 = aSSORTMNT1;
		WH_CODE = wH_CODE;
		WHQTY = wHQTY;
		PACK_SIZE = pACK_SIZE;
		this._40_DAYS_MBQ = _40_DAYS_MBQ;
		LAST_IN_RATE = lAST_IN_RATE;
		SIZE = sIZE;
		COLOR = cOLOR;
		VENDOR_NAME = vENDOR_NAME;
		SEG = sEG;
		OLD_BARCODE_RANK = oLD_BARCODE_RANK;
		FINALREQUIREMENT1 = fINALREQUIREMENT1;
		ACTUAL_STR1 = aCTUAL_STR1;
		FINALALLOCATIONQTY = fINALALLOCATIONQTY;
		UPDATEDREQUIREMENT = uPDATEDREQUIREMENT;
		RANK = rANK;
	}

	public String getSITEALIAS() {
		return SITEALIAS;
	}

	public void setSITEALIAS(String sITEALIAS) {
		SITEALIAS = sITEALIAS;
	}

	public String getDEPARTMENT() {
		return DEPARTMENT;
	}

	public void setDEPARTMENT(String dEPARTMENT) {
		DEPARTMENT = dEPARTMENT;
	}

	public String getRSP() {
		return RSP;
	}

	public void setRSP(String rSP) {
		RSP = rSP;
	}

	public String getASSORTMNT() {
		return ASSORTMNT;
	}

	public void setASSORTMNT(String aSSORTMNT) {
		ASSORTMNT = aSSORTMNT;
	}

	public String getWEEK_SL_Q() {
		return WEEK_SL_Q;
	}

	public void setWEEK_SL_Q(String wEEK_SL_Q) {
		WEEK_SL_Q = wEEK_SL_Q;
	}

	public String getLM_SL_Q() {
		return LM_SL_Q;
	}

	public void setLM_SL_Q(String lM_SL_Q) {
		LM_SL_Q = lM_SL_Q;
	}

	public String getMTD_SL_Q() {
		return MTD_SL_Q;
	}

	public void setMTD_SL_Q(String mTD_SL_Q) {
		MTD_SL_Q = mTD_SL_Q;
	}

	public String getAVERAGE_WEEK_SALE_QTY() {
		return AVERAGE_WEEK_SALE_QTY;
	}

	public void setAVERAGE_WEEK_SALE_QTY(String aVERAGE_WEEK_SALE_QTY) {
		AVERAGE_WEEK_SALE_QTY = aVERAGE_WEEK_SALE_QTY;
	}

	public String getAVERAGE_CURRENT_MONTH_SALE() {
		return AVERAGE_CURRENT_MONTH_SALE;
	}

	public void setAVERAGE_CURRENT_MONTH_SALE(String aVERAGE_CURRENT_MONTH_SALE) {
		AVERAGE_CURRENT_MONTH_SALE = aVERAGE_CURRENT_MONTH_SALE;
	}

	public String getAVERAGE_LAST_MONTH_SALE() {
		return AVERAGE_LAST_MONTH_SALE;
	}

	public void setAVERAGE_LAST_MONTH_SALE(String aVERAGE_LAST_MONTH_SALE) {
		AVERAGE_LAST_MONTH_SALE = aVERAGE_LAST_MONTH_SALE;
	}

	public String getMAX_OF_ABOVE_DATA_AVG_SALES() {
		return MAX_OF_ABOVE_DATA_AVG_SALES;
	}

	public void setMAX_OF_ABOVE_DATA_AVG_SALES(String mAX_OF_ABOVE_DATA_AVG_SALES) {
		MAX_OF_ABOVE_DATA_AVG_SALES = mAX_OF_ABOVE_DATA_AVG_SALES;
	}

	public String getST_Q() {
		return ST_Q;
	}

	public void setST_Q(String sT_Q) {
		ST_Q = sT_Q;
	}

	public String getPACK_PEND_Q() {
		return PACK_PEND_Q;
	}

	public void setPACK_PEND_Q(String pACK_PEND_Q) {
		PACK_PEND_Q = pACK_PEND_Q;
	}

	public String getIN_TRAN_Q() {
		return IN_TRAN_Q;
	}

	public void setIN_TRAN_Q(String iN_TRAN_Q) {
		IN_TRAN_Q = iN_TRAN_Q;
	}

	public String getTOQTY() {
		return TOQTY;
	}

	public void setTOQTY(String tOQTY) {
		TOQTY = tOQTY;
	}

	public String getSTR_PLAN() {
		return STR_PLAN;
	}

	public void setSTR_PLAN(String sTR_PLAN) {
		STR_PLAN = sTR_PLAN;
	}

	public String getMBQ_QTY() {
		return MBQ_QTY;
	}

	public void setMBQ_QTY(String mBQ_QTY) {
		MBQ_QTY = mBQ_QTY;
	}

	public String getAUTO_MBQ() {
		return AUTO_MBQ;
	}

	public void setAUTO_MBQ(String aUTO_MBQ) {
		AUTO_MBQ = aUTO_MBQ;
	}

	public String getACTUAL_STOCK() {
		return ACTUAL_STOCK;
	}

	public void setACTUAL_STOCK(String aCTUAL_STOCK) {
		ACTUAL_STOCK = aCTUAL_STOCK;
	}

	public String getACTUAL_STR() {
		return ACTUAL_STR;
	}

	public void setACTUAL_STR(String aCTUAL_STR) {
		ACTUAL_STR = aCTUAL_STR;
	}

	public String getREVISED_MBQ() {
		return REVISED_MBQ;
	}

	public void setREVISED_MBQ(String rEVISED_MBQ) {
		REVISED_MBQ = rEVISED_MBQ;
	}

	public String getREVISED_AUTO_MBQ() {
		return REVISED_AUTO_MBQ;
	}

	public void setREVISED_AUTO_MBQ(String rEVISED_AUTO_MBQ) {
		REVISED_AUTO_MBQ = rEVISED_AUTO_MBQ;
	}

	public String getREQ1() {
		return REQ1;
	}

	public void setREQ1(String rEQ1) {
		REQ1 = rEQ1;
	}

	public String getREQ2() {
		return REQ2;
	}

	public void setREQ2(String rEQ2) {
		REQ2 = rEQ2;
	}

	public String getFINALREQUIREMENT() {
		return FINALREQUIREMENT;
	}

	public void setFINALREQUIREMENT(String fINALREQUIREMENT) {
		FINALREQUIREMENT = fINALREQUIREMENT;
	}

	public String getICODE() {
		return ICODE;
	}

	public void setICODE(String iCODE) {
		ICODE = iCODE;
	}

	public String getASSORTMNT1() {
		return ASSORTMNT1;
	}

	public void setASSORTMNT1(String aSSORTMNT1) {
		ASSORTMNT1 = aSSORTMNT1;
	}

	public String getWH_CODE() {
		return WH_CODE;
	}

	public void setWH_CODE(String wH_CODE) {
		WH_CODE = wH_CODE;
	}

	public String getWHQTY() {
		return WHQTY;
	}

	public void setWHQTY(String wHQTY) {
		WHQTY = wHQTY;
	}

	public String getPACK_SIZE() {
		return PACK_SIZE;
	}

	public void setPACK_SIZE(String pACK_SIZE) {
		PACK_SIZE = pACK_SIZE;
	}

	public String get_40_DAYS_MBQ() {
		return _40_DAYS_MBQ;
	}

	public void set_40_DAYS_MBQ(String _40_DAYS_MBQ) {
		this._40_DAYS_MBQ = _40_DAYS_MBQ;
	}

	public String getLAST_IN_RATE() {
		return LAST_IN_RATE;
	}

	public void setLAST_IN_RATE(String lAST_IN_RATE) {
		LAST_IN_RATE = lAST_IN_RATE;
	}

	public String getSIZE() {
		return SIZE;
	}

	public void setSIZE(String sIZE) {
		SIZE = sIZE;
	}

	public String getCOLOR() {
		return COLOR;
	}

	public void setCOLOR(String cOLOR) {
		COLOR = cOLOR;
	}

	public String getVENDOR_NAME() {
		return VENDOR_NAME;
	}

	public void setVENDOR_NAME(String vENDOR_NAME) {
		VENDOR_NAME = vENDOR_NAME;
	}

	public String getSEG() {
		return SEG;
	}

	public void setSEG(String sEG) {
		SEG = sEG;
	}

	public String getOLD_BARCODE_RANK() {
		return OLD_BARCODE_RANK;
	}

	public void setOLD_BARCODE_RANK(String oLD_BARCODE_RANK) {
		OLD_BARCODE_RANK = oLD_BARCODE_RANK;
	}

	public String getFINALREQUIREMENT1() {
		return FINALREQUIREMENT1;
	}

	public void setFINALREQUIREMENT1(String fINALREQUIREMENT1) {
		FINALREQUIREMENT1 = fINALREQUIREMENT1;
	}

	public String getACTUAL_STR1() {
		return ACTUAL_STR1;
	}

	public void setACTUAL_STR1(String aCTUAL_STR1) {
		ACTUAL_STR1 = aCTUAL_STR1;
	}

	public String getFINALALLOCATIONQTY() {
		return FINALALLOCATIONQTY;
	}

	public void setFINALALLOCATIONQTY(String fINALALLOCATIONQTY) {
		FINALALLOCATIONQTY = fINALALLOCATIONQTY;
	}

	public String getUPDATEDREQUIREMENT() {
		return UPDATEDREQUIREMENT;
	}

	public void setUPDATEDREQUIREMENT(String uPDATEDREQUIREMENT) {
		UPDATEDREQUIREMENT = uPDATEDREQUIREMENT;
	}

	public String getRANK() {
		return RANK;
	}

	public void setRANK(String rANK) {
		RANK = rANK;
	}

	@Override
	public String toString() {
		return "EnterpriseSpecificOutputNysaa [SITEALIAS=" + SITEALIAS + ", DEPARTMENT=" + DEPARTMENT + ", RSP=" + RSP
				+ ", ASSORTMNT=" + ASSORTMNT + ", WEEK_SL_Q=" + WEEK_SL_Q + ", LM_SL_Q=" + LM_SL_Q + ", MTD_SL_Q="
				+ MTD_SL_Q + ", AVERAGE_WEEK_SALE_QTY=" + AVERAGE_WEEK_SALE_QTY + ", AVERAGE_CURRENT_MONTH_SALE="
				+ AVERAGE_CURRENT_MONTH_SALE + ", AVERAGE_LAST_MONTH_SALE=" + AVERAGE_LAST_MONTH_SALE
				+ ", MAX_OF_ABOVE_DATA_AVG_SALES=" + MAX_OF_ABOVE_DATA_AVG_SALES + ", ST_Q=" + ST_Q + ", PACK_PEND_Q="
				+ PACK_PEND_Q + ", IN_TRAN_Q=" + IN_TRAN_Q + ", TOQTY=" + TOQTY + ", STR_PLAN=" + STR_PLAN
				+ ", MBQ_QTY=" + MBQ_QTY + ", AUTO_MBQ=" + AUTO_MBQ + ", ACTUAL_STOCK=" + ACTUAL_STOCK + ", ACTUAL_STR="
				+ ACTUAL_STR + ", REVISED_MBQ=" + REVISED_MBQ + ", REVISED_AUTO_MBQ=" + REVISED_AUTO_MBQ + ", REQ1="
				+ REQ1 + ", REQ2=" + REQ2 + ", FINALREQUIREMENT=" + FINALREQUIREMENT + ", ICODE=" + ICODE
				+ ", ASSORTMNT1=" + ASSORTMNT1 + ", WH_CODE=" + WH_CODE + ", WHQTY=" + WHQTY + ", PACK_SIZE="
				+ PACK_SIZE + ", _40_DAYS_MBQ=" + _40_DAYS_MBQ + ", LAST_IN_RATE=" + LAST_IN_RATE + ", SIZE=" + SIZE
				+ ", COLOR=" + COLOR + ", VENDOR_NAME=" + VENDOR_NAME + ", SEG=" + SEG + ", OLD_BARCODE_RANK="
				+ OLD_BARCODE_RANK + ", FINALREQUIREMENT1=" + FINALREQUIREMENT1 + ", ACTUAL_STR1=" + ACTUAL_STR1
				+ ", FINALALLOCATIONQTY=" + FINALALLOCATIONQTY + ", UPDATEDREQUIREMENT=" + UPDATEDREQUIREMENT
				+ ", RANK=" + RANK + "]";
	}

}
