package com.supplymint.layer.data.tenant.demand.entity;

public class BudgetedTemporary {
	private String assortmentCode;
	private String billDate;
	private String qtyForeCast;
	private String userName;

	public BudgetedTemporary() {
		// TODO Auto-generated constructor stub
	}

	public BudgetedTemporary(String assortmentCode, String billDate, String qtyForeCast, String userName) {
		super();
		this.assortmentCode = assortmentCode;
		this.billDate = billDate;
		this.qtyForeCast = qtyForeCast;
		this.userName = userName;
	}

	public String getAssortmentCode() {
		return assortmentCode;
	}

	public void setAssortmentCode(String assortmentCode) {
		this.assortmentCode = assortmentCode;
	}

	public String getBillDate() {
		return billDate;
	}

	public void setBillDate(String billDate) {
		this.billDate = billDate;
	}

	public String getQtyForeCast() {
		return qtyForeCast;
	}

	public void setQtyForeCast(String qtyForeCast) {
		this.qtyForeCast = qtyForeCast;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Override
	public String toString() {
		return "BudgetedTemporary [assortmentCode=" + assortmentCode + ", billDate=" + billDate + ", qtyForeCast="
				+ qtyForeCast + ", userName=" + userName + "]";
	}

}
