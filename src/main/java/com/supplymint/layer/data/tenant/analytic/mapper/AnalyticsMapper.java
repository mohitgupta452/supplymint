package com.supplymint.layer.data.tenant.analytic.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.RequestParam;

import com.supplymint.layer.data.tenant.analytic.entity.AnalyticStoreProfile;
import com.supplymint.layer.data.tenant.analytic.entity.SellThrough;
import com.supplymint.layer.data.tenant.analytic.entity.StoreProfileActualData;
import com.supplymint.layer.data.tenant.analytic.entity.StoreProfileActualSaleData;
import com.supplymint.layer.data.tenant.analytic.entity.StoreProfileGraph;

@Mapper
public interface AnalyticsMapper {

	List<AnalyticStoreProfile> getSiteCode();

	AnalyticStoreProfile getStoreInfo(@Param("siteCode") String siteCode);

	List<StoreProfileActualData> getAnalyticsActualData();

	List<SellThrough> getTopBarCodeSellThrough(@RequestParam("storeCode") String storeCode);

	List<SellThrough> getBottomBarCodeSellThrough(@RequestParam("storeCode") String storeCode);

	List<SellThrough> getTopSizeSellThrough(@RequestParam("storeCode") String storeCode);

	List<SellThrough> getBottomSizeSellThrough(@RequestParam("storeCode") String storeCode);

	List<SellThrough> getTopPatternSellThrough(@RequestParam("storeCode") String storeCode);

	List<SellThrough> getBottomPatternSellThrough(@RequestParam("storeCode") String storeCode);

	List<SellThrough> getTopMaterialSellThrough(@RequestParam("storeCode") String storeCode);

	List<SellThrough> getBottomMaterialSellThrough(@RequestParam("storeCode") String storeCode);

	List<SellThrough> getTopBrandSellThrough(@RequestParam("storeCode") String storeCode);

	List<SellThrough> getBottomBrandSellThrough(@RequestParam("storeCode") String storeCode);

	List<SellThrough> getTopAssortmentSellThrough(@RequestParam("storeCode") String storeCode);

	List<SellThrough> getBottomAssortmentSellThrough(@RequestParam("storeCode") String storeCode);

	List<SellThrough> getTopDesignSellThrough(@RequestParam("storeCode") String storeCode);

	List<SellThrough> getBottomDesignSellThrough(@RequestParam("storeCode") String storeCode);

	List getTopArticleData();

	List getBottomArticleData();

	List<StoreProfileActualSaleData> salesTrendGraph();

	List<StoreProfileActualSaleData> getSellProfitTrends();

	List<StoreProfileActualSaleData> getFiveTopRankStoresArticleData();

	List<StoreProfileGraph> getLastFiveMonthsSellData(@Param("storeCode") String storeCode);

	List<StoreProfileActualSaleData> getStockInHand(@Param("storeCode") String storeCode);

	List<StoreProfileActualSaleData> getPerFormingMonth(@Param("storeCode") String storeCode);
}
