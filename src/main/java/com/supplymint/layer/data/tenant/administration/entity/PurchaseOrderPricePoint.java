package com.supplymint.layer.data.tenant.administration.entity;

public class PurchaseOrderPricePoint {

	private String hL1Code;
	private String hL1Name;
	private String hL2Code;
	private String hL2Name;
	private String hL3Code;
	private String hL3Name;
	private String hL4Code;
	private String hL4Name;

	private double MRPRangeFrom;
	private double MRPRangeTo;
	private String hsnSacCode;

	public PurchaseOrderPricePoint() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PurchaseOrderPricePoint(String hL1Code, String hL1Name, String hL2Code, String hL2Name, String hL3Code,
			String hL3Name, String hL4Code, String hL4Name, double mRPRangeFrom, double mRPRangeTo, String hsnSacCode) {
		super();
		this.hL1Code = hL1Code;
		this.hL1Name = hL1Name;
		this.hL2Code = hL2Code;
		this.hL2Name = hL2Name;
		this.hL3Code = hL3Code;
		this.hL3Name = hL3Name;
		this.hL4Code = hL4Code;
		this.hL4Name = hL4Name;
		MRPRangeFrom = mRPRangeFrom;
		MRPRangeTo = mRPRangeTo;
		this.hsnSacCode = hsnSacCode;
	}

	public String gethL1Code() {
		return hL1Code;
	}

	public void sethL1Code(String hL1Code) {
		this.hL1Code = hL1Code;
	}

	public String gethL1Name() {
		return hL1Name;
	}

	public void sethL1Name(String hL1Name) {
		this.hL1Name = hL1Name;
	}

	public String gethL2Code() {
		return hL2Code;
	}

	public void sethL2Code(String hL2Code) {
		this.hL2Code = hL2Code;
	}

	public String gethL2Name() {
		return hL2Name;
	}

	public void sethL2Name(String hL2Name) {
		this.hL2Name = hL2Name;
	}

	public String gethL3Code() {
		return hL3Code;
	}

	public void sethL3Code(String hL3Code) {
		this.hL3Code = hL3Code;
	}

	public String gethL3Name() {
		return hL3Name;
	}

	public void sethL3Name(String hL3Name) {
		this.hL3Name = hL3Name;
	}

	public String gethL4Code() {
		return hL4Code;
	}

	public void sethL4Code(String hL4Code) {
		this.hL4Code = hL4Code;
	}

	public String gethL4Name() {
		return hL4Name;
	}

	public void sethL4Name(String hL4Name) {
		this.hL4Name = hL4Name;
	}

	public double getMRPRangeFrom() {
		return MRPRangeFrom;
	}

	public void setMRPRangeFrom(double mRPRangeFrom) {
		MRPRangeFrom = mRPRangeFrom;
	}

	public double getMRPRangeTo() {
		return MRPRangeTo;
	}

	public void setMRPRangeTo(double mRPRangeTo) {
		MRPRangeTo = mRPRangeTo;
	}

	public String getHsnSacCode() {
		return hsnSacCode;
	}

	public void setHsnSacCode(String hsnSacCode) {
		this.hsnSacCode = hsnSacCode;
	}

	@Override
	public String toString() {
		return "PurchaseOrderPricePoint [hL1Code=" + hL1Code + ", hL1Name=" + hL1Name + ", hL2Code=" + hL2Code
				+ ", hL2Name=" + hL2Name + ", hL3Code=" + hL3Code + ", hL3Name=" + hL3Name + ", hL4Code=" + hL4Code
				+ ", hL4Name=" + hL4Name + ", MRPRangeFrom=" + MRPRangeFrom + ", MRPRangeTo=" + MRPRangeTo
				+ ", hsnSacCode=" + hsnSacCode + "]";
	}

}
