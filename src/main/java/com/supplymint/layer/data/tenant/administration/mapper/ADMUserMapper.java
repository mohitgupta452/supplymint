package com.supplymint.layer.data.tenant.administration.mapper;

import java.time.OffsetDateTime;
import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.supplymint.layer.data.tenant.administration.entity.ADMUser;

@Mapper
public interface ADMUserMapper {

	ADMUser findById(int id);

	ADMUser findByUserName(@Param("userName") String userName);

	List<ADMUser> findAll(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("enterpriseUserName") String enterpriseUserName);

	Integer create(ADMUser users);

	Integer update(ADMUser users);

	@Delete("DELETE FROM ADM_USERS WHERE USER_ID=#{id}")
	Integer delete(Integer id);

	Integer updateStatus(@Param("status") String status, @Param("userName") String userName,
			@Param("ipAddress") String ipAddress, @Param("updationTime") OffsetDateTime updationTime,
			@Param("updatedBy") String updatedBy);

	List<ADMUser> getAccessmode(String accessmode);

	List<ADMUser> getEnterPriseId(Integer id);

	@Select("SELECT COUNT(*) FROM ADM_USERS")
	int record();

	int searchRecord(@Param("search") String search);

	ADMUser getUserName(@Param("userName") String userName);

	List<ADMUser> getEmail(String email);

	List<ADMUser> searchAll(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("search") String search);

	@Select("SELECT COUNT(*) FROM ADM_USERROLES WHERE USERID = #{id}")
	Integer getUserRolesRecords(Integer id);

	List<ADMUser> filter(ADMUser admUser);

	List<ADMUser> getAllUser();

	@Select("SELECT COUNT(*) FROM ADM_USERS WHERE USERNAME = #{username}")
	int checkUsername(String username);

	@Select("SELECT EMAIL as email, USERNAME as userName, " + "MOBILENUMBER as mobileNumber FROM ADM_Users WHERE "
			+ "lower(EMAIL) = lower(#{email}) "
			+ "OR MOBILENUMBER=#{mobileNumber} OR lower(USERNAME) = lower(#{userName})")
	List<ADMUser> checkUserExistance(ADMUser admUser);

	List<ADMUser> checkUserUpdateExistance(ADMUser admUser);

	Integer filterRecord(ADMUser admUser);

	List<ADMUser> getAllRecord(@Param("userName") String userName);

	public ADMUser getUserDetail(@Param("userName") String userName);

	public int updateByUserName(ADMUser admUser);

	@Update("UPDATE ADM_USERS SET IMAGEURL=#{url} WHERE USERNAME=#{userName}")
	public int updateImageProfileUrl(@Param("url") String url, @Param("userName") String userName);

	@Update("UPDATE ADM_USERS SET IMAGEUPDATEDON=#{updationTime,jdbcType=DATE} WHERE USERNAME=#{userName,jdbcType=VARCHAR}")
	public int updateTimeByUserName(@Param("userName") String userName, @Param("updationTime") Date updationTime);

}