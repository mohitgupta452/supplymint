package com.supplymint.layer.data.tenant.administration.mapper;

import java.time.OffsetDateTime;
import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.supplymint.layer.data.tenant.administration.entity.ADMUserRole;

@Mapper
public interface ADMUserRoleMapper {

	ADMUserRole findById(Integer id);

	List<ADMUserRole> findAll(@Param("offset")Integer offset,@Param("pagesize") Integer pagesize,@Param("enterpriseUserName")String enterpriseUserName);

	Integer create(ADMUserRole admUserRole);

	Integer update(ADMUserRole admUserRole);

	@Delete("DELETE FROM ADM_USERROLES WHERE ID=#{id}")
	Integer delete(Integer id);

	List<ADMUserRole> getStatus(String status);

	@Update("UPDATE ADM_USERROLES SET STATUS=#{status} , IPADDRESS = #{ipAddress}, UPDATIONTIME = #{updationTime} WHERE ID=#{uid}")
	Integer updateStatus(@Param("status") String status, @Param("uid") Integer uid,
			@Param("ipAddress") String ipAddress, @Param("updationTime") OffsetDateTime updationTime);

	List<ADMUserRole> getOnRole(String name);

	List<ADMUserRole> getUserById(Integer id);

	List<ADMUserRole> searchAll(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("search") String search);
	
	int searchRecord(@Param("search") String search);

	@Delete("DELETE FROM ADM_USERROLES WHERE USERID=#{id}")
	Integer deleteUser(Integer id);

	@Select("SELECT COUNT(*) FROM ADM_USERROLES")
	Integer record();

	List<ADMUserRole> roleFilter(ADMUserRole admUserRole);
	
	/*@Select("SELECT COUNT(*) from ADM_USERROLES where lower(USERNAME) LIKE lower(#{userName}) || '%' AND"
			+ " lower(ROLE) LIKE lower(#{name}) || '%'  AND  "
			+ "lower(CREATEDBY) LIKE lower(#{createdBy}) || '%' AND"
			+ "STATUS LIKE #{status} || '%'")*/
	
	Integer filterRecord(ADMUserRole admUserRole);
	
	List<ADMUserRole> getAllRecord(@Param("userName") String userName);

	ADMUserRole findByUserName(@Param("userName")String userName);
	
	Integer updateStatusByUserName(ADMUserRole admUserRole);
	
}
