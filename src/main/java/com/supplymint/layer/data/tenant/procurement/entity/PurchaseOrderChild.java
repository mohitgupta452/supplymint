package com.supplymint.layer.data.tenant.procurement.entity;

import java.time.OffsetDateTime;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.supplymint.util.LocalDatetimeDeserializer;
import com.supplymint.util.PILocalDatetimeSerializer;

public class PurchaseOrderChild {

	private int id;
	private List<Category> colors;
	private List<Category> sizes;
	private List<RatioList> ratios;
	private List<String> images;

	private String orderId;
	private String ratio;
	private String size;
	private String color;
	private String itemId;

	private double rsp;
	private double mrp;
	private int qty;
	private int designWiseTotalQty;
	private int sumOfRatio;
	private int noOfSets;
	private double rate;

	private String setHeaderId;

	private String containsImage;
	private String image1;
	private String image2;
	private String image3;
	private String image4;
	private String image5;

	private String typeOfBuying;
	private String marginRule;
	private double totalAmount;
	private String tax;
	private List<String> designWiseTotalTax;
	private String designWiseTotalTaxString;

	private String remarks;
	private double netAmountTotal;
	private double designWiseNetAmountTotal;
	private String calculatedMargin;
	private List<String> designWiseTotalCalculatedMargin;
	private String designWiseTotalCalculatedMargingString;
	private String gst;
	private List<String> designWiseTotalGST;
	private String designWiseTotalGSTString;
	private String otb;
	private String intakeMargin;
	private List<String> designWiseTotalIntakeMargin;
	private String designWiseTotalIntakeMarginString;
	private String designWiseRowId;

	private List<FinCharge> designWiseTotalFinCharges;
	private String designWiseTotalFinChargesString;

	private String designWiseTotalBasic;

	private String designWiseTotalOtb;

	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	private OffsetDateTime deliveryDate;

	private String cat1Code;
	private String cat1Name;
	private String cat2Code;
	private String cat2Name;
	private String cat3Code;
	private String cat3Name;
	private String cat4Code;
	private String cat4Name;

	private String design;
	private String desc2Code;
	private String desc2Name;
	private String desc3Code;
	private String desc3Name;
	private String desc4Code;
	private String desc4Name;
	private String desc5Code;
	private String desc5Name;

	private UDFMaster udf;
	private String udfString;
	private int poLineItemSequence;

	private String udf1;
	private String udf2;
	private String udf3;
	private String udf4;
	private String udf5;
	private String udf6;
	private String udf7;
	private String udf8;
	private String udf9;
	private String udf10;
	private String udf11;
	private String udf12;
	private String udf13;
	private String udf14;
	private String udf15;
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	private OffsetDateTime udf16;
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	private OffsetDateTime udf17;
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	private OffsetDateTime udf18;
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	private OffsetDateTime udf19;
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	private OffsetDateTime udf20;

	private String itemudf1;
	private String itemudf2;
	private String itemudf3;
	private String itemudf4;
	private String itemudf5;
	private String itemudf6;
	private String itemudf7;
	private String itemudf8;
	private String itemudf9;
	private String itemudf10;
	private String itemudf11;
	private String itemudf12;
	private String itemudf13;
	private String itemudf14;
	private String itemudf15;
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	private OffsetDateTime itemudf16;
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	private OffsetDateTime itemudf17;
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	private OffsetDateTime itemudf18;
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	private OffsetDateTime itemudf19;
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	private OffsetDateTime itemudf20;

	private String active;
	private String status;
	private String ipAddress;
	private String createdBy;

	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	private OffsetDateTime createdTime;

	private String updatedBy;

	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	private OffsetDateTime updationTime;
	private String additional;
	private List<FinCharge> finCharge;

	private String discountType;
	private String discountValue;
	private String finalRate;

	public PurchaseOrderChild() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PurchaseOrderChild(int id, List<Category> colors, List<Category> sizes, List<RatioList> ratios,
			List<String> images, String orderId, String ratio, String size, String color, String itemId, double rsp,
			double mrp, int qty, int designWiseTotalQty, int sumOfRatio, int noOfSets, double rate, String setHeaderId,
			String containsImage, String image1, String image2, String image3, String image4, String image5,
			String typeOfBuying, String marginRule, double totalAmount, String tax, List<String> designWiseTotalTax,
			String designWiseTotalTaxString, String remarks, double netAmountTotal, double designWiseNetAmountTotal,
			String calculatedMargin, List<String> designWiseTotalCalculatedMargin,
			String designWiseTotalCalculatedMargingString, String gst, List<String> designWiseTotalGST,
			String designWiseTotalGSTString, String otb, String intakeMargin, List<String> designWiseTotalIntakeMargin,
			String designWiseTotalIntakeMarginString, String designWiseRowId, List<FinCharge> designWiseTotalFinCharges,
			String designWiseTotalFinChargesString, String designWiseTotalBasic, String designWiseTotalOtb,
			OffsetDateTime deliveryDate, String cat1Code, String cat1Name, String cat2Code, String cat2Name,
			String cat3Code, String cat3Name, String cat4Code, String cat4Name, String design, String desc2Code,
			String desc2Name, String desc3Code, String desc3Name, String desc4Code, String desc4Name, String desc5Code,
			String desc5Name, UDFMaster udf, String udfString, int poLineItemSequence, String udf1, String udf2,
			String udf3, String udf4, String udf5, String udf6, String udf7, String udf8, String udf9, String udf10,
			String udf11, String udf12, String udf13, String udf14, String udf15, OffsetDateTime udf16,
			OffsetDateTime udf17, OffsetDateTime udf18, OffsetDateTime udf19, OffsetDateTime udf20, String itemudf1,
			String itemudf2, String itemudf3, String itemudf4, String itemudf5, String itemudf6, String itemudf7,
			String itemudf8, String itemudf9, String itemudf10, String itemudf11, String itemudf12, String itemudf13,
			String itemudf14, String itemudf15, OffsetDateTime itemudf16, OffsetDateTime itemudf17,
			OffsetDateTime itemudf18, OffsetDateTime itemudf19, OffsetDateTime itemudf20, String active, String status,
			String ipAddress, String createdBy, OffsetDateTime createdTime, String updatedBy,
			OffsetDateTime updationTime, String additional, List<FinCharge> finCharge, String discountType,
			String discountValue, String finalRate) {
		super();
		this.id = id;
		this.colors = colors;
		this.sizes = sizes;
		this.ratios = ratios;
		this.images = images;
		this.orderId = orderId;
		this.ratio = ratio;
		this.size = size;
		this.color = color;
		this.itemId = itemId;
		this.rsp = rsp;
		this.mrp = mrp;
		this.qty = qty;
		this.designWiseTotalQty = designWiseTotalQty;
		this.sumOfRatio = sumOfRatio;
		this.noOfSets = noOfSets;
		this.rate = rate;
		this.setHeaderId = setHeaderId;
		this.containsImage = containsImage;
		this.image1 = image1;
		this.image2 = image2;
		this.image3 = image3;
		this.image4 = image4;
		this.image5 = image5;
		this.typeOfBuying = typeOfBuying;
		this.marginRule = marginRule;
		this.totalAmount = totalAmount;
		this.tax = tax;
		this.designWiseTotalTax = designWiseTotalTax;
		this.designWiseTotalTaxString = designWiseTotalTaxString;
		this.remarks = remarks;
		this.netAmountTotal = netAmountTotal;
		this.designWiseNetAmountTotal = designWiseNetAmountTotal;
		this.calculatedMargin = calculatedMargin;
		this.designWiseTotalCalculatedMargin = designWiseTotalCalculatedMargin;
		this.designWiseTotalCalculatedMargingString = designWiseTotalCalculatedMargingString;
		this.gst = gst;
		this.designWiseTotalGST = designWiseTotalGST;
		this.designWiseTotalGSTString = designWiseTotalGSTString;
		this.otb = otb;
		this.intakeMargin = intakeMargin;
		this.designWiseTotalIntakeMargin = designWiseTotalIntakeMargin;
		this.designWiseTotalIntakeMarginString = designWiseTotalIntakeMarginString;
		this.designWiseRowId = designWiseRowId;
		this.designWiseTotalFinCharges = designWiseTotalFinCharges;
		this.designWiseTotalFinChargesString = designWiseTotalFinChargesString;
		this.designWiseTotalBasic = designWiseTotalBasic;
		this.designWiseTotalOtb = designWiseTotalOtb;
		this.deliveryDate = deliveryDate;
		this.cat1Code = cat1Code;
		this.cat1Name = cat1Name;
		this.cat2Code = cat2Code;
		this.cat2Name = cat2Name;
		this.cat3Code = cat3Code;
		this.cat3Name = cat3Name;
		this.cat4Code = cat4Code;
		this.cat4Name = cat4Name;
		this.design = design;
		this.desc2Code = desc2Code;
		this.desc2Name = desc2Name;
		this.desc3Code = desc3Code;
		this.desc3Name = desc3Name;
		this.desc4Code = desc4Code;
		this.desc4Name = desc4Name;
		this.desc5Code = desc5Code;
		this.desc5Name = desc5Name;
		this.udf = udf;
		this.udfString = udfString;
		this.poLineItemSequence = poLineItemSequence;
		this.udf1 = udf1;
		this.udf2 = udf2;
		this.udf3 = udf3;
		this.udf4 = udf4;
		this.udf5 = udf5;
		this.udf6 = udf6;
		this.udf7 = udf7;
		this.udf8 = udf8;
		this.udf9 = udf9;
		this.udf10 = udf10;
		this.udf11 = udf11;
		this.udf12 = udf12;
		this.udf13 = udf13;
		this.udf14 = udf14;
		this.udf15 = udf15;
		this.udf16 = udf16;
		this.udf17 = udf17;
		this.udf18 = udf18;
		this.udf19 = udf19;
		this.udf20 = udf20;
		this.itemudf1 = itemudf1;
		this.itemudf2 = itemudf2;
		this.itemudf3 = itemudf3;
		this.itemudf4 = itemudf4;
		this.itemudf5 = itemudf5;
		this.itemudf6 = itemudf6;
		this.itemudf7 = itemudf7;
		this.itemudf8 = itemudf8;
		this.itemudf9 = itemudf9;
		this.itemudf10 = itemudf10;
		this.itemudf11 = itemudf11;
		this.itemudf12 = itemudf12;
		this.itemudf13 = itemudf13;
		this.itemudf14 = itemudf14;
		this.itemudf15 = itemudf15;
		this.itemudf16 = itemudf16;
		this.itemudf17 = itemudf17;
		this.itemudf18 = itemudf18;
		this.itemudf19 = itemudf19;
		this.itemudf20 = itemudf20;
		this.active = active;
		this.status = status;
		this.ipAddress = ipAddress;
		this.createdBy = createdBy;
		this.createdTime = createdTime;
		this.updatedBy = updatedBy;
		this.updationTime = updationTime;
		this.additional = additional;
		this.finCharge = finCharge;
		this.discountType = discountType;
		this.discountValue = discountValue;
		this.finalRate = finalRate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<Category> getColors() {
		return colors;
	}

	public void setColors(List<Category> colors) {
		this.colors = colors;
	}

	public List<Category> getSizes() {
		return sizes;
	}

	public void setSizes(List<Category> sizes) {
		this.sizes = sizes;
	}

	public List<RatioList> getRatios() {
		return ratios;
	}

	public void setRatios(List<RatioList> ratios) {
		this.ratios = ratios;
	}

	public List<String> getImages() {
		return images;
	}

	public void setImages(List<String> images) {
		this.images = images;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getRatio() {
		return ratio;
	}

	public void setRatio(String ratio) {
		this.ratio = ratio;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public double getRsp() {
		return rsp;
	}

	public void setRsp(double rsp) {
		this.rsp = rsp;
	}

	public double getMrp() {
		return mrp;
	}

	public void setMrp(double mrp) {
		this.mrp = mrp;
	}

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

	public int getDesignWiseTotalQty() {
		return designWiseTotalQty;
	}

	public void setDesignWiseTotalQty(int designWiseTotalQty) {
		this.designWiseTotalQty = designWiseTotalQty;
	}

	public int getSumOfRatio() {
		return sumOfRatio;
	}

	public void setSumOfRatio(int sumOfRatio) {
		this.sumOfRatio = sumOfRatio;
	}

	public int getNoOfSets() {
		return noOfSets;
	}

	public void setNoOfSets(int noOfSets) {
		this.noOfSets = noOfSets;
	}

	public double getRate() {
		return rate;
	}

	public void setRate(double rate) {
		this.rate = rate;
	}

	public String getSetHeaderId() {
		return setHeaderId;
	}

	public void setSetHeaderId(String setHeaderId) {
		this.setHeaderId = setHeaderId;
	}

	public String getContainsImage() {
		return containsImage;
	}

	public void setContainsImage(String containsImage) {
		this.containsImage = containsImage;
	}

	public String getImage1() {
		return image1;
	}

	public void setImage1(String image1) {
		this.image1 = image1;
	}

	public String getImage2() {
		return image2;
	}

	public void setImage2(String image2) {
		this.image2 = image2;
	}

	public String getImage3() {
		return image3;
	}

	public void setImage3(String image3) {
		this.image3 = image3;
	}

	public String getImage4() {
		return image4;
	}

	public void setImage4(String image4) {
		this.image4 = image4;
	}

	public String getImage5() {
		return image5;
	}

	public void setImage5(String image5) {
		this.image5 = image5;
	}

	public String getTypeOfBuying() {
		return typeOfBuying;
	}

	public void setTypeOfBuying(String typeOfBuying) {
		this.typeOfBuying = typeOfBuying;
	}

	public String getMarginRule() {
		return marginRule;
	}

	public void setMarginRule(String marginRule) {
		this.marginRule = marginRule;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getTax() {
		return tax;
	}

	public void setTax(String tax) {
		this.tax = tax;
	}

	public List<String> getDesignWiseTotalTax() {
		return designWiseTotalTax;
	}

	public void setDesignWiseTotalTax(List<String> designWiseTotalTax) {
		this.designWiseTotalTax = designWiseTotalTax;
	}

	public String getDesignWiseTotalTaxString() {
		return designWiseTotalTaxString;
	}

	public void setDesignWiseTotalTaxString(String designWiseTotalTaxString) {
		this.designWiseTotalTaxString = designWiseTotalTaxString;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public double getNetAmountTotal() {
		return netAmountTotal;
	}

	public void setNetAmountTotal(double netAmountTotal) {
		this.netAmountTotal = netAmountTotal;
	}

	public double getDesignWiseNetAmountTotal() {
		return designWiseNetAmountTotal;
	}

	public void setDesignWiseNetAmountTotal(double designWiseNetAmountTotal) {
		this.designWiseNetAmountTotal = designWiseNetAmountTotal;
	}

	public String getCalculatedMargin() {
		return calculatedMargin;
	}

	public void setCalculatedMargin(String calculatedMargin) {
		this.calculatedMargin = calculatedMargin;
	}

	public List<String> getDesignWiseTotalCalculatedMargin() {
		return designWiseTotalCalculatedMargin;
	}

	public void setDesignWiseTotalCalculatedMargin(List<String> designWiseTotalCalculatedMargin) {
		this.designWiseTotalCalculatedMargin = designWiseTotalCalculatedMargin;
	}

	public String getDesignWiseTotalCalculatedMargingString() {
		return designWiseTotalCalculatedMargingString;
	}

	public void setDesignWiseTotalCalculatedMargingString(String designWiseTotalCalculatedMargingString) {
		this.designWiseTotalCalculatedMargingString = designWiseTotalCalculatedMargingString;
	}

	public String getGst() {
		return gst;
	}

	public void setGst(String gst) {
		this.gst = gst;
	}

	public List<String> getDesignWiseTotalGST() {
		return designWiseTotalGST;
	}

	public void setDesignWiseTotalGST(List<String> designWiseTotalGST) {
		this.designWiseTotalGST = designWiseTotalGST;
	}

	public String getDesignWiseTotalGSTString() {
		return designWiseTotalGSTString;
	}

	public void setDesignWiseTotalGSTString(String designWiseTotalGSTString) {
		this.designWiseTotalGSTString = designWiseTotalGSTString;
	}

	public String getOtb() {
		return otb;
	}

	public void setOtb(String otb) {
		this.otb = otb;
	}

	public String getIntakeMargin() {
		return intakeMargin;
	}

	public void setIntakeMargin(String intakeMargin) {
		this.intakeMargin = intakeMargin;
	}

	public List<String> getDesignWiseTotalIntakeMargin() {
		return designWiseTotalIntakeMargin;
	}

	public void setDesignWiseTotalIntakeMargin(List<String> designWiseTotalIntakeMargin) {
		this.designWiseTotalIntakeMargin = designWiseTotalIntakeMargin;
	}

	public String getDesignWiseTotalIntakeMarginString() {
		return designWiseTotalIntakeMarginString;
	}

	public void setDesignWiseTotalIntakeMarginString(String designWiseTotalIntakeMarginString) {
		this.designWiseTotalIntakeMarginString = designWiseTotalIntakeMarginString;
	}

	public String getDesignWiseRowId() {
		return designWiseRowId;
	}

	public void setDesignWiseRowId(String designWiseRowId) {
		this.designWiseRowId = designWiseRowId;
	}

	public List<FinCharge> getDesignWiseTotalFinCharges() {
		return designWiseTotalFinCharges;
	}

	public void setDesignWiseTotalFinCharges(List<FinCharge> designWiseTotalFinCharges) {
		this.designWiseTotalFinCharges = designWiseTotalFinCharges;
	}

	public String getDesignWiseTotalFinChargesString() {
		return designWiseTotalFinChargesString;
	}

	public void setDesignWiseTotalFinChargesString(String designWiseTotalFinChargesString) {
		this.designWiseTotalFinChargesString = designWiseTotalFinChargesString;
	}

	public String getDesignWiseTotalBasic() {
		return designWiseTotalBasic;
	}

	public void setDesignWiseTotalBasic(String designWiseTotalBasic) {
		this.designWiseTotalBasic = designWiseTotalBasic;
	}

	public String getDesignWiseTotalOtb() {
		return designWiseTotalOtb;
	}

	public void setDesignWiseTotalOtb(String designWiseTotalOtb) {
		this.designWiseTotalOtb = designWiseTotalOtb;
	}

	public OffsetDateTime getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(OffsetDateTime deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public String getCat1Code() {
		return cat1Code;
	}

	public void setCat1Code(String cat1Code) {
		this.cat1Code = cat1Code;
	}

	public String getCat1Name() {
		return cat1Name;
	}

	public void setCat1Name(String cat1Name) {
		this.cat1Name = cat1Name;
	}

	public String getCat2Code() {
		return cat2Code;
	}

	public void setCat2Code(String cat2Code) {
		this.cat2Code = cat2Code;
	}

	public String getCat2Name() {
		return cat2Name;
	}

	public void setCat2Name(String cat2Name) {
		this.cat2Name = cat2Name;
	}

	public String getCat3Code() {
		return cat3Code;
	}

	public void setCat3Code(String cat3Code) {
		this.cat3Code = cat3Code;
	}

	public String getCat3Name() {
		return cat3Name;
	}

	public void setCat3Name(String cat3Name) {
		this.cat3Name = cat3Name;
	}

	public String getCat4Code() {
		return cat4Code;
	}

	public void setCat4Code(String cat4Code) {
		this.cat4Code = cat4Code;
	}

	public String getCat4Name() {
		return cat4Name;
	}

	public void setCat4Name(String cat4Name) {
		this.cat4Name = cat4Name;
	}

	public String getDesign() {
		return design;
	}

	public void setDesign(String design) {
		this.design = design;
	}

	public String getDesc2Code() {
		return desc2Code;
	}

	public void setDesc2Code(String desc2Code) {
		this.desc2Code = desc2Code;
	}

	public String getDesc2Name() {
		return desc2Name;
	}

	public void setDesc2Name(String desc2Name) {
		this.desc2Name = desc2Name;
	}

	public String getDesc3Code() {
		return desc3Code;
	}

	public void setDesc3Code(String desc3Code) {
		this.desc3Code = desc3Code;
	}

	public String getDesc3Name() {
		return desc3Name;
	}

	public void setDesc3Name(String desc3Name) {
		this.desc3Name = desc3Name;
	}

	public String getDesc4Code() {
		return desc4Code;
	}

	public void setDesc4Code(String desc4Code) {
		this.desc4Code = desc4Code;
	}

	public String getDesc4Name() {
		return desc4Name;
	}

	public void setDesc4Name(String desc4Name) {
		this.desc4Name = desc4Name;
	}

	public String getDesc5Code() {
		return desc5Code;
	}

	public void setDesc5Code(String desc5Code) {
		this.desc5Code = desc5Code;
	}

	public String getDesc5Name() {
		return desc5Name;
	}

	public void setDesc5Name(String desc5Name) {
		this.desc5Name = desc5Name;
	}

	public UDFMaster getUdf() {
		return udf;
	}

	public void setUdf(UDFMaster udf) {
		this.udf = udf;
	}

	public String getUdfString() {
		return udfString;
	}

	public void setUdfString(String udfString) {
		this.udfString = udfString;
	}

	public int getPoLineItemSequence() {
		return poLineItemSequence;
	}

	public void setPoLineItemSequence(int poLineItemSequence) {
		this.poLineItemSequence = poLineItemSequence;
	}

	public String getUdf1() {
		return udf1;
	}

	public void setUdf1(String udf1) {
		this.udf1 = udf1;
	}

	public String getUdf2() {
		return udf2;
	}

	public void setUdf2(String udf2) {
		this.udf2 = udf2;
	}

	public String getUdf3() {
		return udf3;
	}

	public void setUdf3(String udf3) {
		this.udf3 = udf3;
	}

	public String getUdf4() {
		return udf4;
	}

	public void setUdf4(String udf4) {
		this.udf4 = udf4;
	}

	public String getUdf5() {
		return udf5;
	}

	public void setUdf5(String udf5) {
		this.udf5 = udf5;
	}

	public String getUdf6() {
		return udf6;
	}

	public void setUdf6(String udf6) {
		this.udf6 = udf6;
	}

	public String getUdf7() {
		return udf7;
	}

	public void setUdf7(String udf7) {
		this.udf7 = udf7;
	}

	public String getUdf8() {
		return udf8;
	}

	public void setUdf8(String udf8) {
		this.udf8 = udf8;
	}

	public String getUdf9() {
		return udf9;
	}

	public void setUdf9(String udf9) {
		this.udf9 = udf9;
	}

	public String getUdf10() {
		return udf10;
	}

	public void setUdf10(String udf10) {
		this.udf10 = udf10;
	}

	public String getUdf11() {
		return udf11;
	}

	public void setUdf11(String udf11) {
		this.udf11 = udf11;
	}

	public String getUdf12() {
		return udf12;
	}

	public void setUdf12(String udf12) {
		this.udf12 = udf12;
	}

	public String getUdf13() {
		return udf13;
	}

	public void setUdf13(String udf13) {
		this.udf13 = udf13;
	}

	public String getUdf14() {
		return udf14;
	}

	public void setUdf14(String udf14) {
		this.udf14 = udf14;
	}

	public String getUdf15() {
		return udf15;
	}

	public void setUdf15(String udf15) {
		this.udf15 = udf15;
	}

	public OffsetDateTime getUdf16() {
		return udf16;
	}

	public void setUdf16(OffsetDateTime udf16) {
		this.udf16 = udf16;
	}

	public OffsetDateTime getUdf17() {
		return udf17;
	}

	public void setUdf17(OffsetDateTime udf17) {
		this.udf17 = udf17;
	}

	public OffsetDateTime getUdf18() {
		return udf18;
	}

	public void setUdf18(OffsetDateTime udf18) {
		this.udf18 = udf18;
	}

	public OffsetDateTime getUdf19() {
		return udf19;
	}

	public void setUdf19(OffsetDateTime udf19) {
		this.udf19 = udf19;
	}

	public OffsetDateTime getUdf20() {
		return udf20;
	}

	public void setUdf20(OffsetDateTime udf20) {
		this.udf20 = udf20;
	}

	public String getItemudf1() {
		return itemudf1;
	}

	public void setItemudf1(String itemudf1) {
		this.itemudf1 = itemudf1;
	}

	public String getItemudf2() {
		return itemudf2;
	}

	public void setItemudf2(String itemudf2) {
		this.itemudf2 = itemudf2;
	}

	public String getItemudf3() {
		return itemudf3;
	}

	public void setItemudf3(String itemudf3) {
		this.itemudf3 = itemudf3;
	}

	public String getItemudf4() {
		return itemudf4;
	}

	public void setItemudf4(String itemudf4) {
		this.itemudf4 = itemudf4;
	}

	public String getItemudf5() {
		return itemudf5;
	}

	public void setItemudf5(String itemudf5) {
		this.itemudf5 = itemudf5;
	}

	public String getItemudf6() {
		return itemudf6;
	}

	public void setItemudf6(String itemudf6) {
		this.itemudf6 = itemudf6;
	}

	public String getItemudf7() {
		return itemudf7;
	}

	public void setItemudf7(String itemudf7) {
		this.itemudf7 = itemudf7;
	}

	public String getItemudf8() {
		return itemudf8;
	}

	public void setItemudf8(String itemudf8) {
		this.itemudf8 = itemudf8;
	}

	public String getItemudf9() {
		return itemudf9;
	}

	public void setItemudf9(String itemudf9) {
		this.itemudf9 = itemudf9;
	}

	public String getItemudf10() {
		return itemudf10;
	}

	public void setItemudf10(String itemudf10) {
		this.itemudf10 = itemudf10;
	}

	public String getItemudf11() {
		return itemudf11;
	}

	public void setItemudf11(String itemudf11) {
		this.itemudf11 = itemudf11;
	}

	public String getItemudf12() {
		return itemudf12;
	}

	public void setItemudf12(String itemudf12) {
		this.itemudf12 = itemudf12;
	}

	public String getItemudf13() {
		return itemudf13;
	}

	public void setItemudf13(String itemudf13) {
		this.itemudf13 = itemudf13;
	}

	public String getItemudf14() {
		return itemudf14;
	}

	public void setItemudf14(String itemudf14) {
		this.itemudf14 = itemudf14;
	}

	public String getItemudf15() {
		return itemudf15;
	}

	public void setItemudf15(String itemudf15) {
		this.itemudf15 = itemudf15;
	}

	public OffsetDateTime getItemudf16() {
		return itemudf16;
	}

	public void setItemudf16(OffsetDateTime itemudf16) {
		this.itemudf16 = itemudf16;
	}

	public OffsetDateTime getItemudf17() {
		return itemudf17;
	}

	public void setItemudf17(OffsetDateTime itemudf17) {
		this.itemudf17 = itemudf17;
	}

	public OffsetDateTime getItemudf18() {
		return itemudf18;
	}

	public void setItemudf18(OffsetDateTime itemudf18) {
		this.itemudf18 = itemudf18;
	}

	public OffsetDateTime getItemudf19() {
		return itemudf19;
	}

	public void setItemudf19(OffsetDateTime itemudf19) {
		this.itemudf19 = itemudf19;
	}

	public OffsetDateTime getItemudf20() {
		return itemudf20;
	}

	public void setItemudf20(OffsetDateTime itemudf20) {
		this.itemudf20 = itemudf20;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public OffsetDateTime getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(OffsetDateTime createdTime) {
		this.createdTime = createdTime;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public OffsetDateTime getUpdationTime() {
		return updationTime;
	}

	public void setUpdationTime(OffsetDateTime updationTime) {
		this.updationTime = updationTime;
	}

	public String getAdditional() {
		return additional;
	}

	public void setAdditional(String additional) {
		this.additional = additional;
	}

	public List<FinCharge> getFinCharge() {
		return finCharge;
	}

	public void setFinCharge(List<FinCharge> finCharge) {
		this.finCharge = finCharge;
	}

	public String getDiscountType() {
		return discountType;
	}

	public void setDiscountType(String discountType) {
		this.discountType = discountType;
	}

	public String getDiscountValue() {
		return discountValue;
	}

	public void setDiscountValue(String discountValue) {
		this.discountValue = discountValue;
	}

	public String getFinalRate() {
		return finalRate;
	}

	public void setFinalRate(String finalRate) {
		this.finalRate = finalRate;
	}

	@Override
	public String toString() {
		return "PurchaseOrderChild [id=" + id + ", colors=" + colors + ", sizes=" + sizes + ", ratios=" + ratios
				+ ", images=" + images + ", orderId=" + orderId + ", ratio=" + ratio + ", size=" + size + ", color="
				+ color + ", itemId=" + itemId + ", rsp=" + rsp + ", mrp=" + mrp + ", qty=" + qty
				+ ", designWiseTotalQty=" + designWiseTotalQty + ", sumOfRatio=" + sumOfRatio + ", noOfSets=" + noOfSets
				+ ", rate=" + rate + ", setHeaderId=" + setHeaderId + ", containsImage=" + containsImage + ", image1="
				+ image1 + ", image2=" + image2 + ", image3=" + image3 + ", image4=" + image4 + ", image5=" + image5
				+ ", typeOfBuying=" + typeOfBuying + ", marginRule=" + marginRule + ", totalAmount=" + totalAmount
				+ ", tax=" + tax + ", designWiseTotalTax=" + designWiseTotalTax + ", designWiseTotalTaxString="
				+ designWiseTotalTaxString + ", remarks=" + remarks + ", netAmountTotal=" + netAmountTotal
				+ ", designWiseNetAmountTotal=" + designWiseNetAmountTotal + ", calculatedMargin=" + calculatedMargin
				+ ", designWiseTotalCalculatedMargin=" + designWiseTotalCalculatedMargin
				+ ", designWiseTotalCalculatedMargingString=" + designWiseTotalCalculatedMargingString + ", gst=" + gst
				+ ", designWiseTotalGST=" + designWiseTotalGST + ", designWiseTotalGSTString="
				+ designWiseTotalGSTString + ", otb=" + otb + ", intakeMargin=" + intakeMargin
				+ ", designWiseTotalIntakeMargin=" + designWiseTotalIntakeMargin
				+ ", designWiseTotalIntakeMarginString=" + designWiseTotalIntakeMarginString + ", designWiseRowId="
				+ designWiseRowId + ", designWiseTotalFinCharges=" + designWiseTotalFinCharges
				+ ", designWiseTotalFinChargesString=" + designWiseTotalFinChargesString + ", designWiseTotalBasic="
				+ designWiseTotalBasic + ", designWiseTotalOtb=" + designWiseTotalOtb + ", deliveryDate=" + deliveryDate
				+ ", cat1Code=" + cat1Code + ", cat1Name=" + cat1Name + ", cat2Code=" + cat2Code + ", cat2Name="
				+ cat2Name + ", cat3Code=" + cat3Code + ", cat3Name=" + cat3Name + ", cat4Code=" + cat4Code
				+ ", cat4Name=" + cat4Name + ", design=" + design + ", desc2Code=" + desc2Code + ", desc2Name="
				+ desc2Name + ", desc3Code=" + desc3Code + ", desc3Name=" + desc3Name + ", desc4Code=" + desc4Code
				+ ", desc4Name=" + desc4Name + ", desc5Code=" + desc5Code + ", desc5Name=" + desc5Name + ", udf=" + udf
				+ ", udfString=" + udfString + ", poLineItemSequence=" + poLineItemSequence + ", udf1=" + udf1
				+ ", udf2=" + udf2 + ", udf3=" + udf3 + ", udf4=" + udf4 + ", udf5=" + udf5 + ", udf6=" + udf6
				+ ", udf7=" + udf7 + ", udf8=" + udf8 + ", udf9=" + udf9 + ", udf10=" + udf10 + ", udf11=" + udf11
				+ ", udf12=" + udf12 + ", udf13=" + udf13 + ", udf14=" + udf14 + ", udf15=" + udf15 + ", udf16=" + udf16
				+ ", udf17=" + udf17 + ", udf18=" + udf18 + ", udf19=" + udf19 + ", udf20=" + udf20 + ", itemudf1="
				+ itemudf1 + ", itemudf2=" + itemudf2 + ", itemudf3=" + itemudf3 + ", itemudf4=" + itemudf4
				+ ", itemudf5=" + itemudf5 + ", itemudf6=" + itemudf6 + ", itemudf7=" + itemudf7 + ", itemudf8="
				+ itemudf8 + ", itemudf9=" + itemudf9 + ", itemudf10=" + itemudf10 + ", itemudf11=" + itemudf11
				+ ", itemudf12=" + itemudf12 + ", itemudf13=" + itemudf13 + ", itemudf14=" + itemudf14 + ", itemudf15="
				+ itemudf15 + ", itemudf16=" + itemudf16 + ", itemudf17=" + itemudf17 + ", itemudf18=" + itemudf18
				+ ", itemudf19=" + itemudf19 + ", itemudf20=" + itemudf20 + ", active=" + active + ", status=" + status
				+ ", ipAddress=" + ipAddress + ", createdBy=" + createdBy + ", createdTime=" + createdTime
				+ ", updatedBy=" + updatedBy + ", updationTime=" + updationTime + ", additional=" + additional
				+ ", finCharge=" + finCharge + ", discountType=" + discountType + ", discountValue=" + discountValue
				+ ", finalRate=" + finalRate + "]";
	}

}
