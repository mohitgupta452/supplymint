package com.supplymint.layer.data.tenant.demand.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.supplymint.util.DateFormatSerializer;

public class DemandPlanning {
	private int id;
	private String demandForecast;
	private String frequency;
	private String predictPeriod;
	private String chooseYear;
	private String startMonth;
	private String configuredOn;
	private String startedOn;
	private String totalStore;
	private String totalAssortment;
	private String summary;
	private String fileName;
	private String savedPath;
	private String generatedForecast;
	private String downloadUrl;
	private String status;
	private String ipAddress;
	private String createdBy;
//	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm a z")
	@JsonSerialize(using = DateFormatSerializer.class)
	private Date createdOn;
	private String updatedBy;
	@JsonSerialize(using = DateFormatSerializer.class)
	private Date updatedOn;
	private String additional;
	private String forecastCode;
	private String runId;
	private String scriptName;
	private String duration;
	@JsonProperty(access = Access.WRITE_ONLY)
	private Integer offset;
	@JsonProperty(access = Access.WRITE_ONLY)
	private Integer pageSize;
	private String upnDc;
	private String dbReadStatus;
	private String forecastedAssortment;

	public DemandPlanning() {

	}

	public DemandPlanning(int id, String demandForecast, String frequency, String predictPeriod, String chooseYear,
			String startMonth, String configuredOn, String startedOn, String totalStore, String totalAssortment,
			String summary, String fileName, String savedPath, String generatedForecast, String downloadUrl,
			String status, String ipAddress, String createdBy, Date createdOn, String updatedBy, Date updatedOn,
			String additional, String forecastCode, String runId, String scriptName, String duration, Integer offset,
			Integer pageSize, String upnDc, String dbReadStatus, String forecastedAssortment) {
		super();
		this.id = id;
		this.demandForecast = demandForecast;
		this.frequency = frequency;
		this.predictPeriod = predictPeriod;
		this.chooseYear = chooseYear;
		this.startMonth = startMonth;
		this.configuredOn = configuredOn;
		this.startedOn = startedOn;
		this.totalStore = totalStore;
		this.totalAssortment = totalAssortment;
		this.summary = summary;
		this.fileName = fileName;
		this.savedPath = savedPath;
		this.generatedForecast = generatedForecast;
		this.downloadUrl = downloadUrl;
		this.status = status;
		this.ipAddress = ipAddress;
		this.createdBy = createdBy;
		this.createdOn = createdOn;
		this.updatedBy = updatedBy;
		this.updatedOn = updatedOn;
		this.additional = additional;
		this.forecastCode = forecastCode;
		this.runId = runId;
		this.scriptName = scriptName;
		this.duration = duration;
		this.offset = offset;
		this.pageSize = pageSize;
		this.upnDc = upnDc;
		this.dbReadStatus = dbReadStatus;
		this.forecastedAssortment = forecastedAssortment;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDemandForecast() {
		return demandForecast;
	}

	public void setDemandForecast(String demandForecast) {
		this.demandForecast = demandForecast;
	}

	public String getFrequency() {
		return frequency;
	}

	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}

	public String getPredictPeriod() {
		return predictPeriod;
	}

	public void setPredictPeriod(String predictPeriod) {
		this.predictPeriod = predictPeriod;
	}

	public String getChooseYear() {
		return chooseYear;
	}

	public void setChooseYear(String chooseYear) {
		this.chooseYear = chooseYear;
	}

	public String getStartMonth() {
		return startMonth;
	}

	public void setStartMonth(String startMonth) {
		this.startMonth = startMonth;
	}

	public String getConfiguredOn() {
		return configuredOn;
	}

	public void setConfiguredOn(String configuredOn) {
		this.configuredOn = configuredOn;
	}

	public String getStartedOn() {
		return startedOn;
	}

	public void setStartedOn(String startedOn) {
		this.startedOn = startedOn;
	}

	public String getTotalStore() {
		return totalStore;
	}

	public void setTotalStore(String totalStore) {
		this.totalStore = totalStore;
	}

	public String getTotalAssortment() {
		return totalAssortment;
	}

	public void setTotalAssortment(String totalAssortment) {
		this.totalAssortment = totalAssortment;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getSavedPath() {
		return savedPath;
	}

	public void setSavedPath(String savedPath) {
		this.savedPath = savedPath;
	}

	public String getGeneratedForecast() {
		return generatedForecast;
	}

	public void setGeneratedForecast(String generatedForecast) {
		this.generatedForecast = generatedForecast;
	}

	public String getDownloadUrl() {
		return downloadUrl;
	}

	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getAdditional() {
		return additional;
	}

	public void setAdditional(String additional) {
		this.additional = additional;
	}

	public String getForecastCode() {
		return forecastCode;
	}

	public void setForecastCode(String forecastCode) {
		this.forecastCode = forecastCode;
	}

	public String getRunId() {
		return runId;
	}

	public void setRunId(String runId) {
		this.runId = runId;
	}

	public String getScriptName() {
		return scriptName;
	}

	public void setScriptName(String scriptName) {
		this.scriptName = scriptName;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public Integer getOffset() {
		return offset;
	}

	public void setOffset(Integer offset) {
		this.offset = offset;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public String getUpnDc() {
		return upnDc;
	}

	public void setUpnDc(String upnDc) {
		this.upnDc = upnDc;
	}

	public String getDbReadStatus() {
		return dbReadStatus;
	}

	public void setDbReadStatus(String dbReadStatus) {
		this.dbReadStatus = dbReadStatus;
	}

	public String getForecastedAssortment() {
		return forecastedAssortment;
	}

	public void setForecastedAssortment(String forecastedAssortment) {
		this.forecastedAssortment = forecastedAssortment;
	}

	@Override
	public String toString() {
		return "DemandPlanning [id=" + id + ", demandForecast=" + demandForecast + ", frequency=" + frequency
				+ ", predictPeriod=" + predictPeriod + ", chooseYear=" + chooseYear + ", startMonth=" + startMonth
				+ ", configuredOn=" + configuredOn + ", startedOn=" + startedOn + ", totalStore=" + totalStore
				+ ", totalAssortment=" + totalAssortment + ", summary=" + summary + ", fileName=" + fileName
				+ ", savedPath=" + savedPath + ", generatedForecast=" + generatedForecast + ", downloadUrl="
				+ downloadUrl + ", status=" + status + ", ipAddress=" + ipAddress + ", createdBy=" + createdBy
				+ ", createdOn=" + createdOn + ", updatedBy=" + updatedBy + ", updatedOn=" + updatedOn + ", additional="
				+ additional + ", forecastCode=" + forecastCode + ", runId=" + runId + ", scriptName=" + scriptName
				+ ", duration=" + duration + ", offset=" + offset + ", pageSize=" + pageSize + ", upnDc=" + upnDc
				+ ", dbReadStatus=" + dbReadStatus + ", forecastedAssortment=" + forecastedAssortment + "]";
	}

}
