package com.supplymint.layer.data.tenant.procurement.entity;

import java.math.BigDecimal;
import java.util.Date;

public class Suppliers {

	private Integer id;
	private String slCode;
	private String slName;
	private String slAddr;
	private String slCityName;
	private String transporterCode;
	private String transporterName;
	private String agentCode;
	private String agentName;
	private String agentRate;
	private String creditDays;
	private String isExt;
	private String purchTermCode;
	private String creditAmt;
	private String lgtAPPL;
	private String dueDateBasis;
	private String brand;
	private String product;
	private String outLgtAPPL;
	private String gateInAPPL;
	private String tradeGrpCode;
	private Integer dueDays;
	private Integer deliveryBuffDays;
	private String catCode;
	private String gstInNo;
	private String gstStateCode;
	private Date timestamp;
	volatile BigDecimal leadTime;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSlCode() {
		return slCode;
	}
	public void setSlCode(String slCode) {
		this.slCode = slCode;
	}
	public String getSlName() {
		return slName;
	}
	public void setSlName(String slName) {
		this.slName = slName;
	}
	public String getSlAddr() {
		return slAddr;
	}
	public void setSlAddr(String slAddr) {
		this.slAddr = slAddr;
	}
	public String getSlCityName() {
		return slCityName;
	}
	public void setSlCityName(String slCityName) {
		this.slCityName = slCityName;
	}
	public String getTransporterCode() {
		return transporterCode;
	}
	public void setTransporterCode(String transporterCode) {
		this.transporterCode = transporterCode;
	}
	public String getTransporterName() {
		return transporterName;
	}
	public void setTransporterName(String transporterName) {
		this.transporterName = transporterName;
	}
	public String getAgentCode() {
		return agentCode;
	}
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}
	public String getAgentName() {
		return agentName;
	}
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}
	public String getAgentRate() {
		return agentRate;
	}
	public void setAgentRate(String agentRate) {
		this.agentRate = agentRate;
	}
	public String getCreditDays() {
		return creditDays;
	}
	public void setCreditDays(String creditDays) {
		this.creditDays = creditDays;
	}
	public String getIsExt() {
		return isExt;
	}
	public void setIsExt(String isExt) {
		this.isExt = isExt;
	}
	public String getPurchTermCode() {
		return purchTermCode;
	}
	public void setPurchTermCode(String purchTermCode) {
		this.purchTermCode = purchTermCode;
	}
	public String getCreditAmt() {
		return creditAmt;
	}
	public void setCreditAmt(String creditAmt) {
		this.creditAmt = creditAmt;
	}
	public String getLgtAPPL() {
		return lgtAPPL;
	}
	public void setLgtAPPL(String lgtAPPL) {
		this.lgtAPPL = lgtAPPL;
	}
	public String getDueDateBasis() {
		return dueDateBasis;
	}
	public void setDueDateBasis(String dueDateBasis) {
		this.dueDateBasis = dueDateBasis;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public String getOutLgtAPPL() {
		return outLgtAPPL;
	}
	public void setOutLgtAPPL(String outLgtAPPL) {
		this.outLgtAPPL = outLgtAPPL;
	}
	public String getGateInAPPL() {
		return gateInAPPL;
	}
	public void setGateInAPPL(String gateInAPPL) {
		this.gateInAPPL = gateInAPPL;
	}
	public String getTradeGrpCode() {
		return tradeGrpCode;
	}
	public void setTradeGrpCode(String tradeGrpCode) {
		this.tradeGrpCode = tradeGrpCode;
	}
	public Integer getDueDays() {
		return dueDays;
	}
	public void setDueDays(Integer dueDays) {
		this.dueDays = dueDays;
	}
	public Integer getDeliveryBuffDays() {
		return deliveryBuffDays;
	}
	public void setDeliveryBuffDays(Integer deliveryBuffDays) {
		this.deliveryBuffDays = deliveryBuffDays;
	}
	public String getCatCode() {
		return catCode;
	}
	public void setCatCode(String catCode) {
		this.catCode = catCode;
	}
	public String getGstInNo() {
		return gstInNo;
	}
	public void setGstInNo(String gstInNo) {
		this.gstInNo = gstInNo;
	}
	public String getGstStateCode() {
		return gstStateCode;
	}
	public void setGstStateCode(String gstStateCode) {
		this.gstStateCode = gstStateCode;
	}
	public Date getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	
	public BigDecimal getLeadTime() {
		return leadTime;
	}
	public void setLeadTime(BigDecimal leadTime) {
		this.leadTime = leadTime;
	}
	
	public Suppliers(BigDecimal leadTime) {
		super();
		this.leadTime = leadTime;
	}
	
	@Override
	public String toString() {
		return "Suppliers [id=" + id + ", slCode=" + slCode + ", slName=" + slName + ", slAddr=" + slAddr
				+ ", slCityName=" + slCityName + ", transporterCode=" + transporterCode + ", transporterName="
				+ transporterName + ", agentCode=" + agentCode + ", agentName=" + agentName + ", agentRate=" + agentRate
				+ ", creditDays=" + creditDays + ", isExt=" + isExt + ", purchTermCode=" + purchTermCode
				+ ", creditAmt=" + creditAmt + ", lgtAPPL=" + lgtAPPL + ", dueDateBasis=" + dueDateBasis + ", brand="
				+ brand + ", product=" + product + ", outLgtAPPL=" + outLgtAPPL + ", gateInAPPL=" + gateInAPPL
				+ ", tradeGrpCode=" + tradeGrpCode + ", dueDays=" + dueDays + ", deliveryBuffDays=" + deliveryBuffDays
				+ ", catCode=" + catCode + ", gstInNo=" + gstInNo + ", gstStateCode=" + gstStateCode + ", timestamp="
				+ timestamp + "]";
	}
	
}
