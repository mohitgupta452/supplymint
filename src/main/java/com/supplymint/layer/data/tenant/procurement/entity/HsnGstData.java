package com.supplymint.layer.data.tenant.procurement.entity;

import java.math.BigDecimal;
import java.math.MathContext;
import java.sql.Timestamp;

public class HsnGstData {

	private String slab;
	private String slabBasis;
	private Timestamp effectiveDate;
	private String hsnSacCode;
	private BigDecimal amountFrom;
	private String taxName;
	private BigDecimal cgstRate;
	private BigDecimal sgstRate;
	private BigDecimal igstRate;
	private BigDecimal cessRate;

	public String getSlab() {
		return slab;
	}

	public void setSlab(String slab) {
		this.slab = slab;
	}

	public Timestamp getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Timestamp effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public BigDecimal getAmountFrom() {
		return amountFrom;
	}

	public void setAmountFrom(BigDecimal amountFrom) {
		this.amountFrom = amountFrom;
	}

	public String getTaxName() {
		return taxName;
	}

	public void setTaxName(String taxName) {
		this.taxName = taxName;
	}

	public BigDecimal getCgstRate() {
		return cgstRate;
	}

	public void setCgstRate(BigDecimal cgstRate) {
		this.cgstRate = cgstRate;
	}

	public BigDecimal getSgstRate() {
		return sgstRate;
	}

	public void setSgstRate(BigDecimal sgstRate) {
		this.sgstRate = sgstRate;
	}

	public BigDecimal getIgstRate() {
		return igstRate;
	}

	public void setIgstRate(BigDecimal igstRate) {
		this.igstRate = igstRate;
	}

	public BigDecimal getCessRate() {
		return cessRate;
	}

	public void setCessRate(BigDecimal cessRate) {
		this.cessRate = cessRate;
	}

	public String getSlabBasis() {
		return slabBasis;
	}

	public void setSlabBasis(String slabBasis) {
		this.slabBasis = slabBasis;
	}

	public HsnGstData(String slab, String slabBasis, Timestamp effectiveDate, String hsnSacCode, BigDecimal amountFrom,
			String taxName, BigDecimal cgstRate, BigDecimal sgstRate, BigDecimal igstRate, BigDecimal cessRate) {
		super();
		this.slab = slab;
		this.slabBasis = slabBasis;
		this.effectiveDate = effectiveDate;
		this.hsnSacCode = hsnSacCode;
		this.amountFrom = amountFrom;
		this.taxName = taxName;
		this.cgstRate = cgstRate;
		this.sgstRate = sgstRate;
		this.igstRate = igstRate;
		this.cessRate = cessRate;
	}

	public HsnGstData(String slab, String slabBasis, Timestamp effectiveDate, String hsnSacCode, BigDecimal amountFrom,
			String taxName, Double cgstRate, Double sgstRate, Double igstRate, Double cessRate) {
		super();
		this.slab = slab;
		this.slabBasis = slabBasis;
		this.effectiveDate = effectiveDate;
		this.hsnSacCode = hsnSacCode;
		this.amountFrom = amountFrom;
		this.taxName = taxName;
		this.cgstRate = new BigDecimal(cgstRate, MathContext.DECIMAL64);
		this.sgstRate = new BigDecimal(sgstRate, MathContext.DECIMAL64);
		this.igstRate = new BigDecimal(igstRate, MathContext.DECIMAL64);
		this.cessRate = new BigDecimal(cessRate, MathContext.DECIMAL64);
	}

	public HsnGstData(Timestamp effectiveDate, String slab) {
		super();
		this.effectiveDate = effectiveDate;
		this.slab = slab;
	}

	public HsnGstData(BigDecimal amountFrom) {
		super();
		this.amountFrom = amountFrom;
	}

	@Override
	public String toString() {
		return "HsnGstData [slab=" + slab + ", effectiveDate=" + effectiveDate + ", hsnSacCode=" + hsnSacCode
				+ ", amountFrom=" + amountFrom + ", taxName=" + taxName + ", cgstRate=" + cgstRate + ", sgstRate="
				+ sgstRate + ", igstRate=" + igstRate + ", cessRate=" + cessRate + "]";
	}

}
