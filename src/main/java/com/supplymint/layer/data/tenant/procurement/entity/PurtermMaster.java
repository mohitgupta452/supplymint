package com.supplymint.layer.data.tenant.procurement.entity;

import java.math.BigDecimal;

import oracle.sql.TIMESTAMP;

public class PurtermMaster {

	private BigDecimal id;
	private String code;
	private String name;
	private String ext;
	private String finTradeGroupCode;
	private String headerLevelIsChangeable;
	private String linelevelIsChangeable;
	private String purchaseGLCode;
	private String purchaseSLCode;
	private String purchaseReturnGLCode;
	private String purchaseReturnSLCode;
	private TIMESTAMP creationTime;

	public PurtermMaster() {
		super();
	}

	public PurtermMaster(BigDecimal id, String code, String name, String ext, String finTradeGroupCode,
			String headerLevelIsChangeable, String linelevelIsChangeable, String purchaseGLCode, String purchaseSLCode,
			String purchaseReturnGLCode, String purchaseReturnSLCode, TIMESTAMP creationTime) {
		super();
		this.id = id;
		this.code = code;
		this.name = name;
		this.ext = ext;
		this.finTradeGroupCode = finTradeGroupCode;
		this.headerLevelIsChangeable = headerLevelIsChangeable;
		this.linelevelIsChangeable = linelevelIsChangeable;
		this.purchaseGLCode = purchaseGLCode;
		this.purchaseSLCode = purchaseSLCode;
		this.purchaseReturnGLCode = purchaseReturnGLCode;
		this.purchaseReturnSLCode = purchaseReturnSLCode;
		this.creationTime = creationTime;
	}

	public PurtermMaster(String code, String name, String finTradeGroupCode) {
		super();
		this.code = code;
		this.name = name;
		this.finTradeGroupCode = finTradeGroupCode;
	}

	public PurtermMaster(String code, String name) {
		super();
		this.code = code;
		this.name = name;
	}

	public BigDecimal getId() {
		return id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getExt() {
		return ext;
	}

	public void setExt(String ext) {
		this.ext = ext;
	}

	public String getFinTradeGroupCode() {
		return finTradeGroupCode;
	}

	public void setFinTradeGroupCode(String finTradeGroupCode) {
		this.finTradeGroupCode = finTradeGroupCode;
	}

	public String getHeaderLevelIsChangeable() {
		return headerLevelIsChangeable;
	}

	public void setHeaderLevelIsChangeable(String headerLevelIsChangeable) {
		this.headerLevelIsChangeable = headerLevelIsChangeable;
	}

	public String getLinelevelIsChangeable() {
		return linelevelIsChangeable;
	}

	public void setLinelevelIsChangeable(String linelevelIsChangeable) {
		this.linelevelIsChangeable = linelevelIsChangeable;
	}

	public String getPurchaseGLCode() {
		return purchaseGLCode;
	}

	public void setPurchaseGLCode(String purchaseGLCode) {
		this.purchaseGLCode = purchaseGLCode;
	}

	public String getPurchaseSLCode() {
		return purchaseSLCode;
	}

	public void setPurchaseSLCode(String purchaseSLCode) {
		this.purchaseSLCode = purchaseSLCode;
	}

	public String getPurchaseReturnGLCode() {
		return purchaseReturnGLCode;
	}

	public void setPurchaseReturnGLCode(String purchaseReturnGLCode) {
		this.purchaseReturnGLCode = purchaseReturnGLCode;
	}

	public String getPurchaseReturnSLCode() {
		return purchaseReturnSLCode;
	}

	public void setPurchaseReturnSLCode(String purchaseReturnSLCode) {
		this.purchaseReturnSLCode = purchaseReturnSLCode;
	}

	public TIMESTAMP getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(TIMESTAMP creationTime) {
		this.creationTime = creationTime;
	}

	@Override
	public String toString() {
		return "PurtermMaster [id=" + id + ", code=" + code + ", name=" + name + ", ext=" + ext + ", finTradeGroupCode="
				+ finTradeGroupCode + ", headerLevelIsChangeable=" + headerLevelIsChangeable
				+ ", linelevelIsChangeable=" + linelevelIsChangeable + ", purchaseGLCode=" + purchaseGLCode
				+ ", purchaseSLCode=" + purchaseSLCode + ", purchaseReturnGLCode=" + purchaseReturnGLCode
				+ ", purchaseReturnSLCode=" + purchaseReturnSLCode + ", creationTime=" + creationTime + "]";
	}

}
