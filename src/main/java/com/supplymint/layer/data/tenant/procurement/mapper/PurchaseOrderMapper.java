package com.supplymint.layer.data.tenant.procurement.mapper;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.supplymint.layer.data.tenant.administration.entity.ADMItem;
import com.supplymint.layer.data.tenant.administration.entity.PurchaseOrderPricePoint;
import com.supplymint.layer.data.tenant.entity.procurement.config.DeptItemUDFMappings;
import com.supplymint.layer.data.tenant.procurement.entity.Category;
import com.supplymint.layer.data.tenant.procurement.entity.DeptItemUDFSettings;
import com.supplymint.layer.data.tenant.procurement.entity.DeptSizeData;
import com.supplymint.layer.data.tenant.procurement.entity.FinCharge;
import com.supplymint.layer.data.tenant.procurement.entity.IndtCatDescUDFList;
import com.supplymint.layer.data.tenant.procurement.entity.InvSetUDF;
import com.supplymint.layer.data.tenant.procurement.entity.ProcurementUploadSummary;
import com.supplymint.layer.data.tenant.procurement.entity.PurchaseIndent;
import com.supplymint.layer.data.tenant.procurement.entity.PurchaseIndentHistory;
import com.supplymint.layer.data.tenant.procurement.entity.PurchaseOrder;
import com.supplymint.layer.data.tenant.procurement.entity.PurchaseOrderAudit;
import com.supplymint.layer.data.tenant.procurement.entity.PurchaseOrderChild;
import com.supplymint.layer.data.tenant.procurement.entity.PurchaseOrderConfiguration;
import com.supplymint.layer.data.tenant.procurement.entity.PurchaseOrderParent;
import com.supplymint.layer.data.tenant.procurement.entity.TempPurchaseOrderChild;
import com.supplymint.layer.data.tenant.procurement.entity.UDF_SettingsMaster;

@Mapper
public interface PurchaseOrderMapper {

	@Select("select pattern from PIMAIN  OFFSET #{offset} ROWS FETCH NEXT #{pagesize} ROWS ONLY")
	List<String> getAllPIPattern(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize);

	@Select("select COUNT(*) from PIMAIN ")
	Integer getCountAllPIPattern();

//	@Select("SELECT * FROM ARTICLEHLEVELDATA OFFSET #{offset} ROWS FETCH NEXT #{pagesize} ROWS ONLY")
	List<PurchaseOrderPricePoint> getArticleHierarchyLevelData(@Param("offset") Integer offset,
			@Param("pagesize") Integer pagesize);

	@Select("select count(*) from (SELECT * FROM ARTICLEHLEVELDATA)")
	Integer getRecordArticleHierarchyLevelData();

	List<PurchaseOrderPricePoint> searchAllArticleHierarchyLevelData(@Param("offset") Integer offset,
			@Param("pagesize") Integer pagesize, @Param("search") String search);

	Integer getRecordForSearchAllArticleHierarchyLevelData(@Param("search") String search);

	List<PurchaseOrderPricePoint> filterArticleHierarchyLevelData(@Param("offset") Integer offset,
			@Param("pagesize") Integer pagesize, @Param("hl4Code") String hl4Code, @Param("hl4Name") String hl4Name,
			@Param("hl1Name") String hl1Name, @Param("hl2Name") String hl2Name, @Param("hl3Name") String hl3Name);

	Integer filterRecordArticleHierarchyLevelData(@Param("hl4Code") String hl4Code, @Param("hl4Name") String hl4Name,
			@Param("hl1Name") String hl1Name, @Param("hl2Name") String hl2Name, @Param("hl3Name") String hl3Name);

//	@Select("SELECT icode,itemname,cost_price,sell_price FROM INVITEM WHERE hl4code = #{hl4Code} OFFSET #{offset} ROWS FETCH NEXT #{pagesize} ROWS ONLY")
	List<ADMItem> getItemsByArtCode(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("hl4Code") String hl4Code);

	@Select("SELECT COUNT(*) FROM INVITEM WHERE hl4code = #{hl4Code}")
	Integer getCountItemsByArtCode(@Param("hl4Code") String hl4Code);

	List<ADMItem> searchItemsByArtCode(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("hl4Code") String hl4Code, @Param("search") String search);

	Integer searchCountItemsByArtCode(@Param("hl4Code") String hl4Code, @Param("search") String search);

	List<ADMItem> filterItemsByArtCode(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("hl4Code") String hl4Code, @Param("itemCode") String itemCode, @Param("itemName") String itemName,
			@Param("mrp") String mrp, @Param("rsp") String rsp);

	Integer filterCountItemsByArtCode(@Param("hl4Code") String hl4Code, @Param("itemCode") String itemCode,
			@Param("itemName") String itemName, @Param("mrp") String mrp, @Param("rsp") String rsp);

	List<String> filterAllPIPattern(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("pattern") String pattern);

	Integer countFilterAllPIPattern(@Param("pattern") String pattern);

	List<String> searchAllPIPattern(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("search") String search);

	Integer countSearchAllPIPattern(@Param("search") String search);

	List<PurchaseIndent> getPIData(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize);

	@Select("select COUNT(*) from PIMAIN")
	Integer getCountPIData();

	List<PurchaseIndent> searchPIData(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("search") String search);

	Integer searchCountPIData(@Param("search") String search);

	List<PurchaseIndent> filterPIData(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("poAmount") String poAmount, @Param("poQuantity") String poQuantity,
			@Param("pattern") String pattern);

	Integer filterCountPIData(@Param("poAmount") String poAmount, @Param("poQuantity") String poQuantity,
			@Param("pattern") String pattern);

	List<UDF_SettingsMaster> getUDFMappingData();

	List<UDF_SettingsMaster> getPOUDFSettingData();

	// @Select("SELECT COUNT(*) from UDF_MAPPINGS")
	Integer getUDFMappingDataRecord();

	List<UDF_SettingsMaster> searchUDFMappingData(@Param("search") String search);

	Integer searchUDFMappingDataRecord(@Param("search") String search);

	List<UDF_SettingsMaster> filterUDFMappingData(@Param("udfType") String udfType,
			@Param("isCompulsary") String isCompulsary, @Param("displayName") String displayName);

	Integer filterUDFMappingDataRecord(@Param("udfType") String udfType, @Param("isCompulsary") String isCompulsary,
			@Param("displayName") String displayName);

	List<InvSetUDF> getInvSetUDFDataByUDFType(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("udfType") String udfType);

	List<InvSetUDF> getInvSetUDFDataByUDFTypeExt(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("udfType") String udfType);

	Integer getInvSetUDFDataByUDFTypeRecord(@Param("udfType") String udfType);

	Integer getInvSetUDFDataByUDFTypeExtRecord(@Param("udfType") String udfType);

	Integer getInvSetPOItemUDFDataRecord(@Param("udfType") String udfType);

	List<InvSetUDF> getInvSetPOItemUDFData(@Param("udfType") String udfType);

	List<InvSetUDF> searchInvSetUDFDataByUDFType(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("udfType") String udfType, @Param("search") String search);

	List<InvSetUDF> searchInvSetUDFDataByUDFTypeRecordExt(@Param("offset") Integer offset,
			@Param("pagesize") Integer pagesize, @Param("udfType") String udfType, @Param("search") String search);

	Integer searchInvSetUDFDataByUDFTypeRecord(@Param("udfType") String udfType, @Param("search") String search);

	Integer searchInvSetUDFDataByUDFTypeExtRecord(@Param("udfType") String udfType, @Param("search") String search);

	List<InvSetUDF> filterInvSetUDFDataByUDFType(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("udfType") String udfType, @Param("code") String code, @Param("name") String name);

	List<InvSetUDF> filterInvSetUDFDataByUDFTypeExt(@Param("offset") Integer offset,
			@Param("pagesize") Integer pagesize, @Param("udfType") String udfType, @Param("code") String code,
			@Param("name") String name);

	Integer filterInvSetUDFDataByUDFTypeRecord(@Param("udfType") String udfType, @Param("code") String code,
			@Param("name") String name);

	Integer filterInvSetUDFDataByUDFTypeExtRecord(@Param("udfType") String udfType, @Param("code") String code,
			@Param("name") String name);

	List<InvSetUDF> filterInvSetPOItemUDFData(@Param("udfType") String udfType, @Param("code") String code,
			@Param("name") String name);

	Integer filterInvSetPOItemUDFDataRecord(@Param("udfType") String udfType, @Param("code") String code,
			@Param("name") String name);

	List<InvSetUDF> searchInvSetPOItemUDFData(@Param("udfType") String udfType, @Param("search") String search);

	Integer searchInvSetPOItemUDFDataRecord(@Param("udfType") String udfType, @Param("search") String search);

	int createPO(PurchaseOrder po);

	List<Map<String, String>> findAllDescUdf(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("header") String header);

	List<Map<String, String>> findAllCatValues(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("header") String header);

	Integer countPOHistory(@Param("orgId") String orgId);

	Integer countCatValues(@Param("header") String header);

	Integer countDescUDFValues(@Param("header") String header);

	List<String> searchCatDescUDF(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("search") String search, @Param("header") String header);

	Integer searchCountCatValues(@Param("search") String search, @Param("header") String header);

	Integer searchCountDescUDFValues(@Param("search") String search, @Param("header") String header);

	List<Map<String, String>> searchCatValues(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("search") String search, @Param("header") String header);

	List<Map<String, String>> searchDescUDFValues(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("search") String search, @Param("header") String header);

	int searchArticleCount(@Param("search") String search, @Param("hl3Code") String hl3Code,
			@Param("catDescUDF") String catDescUDF);

	List<Map<String, String>> searchArticle(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("search") String search, @Param("hl3Code") String hl3Code, @Param("catDescUDF") String catDescUDF);

	List<DeptSizeData> getSizeByDept(String dept);

	List<DeptSizeData> getOtherSizeByDept(String dept);

	@Update("UPDATE UDF_SETTINGS SET DISPLAYNAME = #{displayName, jdbcType=VARCHAR}, ISCOMPULSARY = #{isCompulsary,jdbcType=CHAR}, ORDERBY = #{orderBy, jdbcType=VARCHAR}, ISLOV = #{islov,jdbcType=CHAR}  WHERE UDFTYPE = #{udfType}")
	int updateUDFMap(@Param("displayName") String displayName, @Param("isCompulsary") char isCompulsary,
			@Param("orderBy") String orderBy, @Param("udfType") String udfType, @Param("islov") char islov);

//	int updateUDFMap(UDF_SettingsMaster udf);

	PurchaseOrderConfiguration getPOConfiguration(@Param("cid") int cid);

	int createPOAudit(PurchaseOrderAudit poa);

	List<IndtCatDescUDFList> getIndtCatDescUDFListData();

	List<IndtCatDescUDFList> getIndtCatDescUDFListActiveData();

	List<DeptItemUDFSettings> getDeptItemUDFByHl3Name(@Param("hl3code") String hl3code);

	List<DeptItemUDFSettings> getPODeptItemUDFByHl3Name(@Param("hl3code") String hl3code);

	// HL4code remove and variable name changed
	List<DeptItemUDFMappings> getItemUdfMappingByHl3NameAndUDF(@Param("offSet") int offSet,
			@Param("pageSize") int pageSize, @Param("hl3code") String hl3code,
			@Param("cat_desc_udf") String cat_desc_udf, @Param("description") String description,
			@Param("hl4Code") String hl4Code);

	// hl4code removed
	int countItemUdfMappingByHl3NameAndUDF(@Param("hl3code") String hl3code, @Param("cat_desc_udf") String cat_desc_udf,
			@Param("description") String description, @Param("hl4Code") String hl4Code);

//variable udfName changed to description
	List<DeptItemUDFMappings> getItemUdfMappingByHl3NameUDFEXT(@Param("offSet") int offSet,
			@Param("pageSize") int pageSize, @Param("hl3code") String hl3code,
			@Param("cat_desc_udf") String cat_desc_udf);

	int countItemUdfMappingByHl3NameUDFEXT(@Param("hl3code") String hl3code,
			@Param("cat_desc_udf") String cat_desc_udf);

	int updateDeptItemUDF(DeptItemUDFSettings udf);

	int createInvSetUdf(InvSetUDF invSetUDF);

	int updateInvSetUdf(InvSetUDF invSetUDF);

	List<DeptItemUDFSettings> getItemHeaderForPOCreate(@Param("hl3code") String hl3code);

	int createDeptItemUDFMappings(DeptItemUDFMappings deptItemUDFMappings);

	int updateDeptItemUDFMappings(DeptItemUDFMappings deptItemUDFMappings);

	int updateSizeMapping(DeptSizeData deptSize);

	List<Map<String, String>> getArticles(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("hl3Code") String hl3Code, @Param("catDescUDF") String catDescUDF);

	int countArticles(@Param("hl3Code") String hl3Code, @Param("catDescUDF") String catDescUDF);

//	int countSearchArticles(@Param("hl3name") String hl3name, @Param("search") String search);

	// variable name changed
	int searchCountItemUDFMapping(@Param("search") String search, @Param("hl3code") String hl3code,
			@Param("cat_desc_udf") String cat_desc_udf, @Param("hl4code") String hl4code);

	int searchCountPOItemUDFMapping(@Param("hl3code") String hl3code, @Param("search") String search,
			@Param("cat_desc_udf") String search2);

	// variable name changed
	List<DeptItemUDFMappings> searchItemUDFMapping(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("search") String search, @Param("hl3code") String hl3code,
			@Param("cat_desc_udf") String cat_desc_udf, @Param("hl4code") String hl4code);

	List<DeptItemUDFMappings> searchItemUDFMappingEXT(@Param("offset") Integer offset,
			@Param("pagesize") Integer pagesize, @Param("search") String search, @Param("hl3code") String hl3code,
			@Param("cat_desc_udf") String cat_desc_udf);

	List<DeptItemUDFMappings> searchPOItemUDFMapping(@Param("offset") Integer offset,
			@Param("pagesize") Integer pagesize, @Param("hl3code") String hl3code, @Param("search") String search);

	@Select("SELECT count(*) from DEPT_SIZE_DATA WHERE lower(HL3NAME) = lower(#{dept}) AND lower(ID) = lower(#{id})")
	int countSizeMappingByDepartment(@Param("dept") String dept, @Param("id") int id);

	List<PurchaseOrder> findPOBySetHeader(@Param("setHeader") String setHeader);

	@Select("SELECT count(*) FROM INV_SETUDF WHERE lower(UDFTYPE) = lower(#{udfType}) AND lower(NAME) = lower(#{name})")
	int checkUDFMappingCreationTimeExistanceValidation(@Param("udfType") String udfType, @Param("name") String name);

	@Select("SELECT * FROM DEPT_SIZE_DATA WHERE lower(HL3NAME) = lower(#{dept}) AND lower(ID)= lower(#{id})")
	List<DeptSizeData> checkDeptSizeMappingExistanceValidation(@Param("dept") String dept, @Param("id") int id);

//	TABLE NAME  AND VARIABLE NAME CHANGED
//	@Select("SELECT count(*) FROM dept_cat_desc_udf_mapping WHERE lower(HL3CODE) = lower(#{hl3code}) AND lower(DESCRIPTION) = lower(#{description}) AND lower(CAT_DESC_UDF)=lower(#{cat_desc_udf})")
	int checkDeptItemUdfMappingExistanceValidation(@Param("hl3code") String hl3code,
			@Param("cat_desc_udf") String cat_desc_udf, @Param("description") String description,
			@Param("hl4Code") String hl4code);

	@Select("SELECT count(*) FROM DEPT_CAT_DESC_UDF_SETTING WHERE ORDERBY=#{orderBy,jdbcType=VARCHAR} AND CAT_DESC_UDF=#{cat_desc_udf} AND HL3CODE=#{hl3code}")
	int validationOrderBy(@Param("orderBy") String orderBy, @Param("cat_desc_udf") String cat_desc_udf,
			@Param("hl3code") String hl3code);

	@Select("SELECT count(*) FROM DEPT_CAT_DESC_UDF_SETTING WHERE DISPLAY_NAME=#{checkDisplayName,jdbcType=VARCHAR} AND CAT_DESC_UDF=#{cat_desc_udf} AND HL3CODE=#{hl3code}")
	int validateDisplayName(@Param("checkDisplayName") String checkDisplayName,
			@Param("cat_desc_udf") String cat_desc_udf, @Param("hl3code") String hl3code);

	@Select("SELECT count(*) FROM DEPT_CAT_DESC_UDF_SETTING WHERE ORDERBY=#{orderBy,jdbcType=VARCHAR} AND DISPLAY_NAME=#{checkDisplayName,jdbcType=VARCHAR}")
	int countOrderByAndDisplayName(@Param("orderBy") String orderBy,
			@Param("checkDisplayName") String checkDisplayName);

	@Select("SELECT COUNT(*) FROM DEPT_SIZE_DATA WHERE  ORDERBY=#{orderByInDb,jdbcType=VARCHAR}")
	int checkOrderBy(String orderByInDb);

	@Select("SELECT ORDERBY FROM DEPT_SIZE_DATA WHERE HL3NAME=#{hl3name} and CNAME NOT IN(#{newCnameString})")
	List<String> getAllData(@Param("newCnameString") String newCnameString, @Param("hl3name") String hl3name);

	@Select("SELECT COUNT(ORDERBY) FROM DEPT_SIZE_DATA WHERE HL3NAME=#{hl3name} AND ORDERBY=#{payLoadOrderBy}")
	int getAllHlnameAndOrderBY(@Param("hl3name") String hl3name, @Param("payLoadOrderBy") String payLoadOrderBy);

	Set<String> getSizeMappingSizes(@Param("hl3Code") String hl3Code, @Param("sizes") List sizes);

	Set<String> getSizeMappingSizesByHl3name(@Param("hl3Code") String hl3Code);

	Set<String> getAllDisplayNames();

	Set<String> getDisplayNames(@Param("listOfNames") List listOfNames);

	Set<Integer> getAllUdfSettingsOrderBy();

	Set<Integer> getUdfSettingsOrderBy(@Param("listOfOrderBy") List listOfOrderBy);

	int updateUDFSetting(UDF_SettingsMaster udfSetting);

	@Select("SELECT COUNT(*) FROM (SELECT distinct TO_NUMBER(CNAME) AS listedMRP from PO_DESC6_DATA\n"
			+ "		where hl4code = #{hl4Code} and to_number(cname) between to_number(#{mrprangefrom}) and to_number(#{mrprangeto}) )")
	int countDesc6Values(@Param("hl4Code") String hl4Code, @Param("mrprangefrom") String mrprangefrom,
			@Param("mrprangeto") String mrprangeto);

	@Select("SELECT COUNT(*) FROM (SELECT distinct udf4 AS listedMRP from PRPO_MRP\n"
			+ "		where hl4code = #{hl4Code} and to_number(udf4) between to_number(#{mrprangefrom}) and to_number(#{mrprangeto}) )")
	int countOtherDesc6Values(@Param("hl4Code") String hl4Code, @Param("mrprangefrom") String mrprangefrom,
			@Param("mrprangeto") String mrprangeto);

	@Select("SELECT distinct to_number(cname) AS listedMRP from PO_DESC6_DATA\n"
			+ "		where hl4code = #{hl4Code} and to_number(cname) between to_number(#{mrprangefrom}) and to_number(#{mrprangeto}) OFFSET\n"
			+ "		#{offset} ROWS FETCH NEXT #{pagesize} ROWS ONLY")
	List<String> getDesc6Values(@Param("hl4Code") String hl4Code, @Param("mrprangefrom") String mrprangefrom,
			@Param("mrprangeto") String mrprangeto, @Param("offset") Integer offset,
			@Param("pagesize") Integer pagesize);

	@Select("SELECT distinct udf4 AS listedMRP from PRPO_MRP\n"
			+ "		where hl4code = #{hl4Code} and to_number(udf4) between to_number(#{mrprangefrom}) and to_number(#{mrprangeto}) OFFSET\n"
			+ "		#{offset} ROWS FETCH NEXT #{pagesize} ROWS ONLY")
	List<String> getOtherDesc6Values(@Param("hl4Code") String hl4Code, @Param("mrprangefrom") String mrprangefrom,
			@Param("mrprangeto") String mrprangeto, @Param("offset") Integer offset,
			@Param("pagesize") Integer pagesize);

	Integer countLoadIndentFilter(@Param("orderId") String orderId, @Param("supplier") String supplier,
			@Param("cityName") String cityName, @Param("piFromDate") String piFromDate,
			@Param("piToDate") String piToDate);

	List<PurchaseIndentHistory> getLoadIndentFilter(@Param("orderId") String orderId,
			@Param("supplier") String supplier, @Param("cityName") String cityName,
			@Param("piFromDate") String piFromDate, @Param("piToDate") String piToDate, @Param("offSet") int offSet,
			@Param("pageSize") int pageSize);

	Integer countLoadIndentSearch(@Param("search") String search);

	List<PurchaseIndentHistory> getLoadIndentSearch(@Param("search") String search, @Param("offSet") int offSet,
			@Param("pageSize") int pageSize);

	int createMainPO(PurchaseOrderParent po);

	int createDetailPO(PurchaseOrderChild po);

	Integer createFinCharge(FinCharge charge);

	List<PurchaseOrderParent> findAllPOHistory(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("orgId") String orgId);

	Integer countPOHistoryFilter(@Param("orderNo") String orderNo, @Param("orderDate") String orderDate,
			@Param("validFrom") String validFrom, @Param("validTo") String validTo, @Param("slCode") String slCode,
			@Param("slName") String slName, @Param("slCityName") String slCityName, @Param("hl1Name") String hl1Name,
			@Param("hl2Name") String hl2Name, @Param("hl3Name") String hl3Name, @Param("orgId") String orgId);

	List<PurchaseOrderParent> findAllPOHistoryFilter(@Param("orderNo") String orderNo,
			@Param("orderDate") String orderDate, @Param("validFrom") String validFrom,
			@Param("validTo") String validTo, @Param("slCode") String slCode, @Param("slName") String slName,
			@Param("slCityName") String slCityName, @Param("hl1Name") String hl1Name, @Param("hl2Name") String hl2Name,
			@Param("hl3Name") String hl3Name, @Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("orgId") String orgId);

	Integer searchPOHistoryCount(@Param("search") String search, @Param("orgId") String orgId);

	List<PurchaseOrderParent> searchAllPOHistory(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("search") String search, @Param("orgId") String orgId);

	@Select("SELECT COUNT(*) FROM (SELECT distinct hl3code, hl3name FROM POMAIN  where hl3name is not null )")
	Integer countPOSavedDepartment();

	@Select("SELECT distinct hl3code, hl3name FROM POMAIN  where hl3name is not null OFFSET #{offset} ROWS FETCH NEXT #{pagesize} ROWS ONLY")
	List<Map<String, String>> getPOSavedDepartment(@Param("offset") Integer offset,
			@Param("pagesize") Integer pagesize);

	Integer countPOSavedDepartmentSearch(@Param("search") String search);

	List<Map<String, String>> getPOSavedDepartmentSearch(@Param("offset") Integer offset,
			@Param("pagesize") Integer pagesize, @Param("search") String search);

	@Select("SELECT COUNT(*) FROM (SELECT distinct supplier_code, supplier_name FROM POMAIN  where supplier_name is not null and hl3code = #{hl3code})")
	Integer countPOSavedSupplier(@Param("hl3code") String hl3code);

	@Select("SELECT distinct supplier_code, supplier_name FROM POMAIN  where supplier_name is not null and hl3code = #{hl3code} OFFSET #{offset} ROWS FETCH NEXT #{pagesize} ROWS ONLY")
	List<Map<String, String>> getPOSavedSupplier(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("hl3code") String hl3code);

	Integer countPOSavedSupplierSearch(@Param("hl3code") String hl3code, @Param("search") String search);

	List<Map<String, String>> getPOSavedSupplierSearch(@Param("offset") Integer offset,
			@Param("pagesize") Integer pagesize, @Param("hl3code") String hl3code, @Param("search") String search);

	@Select("SELECT COUNT(*) FROM (SELECT distinct ORDER_NO FROM POMAIN  where supplier_code = #{supplierCode}  and hl3code = #{hl3code})")
	Integer countPOSavedOrderNo(@Param("supplierCode") String supplierCode, @Param("hl3code") String hl3code);

	@Select("SELECT distinct ORDER_NO FROM POMAIN  where supplier_code = #{supplierCode}  and hl3code = #{hl3code}  order by id desc OFFSET #{offset} ROWS FETCH NEXT #{pagesize} ROWS ONLY")
	List<Map<String, String>> getPOSavedOrderNo(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("hl3code") String hl3code, @Param("supplierCode") String supplierCode);

	Integer countPOSavedOrderNoSearch(@Param("hl3code") String hl3code, @Param("supplierCode") String supplierCode,
			@Param("search") String search);

	List<Map<String, String>> getPOSavedOrderNoSearch(@Param("offset") Integer offset,
			@Param("pagesize") Integer pagesize, @Param("hl3code") String hl3code,
			@Param("supplierCode") String supplierCode, @Param("search") String search);

	PurchaseOrderParent getPurchaseOrderMain(@Param("orderNo") String orderNo);

	List<PurchaseOrderChild> getPurchaseOrderChild(@Param("orderNo") String orderNo);

	int countPOAdhocDesc6Search(@Param("hl4Code") String hl4Code, @Param("mrprangefrom") String mrprangefrom,
			@Param("mrprangeto") String mrprangeto, @Param("search") String search);

	int countOtherPOAdhocDesc6Search(@Param("hl4Code") String hl4Code, @Param("mrprangefrom") String mrprangefrom,
			@Param("mrprangeto") String mrprangeto, @Param("search") String search);

	List<String> getPOAdhocDesc6Search(@Param("hl4Code") String hl4Code, @Param("mrprangefrom") String mrprangefrom,
			@Param("mrprangeto") String mrprangeto, @Param("offset") Integer offset,
			@Param("pagesize") Integer pagesize, @Param("search") String search);

	List<String> getOtherPOAdhocDesc6Search(@Param("hl4Code") String hl4Code,
			@Param("mrprangefrom") String mrprangefrom, @Param("mrprangeto") String mrprangeto,
			@Param("offset") Integer offset, @Param("pagesize") Integer pagesize, @Param("search") String search);

	int createBatchInsertUDFSetting(@Param("list") List<DeptItemUDFSettings> itemSetting);

	int checkDisplayNameExistance(@Param("displayName") String displayName, @Param("hl3code") String hl3code,
			@Param("cat_desc_udf") String cat_desc_udf, @Param("hl4Code") String hl4Code,
			@Param("description") String description);

	int updateDeptItemMappings(DeptItemUDFMappings deptItemUDFMappings);

	List<DeptItemUDFMappings> searchItemUdfMappingByHl3NameAndUDFforOthers(@Param("offset") int offset,
			@Param("pagesize") int pageSize, @Param("hl3code") String hl3code,
			@Param("cat_desc_udf") String cat_desc_udf, @Param("search") String search,
			@Param("hl4code") String hl4code);

	int searchCountItemUdfMappingByHl3NameAndUDFforOther(@Param("hl3code") String hl3code,
			@Param("cat_desc_udf") String cat_desc_udf, @Param("search") String search,
			@Param("hl4code") String hl4code);

	List<DeptItemUDFMappings> itemUdfMappingByHl3NameAndUDFForOthers(@Param("offSet") int offset,
			@Param("pageSize") int pageSize, @Param("hl3code") String hl3code,
			@Param("cat_desc_udf") String cat_desc_udf, @Param("description") String description,
			@Param("hl4Code") String hl4code);

	int countItemUdfMappingByHl3NameAndUDFForOthers(@Param("hl3code") String hl3code,
			@Param("cat_desc_udf") String cat_desc_udf, @Param("description") String description,
			@Param("hl4Code") String hl4code);

	@Select("SELECT DISPLAY_NAME FROM DEPT_CAT_DESC_UDF_MAPPING WHERE HL3CODE=#{hl3code} AND CAT_DESC_UDF=#{cat_desc_udf} AND HL4CODE=#{hl4Code} AND DESCRIPTION=#{description}")
	List<String> getAllDisplayNameItemUdfMapping(@Param("hl3code") String hl3code,
			@Param("cat_desc_udf") String cat_desc_udf, @Param("hl4Code") String hl4Code,
			@Param("description") String description);

	int insertUploadSummary(ProcurementUploadSummary summary);

//	@Update("UPDATE PROCUREMENT_UPLOAD_SUMMARY SET LASTSUMMARY ='FALSE' WHERE USERNAME =#{userName} AND UUID !=#{uuId}")
	int updateLastRecord(@Param("orgId") String orgId, @Param("uuId") String uuId);

	@Select("select count(*) from procurement_upload_summary")
	int getUploadSummaryCount();

	int insertDataInList(@Param("data") List<PurchaseOrder> data);

	int updateStatus(@Param("status") String status, @Param("count") int count, @Param("url") String url,
			@Param("uuId") String uuId, @Param("userName") String userName);

	@Select("SELECT KEY,VALUE FROM SYSTEM_DEFAULT_CONFIG WHERE KEY IN ('CUSTOM_PO','RESTRICTED_PRO_DATE','ESSENTIAL_PRO_PARAM') AND ORG_ID=#{orgId}")
	List<Map<String, String>> getAvailablePOKeys(@Param("orgId") String orgId);

	int recordOfFCGHistory(@Param("ogdId") String ogdId);

	List<ProcurementUploadSummary> getAllFCGHistory(@Param("offset") int offset, @Param("pagesize") int pagesize,
			@Param("ogdId") String ogdId);

	int filterRecordOfFCGHistory(@Param("uploadedDate") String uploadedDate, @Param("fileName") String fileName,
			@Param("fileRecord") String fileRecord, @Param("status") String status, @Param("orgId") String orgId);

	List<ProcurementUploadSummary> filterFCGHistory(@Param("uploadedDate") String uploadedDate,
			@Param("fileName") String fileName, @Param("fileRecord") String fileRecord, @Param("status") String status,
			@Param("offset") int offset, @Param("pagesize") int pagesize, @Param("orgId") String orgId);

	int searchRecordOfFCGHistory(@Param("search") String search, @Param("orgId") String orgId);

	List<ProcurementUploadSummary> searchFCGHistory(@Param("search") String search, @Param("offset") int offset,
			@Param("pagesize") int pagesize, @Param("orgId") String orgId);

	ProcurementUploadSummary getLastRecord(@Param("userName") String userName, @Param("orgId") String orgId);

	Map<String, String> getHoldDescUDF(@Param("hl3code") String hl3code, @Param("desc2") String desc2,
			@Param("desc3") String desc3, @Param("desc4") String desc4, @Param("desc5") String desc5,
			@Param("itemudf1") String itemudf1, @Param("itemudf2") String itemudf2, @Param("itemudf3") String itemudf3,
			@Param("itemudf4") String itemudf4, @Param("itemudf5") String itemudf5, @Param("itemudf6") String itemudf6,
			@Param("itemudf7") String itemudf7, @Param("itemudf8") String itemudf8, @Param("itemudf9") String itemudf9,
			@Param("itemudf10") String itemudf10, @Param("itemudf11") String itemudf11,
			@Param("itemudf12") String itemudf12, @Param("itemudf13") String itemudf13,
			@Param("itemudf14") String itemudf14, @Param("itemudf15") String itemudf15);

	Map<String, String> getHoldCategoriesMaster(@Param("hl3code") String hl3code, @Param("cat1") String cat1,
			@Param("cat2") String cat2, @Param("cat3") String cat3, @Param("cat4") String cat4);

	List<Category> getHoldSize(@Param("sizeColorList") List sizeColorList, @Param("hl3code") String hl3code);

	List<Category> getHoldColor(@Param("sizeColorList") List sizeColorList, @Param("hl3code") String hl3code);

	ProcurementUploadSummary getSingleRecord(@Param("userName") String userName, @Param("orgId") String orgId,
			@Param("uuId") String uuId);

	int updateStatus1(@Param("status") String status, @Param("count") int count, @Param("url") String url,
			@Param("downloadBucketKey") String downloadBucketKey, @Param("uuId") String uuId,
			@Param("bucketName") String bucketName, @Param("bucketPath") String bucketPath,
			@Param("successRecord") int successRecord, @Param("errorRecord") int errorRecord);

	List<String> getJobIdFCGFile(@Param("ogdId") String ogdId);

	List<TempPurchaseOrderChild> getNullSizeCode();

	String getSizeCode(@Param("sizeName") String sizeName);

	int updateSizeCode(@Param("oldSize") String oldSize, @Param("newSize") String newSize);

	List<TempPurchaseOrderChild> getNullColorCode();

	String getColorCode(@Param("colorName") String colorName);

	int updateColorCode(@Param("oldColor") String oldColor, @Param("newColor") String newColor);

	@Update("UPDATE SYSTEM_DEFAULT_CONFIG SET VALUE=#{dateString,jdbcType=VARCHAR} WHERE KEY='RESTRICTED_PRO_DATE' AND ORG_ID=#{orgId}")
	int configProcurementDate(@Param("dateString") String dateString, @Param("orgId") String orgId);

	int countTotalDistinctCities();

	int countAllSearchedCities(@Param("search") String search);

	@Select("SELECT DISTINCT SLCITYNAME FROM TRANSPORTERS ORDER BY SLCITYNAME ASC OFFSET #{offset} ROWS FETCH NEXT #{pageSize} ROWS ONLY")
	List<String> getAllCitiesDetails(@Param("offset") int offset, @Param("pageSize") int pageSize);

	@Select("SELECT DISTINCT SLCITYNAME FROM TRANSPORTERS WHERE lower(SLCITYNAME) LIKE '%'|| lower(#{search}) || '%' ORDER BY SLCITYNAME ASC OFFSET #{offset} ROWS FETCH NEXT #{pageSize} ROWS ONLY")
	List<String> getAllSearchedCities(@Param("offset") int offset, @Param("pageSize") int pageSize,
			@Param("search") String search);

	List<Map<String, String>> getPODiscount(@Param("hl4code") String hl4code, @Param("mrp") String mrp,
			@Param("discountType") String discountType);

	int createDetailPOForOthers(PurchaseOrderChild poDetail);

	int createFinChargeForOthers(FinCharge fin);

	List<PurchaseOrderChild> getPurchaseOrderChildForOthers(String orderNo);
	
	@Select("SELECT COUNT(*) FROM DEPT_CAT_DESC_UDF_MAPPING WHERE HL3CODE=#{hl3Code}")
	int isExistDepartment(@Param("hl3Code") String hl3Code);
	
	int createSizeMappingForOthers(@Param("item")DeptSizeData deptSize,@Param("catDescType") String catDescType);

}
