package com.supplymint.layer.data.tenant.inventory.mapper;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.supplymint.layer.data.downloads.entity.DownloadRuleEngine;
import com.supplymint.layer.data.tenant.inventory.entity.EnterpriseSpecificOutput;
import com.supplymint.layer.data.tenant.inventory.entity.EnterpriseSpecificOutputNysaa;
import com.supplymint.layer.data.tenant.inventory.entity.HeadersSpecificOutput;
import com.supplymint.layer.data.tenant.inventory.entity.JobDetail;
import com.supplymint.layer.data.tenant.inventory.entity.JobTrigger;

/**
 * This interface describes all service for Rule Engine
 * 
 * @author Prabhakar Srivastava
 * @Date 05 Oct 2018
 * @version 1.0
 */
@Mapper
public interface JobDetailMapper {

	/**
	 * This method describes creating Job with Job ID NULL
	 * 
	 * @param JobDetail
	 * @Table JOBDETAILS
	 */
	int createJob(JobDetail jobDetail);

	/**
	 * This method describes creating Job Trigger for scheduling Job to run on
	 * schedule time
	 * 
	 * @param JobTrigger
	 * @Table JOBTRIGGERS
	 */
	int createTrigger(JobTrigger jobTrigger);

	/**
	 * This method describes Update Running Job Details Information When Job Run
	 * 
	 * @param JobDetail
	 * @Table JOBDETAILS
	 */
	int updateMetaData(JobDetail jobDetail);

	/**
	 * This method describes Stop Running Job From The End User on demand
	 * 
	 * @param JobDetail
	 * @Table JOBDETAILS
	 */
	JobDetail stopOnDemand();

	/**
	 * This method describes getting all record for Job Details History
	 * 
	 * @Table JOBDETAILS
	 */
	@Select("SELECT COUNT(*) FROM JOBDETAILS WHERE JOBID IS NOT NULL AND ORG_ID=#{orgId} AND JOBNAME=#{jobName}")
	int record(@Param("orgId") String orgId, @Param("jobName") String jobName);

	/**
	 * This method describes getting all Job Details history according to pagination
	 * 
	 * @Table JOBDETAILS
	 */
	List<JobDetail> getAll(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("orgId") String orgId, @Param("jobName") String jobName);

	/**
	 * This method describes get by Job ID
	 * 
	 * @param Job ID
	 * @Table JOBDETAILS
	 */
	JobDetail getByJobId(String jobId);

	/**
	 * This method describes get by Job Name with Last Engine True and Job is not
	 * Running
	 * 
	 * @param Job Name
	 * @Table JOBDETAILS
	 */
	@Select("SELECT * FROM JOBDETAILS WHERE JOBID IS NOT NULL AND JOBRUNSTATE !='RUNNING' AND ORG_ID=#{orgId} AND JOBNAME=#{jobName} ORDER BY STARTEDON DESC FETCH FIRST ROW ONLY")
	JobDetail getByJobName(@Param("jobName") String jobName, @Param("orgId") String orgId);

	/**
	 * This method describes update Job Details By Job ID
	 * 
	 * @param Job ID
	 * @Table JOBDETAILS
	 */
	JobDetail updateByJobId(String job);

	/**
	 * This method describes get Job Run State By Job Status
	 * 
	 * @param Status
	 * @Table JOBDETAILS
	 */
	List<JobDetail> getByState(@Param("jobName") String jobName, @Param("jobRunState") String jobRunState);

	/**
	 * This method describes get Last Engine Run Details with status LASTENGINE_RUN
	 * By Job Name
	 * 
	 * @param Job Name
	 * @Table JOBDETAILS
	 */
	JobDetail getLastEngineDetails(String jobName);

	/**
	 * This method describes get Job Trigger Details By Trigger ID
	 * 
	 * @param ID
	 * @Table JOBTRIGGERS
	 */
	JobTrigger getByTriggerId(int id);

	/**
	 * This method describes validate create Job Details With Job IS NULL
	 * 
	 * @param Job Name
	 * @Table JOBDETAILS
	 */
	@SuppressWarnings("rawtypes")
	List validateJobCreation(@Param("jobName") String jobName);

	/**
	 * This method describes update run state for Job Details
	 * 
	 * @param Job ID, Job Name, Job Run State
	 * @Table JOBDETAILS
	 */
	int updateState(@Param("jobId") String jobId, @Param("jobName") String jobName, @Param("state") String state);

	/**
	 * This method describes validate Trigger Details By Unique Trigger Name
	 * 
	 * @param Trigger Name
	 * @Table JOBTRIGGERS
	 */
	@Select("SELECT COUNT(*) FROM JOBTRIGGERS WHERE TRIGGERNAME=#{triggerName}")
	int validateTrigger(@Param("triggerName") String triggerName);

	/**
	 * This method describes update Trigger Details
	 * 
	 * @param JobTrigger
	 * @Table JOBTRIGGERS
	 */
	int uTrigger(JobTrigger jobTrigger);

	/**
	 * This method describes get Trigger Details By Trigger Name
	 * 
	 * @param Trigger Name
	 * @Table JOBTRIGGERS
	 */
	JobTrigger getByTriggerName(@Param("triggerName") String triggerName, @Param("orgId") String orgId);

	/**
	 * This method describes update Trigger Name
	 * 
	 * @param Job Name, Job Id
	 * @Table JOBDETAILS
	 */
	int updateTriggerName(@Param("jobId") String jobId, @Param("jobName") String jobName,
			@Param("triggerName") String triggerName);

	/**
	 * This method describes getting all filter record for all Job History Details
	 * 
	 * @param Start Date, End Date
	 * @Table JOBDETAILS
	 */
	Integer filterRecord(@Param("from") String from, @Param("to") String to, @Param("orgId") String orgId,
			@Param("jobName") String jobName);

	/**
	 * This method describes getting filter for all Job History Details according to
	 * pagination
	 * 
	 * @param Start Date, End Date
	 * @Table JOBDETAILS
	 */
	List<JobDetail> filter(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("from") String from, @Param("to") String to, @Param("orgId") String orgId,
			@Param("jobName") String jobName);

	/**
	 * This method describes Retrieve last Job Details while run state is not
	 * running
	 * 
	 * @param Job ID
	 * @Table JOBDETAILS
	 */
	@Select("SELECT LASTENGINERUN FROM JOBDETAILS WHERE JOBRUNSTATE!='RUNNING' AND STATUS='LASTENGINE_RUN' AND JOBID=#{jobId}")
	String getLastEngineRun(String jobId);

	/**
	 * This method describes validate Job Details while run state is not running
	 * 
	 * @param Job ID
	 * @Table JOBDETAILS
	 */
	@Select("SELECT COUNT(*) FROM JOBDETAILS WHERE JOBNAME=#{jobName} AND STATUS='LASTENGINE_RUN'")
	int validateJobName(String jobName);

	/**
	 * This method describes validate Job Details while run state is not running
	 * 
	 * @param Job ID
	 * @Table JOBDETAILS
	 */
	@Update("UPDATE JOBDETAILS SET STATUS='PASSED' WHERE JOBNAME=#{jobName} AND JOBID!=#{jobId}")
	int updateStatus(JobDetail jobDetail);

	/**
	 * This method describes update next schedule for Active AWS Job
	 * 
	 * @param Next Schedule, TriggerName
	 * @Table JOBTRIGGERS
	 */
	int updateNextSchedule(@Param("nextSchedule") String nextSchedule, @Param("triggerName") String triggerName);

	/**
	 * This method describes get last succeeded Job Details
	 * 
	 * @param Job Name
	 * @Table JOBDETAILS
	 */
	JobDetail getLastSummary(@Param("jobName") String jobName, @Param("orgId") String orgId);

	/**
	 * This method describes get update previous Job Details SUMMARY FALSE
	 * 
	 * @param JobDetail
	 * @Table JOBDETAILS
	 */
	int updateSummary(JobDetail jobDetail);

	/**
	 * This method describes validate count STATUS to TRUE
	 * 
	 * @param JobDetail
	 * @Table JOBDETAILS
	 */
	@Select("SELECT COUNT(*) FROM JOBDETAILS WHERE JOBNAME=#{jobName} AND SUMMARY='TRUE'")
	int validateSummaryUpdate(JobDetail jobDetail);

	/**
	 * This method describes validate Status LASTENGINE_RUN
	 * 
	 * @param Job ID
	 * @Table JOBDETAILS
	 */
	@Select("SELECT COUNT(*) FROM JOBDETAILS WHERE JOBID=#{jobId} AND STATUS='LASTENGINE_RUN'")
	int validateRow(String jobId);

	/**
	 * This method describes getting all filter record for same date Job Details
	 * History
	 * 
	 * @param End Date
	 * @Table JOBDETAILS
	 */
	Integer filterSameDateRecord(@Param("to") String to, @Param("orgId") String orgId,
			@Param("jobName") String jobName);

	/**
	 * This method describes getting all filter for same date Job Details History
	 * according to pagination
	 * 
	 * @param End Date
	 * @Table JOBDETAILS
	 */
	List<JobDetail> filterSameDate(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("to") String to, @Param("orgId") String orgId, @Param("jobName") String jobName);

	@Select("SELECT REP_DATE as repDate FROM JOBDETAILS WHERE STATUS='LASTENGINE_RUN' ")
	JobDetail getLastRunningDate();

	/**
	 * This method describes getting distinct store count for Succeeded Job Summary
	 * 
	 * @Table REP_ALLOC_OUTPUT
	 */
	@Select("SELECT COUNT (DISTINCT LABEL1)  FROM ${tableName}")
	int globalStoreCount(@Param("tableName") String tableName);

	/**
	 * This method describes getting distinct item count for Succeeded Job Summary
	 * 
	 * @Table REP_ALLOC_OUTPUT
	 */
	@Select("SELECT COUNT (DISTINCT LABEL5)  FROM ${tableName}")
	int globalitemCount(@Param("tableName") String tableName);

	/**
	 * This method describes getting distinct transfer order count for Succeeded Job
	 * Summary
	 * 
	 * @Table REP_ALLOC_OUTPUT
	 */
	@Select("SELECT SUM(LABEL32) FROM ${tableName}")
	String globaltotalCount(@Param("tableName") String tableName);

	/**
	 * This method describes getting update all count for Succeeded Job Summary
	 * 
	 * @param JobDetail
	 * @Table JOBDETAILS
	 */
	int globalupdateCount(JobDetail jobDetail);

	/**
	 * This method describes getting all record for last Succeeded Job Summary
	 * Details
	 * 
	 * @Table REP_ALLOC_OUTPUT
	 */
	@Select("SELECT COUNT(*) FROM ${tableName}")
	int globalrecordRAO(@Param("tableName") String tableName);

	/**
	 * This method describes getting all last Succeeded Job Summary Details
	 * according to pagination
	 * 
	 * @Table REP_ALLOC_OUTPUT
	 */
	List<HeadersSpecificOutput> globalAllRAO(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("tableName") String tableName);

	/**
	 * This method describes getting all filter record for last Succeeded Job
	 * Details Summary
	 * 
	 * <<<<<<< HEAD =======
	 * 
	 * @param label >>>>>>> c7018497311ea866df9919c0f0d6f156e96ff177
	 * @param Store Code
	 * @Table REP_ALLOC_OUTPUT
	 */
	int globalSummaryFilterRecord(@Param("storeCode") String storeCode, @Param("tableName") String tableName);

	/**
	 * This method describes getting all filter for last Succeeded Job Details
	 * Summary according to pagination
	 * 
	 * @param Store Code
	 * @Table REP_ALLOC_OUTPUT
	 */
	List<HeadersSpecificOutput> globalSummaryFilter(@Param("offset") Integer offset,
			@Param("pagesize") Integer pagesize, @Param("storeCode") String storeCode,
			@Param("tableName") String tableName);

	/**
	 * This method describes getting all Store Code last Succeeded Job Details
	 * Summary
	 * 
	 * @Table REP_ALLOC_OUTPUT
	 */
	@Select("SELECT DISTINCT LABEL1  FROM ${tableName}")
	List<String> globalAllStoreCode(@Param("tableName") String tableName);

	/**
	 * This method describes getting all warehouse Code last Succeeded Job Details
	 * Summary
	 * 
	 * @Table REP_ALLOC_OUTPUT
	 */
	@Select("SELECT DISTINCT LABEL51 FROM ${tableName}")
	List<String> getWhCode(@Param("tableName") String tableName);

	/**
	 * This method describes retrieve transfer order data for Excel last Succeeded
	 * Job Details Summary
	 * 
	 * @param warehouse code
	 * @Table REP_ALLOC_OUTPUT
	 */
	List<DownloadRuleEngine> downloadExcelSummary(String whCode);

	List<EnterpriseSpecificOutput> downloadExcelSummaryForSkecters(String whCode);

	List<EnterpriseSpecificOutputNysaa> downloadExcelSummaryForNysaa1();

	List<EnterpriseSpecificOutputNysaa> downloadExcelSummaryForNysaa2();

	int updateFileName(@Param("folderName") String folderName, @Param("arguments") String arguments,
			@Param("jobId") String jobId);

	/**
	 * This method describes update current LASTENGINE to TRUE to current Job ID
	 * 
	 * @param Job Name, Job ID
	 * @Table JOBDETAILS
	 */
	int updateLastEngine(@Param("jobId") String jobId, @Param("jobName") String jobName);

	/**
	 * This method describes update previous LASTENGINE to FALSE to Current Job ID
	 * 
	 * @param Job ID
	 * @Table JOBDETAILS
	 */
	int updatePreviousEngine(@Param("jobId") String jobId);

	/**
	 * This method describes update Transfer Order Bucket Key
	 * 
	 * @param mailCount
	 * @param Job       ID
	 * @Table JOBDETAILS
	 */
	@Update("UPDATE JOBDETAILS SET GENERATED_TO_KEY = #{generated_to_key},MAIL_COUNTER=#{mailCount,jdbcType=INTEGER} WHERE JOBID = #{jobId,jdbcType=VARCHAR}")
	int updateJobGenerateTO(@Param("jobId") String jobId, @Param("generated_to_key") String bucketKey,
			@Param("mailCount") int mailCount);

	/**
	 * This method describes get Previous Job ID
	 * 
	 * @Table JOBDETAILS
	 */
	@Select("SELECT JOBID FROM JOBDETAILS WHERE JOBID IS NOT NULL AND JOBRUNSTATE !='RUNNING' ORDER BY CREATEDON DESC FETCH FIRST ROW ONLY")
	String getPreviousJobId();

	/**
	 * This method describes update previous LASTENGINE to FALSE Except Current Job
	 * ID
	 * 
	 * @param Job ID
	 * @Table JOBDETAILS
	 */
	@Update("UPDATE JOBDETAILS SET LASTENGINE='FALSE' WHERE JOBNAME=#{jobName,jdbcType=VARCHAR} AND JOBID!=#{jobId,jdbcType=VARCHAR}")
	int updatePreviousLastEngine(@Param("jobId") String jobId, @Param("jobName") String jobName);

	/**
	 * This method describes update current LASTENGINE to TRUE By Current Job ID
	 * 
	 * @param Job ID
	 * @Table JOBDETAILS
	 */
	@Update("UPDATE JOBDETAILS SET LASTENGINE='TRUE' WHERE JOBID=#{jobId,jdbcType=VARCHAR}")
	int updateCurrentLastEngine(String jobId);

	/**
	 * This method describes get last 5 Succeeded Job Duration for Job Details Run
	 * On Demand Dialer
	 * 
	 * @param Job ID
	 * @Table JOBDETAILS
	 */
	@Select("SELECT DURATION FROM JOBDETAILS WHERE JOBRUNSTATE='SUCCEEDED' AND DURATION IS NOT NULL ORDER BY CREATEDON DESC FETCH FIRST 5 ROWS ONLY")
	List<String> fiveSucceededTime();

	/**
	 * This method describes update replenishment Date and Duration By Job ID
	 * 
	 * @param Job ID
	 * @Table JOBDETAILS
	 */
	@Update("UPDATE JOBDETAILS SET COMPLETEON=#{endDate,jdbcType=DATE},REP_DATE=#{endDate,jdbcType=DATE},DURATION=#{duration,jdbcType=VARCHAR} WHERE JOBID=#{jobId,jdbcType=VARCHAR}")
	int updateEndTimeWithRepDate(@Param("jobId") String jobId, @Param("endDate") Date endDate,
			@Param("duration") String duration);

	/**
	 * This method describes get fixed header for Transfer Order according to
	 * parameter
	 * 
	 * @Table HEADER_CONFIG_LOG
	 */
	@Select("SELECT CUSTOM_HEADER FROM HEADER_CONFIG_LOG WHERE  IS_DEFAULT=#{isDefault} AND attribute_type=#{attributeType} AND DISPLAY_NAME=#{displayName} AND ORG_ID=#{orgId}")
	String getCustomHeaders(@Param("isDefault") String isDefault, @Param("attributeType") String attributeType,
			@Param("displayName") String displayName, @Param("orgId") String orgId);

	/**
	 * This method describes get user header for Transfer Order according to
	 * parameter
	 * 
	 * @Table HEADER_CONFIG_LOG
	 */
	@Select("SELECT CUSTOM_HEADER FROM HEADER_CONFIG_LOG WHERE IS_DEFAULT='TRUE'")
	String getFromHeaderLogs();

	/**
	 * This method describes retrieve transfer order data for Excel last Succeeded
	 * Job Details Summary
	 * 
	 * @param warehouse code
	 * @Table REP_ALLOC_OUTPUT
	 */
	List<HeadersSpecificOutput> getDownloadExcelSummaryData(@Param("whCode") String whCode,
			@Param("tableName") String tableName);

	@Update("UPDATE JOBDETAILS SET STATUS='LAST_ENGINE_RUN' WHERE CREATEDON=(SELECT CREATEDON FROM JOBDETAILS WHERE JOBID IS NOT NULL ORDER BY CREATEDON DESC FETCH FIRST ROW ONLY)")
	int updateLastEngineRun();

	int updatePreviousSchedule(@Param("previousNextSchedule") String previousNextSchedule,
			@Param("triggerName") String triggerName);

	@Update("UPDATE JOBDETAILS SET IS_SENT_MAIL='TRUE' WHERE JOBID=#{jobId,jdbcType=VARCHAR}")
	void updateMailStatus(@Param("jobId") String jobId);

	@Select("SELECT COUNT(*) FROM ${tableName}")
	int globalrecordReq(@Param("tableName") String tableName);

	List<HeadersSpecificOutput> globalAllReq(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("tableName") String tableName);

	int globalSummaryFilterRecordReq(@Param("storeCode") String storeCode, @Param("tableName") String tableName);

	List<HeadersSpecificOutput> globalSummaryFilterReq(@Param("offset") Integer offset,
			@Param("pagesize") Integer pagesize, @Param("storeCode") String storeCode,
			@Param("tableName") String tableName);

	@Select("SELECT CUSTOM_HEADER FROM HEADER_CONFIG_LOG WHERE STATUS ='ACTIVE' AND ATTRIBUTE_TYPE ='EXCEL_HEADER'")
	String getCustomHeader();

	@Update("UPDATE JOBDETAILS SET ORG_ID=#{orgId,jdbcType=VARCHAR} WHERE JOBID=#{jobId,jdbcType=VARCHAR}")
	int updateOrgId(@Param("orgId") String orgId, @Param("jobId") String jobId);

	@Select("SELECT * FROM JOBTRIGGERS WHERE ORG_ID=#{orgId} AND ENTERPRISEID=#{enterpriseId} AND STATUS='TRUE' AND ACTIVE='1' ORDER BY CREATEDON DESC")
	List<JobTrigger> getFiveTriggers(@Param("orgId") String orgId, @Param("enterpriseId") int enterpriseId);

	@Select("SELECT COUNT(*) FROM JOBTRIGGERS WHERE ORG_ID=#{orgId} AND ENTERPRISEID=#{enterpriseId} AND STATUS='TRUE' AND ACTIVE='1'")
	int countTriggerTest(@Param("orgId") String orgId, @Param("enterpriseId") int enterpriseId);

	@Update("UPDATE JOBTRIGGERS SET STATUS='FALSE',ACTIVE='0' WHERE TRIGGERNAME=#{triggerName} AND ORG_ID=#{orgId}")
	int deleteTrigger(@Param("triggerName") String triggerName, @Param("orgId") String orgId);

	@Select("SELECT TRIGGERNAME FROM JOBTRIGGERS WHERE ORG_ID=#{orgId} AND ENTERPRISEID=#{enterpriseId} AND STATUS='TRUE' AND ACTIVE='1'")
	List<String> getPresentTriggers(@Param("orgId") String orgId, @Param("enterpriseId") int enterpriseId);

	@Select("SELECT SCHEDULE FROM JOBTRIGGERS WHERE ORG_ID=#{orgId} AND ENTERPRISEID=#{enterpriseId} ORDER BY CREATEDON DESC FETCH FIRST ROW ONLY")
	Date getPastSchedultTime(@Param("orgId") String orgId, @Param("enterpriseId") int enterpriseId);

	@Select("SELECT JOBNAME  FROM IP_AUTOCONFIG WHERE EID=#{enterpriseId}")
	List<String> getAllJobNameFromOrg(@Param("enterpriseId") int enterpriseId);

	@Update("UPDATE JOBDETAILS SET MAIL_COUNTER=#{mailCount,jdbcType=INTEGER}, IS_SENT_MAIL= 'TRUE' WHERE JOBID = #{jobId,jdbcType=VARCHAR} AND ORG_ID = #{orgId,jdbcType=VARCHAR}")
	int updateMailCounter(@Param("jobId") String jobId, @Param("mailCount") int mailCount,
			@Param("orgId") String orgId);

	JobDetail getJobDetailByJobId(@Param("jobId") String jobId,@Param("orgId") String orgId);

}
