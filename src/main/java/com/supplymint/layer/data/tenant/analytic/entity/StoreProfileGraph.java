package com.supplymint.layer.data.tenant.analytic.entity;

public class StoreProfileGraph {
	private String storeCode;
	private String billDate;
	private int totalQty;
	private int totalCostPrice;
	private int totalSellPrice;
	private int profit;
	private int areaPersqft;

	public StoreProfileGraph() {

	}

	public StoreProfileGraph(String storeCode, String billDate, int totalQty, int totalCostPrice, int totalSellPrice,
			int profit, int areaPersqft) {
		super();
		this.storeCode = storeCode;
		this.billDate = billDate;
		this.totalQty = totalQty;
		this.totalCostPrice = totalCostPrice;
		this.totalSellPrice = totalSellPrice;
		this.profit = profit;
		this.areaPersqft = areaPersqft;
	}

	public String getStoreCode() {
		return storeCode;
	}

	public void setStoreCode(String storeCode) {
		this.storeCode = storeCode;
	}

	public String getBillDate() {
		return billDate;
	}

	public void setBillDate(String billDate) {
		this.billDate = billDate;
	}

	public int getTotalQty() {
		return totalQty;
	}

	public void setTotalQty(int totalQty) {
		this.totalQty = totalQty;
	}

	public int getTotalCostPrice() {
		return totalCostPrice;
	}

	public void setTotalCostPrice(int totalCostPrice) {
		this.totalCostPrice = totalCostPrice;
	}

	public int getTotalSellPrice() {
		return totalSellPrice;
	}

	public void setTotalSellPrice(int totalSellPrice) {
		this.totalSellPrice = totalSellPrice;
	}

	public int getProfit() {
		return profit;
	}

	public void setProfit(int profit) {
		this.profit = profit;
	}

	public int getAreaPersqft() {
		return areaPersqft;
	}

	public void setAreaPersqft(int areaPersqft) {
		this.areaPersqft = areaPersqft;
	}

	@Override
	public String toString() {
		return "StoreProfileGraph [storeCode=" + storeCode + ", billDate=" + billDate + ", totalQty=" + totalQty
				+ ", totalCostPrice=" + totalCostPrice + ", totalSellPrice=" + totalSellPrice + ", profit=" + profit
				+ ", areaPersqft=" + areaPersqft + "]";
	}

}
