package com.supplymint.layer.data.tenant.demand.entity;

import java.time.OffsetDateTime;

public class DemandForecastScheduler {

	private int id;
	private String enterpriseId;
	private String enterpriseUserName;
	private String coreHashKey;
	private String tenantHashKey;
	private String frequency;
	private String type;
	private String period;
	private String startDayOfMonth;
	private String startDayOfWeek;
	private String bucketName;
	private String bucketKey;
	private String filePath;
	private String active;
	private String status;
	private String ipAddress;
	private String createdBy;
	private OffsetDateTime createdOn;
	private String updatedBy;
	private OffsetDateTime updatedOn;
	private String additional;
	private String schedule;
	private String nextSchedule;

	public DemandForecastScheduler() {

	}

	public DemandForecastScheduler(int id, String enterpriseId, String enterpriseUserName, String coreHashKey,
			String tenantHashKey, String frequency, String type, String period, String startDayOfMonth,
			String startDayOfWeek, String bucketName, String bucketKey, String filePath, String active, String status,
			String ipAddress, String createdBy, OffsetDateTime createdOn, String updatedBy, OffsetDateTime updatedOn,
			String additional, String schedule, String nextSchedule) {
		super();
		this.id = id;
		this.enterpriseId = enterpriseId;
		this.enterpriseUserName = enterpriseUserName;
		this.coreHashKey = coreHashKey;
		this.tenantHashKey = tenantHashKey;
		this.frequency = frequency;
		this.type = type;
		this.period = period;
		this.startDayOfMonth = startDayOfMonth;
		this.startDayOfWeek = startDayOfWeek;
		this.bucketName = bucketName;
		this.bucketKey = bucketKey;
		this.filePath = filePath;
		this.active = active;
		this.status = status;
		this.ipAddress = ipAddress;
		this.createdBy = createdBy;
		this.createdOn = createdOn;
		this.updatedBy = updatedBy;
		this.updatedOn = updatedOn;
		this.additional = additional;
		this.schedule = schedule;
		this.nextSchedule = nextSchedule;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEnterpriseId() {
		return enterpriseId;
	}

	public void setEnterpriseId(String enterpriseId) {
		this.enterpriseId = enterpriseId;
	}

	public String getEnterpriseUserName() {
		return enterpriseUserName;
	}

	public void setEnterpriseUserName(String enterpriseUserName) {
		this.enterpriseUserName = enterpriseUserName;
	}

	public String getCoreHashKey() {
		return coreHashKey;
	}

	public void setCoreHashKey(String coreHashKey) {
		this.coreHashKey = coreHashKey;
	}

	public String getTenantHashKey() {
		return tenantHashKey;
	}

	public void setTenantHashKey(String tenantHashKey) {
		this.tenantHashKey = tenantHashKey;
	}

	public String getFrequency() {
		return frequency;
	}

	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public String getStartDayOfMonth() {
		return startDayOfMonth;
	}

	public void setStartDayOfMonth(String startDayOfMonth) {
		this.startDayOfMonth = startDayOfMonth;
	}

	public String getStartDayOfWeek() {
		return startDayOfWeek;
	}

	public void setStartDayOfWeek(String startDayOfWeek) {
		this.startDayOfWeek = startDayOfWeek;
	}

	public String getBucketName() {
		return bucketName;
	}

	public void setBucketName(String bucketName) {
		this.bucketName = bucketName;
	}

	public String getBucketKey() {
		return bucketKey;
	}

	public void setBucketKey(String bucketKey) {
		this.bucketKey = bucketKey;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public OffsetDateTime getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(OffsetDateTime createdOn) {
		this.createdOn = createdOn;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public OffsetDateTime getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(OffsetDateTime updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getAdditional() {
		return additional;
	}

	public void setAdditional(String additional) {
		this.additional = additional;
	}

	public String getSchedule() {
		return schedule;
	}

	public void setSchedule(String schedule) {
		this.schedule = schedule;
	}

	public String getNextSchedule() {
		return nextSchedule;
	}

	public void setNextSchedule(String nextSchedule) {
		this.nextSchedule = nextSchedule;
	}

	@Override
	public String toString() {
		return "ForecastScheduler [id=" + id + ", enterpriseId=" + enterpriseId + ", enterpriseUserName="
				+ enterpriseUserName + ", coreHashKey=" + coreHashKey + ", tenantHashKey=" + tenantHashKey
				+ ", frequency=" + frequency + ", type=" + type + ", period=" + period + ", startDayOfMonth="
				+ startDayOfMonth + ", startDayOfWeek=" + startDayOfWeek + ", bucketName=" + bucketName + ", bucketKey="
				+ bucketKey + ", filePath=" + filePath + ", active=" + active + ", status=" + status + ", ipAddress="
				+ ipAddress + ", createdBy=" + createdBy + ", createdOn=" + createdOn + ", updatedBy=" + updatedBy
				+ ", updatedOn=" + updatedOn + ", additional=" + additional + ", schedule=" + schedule
				+ ", nextSchedule=" + nextSchedule + "]";
	}

}
