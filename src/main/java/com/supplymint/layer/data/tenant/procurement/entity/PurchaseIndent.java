package com.supplymint.layer.data.tenant.procurement.entity;

import java.time.OffsetDateTime;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.supplymint.util.PILocalDatetimeSerializer;

public class PurchaseIndent {

	private int id;
	private String orgId;
	private String hl1Code;
	private String hl2Code;
	private String hl3Code;

	private String hl1Name;
	private String hl2Name;
	private String hl3Name;
	private String slCode;
	private String slName;
	private String slCityName;
	private String slAddr;
	private double leadTime;
	private String termCode;
	private String termName;
	private String transporterCode;
	private String transporterName;
	private double poQuantity;
	private double poAmount;
	private List<PurchaseIndentDetail> piDetails;
	private String active;
	private String status;
	private String ipAddress;
	private String createdBy;
	private String pattern;
	private String path;
	private String stateCode;
	private String isUDFExist;
	private String isSiteExist;
	private String siteCode;
	private String siteName;

	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	private OffsetDateTime createdTime;
	private String updatedBy;

	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	private OffsetDateTime updationTime;
	private String additional;

	public PurchaseIndent() {
		super();
	}

	public PurchaseIndent(double poQuantity, double poAmount, String pattern) {
		super();
		this.poQuantity = poQuantity;
		this.poAmount = poAmount;
		this.pattern = pattern;
	}

	public PurchaseIndent(int id, String hl1Code, String hl2Code, String hl3Code, String hl1Name, String hl2Name,
			String hl3Name, String slCode, String slName, String slCityName, String slAddr, double leadTime,
			String termCode, String termName, String transporterCode, String transporterName, double poQuantity,
			double poAmount, List<PurchaseIndentDetail> piDetails, String active, String status, String ipAddress,
			String createdBy, String pattern, String path, OffsetDateTime createdTime, String updatedBy,
			OffsetDateTime updationTime, String additional,String orgId) {
		super();
		this.id = id;
		this.orgId = orgId;
		this.hl1Code = hl1Code;
		this.hl2Code = hl2Code;
		this.hl3Code = hl3Code;
		this.hl1Name = hl1Name;
		this.hl2Name = hl2Name;
		this.hl3Name = hl3Name;
		this.slCode = slCode;
		this.slName = slName;
		this.slCityName = slCityName;
		this.slAddr = slAddr;
		this.leadTime = leadTime;
		this.termCode = termCode;
		this.termName = termName;
		this.transporterCode = transporterCode;
		this.transporterName = transporterName;
		this.poQuantity = poQuantity;
		this.poAmount = poAmount;
		this.piDetails = piDetails;
		this.active = active;
		this.status = status;
		this.ipAddress = ipAddress;
		this.createdBy = createdBy;
		this.pattern = pattern;
		this.path = path;
		this.createdTime = createdTime;
		this.updatedBy = updatedBy;
		this.updationTime = updationTime;
		this.additional = additional;
	}
	
	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getIsSiteExist() {
		return isSiteExist;
	}

	public void setIsSiteExist(String isSiteExist) {
		this.isSiteExist = isSiteExist;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getHl1Code() {
		return hl1Code;
	}

	public void setHl1Code(String hl1Code) {
		this.hl1Code = hl1Code;
	}

	public String getHl2Code() {
		return hl2Code;
	}

	public void setHl2Code(String hl2Code) {
		this.hl2Code = hl2Code;
	}

	public String getHl3Code() {
		return hl3Code;
	}

	public void setHl3Code(String hl3Code) {
		this.hl3Code = hl3Code;
	}

	public String getHl1Name() {
		return hl1Name;
	}

	public void setHl1Name(String hl1Name) {
		this.hl1Name = hl1Name;
	}

	public String getHl2Name() {
		return hl2Name;
	}

	public void setHl2Name(String hl2Name) {
		this.hl2Name = hl2Name;
	}

	public String getHl3Name() {
		return hl3Name;
	}

	public void setHl3Name(String hl3Name) {
		this.hl3Name = hl3Name;
	}

	public String getSlCode() {
		return slCode;
	}

	public void setSlCode(String slCode) {
		this.slCode = slCode;
	}

	public String getSlName() {
		return slName;
	}

	public void setSlName(String slName) {
		this.slName = slName;
	}

	public String getSlCityName() {
		return slCityName;
	}

	public void setSlCityName(String slCityName) {
		this.slCityName = slCityName;
	}

	public String getSlAddr() {
		return slAddr;
	}

	public void setSlAddr(String slAddr) {
		this.slAddr = slAddr;
	}

	public double getLeadTime() {
		return leadTime;
	}

	public void setLeadTime(double leadTime) {
		this.leadTime = leadTime;
	}

	public String getTermCode() {
		return termCode;
	}

	public void setTermCode(String termCode) {
		this.termCode = termCode;
	}

	public String getTermName() {
		return termName;
	}

	public void setTermName(String termName) {
		this.termName = termName;
	}

	public String getTransporterCode() {
		return transporterCode;
	}

	public void setTransporterCode(String transporterCode) {
		this.transporterCode = transporterCode;
	}

	public String getTransporterName() {
		return transporterName;
	}

	public void setTransporterName(String transporterName) {
		this.transporterName = transporterName;
	}

	public double getPoQuantity() {
		return poQuantity;
	}

	public void setPoQuantity(double poQuantity) {
		this.poQuantity = poQuantity;
	}

	public double getPoAmount() {
		return poAmount;
	}

	public void setPoAmount(double poAmount) {
		this.poAmount = poAmount;
	}

	public List<PurchaseIndentDetail> getPiDetails() {
		return piDetails;
	}

	public void setPiDetails(List<PurchaseIndentDetail> piDetails) {
		this.piDetails = piDetails;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public OffsetDateTime getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(OffsetDateTime createdTime) {
		this.createdTime = createdTime;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public OffsetDateTime getUpdationTime() {
		return updationTime;
	}

	public void setUpdationTime(OffsetDateTime updationTime) {
		this.updationTime = updationTime;
	}

	public String getAdditional() {
		return additional;
	}

	public void setAdditional(String additional) {
		this.additional = additional;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public String getIsUDFExist() {
		return isUDFExist;
	}

	public void setIsUDFExist(String isUDFExist) {
		this.isUDFExist = isUDFExist;
	}

	public String getSiteCode() {
		return siteCode;
	}

	public void setSiteCode(String siteCode) {
		this.siteCode = siteCode;
	}

	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	@Override
	public String toString() {
		return "PurchaseIndent [id=" + id + ", orgId=" + orgId + ", hl1Code=" + hl1Code + ", hl2Code=" + hl2Code
				+ ", hl3Code=" + hl3Code + ", hl1Name=" + hl1Name + ", hl2Name=" + hl2Name + ", hl3Name=" + hl3Name
				+ ", slCode=" + slCode + ", slName=" + slName + ", slCityName=" + slCityName + ", slAddr=" + slAddr
				+ ", leadTime=" + leadTime + ", termCode=" + termCode + ", termName=" + termName + ", transporterCode="
				+ transporterCode + ", transporterName=" + transporterName + ", poQuantity=" + poQuantity
				+ ", poAmount=" + poAmount + ", piDetails=" + piDetails + ", active=" + active + ", status=" + status
				+ ", ipAddress=" + ipAddress + ", createdBy=" + createdBy + ", pattern=" + pattern + ", path=" + path
				+ ", stateCode=" + stateCode + ", isUDFExist=" + isUDFExist + ", isSiteExist=" + isSiteExist
				+ ", siteCode=" + siteCode + ", siteName=" + siteName + ", createdTime=" + createdTime + ", updatedBy="
				+ updatedBy + ", updationTime=" + updationTime + ", additional=" + additional + "]";
	}

}
