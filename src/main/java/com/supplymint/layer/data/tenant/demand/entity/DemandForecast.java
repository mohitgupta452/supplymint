package com.supplymint.layer.data.tenant.demand.entity;

public class DemandForecast {
	private String assortmentCode;
	private String qty;
	private String billDate;
	private String qtyForecast;
	private String flag;
	private String siteCode;
	private double costPrice;
	private double sellValue;
	private String billdateFrom;
	private String billdateTo;
	private String quarter;
	private double openPurchaseOrder;
	private double closingInventory;
	private double openingStock;

	public DemandForecast() {

	}

	public DemandForecast(String assortmentCode, String qty, String billDate, String qtyForecast, String flag,
			String siteCode, double costPrice, double sellValue, String billdateFrom, String billdateTo, String quarter,
			double openPurchaseOrder, double closingInventory, double openingStock) {
		super();
		this.assortmentCode = assortmentCode;
		this.qty = qty;
		this.billDate = billDate;
		this.qtyForecast = qtyForecast;
		this.flag = flag;
		this.siteCode = siteCode;
		this.costPrice = costPrice;
		this.sellValue = sellValue;
		this.billdateFrom = billdateFrom;
		this.billdateTo = billdateTo;
		this.quarter = quarter;
		this.openPurchaseOrder = openPurchaseOrder;
		this.closingInventory = closingInventory;
		this.openingStock = openingStock;
	}

	public String getAssortmentCode() {
		return assortmentCode;
	}

	public void setAssortmentCode(String assortmentCode) {
		this.assortmentCode = assortmentCode;
	}

	public String getQty() {
		return qty;
	}

	public void setQty(String qty) {
		this.qty = qty;
	}

	public String getBillDate() {
		return billDate;
	}

	public void setBillDate(String billDate) {
		this.billDate = billDate;
	}

	public String getQtyForecast() {
		return qtyForecast;
	}

	public void setQtyForecast(String qtyForecast) {
		this.qtyForecast = qtyForecast;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getSiteCode() {
		return siteCode;
	}

	public void setSiteCode(String siteCode) {
		this.siteCode = siteCode;
	}

	public double getCostPrice() {
		return costPrice;
	}

	public void setCostPrice(double costPrice) {
		this.costPrice = costPrice;
	}

	public double getSellValue() {
		return sellValue;
	}

	public void setSellValue(double sellValue) {
		this.sellValue = sellValue;
	}

	public String getBilldateFrom() {
		return billdateFrom;
	}

	public void setBilldateFrom(String billdateFrom) {
		this.billdateFrom = billdateFrom;
	}

	public String getBilldateTo() {
		return billdateTo;
	}

	public void setBilldateTo(String billdateTo) {
		this.billdateTo = billdateTo;
	}

	public String getQuarter() {
		return quarter;
	}

	public void setQuarter(String quarter) {
		this.quarter = quarter;
	}

	public double getOpenPurchaseOrder() {
		return openPurchaseOrder;
	}

	public void setOpenPurchaseOrder(double openPurchaseOrder) {
		this.openPurchaseOrder = openPurchaseOrder;
	}

	public double getClosingInventory() {
		return closingInventory;
	}

	public void setClosingInventory(double closingInventory) {
		this.closingInventory = closingInventory;
	}

	public double getOpeningStock() {
		return openingStock;
	}

	public void setOpeningStock(double openingStock) {
		this.openingStock = openingStock;
	}

	@Override
	public String toString() {
		return "DemandForecast [assortmentCode=" + assortmentCode + ", qty=" + qty + ", billDate=" + billDate
				+ ", qtyForecast=" + qtyForecast + ", flag=" + flag + ", siteCode=" + siteCode + ", costPrice="
				+ costPrice + ", sellValue=" + sellValue + ", billdateFrom=" + billdateFrom + ", billdateTo="
				+ billdateTo + ", quarter=" + quarter + ", openPurchaseOrder=" + openPurchaseOrder
				+ ", closingInventory=" + closingInventory + ", openingStock=" + openingStock + "]";
	}

}
