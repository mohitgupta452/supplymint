package com.supplymint.layer.data.tenant.demand.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.mapping.StatementType;

import com.supplymint.layer.data.tenant.administration.entity.ADMItem;
import com.supplymint.layer.data.tenant.demand.entity.Assortment;
import com.supplymint.layer.data.tenant.demand.entity.AssortmentViewLog;
import com.supplymint.layer.data.tenant.demand.entity.MViewRefreshHistory;

/**
 * This interface will act as Mapper for SQL queries
 * @author Prabhakar Srivastava
 * @Date 15 Dec 2018
 * @version 2.0
 */
@Mapper
public interface AssortmentMapper {
	
	/**
	 * This method describes getting all record of Division 
	 * @Table INVITEM
	 */
	@Select("SELECT COUNT(DISTINCT HL1NAME) FROM INVITEM")
	int getHl1Record();

	/**
	 * This method describes getting all Division according to pagination 
	 * @Table INVITEM
	 */
	List<ADMItem> getHL1Name(@Param("offset")int offset,@Param("pageSize")int pageSize);
	
	/**
	 * This method describes getting all search record for Division
	 * @param Search
	 * @Table INVITEM
	 */
	int getHl1SearchRecord(@Param("search") String search);
	
	/**
	 * This method describes getting all search for Division according to pagination 
	 * @Table INVITEM
	 */
	List<ADMItem> getHL1NameSearch(@Param("offset")int offset,@Param("pageSize")int pageSize,@Param("search")String search);

	/**
	 * This method describes getting all record of Section
	 * @param Division 
	 * @Table INVITEM
	 */
	int getHl2Record(@Param("hl1Name") String hl1Name);
	
	/**
	 * This method describes getting all Section according to pagination
	 * @param Division 
	 * @Table INVITEM
	 */
	List<ADMItem> getHL2Name(@Param("offset")int offset,@Param("pageSize")int pageSize,@Param("hl1Name") String hl1Name);
	
	/**
	 * This method describes getting all search record for Section 
	 * @param Division 
	 * @Table INVITEM
	 */
	int getHl2SearchRecord(@Param("hl1Name") String hl1Name,@Param("search") String search);
	
	/**
	 * This method describes getting all search for Section according to pagination 
	 * @param Division
	 * @Table INVITEM
	 */
	List<ADMItem> getHl2Search(@Param("offset")int offset,@Param("pageSize")int pageSize,@Param("hl1Name")String hl1Name,@Param("search")String search);
	
	/**
	 * This method describes getting all record of Department
	 * @param Division, Section
	 * @Table INVITEM
	 */
	int getHl3Record(@Param("hl1Name") String hl1Name, @Param("hl2Name") String hl2Name);

	/**
	 * This method describes getting all Department according to pagination
	 * @param Division, Section
	 * @Table INVITEM
	 */
	List<ADMItem> getHL3Name(@Param("offset")int offset,@Param("pageSize")int pageSize,@Param("hl1Name") String hl1Name, @Param("hl2Name") String hl2Name);
	
	/**
	 * This method describes getting all search record for Department 
	 * @param Division, Section
	 * @Table INVITEM
	 */
	int getHl3SearchRecord(@Param("hl1Name") String hl1Name,@Param("hl2Name") String hl2Name,@Param("search") String search);
	
	/**
	 * This method describes getting all search for Department according to pagination 
	 * @param Division, Section
	 * @Table INVITEM
	 */
	List<ADMItem> getHl3Search(@Param("offset")int offset,@Param("pageSize")int pageSize,@Param("hl1Name")String hl1Name,@Param("hl2Name")String hl2Name,@Param("search")String search);
	
	/**
	 * This method describes getting all record of Article
	 * @param Division, Section, Department
	 * @Table INVITEM
	 */
	int getHl4Record(Assortment container);

	/**
	 * This method describes getting all Article according to pagination
	 * @param Division, Section, Department
	 * @Table INVITEM
	 */
	List<ADMItem> getHL4Name(Assortment container);
	
	/**
	 * This method describes getting all search record for Article 
	 * @param Division, Section, Department
	 * @Table INVITEM
	 */
	int getHl4SearchRecord(Assortment container);
	
	/**
	 * This method describes getting all search for Article according to pagination 
	 * @param Division, Section, Department
	 * @Table INVITEM
	 */
	List<ADMItem> getHl4Search(Assortment container);

	/**
	 * This method describes getting all Item count according to parameter
	 * @param Division, Section, Department, Article
	 * @Table INVITEM
	 */
	int getMRP(Assortment container);
	
	/**
	 * This method describes delete assortmentCode according to matching ICODE
	 * @param Division, Section, Department, Article
	 * @Table ASSORTMENT, INVITEM
	 */
	int deleteAssortmentCode(Assortment container);

	/**
	 * This method describes create assortment according to user
	 * @param Division, Section, Department, Article, USER ASSORTMENTCODE
	 * @Table ASSORTMENT
	 */
	int createAssortmentCode(Assortment container);

	/**
	 * This method describes getting all record of Final Assortment
	 * @Table FINAL_ASSORTMENT
	 */
	@Select("SELECT COUNT(*) FROM FINAL_ASSORTMENT")
	int assortmentRecord();

	/**
	 * This method describes getting all Final Assortment according to pagination 
	 * @Table FINAL_ASSORTMENT
	 */
	List<Assortment> getAllAssortment(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize);

	/**
	 * This method describes getting all search record for Final Assortment 
	 * @param Search
	 * @Table FINAL_ASSORTMENT
	 */
	int searchAssortmentRecord(@Param("search") String search);

	/**
	 * This method describes getting all search for Final Assortment 
	 * according to pagination 
	 * @param Search
	 * @Table FINAL_ASSORTMENT
	 */
	List<Assortment> searchAssortment(@Param("offset") Integer offset, @Param("pageSize") Integer pageSize,
			@Param("search") String search);
	
	/**
	 * This method describes create Assortment Activity log according to user
	 * @param Division, Section, Department, Article, AssortmentCode ,etc.
	 * @Table ASSORTMENT_VIEWLOG
	 */
	int createLog(AssortmentViewLog viewLog);

	/**
	 * This method describes getting all record of Assortment Activity log
	 * @Table ASSORTMENT_VIEWLOG
	 */
	@Select("SELECT COUNT(*) FROM ASSORTMENT_VIEWLOG")
	int getAllHistotyCount();

	/**
	 * This method describes getting all Assortment Activity log
	 * according to pagination 
	 * @Table ASSORTMENT_VIEWLOG
	 */
	List<AssortmentViewLog> getAllHistoty(Assortment container);

	/**
	 * This method describes getting all filter record of Assortment Activity log
	 * @param itemCount, startDate, endDate, createdOn, status 
	 * @Table ASSORTMENT_VIEWLOG
	 */
	int filterHistoryRecord(Assortment container);

	/**
	 * This method describes getting all filter for Assortment Activity log 
	 * according to pagination
	 * @param itemCount, startDate, endDate, createdOn, status 
	 * @Table ASSORTMENT_VIEWLOG
	 */
	List<AssortmentViewLog> filterHistory(Assortment container);

	/**
	 * This method describes getting all search record for Assortment Activity log 
	 * @param Search
	 * @Table ASSORTMENT_VIEWLOG
	 */
	int searchHistoryRecord(Assortment container);

	/**
	 * This method describes getting all search for Assortment Activity log 
	 * according to pagination 
	 * @param Search
	 * @Table ASSORTMENT_VIEWLOG
	 */
	List<AssortmentViewLog> searchHistory(Assortment container);

	/**
	 * This method describes checking condition for existence of MV 
	 * according to parameter
	 * @param Module, Sub Module
	 * @Table MVIEW_REFRESH_HISTORY
	 */
	@Select("SELECT COUNT(*) FROM MVIEW_REFRESH_HISTORY WHERE MODULE=#{module} AND SUBMODULE=#{subModule}")
	int isExistenceMViewWithModule(@Param("module")String module,@Param("subModule")String subModule);
	
	/**
	 * This method describes checking creating MV History according to parameter
	 * @param MViewRefreshHistory Object
	 * @Table MVIEW_REFRESH_HISTORY
	 */
	int createMViewRefresh(MViewRefreshHistory mvRefresh);
	
	/**
	 * This method describes refreshing all MV after creating Assortment 
	 * @MaterlizedView DEFAULT_ASSORTMENT, FINAL_ASSORTMENT,
	 * DEMAND_ACTUAL_MONTHLY, DEMAND_ACTUAL_WEEKLY
	 */
	@Select(value= "CALL DBMS_MVIEW.REFRESH('DEFAULT_ASSORTMENT,FINAL_ASSORTMENT,DEMAND_ACTUAL_MONTHLY,DEMAND_ACTUAL_WEEKLY','?')")
	@Options(statementType = StatementType.CALLABLE,fetchSize=1000)
	void refreshAssortmentMV();
	
	/**
	 * This method describes update materialized refresh status
	 * @param MViewRefreshHistory Object
	 * @Table MVIEW_REFRESH_HISTORY
	 */
	int updateMViewRefresh(MViewRefreshHistory mvRefresh);
	
	/**
	 * This method describes getting default assortment according to parameter
	 * @param defaultAssortment key
	 * @Table SYSTEM_DEFAULT_CONFIG
	 */
	@Select("SELECT VALUE FROM SYSTEM_DEFAULT_CONFIG WHERE KEY=#{defaultAssortment}")
	String getDefaultAssortment(String defaultAssortment);
	
	/**
	 * This method describes getting MV refresh status
	 * @Table MVIEW_REFRESH_HISTORY
	 */
	@Select("SELECT STATUS FROM MVIEW_REFRESH_HISTORY WHERE MODULE='INVENTRY_PLANNING' AND SUBMODULE='ASSORTMENT'")
	String getMVStatus();
	
	/**
	 * This method describes update MV status viewed
	 * @Table MVIEW_REFRESH_HISTORY
	 */
	int viewedAssortmentMessage(@Param("module")String module,@Param("subModule")String subModule);
	
	/**
	 * This method describes get fetching data status
	 * @Table DEMAND_PLANNING
	 */
	@Select("SELECT DB_READ_STATUS FROM DEMAND_PLANNING WHERE STATUS='Processing'")
	String getAssortmentStatus();
	
	/**
	 * This method describes update forecast status succeeded
	 * @Table MVIEW_REFRESH_HISTORY
	 */
	@Update("UPDATE MVIEW_REFRESH_HISTORY SET STATUS='SUCCEEDED',ACTIVE='1' WHERE MODULE=#{module} AND SUBMODULE=#{subModule}")
	int updateMVForecastStatus(@Param("module")String module,@Param("subModule")String subModule);
	
	/**
	 * This method describes get forecast read status
	 * @Table MVIEW_REFRESH_HISTORY
	 */
	@Select("SELECT FORECAST_READ_STATUS FROM MVIEW_REFRESH_HISTORY WHERE MODULE='INVENTRY_PLANNING' AND SUBMODULE='ASSORTMENT'")
	String forecastReadSatus();
	
	/**
	 * This method describes drop view for Branded enterprise
	 * @View BRANDED_ASSORTMENT_MONTHLY
	 */
	@Delete("DROP VIEW BRANDED_ASSORTMENT_MONTHLY")
	void dropMainHierarchyViewForForecastMonthly();
	
	/**
	 * This method describes drop view for Branded enterprise
	 * @View BRANDED_ASSORTMENT_WEEKLY
	 */
	@Delete("DROP VIEW BRANDED_ASSORTMENT_WEEKLY")
	void dropMainHierarchyViewForForecastWeekly();
	
	/**
	 * This method describes drop view for Branded enterprise
	 * @View DEMAND_FORECAST_MONTHLY_FINAL
	 */
	@Delete("DROP VIEW DEMAND_FORECAST_MONTHLY_FINAL")
	void dropMainHierarchyViewForForecastMonthlyFinal();
	
	/**
	 * This method describes drop view for Branded enterprise
	 * @View DEMAND_FORECAST_WEEKLY_FINAL
	 */
	@Delete("DROP VIEW DEMAND_FORECAST_WEEKLY_FINAL")
	void dropMainHierarchyViewForForecastWeeklyFinal();
	
	/**
	 * This method describes create view for Branded enterprise
	 * @View BRANDED_ASSORTMENT_MONTHLY
	 */
	void createMainHierarchyViewForForecastMonthly(@Param("mainCategory")String mainCategory,@Param("billDateMonthly")String billDateMonthly);
	
	/**
	 * This method describes create view for Branded enterprise
	 * @View BRANDED_ASSORTMENT_WEEKLY
	 */
	void createMainHierarchyViewForForecastWeekly(@Param("mainCategory")String mainCategory,@Param("billDateWeekly")String billDateWeekly);
	
	/**
	 * This method describes create view for Branded enterprise
	 * @View DEMAND_FORECAST_MONTHLY_FINAL
	 */
	void createMainHierarchyViewForForecastMonthlyFinal(@Param("assortmentHierarchy")String assortmentHierarchy,@Param("forecastMainHierarchy")String forecastMainHierarchy,@Param("invSubHierarchy")String invSubHierarchy,@Param("desSubHierarchy")String desSubHierarchy);
	
	/**
	 * This method describes create view for Branded enterprise
	 * @View DEMAND_FORECAST_WEEKLY_FINAL
	 */
	void createMainHierarchyViewForForecastWeeklyFinal(@Param("assortmentHierarchy")String assortmentHierarchy,@Param("forecastMainHierarchy")String forecastMainHierarchy,@Param("invSubHierarchy")String invSubHierarchy,@Param("desSubHierarchy")String desSubHierarchy);
	
	/**
	 * This method describes getting Bill date Format
	 * @Table INVSTOCK_COGS
	 */
	@Select("SELECT BILLDATE FROM INVSTOCK_COGS FETCH FIRST ROWS ONLY")
	String getBillDateFormat();

}
