package com.supplymint.layer.data.tenant.demand.entity;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.supplymint.util.DateFormatSerializer;

public class OpenToBuy {

	private int id;
	private String planId;
	private String planName;
	private String planType;
	private String isUfAsd;
	private String isUfDfd;
	private String increasePercentageAsd;
	private String increasePercentageDfd;
	private String activeDataOption;
	private String status;
	private char active;
	private String ipAddress;
	private String createdBy;
	@JsonSerialize(using = DateFormatSerializer.class)
	private Date createdOn;
	private String updatedBy;
	@JsonSerialize(using = DateFormatSerializer.class)
	private Date updatedOn;
	private String additional;
	private String makeCopy;
	private String isUserOTBPlan;
	private String isCopied;

	public OpenToBuy() {

	}

	public OpenToBuy(int id, String planId, String planName, String planType, String isUfAsd, String isUfDfd,
			String increasePercentageAsd, String increasePercentageDfd, String activeDataOption, String status,
			char active, String ipAddress, String createdBy, Date createdOn, String updatedBy, Date updatedOn,
			String additional, String makeCopy, String isUserOTBPlan, String isCopied) {
		super();
		this.id = id;
		this.planId = planId;
		this.planName = planName;
		this.planType = planType;
		this.isUfAsd = isUfAsd;
		this.isUfDfd = isUfDfd;
		this.increasePercentageAsd = increasePercentageAsd;
		this.increasePercentageDfd = increasePercentageDfd;
		this.activeDataOption = activeDataOption;
		this.status = status;
		this.active = active;
		this.ipAddress = ipAddress;
		this.createdBy = createdBy;
		this.createdOn = createdOn;
		this.updatedBy = updatedBy;
		this.updatedOn = updatedOn;
		this.additional = additional;
		this.makeCopy = makeCopy;
		this.isUserOTBPlan = isUserOTBPlan;
		this.isCopied = isCopied;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public String getPlanType() {
		return planType;
	}

	public void setPlanType(String planType) {
		this.planType = planType;
	}

	public String getIsUfAsd() {
		return isUfAsd;
	}

	public void setIsUfAsd(String isUfAsd) {
		this.isUfAsd = isUfAsd;
	}

	public String getIsUfDfd() {
		return isUfDfd;
	}

	public void setIsUfDfd(String isUfDfd) {
		this.isUfDfd = isUfDfd;
	}

	public String getIncreasePercentageAsd() {
		return increasePercentageAsd;
	}

	public void setIncreasePercentageAsd(String increasePercentageAsd) {
		this.increasePercentageAsd = increasePercentageAsd;
	}

	public String getIncreasePercentageDfd() {
		return increasePercentageDfd;
	}

	public void setIncreasePercentageDfd(String increasePercentageDfd) {
		this.increasePercentageDfd = increasePercentageDfd;
	}

	public String getActiveDataOption() {
		return activeDataOption;
	}

	public void setActiveDataOption(String activeDataOption) {
		this.activeDataOption = activeDataOption;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public char getActive() {
		return active;
	}

	public void setActive(char active) {
		this.active = active;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getAdditional() {
		return additional;
	}

	public void setAdditional(String additional) {
		this.additional = additional;
	}

	public String getMakeCopy() {
		return makeCopy;
	}

	public void setMakeCopy(String makeCopy) {
		this.makeCopy = makeCopy;
	}

	public String getIsUserOTBPlan() {
		return isUserOTBPlan;
	}

	public void setIsUserOTBPlan(String isUserOTBPlan) {
		this.isUserOTBPlan = isUserOTBPlan;
	}

	public String getIsCopied() {
		return isCopied;
	}

	public void setIsCopied(String isCopied) {
		this.isCopied = isCopied;
	}

	@Override
	public String toString() {
		return "OpenToBuy [id=" + id + ", planId=" + planId + ", planName=" + planName + ", planType=" + planType
				+ ", isUfAsd=" + isUfAsd + ", isUfDfd=" + isUfDfd + ", increasePercentageAsd=" + increasePercentageAsd
				+ ", increasePercentageDfd=" + increasePercentageDfd + ", activeDataOption=" + activeDataOption
				+ ", status=" + status + ", active=" + active + ", ipAddress=" + ipAddress + ", createdBy=" + createdBy
				+ ", createdOn=" + createdOn + ", updatedBy=" + updatedBy + ", updatedOn=" + updatedOn + ", additional="
				+ additional + ", makeCopy=" + makeCopy + ", isUserOTBPlan=" + isUserOTBPlan + ", isCopied=" + isCopied
				+ "]";
	}

}
