package com.supplymint.layer.data.tenant.inventory.entity;

public class RepAllocOutput {

	private String storeCode;
	private String availableStock;
	private String itemCode;
	private String transferOrder;
	private String str;
	private String whCode;
	private String uniqueCode;

	// Skechters
	private String STORE_CODE;
	private String ARTICLE_NAME;
	private String STORE_RANK;
	private String BARCODE;
	private String FINAL_REQUIREMENT;
	private String ALLOCATED_QTY;
	private String STORE_NAME;

	public RepAllocOutput() {

	}

	public RepAllocOutput(String storeCode, String availableStock, String itemCode, String transferOrder, String str,
			String whCode, String uniqueCode, String sTORE_CODE, String aRTICLE_NAME, String sTORE_RANK, String bARCODE,
			String fINAL_REQUIREMENT, String aLLOCATED_QTY, String sTORE_NAME) {
		super();
		this.storeCode = storeCode;
		this.availableStock = availableStock;
		this.itemCode = itemCode;
		this.transferOrder = transferOrder;
		this.str = str;
		this.whCode = whCode;
		this.uniqueCode = uniqueCode;
		STORE_CODE = sTORE_CODE;
		ARTICLE_NAME = aRTICLE_NAME;
		STORE_RANK = sTORE_RANK;
		BARCODE = bARCODE;
		FINAL_REQUIREMENT = fINAL_REQUIREMENT;
		ALLOCATED_QTY = aLLOCATED_QTY;
		STORE_NAME = sTORE_NAME;
	}

	public RepAllocOutput(String whCode) {
		super();
		this.whCode = whCode;
	}

	public String getStoreCode() {
		return storeCode;
	}

	public void setStoreCode(String storeCode) {
		this.storeCode = storeCode;
	}

	public String getAvailableStock() {
		return availableStock;
	}

	public void setAvailableStock(String availableStock) {
		this.availableStock = availableStock;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getTransferOrder() {
		return transferOrder;
	}

	public void setTransferOrder(String transferOrder) {
		this.transferOrder = transferOrder;
	}

	public String getStr() {
		return str;
	}

	public void setStr(String str) {
		this.str = str;
	}

	public String getWhCode() {
		return whCode;
	}

	public void setWhCode(String whCode) {
		this.whCode = whCode;
	}

	public String getUniqueCode() {
		return uniqueCode;
	}

	public void setUniqueCode(String uniqueCode) {
		this.uniqueCode = uniqueCode;
	}

	public String getSTORE_CODE() {
		return STORE_CODE;
	}

	public void setSTORE_CODE(String sTORE_CODE) {
		STORE_CODE = sTORE_CODE;
	}

	public String getARTICLE_NAME() {
		return ARTICLE_NAME;
	}

	public void setARTICLE_NAME(String aRTICLE_NAME) {
		ARTICLE_NAME = aRTICLE_NAME;
	}

	public String getSTORE_RANK() {
		return STORE_RANK;
	}

	public void setSTORE_RANK(String sTORE_RANK) {
		STORE_RANK = sTORE_RANK;
	}

	public String getBARCODE() {
		return BARCODE;
	}

	public void setBARCODE(String bARCODE) {
		BARCODE = bARCODE;
	}

	public String getFINAL_REQUIREMENT() {
		return FINAL_REQUIREMENT;
	}

	public void setFINAL_REQUIREMENT(String fINAL_REQUIREMENT) {
		FINAL_REQUIREMENT = fINAL_REQUIREMENT;
	}

	public String getALLOCATED_QTY() {
		return ALLOCATED_QTY;
	}

	public void setALLOCATED_QTY(String aLLOCATED_QTY) {
		ALLOCATED_QTY = aLLOCATED_QTY;
	}

	public String getSTORE_NAME() {
		return STORE_NAME;
	}

	public void setSTORE_NAME(String sTORE_NAME) {
		STORE_NAME = sTORE_NAME;
	}

	@Override
	public String toString() {
		return "RepAllocOutput [storeCode=" + storeCode + ", availableStock=" + availableStock + ", itemCode="
				+ itemCode + ", transferOrder=" + transferOrder + ", str=" + str + ", whCode=" + whCode
				+ ", uniqueCode=" + uniqueCode + "]";
	}

}
