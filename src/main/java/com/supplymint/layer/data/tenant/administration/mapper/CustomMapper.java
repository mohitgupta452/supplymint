package com.supplymint.layer.data.tenant.administration.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.supplymint.layer.data.tenant.administration.entity.CustomDataTemplate;
import com.supplymint.layer.data.tenant.administration.entity.CustomFileUpload;
import com.supplymint.layer.data.tenant.administration.entity.CustomMaster;
import com.supplymint.layer.data.tenant.administration.entity.DataReference;
import com.supplymint.layer.data.tenant.administration.entity.EventMaster;
import com.supplymint.layer.data.tenant.administration.entity.FMCGStores;

@Mapper
public interface CustomMapper {

	int createCustomFileUpload(CustomFileUpload customFileUpload);

	@Select("SELECT COUNT(*) FROM CUSTOMFILEUPLOAD")
	int getAllCustomRecord();

	List<CustomFileUpload> getAllCustomFileUpload(@Param("offset") int offset, @Param("pageSize") int pageSize);

//	@Select("SELECT * FROM CUSTOM_SITE_MASTER WHERE PARTNER_NAME=#{partner} AND CHANNEL=#{channel}")
	List<CustomMaster> getAllZoneByChannel(@Param("partner") String partner, @Param("channel") String channel);

	List<CustomMaster> getAllGradeByZone(@Param("partner") String partner, @Param("channel") String channel,
			@Param("zone") String zone);

	List<CustomMaster> getAllStoreCode(@Param("partner") String partner, @Param("channel") String channel,
			@Param("zone") String zone, @Param("grade") String grade);

	List<CustomDataTemplate> getAllTemplate(@Param("channel") String channel);

	List<DataReference> getAllDataReference(@Param("offset") Integer offset, @Param("pagesize") Integer pageSize,
			@Param("channel") String channel);

	@Select("SELECT COUNT(*) FROM DATA_REFERENCE  WHERE KEY LIKE '%' || #{channel} ||'%'")
	int refRecordCount(@Param("channel") String channel);

	int searchDataReferenceRecord(@Param("name") String name, @Param("key") String key,
			@Param("eventStart") String eventStart, @Param("eventEnd") String eventEnd, @Param("status") String status,
			@Param("dataCount") Integer dataCount, @Param("channel") String channel);

	List<DataReference> searchDataReference(@Param("pageSize") Integer pageSize, @Param("offset") Integer offset,
			@Param("name") String name, @Param("key") String key, @Param("eventStart") String eventStart,
			@Param("eventEnd") String eventEnd, @Param("status") String status, @Param("dataCount") Integer dataCount,
			@Param("channel") String channel);

	List<DataReference> getAllRecords();

	List<DataReference> searchAllDataReference(@Param("offset") Integer offset, @Param("pagesize") Integer pageSize,
			@Param("search") String search, @Param("channel") String channel);

	Integer searchAllRecordDataReference(@Param("search") String search, @Param("channel") String channel);

	List<CustomMaster> getAllPartnerByChannel(@Param("channel") String channel);

	List<DataReference> getTop5DataReference();

	List<CustomMaster> filterStoreCode(@Param("channel") String channel, @Param("partner") String partner,
			@Param("zone") String zone, @Param("grade") String grade);

	@Select("SELECT SETUP_VALUE FROM GENERAL_SETTING WHERE ACTIVE='1' AND STATUS='TRUE' AND SETTING='DATE_FORMAT'")
	String getuserDateFormat();

	@Select("SELECT VALUE FROM SYSTEM_DEFAULT_CONFIG WHERE KEY=#{key}")
	String getCustomByKey(@Param("key") String key);

	List<DataReference> getAllDataReferenceScheduler();

	int createEventMaster(EventMaster eventMaster);

	List<EventMaster> searchAllCustomMasterData(@Param("offset") int offset, @Param("pageSize") int pageSize,
			@Param("search") String search, @Param("orgId") String orgId);

	int searchCountCustomMasterData(@Param("search") String search, @Param("orgId") String orgId);

	List<EventMaster> getFilterCustomEventMaster(@Param("pageSize") int pageSize, @Param("offset") int offset,
			@Param("eventName") String eventName, @Param("startDate") String startDate,
			@Param("endDate") String endDate, @Param("stockDays") String stockDays,
			@Param("multiplierROS") String multiplierROS, @Param("partner") String partner,
			@Param("grade") String grade, @Param("storeCode") String storeCode, @Param("zone") String zone,
			@Param("orgId") String orgId);

	int filterCountEventMasterRecord(@Param("eventName") String eventName, @Param("startDate") String startDate,
			@Param("endDate") String endDate, @Param("stockDays") String stockDays,
			@Param("multiplierROS") String multiplierROS, @Param("partner") String partner,
			@Param("grade") String grade, @Param("storeCode") String storeCode, @Param("zone") String zone,
			@Param("orgId") String orgId);

	List<EventMaster> getAllEventMasterData(@Param("offset") int offset, @Param("pageSize") int pageSize,
			@Param("orgId") String orgId);

	int getAllCountEventMaster(@Param("orgId") String orgId);

	int updateEventMaster(EventMaster eventMaster);

	@Select("SELECT COUNT(*) FROM CUSTOM_EVENT_MASTER WHERE LOWER(EVENT_NAME)=LOWER(#{eventName}) AND ORG_ID=#{orgId} AND STATUS='TRUE' AND ACTIVE='1'")
	int isValidEventName(@Param("eventName") String eventName, @Param("orgId") String orgId);

	@Update("UPDATE CUSTOM_EVENT_MASTER SET STATUS='FALSE', ACTIVE='0' WHERE EVENT_NAME=#{eventName} AND ORG_ID=#{orgId}")
	int deleteEventMaster(@Param("eventName") String eventName, @Param("orgId") String orgId);

	@Select("SELECT COUNT(*) FROM CUSTOM_EVENT_MASTER WHERE EVENT_NAME=#{eventName}")
	int validateEventName(@Param("eventName") String eventName);

	int getMotherCompRecord();

	List<String> getAllMotherComp(@Param("offset") int offset, @Param("pageSize") int pageSize);

	List<FMCGStores> getAllArticle(@Param("offset") int offset, @Param("pageSize") int pageSize,
			@Param("motherComp") List<String> motherComp, @Param("motherCompStr") String motherCompStr);

	int getAllArticleRecords(@Param("motherComp") List<String> motherComp,
			@Param("motherCompStr") String motherCompStr);

	int getAllArticleRecord();

	List<FMCGStores> getAllVendor(@Param("offset") int offset, @Param("pageSize") int pageSize,
			@Param("motherComp") List<String> motherComp, @Param("motherCompStr") String motherCompStr,
			@Param("articleCode") List<String> articleCode, @Param("articleCodeStr") String articleCodeStr);

	int getAllVendorRecord(@Param("motherComp") List<String> motherComp, @Param("motherCompStr") String motherCompStr,
			@Param("articleCode") List<String> articleCode, @Param("articleCodeStr") String articleCodeStr);

	int getAllVendorRecords();

	List<FMCGStores> getAllStore(@Param("offset") int offset, @Param("pageSize") int pageSize,
			@Param("motherComp") List<String> motherComp, @Param("motherCompStr") String motherCompStr,
			@Param("articleCode") List<String> articleCode, @Param("articleCodeStr") String articleCodeStr,
			@Param("vendorCode") List<String> vendorCode, @Param("vendorCodeStr") String vendorCodeStr);

	int getAllStoreRecord(@Param("motherComp") List<String> motherComp, @Param("motherCompStr") String motherCompStr,
			@Param("articleCode") List<String> articleCode, @Param("articleCodeStr") String articleCodeStr,
			@Param("vendorCode") List<String> vendorCode, @Param("vendorCodeStr") String vendorCodeStr);

	int getAllStoreRecords();

	List<FMCGStores> getAllStores(@Param("motherComp") List<String> motherComp,
			@Param("motherCompStr") String motherCompStr, @Param("articleCode") List<String> articleCode,
			@Param("articleCodeStr") String articleCodeStr, @Param("vendorCode") List<String> vendorCode,
			@Param("vendorCodeStr") String vendorCodeStr);

	int searchMotherCompRecord(@Param("search") String search);

	List<String> searchMotherComp(@Param("offset") int offset, @Param("pageSize") int pageSize,
			@Param("search") String search);

	List<FMCGStores> searchArticle(@Param("offset") int offset, @Param("pageSize") int pageSize,
			@Param("motherComp") List<String> motherComp, @Param("motherCompStr") String motherCompStr,
			@Param("search") String search);

	int searchArticleRecords(@Param("motherComp") List<String> motherComp, @Param("motherCompStr") String motherCompStr,
			@Param("search") String search);

	List<FMCGStores> searchVendor(@Param("offset") int offset, @Param("pageSize") int pageSize,
			@Param("motherComp") List<String> motherComp, @Param("motherCompStr") String motherCompStr,
			@Param("articleCode") List<String> articleCode, @Param("articleCodeStr") String articleCodeStr,
			@Param("search") String search);

	int searchVendorRecord(@Param("motherComp") List<String> motherComp, @Param("motherCompStr") String motherCompStr,
			@Param("articleCode") List<String> articleCode, @Param("articleCodeStr") String articleCodeStr,
			@Param("search") String search);

	List<FMCGStores> searchStore(@Param("offset") int offset, @Param("pageSize") int pageSize,
			@Param("motherComp") List<String> motherComp, @Param("motherCompStr") String motherCompStr,
			@Param("articleCode") List<String> articleCode, @Param("articleCodeStr") String articleCodeStr,
			@Param("vendorCode") List<String> vendorCode, @Param("vendorCodeStr") String vendorCodeStr,
			@Param("search") String search);

	int searchStoreRecord(@Param("motherComp") List<String> motherComp, @Param("motherCompStr") String motherCompStr,
			@Param("articleCode") List<String> articleCode, @Param("articleCodeStr") String articleCodeStr,
			@Param("vendorCode") List<String> vendorCode, @Param("vendorCodeStr") String vendorCodeStr,
			@Param("search") String search);

}
