package com.supplymint.layer.data.tenant.procurement.entity;

import java.time.OffsetDateTime;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.supplymint.util.LocalDatetimeDeserializer;
import com.supplymint.util.PILocalDatetimeSerializer;

public class PurchaseOrderConfiguration {

	private int id;
	private String url;
	private String headers;

	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	private OffsetDateTime creationTime;

	public PurchaseOrderConfiguration() {
		super();
	}

	public PurchaseOrderConfiguration(int id, String url, String headers, OffsetDateTime creationTime) {
		super();
		this.id = id;
		this.url = url;
		this.headers = headers;
		this.creationTime = creationTime;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getHeaders() {
		return headers;
	}

	public void setHeaders(String headers) {
		this.headers = headers;
	}

	public OffsetDateTime getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(OffsetDateTime creationTime) {
		this.creationTime = creationTime;
	}

	@Override
	public String toString() {
		return "PurchaseOrderConfiguration [id=" + id + ", url=" + url + ", headers=" + headers + ", creationTime="
				+ creationTime + "]";
	}

}
