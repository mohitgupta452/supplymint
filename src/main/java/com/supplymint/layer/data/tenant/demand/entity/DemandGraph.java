package com.supplymint.layer.data.tenant.demand.entity;

public class DemandGraph {
	private int actual;
	private int predicted;
	private int budgeted;
	private String billDate;
	private int deviation;

	public DemandGraph() {

	}

	public DemandGraph(int actual, int predicted, int budgeted, String billDate, int deviation) {
		super();
		this.actual = actual;
		this.predicted = predicted;
		this.budgeted = budgeted;
		this.billDate = billDate;
		this.deviation = deviation;
	}

	public int getActual() {
		return actual;
	}

	public void setActual(int actual) {
		this.actual = actual;
	}

	public int getPredicted() {
		return predicted;
	}

	public void setPredicted(int predicted) {
		this.predicted = predicted;
	}

	public int getBudgeted() {
		return budgeted;
	}

	public void setBudgeted(int budgeted) {
		this.budgeted = budgeted;
	}

	public String getBillDate() {
		return billDate;
	}

	public void setBillDate(String billDate) {
		this.billDate = billDate;
	}

	public int getDeviation() {
		return deviation;
	}

	public void setDeviation(int deviation) {
		this.deviation = deviation;
	}

	@Override
	public String toString() {
		return "DemandGraph [actual=" + actual + ", predicted=" + predicted + ", budgeted=" + budgeted + ", billDate="
				+ billDate + ", deviation=" + deviation + "]";
	}

}
