package com.supplymint.layer.data.tenant.procurement.entity;

import java.time.OffsetDateTime;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.supplymint.util.LocalDateTimeSerializer;
import com.supplymint.util.LocalDatetimeDeserializer;
import com.supplymint.util.PILocalDatetimeSerializer;

public class PurchaseOrderTemp {

	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	public OffsetDateTime orderDate;

	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	public OffsetDateTime validFrom;

	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	public OffsetDateTime validTo;

	public int articleId;
	public int vendorId;
	public int transporterId;
	public int termCode;
	public double mrp;
	public int userId;
	public String orderNo;
	public String poRemarks;

	public String supplierName;
	public String supplierAddress;
	public String gstNo;
	public String stateCode;

	private String active;
	private String status;
	private String ipAddress;
	private String createdBy;

	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	private OffsetDateTime createdTime;
	private String updatedBy;

	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	private OffsetDateTime updationTime;
	private String additional;

	public List<PurchaseOrderLineItem> pol;

	public PurchaseOrderTemp() {
		super();
	}

	public PurchaseOrderTemp(OffsetDateTime orderDate, OffsetDateTime validFrom, OffsetDateTime validTo, int articleId,
			int vendorId, int transporterId, int termCode, double mrp, int userId, String orderNo, String poRemarks,
			String active, String status, String ipAddress, String createdBy, OffsetDateTime createdTime,
			String updatedBy, OffsetDateTime updationTime, String additional, List<PurchaseOrderLineItem> pol) {
		super();
		this.orderDate = orderDate;
		this.validFrom = validFrom;
		this.validTo = validTo;
		this.articleId = articleId;
		this.vendorId = vendorId;
		this.transporterId = transporterId;
		this.termCode = termCode;
		this.mrp = mrp;
		this.userId = userId;
		this.orderNo = orderNo;
		this.poRemarks = poRemarks;
		this.active = active;
		this.status = status;
		this.ipAddress = ipAddress;
		this.createdBy = createdBy;
		this.createdTime = createdTime;
		this.updatedBy = updatedBy;
		this.updationTime = updationTime;
		this.additional = additional;
		this.pol = pol;
	}

	public PurchaseOrderTemp(OffsetDateTime orderDate, OffsetDateTime validFrom, OffsetDateTime validTo, int articleId,
			int vendorId, int transporterId, int termCode, double mrp, List<PurchaseOrderLineItem> pol) {
		super();
		this.orderDate = orderDate;
		this.validFrom = validFrom;
		this.validTo = validTo;
		this.articleId = articleId;
		this.vendorId = vendorId;
		this.transporterId = transporterId;
		this.termCode = termCode;
		this.mrp = mrp;
		this.pol = pol;
	}

	public OffsetDateTime getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(OffsetDateTime orderDate) {
		this.orderDate = orderDate;
	}

	public OffsetDateTime getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(OffsetDateTime validFrom) {
		this.validFrom = validFrom;
	}

	public OffsetDateTime getValidTo() {
		return validTo;
	}

	public void setValidTo(OffsetDateTime validTo) {
		this.validTo = validTo;
	}

	public int getArticleId() {
		return articleId;
	}

	public void setArticleId(int articleId) {
		this.articleId = articleId;
	}

	public int getVendorId() {
		return vendorId;
	}

	public void setVendorId(int vendorId) {
		this.vendorId = vendorId;
	}

	public int getTransporterId() {
		return transporterId;
	}

	public void setTransporterId(int transporterId) {
		this.transporterId = transporterId;
	}

	public int getTermCode() {
		return termCode;
	}

	public void setTermCode(int termCode) {
		this.termCode = termCode;
	}

	public double getMrp() {
		return mrp;
	}

	public void setMrp(double mrp) {
		this.mrp = mrp;
	}

	public List<PurchaseOrderLineItem> getPol() {
		return pol;
	}

	public void setPol(List<PurchaseOrderLineItem> pol) {
		this.pol = pol;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getPoRemarks() {
		return poRemarks;
	}

	public void setPoRemarks(String poRemarks) {
		this.poRemarks = poRemarks;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public OffsetDateTime getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(OffsetDateTime createdTime) {
		this.createdTime = createdTime;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public OffsetDateTime getUpdationTime() {
		return updationTime;
	}

	public void setUpdationTime(OffsetDateTime updationTime) {
		this.updationTime = updationTime;
	}

	public String getAdditional() {
		return additional;
	}

	public void setAdditional(String additional) {
		this.additional = additional;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public String getSupplierAddress() {
		return supplierAddress;
	}

	public void setSupplierAddress(String supplierAddress) {
		this.supplierAddress = supplierAddress;
	}

	public String getGstNo() {
		return gstNo;
	}

	public void setGstNo(String gstNo) {
		this.gstNo = gstNo;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	@Override
	public String toString() {
		return "PurchaseOrderTemp [orderDate=" + orderDate + ", validFrom=" + validFrom + ", validTo=" + validTo
				+ ", articleId=" + articleId + ", vendorId=" + vendorId + ", transporterId=" + transporterId
				+ ", termCode=" + termCode + ", mrp=" + mrp + ", userId=" + userId + ", orderNo=" + orderNo
				+ ", poRemarks=" + poRemarks + ", supplierName=" + supplierName + ", supplierAddress=" + supplierAddress
				+ ", gstNo=" + gstNo + ", stateCode=" + stateCode + ", active=" + active + ", status=" + status
				+ ", ipAddress=" + ipAddress + ", createdBy=" + createdBy + ", createdTime=" + createdTime
				+ ", updatedBy=" + updatedBy + ", updationTime=" + updationTime + ", additional=" + additional
				+ ", pol=" + pol + "]";
	}

}
