package com.supplymint.layer.data.tenant.demand.entity;

public class AdHocItemRequest {

	private String itemCode;
	private String quantity;
	private String status;
	private String itemStatus;

	public AdHocItemRequest() {

	}

	public AdHocItemRequest(String itemCode, String quantity, String status, String itemStatus) {
		super();
		this.itemCode = itemCode;
		this.quantity = quantity;
		this.status = status;
		this.itemStatus = itemStatus;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getItemStatus() {
		return itemStatus;
	}

	public void setItemStatus(String itemStatus) {
		this.itemStatus = itemStatus;
	}

	@Override
	public String toString() {
		return "AdHocItemRequest [itemCode=" + itemCode + ", quantity=" + quantity + ", status=" + status
				+ ", itemStatus=" + itemStatus + "]";
	}

}
