package com.supplymint.layer.data.tenant.entity.procurement.config;

import java.time.OffsetDateTime;

public class DeptItemUDFMappings {
	private int id;
	private String hl3Name;
	private String cat_desc_udf;
	private String displayName;
	private String code;
	private String description;
	private String map;
	private OffsetDateTime createdAt;
	private OffsetDateTime updatedAt;
	private String createdBy;
	private String updatedBy;
	private String ipaddress;
	private String ext;
	private String hl3code;
	private String hl4code;
	private String hl4Name;

	public DeptItemUDFMappings() {
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getHl3Name() {
		return hl3Name;
	}

	public void setHl3Name(String hl3Name) {
		this.hl3Name = hl3Name;
	}

	public String getCat_desc_udf() {
		return cat_desc_udf;
	}

	public void setCat_desc_udf(String cat_desc_udf) {
		this.cat_desc_udf = cat_desc_udf;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getMap() {
		return map;
	}

	public void setMap(String map) {
		this.map = map;
	}

	public OffsetDateTime getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(OffsetDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public OffsetDateTime getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(OffsetDateTime updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getIpaddress() {
		return ipaddress;
	}

	public void setIpaddress(String ipaddress) {
		this.ipaddress = ipaddress;
	}

	public String getExt() {
		return ext;
	}

	public void setExt(String ext) {
		this.ext = ext;
	}

	public String getHl3code() {
		return hl3code;
	}

	public void setHl3code(String hl3code) {
		this.hl3code = hl3code;
	}

	public String getHl4code() {
		return hl4code;
	}

	public void setHl4code(String hl4code) {
		this.hl4code = hl4code;
	}

	public String getHl4Name() {
		return hl4Name;
	}

	public void setHl4Name(String hl4Name) {
		this.hl4Name = hl4Name;
	}

	@Override
	public String toString() {
		return "DeptItemUDFMappings [id=" + id + ", hl3Name=" + hl3Name + ", cat_desc_udf=" + cat_desc_udf
				+ ", displayName=" + displayName + ", code=" + code + ", description=" + description + ", map=" + map
				+ ", createdAt=" + createdAt + ", updatedAt=" + updatedAt + ", createdBy=" + createdBy + ", updatedBy="
				+ updatedBy + ", ipaddress=" + ipaddress + ", ext=" + ext + ", hl3code=" + hl3code + ", hl4code="
				+ hl4code + ", hl4Name=" + hl4Name + "]";
	}

}
