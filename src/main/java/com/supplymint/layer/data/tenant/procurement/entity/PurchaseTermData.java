package com.supplymint.layer.data.tenant.procurement.entity;

public class PurchaseTermData {

	private Integer slCode;
	private String code;
	private String name;

	public PurchaseTermData() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PurchaseTermData(Integer slCode, String code, String name) {
		super();
		this.slCode = slCode;
		this.code = code;
		this.name = name;
	}

	public Integer getSlCode() {
		return slCode;
	}

	public void setSlCode(Integer slCode) {
		this.slCode = slCode;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public PurchaseTermData(String code, String name) {
		super();
		this.code = code;
		this.name = name;
	}

	@Override
	public String toString() {
		return "PurchaseTermData [slCode=" + slCode + ", code=" + code + ", name=" + name + "]";
	}

}
