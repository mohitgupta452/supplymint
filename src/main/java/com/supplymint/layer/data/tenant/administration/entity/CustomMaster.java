package com.supplymint.layer.data.tenant.administration.entity;

public class CustomMaster {

	private int id;
	private String channel;
	private String storeCode;
	private String orgCode;
	private String storeName;
	private String zone;
	private String storeGrade;
	private String storeRank;
	private String partner;

	public CustomMaster() {

	}

	public CustomMaster(int id, String channel, String storeCode, String orgCode, String storeName, String zone,
			String storeGrade, String storeRank, String partner) {
		super();
		this.id = id;
		this.channel = channel;
		this.storeCode = storeCode;
		this.orgCode = orgCode;
		this.storeName = storeName;
		this.zone = zone;
		this.storeGrade = storeGrade;
		this.storeRank = storeRank;
		this.partner = partner;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getStoreCode() {
		return storeCode;
	}

	public void setStoreCode(String storeCode) {
		this.storeCode = storeCode;
	}

	public String getOrgCode() {
		return orgCode;
	}

	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

	public String getStoreGrade() {
		return storeGrade;
	}

	public void setStoreGrade(String storeGrade) {
		this.storeGrade = storeGrade;
	}

	public String getStoreRank() {
		return storeRank;
	}

	public void setStoreRank(String storeRank) {
		this.storeRank = storeRank;
	}

	public String getPartner() {
		return partner;
	}

	public void setPartner(String partner) {
		this.partner = partner;
	}

	@Override
	public String toString() {
		return "CustomMaster [id=" + id + ", channel=" + channel + ", storeCode=" + storeCode + ", orgCode=" + orgCode
				+ ", storeName=" + storeName + ", zone=" + zone + ", storeGrade=" + storeGrade + ", storeRank="
				+ storeRank + ", partner=" + partner + "]";
	}

}
