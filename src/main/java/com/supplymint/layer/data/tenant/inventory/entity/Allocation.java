package com.supplymint.layer.data.tenant.inventory.entity;

import java.time.OffsetDateTime;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.supplymint.util.LocalDateTimeSerializer;
import com.supplymint.util.LocalDatetimeDeserializer;

public class Allocation {
	private int id;
	private String season;
	private String month;
	private String basis;
	private String status;
	private char active;
	private String ipAddress;
	private String createdBy;
	
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	private OffsetDateTime createdTime;
	private String updatedBy;
	
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	private OffsetDateTime updationTime;
	private String additional;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public String getBasis() {
		return basis;
	}
	public void setBasis(String basis) {
		this.basis = basis;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public char getActive() {
		return active;
	}
	public void setActive(char active) {
		this.active = active;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public OffsetDateTime getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(OffsetDateTime createdTime) {
		this.createdTime = createdTime;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public OffsetDateTime getUpdationTime() {
		return updationTime;
	}
	public void setUpdationTime(OffsetDateTime updationTime) {
		this.updationTime = updationTime;
	}
	public String getAdditional() {
		return additional;
	}
	public void setAdditional(String additional) {
		this.additional = additional;
	}
	
	
	
	public Allocation(int id, String season, String month, String basis, String status, char active, String ipAddress,
			String createdBy, OffsetDateTime createdTime, String updatedBy, OffsetDateTime updationTime,
			String additional) {
		super();
		this.id = id;
		this.season = season;
		this.month = month;
		this.basis = basis;
		this.status = status;
		this.active = active;
		this.ipAddress = ipAddress;
		this.createdBy = createdBy;
		this.createdTime = createdTime;
		this.updatedBy = updatedBy;
		this.updationTime = updationTime;
		this.additional = additional;
	}
	public Allocation() {
	
	}
	
	@Override
	public String toString() {
		return "Allocation [id=" + id + ", season=" + season + ", month=" + month + ", basis=" + basis + ", status="
				+ status + ", active=" + active + ", ipAddress=" + ipAddress + ", createdBy=" + createdBy
				+ ", createdTime=" + createdTime + ", updatedBy=" + updatedBy + ", updationTime=" + updationTime
				+ ", additional=" + additional + "]";
	}
	
	
}
