package com.supplymint.layer.data.tenant.procurement.entity;

import java.time.OffsetDateTime;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.supplymint.util.LocalDateTimeSerializer;
import com.supplymint.util.LocalDatetimeDeserializer;

public class Vendor {

	private int id;
	private String organisationName;
	private String vendorName;
	private String vendorCode;
	private String description;
	private char active;
	private String country;
	private String state;
	private String city;
	private String zipCode;
	private String url;
	private String notes;
	private String fax;
	private String phone;
	private String email;
	private String contactName;
	private String contactPhone;
	private String contactEmail;
	private String contactAddress;
	private String shippingCNTName;
	private String shippingPhone;
	private String shippingEmail;
	private String shippingCNTAddress;
	private String purchasingAddress;
	private String paymentTerms;
	private String orderLeadTimeDays;
	private String status;
	private String ipAddress;
	private String createdBy;
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	private OffsetDateTime createdTime;
	private String updatedBy;
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	private OffsetDateTime updationTime;
	private String additional;

	public Vendor() {

	}

	public Vendor(int id, String organisationName, String vendorName, String vendorCode, String description,
			char active, String country, String state, String city, String zipCode, String url, String notes,
			String fax, String phone, String email, String contactName, String contactPhone, String contactEmail,
			String contactAddress, String shippingCNTName, String shippingPhone, String shippingEmail,
			String shippingCNTAddress, String purchasingAddress, String paymentTerms, String orderLeadTimeDays,
			String status, String ipAddress, String createdBy, OffsetDateTime createdTime, String updatedBy,
			OffsetDateTime updationTime, String additional) {
		super();
		this.id = id;
		this.organisationName = organisationName;
		this.vendorName = vendorName;
		this.vendorCode = vendorCode;
		this.description = description;
		this.active = active;
		this.country = country;
		this.state = state;
		this.city = city;
		this.zipCode = zipCode;
		this.url = url;
		this.notes = notes;
		this.fax = fax;
		this.phone = phone;
		this.email = email;
		this.contactName = contactName;
		this.contactPhone = contactPhone;
		this.contactEmail = contactEmail;
		this.contactAddress = contactAddress;
		this.shippingCNTName = shippingCNTName;
		this.shippingPhone = shippingPhone;
		this.shippingEmail = shippingEmail;
		this.shippingCNTAddress = shippingCNTAddress;
		this.purchasingAddress = purchasingAddress;
		this.paymentTerms = paymentTerms;
		this.orderLeadTimeDays = orderLeadTimeDays;
		this.status = status;
		this.ipAddress = ipAddress;
		this.createdBy = createdBy;
		this.createdTime = createdTime;
		this.updatedBy = updatedBy;
		this.updationTime = updationTime;
		this.additional = additional;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getOrganisationName() {
		return organisationName;
	}

	public void setOrganisationName(String organisationName) {
		this.organisationName = organisationName;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public String getVendorCode() {
		return vendorCode;
	}

	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public char getActive() {
		return active;
	}

	public void setActive(char active) {
		this.active = active;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getContactPhone() {
		return contactPhone;
	}

	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public String getContactAddress() {
		return contactAddress;
	}

	public void setContactAddress(String contactAddress) {
		this.contactAddress = contactAddress;
	}

	public String getShippingCNTName() {
		return shippingCNTName;
	}

	public void setShippingCNTName(String shippingCNTName) {
		this.shippingCNTName = shippingCNTName;
	}

	public String getShippingPhone() {
		return shippingPhone;
	}

	public void setShippingPhone(String shippingPhone) {
		this.shippingPhone = shippingPhone;
	}

	public String getShippingEmail() {
		return shippingEmail;
	}

	public void setShippingEmail(String shippingEmail) {
		this.shippingEmail = shippingEmail;
	}

	public String getShippingCNTAddress() {
		return shippingCNTAddress;
	}

	public void setShippingCNTAddress(String shippingCNTAddress) {
		this.shippingCNTAddress = shippingCNTAddress;
	}

	public String getPurchasingAddress() {
		return purchasingAddress;
	}

	public void setPurchasingAddress(String purchasingAddress) {
		this.purchasingAddress = purchasingAddress;
	}

	public String getPaymentTerms() {
		return paymentTerms;
	}

	public void setPaymentTerms(String paymentTerms) {
		this.paymentTerms = paymentTerms;
	}

	public String getOrderLeadTimeDays() {
		return orderLeadTimeDays;
	}

	public void setOrderLeadTimeDays(String orderLeadTimeDays) {
		this.orderLeadTimeDays = orderLeadTimeDays;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public OffsetDateTime getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(OffsetDateTime createdTime) {
		this.createdTime = createdTime;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public OffsetDateTime getUpdationTime() {
		return updationTime;
	}

	public void setUpdationTime(OffsetDateTime updationTime) {
		this.updationTime = updationTime;
	}

	public String getAdditional() {
		return additional;
	}

	public void setAdditional(String additional) {
		this.additional = additional;
	}

	@Override
	public String toString() {
		return "Vendor [id=" + id + ", organisationName=" + organisationName + ", vendorName=" + vendorName
				+ ", vendorCode=" + vendorCode + ", description=" + description + ", active=" + active + ", country="
				+ country + ", state=" + state + ", city=" + city + ", zipCode=" + zipCode + ", url=" + url + ", notes="
				+ notes + ", fax=" + fax + ", phone=" + phone + ", email=" + email + ", contactName=" + contactName
				+ ", contactPhone=" + contactPhone + ", contactEmail=" + contactEmail + ", contactAddress="
				+ contactAddress + ", shippingCNTName=" + shippingCNTName + ", shippingPhone=" + shippingPhone
				+ ", shippingEmail=" + shippingEmail + ", shippingCNTAddress=" + shippingCNTAddress
				+ ", purchasingAddress=" + purchasingAddress + ", paymentTerms=" + paymentTerms + ", orderLeadTimeDays="
				+ orderLeadTimeDays + ", status=" + status + ", ipAddress=" + ipAddress + ", createdBy=" + createdBy
				+ ", createdTime=" + createdTime + ", updatedBy=" + updatedBy + ", updationTime=" + updationTime
				+ ", additional=" + additional + "]";
	}

}
