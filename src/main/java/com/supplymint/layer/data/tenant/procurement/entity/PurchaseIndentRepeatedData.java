package com.supplymint.layer.data.tenant.procurement.entity;

public class PurchaseIndentRepeatedData {

	public int itemDetails;
	public String cat1;
	public String cat2;
	public String cat3;
	public String cat4;
	public String UDF;
	public String desc1;
	public String desc2;
	public String desc3;
	public String desc4;
	public String desc5;
	public String desc6;
	public String noSet;
	public String gst;
	public String ppQyt;
	public String rate;
	public String netAmt;
	public String act;
	public String color;
	public String size;
	public String total1;
	
	public PurchaseIndentRepeatedData() {
		super();
	}
	
	public PurchaseIndentRepeatedData(int itemDetails, String cat1, String cat2, String cat3, String cat4, String uDF,
			String desc1, String desc2, String desc3, String desc4, String desc5, String desc6, String noSet,
			String gst, String ppQyt, String rate, String netAmt, String act, String color, String size,
			String total1) {
		super();
		this.itemDetails = itemDetails;
		this.cat1 = cat1;
		this.cat2 = cat2;
		this.cat3 = cat3;
		this.cat4 = cat4;
		UDF = uDF;
		this.desc1 = desc1;
		this.desc2 = desc2;
		this.desc3 = desc3;
		this.desc4 = desc4;
		this.desc5 = desc5;
		this.desc6 = desc6;
		this.noSet = noSet;
		this.gst = gst;
		this.ppQyt = ppQyt;
		this.rate = rate;
		this.netAmt = netAmt;
		this.act = act;
		this.color = color;
		this.size = size;
		this.total1 = total1;
	}
	public int getItemDetails() {
		return itemDetails;
	}
	public void setItemDetails(int itemDetails) {
		this.itemDetails = itemDetails;
	}
	public String getCat1() {
		return cat1;
	}
	public void setCat1(String cat1) {
		this.cat1 = cat1;
	}
	public String getCat2() {
		return cat2;
	}
	public void setCat2(String cat2) {
		this.cat2 = cat2;
	}
	public String getCat3() {
		return cat3;
	}
	public void setCat3(String cat3) {
		this.cat3 = cat3;
	}
	public String getCat4() {
		return cat4;
	}
	public void setCat4(String cat4) {
		this.cat4 = cat4;
	}
	public String getUDF() {
		return UDF;
	}
	public void setUDF(String uDF) {
		UDF = uDF;
	}
	public String getDesc1() {
		return desc1;
	}
	public void setDesc1(String desc1) {
		this.desc1 = desc1;
	}
	public String getDesc2() {
		return desc2;
	}
	public void setDesc2(String desc2) {
		this.desc2 = desc2;
	}
	public String getDesc3() {
		return desc3;
	}
	public void setDesc3(String desc3) {
		this.desc3 = desc3;
	}
	public String getDesc4() {
		return desc4;
	}
	public void setDesc4(String desc4) {
		this.desc4 = desc4;
	}
	public String getDesc5() {
		return desc5;
	}
	public void setDesc5(String desc5) {
		this.desc5 = desc5;
	}
	public String getDesc6() {
		return desc6;
	}
	public void setDesc6(String desc6) {
		this.desc6 = desc6;
	}
	public String getNoSet() {
		return noSet;
	}
	public void setNoSet(String noSet) {
		this.noSet = noSet;
	}
	public String getGst() {
		return gst;
	}
	public void setGst(String gst) {
		this.gst = gst;
	}
	public String getPpQyt() {
		return ppQyt;
	}
	public void setPpQyt(String ppQyt) {
		this.ppQyt = ppQyt;
	}
	public String getRate() {
		return rate;
	}
	public void setRate(String rate) {
		this.rate = rate;
	}
	public String getnetAmt() {
		return netAmt;
	}
	public void setnetAmt(String netAmt) {
		this.netAmt = netAmt;
	}
	public String getAct() {
		return act;
	}
	public void setAct(String act) {
		this.act = act;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public String gettotal1() {
		return total1;
	}
	public void settotal1(String total1) {
		this.total1 = total1;
	}
	@Override
	public String toString() {
		return "PurchaseIndentRepeatedData [itemDetails=" + itemDetails + ", cat1=" + cat1 + ", cat2=" + cat2
				+ ", cat3=" + cat3 + ", cat4=" + cat4 + ", UDF=" + UDF + ", desc1=" + desc1 + ", desc2=" + desc2
				+ ", desc3=" + desc3 + ", desc4=" + desc4 + ", desc5=" + desc5 + ", desc6=" + desc6 + ", noSet=" + noSet
				+ ", gst=" + gst + ", ppQyt=" + ppQyt + ", rate=" + rate + ", netAmt=" + netAmt + ", act=" + act
				+ ", color=" + color + ", size=" + size + ", total1=" + total1 + "]";
	}
	
	
}
