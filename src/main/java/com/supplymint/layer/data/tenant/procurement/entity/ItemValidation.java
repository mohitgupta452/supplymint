package com.supplymint.layer.data.tenant.procurement.entity;

public class ItemValidation {
	public String hl1Name;
	public String hl2Name;
	public String hl3Name;
	public String hl4Code;
	public String iCode;
	public String itemName;
	public String unique;
	public String brand;
	public String pattern;
	public String assortment;
	public String size;
	public String color;
	public String design;
	public String season;
	public String fabric;
	public String darwin;
	public String weaved;
	public double mrp;
	
	public ItemValidation(){
	super();
	}

	public ItemValidation(String hl1Name, String hl2Name, String hl3Name, String hl4Code, String iCode, String itemName,
			String unique, String brand, String pattern, String assortment, String size, String color, String design,
			String season, String fabric, String darwin, String weaved, double mrp) {
		super();
		this.hl1Name = hl1Name;
		this.hl2Name = hl2Name;
		this.hl3Name = hl3Name;
		this.hl4Code = hl4Code;
		this.iCode = iCode;
		this.itemName = itemName;
		this.unique = unique;
		this.brand = brand;
		this.pattern = pattern;
		this.assortment = assortment;
		this.size = size;
		this.color = color;
		this.design = design;
		this.season = season;
		this.fabric = fabric;
		this.darwin = darwin;
		this.weaved = weaved;
		this.mrp = mrp;
	}

	public String getHl1Name() {
		return hl1Name;
	}

	public void setHl1Name(String hl1Name) {
		this.hl1Name = hl1Name;
	}

	public String getHl2Name() {
		return hl2Name;
	}

	public void setHl2Name(String hl2Name) {
		this.hl2Name = hl2Name;
	}

	public String getHl3Name() {
		return hl3Name;
	}

	public void setHl3Name(String hl3Name) {
		this.hl3Name = hl3Name;
	}

	public String getHl4Code() {
		return hl4Code;
	}

	public void setHl4Code(String hl4Code) {
		this.hl4Code = hl4Code;
	}

	public String getiCode() {
		return iCode;
	}

	public void setiCode(String iCode) {
		this.iCode = iCode;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getUnique() {
		return unique;
	}

	public void setUnique(String unique) {
		this.unique = unique;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	public String getAssortment() {
		return assortment;
	}

	public void setAssortment(String assortment) {
		this.assortment = assortment;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getDesign() {
		return design;
	}

	public void setDesign(String design) {
		this.design = design;
	}

	public String getSeason() {
		return season;
	}

	public void setSeason(String season) {
		this.season = season;
	}

	public String getFabric() {
		return fabric;
	}

	public void setFabric(String fabric) {
		this.fabric = fabric;
	}

	public String getDarwin() {
		return darwin;
	}

	public void setDarwin(String darwin) {
		this.darwin = darwin;
	}

	public String getWeaved() {
		return weaved;
	}

	public void setWeaved(String weaved) {
		this.weaved = weaved;
	}

	public double getMrp() {
		return mrp;
	}

	public void setMrp(double mrp) {
		this.mrp = mrp;
	}

	@Override
	public String toString() {
		return "ItemValidation [hl1Name=" + hl1Name + ", hl2Name=" + hl2Name + ", hl3Name=" + hl3Name + ", hl4Code="
				+ hl4Code + ", iCode=" + iCode + ", itemName=" + itemName + ", unique=" + unique + ", brand=" + brand
				+ ", pattern=" + pattern + ", assortment=" + assortment + ", size=" + size + ", color=" + color
				+ ", design=" + design + ", season=" + season + ", fabric=" + fabric + ", darwin=" + darwin
				+ ", weaved=" + weaved + ", mrp=" + mrp + "]";
	}

	
	
	
	
}
