package com.supplymint.layer.data.tenant.administration.entity;

import java.time.OffsetDateTime;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.supplymint.util.LocalDateTimeSerializer;

public class ADMSiteDetail {

	private int id;
	private int siteId;
	private String detailsType;
	private String name;
	private String phone;
	private String email;
	private String mobile;
	private String website;
	private String address;
	private char active;
	private String status;
	private String ipAddress;
	private String createdBy;
	@JsonSerialize(using = LocalDateTimeSerializer.class)
//	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	private OffsetDateTime createdTime;
	private String updatedBy;
	@JsonSerialize(using = LocalDateTimeSerializer.class)
//	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	private OffsetDateTime updationTime;
	private String additional;

	public ADMSiteDetail() {

	}

	public ADMSiteDetail(int id, int siteId, String detailsType, String name, String phone, String email, String mobile,
			String website, String address, char active, String status, String ipAddress, String createdBy,
			OffsetDateTime createdTime, String updatedBy, OffsetDateTime updationTime, String additional) {
		super();
		this.id = id;
		this.siteId = siteId;
		this.detailsType = detailsType;
		this.name = name;
		this.phone = phone;
		this.email = email;
		this.mobile = mobile;
		this.website = website;
		this.address = address;
		this.active = active;
		this.status = status;
		this.ipAddress = ipAddress;
		this.createdBy = createdBy;
		this.createdTime = createdTime;
		this.updatedBy = updatedBy;
		this.updationTime = updationTime;
		this.additional = additional;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getSiteId() {
		return siteId;
	}

	public void setSiteId(int siteId) {
		this.siteId = siteId;
	}

	public String getDetailsType() {
		return detailsType;
	}

	public void setDetailsType(String detailsType) {
		this.detailsType = detailsType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public char getActive() {
		return active;
	}

	public void setActive(char active) {
		this.active = active;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public OffsetDateTime getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(OffsetDateTime createdTime) {
		this.createdTime = createdTime;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public OffsetDateTime getUpdationTime() {
		return updationTime;
	}

	public void setUpdationTime(OffsetDateTime updationTime) {
		this.updationTime = updationTime;
	}

	public String getAdditional() {
		return additional;
	}

	public void setAdditional(String additional) {
		this.additional = additional;
	}

	@Override
	public String toString() {
		return "ADMSiteDetail [id=" + id + ", siteId=" + siteId + ", detailsType=" + detailsType + ", name=" + name
				+ ", phone=" + phone + ", email=" + email + ", mobile=" + mobile + ", website=" + website + ", address="
				+ address + ", active=" + active + ", status=" + status + ", ipAddress=" + ipAddress + ", createdBy="
				+ createdBy + ", createdTime=" + createdTime + ", updatedBy=" + updatedBy + ", updationTime="
				+ updationTime + ", additional=" + additional + "]";
	}

	
}
