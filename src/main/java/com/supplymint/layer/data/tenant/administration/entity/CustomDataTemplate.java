package com.supplymint.layer.data.tenant.administration.entity;

public class CustomDataTemplate {

	private String fileName;
	private String fullFileName;
	private String orgCode;
	private String dataName;
	private String source;
	private String header;

	public CustomDataTemplate() {

	}

	public CustomDataTemplate(String fileName, String fullFileName, String orgCode, String dataName, String source,
			String header) {
		super();
		this.fileName = fileName;
		this.fullFileName = fullFileName;
		this.orgCode = orgCode;
		this.dataName = dataName;
		this.source = source;
		this.header = header;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFullFileName() {
		return fullFileName;
	}

	public void setFullFileName(String fullFileName) {
		this.fullFileName = fullFileName;
	}

	public String getOrgCode() {
		return orgCode;
	}

	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}

	public String getDataName() {
		return dataName;
	}

	public void setDataName(String dataName) {
		this.dataName = dataName;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	@Override
	public String toString() {
		return "CustomDataTemplate [fileName=" + fileName + ", fullFileName=" + fullFileName + ", orgCode=" + orgCode
				+ ", dataName=" + dataName + ", source=" + source + ", header=" + header + "]";
	}

}
