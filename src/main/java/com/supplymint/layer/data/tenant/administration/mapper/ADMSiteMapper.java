package com.supplymint.layer.data.tenant.administration.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.supplymint.layer.data.tenant.administration.entity.ADMSite;
import com.supplymint.layer.data.tenant.administration.entity.ADMSiteDetail;

@Mapper
public interface ADMSiteMapper {

	/**
	 * 
	 * @param id
	 * @return site data according to POJO
	 */
	ADMSite findById(Integer id);

	/**
	 * 
	 * @param offset
	 * @param pagesize
	 * @return all data according to page on UI.
	 */
	List<ADMSite> findAll(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize);

	/**
	 * 
	 * @param aDMSite
	 * @return //create the site data
	 */
	Integer create(ADMSite aDMSite);

	/**
	 * 
	 * @param aDMSite
	 * @return //update the site data
	 */
	Integer update(ADMSite aDMSite);

	/**
	 * 
	 * @param id
	 * @return delete the site data according to ID
	 */
	Integer delete(Integer id);

	/**
	 * 
	 * @param admSiteDetail
	 * @return count of creating ADM site detail data
	 */
	Integer createSiteDetail(ADMSiteDetail siteDetail);

	/**
	 * 
	 * @param admSiteDetail
	 * @return count of updating ADM site detail data
	 */
	Integer udpateSiteDetail(ADMSiteDetail siteDetail);

	/**
	 * 
	 * @param id
	 * @return count of deleting ADM site detail data
	 */
	Integer deleteSiteDetail(Integer id);

	/**
	 * 
	 * @return count of record
	 */
	@Select("SELECT COUNT(*) FROM ADM_SITES")
	Integer record();

	/**
	 * 
	 * @return List of all ADM site
	 */
	List<ADMSite> getAllSite();

	/**
	 * 
	 * @param offset
	 * @param pagesize
	 * @param search
	 * @return List of all searched data according to @param
	 */
	List<ADMSite> searchAll(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("search") String search);

	/**
	 * 
	 * @param search
	 * @return count of search record
	 */
	Integer searchRecord(@Param("search") String search);

	/**
	 * 
	 * @param admSite
	 * @return filtering data from ADM site
	 */
	List<ADMSite> filter(ADMSite admSite);

	/**
	 * 
	 * @param id
	 * @return count of deleting site data
	 */
	@Delete("DELETE FROM ADM_SITEMAP WHERE FROMSITE_ID = #{id} OR TOSITE_ID=#{id}")
	Integer deleteSiteMap(int id);

	/**
	 * 
	 * @param admSiteDetail
	 * @return List of checking data as its Existence
	 */
	@Select("SELECT EMAIL as email, MOBILE as mobile  FROM ADM_SITEDETAILS WHERE EMAIL = #{email} OR  MOBILE = #{mobile}")
	List<ADMSiteDetail> checkSiteExistance(ADMSiteDetail admSiteDetail);

	/**
	 * 
	 * @param admSiteDetail
	 * @return List of update data in ADM site detail
	 */
	@Select("SELECT EMAIL as email, MOBILE as mobile  FROM ADM_SITEDETAILS WHERE "
			+ "(EMAIL = #{email} OR  MOBILE = #{mobile}) AND SITE_ID != #{siteId}")
	List<ADMSiteDetail> checkSiteUpdateExistance(ADMSiteDetail admSiteDetail);

	/**
	 * 
	 * @param admSite
	 * @return count of presenting data in ADM site
	 */
	Integer filterRecord(ADMSite admSite);

	/**
	 * 
	 * @return List of all ADM site record
	 */
	List<ADMSite> getAllRecord();

	/**
	 * 
	 * @param listAdmSite
	 * @return count of inserting data in ADM site
	 */
	public int batchSiteInsert(List<ADMSite> listAdmSite);

	@Select("select count(*) from user_site where username = #{username}")
	public int countPOSite(@Param("username") String username);

	@Select("SELECT distinct SITE_CODE, SITE_NAME FROM USER_SITE  where USERNAME = #{username} OFFSET #{offset} ROWS FETCH NEXT #{pagesize} ROWS ONLY")
	public List<Map<String, String>> getPOSite(@Param("username") String username, @Param("offset") int offset,
			@Param("pagesize") int pagesize);

	public int countSearchPOSite(@Param("username") String username, @Param("search") String search);

	public List<Map<String, String>> getSearchPOSite(@Param("username") String username, @Param("search") String search,
			@Param("offset") int offset, @Param("pagesize") int pagesize);

	@Select("SELECT distinct SITE_CODE, SITE_NAME FROM USER_SITE  where USERNAME = #{username} and default_site = 'Y'")
	public Map<String, String> getDefaultPOSite(@Param("username") String username);
}
