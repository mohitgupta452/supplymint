package com.supplymint.layer.data.tenant.inventory.entity;

public class HeadersSpecificOutput {

	private String label1;
	private String label2;
	private String label3;
	private String label4;
	private String label5;
	private String label6;
	private String label7;
	private String label8;
	private String label9;
	private String label10;
	private String label11;
	private String label12;
	private String label13;
	private String label14;
	private String label15;
	private String label16;
	private String label17;
	private String label18;
	private String label19;
	private String label20;
	private String label21;
	private String label22;
	private String label23;
	private String label24;
	private String label25;
	private String label26;
	private String label27;
	private String label28;
	private String label29;
	private String label30;
	private String label31;
	private String label32;
	private String label33;
	private String label34;
	private String label35;
	private String label36;
	private String label37;
	private String label38;
	private String label39;
	private String label40;
	private String label41;
	private String label42;
	private String label43;
	private String label44;
	private String label45;
	private String label46;
	private String label47;
	private String label48;
	private String label49;
	private String label50;
	private String label51;
	private String label52;
	private String label53;
	private String label54;
	private String label55;
	private String label56;
	private String label57;
	private String label58;
	private String label59;
	private String label60;
	private String label61;
	private String label62;
	private String label63;
	private String label64;
	private String label65;
	private String label66;
	private String label67;
	private String label68;
	private String label69;
	private String label70;
	private String label71;
	private String label72;
	private String label73;
	private String label74;
	private String label75;
	private String label76;
	private String label77;
	private String label78;
	private String label79;
	private String label80;
	private String label81;
	private String label82;
	private String label83;
	private String label84;
	private String label85;
	private String label86;
	private String label87;
	private String label88;
	private String label89;
	private String label90;
	
	
	public HeadersSpecificOutput() {
		super();
	}

	public HeadersSpecificOutput(String label0, String label1, String label2, String label3, String label4,
			String label5, String label6, String label7, String label8, String label9, String label10, String label11,
			String label12, String label13, String label14, String label15, String label16, String label17,
			String label18, String label19, String label20, String label21, String label22, String label23,
			String label24, String label25, String label26, String label27, String label28, String label29,
			String label30, String label31, String label32, String label33, String label34, String label35,
			String label36, String label37, String label38, String label39, String label40, String label41,
			String label42, String label43, String label44, String label45, String label46, String label47,
			String label48, String label49, String label50, String label51, String label52, String label53,
			String label54, String label55, String label56, String label57, String label58, String label59,
			String label60, String label61, String label62, String label63, String label64, String label65,
			String label66, String label67, String label68, String label69, String label70, String label71,
			String label72, String label73, String label74, String label75, String label76, String label77,
			String label78, String label79, String label80, String label81, String label82, String label83,
			String label84, String label85, String label86, String label87, String label88, String label89,
			String label90) {
		super();
		this.label1 = label1;
		this.label2 = label2;
		this.label3 = label3;
		this.label4 = label4;
		this.label5 = label5;
		this.label6 = label6;
		this.label7 = label7;
		this.label8 = label8;
		this.label9 = label9;
		this.label10 = label10;
		this.label11 = label11;
		this.label12 = label12;
		this.label13 = label13;
		this.label14 = label14;
		this.label15 = label15;
		this.label16 = label16;
		this.label17 = label17;
		this.label18 = label18;
		this.label19 = label19;
		this.label20 = label20;
		this.label21 = label21;
		this.label22 = label22;
		this.label23 = label23;
		this.label24 = label24;
		this.label25 = label25;
		this.label26 = label26;
		this.label27 = label27;
		this.label28 = label28;
		this.label29 = label29;
		this.label30 = label30;
		this.label31 = label31;
		this.label32 = label32;
		this.label33 = label33;
		this.label34 = label34;
		this.label35 = label35;
		this.label36 = label36;
		this.label37 = label37;
		this.label38 = label38;
		this.label39 = label39;
		this.label40 = label40;
		this.label41 = label41;
		this.label42 = label42;
		this.label43 = label43;
		this.label44 = label44;
		this.label45 = label45;
		this.label46 = label46;
		this.label47 = label47;
		this.label48 = label48;
		this.label49 = label49;
		this.label50 = label50;
		this.label51 = label51;
		this.label52 = label52;
		this.label53 = label53;
		this.label54 = label54;
		this.label55 = label55;
		this.label56 = label56;
		this.label57 = label57;
		this.label58 = label58;
		this.label59 = label59;
		this.label60 = label60;
		this.label61 = label61;
		this.label62 = label62;
		this.label63 = label63;
		this.label64 = label64;
		this.label65 = label65;
		this.label66 = label66;
		this.label67 = label67;
		this.label68 = label68;
		this.label69 = label69;
		this.label70 = label70;
		this.label71 = label71;
		this.label72 = label72;
		this.label73 = label73;
		this.label74 = label74;
		this.label75 = label75;
		this.label76 = label76;
		this.label77 = label77;
		this.label78 = label78;
		this.label79 = label79;
		this.label80 = label80;
		this.label81 = label81;
		this.label82 = label82;
		this.label83 = label83;
		this.label84 = label84;
		this.label85 = label85;
		this.label86 = label86;
		this.label87 = label87;
		this.label88 = label88;
		this.label89 = label89;
		this.label90 = label90;
	}


	public String getLabel1() {
		return label1;
	}

	public void setLabel1(String label1) {
		this.label1 = label1;
	}

	public String getLabel2() {
		return label2;
	}

	public void setLabel2(String label2) {
		this.label2 = label2;
	}

	public String getLabel3() {
		return label3;
	}

	public void setLabel3(String label3) {
		this.label3 = label3;
	}

	public String getLabel4() {
		return label4;
	}

	public void setLabel4(String label4) {
		this.label4 = label4;
	}

	public String getLabel5() {
		return label5;
	}

	public void setLabel5(String label5) {
		this.label5 = label5;
	}

	public String getLabel6() {
		return label6;
	}

	public void setLabel6(String label6) {
		this.label6 = label6;
	}

	public String getLabel7() {
		return label7;
	}

	public void setLabel7(String label7) {
		this.label7 = label7;
	}

	public String getLabel8() {
		return label8;
	}

	public void setLabel8(String label8) {
		this.label8 = label8;
	}

	public String getLabel9() {
		return label9;
	}

	public void setLabel9(String label9) {
		this.label9 = label9;
	}

	public String getLabel10() {
		return label10;
	}

	public void setLabel10(String label10) {
		this.label10 = label10;
	}

	public String getLabel11() {
		return label11;
	}

	public void setLabel11(String label11) {
		this.label11 = label11;
	}

	public String getLabel12() {
		return label12;
	}

	public void setLabel12(String label12) {
		this.label12 = label12;
	}

	public String getLabel13() {
		return label13;
	}

	public void setLabel13(String label13) {
		this.label13 = label13;
	}

	public String getLabel14() {
		return label14;
	}

	public void setLabel14(String label14) {
		this.label14 = label14;
	}

	public String getLabel15() {
		return label15;
	}

	public void setLabel15(String label15) {
		this.label15 = label15;
	}

	public String getLabel16() {
		return label16;
	}

	public void setLabel16(String label16) {
		this.label16 = label16;
	}

	public String getLabel17() {
		return label17;
	}

	public void setLabel17(String label17) {
		this.label17 = label17;
	}

	public String getLabel18() {
		return label18;
	}

	public void setLabel18(String label18) {
		this.label18 = label18;
	}

	public String getLabel19() {
		return label19;
	}

	public void setLabel19(String label19) {
		this.label19 = label19;
	}

	public String getLabel20() {
		return label20;
	}

	public void setLabel20(String label20) {
		this.label20 = label20;
	}

	public String getLabel21() {
		return label21;
	}

	public void setLabel21(String label21) {
		this.label21 = label21;
	}

	public String getLabel22() {
		return label22;
	}

	public void setLabel22(String label22) {
		this.label22 = label22;
	}

	public String getLabel23() {
		return label23;
	}

	public void setLabel23(String label23) {
		this.label23 = label23;
	}

	public String getLabel24() {
		return label24;
	}

	public void setLabel24(String label24) {
		this.label24 = label24;
	}

	public String getLabel25() {
		return label25;
	}

	public void setLabel25(String label25) {
		this.label25 = label25;
	}

	public String getLabel26() {
		return label26;
	}

	public void setLabel26(String label26) {
		this.label26 = label26;
	}

	public String getLabel27() {
		return label27;
	}

	public void setLabel27(String label27) {
		this.label27 = label27;
	}

	public String getLabel28() {
		return label28;
	}

	public void setLabel28(String label28) {
		this.label28 = label28;
	}

	public String getLabel29() {
		return label29;
	}

	public void setLabel29(String label29) {
		this.label29 = label29;
	}

	public String getLabel30() {
		return label30;
	}

	public void setLabel30(String label30) {
		this.label30 = label30;
	}

	public String getLabel31() {
		return label31;
	}

	public void setLabel31(String label31) {
		this.label31 = label31;
	}

	public String getLabel32() {
		return label32;
	}

	public void setLabel32(String label32) {
		this.label32 = label32;
	}

	public String getLabel33() {
		return label33;
	}

	public void setLabel33(String label33) {
		this.label33 = label33;
	}

	public String getLabel34() {
		return label34;
	}

	public void setLabel34(String label34) {
		this.label34 = label34;
	}

	public String getLabel35() {
		return label35;
	}

	public void setLabel35(String label35) {
		this.label35 = label35;
	}

	public String getLabel36() {
		return label36;
	}

	public void setLabel36(String label36) {
		this.label36 = label36;
	}

	public String getLabel37() {
		return label37;
	}

	public void setLabel37(String label37) {
		this.label37 = label37;
	}

	public String getLabel38() {
		return label38;
	}

	public void setLabel38(String label38) {
		this.label38 = label38;
	}

	public String getLabel39() {
		return label39;
	}

	public void setLabel39(String label39) {
		this.label39 = label39;
	}

	public String getLabel40() {
		return label40;
	}

	public void setLabel40(String label40) {
		this.label40 = label40;
	}

	public String getLabel41() {
		return label41;
	}

	public void setLabel41(String label41) {
		this.label41 = label41;
	}

	public String getLabel42() {
		return label42;
	}

	public void setLabel42(String label42) {
		this.label42 = label42;
	}

	public String getLabel43() {
		return label43;
	}

	public void setLabel43(String label43) {
		this.label43 = label43;
	}

	public String getLabel44() {
		return label44;
	}

	public void setLabel44(String label44) {
		this.label44 = label44;
	}

	public String getLabel45() {
		return label45;
	}

	public void setLabel45(String label45) {
		this.label45 = label45;
	}

	public String getLabel46() {
		return label46;
	}

	public void setLabel46(String label46) {
		this.label46 = label46;
	}

	public String getLabel47() {
		return label47;
	}

	public void setLabel47(String label47) {
		this.label47 = label47;
	}

	public String getLabel48() {
		return label48;
	}

	public void setLabel48(String label48) {
		this.label48 = label48;
	}

	public String getLabel49() {
		return label49;
	}

	public void setLabel49(String label49) {
		this.label49 = label49;
	}

	public String getLabel50() {
		return label50;
	}

	public void setLabel50(String label50) {
		this.label50 = label50;
	}

	public String getLabel51() {
		return label51;
	}

	public void setLabel51(String label51) {
		this.label51 = label51;
	}

	public String getLabel52() {
		return label52;
	}

	public void setLabel52(String label52) {
		this.label52 = label52;
	}

	public String getLabel53() {
		return label53;
	}

	public void setLabel53(String label53) {
		this.label53 = label53;
	}

	public String getLabel54() {
		return label54;
	}

	public void setLabel54(String label54) {
		this.label54 = label54;
	}

	public String getLabel55() {
		return label55;
	}

	public void setLabel55(String label55) {
		this.label55 = label55;
	}

	public String getLabel56() {
		return label56;
	}

	public void setLabel56(String label56) {
		this.label56 = label56;
	}

	public String getLabel57() {
		return label57;
	}

	public void setLabel57(String label57) {
		this.label57 = label57;
	}

	public String getLabel58() {
		return label58;
	}

	public void setLabel58(String label58) {
		this.label58 = label58;
	}

	public String getLabel59() {
		return label59;
	}

	public void setLabel59(String label59) {
		this.label59 = label59;
	}

	public String getLabel60() {
		return label60;
	}

	public void setLabel60(String label60) {
		this.label60 = label60;
	}

	public String getLabel61() {
		return label61;
	}

	public void setLabel61(String label61) {
		this.label61 = label61;
	}

	public String getLabel62() {
		return label62;
	}

	public void setLabel62(String label62) {
		this.label62 = label62;
	}

	public String getLabel63() {
		return label63;
	}

	public void setLabel63(String label63) {
		this.label63 = label63;
	}

	public String getLabel64() {
		return label64;
	}

	public void setLabel64(String label64) {
		this.label64 = label64;
	}

	public String getLabel65() {
		return label65;
	}

	public void setLabel65(String label65) {
		this.label65 = label65;
	}

	public String getLabel66() {
		return label66;
	}

	public void setLabel66(String label66) {
		this.label66 = label66;
	}

	public String getLabel67() {
		return label67;
	}

	public void setLabel67(String label67) {
		this.label67 = label67;
	}

	public String getLabel68() {
		return label68;
	}

	public void setLabel68(String label68) {
		this.label68 = label68;
	}

	public String getLabel69() {
		return label69;
	}

	public void setLabel69(String label69) {
		this.label69 = label69;
	}

	public String getLabel70() {
		return label70;
	}

	public void setLabel70(String label70) {
		this.label70 = label70;
	}

	public String getLabel71() {
		return label71;
	}

	public void setLabel71(String label71) {
		this.label71 = label71;
	}

	public String getLabel72() {
		return label72;
	}

	public void setLabel72(String label72) {
		this.label72 = label72;
	}

	public String getLabel73() {
		return label73;
	}

	public void setLabel73(String label73) {
		this.label73 = label73;
	}

	public String getLabel74() {
		return label74;
	}

	public void setLabel74(String label74) {
		this.label74 = label74;
	}

	public String getLabel75() {
		return label75;
	}

	public void setLabel75(String label75) {
		this.label75 = label75;
	}

	public String getLabel76() {
		return label76;
	}

	public void setLabel76(String label76) {
		this.label76 = label76;
	}

	public String getLabel77() {
		return label77;
	}

	public void setLabel77(String label77) {
		this.label77 = label77;
	}

	public String getLabel78() {
		return label78;
	}

	public void setLabel78(String label78) {
		this.label78 = label78;
	}

	public String getLabel79() {
		return label79;
	}

	public void setLabel79(String label79) {
		this.label79 = label79;
	}

	public String getLabel80() {
		return label80;
	}

	public void setLabel80(String label80) {
		this.label80 = label80;
	}

	public String getLabel81() {
		return label81;
	}

	public void setLabel81(String label81) {
		this.label81 = label81;
	}

	public String getLabel82() {
		return label82;
	}

	public void setLabel82(String label82) {
		this.label82 = label82;
	}

	public String getLabel83() {
		return label83;
	}

	public void setLabel83(String label83) {
		this.label83 = label83;
	}

	public String getLabel84() {
		return label84;
	}

	public void setLabel84(String label84) {
		this.label84 = label84;
	}

	public String getLabel85() {
		return label85;
	}

	public void setLabel85(String label85) {
		this.label85 = label85;
	}

	public String getLabel86() {
		return label86;
	}

	public void setLabel86(String label86) {
		this.label86 = label86;
	}

	public String getLabel87() {
		return label87;
	}

	public void setLabel87(String label87) {
		this.label87 = label87;
	}

	public String getLabel88() {
		return label88;
	}

	public void setLabel88(String label88) {
		this.label88 = label88;
	}

	public String getLabel89() {
		return label89;
	}

	public void setLabel89(String label89) {
		this.label89 = label89;
	}

	public String getLabel90() {
		return label90;
	}

	public void setLabel90(String label90) {
		this.label90 = label90;
	}

	@Override
	public String toString() {
		return "HeadersSpecificOutput [label1=" + label1 + ", label2=" + label2 + ", label3="
				+ label3 + ", label4=" + label4 + ", label5=" + label5 + ", label6=" + label6 + ", label7=" + label7
				+ ", label8=" + label8 + ", label9=" + label9 + ", label10=" + label10 + ", label11=" + label11
				+ ", label12=" + label12 + ", label13=" + label13 + ", label14=" + label14 + ", label15=" + label15
				+ ", label16=" + label16 + ", label17=" + label17 + ", label18=" + label18 + ", label19=" + label19
				+ ", label20=" + label20 + ", label21=" + label21 + ", label22=" + label22 + ", label23=" + label23
				+ ", label24=" + label24 + ", label25=" + label25 + ", label26=" + label26 + ", label27=" + label27
				+ ", label28=" + label28 + ", label29=" + label29 + ", label30=" + label30 + ", label31=" + label31
				+ ", label32=" + label32 + ", label33=" + label33 + ", label34=" + label34 + ", label35=" + label35
				+ ", label36=" + label36 + ", label37=" + label37 + ", label38=" + label38 + ", label39=" + label39
				+ ", label40=" + label40 + ", label41=" + label41 + ", label42=" + label42 + ", label43=" + label43
				+ ", label44=" + label44 + ", label45=" + label45 + ", label46=" + label46 + ", label47=" + label47
				+ ", label48=" + label48 + ", label49=" + label49 + ", label50=" + label50 + ", label51=" + label51
				+ ", label52=" + label52 + ", label53=" + label53 + ", label54=" + label54 + ", label55=" + label55
				+ ", label56=" + label56 + ", label57=" + label57 + ", label58=" + label58 + ", label59=" + label59
				+ ", label60=" + label60 + ", label61=" + label61 + ", label62=" + label62 + ", label63=" + label63
				+ ", label64=" + label64 + ", label65=" + label65 + ", label66=" + label66 + ", label67=" + label67
				+ ", label68=" + label68 + ", label69=" + label69 + ", label70=" + label70 + ", label71=" + label71
				+ ", label72=" + label72 + ", label73=" + label73 + ", label74=" + label74 + ", label75=" + label75
				+ ", label76=" + label76 + ", label77=" + label77 + ", label78=" + label78 + ", label79=" + label79
				+ ", label80=" + label80 + ", label81=" + label81 + ", label82=" + label82 + ", label83=" + label83
				+ ", label84=" + label84 + ", label85=" + label85 + ", label86=" + label86 + ", label87=" + label87
				+ ", label88=" + label88 + ", label89=" + label89 + ", label90=" + label90 + "]";
	}
	
	
	

}
