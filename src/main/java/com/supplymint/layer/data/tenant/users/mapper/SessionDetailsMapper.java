package com.supplymint.layer.data.tenant.users.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.supplymint.layer.data.tenant.users.entity.SessionDetails;

@Mapper
public interface SessionDetailsMapper {

	int create(SessionDetails sessionDetails);

	int update(SessionDetails sessionDetails);
	int updateOtherUser(SessionDetails sessionDetails);

	@Update("UPDATE SESSION_DETAILS SET STATUS='INACTIVE' WHERE USER_NAME =#{userName} AND STATUS='ACTIVE'")
	int updateStatus(String userName);
	
	SessionDetails getByUserName(@Param("userName") String userName);

}
