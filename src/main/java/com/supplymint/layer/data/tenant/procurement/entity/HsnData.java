package com.supplymint.layer.data.tenant.procurement.entity;

import java.math.BigDecimal;

public class HsnData {

	private BigDecimal code;
	private String hsn_sac_code;

	public HsnData() {
		super();
		// TODO Auto-generated constructor stub
	}

	public BigDecimal getCode() {
		return code;
	}

	public void setCode(BigDecimal code) {
		this.code = code;
	}

	public String getHsn_sac_code() {
		return hsn_sac_code;
	}

	public void setHsn_sac_code(String hsn_sac_code) {
		this.hsn_sac_code = hsn_sac_code;
	}

	@Override
	public String toString() {
		return "HsnData [code=" + code + ", hsn_sac_code=" + hsn_sac_code + "]";
	}

}
