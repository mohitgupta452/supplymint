package com.supplymint.layer.data.tenant.demand.mapper;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.mapping.StatementType;

import com.supplymint.layer.data.tenant.demand.entity.BudgetedDemandPlanning;
import com.supplymint.layer.data.tenant.demand.entity.BudgetedTemporary;
import com.supplymint.layer.data.tenant.demand.entity.DemandForecast;
import com.supplymint.layer.data.tenant.demand.entity.DemandForecastScheduler;
import com.supplymint.layer.data.tenant.demand.entity.DemandPlanning;
import com.supplymint.layer.data.tenant.demand.entity.DemandPlanningViewLog;
import com.supplymint.layer.data.tenant.demand.entity.DpUploadSummary;
import com.supplymint.layer.data.tenant.demand.entity.FinalAssortment;

/**
 * This interface describes all service for Demand Planning
 * @author Prabhakar Srivastava
 * @author Bishnu Dutta
 * @Date 10 Jan 2019
 * @version 1.0
 */
@Mapper
public interface DemandPlanningMapper {
	
	/**
	 * This method describes checking condition
	 * for running Forecast job
	 * @Table DEMAND_PLANNING
	 */
	@Select("SELECT COUNT(*) FROM DEMAND_PLANNING WHERE STATUS='Processing'")
	int validationCreate();

	/**
	 * This method describes create Demand Plan
	 * @param DemandPlanning
	 * @Table DEMAND_PLANNING
	 */
	int create(DemandPlanning demandPlanning);
	
	/**
	 * This method describes update status with code
	 * for running Forecast job
	 * @Table DEMAND_PLANNING
	 */
	int updateCodeWithStatus(@Param("statusCode") Integer statusCode,
			@Param("statusMessage") String statusMessage, @Param("runId") String runId,
			@Param("duration") String duration, @Param("updationTime") Date updationTime,@Param("forcastOnAssortmentCode") String forcastOnAssortmentCode);

	/**
	 * This method describes getting all record
	 * for demand planning history 
	 * @Table DEMAND_PLANNING
	 */
	@Select("SELECT COUNT(*) FROM DEMAND_PLANNING")
	int record();

	/**
	 * This method describes getting all demand planning history
	 * according to pagination
	 * @Table DEMAND_PLANNING
	 */
	List<DemandPlanning> getAll(@Param("offset") Integer offset, @Param("pageSize") Integer pageSize);

	/**
	 * This method describes getting all filter record for
	 * demand planning history
	 * @param DemandPlanning
	 * @Table DEMAND_PLANNING
	 */
	int filterRecord(DemandPlanning demandPlanning);

	/**
	 * This method describes getting all filter for
	 * demand planning history according to pagination
	 * @param DemandPlanning
	 * @Table DEMAND_PLANNING
	 */
	List<DemandPlanning> filter(DemandPlanning demandPlanning);

	/**
	 * This method describes getting all Search record for
	 * demand planning history
	 * @param Search
	 * @Table DEMAND_PLANNING
	 */
	int countSearchRecord(@Param("search") String search);

	/**
	 * This method describes getting all Search for
	 * demand planning history according to pagination
	 * @param Search
	 * @Table DEMAND_PLANNING
	 */
	List<DemandPlanning> search(@Param("offset") Integer offset, @Param("pageSize") Integer pageSize,
			@Param("search") String search);

	/**
	 * This method describes getting all Distinct Assortment
	 * record for demand planning graph
	 * @Table DEMAND_ASSORTMENT_MONTHLY
	 */
	@Select("SELECT COUNT(DISTINCT ASSORTMENTCODE) FROM DEMAND_ASSORTMENT_MONTHLY")
	int assortmentRecord();

	/**
	 * This method describes getting all Distinct Assortment
	 * for demand planning graph according to pagination
	 * @Table DEMAND_ASSORTMENT_MONTHLY
	 */
	@Select("SELECT DISTINCT ASSORTMENTCODE FROM DEMAND_ASSORTMENT_MONTHLY  OFFSET #{offset} ROWS FETCH NEXT #{pageSize} ROWS ONLY")
	List<String> getAssortment(@Param("offset") Integer offset, @Param("pageSize") Integer pageSize);

	/**
	 * This method describes getting all Search record
	 * for distinct assortmentCode
	 * @param Search
	 * @Table DEMAND_ASSORTMENT_MONTHLY
	 */
	int assortmentSearchRecord(@Param("search") String search);

	/**
	 * This method describes getting all Search for
	 * distinct assortmentCode according to pagination
	 * @param Search
	 * @Table DEMAND_ASSORTMENT_MONTHLY
	 */
	List<String> assortmentSearch(@Param("offset") Integer offset, @Param("pageSize") Integer pageSize,
			@Param("search") String search);
	
	/**
	 * This method describes getting Actual Data graph
	 * for Monthly according to user date range
	 * @Table DEMAND_ASSORTMENT_MONTHLY
	 */
	List<DemandForecast> actualMonthlyAssortment(@Param("assortment") String assortment,
			@Param("startDate") String startDate, @Param("endDate") String endDate);

	/**
	 * This method describes getting Predicted Data graph
	 * for Monthly according to user date range
	 * @Table DEMAND_FORECAST_MONTHLY
	 */
	List<DemandForecast> predictedMonthlyAssortment(@Param("assortment") String assortment,
			@Param("startDate") String startDate, @Param("endDate") String endDate);

	/**
	 * This method describes getting Budgeted Data graph
	 * for Monthly according to user date range
	 * @Table DEMAND_BUDGETED_MONTHLY
	 */
	List<DemandForecast> budgetedMonthlyAssortment(@Param("assortment") String assortment,
			@Param("startDate") String startDate, @Param("endDate") String endDate);

	/**
	 * This method describes getting Actual Data graph
	 * for Monthly according to user date range
	 * @Table DEMAND_ASSORTMENT_WEEKLY
	 */
	List<DemandForecast> actualWeeklyAssortment(@Param("assortment") String assortment,
			@Param("startDate") String startDate, @Param("endDate") String endDate);

	/**
	 * This method describes getting Predicted Data graph
	 * for Weekly according to user date range
	 * @Table DEMAND_FORECAST_WEEKLY
	 */
	List<DemandForecast> predictedWeeklyAssortment(@Param("assortment") String assortment,
			@Param("startDate") String startDate, @Param("endDate") String endDate);

	/**
	 * This method describes getting Budgeted Data graph
	 * for Monthly according to user date range
	 * @Table DEMAND_BUDGETED_WEEKLY
	 */
	List<DemandForecast> budgetedWeeklyAssortment(@Param("assortment") String assortment,
			@Param("startDate") String startDate, @Param("endDate") String endDate);

	/**
	 * This method describes get previous forecast
	 * @Table DEMAND_PLANNING
	 */
	@Select("SELECT RUNID FROM DEMAND_PLANNING ORDER BY CREATEDON DESC FETCH FIRST 1 ROWS ONLY")
	String getRunIdPattern();

	/**
	 * This method describes get by Run ID
	 * @param RUN ID
	 * @Table DEMAND_PLANNING
	 */
	DemandPlanning getByRunId(@Param("runId") String runId);

	/**
	 * This method describes update summary status
	 * @param RUN ID, Summary status
	 * @Table DEMAND_PLANNING
	 */
	int updateSummary(@Param("summary") String summary, @Param("runId") String runId);

	/**
	 * This method describes update previous summary status
	 * @param RUN ID
	 * @Table DEMAND_PLANNING
	 */
	@Update("UPDATE DEMAND_PLANNING SET SUMMARY='FALSE' WHERE RUNID!=#{runId}")
	int updatePreviousSummary(@Param("runId") String runId);

	/**
	 * This method describes Retrieve all details
	 * for demand planning
	 * @Table DEMAND_PLANNING
	 */
	List<DemandPlanning> downloadForecastDetails();

	/**
	 * This method describes update Forecast download URL
	 * after succeeded the forecast
	 * @param Saved Path, Download Path, Run ID
	 * @Table DEMAND_PLANNING
	 */
	int updateDownloadUrl(@Param("savedPath") String savedPath, @Param("downloadUrl") String downloadUrl,
			@Param("runId") String runId);

	/**
	 * This method describes create Activity Log for
	 * Demand Planning Insight Graph according to parameter
	 * @Table DEMAND_PLANNING_VIEWLOG
	 */
	int createDemandPlanningLog(DemandPlanningViewLog viewLog);

	/**
	 * This method describes update Previous STATUS to False for
	 * Demand Planning Insight Graph according to parameter
	 * @Table DEMAND_PLANNING_VIEWLOG
	 */
	@Update("UPDATE DEMAND_PLANNING_VIEWLOG SET LASTSUMMARY='FALSE' WHERE ID!=#{uuid} AND FREQUENCY=#{frequency}")
	int updateSummaryStatus(@Param("uuid") String uuid, @Param("frequency") String frequency);

	/**
	 * This method describes update Current STATUS to False for
	 * Demand Planning Insight Graph according to parameter
	 * @Table DEMAND_PLANNING_VIEWLOG
	 */
	@Update("UPDATE DEMAND_PLANNING_VIEWLOG SET LASTSUMMARY='FALSE' WHERE ID=#{uuid} AND FREQUENCY=#{frequency}")
	int updateFailedSummaryStatus(@Param("uuid") String uuid, @Param("frequency") String frequency);

	/**
	 * This method describes update STATUS and URL for
	 * Demand Planning Insight Graph according to parameter
	 * @Table DEMAND_PLANNING_VIEWLOG
	 */
	@Update("UPDATE DEMAND_PLANNING_VIEWLOG SET STATUS=#{status},LASTSUMMARY=#{summary},DOWNLOAD_URL=#{url} WHERE ID=#{uuid}")
	int updateStatus(@Param("status") String status, @Param("summary") String summary, @Param("url") String url,
			@Param("uuid") String uuid);

	/**
	 * This method describes getting Demand Planning
	 * Graph Info
	 * @param Frequency, UUID
	 * @Table DEMAND_PLANNING_VIEWLOG
	 */
	DemandPlanningViewLog getLastReportByFrequency(@Param("frequency") String frequency,
			@Param("uuid") String uuid);
	
	/**
	 * This method describes getting all record
	 * for demand Budgeted history 
	 * @Table DP_UPLOAD_SUMMARY
	 */
	@Select("SELECT COUNT(*) FROM DP_UPLOAD_SUMMARY")
	int countBudgetedHistoryData();

	/**
	 * This method describes getting all data
	 * for demand Budgeted history according to pagination
	 * @Table DP_UPLOAD_SUMMARY
	 */
	List<DpUploadSummary> getBudgetedHistoryData(@Param("offset") Integer offset,
			@Param("pagesize") Integer pagesize);

	/**
	 * This method describes getting all filter record for
	 * demand budgeted history
	 * @Table DP_UPLOAD_SUMMARY
	 */
	int countFilterBudgetedHistoryData(@Param("uploadedDate") String uploadedDate,
			@Param("fileName") String fileName, @Param("totalData") String totalData, @Param("valid") String valid,
			@Param("invalid") String invalid);

	/**
	 * This method describes getting all filter for
	 * demand budgeted history according to pagination
	 * @Table DP_UPLOAD_SUMMARY
	 */
	List<DpUploadSummary> filterBudgetedHistoryData(@Param("offset") Integer offset,
			@Param("pagesize") Integer pagesize, @Param("uploadedDate") String uploadedDate,
			@Param("fileName") String fileName, @Param("totalData") String totalData, @Param("valid") String valid,
			@Param("invalid") String invalid);

	/**
	 * This method describes getting all Search record for
	 * demand Budgeted history
	 * @param Search
	 * @Table DP_UPLOAD_SUMMARY
	 */
	int countSearchBudgetedHistoryData(@Param("search") String search);

	/**
	 * This method describes getting all Search for
	 * demand budgeted history according to pagination
	 * @param Search
	 * @Table DP_UPLOAD_SUMMARY
	 */
	List<DpUploadSummary> searchBudgetedHistoryData(@Param("offset") Integer offset,
			@Param("pagesize") Integer pagesize, @Param("search") String search);

	/**
	 * This method describes update last Demand Budgeted Weekly Status False
	 * @param User Name
	 * @Table DEMAND_BUDGETED_WEEKLY
	 */
	int updateLastDemandBudgetedWeeklyData(@Param("userName") String userName);

	/**
	 * This method describes update last Demand Budgeted Monthly Status False
	 * @param User Name
	 * @Table DEMAND_BUDGETED_MONTHLY
	 */
	int updateLastDemandBudgeteMonthlyData(@Param("userName") String userName);

	/**
	 * This method describes get All invalid Demand Budgeted Weekly Data
	 * @Table DEMAND_BUDGETED_WEEKLY
	 */
	List<BudgetedDemandPlanning> getAllInvalidWeeklyData();

	/**
	 * This method describes get All invalid Demand Budgeted Monthly Data
	 * @Table DEMAND_BUDGETED_MONTHLY
	 */
	List<BudgetedDemandPlanning> getAllInvalidMonthlyData();

	/**
	 * This method describes get last Demand Budgeted Weekly Data
	 * @param User Name
	 * @Table DEMAND_BUDGETED_WEEKLY
	 */
	List<BudgetedDemandPlanning> getAllWeeklyLastData(@Param("userName") String userName);

	/**
	 * This method describes get last Demand Budgeted Monthly Data
	 * @param User Name
	 * @Table DEMAND_BUDGETED_MONTHLY
	 */
	List<BudgetedDemandPlanning> getAllMonthlyLastData(@Param("userName") String userName);

	/**
	 * This method describes batch insert for
	 * Demand Budgeted Temporary table
	 * @param List<BudgetedTemporary>
	 * @Table DEMAND_BUDGETED_TEMP
	 */
	int insertFromExcelFileInList(@Param("data") List<BudgetedTemporary> data);

	/**
	 * This method describes update previous status False
	 * @param User Name, ID
	 * @Table DP_UPLOAD_SUMMARY
	 */
	int updateBudgetedSummary(@Param("userName") String userName, @Param("id") String id);
	
	/**
	 * This method describes get Final Assortment Data For Excel
	 * @Table FINAL_ASSORTMENT
	 */
	List<FinalAssortment> getFinalAssortmentData();

	/**
	 * This method describes get valid Budgeted Monthly Data For Excel
	 * for user according date range
	 * @Table DEMAND_BUDGETED_MONTHLY
	 */
	List<BudgetedDemandPlanning> getBudgetedMonthlyData(@Param("from") String from,
			@Param("to") String to);

	/**
	 * This method describes get valid Budgeted Weekly Data For Excel
	 * for user according date range
	 * @Table DEMAND_BUDGETED_WEEKLY
	 */
	List<BudgetedDemandPlanning> getBudgetedWeeklyData(@Param("from") String from,
			@Param("to") String to);

	/**
	 * This method describes get last File upload status
	 * user specific
	 * @param User Name
	 * @Table DP_UPLOAD_SUMMARY
	 */
	DpUploadSummary getLastStatus(@Param("userName") String userName);
	
	/**
	 * This method describes TRUNCATE Budgeted Temporary Data
	 * @Table DEMAND_BUDGETED_TEMP
	 */
	@Delete("DELETE FROM DEMAND_BUDGETED_TEMP WHERE USERNAME = #{userName}")
	int truncateTempBudgetedData(@Param("userName") String userName);
	
	/**
	 * This method describes insert record
	 * for successfully Upload Excel File
	 * @Table DP_UPLOAD_SUMMARY
	 */
	int insertDpUploadSummary(DpUploadSummary dpUploadSummary);
	
	List<BudgetedDemandPlanning> getCompareData(@Param("userName") String userName);

	List<BudgetedDemandPlanning> getNotMatchCompareData(@Param("userName") String userName);

	/**
	 * This method describes update File upload status
	 * @Table DP_UPLOAD_SUMMARY
	 */
	int updateDPUploadSummary(@Param("status") String status, @Param("valid") int valid,
			@Param("invalid") int invalid, @Param("url") String url, @Param("id") String id);
	
	/**
	 * This method describes delete matching assortmentCode for
	 * Demand Budgeted Weekly Data
	 * @param User Name
	 * @Table DEMAND_BUDGETED_WEEKLY
	 */
	List<BudgetedDemandPlanning> deleteMatchedAssortmentCodeTempDataForWeeklyBudgeted(
			@Param("userName") String userName);

	/**
	 * This method describes delete matching assortmentCode for
	 * Demand Budgeted Monthly Data
	 * @param User Name
	 * @Table DEMAND_BUDGETED_MONTHLY
	 */
	List<BudgetedDemandPlanning> deleteMatchedAssortmentCodeTempDataForMonthlyBudgeted(
			@Param("userName") String userName);
	
	/**
	 * This method describes batch insert From
	 * valid Demand Budgeted Temporary table 
	 * @param User Name
	 * @Table DEMAND_BUDGETED_TEMP, DEMAND_BUDGETED_WEEKLY
	 */
	int insertMatchBudgetedWeeklyData(@Param("userName") String userName);

	/**
	 * This method describes batch insert From
	 * Invalid Demand Budgeted Temporary table 
	 * @param User Name
	 * @Table DEMAND_BUDGETED_TEMP, DEMAND_BUDGETED_WEEKLY
	 */
	int insertNotMatchBudgetedWeeklyData(@Param("userName") String userName);

	/**
	 * This method describes batch insert From
	 * valid Demand Budgeted Temporary table 
	 * @param User Name
	 * @Table DEMAND_BUDGETED_TEMP, DEMAND_BUDGETED_MONTHLY
	 */
	int insertMatchBudgetedMonthlyData(@Param("userName") String userName);

	/**
	 * This method describes batch insert From
	 * Invalid Demand Budgeted Temporary table 
	 * @param User Name
	 * @Table DEMAND_BUDGETED_TEMP, DEMAND_BUDGETED_MONTHLY
	 */
	int insertNotMatchBudgetedMonthlyData(@Param("userName") String userName);

	/**
	 * This method describes get Budgeted History Information
	 * @param ID
	 * @Table DP_UPLOAD_SUMMARY
	 */
	DpUploadSummary getBudgetedHistoryDataByID(@Param("id") String id);

	int createForecastSchedulerAutoConfig(DemandForecastScheduler forecastScheduler);

	@Select("SELECT ENTERPRISE_USER_NAME as enterpriseUserName,FREQUENCY as frequency, SCHEDULE as schedule,NEXT_SCHEDULE as nextSchedule,BUCKET_NAME as bucketName,BUCKET_KEY as bucketKey FROM DEMAND_FORECAST_AUTOCONFIG WHERE STATUS = 'ACTIVE' AND ENTERPRISE_ID = #{eid}")
	DemandForecastScheduler getForecastSchedulerAutoConfigData(@Param("eid") String eid);

	@Update("UPDATE DEMAND_FORECAST_AUTOCONFIG SET SCHEDULE = #{schedule},NEXT_SCHEDULE=#{nextSchedule} WHERE STATUS = 'ACTIVE' AND ENTERPRISE_ID=#{eid}")
	int updateForecastSchedule(@Param("schedule") String schedule, @Param("nextSchedule") String nextSchedule,
			@Param("eid") String eid);

	@Update("UPDATE DEMAND_FORECAST_AUTOCONFIG SET STATUS = 'INACTIVE' WHERE ENTERPRISE_ID=#{eid}")
	int updateStatusForecastSchedule(@Param("eid") String eid);

	/**
	 * This method describes getting previous Demand Planning
	 * Graph Info
	 * @param Frequency, User Name
	 * @Table DEMAND_PLANNING_VIEWLOG
	 */
	DemandPlanningViewLog getLastReport(@Param("frequency") String frequency,
			@Param("userName") String userName);

	/**
	 * This method describes getting all record
	 * for demand planning Insight history 
	 * @Table DEMAND_PLANNING_VIEWLOG
	 */
	@Select("SELECT COUNT(*) FROM DEMAND_PLANNING_VIEWLOG WHERE FREQUENCY=#{frequency}")
	int getAllHistotyCount(@Param("frequency") String frequency);

	/**
	 * This method describes getting all demand planning Insight
	 * history according to pagination
	 * @Table DEMAND_PLANNING_VIEWLOG
	 */
	List<DemandPlanningViewLog> getAllHistory(@Param("offset") Integer offset,
			@Param("pageSize") Integer pageSize, @Param("frequency") String frequency);

	/**
	 * This method describes getting all filter record for
	 * demand planning Insight history
	 * @param DemandPlanning
	 * @Table DEMAND_PLANNING_VIEWLOG
	 */
	int filterHistoryRecord(@Param("offset") String assortment, @Param("startDate") String startDate,
			@Param("endDate") String endDate, @Param("createdOn") String createdOn,
			@Param("frequency") String frequency);

	/**
	 * This method describes getting all filter for
	 * demand planning Insight history according to pagination
	 * @param DemandPlanning
	 * @Table DEMAND_PLANNING_VIEWLOG
	 */
	List<DemandPlanningViewLog> filterHistory(@Param("offset") Integer offset,
			@Param("pageSize") Integer pageSize, @Param("assortment") String assortment,
			@Param("startDate") String startDate, @Param("endDate") String endDate,
			@Param("createdOn") String createdOn, @Param("frequency") String frequency);

	/**
	 * This method describes getting all Search record for
	 * demand planning Insight history
	 * @param Search
	 * @Table DEMAND_PLANNING_VIEWLOG
	 */
	int searchHistoryRecord(@Param("search") String search, @Param("frequency") String frequency);

	/**
	 * This method describes getting all Search for
	 * demand planning Insight history according to pagination
	 * @param Search
	 * @Table DEMAND_PLANNING_VIEWLOG
	 */
	List<DemandPlanningViewLog> searchHistory(@Param("offset") Integer offset,
			@Param("pageSize") Integer pageSize, @Param("search") String search, @Param("frequency") String frequency);

	/**
	 * This method describes update Forecast download URL
	 * In Graph Area
	 * @param URL, UUID
	 * @Table DEMAND_PLANNING_VIEWLOG
	 */
	@Update("UPDATE DEMAND_PLANNING_VIEWLOG SET DOWNLOAD_URL=#{downloadUrl} WHERE ID=#{uuid}")
	int updatedownloadUrlLogById(@Param("downloadUrl") String downloadUrl, @Param("uuid") String uuid);

	/**
	 * This method describes get last Forecast
	 * Information where status is not Processing
	 * @Table DEMAND_PLANNING
	 */
	@Select("SELECT * FROM DEMAND_PLANNING WHERE STATUS!='Processing' ORDER BY CREATEDON DESC FETCH FIRST 1 ROWS ONLY")
	DemandPlanning getLastSummaryData();

	/**
	 * This method describes get Running Forecast
	 * ID where status is Processing
	 * @Table DEMAND_PLANNING
	 */
	@Select("SELECT RUNID FROM DEMAND_PLANNING WHERE STATUS='Processing'")
	String getRunningId();

	/**
	 * This method describes get last 5 Succeeded Forecast
	 * Run Time
	 * @Table DEMAND_PLANNING
	 */
	@Select("SELECT DURATION FROM DEMAND_PLANNING WHERE STATUS='Succeeded' AND DURATION IS NOT NULL ORDER BY CREATEDON DESC FETCH FIRST 5 ROWS ONLY")
	List<String> fiveSucceededTime();

	/**
	 * This method describes update MV refresh status
	 * according to parameter
	 * @Table MVIEW_REFRESH_HISTORY
	 */
	@Update("UPDATE MVIEW_REFRESH_HISTORY SET STATUS = #{status,jdbcType=VARCHAR},ACTIVE=#{active,jdbcType=VARCHAR},FORECAST_READ_STATUS=#{forecastReadStatus,jdbcType=VARCHAR} WHERE MODULE='INVENTRY_PLANNING' AND SUBMODULE='ASSORTMENT'")
	int updateMVStatus(@Param("status") String status, @Param("active") char active,
			@Param("forecastReadStatus") String forecastReadStatus);

//	@Select("SELECT DB_READ_STATUS FROM DEMAND_PLANNING WHERE STATUS='Processing'")
//	String getStatus();
	
	/**
	 * This method describes update processing status to Failed
	 * due to processing time take more than expecting time
	 * @Table DEMAND_PLANNING
	 */
	@Update("UPDATE DEMAND_PLANNING SET STATUS='Failed',DURATION='3000' WHERE STATUS='Processing'")
	int updateProcessingToFailed();

	/**
	 * This method describes refresh OTB FORECAST MONTHLY
	 * after Succeeded the forecast Job
	 * @MaterializedView OTB_FORECAST_MONTHLY
	 */
	@Select(value= "CALL DBMS_MVIEW.REFRESH('OTB_FORECAST_MONTHLY','?')")
	@Options(statementType = StatementType.CALLABLE,fetchSize=1000)
	void refreshMView();
	
	/**
	 * This method describes refresh Actual Data materialized view
	 * before Running the forecast Job
	 * @MaterializedView DEMAND_ASSORTMENT_MONTHLY, DEMAND_ASSORTMENT_WEEKLY
	 */
	@Select(value= "CALL DBMS_MVIEW.REFRESH('DEMAND_ASSORTMENT_MONTHLY,DEMAND_ASSORTMENT_WEEKLY','?')")
	@Options(statementType = StatementType.CALLABLE,fetchSize=1000)
	void refreshActualDataMView();
	
	/**
	 * This method describes get last Forecast Assortment
	 * @Table DEMAND_PLANNING
	 */
	@Select("SELECT ASSORTMENTCODE FROM ASSORTMENT_VIEWLOG WHERE STATUS='SUCCEEDED' ORDER by CREATEDON DESC FETCH FIRST ROWS ONLY")
	String getForcastOnAssortmentCode();
	
	/**
	 * This method describes get all previous Forecast Assortment record
	 * according to parameter
	 * @Table DEMAND_PLANNING
	 */
	int assortmentHistoryRecord(@Param("startDate") String startDate,@Param("endDate") String endDate,@Param("frequency") String frequency);
	
	/**
	 * This method describes get all previous Forecast Assortment list
	 * according to pagination and parameter
	 * @Table DEMAND_PLANNING
	 */
	List<String> getAssortmentHistory(@Param("offset") Integer offset,
			@Param("pageSize") Integer pageSize,@Param("startDate") String startDate,@Param("endDate") String endDate,@Param("frequency") String frequency);
	
	/**
	 * This method describes get previous Forecast Assortment search 
	 * record according to pagination and parameter
	 * @Table DEMAND_PLANNING
	 */
	int assortmentSearchHistoryRecord(@Param("search") String search,@Param("startDate") String startDate,@Param("endDate") String endDate,@Param("frequency") String frequency);
	
	/**
	 * This method describes get previous Forecast Assortment search
	 * according to pagination and parameter
	 * @Table DEMAND_PLANNING
	 */
	List<String> assortmentHistorySearch(@Param("offset") Integer offset,
			@Param("pageSize") Integer pageSize, @Param("search") String search,@Param("startDate") String startDate,@Param("endDate") String endDate,@Param("frequency") String frequency);
	
	/**
	 * This method describes get last succeeded Forecast ID
	 * @Table DEMAND_PLANNING
	 */
	@Select("SELECT RUNID FROM DEMAND_PLANNING WHERE STATUS='Succeeded' ORDER by CREATEDON DESC FETCH FIRST ROWS ONLY")
	String getLastRunId();
}
