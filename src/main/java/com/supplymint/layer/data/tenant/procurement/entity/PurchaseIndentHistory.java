package com.supplymint.layer.data.tenant.procurement.entity;

import java.time.OffsetDateTime;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.supplymint.util.LocalDatetimeDeserializer;
import com.supplymint.util.PILocalDatetimeSerializer;

public class PurchaseIndentHistory {

	private String hl1Name;
	private String hl2Name;
	private String hl3Name;
	private String slName;
	private String slCityName;
	private String pattern;
	private String orgId;

	private String orderDetailId;
	private int orderId;
	private String hl4Code;
	private String hl4Name;

	private String desc2Code;
	private String desc2Name;
	private String pdfPath;

	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	private OffsetDateTime deliveryDate;

	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	private OffsetDateTime createdTime;

	private String status;

	public PurchaseIndentHistory() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PurchaseIndentHistory(String hl1Name, String hl2Name, String hl3Name, String slName, String slCityName,
			String pattern, String orderDetailId, int orderId, String hl4Code, String hl4Name, String desc2Code,
			String desc2Name, OffsetDateTime deliveryDate, OffsetDateTime createdTime, String status, String path,
			String orgId) {
		super();
		this.hl1Name = hl1Name;
		this.hl2Name = hl2Name;
		this.hl3Name = hl3Name;
		this.slName = slName;
		this.slCityName = slCityName;
		this.pattern = pattern;
		this.orderDetailId = orderDetailId;
		this.orderId = orderId;
		this.hl4Code = hl4Code;
		this.hl4Name = hl4Name;
		this.desc2Code = desc2Code;
		this.desc2Name = desc2Name;
		this.deliveryDate = deliveryDate;
		this.createdTime = createdTime;
		this.status = status;
		this.pdfPath = path;
		this.orgId = orgId;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getHl1Name() {
		return hl1Name;
	}

	public void setHl1Name(String hl1Name) {
		this.hl1Name = hl1Name;
	}

	public String getHl2Name() {
		return hl2Name;
	}

	public void setHl2Name(String hl2Name) {
		this.hl2Name = hl2Name;
	}

	public String getHl3Name() {
		return hl3Name;
	}

	public void setHl3Name(String hl3Name) {
		this.hl3Name = hl3Name;
	}

	public String getSlName() {
		return slName;
	}

	public void setSlName(String slName) {
		this.slName = slName;
	}

	public String getSlCityName() {
		return slCityName;
	}

	public void setSlCityName(String slCityName) {
		this.slCityName = slCityName;
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	public String getOrderDetailId() {
		return orderDetailId;
	}

	public void setOrderDetailId(String orderDetailId) {
		this.orderDetailId = orderDetailId;
	}

	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public String getHl4Code() {
		return hl4Code;
	}

	public void setHl4Code(String hl4Code) {
		this.hl4Code = hl4Code;
	}

	public String getHl4Name() {
		return hl4Name;
	}

	public void setHl4Name(String hl4Name) {
		this.hl4Name = hl4Name;
	}

	public String getDesc2Code() {
		return desc2Code;
	}

	public void setDesc2Code(String desc2Code) {
		this.desc2Code = desc2Code;
	}

	public String getDesc2Name() {
		return desc2Name;
	}

	public void setDesc2Name(String desc2Name) {
		this.desc2Name = desc2Name;
	}

	public OffsetDateTime getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(OffsetDateTime deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public OffsetDateTime getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(OffsetDateTime createdTime) {
		this.createdTime = createdTime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPdfPath() {
		return pdfPath;
	}

	public void setPdfPath(String pdfPath) {
		this.pdfPath = pdfPath;
	}

	@Override
	public String toString() {
		return "PurchaseIndentHistory [hl1Name=" + hl1Name + ", hl2Name=" + hl2Name + ", hl3Name=" + hl3Name
				+ ", slName=" + slName + ", slCityName=" + slCityName + ", pattern=" + pattern + ", orgId=" + orgId
				+ ", orderDetailId=" + orderDetailId + ", orderId=" + orderId + ", hl4Code=" + hl4Code + ", hl4Name="
				+ hl4Name + ", desc2Code=" + desc2Code + ", desc2Name=" + desc2Name + ", pdfPath=" + pdfPath
				+ ", deliveryDate=" + deliveryDate + ", createdTime=" + createdTime + ", status=" + status + "]";
	}

}
