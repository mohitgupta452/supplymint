package com.supplymint.layer.data.tenant.dashboard.entity;

import java.time.OffsetDateTime;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.supplymint.util.LocalDatetimeDeserializer;
import com.supplymint.util.PILocalDatetimeSerializer;

public class StoreArticleRank {

	public String id;
	public String code;
	public String name;
	public String type;
	public String rank;
	public String lastYearSV;
	public String currentYearSV;
	public String lastMonthSV;
	public String pyLastMonthSV;
	public String status;
	public String lastYearSign;
	public String currentYearSign;
	public String lastMonthSign;
	public String prevYearLastMonthSign;

	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	public OffsetDateTime createdOn;
	public String createdBy;

	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	public OffsetDateTime updatedOn;
	public String updatedBy;
	public String ipaddress;
	public String week;
	public String month;
	public String year;

	public StoreArticleRank() {
		super();
	}

	public StoreArticleRank(String id, String code, String name, String type, String rank, String lastYearSV,
			String currentYearSV, String lastMonthSV, String pyLastMonthSV, String status, String lastYearSign,
			String currentYearSign, String lastMonthSign, String prevYearLastMonthSign, OffsetDateTime createdOn,
			String createdBy, OffsetDateTime updatedOn, String updatedBy, String ipaddress, String week, String month,
			String year) {
		super();
		this.id = id;
		this.code = code;
		this.name = name;
		this.type = type;
		this.rank = rank;
		this.lastYearSV = lastYearSV;
		this.currentYearSV = currentYearSV;
		this.lastMonthSV = lastMonthSV;
		this.pyLastMonthSV = pyLastMonthSV;
		this.status = status;
		this.lastYearSign = lastYearSign;
		this.currentYearSign = currentYearSign;
		this.lastMonthSign = lastMonthSign;
		this.prevYearLastMonthSign = prevYearLastMonthSign;
		this.createdOn = createdOn;
		this.createdBy = createdBy;
		this.updatedOn = updatedOn;
		this.updatedBy = updatedBy;
		this.ipaddress = ipaddress;
		this.week = week;
		this.month = month;
		this.year = year;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getRank() {
		return rank;
	}

	public void setRank(String rank) {
		this.rank = rank;
	}

	public String getLastYearSV() {
		return lastYearSV;
	}

	public void setLastYearSV(String lastYearSV) {
		this.lastYearSV = lastYearSV;
	}

	public String getCurrentYearSV() {
		return currentYearSV;
	}

	public void setCurrentYearSV(String currentYearSV) {
		this.currentYearSV = currentYearSV;
	}

	public String getLastMonthSV() {
		return lastMonthSV;
	}

	public void setLastMonthSV(String lastMonthSV) {
		this.lastMonthSV = lastMonthSV;
	}

	public String getPyLastMonthSV() {
		return pyLastMonthSV;
	}

	public void setPyLastMonthSV(String pyLastMonthSV) {
		this.pyLastMonthSV = pyLastMonthSV;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getLastYearSign() {
		return lastYearSign;
	}

	public void setLastYearSign(String lastYearSign) {
		this.lastYearSign = lastYearSign;
	}

	public String getCurrentYearSign() {
		return currentYearSign;
	}

	public void setCurrentYearSign(String currentYearSign) {
		this.currentYearSign = currentYearSign;
	}

	public String getLastMonthSign() {
		return lastMonthSign;
	}

	public void setLastMonthSign(String lastMonthSign) {
		this.lastMonthSign = lastMonthSign;
	}

	public String getPrevYearLastMonthSign() {
		return prevYearLastMonthSign;
	}

	public void setPrevYearLastMonthSign(String prevYearLastMonthSign) {
		this.prevYearLastMonthSign = prevYearLastMonthSign;
	}

	public OffsetDateTime getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(OffsetDateTime createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public OffsetDateTime getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(OffsetDateTime updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getIpaddress() {
		return ipaddress;
	}

	public void setIpaddress(String ipaddress) {
		this.ipaddress = ipaddress;
	}

	public String getWeek() {
		return week;
	}

	public void setWeek(String week) {
		this.week = week;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	@Override
	public String toString() {
		return "StoreArticleRank [id=" + id + ", code=" + code + ", name=" + name + ", type=" + type + ", rank=" + rank
				+ ", lastYearSV=" + lastYearSV + ", currentYearSV=" + currentYearSV + ", lastMonthSV=" + lastMonthSV
				+ ", pyLastMonthSV=" + pyLastMonthSV + ", status=" + status + ", lastYearSign=" + lastYearSign
				+ ", currentYearSign=" + currentYearSign + ", lastMonthSign=" + lastMonthSign
				+ ", prevYearLastMonthSign=" + prevYearLastMonthSign + ", createdOn=" + createdOn + ", createdBy="
				+ createdBy + ", updatedOn=" + updatedOn + ", updatedBy=" + updatedBy + ", ipaddress=" + ipaddress
				+ ", week=" + week + ", month=" + month + ", year=" + year + "]";
	}

}
