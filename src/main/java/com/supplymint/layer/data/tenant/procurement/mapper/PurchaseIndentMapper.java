package com.supplymint.layer.data.tenant.procurement.mapper;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.mapping.StatementType;

import com.supplymint.layer.data.tenant.administration.entity.ADMItem;
import com.supplymint.layer.data.tenant.procurement.entity.CatDescMapping;
import com.supplymint.layer.data.tenant.procurement.entity.CatDescMaster;
import com.supplymint.layer.data.tenant.procurement.entity.CatMaster;
import com.supplymint.layer.data.tenant.procurement.entity.DeptItemUDFSettings;
import com.supplymint.layer.data.tenant.procurement.entity.FinCharge;
import com.supplymint.layer.data.tenant.procurement.entity.HLevelData;
import com.supplymint.layer.data.tenant.procurement.entity.HsnData;
import com.supplymint.layer.data.tenant.procurement.entity.HsnGstData;
import com.supplymint.layer.data.tenant.procurement.entity.ItemValidation;
import com.supplymint.layer.data.tenant.procurement.entity.PurchaseIndent;
import com.supplymint.layer.data.tenant.procurement.entity.PurchaseIndentDetail;
import com.supplymint.layer.data.tenant.procurement.entity.PurchaseIndentHistory;
import com.supplymint.layer.data.tenant.procurement.entity.PurchaseIndentProperty;
import com.supplymint.layer.data.tenant.procurement.entity.PurchaseTermData;
import com.supplymint.layer.data.tenant.procurement.entity.PurtermCharge;
import com.supplymint.layer.data.tenant.procurement.entity.PurtermDet;
import com.supplymint.layer.data.tenant.procurement.entity.PurtermMaster;
import com.supplymint.layer.data.tenant.procurement.entity.SupplierData;
import com.supplymint.layer.data.tenant.procurement.entity.Transporters;

@Mapper
public interface PurchaseIndentMapper {

	@Select("select * from HLEVELDATA OFFSET #{offset} ROWS FETCH	NEXT #{pagesize} ROWS ONLY")
	@Options(fetchSize = 1000, timeout = 360000)
	List<HLevelData> findDistinctHierarachyLevels(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize);

	@Select("select COUNT(*) from HLEVELDATA")
	@Options(fetchSize = 1000, timeout = 360000)
	Integer recordDistinctHierarachyLevels();

	@Select("SELECT SLCODE,TRANSPORTERCODE,TRANSPORTERNAME FROM transporters WHERE SLCODE = #{slCode} AND IS_EXT = 'N' OFFSET #{offset} ROWS FETCH	NEXT #{pagesize} ROWS ONLY")
	@Options(fetchSize = 1000, timeout = 360000)
	List<Transporters> getTransportersBySlcode(@Param("slCode") String slCode, @Param("offset") Integer offset,
			@Param("pagesize") Integer pagesize);

	@Select("SELECT COUNT(*) FROM transporters WHERE SLCODE = #{slCode} AND IS_EXT = 'N'")
	@Options(fetchSize = 1000, timeout = 360000)
	Integer getRecordTransportersBySlcode(@Param("slCode") String slCode);

	List<HsnGstData> getHsnGstDataByHsnSacCode(@Param("hsnSacCode") String hsnSacCode, @Param("piDate") String piDate);

	HsnGstData getMaxEffectiveDateByHsnSacCode(@Param("hsnSacCode") String hsnSacCode, @Param("piDate") String piDate);

	@Select("select MAX(EFFECTIVE_DATE) EFFECTIVE_DATE FROM HSNGSTDATA WHERE HSN_SAC_CODE = #{hsnSacCode} AND\n"
			+ "		 AMOUNTFROM &lt;= #{transactionPrice} ")
	HsnGstData getGstSlabByTransactionPrice(@Param("hsnSacCode") Integer hsnSacCode,
			@Param("transactionPrice") Double transactionPrice);

	@Select("select MAX(AMOUNTFROM) AMOUNTFROM FROM HSNGSTDATA WHERE HSN_SAC_CODE = #{hsnSacCode} AND\n"
			+ "		 EFFECTIVE_DATE = TO_DATE(#{piDate},'DD-MON-YY') ")
	HsnGstData getMaxAmountFrom(@Param("hsnSacCode") String hsnSacCode, @Param("piDate") String piDate);

	@Select("select MIN(AMOUNTFROM) AMOUNTFROM FROM HSNGSTDATA WHERE HSN_SAC_CODE = #{hsnSacCode} AND\n"
			+ "		 EFFECTIVE_DATE = TO_DATE(#{piDate},'DD-MON-YY') ")
	HsnGstData getMinAmountFrom(@Param("hsnSacCode") String hsnSacCode, @Param("piDate") String piDate);

	@Select("select * FROM HSNGSTDATA WHERE HSN_SAC_CODE = #{hsnSacCode} AND\n"
			+ "		AMOUNTFROM = #{transactionPrice} AND EFFECTIVE_DATE = TO_DATE(#{piDate},'DD-MON-YY') ")
	HsnGstData getGstSlabUsingTransactionPrice(@Param("hsnSacCode") String hsnSacCode, @Param("piDate") String piDate,
			@Param("transactionPrice") Double transactionPrice);

	List<SupplierData> getSupplierData(@Param("department") String department, @Param("siteCode") String siteCode,
			@Param("offset") Integer offset, @Param("pagesize") Integer pagesize);

	@Select("SELECT count(*) FROM SUPPLIERDATA sd join finsl_ou fo on sd.slcode = fo.slcode WHERE HL3CODE = #{departmentCode} and admsite_code = #{siteCode}")
	@Options(fetchSize = 1000, timeout = 360000)
	Integer getRecordSupplierData(@Param("departmentCode") String departmentCode, @Param("siteCode") String siteCode);

	Integer getSupplierLeadTime(@Param("slCode") String slCode);

	@Select("SELECT * FROM PURCH_TERMMAIN WHERE FINTRADEGRP_CODE = #{tradeGrpCode} AND EXT = 'N'")
	@Options(fetchSize = 1000, timeout = 360000)
	List<PurtermMaster> getPurchaseTermDataForCGST(@Param("tradeGrpCode") String tradeGrpCode);

	@Select("SELECT * FROM PURCH_TERMMAIN WHERE FINTRADEGRP_CODE != 1 AND EXT = 'N'")
	@Options(fetchSize = 1000, timeout = 360000)
	List<PurtermMaster> getPurchaseTermDataForIgst(@Param("tradeGrpCode") String tradeGrpCode);

	@Select("SELECT * FROM PURCH_TERMMAIN WHERE EXT = 'N' ")
	List<PurtermMaster> getAllPurchaseTerms();

	@Select("SELECT * FROM PURTERMDET WHERE PURTERMMAIN_CODE = #{purchaseTermCode} AND CHGCODE =#{chgCode} "
			+ "AND OPERATION_LEVEL = 'L' order by SEQ")
	PurtermDet getPurchaseTermDetailsByChgCode(@Param("purchaseTermCode") Integer purchaseTermCode,
			@Param("chgCode") BigDecimal chgCode);

	List<PurtermCharge> getPurchaseTermChargeDetailByPurtermCode(Integer purchaseTermCode);

	List<ADMItem> getArticlePricePoint(@Param("slCode") String slCode, @Param("department") String department,
			@Param("offset") Integer offset, @Param("pagesize") Integer pagesize);

	Integer getRecordArticlePricePoint(@Param("slCode") String slCode, @Param("department") String department);

	@Select("SELECT code, cname,hl3Name FROM CAT_COLOR WHERE HL3CODE = #{hl3Code} AND CNAME IS NOT NULL OFFSET #{offset} ROWS FETCH NEXT #{pagesize} ROWS ONLY")
	List<CatDescMaster> getAllColors(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("hl3Code") String hl3Code);

	@Select("SELECT COUNT(*) FROM CAT_COLOR WHERE hl4Code = #{hl4Code}")
	Integer getRecordAllColors(String hl4Code);

	List<CatDescMaster> getAllUniqueCodeByArticleCode(@Param("offset") Integer offset,
			@Param("pagesize") Integer pagesize, @Param("hl3Code") String hl3Code);

	@Select("SELECT COUNT(*)  FROM CAT_UNIQUECODE WHERE HL3CODE = #{hl3Code} AND CNAME IS NOT NULL")
	Integer getRecordAllUniqueCodeByArticleCode(String hl3Code);

	@Select("SELECT code,cname,hl3Name FROM CAT_BRAND WHERE HL3CODE = #{hl3Code} AND CNAME IS NOT NULL OFFSET #{offset} ROWS FETCH NEXT #{pagesize} ROWS ONLY")
	List<CatDescMaster> getAllBrandsByArticleCode(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("hl3Code") String hl3Code);

	@Select("SELECT COUNT(*) FROM CAT_BRAND WHERE HL3CODE = #{hl3Code} AND CNAME IS NOT NULL")
	Integer getRecordAllBrandsByArticleCode(String hl3Code);

	@Select("SELECT code,cname,hl3Name FROM CAT_PATTERN WHERE HL3CODE = #{hl3Code} AND CNAME IS NOT NULL  OFFSET #{offset} ROWS FETCH NEXT #{pagesize} ROWS ONLY")
	List<CatDescMaster> getAllPatternsByArticleCode(@Param("offset") Integer offset,
			@Param("pagesize") Integer pagesize, @Param("hl3Code") String hl3Code);

	@Select("SELECT COUNT(*) FROM CAT_PATTERN WHERE HL3CODE = #{hl3Code} AND CNAME IS NOT NULL")
	Integer getRecordAllPatternsByArticleCode(String hl3Code);

	@Select("SELECT code,cname FROM CAT_ASSORTMENT WHERE HL3CODE = #{hl3Code} AND CNAME IS NOT NULL  OFFSET #{offset} ROWS FETCH NEXT #{pagesize} ROWS ONLY")
	List<CatDescMaster> getAllAssortmentsByArticleCode(@Param("offset") Integer offset,
			@Param("pagesize") Integer pagesize, @Param("hl3Code") String hl3Code);

	@Select("SELECT COUNT(*) FROM CAT_ASSORTMENT WHERE HL3CODE = #{hl3Code} AND CNAME IS NOT NULL")
	Integer getRecordAllAssortmentsByArticleCode(String hl3Code);

	@Select("SELECT CODE as code, CNAME as cname FROM cat_size  WHERE HL3CODE = #{hl3Code} AND CNAME IS NOT NULL order by to_number(orderby) OFFSET #{offset} ROWS FETCH NEXT #{pagesize} ROWS ONLY")
	List<CatDescMaster> getAllSizeByArticleCode(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("hl3Code") String hl3Code);

	@Select("SELECT CODE as code, CNAME as cname FROM DEPT_SIZE_DATA  WHERE HL3NAME = #{hl3Name} AND CNAME IS NOT NULL order by to_number(orderby) OFFSET #{offset} ROWS FETCH NEXT #{pagesize} ROWS ONLY")
	List<CatDescMaster> getOtherAllSizeByArticleCode(@Param("offset") Integer offset,
			@Param("pagesize") Integer pagesize, @Param("hl3Name") String hl3Name);

	@Select("SELECT COUNT(*) FROM CAT_SIZE WHERE HL3CODE = #{hl3Code} AND CNAME IS NOT NULL")
	Integer getRecordAllSizeByArticleCode(@Param("hl3Code") String hl3Code);

	@Select("SELECT COUNT(*) FROM DEPT_SIZE_DATA WHERE HL3NAME = #{hl3Name} AND CNAME IS NOT NULL")
	Integer getOtherRecordAllSizeByArticleCode(String hl3Name);

	List<ADMItem> getArticleData(String hl4Code);

	@Select("SELECT hl3name,description FROM DESC_SEASONCODE WHERE HL3CODE = #{hl3Code} AND DESCRIPTION IS NOT NULL OFFSET #{offset} ROWS FETCH NEXT #{pagesize} ROWS ONLY")
	List<CatDescMaster> getAllSeasonByArticleCode(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("hl3Code") String hl3Code);

	@Select("SELECT COUNT(*) FROM DESC_SEASONCODE WHERE HL3CODE = #{hl3Code} AND DESCRIPTION IS NOT NULL")
	Integer getRecordAllSeasonByArticleCode(String hl3Code);

	@Select("SELECT hl3Name,description FROM DESC_FABRIC WHERE hl3Code = #{hl3Code} AND DESCRIPTION IS NOT NULL OFFSET #{offset} ROWS FETCH NEXT #{pagesize} ROWS ONLY")
	List<CatDescMaster> getAllFabricByArticleCode(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("hl3Code") String hl3Code);

	@Select("SELECT COUNT(*) FROM DESC_FABRIC WHERE hl3Code = #{hl3Code} AND DESCRIPTION IS NOT NULL")
	Integer getRecordAllFabricByArticleCode(String hl3Code);

	@Select("SELECT hl3Name,description FROM DESC_DRAWINCODE WHERE hl3Code = #{hl3Code} AND DESCRIPTION IS NOT NULL OFFSET #{offset} ROWS FETCH NEXT #{pagesize} ROWS ONLY")
	List<CatDescMaster> getAllDarwinByArticleCode(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("hl3Code") String hl3Code);

	@Select("SELECT COUNT(*) FROM DESC_DRAWINCODE WHERE hl3Code = #{hl3Code} AND DESCRIPTION IS NOT NULL")
	Integer getRecordAllDarwinByArticleCode(String hl3Code);

	@Select("SELECT hl3Name,description FROM DESC_WEAVED WHERE hl3Code = #{hl3Code} AND DESCRIPTION IS NOT NULL  OFFSET #{offset} ROWS FETCH NEXT #{pagesize} ROWS ONLY")
	List<CatDescMaster> getAllWeavedByArticleCode(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("hl3Code") String hl3Code);

	@Select("SELECT COUNT(*) FROM DESC_WEAVED WHERE hl3Code = #{hl3Code} AND DESCRIPTION IS NOT NULL")
	Integer getRecordAllWeavedByArticleCode(String hl3Code);

	int validateItem(ItemValidation item);

	Integer createPI(PurchaseIndent item);

	Integer createPIDetails(PurchaseIndentDetail items);

	Integer createItem(ADMItem item);

	Integer createFinCharge(FinCharge charge);

	List<ADMItem> searchByArticlePricePoint(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("slCode") String slCode, @Param("articleName") String articleName,
			@Param("department") String department, @Param("costPrice") String costPrice,
			@Param("sellPrice") String sellPrice);

	Integer recordSearchByArticlePricePoint(@Param("slCode") String slCode, @Param("articleName") String articleName,
			@Param("department") String department, @Param("costPrice") String costPrice,
			@Param("sellPrice") String sellPrice);

	List<ADMItem> searchArticlePricePoint(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("slCode") String slCode, @Param("department") String department, @Param("search") String search);

	Integer recordArticlePricePoint(@Param("slCode") String slCode, @Param("department") String department,
			@Param("search") String search);

	List<Transporters> searchTransportersBySlcode(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("slCode") String slCode, @Param("transporterName") String transporterName);

	Integer recordTransportersBySlcode(@Param("slCode") String slCode,
			@Param("transporterName") String transporterName);

	List<CatDescMaster> searchAllColors(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("hl3Code") String hl3Code, @Param("search") String search);

	Integer recordAllColors(@Param("hl3Code") String hl3Code, @Param("search") String search);

	List<CatDescMaster> searchAllBrandsByArticleCode(@Param("offset") Integer offset,
			@Param("pagesize") Integer pagesize, @Param("hl3Code") String hl3Code, @Param("search") String search);

	Integer recordAllBrandsByArticleCode(@Param("hl3Code") String hl3Code, @Param("search") String search);

	List<CatDescMaster> searchAllPatternsByArticleCode(@Param("offset") Integer offset,
			@Param("pagesize") Integer pagesize, @Param("hl3Code") String hl3Code, @Param("search") String search);

	Integer recordAllPatternsByArticleCode(@Param("hl3Code") String hl3Code, @Param("search") String search);

	List<CatDescMaster> searchAllUniqueCodeByArticleCode(@Param("offset") Integer offset,
			@Param("pagesize") Integer pagesize, @Param("hl3Code") String hl3Code, @Param("search") String search);

	Integer recordAllUniqueCodeByArticleCode(@Param("hl3Code") String hl3Code, @Param("search") String search);

	List<CatDescMaster> searchAllAssortmentsByArticleCode(@Param("offset") Integer offset,
			@Param("pagesize") Integer pagesize, @Param("hl3Code") String hl3Code, @Param("search") String search);

	Integer recordAllAssortmentsByArticleCode(@Param("hl3Code") String hl3Code, @Param("search") String search);

	List<CatDescMaster> searchAllSizeByArticleCode(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("hl3Name") String hl3Name, @Param("search") String search);

	Integer recordAllSizeByArticleCode(@Param("hl3Name") String hl3Name, @Param("search") String search);

	List<CatDescMaster> searchAllSeasonByArticleCode(@Param("offset") Integer offset,
			@Param("pagesize") Integer pagesize, @Param("hl3Code") String hl3Code, @Param("search") String search);

	Integer recordAllSeasonByArticleCode(@Param("hl3Code") String hl3Code, @Param("search") String search);

	List<CatDescMaster> searchAllFabricByArticleCode(@Param("offset") Integer offset,
			@Param("pagesize") Integer pagesize, @Param("hl3Code") String hl3Code, @Param("search") String search);

	Integer recordAllFabricByArticleCode(@Param("hl3Code") String hl3Code, @Param("search") String search);

	List<CatDescMaster> searchAllDarwinByArticleCode(@Param("offset") Integer offset,
			@Param("pagesize") Integer pagesize, @Param("hl3Code") String hl3Code, @Param("search") String search);

	Integer recordAllDarwinByArticleCode(@Param("hl3Code") String hl3Code, @Param("search") String search);

	List<CatDescMaster> searchAllWeavedByArticleCode(@Param("offset") Integer offset,
			@Param("pagesize") Integer pagesize, @Param("hl3Code") String hl3Code, @Param("search") String search);

	Integer recordAllWeavedByArticleCode(@Param("hl3Code") String hl3Code, @Param("search") String search);

	List<HLevelData> searchByDistinctHierarachyLevels(@Param("offset") Integer offset,
			@Param("pagesize") Integer pagesize, @Param("hl1name") String hl1name, @Param("hl2name") String hl2name,
			@Param("hl3name") String hl3name);

	Integer recordSearchByDistinctHierarachyLevels(@Param("hl1name") String hl1name, @Param("hl2name") String hl2name,
			@Param("hl3name") String hl3name);

	@Select("select distinct detailjson from pidet where detailjson is not null ORDER BY CREATEDTIME DESC  OFFSET #{offset} ROWS FETCH NEXT #{pagesize} ROWS ONLY")
	List<String> findAll(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize);

	@Select("SELECT DISTINCT DETAILJSON FROM PIDET WHERE ORDERID=#{orderId} AND ORDERDETAILID = #{orderDetailId} and rownum = 1")
	String findPIJSONByPattern(@Param("orderId") String orderId, @Param("orderDetailId") String orderDetailId);

	@Select("select count(*) from (\n"
			+ "select pi.pattern, pi.hl1name, pi.hl2name, pi.hl3name, pi.slname, pi.slcityname,  pid. deliverydate,pid.orderid, pid.orderdetailid, pid.hl4code, pid.createdtime, pid.status, pid.desc2code, pid.desc2name, pid.path,pi.org_id from pimain pi join  (select distinct orderid, orderdetailid, deliverydate, hl4code, createdtime, status, desc2code, desc2name, path from pidet) pid  on pi.id = pid.orderid) WHERE ORG_ID=#{orgId}")
	Integer record(@Param("orgId") String orgId);

	List<PurchaseIndentHistory> getPIHistory(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("orgId") String orgId);

	List<PurchaseIndentHistory> getLoadIndent(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize);

	Integer countLoadIndent();

	List<SupplierData> searchBySupplierData(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("slCode") String slCode, @Param("slName") String slName, @Param("slAddr") String slAddr,
			@Param("department") String department);

	Integer recordsearchBySupplierData(@Param("slCode") String slCode, @Param("slName") String slName,
			@Param("slAddr") String slAddr, @Param("departmentName") String departmentName);

	List<SupplierData> searchSupplierData(@Param("department") String department, @Param("siteCode") String siteCode,
			@Param("offset") Integer offset, @Param("pagesize") Integer pagesize, @Param("search") String search);

	Integer recordSearchSupplierData(@Param("department") String department, @Param("siteCode") String siteCode,
			@Param("search") String search);

	List<HLevelData> searchAllDistinctHierarachyLevels(@Param("offset") Integer offset,
			@Param("pagesize") Integer pagesize, @Param("search") String search);

	Integer recordALLSearchDistinctHierarachyLevels(@Param("search") String search);

	@Select("SELECT ORDERDETAILID FROM PIDET ORDER BY ID DESC FETCH FIRST 1 ROWS ONLY")
	String getLastPattern();

	@Select("SELECT CODE,NAME FROM PURCHASE_TERM_DATA WHERE SLCODE = #{slCode}")
	PurchaseTermData getPurchaseTermDataBySlCode(@Param("slCode") Integer slCode);

	PurchaseIndent getPIByPattern(String pattern);

	Integer updatePDFPath(@Param("pattern") String pattern, @Param("path") String path);

	@Select("SELECT PATH FROM PIMAIN WHERE PATTERN = #{pattern}")
	String getPDFPath(@Param("pattern") String pattern);

	Integer updateStatus(@Param("status") String status, @Param("pattern") String pattern,
			@Param("ipAddress") String ipAddress, @Param("updationTime") OffsetDateTime updationTime,
			@Param("updatedBy") String updatedBy);

	@Select("SELECT COUNT(*) FROM CAT_BRAND WHERE CODE=#{code} AND CNAME =#{cname} AND HL4CODE = #{hl4Code}")
	Integer countBrand(CatMaster catMaster);

	@Select("SELECT COUNT(*) FROM CAT_COLOR WHERE HL3CODE = #{hl3Code} AND CNAME IS NOT NULL")
	Integer countColor(@Param("hl3Code") String hl3Code);

	@Select("SELECT COUNT(*) FROM CAT_SIZE WHERE CODE={code} AND CNAME ={cname} AND HL3CODE = {hl3Code}")
	Integer countSize(@Param("hl3Code") String hl3Code);

	@Select("SELECT COUNT(*) FROM CAT_UNIQUECODE WHERE CODE={code} AND CNAME ={cname} AND HL4CODE = {hl4Code}")
	Integer countUniqueCode(CatMaster catMaster);

	@Insert("INSERT into CAT_BRAND(CODE,CNAME, HL4CODE) VALUES(#{code}, #{cname}, #{hl4Code})")
	Integer insertBrand(CatMaster catMaster);

	@Insert("INSERT into CAT_COLOR(CODE,CNAME, HL4CODE) VALUES(#{code}, #{cname}, #{hl4Code})")
	Integer insertColor(CatMaster catMaster);

	@Insert("INSERT into CAT_SIZE(CODE,CNAME, HL4CODE) VALUES(#{code}, #{cname}, #{hl4Code})")
	Integer insertSize(CatMaster catMaster);

	@Insert("INSERT into CAT_UNIQUECODE(CODE,CNAME, HL4CODE) VALUES(#{code}, #{cname}, #{hl4Code})")
	Integer insertUniqueCode(CatMaster catMaster);

	List<PurchaseIndent> filter(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("pattern") String pattern, @Param("supplierName") String supplierName,
			@Param("articleCode") String articleCode, @Param("division") String division,
			@Param("status") String status, @Param("section") String section, @Param("department") String department,
			@Param("generatedDate") String generatedDate);

	Integer filterRecord(@Param("pattern") String pattern, @Param("supplierName") String supplierName,
			@Param("articleCode") String articleCode, @Param("division") String division,
			@Param("status") String status, @Param("section") String section, @Param("department") String department,
			@Param("generatedDate") String generatedDate);

	List<PurchaseIndent> search(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("search") String search);

	Integer searchRecord(@Param("search") String search);

	PurchaseIndentProperty getPIMainProperty(@Param("id") int id);

	List<Transporters> getAllTransporters(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize);

	Integer getAllTransportersRecord();

	List<Transporters> getAllFilterTransporters(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("transporterName") String transporterName, @Param("transporterCode") String transporterCode);

	Integer getAllFilterTransportersRecord(@Param("transporterName") String transporterName,
			@Param("transporterCode") String transporterCode);

	List<Transporters> getAllSearchTransporters(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("search") String search);

	Integer getAllSearchTransportersRecord(@Param("search") String search);

	@Select("SELECT COUNT(*)  FROM CAT_UNIQUECODE WHERE REPLACE(lower(CNAME),' ','') = REPLACE(lower(#{categoryName}),' ','') AND lower(HL3CODE)= trim(lower(#{hl3Code}))")
	int getExistanceUniqueCodeByArticleCode(@Param("hl3Code") String hl3Code,
			@Param("categoryName") String categoryName);

	@Select("SELECT COUNT(*)  FROM CAT_BRAND WHERE REPLACE(lower(CNAME),' ','') = REPLACE(lower(#{categoryName}),' ','') AND lower(HL3CODE)= trim(lower(#{hl3Code}))")
	int getExistanceBrandsByArticleCode(@Param("hl3Code") String hl3Code, @Param("categoryName") String categoryName);

	@Select("SELECT COUNT(*)  FROM CAT_PATTERN WHERE  REPLACE(lower(CNAME),' ','') = REPLACE(lower(#{categoryName}),' ','') AND lower(HL3CODE)= trim(lower(#{hl3Code}))")
	int getExistancePatternsByArticleCode(@Param("hl3Code") String hl3Code, @Param("categoryName") String categoryName);

	@Select("SELECT COUNT(*)  FROM CAT_ASSORTMENT WHERE REPLACE(lower(CNAME),' ','') = REPLACE(lower(#{categoryName}),' ','') AND lower(HL3CODE)= trim(lower(#{hl3Code}))")
	int getExistanceAssortmentsByArticleCode(@Param("hl3Code") String hl3Code,
			@Param("categoryName") String categoryName);

	@Select("SELECT COUNT(*)  FROM DESC_SEASONCODE WHERE REPLACE(lower(DESCRIPTION),' ','') = REPLACE(lower(#{description}),' ','') AND lower(HL3CODE)= trim(lower(#{hl3Code}))")
	int getExistanceSeasonByArticleCode(@Param("hl3Code") String hl3Code, @Param("description") String description);

	@Select("SELECT COUNT(*)  FROM DESC_FABRIC WHERE REPLACE(lower(DESCRIPTION),' ','') = REPLACE(lower(#{description}),' ','') AND lower(HL3CODE)= trim(lower(#{hl3Code}))")
	int getExistanceFabricByArticleCode(@Param("hl3Code") String hl3Code, @Param("description") String description);

	@Select("SELECT COUNT(*)  FROM DESC_DRAWINCODE WHERE REPLACE(lower(DESCRIPTION),' ','') = REPLACE(lower(#{description}),' ','') AND lower(HL3CODE)= trim(lower(#{hl3Code}))")
	int getExistanceDarwinByArticleCode(@Param("hl3Code") String hl3Code, @Param("description") String description);

	@Select("SELECT COUNT(*)  FROM DESC_WEAVED WHERE REPLACE(lower(DESCRIPTION),' ','') = REPLACE(lower(#{description}),' ','') AND lower(HL3CODE)= trim(lower(#{hl3Code}))")
	int getExistanceWeavedByArticleCode(@Param("hl3Code") String hl3Code, @Param("description") String description);

	@Select("SELECT COUNT(*)  FROM CAT_SIZE WHERE REPLACE(lower(CNAME),' ','') = REPLACE(lower(#{categoryName}),' ','') AND lower(HL3NAME)= trim(lower(#{hl3Name}))")
	int getExistanceSizeByArticleCode(@Param("hl3Name") String hl3Name, @Param("categoryName") String categoryName);

	@Select("SELECT COUNT(*)  FROM CAT_COLOR WHERE REPLACE(lower(CNAME),' ','') = REPLACE(lower(#{categoryName}),' ','') AND lower(HL3CODE)= trim(lower(#{hl3Code}))")
	int getExistanceColorByArticleCode(@Param("hl3Code") String hl3Code, @Param("categoryName") String categoryName);

	@Select("SELECT COUNT(*)  FROM HLEVELDATA WHERE REPLACE(lower(HL1NAME),' ','') =REPLACE(lower(#{hl1name}),' ','') AND REPLACE(lower(HL2NAME),' ','') =REPLACE(lower(#{hl2name}),' ','') AND REPLACE(lower(HL3NAME),' ','') =REPLACE(lower(#{hl3name}),' ','')")
	int getExistanceHierarachyLevels(@Param("hl1name") String hl1name, @Param("hl2name") String hl2name,
			@Param("hl3name") String hl3name);

	String marginRule(@Param("slCode") String slCode, @Param("articleCode") String articleCode);

	String marginRuleValue(@Param("slCode") String slCode, @Param("articleCode") String articleCode,
			@Param("siteCode") String siteCode);

	@Select("select OTB_AMOUNT from ACTUAL_OTB where DESC6 =#{mrp} AND ARTICLE_CODE=#{articleCode} AND lower(MONTH)=lower(#{month}) AND YEAR=#{year}")
	String getActualOtbData(@Param("articleCode") String article, @Param("mrp") String mrp,
			@Param("month") String month, @Param("year") String year);

	List<DeptItemUDFSettings> getCatDescByHl3Name(@Param("hl3Code") String hl3Code);

	Integer countIndentHistoryFilter(@Param("pattern") String pattern, @Param("supplierName") String supplierName,
			@Param("articleCode") String articleCode, @Param("division") String division,
			@Param("status") String status, @Param("section") String section, @Param("department") String department,
			@Param("generatedDate") String generatedDate, @Param("slCityName") String slCityName,
			@Param("desc2Code") String desc2Code, @Param("deliveryDateFrom") String deliveryDateFrom,
			@Param("deliveryDateTo") String deliveryDateTo, @Param("orgId") String orgId);

	List<PurchaseIndentHistory> getIndentHistoryFilter(@Param("offset") Integer offset,
			@Param("pagesize") Integer pagesize, @Param("pattern") String pattern,
			@Param("supplierName") String supplierName, @Param("articleCode") String articleCode,
			@Param("division") String division, @Param("status") String status, @Param("section") String section,
			@Param("department") String department, @Param("generatedDate") String generatedDate,
			@Param("slCityName") String slCityName, @Param("desc2Code") String desc2Code,
			@Param("deliveryDateFrom") String deliveryDateFrom, @Param("deliveryDateTo") String deliveryDateTo,
			@Param("orgId") String orgId);

	Integer countIndentHistorySearch(@Param("search") String search, @Param("orgId") String orgId);

	List<PurchaseIndentHistory> getIndentHistorySearch(@Param("offset") Integer offset,
			@Param("pagesize") Integer pagesize, @Param("search") String search, @Param("orgId") String orgId);

	PurchaseIndent getPurchaseIndentMain(@Param("orderId") int orderId);

	List<PurchaseIndentDetail> getPurchaseIndentDetail(@Param("orderId") int orderId,
			@Param("orderDetailId") String orderDetailId);

	@Select("select count(*) from (select distinct code, hsn_sac_code  from invhsnsacmain where extinct = 'N' and appl = 'G' )")
	Integer countHSNCode();

	@Select("select distinct code, hsn_sac_code  from invhsnsacmain where extinct = 'N' and appl = 'G' OFFSET #{offset} ROWS FETCH NEXT #{pagesize} ROWS ONLY")
	List<HsnData> getHSNCode(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize);

	Integer countHSNCodeSearch(@Param("search") String search);

	List<HsnData> getHSNCodeSearch(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("search") String search);

	@Select("select value from system_default_config where key = 'PRO_CONFIG' AND ORG_ID=#{orgId}")
	String getUDFStatus(@Param("orgId") String orgId);

	List<SupplierData> getCustomSupplierData(@Param("department") String department, @Param("offset") Integer offset,
			@Param("pagesize") Integer pagesize);

	@Select("SELECT COUNT(*) FROM SUPPLIERS_CUSTOM_DATA WHERE HL3NAME = #{department}")
	@Options(fetchSize = 1000, timeout = 360000)
	Integer getRecordCustomSupplierData(@Param("department") String department);

	List<SupplierData> searchByCustomSupplierData(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("slCode") String slCode, @Param("slName") String slName, @Param("slAddr") String slAddr,
			@Param("department") String department);

	Integer recordsearchByCustomSupplierData(@Param("slCode") String slCode, @Param("slName") String slName,
			@Param("slAddr") String slAddr, @Param("department") String department);

	List<SupplierData> searchCustomSupplierData(@Param("department") String department, @Param("offset") Integer offset,
			@Param("pagesize") Integer pagesize, @Param("search") String search);

	Integer recordSearchCustomSupplierData(@Param("department") String department, @Param("search") String search);

	@Select("SELECT COUNT(*) FROM SUPPLIERS")
	@Options(fetchSize = 1000, timeout = 360000)
	Integer getAllRecordCustomSupplierData();

	List<SupplierData> getAllCustomSupplierData(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize);

	List<SupplierData> searchAllByCustomSupplierData(@Param("offset") Integer offset,
			@Param("pagesize") Integer pagesize, @Param("slCode") String slCode, @Param("slName") String slName,
			@Param("slAddr") String slAddr);

	Integer recordsearchAllByCustomSupplierData(@Param("slCode") String slCode, @Param("slName") String slName,
			@Param("slAddr") String slAddr);

	List<SupplierData> searchAllCustomSupplierData(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("search") String search);

	Integer recordSearchAllCustomSupplierData(@Param("search") String search);

	Integer getCustomRecordArticlePricePoint(@Param("department") String department);

	List<ADMItem> getCustomPricePoint(@Param("department") String department, @Param("offset") Integer offset,
			@Param("pagesize") Integer pagesize);

	Integer recordCustomSearchByArticlePricePoint(@Param("articleName") String articleName,
			@Param("department") String department, @Param("costPrice") String costPrice,
			@Param("sellPrice") String sellPrice);

	List<ADMItem> searchByCustomArticlePricePoint(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("articleName") String articleName, @Param("department") String department,
			@Param("costPrice") String costPrice, @Param("sellPrice") String sellPrice);

	Integer recordCustomArticlePricePoint(@Param("department") String department, @Param("search") String search);

	List<ADMItem> searchCustomArticlePricePoint(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("department") String department, @Param("search") String search);

	@Select(value = "CALL DBMS_MVIEW.REFRESH('ARTICLEHLEVELDATA,CAT_ASSORTMENT,CAT_BRAND,CAT_COLOR,CAT_PATTERN,CAT_SIZE,CAT_UNIQUECODE,DESC_DRAWINCODE,DESC_FABRIC,\n"
			+ "DESC_SEASONCODE,DESC_WEAVED,DESC6DATA,HLEVELDATA,PO_DESC6_DATA,SUPPLIERDATA','?')")
	@Options(statementType = StatementType.CALLABLE, fetchSize = 1000)
	void resfreshPIMv();

	@Select("SELECT VALUE FROM SYSTEM_DEFAULT_CONFIG WHERE KEY=#{key} AND ORG_ID=#{orgId}")
	String getSystemDefaultConfig(@Param("key") String key, @Param("orgId") String orgId);

	@Select("SELECT COUNT(*) FROM SUPPLIERS WHERE LOWER(SLCITYNAME)=LOWER(#{cityName})")
	@Options(fetchSize = 1000, timeout = 360000)
	int getRecordSuppliersByCity(@Param("cityName") String cityName);

	List<SupplierData> searchByCustomSuppliersByCity(@Param("offset") int offset, @Param("pageSize") int pageSize,
			@Param("slCode") String slCode, @Param("slName") String slName, @Param("slAddr") String slAddr,
			@Param("cityName") String cityName);

	int recordsearchByCustomSuppliersByCity(@Param("slCode") String slCode, @Param("slName") String slName,
			@Param("slAddr") String slAddr, @Param("cityName") String cityName);

	List<SupplierData> getCustomSuppliersByCity(@Param("cityName") String cityName, @Param("offset") int offset,
			@Param("pageSize") int pageSize);

	int recordSearchCustomSuppliersByCity(@Param("cityName") String cityName, @Param("search") String search);

	List<SupplierData> searchCustomSuppliersByCity(@Param("cityName") String cityName, @Param("offset") int offset,
			@Param("pageSize") int pageSize, @Param("search") String search);

////////// code by transporter
	@Select("SELECT COUNT(DISTINCT TRANSPORTERCODE ) FROM TRANSPORTERS WHERE LOWER(SLCITYNAME)=LOWER(#{cityName}) AND IS_EXT =\n"
			+ "		'N' AND TRANSPORTERCODE IS NOT NULL")
	@Options(fetchSize = 1000, timeout = 360000)
	int getAllTransportersRecordByCity(@Param("cityName") String cityName);

	List<Transporters> getAllTransportersByCity(@Param("offset") int offset, @Param("pageSize") int pageSize,
			@Param("cityName") String cityName);

	int getAllFilterTransportersRecordByCity(@Param("transporterName") String transporterName,
			@Param("transporterCode") String transporterCode, @Param("cityName") String cityName);

	List<Transporters> getAllFilterTransportersByCity(@Param("offset") int offset, @Param("pageSize") int pageSize,
			@Param("transporterName") String transporterName, @Param("transporterCode") String transporterCode,
			@Param("cityName") String cityName);

	int getAllSearchTransportersRecordByCity(@Param("search") String search, @Param("cityName") String cityName);

	List<Transporters> getAllSearchTransportersByCity(@Param("offset") int offset, @Param("pageSize") int pageSize,
			@Param("search") String search, @Param("cityName") String cityName);

	@Select("SELECT COUNT(*) FROM ${tableName} WHERE HL3CODE = #{hl3Code} AND CNAME IS NOT NULL")
	Integer getAllRecordByCategaory(@Param("hl3Code") String hl3Code, @Param("tableName") String tableName);

	@Select("SELECT code,cname,hl3Name FROM ${tableName} WHERE HL3CODE = #{hl3Code} AND CNAME IS NOT NULL OFFSET #{offset} ROWS FETCH NEXT #{pagesize} ROWS ONLY")
	List<CatDescMaster> getAllByCategaory(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("hl3Code") String hl3Code, @Param("tableName") String tableName);

	@Select("SELECT COUNT(*) FROM ${tableName} WHERE hl3Code = #{hl3Code} AND DESCRIPTION IS NOT NULL")
	Integer getAllRecordByDescription(@Param("hl3Code") String hl3Code, @Param("tableName") String tableName);

	@Select("SELECT hl3Name,description FROM ${tableName} WHERE hl3Code = #{hl3Code} AND DESCRIPTION IS NOT NULL OFFSET #{offset} ROWS FETCH NEXT #{pagesize} ROWS ONLY")
	List<CatDescMaster> getAllByDescription(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("hl3Code") String hl3Code, @Param("tableName") String tableName);

	Integer searchAllRecordByCategaory(@Param("hl3Code") String hl3Code, @Param("search") String search,
			@Param("tableName") String tableName);

	List<CatDescMaster> searchAllByCategaory(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("hl3Code") String hl3Code, @Param("search") String search, @Param("tableName") String tableName);

	Integer searchAllRecordByDescription(@Param("hl3Code") String hl3Code, @Param("search") String search,
			@Param("tableName") String tableName);

	List<CatDescMaster> searchAllByDescription(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("hl3Code") String hl3Code, @Param("search") String search, @Param("tableName") String tableName);

	List<CatDescMapping> getAllCatDescMapping();

	int createPIDetailsForOthers(PurchaseIndentDetail piItems);

	int createFinChargeForOthers(FinCharge finCharge);

	List<PurchaseIndentDetail> getPurchaseIndentDetailForOthers(@Param("orderId") int orderId,
			@Param("orderDetailId") String orderDetailId);

	@Select("SELECT COUNT(*) FROM ARTICLEHLEVELDATA WHERE HL3Code=#{hl3Code}")
	@Options(fetchSize = 1000, timeout = 360000)
	int recordDistinctArticle(String hl3Code);

	@Select("SELECT HL3CODE DepartmentCode,HL4NAME ArticleName,HL4CODE ArticleCode,MRPRANGEFROM MrpRangeFrom, MRPRANGETO MrpRangeTo FROM ARTICLEHLEVELDATA WHERE HL3Code=#{hl3Code} OFFSET #{offset} ROWS FETCH NEXT #{pagesize} ROWS ONLY")
	List<Map<String, String>> getByDepartmentCode(@Param("hl3Code") String hl3Code, @Param("offset") Integer offset,
			@Param("pagesize") Integer pagesize);

	int searchRecordDistinctArticle(@Param("search") String search);

	List<Map<String, String>> searchByDepartmentCode(@Param("offset") Integer offset,
			@Param("pageSize") Integer pageSize, @Param("search") String search);

}
