package com.supplymint.layer.data.tenant.procurement.entity;

public class SupplierData {

	private String slAddr;
	private String slName;
	private String slCode;
	private String slId;
	private String hl4Name;
	private String hl4Code;
	private String hl3Name;
	private String hl2Name;
	private String hl1Name;
	private String city;
	private String gstInStateCode;
	private String gstinNo;
	private String transporterCode;
	private String transporterName;
	private String tradeGrpCode;
	private String purchaseTermCode;
	private String purchaseTermName;

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getGstInStateCode() {
		return gstInStateCode;
	}

	public void setGstInStateCode(String gstInStateCode) {
		this.gstInStateCode = gstInStateCode;
	}

	public String getGstinNo() {
		return gstinNo;
	}

	public void setGstinNo(String gstinNo) {
		this.gstinNo = gstinNo;
	}

	public String getSlAddr() {
		return slAddr;
	}

	public void setSlAddr(String slAddr) {
		this.slAddr = slAddr;
	}

	public String getSlName() {
		return slName;
	}

	public void setSlName(String slName) {
		this.slName = slName;
	}

	public String getSlCode() {
		return slCode;
	}

	public void setSlCode(String slCode) {
		this.slCode = slCode;
	}

	public String getHl4Name() {
		return hl4Name;
	}

	public void setHl4Name(String hl4Name) {
		this.hl4Name = hl4Name;
	}

	public String getHl4Code() {
		return hl4Code;
	}

	public void setHl4Code(String hl4Code) {
		this.hl4Code = hl4Code;
	}

	public String getHl3Name() {
		return hl3Name;
	}

	public void setHl3Name(String hl3Name) {
		this.hl3Name = hl3Name;
	}

	public String getHl2Name() {
		return hl2Name;
	}

	public void setHl2Name(String hl2Name) {
		this.hl2Name = hl2Name;
	}

	public String getHl1Name() {
		return hl1Name;
	}

	public void setHl1Name(String hl1Name) {
		this.hl1Name = hl1Name;
	}

	public String getTransporterCode() {
		return transporterCode;
	}

	public void setTransporterCode(String transporterCode) {
		this.transporterCode = transporterCode;
	}

	public String getTransporterName() {
		return transporterName;
	}

	public void setTransporterName(String transporterName) {
		this.transporterName = transporterName;
	}

	public String getTradeGrpCode() {
		return tradeGrpCode;
	}

	public void setTradeGrpCode(String tradeGrpCode) {
		this.tradeGrpCode = tradeGrpCode;
	}

	public String getPurchaseTermCode() {
		return purchaseTermCode;
	}

	public void setPurchaseTermCode(String purchaseTermCode) {
		this.purchaseTermCode = purchaseTermCode;
	}

	public String getPurchaseTermName() {
		return purchaseTermName;
	}

	public void setPurchaseTermName(String purchaseTermName) {
		this.purchaseTermName = purchaseTermName;
	}

	public String getSlId() {
		return slId;
	}

	public void setSlId(String slId) {
		this.slId = slId;
	}

	@Override
	public String toString() {
		return "SupplierData [slAddr=" + slAddr + ", slName=" + slName + ", slCode=" + slCode + ", slId=" + slId
				+ ", hl4Name=" + hl4Name + ", hl4Code=" + hl4Code + ", hl3Name=" + hl3Name + ", hl2Name=" + hl2Name
				+ ", hl1Name=" + hl1Name + ", city=" + city + ", gstInStateCode=" + gstInStateCode + ", gstinNo="
				+ gstinNo + ", transporterCode=" + transporterCode + ", transporterName=" + transporterName
				+ ", tradeGrpCode=" + tradeGrpCode + ", purchaseTermCode=" + purchaseTermCode + ", purchaseTermName="
				+ purchaseTermName + "]";
	}

}
