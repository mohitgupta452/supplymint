package com.supplymint.layer.data.tenant.demand.entity;

import java.time.OffsetDateTime;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Assortment {
	private String iCode;
	private String assortmentCode;
	private String hl1Name;
	private String hl2Name;
	private String hl3Name;
	private String hl4Name;
	private String itemName;
	private String brand;
	private String color;
	private String size;
	private String pattern;
	private String material;
	private String design;
	private int mrp;
	private char active;
	private String status;
	private String ipAddress;
	private String createdBy;
	private OffsetDateTime createdTime;
	@JsonIgnore
	private int offset;
	@JsonIgnore
	private int pageSize;
	@JsonIgnore
	private String search;
	@JsonIgnore
	private String startDate;
	@JsonIgnore
	private String endDate;
	@JsonIgnore
	private String itemCount;
	@JsonIgnore
	private String hierarchyPattarn;
	@JsonIgnore
	private String patternLog;
	@JsonIgnore
	private String createdOn;

	public Assortment() {
		// TODO Auto-generated constructor stub
	}

	public Assortment(String iCode, String assortmentCode, String hl1Name, String hl2Name, String hl3Name,
			String hl4Name, String itemName, String brand, String color, String size, String pattern, String material,
			String design, int mrp, char active, String status, String ipAddress, String createdBy,
			OffsetDateTime createdTime, int offset, int pageSize, String search, String startDate, String endDate,
			String itemCount, String hierarchyPattarn, String patternLog, String createdOn) {
		super();
		this.iCode = iCode;
		this.assortmentCode = assortmentCode;
		this.hl1Name = hl1Name;
		this.hl2Name = hl2Name;
		this.hl3Name = hl3Name;
		this.hl4Name = hl4Name;
		this.itemName = itemName;
		this.brand = brand;
		this.color = color;
		this.size = size;
		this.pattern = pattern;
		this.material = material;
		this.design = design;
		this.mrp = mrp;
		this.active = active;
		this.status = status;
		this.ipAddress = ipAddress;
		this.createdBy = createdBy;
		this.createdTime = createdTime;
		this.offset = offset;
		this.pageSize = pageSize;
		this.search = search;
		this.startDate = startDate;
		this.endDate = endDate;
		this.itemCount = itemCount;
		this.hierarchyPattarn = hierarchyPattarn;
		this.patternLog = patternLog;
		this.createdOn = createdOn;
	}

	public String getiCode() {
		return iCode;
	}

	public void setiCode(String iCode) {
		this.iCode = iCode;
	}

	public String getAssortmentCode() {
		return assortmentCode;
	}

	public void setAssortmentCode(String assortmentCode) {
		this.assortmentCode = assortmentCode;
	}

	public String getHl1Name() {
		return hl1Name;
	}

	public void setHl1Name(String hl1Name) {
		this.hl1Name = hl1Name;
	}

	public String getHl2Name() {
		return hl2Name;
	}

	public void setHl2Name(String hl2Name) {
		this.hl2Name = hl2Name;
	}

	public String getHl3Name() {
		return hl3Name;
	}

	public void setHl3Name(String hl3Name) {
		this.hl3Name = hl3Name;
	}

	public String getHl4Name() {
		return hl4Name;
	}

	public void setHl4Name(String hl4Name) {
		this.hl4Name = hl4Name;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	public String getMaterial() {
		return material;
	}

	public void setMaterial(String material) {
		this.material = material;
	}

	public String getDesign() {
		return design;
	}

	public void setDesign(String design) {
		this.design = design;
	}

	public int getMrp() {
		return mrp;
	}

	public void setMrp(int mrp) {
		this.mrp = mrp;
	}

	public char getActive() {
		return active;
	}

	public void setActive(char active) {
		this.active = active;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public OffsetDateTime getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(OffsetDateTime createdTime) {
		this.createdTime = createdTime;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getItemCount() {
		return itemCount;
	}

	public void setItemCount(String itemCount) {
		this.itemCount = itemCount;
	}

	public String getHierarchyPattarn() {
		return hierarchyPattarn;
	}

	public void setHierarchyPattarn(String hierarchyPattarn) {
		this.hierarchyPattarn = hierarchyPattarn;
	}

	public String getPatternLog() {
		return patternLog;
	}

	public void setPatternLog(String patternLog) {
		this.patternLog = patternLog;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	@Override
	public String toString() {
		return "Assortment [iCode=" + iCode + ", assortmentCode=" + assortmentCode + ", hl1Name=" + hl1Name
				+ ", hl2Name=" + hl2Name + ", hl3Name=" + hl3Name + ", hl4Name=" + hl4Name + ", itemName=" + itemName
				+ ", brand=" + brand + ", color=" + color + ", size=" + size + ", pattern=" + pattern + ", material="
				+ material + ", design=" + design + ", mrp=" + mrp + ", active=" + active + ", status=" + status
				+ ", ipAddress=" + ipAddress + ", createdBy=" + createdBy + ", createdTime=" + createdTime + ", offset="
				+ offset + ", pageSize=" + pageSize + ", search=" + search + ", startDate=" + startDate + ", endDate="
				+ endDate + ", itemCount=" + itemCount + ", hierarchyPattarn=" + hierarchyPattarn + ", patternLog="
				+ patternLog + ", createdOn=" + createdOn + "]";
	}

}
