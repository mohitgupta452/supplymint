package com.supplymint.layer.data.tenant.procurement.entity;

import java.time.OffsetDateTime;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.supplymint.util.LocalDatetimeDeserializer;
import com.supplymint.util.PILocalDatetimeSerializer;

public class PurchaseOrderLineItem {

	public List<CatMaster> colors;
	public List<CatMaster> sizes;
	public List<Integer> ratio;
	public double qty;
	public String desc1;
	public double rate;

	public String itemName;
	public String itemId;
	public int cCode1;
	public int cCode2;
	public int cCode3;
	public int cCode4;
	public String cName1;
	public String cName2;
	public String cName3;
	public String cName4;
	public String desc2;
	public String desc3;
	public String desc5;

	public String itemudf1;
	public String itemudf2;
	public String itemudf3;
	public String itemudf4;
	public String itemudf5;
	public String itemudf6;
	public String itemudf7;
	public String itemudf8;
	public String itemudf9;
	public String itemudf10;
	public int itemudf11;
	public int itemudf12;
	public int itemudf13;
	public int itemudf14;
	public int itemudf15;

	public String setRemarks;

	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	public OffsetDateTime itemudf16;

	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	public OffsetDateTime itemudf17;

	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	public OffsetDateTime itemudf18;

	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	public OffsetDateTime itemudf19;

	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	public OffsetDateTime itemudf20;

	public String udf1;
	public String udf2;
	public String udf3;
	public String udf4;
	public String udf5;
	public String udf6;
	public String udf7;
	public String udf8;
	public String udf9;
	public String udf10;
	public int udf11;
	public int udf12;
	public int udf13;
	public int udf14;
	public int udf15;

	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	public OffsetDateTime udf16;

	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	public OffsetDateTime udf17;

	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	public OffsetDateTime udf18;

	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	public OffsetDateTime udf19;

	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	public OffsetDateTime udf20;

	public PurchaseOrderLineItem() {
		super();
	}

	public PurchaseOrderLineItem(List<CatMaster> colors, List<CatMaster> sizes, List<Integer> ratio, double qty,
			String desc1, double rate, String itemName, String itemId, int cCode1, int cCode2, int cCode3, int cCode4,
			String cName1, String cName2, String cName3, String cName4, String desc2, String desc3, String desc5,
			String itemudf1, String itemudf2, String itemudf3, String itemudf4, String itemudf5, String itemudf6,
			String itemudf7, String itemudf8, String itemudf9, String itemudf10, int itemudf11, int itemudf12,
			int itemudf13, int itemudf14, int itemudf15, OffsetDateTime itemudf16, OffsetDateTime itemudf17,
			OffsetDateTime itemudf18, OffsetDateTime itemudf19, OffsetDateTime itemudf20, String udf1, String udf2,
			String udf3, String udf4, String udf5, String udf6, String udf7, String udf8, String udf9, String udf10,
			int udf11, int udf12, int udf13, int udf14, int udf15, OffsetDateTime udf16, OffsetDateTime udf17,
			OffsetDateTime udf18, OffsetDateTime udf19, OffsetDateTime udf20) {
		super();
		this.colors = colors;
		this.sizes = sizes;
		this.ratio = ratio;
		this.qty = qty;
		this.desc1 = desc1;
		this.rate = rate;
		this.itemName = itemName;
		this.itemId = itemId;
		this.cCode1 = cCode1;
		this.cCode2 = cCode2;
		this.cCode3 = cCode3;
		this.cCode4 = cCode4;
		this.cName1 = cName1;
		this.cName2 = cName2;
		this.cName3 = cName3;
		this.cName4 = cName4;
		this.desc2 = desc2;
		this.desc3 = desc3;
		this.desc5 = desc5;
		this.itemudf1 = itemudf1;
		this.itemudf2 = itemudf2;
		this.itemudf3 = itemudf3;
		this.itemudf4 = itemudf4;
		this.itemudf5 = itemudf5;
		this.itemudf6 = itemudf6;
		this.itemudf7 = itemudf7;
		this.itemudf8 = itemudf8;
		this.itemudf9 = itemudf9;
		this.itemudf10 = itemudf10;
		this.itemudf11 = itemudf11;
		this.itemudf12 = itemudf12;
		this.itemudf13 = itemudf13;
		this.itemudf14 = itemudf14;
		this.itemudf15 = itemudf15;
		this.itemudf16 = itemudf16;
		this.itemudf17 = itemudf17;
		this.itemudf18 = itemudf18;
		this.itemudf19 = itemudf19;
		this.itemudf20 = itemudf20;
		this.udf1 = udf1;
		this.udf2 = udf2;
		this.udf3 = udf3;
		this.udf4 = udf4;
		this.udf5 = udf5;
		this.udf6 = udf6;
		this.udf7 = udf7;
		this.udf8 = udf8;
		this.udf9 = udf9;
		this.udf10 = udf10;
		this.udf11 = udf11;
		this.udf12 = udf12;
		this.udf13 = udf13;
		this.udf14 = udf14;
		this.udf15 = udf15;
		this.udf16 = udf16;
		this.udf17 = udf17;
		this.udf18 = udf18;
		this.udf19 = udf19;
		this.udf20 = udf20;
	}

	public List<CatMaster> getColors() {
		return colors;
	}

	public void setColors(List<CatMaster> colors) {
		this.colors = colors;
	}

	public List<CatMaster> getSizes() {
		return sizes;
	}

	public void setSizes(List<CatMaster> sizes) {
		this.sizes = sizes;
	}

	public List<Integer> getRatio() {
		return ratio;
	}

	public void setRatio(List<Integer> ratio) {
		this.ratio = ratio;
	}

	public double getQty() {
		return qty;
	}

	public void setQty(double qty) {
		this.qty = qty;
	}

	public String getDesc1() {
		return desc1;
	}

	public void setDesc1(String desc1) {
		this.desc1 = desc1;
	}

	public double getRate() {
		return rate;
	}

	public void setRate(double rate) {
		this.rate = rate;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public int getcCode1() {
		return cCode1;
	}

	public void setcCode1(int cCode1) {
		this.cCode1 = cCode1;
	}

	public int getcCode2() {
		return cCode2;
	}

	public void setcCode2(int cCode2) {
		this.cCode2 = cCode2;
	}

	public int getcCode3() {
		return cCode3;
	}

	public void setcCode3(int cCode3) {
		this.cCode3 = cCode3;
	}

	public int getcCode4() {
		return cCode4;
	}

	public void setcCode4(int cCode4) {
		this.cCode4 = cCode4;
	}

	public String getcName1() {
		return cName1;
	}

	public void setcName1(String cName1) {
		this.cName1 = cName1;
	}

	public String getcName2() {
		return cName2;
	}

	public void setcName2(String cName2) {
		this.cName2 = cName2;
	}

	public String getcName3() {
		return cName3;
	}

	public void setcName3(String cName3) {
		this.cName3 = cName3;
	}

	public String getcName4() {
		return cName4;
	}

	public void setcName4(String cName4) {
		this.cName4 = cName4;
	}

	public String getDesc2() {
		return desc2;
	}

	public void setDesc2(String desc2) {
		this.desc2 = desc2;
	}

	public String getDesc3() {
		return desc3;
	}

	public void setDesc3(String desc3) {
		this.desc3 = desc3;
	}

	public String getDesc5() {
		return desc5;
	}

	public void setDesc5(String desc5) {
		this.desc5 = desc5;
	}

	public String getUdf1() {
		return udf1;
	}

	public void setUdf1(String udf1) {
		this.udf1 = udf1;
	}

	public String getUdf2() {
		return udf2;
	}

	public void setUdf2(String udf2) {
		this.udf2 = udf2;
	}

	public String getUdf3() {
		return udf3;
	}

	public void setUdf3(String udf3) {
		this.udf3 = udf3;
	}

	public String getUdf4() {
		return udf4;
	}

	public void setUdf4(String udf4) {
		this.udf4 = udf4;
	}

	public String getUdf5() {
		return udf5;
	}

	public void setUdf5(String udf5) {
		this.udf5 = udf5;
	}

	public String getUdf6() {
		return udf6;
	}

	public void setUdf6(String udf6) {
		this.udf6 = udf6;
	}

	public String getUdf7() {
		return udf7;
	}

	public void setUdf7(String udf7) {
		this.udf7 = udf7;
	}

	public String getUdf8() {
		return udf8;
	}

	public void setUdf8(String udf8) {
		this.udf8 = udf8;
	}

	public String getUdf9() {
		return udf9;
	}

	public void setUdf9(String udf9) {
		this.udf9 = udf9;
	}

	public String getUdf10() {
		return udf10;
	}

	public void setUdf10(String udf10) {
		this.udf10 = udf10;
	}

	public int getUdf11() {
		return udf11;
	}

	public void setUdf11(int udf11) {
		this.udf11 = udf11;
	}

	public int getUdf12() {
		return udf12;
	}

	public void setUdf12(int udf12) {
		this.udf12 = udf12;
	}

	public int getUdf13() {
		return udf13;
	}

	public void setUdf13(int udf13) {
		this.udf13 = udf13;
	}

	public int getUdf14() {
		return udf14;
	}

	public void setUdf14(int udf14) {
		this.udf14 = udf14;
	}

	public int getUdf15() {
		return udf15;
	}

	public void setUdf15(int udf15) {
		this.udf15 = udf15;
	}

	public OffsetDateTime getUdf16() {
		return udf16;
	}

	public void setUdf16(OffsetDateTime udf16) {
		this.udf16 = udf16;
	}

	public OffsetDateTime getUdf17() {
		return udf17;
	}

	public void setUdf17(OffsetDateTime udf17) {
		this.udf17 = udf17;
	}

	public OffsetDateTime getUdf18() {
		return udf18;
	}

	public void setUdf18(OffsetDateTime udf18) {
		this.udf18 = udf18;
	}

	public OffsetDateTime getUdf19() {
		return udf19;
	}

	public void setUdf19(OffsetDateTime udf19) {
		this.udf19 = udf19;
	}

	public OffsetDateTime getUdf20() {
		return udf20;
	}

	public void setUdf20(OffsetDateTime udf20) {
		this.udf20 = udf20;
	}

	public String getItemudf1() {
		return itemudf1;
	}

	public void setItemudf1(String itemudf1) {
		this.itemudf1 = itemudf1;
	}

	public String getItemudf2() {
		return itemudf2;
	}

	public void setItemudf2(String itemudf2) {
		this.itemudf2 = itemudf2;
	}

	public String getItemudf3() {
		return itemudf3;
	}

	public void setItemudf3(String itemudf3) {
		this.itemudf3 = itemudf3;
	}

	public String getItemudf4() {
		return itemudf4;
	}

	public void setItemudf4(String itemudf4) {
		this.itemudf4 = itemudf4;
	}

	public String getItemudf5() {
		return itemudf5;
	}

	public void setItemudf5(String itemudf5) {
		this.itemudf5 = itemudf5;
	}

	public String getItemudf6() {
		return itemudf6;
	}

	public void setItemudf6(String itemudf6) {
		this.itemudf6 = itemudf6;
	}

	public String getItemudf7() {
		return itemudf7;
	}

	public void setItemudf7(String itemudf7) {
		this.itemudf7 = itemudf7;
	}

	public String getItemudf8() {
		return itemudf8;
	}

	public void setItemudf8(String itemudf8) {
		this.itemudf8 = itemudf8;
	}

	public String getItemudf9() {
		return itemudf9;
	}

	public void setItemudf9(String itemudf9) {
		this.itemudf9 = itemudf9;
	}

	public String getItemudf10() {
		return itemudf10;
	}

	public void setItemudf10(String itemudf10) {
		this.itemudf10 = itemudf10;
	}

	public int getItemudf11() {
		return itemudf11;
	}

	public void setItemudf11(int itemudf11) {
		this.itemudf11 = itemudf11;
	}

	public int getItemudf12() {
		return itemudf12;
	}

	public void setItemudf12(int itemudf12) {
		this.itemudf12 = itemudf12;
	}

	public int getItemudf13() {
		return itemudf13;
	}

	public void setItemudf13(int itemudf13) {
		this.itemudf13 = itemudf13;
	}

	public int getItemudf14() {
		return itemudf14;
	}

	public void setItemudf14(int itemudf14) {
		this.itemudf14 = itemudf14;
	}

	public int getItemudf15() {
		return itemudf15;
	}

	public void setItemudf15(int itemudf15) {
		this.itemudf15 = itemudf15;
	}

	public OffsetDateTime getItemudf16() {
		return itemudf16;
	}

	public void setItemudf16(OffsetDateTime itemudf16) {
		this.itemudf16 = itemudf16;
	}

	public OffsetDateTime getItemudf17() {
		return itemudf17;
	}

	public void setItemudf17(OffsetDateTime itemudf17) {
		this.itemudf17 = itemudf17;
	}

	public OffsetDateTime getItemudf18() {
		return itemudf18;
	}

	public void setItemudf18(OffsetDateTime itemudf18) {
		this.itemudf18 = itemudf18;
	}

	public OffsetDateTime getItemudf19() {
		return itemudf19;
	}

	public void setItemudf19(OffsetDateTime itemudf19) {
		this.itemudf19 = itemudf19;
	}

	public OffsetDateTime getItemudf20() {
		return itemudf20;
	}

	public void setItemudf20(OffsetDateTime itemudf20) {
		this.itemudf20 = itemudf20;
	}

	public String getSetRemarks() {
		return setRemarks;
	}

	public void setSetRemarks(String setRemarks) {
		this.setRemarks = setRemarks;
	}

	@Override
	public String toString() {
		return "PurchaseOrderLineItem [colors=" + colors + ", sizes=" + sizes + ", ratio=" + ratio + ", qty=" + qty
				+ ", desc1=" + desc1 + ", rate=" + rate + ", itemName=" + itemName + ", itemId=" + itemId + ", cCode1="
				+ cCode1 + ", cCode2=" + cCode2 + ", cCode3=" + cCode3 + ", cCode4=" + cCode4 + ", cName1=" + cName1
				+ ", cName2=" + cName2 + ", cName3=" + cName3 + ", cName4=" + cName4 + ", desc2=" + desc2 + ", desc3="
				+ desc3 + ", desc5=" + desc5 + ", udf1=" + udf1 + ", udf2=" + udf2 + ", udf3=" + udf3 + ", udf4=" + udf4
				+ ", udf5=" + udf5 + ", udf6=" + udf6 + ", udf7=" + udf7 + ", udf8=" + udf8 + ", udf9=" + udf9
				+ ", udf10=" + udf10 + ", udf11=" + udf11 + ", udf12=" + udf12 + ", udf13=" + udf13 + ", udf14=" + udf14
				+ ", udf15=" + udf15 + ", udf16=" + udf16 + ", udf17=" + udf17 + ", udf18=" + udf18 + ", udf19=" + udf19
				+ ", udf20=" + udf20 + "]";
	}

}
