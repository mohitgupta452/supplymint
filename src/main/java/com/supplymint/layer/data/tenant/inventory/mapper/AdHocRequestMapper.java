package com.supplymint.layer.data.tenant.inventory.mapper;

import java.time.OffsetDateTime;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.supplymint.layer.data.tenant.administration.entity.ADMItem;
import com.supplymint.layer.data.tenant.administration.entity.ADMSite;
import com.supplymint.layer.data.tenant.demand.entity.AdHocItemRequest;
import com.supplymint.layer.data.tenant.demand.entity.AdHocRequest;

@Mapper
public interface AdHocRequestMapper {

	int create(AdHocRequest adocRequest);

	// @Select("SELECT DISTINCT ICODE FROM INVITEM WHERE ICODE IS NOT NULL ORDER BY
	// ICODE OFFSET #{offset} ROWS FETCH NEXT #{pagesize} ROWS ONLY")
	// List<ADMItem> itemCode(@Param("offset") Integer offset, @Param("pagesize")
	// Integer pagesize);

	List<ADMItem> itemCode(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize);

	// @Select("SELECT COUNT(*) from (SELECT DISTINCT ICODE FROM INVITEM)")
	Integer countItemCode();

	List<ADMItem> searchItemCode(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("search") String search);

	Integer counterForSerarchItemCode(@Param("search") String search);

	@Select("SELECT DISTINCT NAME,CODE FROM ADM_SITES WHERE NAME IS NOT NULL AND CODE IS NOT NULL")
	List<ADMSite> getLocation();

	List<AdHocRequest> getAll(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize);

	Integer count();

	List<AdHocRequest> searchAll(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("search") String search);

	Integer searchCount(@Param("search") String search);

	@Select("SELECT WORK_ORDER FROM ADHOC_REQUEST ORDER BY ID DESC FETCH FIRST 1 ROWS ONLY")
	String getLastWorkOrder();

	@Select("SELECT ITEMCODE as itemCode , QUANTITY as quantity, ITEM_STATUS as itemStatus FROM ADHOC_REQUEST WHERE WORK_ORDER =#{workOrder}")
	List<AdHocItemRequest> getItem(String workOrder);

	int updateStatus(@Param("workOrder") String workOrder, @Param("ipAddress") String ipAddress,
			@Param("updationTime") OffsetDateTime updationTime, @Param("status") String status);

	List<AdHocRequest> getByworkOrder(@Param("workOrder") String workOrder);

	List<AdHocRequest> getItemByworkOrder(@Param("workOrder") String workOrder);

	List<AdHocRequest> getAllRecords();

	List<AdHocRequest> filter(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("workOrder") String workOrder, @Param("status") String status, @Param("site") String site,
			@Param("createdOn") String createdOn);

	Integer filterRecord(@Param("workOrder") String workOrder, @Param("status") String status,
			@Param("site") String site, @Param("createdOn") String createdOn);

	List<AdHocRequest> filterByDate(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("createdOn") String createdOn);

	Integer getRecordFilterByDate(@Param("createdOn") String createdOn);

	// @Delete("DELETE FROM ADHOC_REQUEST WHERE WORK_ORDER = #{workOrder} AND
	// ITEMCODE= #{itemData} ")
	@Update("UPDATE ADHOC_REQUEST SET ITEM_STATUS = 'Removed' WHERE WORK_ORDER = #{workOrder} AND ITEMCODE= #{itemData}")
	Integer deleteByWorkOrderAndItemCode(@Param("workOrder") String workOrder, @Param("itemData") String itemData);

	@Select("SELECT ITEMCODE AS itemCode, WORK_ORDER AS workOrder FROM ADHOC_REQUEST WHERE WORK_ORDER = #{workOrder} AND ITEMCODE = #{itemData}")
	List<AdHocRequest> validation(@Param("workOrder") String workOrder, @Param("itemData") String itemData);

	Integer cancelWorkedOrder(@Param("workOrder") String workOrder, @Param("ipAddress") String ipAddress,
			@Param("updationTime") OffsetDateTime updationTime);

	int countActiveStatus(@Param("workOrder") String workOrder);

	Integer removedWorkedOrder(@Param("workOrder") String workOrder, @Param("ipAddress") String ipAddress,
			@Param("updationTime") OffsetDateTime updationTime);

}
