package com.supplymint.layer.data.tenant.demand.entity;

public class BudgetedDemandPlanning {

	private String assortmentCode;
	private String billDate;
	private String qtyForeCast;
	private String flag;
	private String lastSummary;
	private String userName;

	public BudgetedDemandPlanning() {
		// TODO Auto-generated constructor stub
	}

	public BudgetedDemandPlanning(String assortmentCode, String billDate, String qtyForeCast, String flag,
			String lastSummary, String userName) {
		super();
		this.assortmentCode = assortmentCode;
		this.billDate = billDate;
		this.qtyForeCast = qtyForeCast;
		this.flag = flag;
		this.lastSummary = lastSummary;
		this.userName = userName;
	}

	public String getAssortmentCode() {
		return assortmentCode;
	}

	public void setAssortmentCode(String assortmentCode) {
		this.assortmentCode = assortmentCode;
	}

	public String getBillDate() {
		return billDate;
	}

	public void setBillDate(String billDate) {
		this.billDate = billDate;
	}

	public String getQtyForeCast() {
		return qtyForeCast;
	}

	public void setQtyForeCast(String qtyForeCast) {
		this.qtyForeCast = qtyForeCast;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getLastSummary() {
		return lastSummary;
	}

	public void setLastSummary(String lastSummary) {
		this.lastSummary = lastSummary;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Override
	public String toString() {
		return "BudgetedDemandPlanning [assortmentCode=" + assortmentCode + ", billDate=" + billDate + ", qtyForeCast="
				+ qtyForeCast + ", flag=" + flag + ", lastSummary=" + lastSummary + ", userName=" + userName + "]";
	}

}
