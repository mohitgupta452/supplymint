package com.supplymint.layer.data.tenant.procurement.entity;

import java.time.OffsetDateTime;

public class ProcurementUploadSummary {
	private String uuId;
	private String procurementType;
	private int rowCount;
	private String url;
	private String lastSummary;
	private String fileName;
	private String uploadStatus;
	private String userName;
	private String orgId;
	private String orgName;
	private String status;
	private String createdBy;
	private OffsetDateTime createdTime;
	private String updatedBy;
	private OffsetDateTime updationTime;
	private String ipAddress;
	private String active;
	private String additional;
	private String bucketName;
	private String bucketPath;
	private String successRecord;
	private String errorRecord;
	private String downloadBucketKey;
	private String jobId;

	public ProcurementUploadSummary() {
		// TODO Auto-generated constructor stub
	}

	public ProcurementUploadSummary(String uuId, String procurementType, int rowCount, String url, String lastSummary,
			String fileName, String uploadStatus, String userName, String orgId, String orgName, String status,
			String createdBy, OffsetDateTime createdTime, String updatedBy, OffsetDateTime updationTime,
			String ipAddress, String active, String additional, String bucketName, String bucketPath,
			String successRecord, String errorRecord, String downloadBucketKey, String jobId) {
		super();
		this.uuId = uuId;
		this.procurementType = procurementType;
		this.rowCount = rowCount;
		this.url = url;
		this.lastSummary = lastSummary;
		this.fileName = fileName;
		this.uploadStatus = uploadStatus;
		this.userName = userName;
		this.orgId = orgId;
		this.orgName = orgName;
		this.status = status;
		this.createdBy = createdBy;
		this.createdTime = createdTime;
		this.updatedBy = updatedBy;
		this.updationTime = updationTime;
		this.ipAddress = ipAddress;
		this.active = active;
		this.additional = additional;
		this.bucketName = bucketName;
		this.bucketPath = bucketPath;
		this.successRecord = successRecord;
		this.errorRecord = errorRecord;
		this.downloadBucketKey = downloadBucketKey;
		this.jobId = jobId;
	}

	public String getUuId() {
		return uuId;
	}

	public void setUuId(String uuId) {
		this.uuId = uuId;
	}

	public String getProcurementType() {
		return procurementType;
	}

	public void setProcurementType(String procurementType) {
		this.procurementType = procurementType;
	}

	public int getRowCount() {
		return rowCount;
	}

	public void setRowCount(int rowCount) {
		this.rowCount = rowCount;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getLastSummary() {
		return lastSummary;
	}

	public void setLastSummary(String lastSummary) {
		this.lastSummary = lastSummary;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getUploadStatus() {
		return uploadStatus;
	}

	public void setUploadStatus(String uploadStatus) {
		this.uploadStatus = uploadStatus;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public OffsetDateTime getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(OffsetDateTime createdTime) {
		this.createdTime = createdTime;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public OffsetDateTime getUpdationTime() {
		return updationTime;
	}

	public void setUpdationTime(OffsetDateTime updationTime) {
		this.updationTime = updationTime;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getAdditional() {
		return additional;
	}

	public void setAdditional(String additional) {
		this.additional = additional;
	}

	public String getBucketName() {
		return bucketName;
	}

	public void setBucketName(String bucketName) {
		this.bucketName = bucketName;
	}

	public String getBucketPath() {
		return bucketPath;
	}

	public void setBucketPath(String bucketPath) {
		this.bucketPath = bucketPath;
	}

	public String getSuccessRecord() {
		return successRecord;
	}

	public void setSuccessRecord(String successRecord) {
		this.successRecord = successRecord;
	}

	public String getErrorRecord() {
		return errorRecord;
	}

	public void setErrorRecord(String errorRecord) {
		this.errorRecord = errorRecord;
	}

	public String getDownloadBucketKey() {
		return downloadBucketKey;
	}

	public void setDownloadBucketKey(String downloadBucketKey) {
		this.downloadBucketKey = downloadBucketKey;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	@Override
	public String toString() {
		return "ProcurementUploadSummary [uuId=" + uuId + ", procurementType=" + procurementType + ", rowCount="
				+ rowCount + ", url=" + url + ", lastSummary=" + lastSummary + ", fileName=" + fileName
				+ ", uploadStatus=" + uploadStatus + ", userName=" + userName + ", orgId=" + orgId + ", orgName="
				+ orgName + ", status=" + status + ", createdBy=" + createdBy + ", createdTime=" + createdTime
				+ ", updatedBy=" + updatedBy + ", updationTime=" + updationTime + ", ipAddress=" + ipAddress
				+ ", active=" + active + ", additional=" + additional + ", bucketName=" + bucketName + ", bucketPath="
				+ bucketPath + ", successRecord=" + successRecord + ", errorRecord=" + errorRecord
				+ ", downloadBucketKey=" + downloadBucketKey + ", jobId=" + jobId + "]";
	}

}