package com.supplymint.layer.data.tenant.mcstr.mapper;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;

import com.supplymint.layer.data.tenant.mcstr.entity.MCSTR;

@Mapper
public interface MCSTRMapper {

	Integer create(MCSTR mcstr);

	@Delete("DELETE from ADC_MCSTR WHERE ID = #{id}")
	Integer removeByStoreId(Integer storeId);

}
