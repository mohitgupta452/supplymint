package com.supplymint.layer.data.tenant.procurement.entity;

import java.time.OffsetDateTime;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.supplymint.util.PILocalDatetimeSerializer;

public class FinCharge {
	private int id;
	private int detailId;
	private String chgCode;
	private String chgName;
	private String seq;
	private String finFormula;
	private String finChgRate;
	private String finSource;
	private String gstComponent;
	private String isTax;
	private String sign;
	private String finChgOperationLevel;
	private String calculationBasis;

	private String active;
	private String status;
	private String ipAddress;
	private String createdBy;

	private ObjectNode charges;
	public FinChargeDetail chargesPojo;
	private String chargesString;

	private ObjectNode rates;
	private FinChargeRateDetail ratesPojo;
	private String ratesString;

	private String subTotal;

	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	private OffsetDateTime createdTime;
	private String updatedBy;

	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	private OffsetDateTime updationTime;
	private String additional;

	private String discountType;
	private String discountValue;
	private String finalRate;

	public FinCharge() {
		super();
	}

	public FinCharge(int id, int detailId, String chgCode, String chgName, String seq, String finFormula,
			String finChgRate, String finSource, String gstComponent, String isTax, String sign,
			String finChgOperationLevel, String calculationBasis, String active, String status, String ipAddress,
			String createdBy, ObjectNode charges, FinChargeDetail chargesPojo, String chargesString, ObjectNode rates,
			FinChargeRateDetail ratesPojo, String ratesString, String subTotal, OffsetDateTime createdTime,
			String updatedBy, OffsetDateTime updationTime, String additional, String discountType, String discountValue,
			String finalRate) {
		super();
		this.id = id;
		this.detailId = detailId;
		this.chgCode = chgCode;
		this.chgName = chgName;
		this.seq = seq;
		this.finFormula = finFormula;
		this.finChgRate = finChgRate;
		this.finSource = finSource;
		this.gstComponent = gstComponent;
		this.isTax = isTax;
		this.sign = sign;
		this.finChgOperationLevel = finChgOperationLevel;
		this.calculationBasis = calculationBasis;
		this.active = active;
		this.status = status;
		this.ipAddress = ipAddress;
		this.createdBy = createdBy;
		this.charges = charges;
		this.chargesPojo = chargesPojo;
		this.chargesString = chargesString;
		this.rates = rates;
		this.ratesPojo = ratesPojo;
		this.ratesString = ratesString;
		this.subTotal = subTotal;
		this.createdTime = createdTime;
		this.updatedBy = updatedBy;
		this.updationTime = updationTime;
		this.additional = additional;
		this.discountType = discountType;
		this.discountValue = discountValue;
		this.finalRate = finalRate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getDetailId() {
		return detailId;
	}

	public void setDetailId(int detailId) {
		this.detailId = detailId;
	}

	public String getChgCode() {
		return chgCode;
	}

	public void setChgCode(String chgCode) {
		this.chgCode = chgCode;
	}

	public String getChgName() {
		return chgName;
	}

	public void setChgName(String chgName) {
		this.chgName = chgName;
	}

	public String getSeq() {
		return seq;
	}

	public void setSeq(String seq) {
		this.seq = seq;
	}

	public String getFinFormula() {
		return finFormula;
	}

	public void setFinFormula(String finFormula) {
		this.finFormula = finFormula;
	}

	public String getFinChgRate() {
		return finChgRate;
	}

	public void setFinChgRate(String finChgRate) {
		this.finChgRate = finChgRate;
	}

	public String getFinSource() {
		return finSource;
	}

	public void setFinSource(String finSource) {
		this.finSource = finSource;
	}

	public String getGstComponent() {
		return gstComponent;
	}

	public void setGstComponent(String gstComponent) {
		this.gstComponent = gstComponent;
	}

	public String getIsTax() {
		return isTax;
	}

	public void setIsTax(String isTax) {
		this.isTax = isTax;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getFinChgOperationLevel() {
		return finChgOperationLevel;
	}

	public void setFinChgOperationLevel(String finChgOperationLevel) {
		this.finChgOperationLevel = finChgOperationLevel;
	}

	public String getCalculationBasis() {
		return calculationBasis;
	}

	public void setCalculationBasis(String calculationBasis) {
		this.calculationBasis = calculationBasis;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public ObjectNode getCharges() {
		return charges;
	}

	public void setCharges(ObjectNode charges) {
		this.charges = charges;
	}

	public FinChargeDetail getChargesPojo() {
		return chargesPojo;
	}

	public void setChargesPojo(FinChargeDetail chargesPojo) {
		this.chargesPojo = chargesPojo;
	}

	public String getChargesString() {
		return chargesString;
	}

	public void setChargesString(String chargesString) {
		this.chargesString = chargesString;
	}

	public ObjectNode getRates() {
		return rates;
	}

	public void setRates(ObjectNode rates) {
		this.rates = rates;
	}

	public FinChargeRateDetail getRatesPojo() {
		return ratesPojo;
	}

	public void setRatesPojo(FinChargeRateDetail ratesPojo) {
		this.ratesPojo = ratesPojo;
	}

	public String getRatesString() {
		return ratesString;
	}

	public void setRatesString(String ratesString) {
		this.ratesString = ratesString;
	}

	public String getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(String subTotal) {
		this.subTotal = subTotal;
	}

	public OffsetDateTime getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(OffsetDateTime createdTime) {
		this.createdTime = createdTime;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public OffsetDateTime getUpdationTime() {
		return updationTime;
	}

	public void setUpdationTime(OffsetDateTime updationTime) {
		this.updationTime = updationTime;
	}

	public String getAdditional() {
		return additional;
	}

	public void setAdditional(String additional) {
		this.additional = additional;
	}

	public String getDiscountType() {
		return discountType;
	}

	public void setDiscountType(String discountType) {
		this.discountType = discountType;
	}

	public String getDiscountValue() {
		return discountValue;
	}

	public void setDiscountValue(String discountValue) {
		this.discountValue = discountValue;
	}

	public String getFinalRate() {
		return finalRate;
	}

	public void setFinalRate(String finalRate) {
		this.finalRate = finalRate;
	}

	@Override
	public String toString() {
		return "FinCharge [id=" + id + ", detailId=" + detailId + ", chgCode=" + chgCode + ", chgName=" + chgName
				+ ", seq=" + seq + ", finFormula=" + finFormula + ", finChgRate=" + finChgRate + ", finSource="
				+ finSource + ", gstComponent=" + gstComponent + ", isTax=" + isTax + ", sign=" + sign
				+ ", finChgOperationLevel=" + finChgOperationLevel + ", calculationBasis=" + calculationBasis
				+ ", active=" + active + ", status=" + status + ", ipAddress=" + ipAddress + ", createdBy=" + createdBy
				+ ", charges=" + charges + ", chargesPojo=" + chargesPojo + ", chargesString=" + chargesString
				+ ", rates=" + rates + ", ratesPojo=" + ratesPojo + ", ratesString=" + ratesString + ", subTotal="
				+ subTotal + ", createdTime=" + createdTime + ", updatedBy=" + updatedBy + ", updationTime="
				+ updationTime + ", additional=" + additional + ", discountType=" + discountType + ", discountValue="
				+ discountValue + ", finalRate=" + finalRate + "]";
	}

}
