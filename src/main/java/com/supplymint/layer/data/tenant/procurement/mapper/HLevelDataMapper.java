package com.supplymint.layer.data.tenant.procurement.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import com.supplymint.layer.data.tenant.procurement.entity.HLevelData;

@Mapper
public interface HLevelDataMapper {

	@Select("SELECT * FROM HLEVELDATA")
	List<HLevelData> findDistinctHierarachyLevels();
	
}
