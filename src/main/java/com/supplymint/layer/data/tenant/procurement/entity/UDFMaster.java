package com.supplymint.layer.data.tenant.procurement.entity;

import java.time.OffsetDateTime;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.supplymint.util.LocalDatetimeDeserializer;
import com.supplymint.util.PILocalDatetimeSerializer;

public class UDFMaster {

	private String udf1;
	private String udf2;
	private String udf3;
	private String udf4;
	private String udf5;
	private String udf6;
	private String udf7;
	private String udf8;
	private String udf9;
	private String udf10;
	private String udf11;
	private String udf12;
	private String udf13;
	private String udf14;
	private String udf15;
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	private OffsetDateTime udf16;
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	private OffsetDateTime udf17;
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	private OffsetDateTime udf18;
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	private OffsetDateTime udf19;
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	private OffsetDateTime udf20;

	private String itemudf1;
	private String itemudf2;
	private String itemudf3;
	private String itemudf4;
	private String itemudf5;
	private String itemudf6;
	private String itemudf7;
	private String itemudf8;
	private String itemudf9;
	private String itemudf10;
	private String itemudf11;
	private String itemudf12;
	private String itemudf13;
	private String itemudf14;
	private String itemudf15;
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	private OffsetDateTime itemudf16;
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	private OffsetDateTime itemudf17;
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	private OffsetDateTime itemudf18;
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	private OffsetDateTime itemudf19;
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	private OffsetDateTime itemudf20;

	public UDFMaster() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UDFMaster(String udf1, String udf2, String udf3, String udf4, String udf5, String udf6, String udf7,
			String udf8, String udf9, String udf10, String udf11, String udf12, String udf13, String udf14,
			String udf15, OffsetDateTime udf16, OffsetDateTime udf17, OffsetDateTime udf18, OffsetDateTime udf19,
			OffsetDateTime udf20, String itemudf1, String itemudf2, String itemudf3, String itemudf4, String itemudf5,
			String itemudf6, String itemudf7, String itemudf8, String itemudf9, String itemudf10, String itemudf11,
			String itemudf12, String itemudf13, String itemudf14, String itemudf15, OffsetDateTime itemudf16,
			OffsetDateTime itemudf17, OffsetDateTime itemudf18, OffsetDateTime itemudf19, OffsetDateTime itemudf20) {
		super();
		this.udf1 = udf1;
		this.udf2 = udf2;
		this.udf3 = udf3;
		this.udf4 = udf4;
		this.udf5 = udf5;
		this.udf6 = udf6;
		this.udf7 = udf7;
		this.udf8 = udf8;
		this.udf9 = udf9;
		this.udf10 = udf10;
		this.udf11 = udf11;
		this.udf12 = udf12;
		this.udf13 = udf13;
		this.udf14 = udf14;
		this.udf15 = udf15;
		this.udf16 = udf16;
		this.udf17 = udf17;
		this.udf18 = udf18;
		this.udf19 = udf19;
		this.udf20 = udf20;
		this.itemudf1 = itemudf1;
		this.itemudf2 = itemudf2;
		this.itemudf3 = itemudf3;
		this.itemudf4 = itemudf4;
		this.itemudf5 = itemudf5;
		this.itemudf6 = itemudf6;
		this.itemudf7 = itemudf7;
		this.itemudf8 = itemudf8;
		this.itemudf9 = itemudf9;
		this.itemudf10 = itemudf10;
		this.itemudf11 = itemudf11;
		this.itemudf12 = itemudf12;
		this.itemudf13 = itemudf13;
		this.itemudf14 = itemudf14;
		this.itemudf15 = itemudf15;
		this.itemudf16 = itemudf16;
		this.itemudf17 = itemudf17;
		this.itemudf18 = itemudf18;
		this.itemudf19 = itemudf19;
		this.itemudf20 = itemudf20;
	}

	public String getUdf1() {
		return udf1;
	}

	public void setUdf1(String udf1) {
		this.udf1 = udf1;
	}

	public String getUdf2() {
		return udf2;
	}

	public void setUdf2(String udf2) {
		this.udf2 = udf2;
	}

	public String getUdf3() {
		return udf3;
	}

	public void setUdf3(String udf3) {
		this.udf3 = udf3;
	}

	public String getUdf4() {
		return udf4;
	}

	public void setUdf4(String udf4) {
		this.udf4 = udf4;
	}

	public String getUdf5() {
		return udf5;
	}

	public void setUdf5(String udf5) {
		this.udf5 = udf5;
	}

	public String getUdf6() {
		return udf6;
	}

	public void setUdf6(String udf6) {
		this.udf6 = udf6;
	}

	public String getUdf7() {
		return udf7;
	}

	public void setUdf7(String udf7) {
		this.udf7 = udf7;
	}

	public String getUdf8() {
		return udf8;
	}

	public void setUdf8(String udf8) {
		this.udf8 = udf8;
	}

	public String getUdf9() {
		return udf9;
	}

	public void setUdf9(String udf9) {
		this.udf9 = udf9;
	}

	public String getUdf10() {
		return udf10;
	}

	public void setUdf10(String udf10) {
		this.udf10 = udf10;
	}

	public String getUdf11() {
		return udf11;
	}

	public void setUdf11(String udf11) {
		this.udf11 = udf11;
	}

	public String getUdf12() {
		return udf12;
	}

	public void setUdf12(String udf12) {
		this.udf12 = udf12;
	}

	public String getUdf13() {
		return udf13;
	}

	public void setUdf13(String udf13) {
		this.udf13 = udf13;
	}

	public String getUdf14() {
		return udf14;
	}

	public void setUdf14(String udf14) {
		this.udf14 = udf14;
	}

	public String getUdf15() {
		return udf15;
	}

	public void setUdf15(String udf15) {
		this.udf15 = udf15;
	}

	public OffsetDateTime getUdf16() {
		return udf16;
	}

	public void setUdf16(OffsetDateTime udf16) {
		this.udf16 = udf16;
	}

	public OffsetDateTime getUdf17() {
		return udf17;
	}

	public void setUdf17(OffsetDateTime udf17) {
		this.udf17 = udf17;
	}

	public OffsetDateTime getUdf18() {
		return udf18;
	}

	public void setUdf18(OffsetDateTime udf18) {
		this.udf18 = udf18;
	}

	public OffsetDateTime getUdf19() {
		return udf19;
	}

	public void setUdf19(OffsetDateTime udf19) {
		this.udf19 = udf19;
	}

	public OffsetDateTime getUdf20() {
		return udf20;
	}

	public void setUdf20(OffsetDateTime udf20) {
		this.udf20 = udf20;
	}

	public String getItemudf1() {
		return itemudf1;
	}

	public void setItemudf1(String itemudf1) {
		this.itemudf1 = itemudf1;
	}

	public String getItemudf2() {
		return itemudf2;
	}

	public void setItemudf2(String itemudf2) {
		this.itemudf2 = itemudf2;
	}

	public String getItemudf3() {
		return itemudf3;
	}

	public void setItemudf3(String itemudf3) {
		this.itemudf3 = itemudf3;
	}

	public String getItemudf4() {
		return itemudf4;
	}

	public void setItemudf4(String itemudf4) {
		this.itemudf4 = itemudf4;
	}

	public String getItemudf5() {
		return itemudf5;
	}

	public void setItemudf5(String itemudf5) {
		this.itemudf5 = itemudf5;
	}

	public String getItemudf6() {
		return itemudf6;
	}

	public void setItemudf6(String itemudf6) {
		this.itemudf6 = itemudf6;
	}

	public String getItemudf7() {
		return itemudf7;
	}

	public void setItemudf7(String itemudf7) {
		this.itemudf7 = itemudf7;
	}

	public String getItemudf8() {
		return itemudf8;
	}

	public void setItemudf8(String itemudf8) {
		this.itemudf8 = itemudf8;
	}

	public String getItemudf9() {
		return itemudf9;
	}

	public void setItemudf9(String itemudf9) {
		this.itemudf9 = itemudf9;
	}

	public String getItemudf10() {
		return itemudf10;
	}

	public void setItemudf10(String itemudf10) {
		this.itemudf10 = itemudf10;
	}

	public String getItemudf11() {
		return itemudf11;
	}

	public void setItemudf11(String itemudf11) {
		this.itemudf11 = itemudf11;
	}

	public String getItemudf12() {
		return itemudf12;
	}

	public void setItemudf12(String itemudf12) {
		this.itemudf12 = itemudf12;
	}

	public String getItemudf13() {
		return itemudf13;
	}

	public void setItemudf13(String itemudf13) {
		this.itemudf13 = itemudf13;
	}

	public String getItemudf14() {
		return itemudf14;
	}

	public void setItemudf14(String itemudf14) {
		this.itemudf14 = itemudf14;
	}

	public String getItemudf15() {
		return itemudf15;
	}

	public void setItemudf15(String itemudf15) {
		this.itemudf15 = itemudf15;
	}

	public OffsetDateTime getItemudf16() {
		return itemudf16;
	}

	public void setItemudf16(OffsetDateTime itemudf16) {
		this.itemudf16 = itemudf16;
	}

	public OffsetDateTime getItemudf17() {
		return itemudf17;
	}

	public void setItemudf17(OffsetDateTime itemudf17) {
		this.itemudf17 = itemudf17;
	}

	public OffsetDateTime getItemudf18() {
		return itemudf18;
	}

	public void setItemudf18(OffsetDateTime itemudf18) {
		this.itemudf18 = itemudf18;
	}

	public OffsetDateTime getItemudf19() {
		return itemudf19;
	}

	public void setItemudf19(OffsetDateTime itemudf19) {
		this.itemudf19 = itemudf19;
	}

	public OffsetDateTime getItemudf20() {
		return itemudf20;
	}

	public void setItemudf20(OffsetDateTime itemudf20) {
		this.itemudf20 = itemudf20;
	}

	@Override
	public String toString() {
		return "PurchaseOrderUDF [udf1=" + udf1 + ", udf2=" + udf2 + ", udf3=" + udf3 + ", udf4=" + udf4 + ", udf5="
				+ udf5 + ", udf6=" + udf6 + ", udf7=" + udf7 + ", udf8=" + udf8 + ", udf9=" + udf9 + ", udf10=" + udf10
				+ ", udf11=" + udf11 + ", udf12=" + udf12 + ", udf13=" + udf13 + ", udf14=" + udf14 + ", udf15=" + udf15
				+ ", udf16=" + udf16 + ", udf17=" + udf17 + ", udf18=" + udf18 + ", udf19=" + udf19 + ", udf20=" + udf20
				+ ", itemudf1=" + itemudf1 + ", itemudf2=" + itemudf2 + ", itemudf3=" + itemudf3 + ", itemudf4="
				+ itemudf4 + ", itemudf5=" + itemudf5 + ", itemudf6=" + itemudf6 + ", itemudf7=" + itemudf7
				+ ", itemudf8=" + itemudf8 + ", itemudf9=" + itemudf9 + ", itemudf10=" + itemudf10 + ", itemudf11="
				+ itemudf11 + ", itemudf12=" + itemudf12 + ", itemudf13=" + itemudf13 + ", itemudf14=" + itemudf14
				+ ", itemudf15=" + itemudf15 + ", itemudf16=" + itemudf16 + ", itemudf17=" + itemudf17 + ", itemudf18="
				+ itemudf18 + ", itemudf19=" + itemudf19 + ", itemudf20=" + itemudf20 + "]";
	}

}
