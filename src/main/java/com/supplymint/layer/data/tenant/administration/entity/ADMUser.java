package com.supplymint.layer.data.tenant.administration.entity;

import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.supplymint.util.DateFormatSerializer;
import com.supplymint.util.LocalDateTimeSerializer;

public class ADMUser {

	private int userId;
	private int partnerEnterpriseId;
	private String partnerEnterpriseName;
	private String firstName;
	private String middleName;
	private String lastName;
	private String userName;
	private String email;
	private String mobileNumber;
	private String workPhone;
	private String address;
	private String accessMode;
	private char active;
	private String status;
	private String ipAddress;
	private String createdBy;
	private String roles;

	@JsonSerialize(using = LocalDateTimeSerializer.class)
//	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	private OffsetDateTime createdTime;
	private String updatedBy;

	// @JsonProperty("updationTime")
	// @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd
	// HH:mm:ss.SSSSSSx")

	@JsonSerialize(using = LocalDateTimeSerializer.class)
//	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	private OffsetDateTime updationTime;
	private String additional;
	private String imageUrl;
	@JsonSerialize(using = DateFormatSerializer.class)
	private Date imageUpdatedOn;
	@JsonProperty(access = Access.WRITE_ONLY)
	private String enterpriseUserName;

	private String[] userRoles;

	private ADMUserRole userRole;

	// this data member only use for pagination
	private int offset;
	private int pagesize;

	public ADMUser() {

	}

	public ADMUser(int userId, int partnerEnterpriseId, String partnerEnterpriseName, String firstName,
			String middleName, String lastName, String userName, String email, String mobileNumber, String workPhone,
			String address, String accessMode, char active, String status, String ipAddress, String createdBy,
			String roles, OffsetDateTime createdTime, String updatedBy, OffsetDateTime updationTime, String additional,
			String imageUrl, Date imageUpdatedOn, String enterpriseUserName, String[] userRoles, ADMUserRole userRole,
			int offset, int pagesize) {
		super();
		this.userId = userId;
		this.partnerEnterpriseId = partnerEnterpriseId;
		this.partnerEnterpriseName = partnerEnterpriseName;
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.userName = userName;
		this.email = email;
		this.mobileNumber = mobileNumber;
		this.workPhone = workPhone;
		this.address = address;
		this.accessMode = accessMode;
		this.active = active;
		this.status = status;
		this.ipAddress = ipAddress;
		this.createdBy = createdBy;
		this.roles = roles;
		this.createdTime = createdTime;
		this.updatedBy = updatedBy;
		this.updationTime = updationTime;
		this.additional = additional;
		this.imageUrl = imageUrl;
		this.imageUpdatedOn = imageUpdatedOn;
		this.enterpriseUserName = enterpriseUserName;
		this.userRoles = userRoles;
		this.userRole = userRole;
		this.offset = offset;
		this.pagesize = pagesize;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getPartnerEnterpriseId() {
		return partnerEnterpriseId;
	}

	public void setPartnerEnterpriseId(int partnerEnterpriseId) {
		this.partnerEnterpriseId = partnerEnterpriseId;
	}

	public String getPartnerEnterpriseName() {
		return partnerEnterpriseName;
	}

	public void setPartnerEnterpriseName(String partnerEnterpriseName) {
		this.partnerEnterpriseName = partnerEnterpriseName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getWorkPhone() {
		return workPhone;
	}

	public void setWorkPhone(String workPhone) {
		this.workPhone = workPhone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAccessMode() {
		return accessMode;
	}

	public void setAccessMode(String accessMode) {
		this.accessMode = accessMode;
	}

	public char getActive() {
		return active;
	}

	public void setActive(char active) {
		this.active = active;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getRoles() {
		return roles;
	}

	public void setRoles(String roles) {
		this.roles = roles;
	}

	public OffsetDateTime getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(OffsetDateTime createdTime) {
		this.createdTime = createdTime;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public OffsetDateTime getUpdationTime() {
		return updationTime;
	}

	public void setUpdationTime(OffsetDateTime updationTime) {
		this.updationTime = updationTime;
	}

	public String getAdditional() {
		return additional;
	}

	public void setAdditional(String additional) {
		this.additional = additional;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public Date getImageUpdatedOn() {
		return imageUpdatedOn;
	}

	public void setImageUpdatedOn(Date imageUpdatedOn) {
		this.imageUpdatedOn = imageUpdatedOn;
	}

	public String getEnterpriseUserName() {
		return enterpriseUserName;
	}

	public void setEnterpriseUserName(String enterpriseUserName) {
		this.enterpriseUserName = enterpriseUserName;
	}

	public String[] getUserRoles() {
		return userRoles;
	}

	public void setUserRoles(String[] userRoles) {
		this.userRoles = userRoles;
	}

	public ADMUserRole getUserRole() {
		return userRole;
	}

	public void setUserRole(ADMUserRole userRole) {
		this.userRole = userRole;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public int getPagesize() {
		return pagesize;
	}

	public void setPagesize(int pagesize) {
		this.pagesize = pagesize;
	}

	@Override
	public String toString() {
		return "ADMUser [userId=" + userId + ", partnerEnterpriseId=" + partnerEnterpriseId + ", partnerEnterpriseName="
				+ partnerEnterpriseName + ", firstName=" + firstName + ", middleName=" + middleName + ", lastName="
				+ lastName + ", userName=" + userName + ", email=" + email + ", mobileNumber=" + mobileNumber
				+ ", workPhone=" + workPhone + ", address=" + address + ", accessMode=" + accessMode + ", active="
				+ active + ", status=" + status + ", ipAddress=" + ipAddress + ", createdBy=" + createdBy + ", roles="
				+ roles + ", createdTime=" + createdTime + ", updatedBy=" + updatedBy + ", updationTime=" + updationTime
				+ ", additional=" + additional + ", imageUrl=" + imageUrl + ", imageUpdatedOn=" + imageUpdatedOn
				+ ", enterpriseUserName=" + enterpriseUserName + ", userRoles=" + Arrays.toString(userRoles)
				+ ", userRole=" + userRole + ", offset=" + offset + ", pagesize=" + pagesize + "]";
	}

}
