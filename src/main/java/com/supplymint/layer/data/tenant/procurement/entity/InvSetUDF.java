package com.supplymint.layer.data.tenant.procurement.entity;

public class InvSetUDF {

	private int code;
	private String name;
	private char ext;
	private String udfType;

	public InvSetUDF() {
		// TODO Auto-generated constructor stub
	}

	public InvSetUDF(int code, String name, char ext, String udfType) {
		super();
		this.code = code;
		this.name = name;
		this.ext = ext;
		this.udfType = udfType;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public char getExt() {
		return ext;
	}

	public void setExt(char ext) {
		this.ext = ext;
	}

	public String getUdfType() {
		return udfType;
	}

	public void setUdfType(String udfType) {
		this.udfType = udfType;
	}

	@Override
	public String toString() {
		return "InvSetUDF [code=" + code + ", name=" + name + ", ext=" + ext + ", udfType=" + udfType + "]";
	}

}
