package com.supplymint.layer.data.tenant.procurement.entity;

public class CatMaster {
	private int id;
	private String code;
	private String cname;
	private String hl3Code;
	private String hl3Name;

	public CatMaster() {
		super();
	}

	public CatMaster(String code, String cname, String hl3Code, String hl3Name) {
		super();
		this.code = code;
		this.cname = cname;
		this.hl3Code = hl3Code;
		this.hl3Name = hl3Name;
	}

	public CatMaster(int id, String code, String cname, String hl3Code, String hl3Name) {
		super();
		this.id = id;
		this.code = code;
		this.cname = cname;
		this.hl3Code = hl3Code;
		this.hl3Name = hl3Name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCname() {
		return cname;
	}

	public void setCname(String cname) {
		this.cname = cname;
	}

	public String getHl3Code() {
		return hl3Code;
	}

	public void setHl3Code(String hl3Code) {
		this.hl3Code = hl3Code;
	}

	public String getHl3Name() {
		return hl3Name;
	}

	public void setHl3Name(String hl3Name) {
		this.hl3Name = hl3Name;
	}

	@Override
	public String toString() {
		return "CatMaster [code=" + code + ", cname=" + cname + ", hl3Code=" + hl3Code + ", hl3Name=" + hl3Name + "]";
	}

}
