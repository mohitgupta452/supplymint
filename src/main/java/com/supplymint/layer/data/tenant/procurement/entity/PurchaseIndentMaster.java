package com.supplymint.layer.data.tenant.procurement.entity;

public class PurchaseIndentMaster {
	public PurchaseIndent piKey;
	
	public PurchaseIndentMaster() {
		super();
	}

	public PurchaseIndent getPiKey() {
		return piKey;
	}

	public void setPiKey(PurchaseIndent piKey) {
		this.piKey = piKey;
	}

	@Override
	public String toString() {
		return "PurchaseIndentMaster [piKey=" + piKey + "]";
	}

	
	
}
