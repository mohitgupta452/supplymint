package com.supplymint.layer.data.tenant.procurement.entity;

import java.util.List;

public class PurchaseOrderNode {

	private List<PurchaseOrder> poData;

	public PurchaseOrderNode() {
		super();
	}

	public PurchaseOrderNode(List<PurchaseOrder> poData) {
		super();
		this.poData = poData;
	}

	public List<PurchaseOrder> getPoData() {
		return poData;
	}

	public void setPoData(List<PurchaseOrder> poData) {
		this.poData = poData;
	}

}
