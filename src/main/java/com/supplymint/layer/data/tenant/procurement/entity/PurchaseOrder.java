package com.supplymint.layer.data.tenant.procurement.entity;

import java.time.OffsetDateTime;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.supplymint.util.LocalDatetimeDeserializer;
import com.supplymint.util.PILocalDatetimeSerializer;

public class PurchaseOrder {

	public String vendorName;
	public String intGCode;
	public String intGHeaderId;
	public String intGLineId;
	public String orderNo;

	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	public OffsetDateTime orderDate;
	public String vendorId;
	public String transporterId;
	public String agentId;
	public double agentRate;
	public String poRemarks;
	public String createdById;

	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	public OffsetDateTime validFrom;

	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	public OffsetDateTime validTo;
	public String merchandiserId;
	public String siteId;
	public String itemId;
	public String setRemarks;
	public double setRatio;
	public String articleId;
	public String itemName;
	public String cCode1;
	public String cCode2;
	public String cCode3;
	public String cCode4;
	public String cCode5;
	public String cCode6;
	public String cName1;
	public String cName2;
	public String cName3;
	public String cName4;
	public String cName5;
	public String cName6;
	public String desc1;
	public String desc2;
	public String desc3;
	public String desc4;
	public String desc5;
	public String desc6;
	public double mrp;
	public double listedMRP;
	public double wsp;
	public String uom;
	public String materialType;
	public double qty;
	public double rate;
	public String poItemRemarks;
	public String isImported;
	public String termCode;
	public String isValidated;
	public String validationError;
	public String docCode;
	public String setHeaderId;
	public int key;

	public String hsnCode;

	public String poudfStrin01;
	public String poudfStrin02;
	public String poudfStrin03;
	public String poudfStrin04;
	public String poudfStrin05;
	public String poudfStrin06;
	public String poudfStrin07;
	public String poudfStrin08;
	public String poudfStrin09;
	public String poudfStrin010;
	public String poudfNum01;
	public String poudfNum02;
	public String poudfNum03;
	public String poudfNum04;
	public String poudfNum05;

	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	public OffsetDateTime poudfDate01;

	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	public OffsetDateTime poudfDate02;
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	public OffsetDateTime poudfDate03;
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	public OffsetDateTime poudfDate04;
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	public OffsetDateTime poudfDate05;
	public String smUDFStrin01;
	public String smUDFStrin02;
	public String smUDFStrin03;
	public String smUDFStrin04;
	public String smUDFStrin05;
	public String smUDFStrin06;
	public String smUDFStrin07;
	public String smUDFStrin08;
	public String smUDFStrin09;
	public String smUDFStrin010;
	public String smUDFNum01;
	public String smUDFNum02;
	public String smUDFNum03;
	public String smUDFNum04;
	public String smUDFNum05;
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	public OffsetDateTime smUDFDate01;
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	public OffsetDateTime smUDFDate02;
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	public OffsetDateTime smUDFDate03;
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	public OffsetDateTime smUDFDate04;
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	public OffsetDateTime smUDFDate05;
	public String imUDFString01;
	public String imUDFString02;
	public String imUDFString03;
	public String imUDFString04;
	public String imUDFString05;
	public String imUDFString06;
	public String imUDFString07;
	public String imUDFString08;
	public String imUDFString09;
	public String imUDFString010;
	public String imUDFNum01;
	public String imUDFNum02;
	public String imUDFNum03;
	public String imUDFNum04;
	public String imUDFNum05;
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	public OffsetDateTime imUDFDate01;
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	public OffsetDateTime imUDFDate02;
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	public OffsetDateTime imUDFDate03;
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	public OffsetDateTime imUDFDate04;
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	public OffsetDateTime imUDFDate05;

	public String poReferenceId;
	
	public String orgId;

	public PurchaseOrder() {
		super();
	}

	public PurchaseOrder(String vendorName, String orderNo, OffsetDateTime orderDate, String vendorId, String poRemarks,
			OffsetDateTime validFrom, OffsetDateTime validTo, double mrp, double qty) {
		super();
		this.vendorName = vendorName;
		this.orderNo = orderNo;
		this.orderDate = orderDate;
		this.vendorId = vendorId;
		this.poRemarks = poRemarks;
		this.validFrom = validFrom;
		this.validTo = validTo;
		this.mrp = mrp;
		this.qty = qty;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public String getIntGCode() {
		return intGCode;
	}

	public void setIntGCode(String intGCode) {
		this.intGCode = intGCode;
	}

	public String getIntGHeaderId() {
		return intGHeaderId;
	}

	public void setIntGHeaderId(String intGHeaderId) {
		this.intGHeaderId = intGHeaderId;
	}

	public String getIntGLineId() {
		return intGLineId;
	}

	public void setIntGLineId(String intGLineId) {
		this.intGLineId = intGLineId;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public OffsetDateTime getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(OffsetDateTime orderDate) {
		this.orderDate = orderDate;
	}

	public String getVendorId() {
		return vendorId;
	}

	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}

	public String getTransporterId() {
		return transporterId;
	}

	public void setTransporterId(String transporterId) {
		this.transporterId = transporterId;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public double getAgentRate() {
		return agentRate;
	}

	public void setAgentRate(double agentRate) {
		this.agentRate = agentRate;
	}

	public String getPoRemarks() {
		return poRemarks;
	}

	public void setPoRemarks(String poRemarks) {
		this.poRemarks = poRemarks;
	}

	public String getCreatedById() {
		return createdById;
	}

	public void setCreatedById(String createdById) {
		this.createdById = createdById;
	}

	public OffsetDateTime getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(OffsetDateTime validFrom) {
		this.validFrom = validFrom;
	}

	public OffsetDateTime getValidTo() {
		return validTo;
	}

	public void setValidTo(OffsetDateTime validTo) {
		this.validTo = validTo;
	}

	public String getMerchandiserId() {
		return merchandiserId;
	}

	public void setMerchandiserId(String merchandiserId) {
		this.merchandiserId = merchandiserId;
	}

	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getSetRemarks() {
		return setRemarks;
	}

	public void setSetRemarks(String setRemarks) {
		this.setRemarks = setRemarks;
	}

	public double getSetRatio() {
		return setRatio;
	}

	public void setSetRatio(double setRatio) {
		this.setRatio = setRatio;
	}

	public String getArticleId() {
		return articleId;
	}

	public void setArticleId(String articleId) {
		this.articleId = articleId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getcCode1() {
		return cCode1;
	}

	public void setcCode1(String cCode1) {
		this.cCode1 = cCode1;
	}

	public String getcCode2() {
		return cCode2;
	}

	public void setcCode2(String cCode2) {
		this.cCode2 = cCode2;
	}

	public String getcCode3() {
		return cCode3;
	}

	public void setcCode3(String cCode3) {
		this.cCode3 = cCode3;
	}

	public String getcCode4() {
		return cCode4;
	}

	public void setcCode4(String cCode4) {
		this.cCode4 = cCode4;
	}

	public String getcCode5() {
		return cCode5;
	}

	public void setcCode5(String cCode5) {
		this.cCode5 = cCode5;
	}

	public String getcCode6() {
		return cCode6;
	}

	public void setcCode6(String cCode6) {
		this.cCode6 = cCode6;
	}

	public String getcName1() {
		return cName1;
	}

	public void setcName1(String cName1) {
		this.cName1 = cName1;
	}

	public String getcName2() {
		return cName2;
	}

	public void setcName2(String cName2) {
		this.cName2 = cName2;
	}

	public String getcName3() {
		return cName3;
	}

	public void setcName3(String cName3) {
		this.cName3 = cName3;
	}

	public String getcName4() {
		return cName4;
	}

	public void setcName4(String cName4) {
		this.cName4 = cName4;
	}

	public String getcName5() {
		return cName5;
	}

	public void setcName5(String cName5) {
		this.cName5 = cName5;
	}

	public String getcName6() {
		return cName6;
	}

	public void setcName6(String cName6) {
		this.cName6 = cName6;
	}

	public String getDesc1() {
		return desc1;
	}

	public void setDesc1(String desc1) {
		this.desc1 = desc1;
	}

	public String getDesc2() {
		return desc2;
	}

	public void setDesc2(String desc2) {
		this.desc2 = desc2;
	}

	public String getDesc3() {
		return desc3;
	}

	public void setDesc3(String desc3) {
		this.desc3 = desc3;
	}

	public String getDesc4() {
		return desc4;
	}

	public void setDesc4(String desc4) {
		this.desc4 = desc4;
	}

	public String getDesc5() {
		return desc5;
	}

	public void setDesc5(String desc5) {
		this.desc5 = desc5;
	}

	public String getDesc6() {
		return desc6;
	}

	public void setDesc6(String desc6) {
		this.desc6 = desc6;
	}

	public double getMrp() {
		return mrp;
	}

	public void setMrp(double mrp) {
		this.mrp = mrp;
	}

	public double getListedMRP() {
		return listedMRP;
	}

	public void setListedMRP(double listedMRP) {
		this.listedMRP = listedMRP;
	}

	public double getWsp() {
		return wsp;
	}

	public void setWsp(double wsp) {
		this.wsp = wsp;
	}

	public String getUom() {
		return uom;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}

	public String getMaterialType() {
		return materialType;
	}

	public void setMaterialType(String materialType) {
		this.materialType = materialType;
	}

	public double getQty() {
		return qty;
	}

	public void setQty(double qty) {
		this.qty = qty;
	}

	public double getRate() {
		return rate;
	}

	public void setRate(double rate) {
		this.rate = rate;
	}

	public String getPoItemRemarks() {
		return poItemRemarks;
	}

	public void setPoItemRemarks(String poItemRemarks) {
		this.poItemRemarks = poItemRemarks;
	}

	public String getIsImported() {
		return isImported;
	}

	public void setIsImported(String isImported) {
		this.isImported = isImported;
	}

	public String getTermCode() {
		return termCode;
	}

	public void setTermCode(String termCode) {
		this.termCode = termCode;
	}

	public String getIsValidated() {
		return isValidated;
	}

	public void setIsValidated(String isValidated) {
		this.isValidated = isValidated;
	}

	public String getValidationError() {
		return validationError;
	}

	public void setValidationError(String validationError) {
		this.validationError = validationError;
	}

	public String getDocCode() {
		return docCode;
	}

	public void setDocCode(String docCode) {
		this.docCode = docCode;
	}

	public String getSetHeaderId() {
		return setHeaderId;
	}

	public void setSetHeaderId(String setHeaderId) {
		this.setHeaderId = setHeaderId;
	}

	public int getKey() {
		return key;
	}

	public void setKey(int key) {
		this.key = key;
	}

	public String getPoudfStrin01() {
		return poudfStrin01;
	}

	public void setPoudfStrin01(String poudfStrin01) {
		this.poudfStrin01 = poudfStrin01;
	}

	public String getPoudfStrin02() {
		return poudfStrin02;
	}

	public void setPoudfStrin02(String poudfStrin02) {
		this.poudfStrin02 = poudfStrin02;
	}

	public String getPoudfStrin03() {
		return poudfStrin03;
	}

	public void setPoudfStrin03(String poudfStrin03) {
		this.poudfStrin03 = poudfStrin03;
	}

	public String getPoudfStrin04() {
		return poudfStrin04;
	}

	public void setPoudfStrin04(String poudfStrin04) {
		this.poudfStrin04 = poudfStrin04;
	}

	public String getPoudfStrin05() {
		return poudfStrin05;
	}

	public void setPoudfStrin05(String poudfStrin05) {
		this.poudfStrin05 = poudfStrin05;
	}

	public String getPoudfStrin06() {
		return poudfStrin06;
	}

	public void setPoudfStrin06(String poudfStrin06) {
		this.poudfStrin06 = poudfStrin06;
	}

	public String getPoudfStrin07() {
		return poudfStrin07;
	}

	public void setPoudfStrin07(String poudfStrin07) {
		this.poudfStrin07 = poudfStrin07;
	}

	public String getPoudfStrin08() {
		return poudfStrin08;
	}

	public void setPoudfStrin08(String poudfStrin08) {
		this.poudfStrin08 = poudfStrin08;
	}

	public String getPoudfStrin09() {
		return poudfStrin09;
	}

	public void setPoudfStrin09(String poudfStrin09) {
		this.poudfStrin09 = poudfStrin09;
	}

	public String getPoudfStrin010() {
		return poudfStrin010;
	}

	public void setPoudfStrin010(String poudfStrin010) {
		this.poudfStrin010 = poudfStrin010;
	}

	public String getPoudfNum01() {
		return poudfNum01;
	}

	public void setPoudfNum01(String poudfNum01) {
		this.poudfNum01 = poudfNum01;
	}

	public String getPoudfNum02() {
		return poudfNum02;
	}

	public void setPoudfNum02(String poudfNum02) {
		this.poudfNum02 = poudfNum02;
	}

	public String getPoudfNum03() {
		return poudfNum03;
	}

	public void setPoudfNum03(String poudfNum03) {
		this.poudfNum03 = poudfNum03;
	}

	public String getPoudfNum04() {
		return poudfNum04;
	}

	public void setPoudfNum04(String poudfNum04) {
		this.poudfNum04 = poudfNum04;
	}

	public String getPoudfNum05() {
		return poudfNum05;
	}

	public void setPoudfNum05(String poudfNum05) {
		this.poudfNum05 = poudfNum05;
	}

	public OffsetDateTime getPoudfDate01() {
		return poudfDate01;
	}

	public void setPoudfDate01(OffsetDateTime poudfDate01) {
		this.poudfDate01 = poudfDate01;
	}

	public OffsetDateTime getPoudfDate02() {
		return poudfDate02;
	}

	public void setPoudfDate02(OffsetDateTime poudfDate02) {
		this.poudfDate02 = poudfDate02;
	}

	public OffsetDateTime getPoudfDate03() {
		return poudfDate03;
	}

	public void setPoudfDate03(OffsetDateTime poudfDate03) {
		this.poudfDate03 = poudfDate03;
	}

	public OffsetDateTime getPoudfDate04() {
		return poudfDate04;
	}

	public void setPoudfDate04(OffsetDateTime poudfDate04) {
		this.poudfDate04 = poudfDate04;
	}

	public OffsetDateTime getPoudfDate05() {
		return poudfDate05;
	}

	public void setPoudfDate05(OffsetDateTime poudfDate05) {
		this.poudfDate05 = poudfDate05;
	}

	public String getSmUDFStrin01() {
		return smUDFStrin01;
	}

	public void setSmUDFStrin01(String smUDFStrin01) {
		this.smUDFStrin01 = smUDFStrin01;
	}

	public String getSmUDFStrin02() {
		return smUDFStrin02;
	}

	public void setSmUDFStrin02(String smUDFStrin02) {
		this.smUDFStrin02 = smUDFStrin02;
	}

	public String getSmUDFStrin03() {
		return smUDFStrin03;
	}

	public void setSmUDFStrin03(String smUDFStrin03) {
		this.smUDFStrin03 = smUDFStrin03;
	}

	public String getSmUDFStrin04() {
		return smUDFStrin04;
	}

	public void setSmUDFStrin04(String smUDFStrin04) {
		this.smUDFStrin04 = smUDFStrin04;
	}

	public String getSmUDFStrin05() {
		return smUDFStrin05;
	}

	public void setSmUDFStrin05(String smUDFStrin05) {
		this.smUDFStrin05 = smUDFStrin05;
	}

	public String getSmUDFStrin06() {
		return smUDFStrin06;
	}

	public void setSmUDFStrin06(String smUDFStrin06) {
		this.smUDFStrin06 = smUDFStrin06;
	}

	public String getSmUDFStrin07() {
		return smUDFStrin07;
	}

	public void setSmUDFStrin07(String smUDFStrin07) {
		this.smUDFStrin07 = smUDFStrin07;
	}

	public String getSmUDFStrin08() {
		return smUDFStrin08;
	}

	public void setSmUDFStrin08(String smUDFStrin08) {
		this.smUDFStrin08 = smUDFStrin08;
	}

	public String getSmUDFStrin09() {
		return smUDFStrin09;
	}

	public void setSmUDFStrin09(String smUDFStrin09) {
		this.smUDFStrin09 = smUDFStrin09;
	}

	public String getSmUDFStrin010() {
		return smUDFStrin010;
	}

	public void setSmUDFStrin010(String smUDFStrin010) {
		this.smUDFStrin010 = smUDFStrin010;
	}

	public String getSmUDFNum01() {
		return smUDFNum01;
	}

	public void setSmUDFNum01(String smUDFNum01) {
		this.smUDFNum01 = smUDFNum01;
	}

	public String getSmUDFNum02() {
		return smUDFNum02;
	}

	public void setSmUDFNum02(String smUDFNum02) {
		this.smUDFNum02 = smUDFNum02;
	}

	public String getSmUDFNum03() {
		return smUDFNum03;
	}

	public void setSmUDFNum03(String smUDFNum03) {
		this.smUDFNum03 = smUDFNum03;
	}

	public String getSmUDFNum04() {
		return smUDFNum04;
	}

	public void setSmUDFNum04(String smUDFNum04) {
		this.smUDFNum04 = smUDFNum04;
	}

	public String getSmUDFNum05() {
		return smUDFNum05;
	}

	public void setSmUDFNum05(String smUDFNum05) {
		this.smUDFNum05 = smUDFNum05;
	}

	public OffsetDateTime getSmUDFDate01() {
		return smUDFDate01;
	}

	public void setSmUDFDate01(OffsetDateTime smUDFDate01) {
		this.smUDFDate01 = smUDFDate01;
	}

	public OffsetDateTime getSmUDFDate02() {
		return smUDFDate02;
	}

	public void setSmUDFDate02(OffsetDateTime smUDFDate02) {
		this.smUDFDate02 = smUDFDate02;
	}

	public OffsetDateTime getSmUDFDate03() {
		return smUDFDate03;
	}

	public void setSmUDFDate03(OffsetDateTime smUDFDate03) {
		this.smUDFDate03 = smUDFDate03;
	}

	public OffsetDateTime getSmUDFDate04() {
		return smUDFDate04;
	}

	public void setSmUDFDate04(OffsetDateTime smUDFDate04) {
		this.smUDFDate04 = smUDFDate04;
	}

	public OffsetDateTime getSmUDFDate05() {
		return smUDFDate05;
	}

	public void setSmUDFDate05(OffsetDateTime smUDFDate05) {
		this.smUDFDate05 = smUDFDate05;
	}

	public String getImUDFString01() {
		return imUDFString01;
	}

	public void setImUDFString01(String imUDFString01) {
		this.imUDFString01 = imUDFString01;
	}

	public String getImUDFString02() {
		return imUDFString02;
	}

	public void setImUDFString02(String imUDFString02) {
		this.imUDFString02 = imUDFString02;
	}

	public String getImUDFString03() {
		return imUDFString03;
	}

	public void setImUDFString03(String imUDFString03) {
		this.imUDFString03 = imUDFString03;
	}

	public String getImUDFString04() {
		return imUDFString04;
	}

	public void setImUDFString04(String imUDFString04) {
		this.imUDFString04 = imUDFString04;
	}

	public String getImUDFString05() {
		return imUDFString05;
	}

	public void setImUDFString05(String imUDFString05) {
		this.imUDFString05 = imUDFString05;
	}

	public String getImUDFString06() {
		return imUDFString06;
	}

	public void setImUDFString06(String imUDFString06) {
		this.imUDFString06 = imUDFString06;
	}

	public String getImUDFString07() {
		return imUDFString07;
	}

	public void setImUDFString07(String imUDFString07) {
		this.imUDFString07 = imUDFString07;
	}

	public String getImUDFString08() {
		return imUDFString08;
	}

	public void setImUDFString08(String imUDFString08) {
		this.imUDFString08 = imUDFString08;
	}

	public String getImUDFString09() {
		return imUDFString09;
	}

	public void setImUDFString09(String imUDFString09) {
		this.imUDFString09 = imUDFString09;
	}

	public String getImUDFString010() {
		return imUDFString010;
	}

	public void setImUDFString010(String imUDFString010) {
		this.imUDFString010 = imUDFString010;
	}

	public String getImUDFNum01() {
		return imUDFNum01;
	}

	public void setImUDFNum01(String imUDFNum01) {
		this.imUDFNum01 = imUDFNum01;
	}

	public String getImUDFNum02() {
		return imUDFNum02;
	}

	public void setImUDFNum02(String imUDFNum02) {
		this.imUDFNum02 = imUDFNum02;
	}

	public String getImUDFNum03() {
		return imUDFNum03;
	}

	public void setImUDFNum03(String imUDFNum03) {
		this.imUDFNum03 = imUDFNum03;
	}

	public String getImUDFNum04() {
		return imUDFNum04;
	}

	public void setImUDFNum04(String imUDFNum04) {
		this.imUDFNum04 = imUDFNum04;
	}

	public String getImUDFNum05() {
		return imUDFNum05;
	}

	public void setImUDFNum05(String imUDFNum05) {
		this.imUDFNum05 = imUDFNum05;
	}

	public OffsetDateTime getImUDFDate01() {
		return imUDFDate01;
	}

	public void setImUDFDate01(OffsetDateTime imUDFDate01) {
		this.imUDFDate01 = imUDFDate01;
	}

	public OffsetDateTime getImUDFDate02() {
		return imUDFDate02;
	}

	public void setImUDFDate02(OffsetDateTime imUDFDate02) {
		this.imUDFDate02 = imUDFDate02;
	}

	public OffsetDateTime getImUDFDate03() {
		return imUDFDate03;
	}

	public void setImUDFDate03(OffsetDateTime imUDFDate03) {
		this.imUDFDate03 = imUDFDate03;
	}

	public OffsetDateTime getImUDFDate04() {
		return imUDFDate04;
	}

	public void setImUDFDate04(OffsetDateTime imUDFDate04) {
		this.imUDFDate04 = imUDFDate04;
	}

	public OffsetDateTime getImUDFDate05() {
		return imUDFDate05;
	}

	public void setImUDFDate05(OffsetDateTime imUDFDate05) {
		this.imUDFDate05 = imUDFDate05;
	}

	public String getPoReferenceId() {
		return poReferenceId;
	}

	public void setPoReferenceId(String poReferenceId) {
		this.poReferenceId = poReferenceId;
	}

	public String getHsnCode() {
		return hsnCode;
	}

	public void setHsnCode(String hsnCode) {
		this.hsnCode = hsnCode;
	}
	
	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	@Override
	public String toString() {
		return "PurchaseOrder [vendorName=" + vendorName + ", intGCode=" + intGCode + ", intGHeaderId=" + intGHeaderId
				+ ", intGLineId=" + intGLineId + ", orderNo=" + orderNo + ", orderDate=" + orderDate + ", vendorId="
				+ vendorId + ", transporterId=" + transporterId + ", agentId=" + agentId + ", agentRate=" + agentRate
				+ ", poRemarks=" + poRemarks + ", createdById=" + createdById + ", validFrom=" + validFrom
				+ ", validTo=" + validTo + ", merchandiserId=" + merchandiserId + ", siteId=" + siteId + ", itemId="
				+ itemId + ", setRemarks=" + setRemarks + ", setRatio=" + setRatio + ", articleId=" + articleId
				+ ", itemName=" + itemName + ", cCode1=" + cCode1 + ", cCode2=" + cCode2 + ", cCode3=" + cCode3
				+ ", cCode4=" + cCode4 + ", cCode5=" + cCode5 + ", cCode6=" + cCode6 + ", cName1=" + cName1
				+ ", cName2=" + cName2 + ", cName3=" + cName3 + ", cName4=" + cName4 + ", cName5=" + cName5
				+ ", cName6=" + cName6 + ", desc1=" + desc1 + ", desc2=" + desc2 + ", desc3=" + desc3 + ", desc4="
				+ desc4 + ", desc5=" + desc5 + ", desc6=" + desc6 + ", mrp=" + mrp + ", listedMRP=" + listedMRP
				+ ", wsp=" + wsp + ", uom=" + uom + ", materialType=" + materialType + ", qty=" + qty + ", rate=" + rate
				+ ", poItemRemarks=" + poItemRemarks + ", isImported=" + isImported + ", termCode=" + termCode
				+ ", isValidated=" + isValidated + ", validationError=" + validationError + ", docCode=" + docCode
				+ ", setHeaderId=" + setHeaderId + ", key=" + key + ", hsnCode=" + hsnCode + ", poudfStrin01="
				+ poudfStrin01 + ", poudfStrin02=" + poudfStrin02 + ", poudfStrin03=" + poudfStrin03 + ", poudfStrin04="
				+ poudfStrin04 + ", poudfStrin05=" + poudfStrin05 + ", poudfStrin06=" + poudfStrin06 + ", poudfStrin07="
				+ poudfStrin07 + ", poudfStrin08=" + poudfStrin08 + ", poudfStrin09=" + poudfStrin09
				+ ", poudfStrin010=" + poudfStrin010 + ", poudfNum01=" + poudfNum01 + ", poudfNum02=" + poudfNum02
				+ ", poudfNum03=" + poudfNum03 + ", poudfNum04=" + poudfNum04 + ", poudfNum05=" + poudfNum05
				+ ", poudfDate01=" + poudfDate01 + ", poudfDate02=" + poudfDate02 + ", poudfDate03=" + poudfDate03
				+ ", poudfDate04=" + poudfDate04 + ", poudfDate05=" + poudfDate05 + ", smUDFStrin01=" + smUDFStrin01
				+ ", smUDFStrin02=" + smUDFStrin02 + ", smUDFStrin03=" + smUDFStrin03 + ", smUDFStrin04=" + smUDFStrin04
				+ ", smUDFStrin05=" + smUDFStrin05 + ", smUDFStrin06=" + smUDFStrin06 + ", smUDFStrin07=" + smUDFStrin07
				+ ", smUDFStrin08=" + smUDFStrin08 + ", smUDFStrin09=" + smUDFStrin09 + ", smUDFStrin010="
				+ smUDFStrin010 + ", smUDFNum01=" + smUDFNum01 + ", smUDFNum02=" + smUDFNum02 + ", smUDFNum03="
				+ smUDFNum03 + ", smUDFNum04=" + smUDFNum04 + ", smUDFNum05=" + smUDFNum05 + ", smUDFDate01="
				+ smUDFDate01 + ", smUDFDate02=" + smUDFDate02 + ", smUDFDate03=" + smUDFDate03 + ", smUDFDate04="
				+ smUDFDate04 + ", smUDFDate05=" + smUDFDate05 + ", imUDFString01=" + imUDFString01 + ", imUDFString02="
				+ imUDFString02 + ", imUDFString03=" + imUDFString03 + ", imUDFString04=" + imUDFString04
				+ ", imUDFString05=" + imUDFString05 + ", imUDFString06=" + imUDFString06 + ", imUDFString07="
				+ imUDFString07 + ", imUDFString08=" + imUDFString08 + ", imUDFString09=" + imUDFString09
				+ ", imUDFString010=" + imUDFString010 + ", imUDFNum01=" + imUDFNum01 + ", imUDFNum02=" + imUDFNum02
				+ ", imUDFNum03=" + imUDFNum03 + ", imUDFNum04=" + imUDFNum04 + ", imUDFNum05=" + imUDFNum05
				+ ", imUDFDate01=" + imUDFDate01 + ", imUDFDate02=" + imUDFDate02 + ", imUDFDate03=" + imUDFDate03
				+ ", imUDFDate04=" + imUDFDate04 + ", imUDFDate05=" + imUDFDate05 + ", poReferenceId=" + poReferenceId
				+ ", orgId=" + orgId + "]";
	}
	

}
