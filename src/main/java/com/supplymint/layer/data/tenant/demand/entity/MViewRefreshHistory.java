package com.supplymint.layer.data.tenant.demand.entity;

import java.util.Date;

public class MViewRefreshHistory {

	private int id;
	private String module;
	private String subModule;
	private String refreshMView;
	private String status;
	private char active;
	private String ipAddress;
	private String userName;
	private String createdBy;
	private Date createdOn;
	private String updatedBy;
	private Date updatedOn;
	private String additional;
	private String forecastReadStatus;

	public MViewRefreshHistory() {

	}

	public MViewRefreshHistory(int id, String module, String subModule, String refreshMView, String status, char active,
			String ipAddress, String userName, String createdBy, Date createdOn, String updatedBy, Date updatedOn,
			String additional, String forecastReadStatus) {
		super();
		this.id = id;
		this.module = module;
		this.subModule = subModule;
		this.refreshMView = refreshMView;
		this.status = status;
		this.active = active;
		this.ipAddress = ipAddress;
		this.userName = userName;
		this.createdBy = createdBy;
		this.createdOn = createdOn;
		this.updatedBy = updatedBy;
		this.updatedOn = updatedOn;
		this.additional = additional;
		this.forecastReadStatus = forecastReadStatus;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public String getSubModule() {
		return subModule;
	}

	public void setSubModule(String subModule) {
		this.subModule = subModule;
	}

	public String getRefreshMView() {
		return refreshMView;
	}

	public void setRefreshMView(String refreshMView) {
		this.refreshMView = refreshMView;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public char getActive() {
		return active;
	}

	public void setActive(char active) {
		this.active = active;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getAdditional() {
		return additional;
	}

	public void setAdditional(String additional) {
		this.additional = additional;
	}

	public String getForecastReadStatus() {
		return forecastReadStatus;
	}

	public void setForecastReadStatus(String forecastReadStatus) {
		this.forecastReadStatus = forecastReadStatus;
	}

	@Override
	public String toString() {
		return "MViewRefreshHistory [id=" + id + ", module=" + module + ", subModule=" + subModule + ", refreshMView="
				+ refreshMView + ", status=" + status + ", active=" + active + ", ipAddress=" + ipAddress
				+ ", userName=" + userName + ", createdBy=" + createdBy + ", createdOn=" + createdOn + ", updatedBy="
				+ updatedBy + ", updatedOn=" + updatedOn + ", additional=" + additional + ", forecastReadStatus="
				+ forecastReadStatus + "]";
	}

}
