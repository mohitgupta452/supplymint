package com.supplymint.layer.data.tenant.inventory.entity;

public class Temp_Monthly {

	private String actual;
	private String predicted;
	private String budgeted;
	private String billDate;

	public Temp_Monthly() {

	}

	public Temp_Monthly(String actual, String predicted, String budgeted, String billDate) {
		super();
		this.actual = actual;
		this.predicted = predicted;
		this.budgeted = budgeted;
		this.billDate = billDate;
	}

	public String getActual() {
		return actual;
	}

	public void setActual(String actual) {
		this.actual = actual;
	}

	public String getPredicted() {
		return predicted;
	}

	public void setPredicted(String predicted) {
		this.predicted = predicted;
	}

	public String getBudgeted() {
		return budgeted;
	}

	public void setBudgeted(String budgeted) {
		this.budgeted = budgeted;
	}

	public String getBillDate() {
		return billDate;
	}

	public void setBillDate(String billDate) {
		this.billDate = billDate;
	}

	@Override
	public String toString() {
		return "Temp_Monthly [actual=" + actual + ", predicted=" + predicted + ", budgeted=" + budgeted + ", billDate="
				+ billDate + "]";
	}

}
