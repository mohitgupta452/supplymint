package com.supplymint.layer.data.tenant.administration.entity;

import java.time.OffsetDateTime;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.supplymint.util.LocalDateTimeSerializer;

import io.swagger.annotations.ApiModelProperty;

public class ADMOrganisation {
	@ApiModelProperty(notes = "organisation ID",name="orgID",required=true,value="test id")
	private int orgID;
	private String orgName;
	private String displayName;
	private String description;
	private String billToAddress;
	private String contPerson;
	private String contNumber;
	private String contEmail;
	private char active;
	private String status;
	private String ipAddress;
	private String createdBy;
	private String enterpriseGSTN;

	@JsonSerialize(using = LocalDateTimeSerializer.class)
	private OffsetDateTime createdTime;

	private String updatedBy;

	@JsonSerialize(using = LocalDateTimeSerializer.class)
	private OffsetDateTime updationTime;
	private String additional;
	private String file;
	private String fileName;

	// this data member only use for pagination
	private int offset;
	private int pagesize;
	private String imagePath;

	private String isDefault;

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public void setPagesize(int pagesize) {
		this.pagesize = pagesize;
	}

	public ADMOrganisation() {
		// TODO Auto-generated constructor stub
	}

	public ADMOrganisation(int orgID, String orgName, String displayName, String description, String billToAddress,
			String contPerson, String contNumber, String contEmail, char active, String status, String ipAddress,
			String createdBy, String enterpriseGSTN, OffsetDateTime createdTime, String updatedBy,
			OffsetDateTime updationTime, String additional, String file, String fileName, int offset, int pagesize,
			String imagePath, String isDefault) {
		super();
		this.orgID = orgID;
		this.orgName = orgName;
		this.displayName = displayName;
		this.description = description;
		this.billToAddress = billToAddress;
		this.contPerson = contPerson;
		this.contNumber = contNumber;
		this.contEmail = contEmail;
		this.active = active;
		this.status = status;
		this.ipAddress = ipAddress;
		this.createdBy = createdBy;
		this.enterpriseGSTN = enterpriseGSTN;
		this.createdTime = createdTime;
		this.updatedBy = updatedBy;
		this.updationTime = updationTime;
		this.additional = additional;
		this.file = file;
		this.fileName = fileName;
		this.offset = offset;
		this.pagesize = pagesize;
		this.imagePath = imagePath;
		this.isDefault = isDefault;
	}

	public int getOrgID() {
		return orgID;
	}

	public void setOrgID(int orgID) {
		this.orgID = orgID;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getBillToAddress() {
		return billToAddress;
	}

	public void setBillToAddress(String billToAddress) {
		this.billToAddress = billToAddress;
	}

	public String getContPerson() {
		return contPerson;
	}

	public void setContPerson(String contPerson) {
		this.contPerson = contPerson;
	}

	public String getContNumber() {
		return contNumber;
	}

	public void setContNumber(String contNumber) {
		this.contNumber = contNumber;
	}

	public String getContEmail() {
		return contEmail;
	}

	public void setContEmail(String contEmail) {
		this.contEmail = contEmail;
	}

	public char getActive() {
		return active;
	}

	public void setActive(char active) {
		this.active = active;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getEnterpriseGSTN() {
		return enterpriseGSTN;
	}

	public void setEnterpriseGSTN(String enterpriseGSTN) {
		this.enterpriseGSTN = enterpriseGSTN;
	}

	public OffsetDateTime getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(OffsetDateTime createdTime) {
		this.createdTime = createdTime;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public OffsetDateTime getUpdationTime() {
		return updationTime;
	}

	public void setUpdationTime(OffsetDateTime updationTime) {
		this.updationTime = updationTime;
	}

	public String getAdditional() {
		return additional;
	}

	public void setAdditional(String additional) {
		this.additional = additional;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public String getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(String isDefault) {
		this.isDefault = isDefault;
	}

	public int getOffset() {
		return offset;
	}

	public int getPagesize() {
		return pagesize;
	}

	@Override
	public String toString() {
		return "ADMOrganisation [orgID=" + orgID + ", orgName=" + orgName + ", displayName=" + displayName
				+ ", description=" + description + ", billToAddress=" + billToAddress + ", contPerson=" + contPerson
				+ ", contNumber=" + contNumber + ", contEmail=" + contEmail + ", active=" + active + ", status="
				+ status + ", ipAddress=" + ipAddress + ", createdBy=" + createdBy + ", enterpriseGSTN="
				+ enterpriseGSTN + ", createdTime=" + createdTime + ", updatedBy=" + updatedBy + ", updationTime="
				+ updationTime + ", additional=" + additional + ", file=" + file + ", fileName=" + fileName
				+ ", offset=" + offset + ", pagesize=" + pagesize + ", imagePath=" + imagePath + ", isDefault="
				+ isDefault + "]";
	}

}
