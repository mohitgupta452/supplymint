package com.supplymint.layer.data.tenant.administration.entity;

import java.time.OffsetDateTime;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.supplymint.util.CurrencySerializer;
import com.supplymint.util.DateFormatSerializer;
import com.supplymint.util.LocalDateTimeSerializer;
import com.supplymint.util.LocalDatetimeDeserializer;

public class ADMSite {

	private int id;
	private String name;
	private String code;
	private String displayName;
	private String country;
	private String state;
	private String city;
	private String zipCode;
	private String timeZone;
	private String latitude;
	private String longitude;
	private String type;
	private String area;
	private String areaUnit;
	private String numberFloors;
	@JsonSerialize(using = DateFormatSerializer.class)
	private Date optStoreSDate;
	@JsonSerialize(using = DateFormatSerializer.class)
	private Date optStoreEDate;
	private String isBilling;
	private String isShipping;
	private String isOnlineStore;
	private String status;
	private char active;
	private String ipAddress;
	private String createdBy;
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	private OffsetDateTime createdTime;
	private String updatedBy;
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	private OffsetDateTime updationTime;
	private String additional;
	private List<ADMSiteDetail> siteDetail;
	private int offset;
	private int pagesize;
	private ADMSiteDetail contact;
	private ADMSiteDetail billing;
	private ADMSiteDetail shipping;
	private ADMSiteDetail receiver;

	public ADMSite() {
		// TODO Auto-generated constructor stub
	}

	public ADMSite(int id, String name, String code, String displayName, String country, String state, String city,
			String zipCode, String timeZone, String latitude, String longitude, String type, String area,
			String areaUnit, String numberFloors, Date optStoreSDate, Date optStoreEDate, String isBilling,
			String isShipping, String isOnlineStore, String status, char active, String ipAddress, String createdBy,
			OffsetDateTime createdTime, String updatedBy, OffsetDateTime updationTime, String additional,
			List<ADMSiteDetail> siteDetail, int offset, int pagesize, ADMSiteDetail contact, ADMSiteDetail billing,
			ADMSiteDetail shipping, ADMSiteDetail receiver) {
		super();
		this.id = id;
		this.name = name;
		this.code = code;
		this.displayName = displayName;
		this.country = country;
		this.state = state;
		this.city = city;
		this.zipCode = zipCode;
		this.timeZone = timeZone;
		this.latitude = latitude;
		this.longitude = longitude;
		this.type = type;
		this.area = area;
		this.areaUnit = areaUnit;
		this.numberFloors = numberFloors;
		this.optStoreSDate = optStoreSDate;
		this.optStoreEDate = optStoreEDate;
		this.isBilling = isBilling;
		this.isShipping = isShipping;
		this.isOnlineStore = isOnlineStore;
		this.status = status;
		this.active = active;
		this.ipAddress = ipAddress;
		this.createdBy = createdBy;
		this.createdTime = createdTime;
		this.updatedBy = updatedBy;
		this.updationTime = updationTime;
		this.additional = additional;
		this.siteDetail = siteDetail;
		this.offset = offset;
		this.pagesize = pagesize;
		this.contact = contact;
		this.billing = billing;
		this.shipping = shipping;
		this.receiver = receiver;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getAreaUnit() {
		return areaUnit;
	}

	public void setAreaUnit(String areaUnit) {
		this.areaUnit = areaUnit;
	}

	public String getNumberFloors() {
		return numberFloors;
	}

	public void setNumberFloors(String numberFloors) {
		this.numberFloors = numberFloors;
	}

	public Date getOptStoreSDate() {
		return optStoreSDate;
	}

	public void setOptStoreSDate(Date optStoreSDate) {
		this.optStoreSDate = optStoreSDate;
	}

	public Date getOptStoreEDate() {
		return optStoreEDate;
	}

	public void setOptStoreEDate(Date optStoreEDate) {
		this.optStoreEDate = optStoreEDate;
	}

	public String getIsBilling() {
		return isBilling;
	}

	public void setIsBilling(String isBilling) {
		this.isBilling = isBilling;
	}

	public String getIsShipping() {
		return isShipping;
	}

	public void setIsShipping(String isShipping) {
		this.isShipping = isShipping;
	}

	public String getIsOnlineStore() {
		return isOnlineStore;
	}

	public void setIsOnlineStore(String isOnlineStore) {
		this.isOnlineStore = isOnlineStore;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public char getActive() {
		return active;
	}

	public void setActive(char active) {
		this.active = active;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public OffsetDateTime getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(OffsetDateTime createdTime) {
		this.createdTime = createdTime;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public OffsetDateTime getUpdationTime() {
		return updationTime;
	}

	public void setUpdationTime(OffsetDateTime updationTime) {
		this.updationTime = updationTime;
	}

	public String getAdditional() {
		return additional;
	}

	public void setAdditional(String additional) {
		this.additional = additional;
	}

	public List<ADMSiteDetail> getSiteDetail() {
		return siteDetail;
	}

	public void setSiteDetail(List<ADMSiteDetail> siteDetail) {
		this.siteDetail = siteDetail;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public int getPagesize() {
		return pagesize;
	}

	public void setPagesize(int pagesize) {
		this.pagesize = pagesize;
	}

	public ADMSiteDetail getContact() {
		return contact;
	}

	public void setContact(ADMSiteDetail contact) {
		this.contact = contact;
	}

	public ADMSiteDetail getBilling() {
		return billing;
	}

	public void setBilling(ADMSiteDetail billing) {
		this.billing = billing;
	}

	public ADMSiteDetail getShipping() {
		return shipping;
	}

	public void setShipping(ADMSiteDetail shipping) {
		this.shipping = shipping;
	}

	public ADMSiteDetail getReceiver() {
		return receiver;
	}

	public void setReceiver(ADMSiteDetail receiver) {
		this.receiver = receiver;
	}

	@Override
	public String toString() {
		return "ADMSite [id=" + id + ", name=" + name + ", code=" + code + ", displayName=" + displayName + ", country="
				+ country + ", state=" + state + ", city=" + city + ", zipCode=" + zipCode + ", timeZone=" + timeZone
				+ ", latitude=" + latitude + ", longitude=" + longitude + ", type=" + type + ", area=" + area
				+ ", areaUnit=" + areaUnit + ", numberFloors=" + numberFloors + ", optStoreSDate=" + optStoreSDate
				+ ", optStoreEDate=" + optStoreEDate + ", isBilling=" + isBilling + ", isShipping=" + isShipping
				+ ", isOnlineStore=" + isOnlineStore + ", status=" + status + ", active=" + active + ", ipAddress="
				+ ipAddress + ", createdBy=" + createdBy + ", createdTime=" + createdTime + ", updatedBy=" + updatedBy
				+ ", updationTime=" + updationTime + ", additional=" + additional + ", siteDetail=" + siteDetail
				+ ", offset=" + offset + ", pagesize=" + pagesize + ", contact=" + contact + ", billing=" + billing
				+ ", shipping=" + shipping + ", receiver=" + receiver + "]";
	}

}
