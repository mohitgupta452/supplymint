package com.supplymint.layer.data.tenant.procurement.entity;

public class DeptSizeData {

	private int id;
	private String code;
	private String cname;
	private String hl3Name;
	private String hl3Code;
	private String orderBy;

	public DeptSizeData() {
		// TODO Auto-generated constructor stub
	}

	public DeptSizeData(int id, String code, String cname, String hl3Name, String orderBy) {
		super();
		this.id = id;
		this.code = code;
		this.cname = cname;
		this.hl3Name = hl3Name;
		this.orderBy = orderBy;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCname() {
		return cname;
	}

	public void setCname(String cname) {
		this.cname = cname;
	}

	public String getHl3Name() {
		return hl3Name;
	}

	public void setHl3Name(String hl3Name) {
		this.hl3Name = hl3Name;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public String getHl3Code() {
		return hl3Code;
	}

	public void setHl3Code(String hl3Code) {
		this.hl3Code = hl3Code;
	}

	@Override
	public String toString() {
		return "DeptSizeData [id=" + id + ", code=" + code + ", cname=" + cname + ", hl3Name=" + hl3Name + ", orderBy="
				+ orderBy + "]";
	}

}
