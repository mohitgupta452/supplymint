package com.supplymint.layer.data.tenant.analytic.entity;

public class StoreProfileActualSaleData {

	private int ranks;
	private String storeCode;
	private String billDate;
	private int sellPerSqFeet;
	private int qty;
	private String stockInHand;
	private int dayCoverage;
	private String lastMonthSV;
	private String articleCode;
	private String articleName;
	private String lastYearQty;
	private String lastThreeMonthQty;
	private String currentMonthQty;
	private String secondLastYearQty;
	private String secondLastThreeMonthQty;
	private String lastYearCurrentMonthQty;
	private String lastYearSellValue;
	private String lastThreeMonthSellValue;
	private String currentMonthSellValue;
	private String seconndLastYearSellValue;
	private String secondLastThreeMonthSellValue;
	private String lastYearCurrentMonthSellValue;
	private String lastMonthQTY;
	private String lastMonthCostValue;
	private double lastMonthProfitValue;

	public StoreProfileActualSaleData() {

	}

	public StoreProfileActualSaleData(int ranks, String storeCode, String billDate, int sellPerSqFeet, int qty,
			String stockInHand, int dayCoverage, String lastMonthSV, String articleCode, String articleName,
			String lastYearQty, String lastThreeMonthQty, String currentMonthQty, String secondLastYearQty,
			String secondLastThreeMonthQty, String lastYearCurrentMonthQty, String lastYearSellValue,
			String lastThreeMonthSellValue, String currentMonthSellValue, String seconndLastYearSellValue,
			String secondLastThreeMonthSellValue, String lastYearCurrentMonthSellValue, String lastMonthQTY,
			String lastMonthCostValue, double lastMonthProfitValue) {
		super();
		this.ranks = ranks;
		this.storeCode = storeCode;
		this.billDate = billDate;
		this.sellPerSqFeet = sellPerSqFeet;
		this.qty = qty;
		this.stockInHand = stockInHand;
		this.dayCoverage = dayCoverage;
		this.lastMonthSV = lastMonthSV;
		this.articleCode = articleCode;
		this.articleName = articleName;
		this.lastYearQty = lastYearQty;
		this.lastThreeMonthQty = lastThreeMonthQty;
		this.currentMonthQty = currentMonthQty;
		this.secondLastYearQty = secondLastYearQty;
		this.secondLastThreeMonthQty = secondLastThreeMonthQty;
		this.lastYearCurrentMonthQty = lastYearCurrentMonthQty;
		this.lastYearSellValue = lastYearSellValue;
		this.lastThreeMonthSellValue = lastThreeMonthSellValue;
		this.currentMonthSellValue = currentMonthSellValue;
		this.seconndLastYearSellValue = seconndLastYearSellValue;
		this.secondLastThreeMonthSellValue = secondLastThreeMonthSellValue;
		this.lastYearCurrentMonthSellValue = lastYearCurrentMonthSellValue;
		this.lastMonthQTY = lastMonthQTY;
		this.lastMonthCostValue = lastMonthCostValue;
		this.lastMonthProfitValue = lastMonthProfitValue;
	}

	public int getRanks() {
		return ranks;
	}

	public void setRanks(int ranks) {
		this.ranks = ranks;
	}

	public String getStoreCode() {
		return storeCode;
	}

	public void setStoreCode(String storeCode) {
		this.storeCode = storeCode;
	}

	public String getBillDate() {
		return billDate;
	}

	public void setBillDate(String billDate) {
		this.billDate = billDate;
	}

	public int getSellPerSqFeet() {
		return sellPerSqFeet;
	}

	public void setSellPerSqFeet(int sellPerSqFeet) {
		this.sellPerSqFeet = sellPerSqFeet;
	}

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

	public String getStockInHand() {
		return stockInHand;
	}

	public void setStockInHand(String stockInHand) {
		this.stockInHand = stockInHand;
	}

	public int getDayCoverage() {
		return dayCoverage;
	}

	public void setDayCoverage(int dayCoverage) {
		this.dayCoverage = dayCoverage;
	}

	public String getLastMonthSV() {
		return lastMonthSV;
	}

	public void setLastMonthSV(String lastMonthSV) {
		this.lastMonthSV = lastMonthSV;
	}

	public String getArticleCode() {
		return articleCode;
	}

	public void setArticleCode(String articleCode) {
		this.articleCode = articleCode;
	}

	public String getArticleName() {
		return articleName;
	}

	public void setArticleName(String articleName) {
		this.articleName = articleName;
	}

	public String getLastYearQty() {
		return lastYearQty;
	}

	public void setLastYearQty(String lastYearQty) {
		this.lastYearQty = lastYearQty;
	}

	public String getLastThreeMonthQty() {
		return lastThreeMonthQty;
	}

	public void setLastThreeMonthQty(String lastThreeMonthQty) {
		this.lastThreeMonthQty = lastThreeMonthQty;
	}

	public String getCurrentMonthQty() {
		return currentMonthQty;
	}

	public void setCurrentMonthQty(String currentMonthQty) {
		this.currentMonthQty = currentMonthQty;
	}

	public String getSecondLastYearQty() {
		return secondLastYearQty;
	}

	public void setSecondLastYearQty(String secondLastYearQty) {
		this.secondLastYearQty = secondLastYearQty;
	}

	public String getSecondLastThreeMonthQty() {
		return secondLastThreeMonthQty;
	}

	public void setSecondLastThreeMonthQty(String secondLastThreeMonthQty) {
		this.secondLastThreeMonthQty = secondLastThreeMonthQty;
	}

	public String getLastYearCurrentMonthQty() {
		return lastYearCurrentMonthQty;
	}

	public void setLastYearCurrentMonthQty(String lastYearCurrentMonthQty) {
		this.lastYearCurrentMonthQty = lastYearCurrentMonthQty;
	}

	public String getLastYearSellValue() {
		return lastYearSellValue;
	}

	public void setLastYearSellValue(String lastYearSellValue) {
		this.lastYearSellValue = lastYearSellValue;
	}

	public String getLastThreeMonthSellValue() {
		return lastThreeMonthSellValue;
	}

	public void setLastThreeMonthSellValue(String lastThreeMonthSellValue) {
		this.lastThreeMonthSellValue = lastThreeMonthSellValue;
	}

	public String getCurrentMonthSellValue() {
		return currentMonthSellValue;
	}

	public void setCurrentMonthSellValue(String currentMonthSellValue) {
		this.currentMonthSellValue = currentMonthSellValue;
	}

	public String getSeconndLastYearSellValue() {
		return seconndLastYearSellValue;
	}

	public void setSeconndLastYearSellValue(String seconndLastYearSellValue) {
		this.seconndLastYearSellValue = seconndLastYearSellValue;
	}

	public String getSecondLastThreeMonthSellValue() {
		return secondLastThreeMonthSellValue;
	}

	public void setSecondLastThreeMonthSellValue(String secondLastThreeMonthSellValue) {
		this.secondLastThreeMonthSellValue = secondLastThreeMonthSellValue;
	}

	public String getLastYearCurrentMonthSellValue() {
		return lastYearCurrentMonthSellValue;
	}

	public void setLastYearCurrentMonthSellValue(String lastYearCurrentMonthSellValue) {
		this.lastYearCurrentMonthSellValue = lastYearCurrentMonthSellValue;
	}

	public String getLastMonthQTY() {
		return lastMonthQTY;
	}

	public void setLastMonthQTY(String lastMonthQTY) {
		this.lastMonthQTY = lastMonthQTY;
	}

	public String getLastMonthCostValue() {
		return lastMonthCostValue;
	}

	public void setLastMonthCostValue(String lastMonthCostValue) {
		this.lastMonthCostValue = lastMonthCostValue;
	}

	public double getLastMonthProfitValue() {
		return lastMonthProfitValue;
	}

	public void setLastMonthProfitValue(double lastMonthProfitValue) {
		this.lastMonthProfitValue = lastMonthProfitValue;
	}

	@Override
	public String toString() {
		return "StoreProfileActualSaleData [ranks=" + ranks + ", storeCode=" + storeCode + ", billDate=" + billDate
				+ ", sellPerSqFeet=" + sellPerSqFeet + ", qty=" + qty + ", stockInHand=" + stockInHand
				+ ", dayCoverage=" + dayCoverage + ", lastMonthSV=" + lastMonthSV + ", articleCode=" + articleCode
				+ ", articleName=" + articleName + ", lastYearQty=" + lastYearQty + ", lastThreeMonthQty="
				+ lastThreeMonthQty + ", currentMonthQty=" + currentMonthQty + ", secondLastYearQty="
				+ secondLastYearQty + ", secondLastThreeMonthQty=" + secondLastThreeMonthQty
				+ ", lastYearCurrentMonthQty=" + lastYearCurrentMonthQty + ", lastYearSellValue=" + lastYearSellValue
				+ ", lastThreeMonthSellValue=" + lastThreeMonthSellValue + ", currentMonthSellValue="
				+ currentMonthSellValue + ", seconndLastYearSellValue=" + seconndLastYearSellValue
				+ ", secondLastThreeMonthSellValue=" + secondLastThreeMonthSellValue
				+ ", lastYearCurrentMonthSellValue=" + lastYearCurrentMonthSellValue + ", lastMonthQTY=" + lastMonthQTY
				+ ", lastMonthCostValue=" + lastMonthCostValue + ", lastMonthProfitValue=" + lastMonthProfitValue + "]";
	}

}
