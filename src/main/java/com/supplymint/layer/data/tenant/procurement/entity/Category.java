package com.supplymint.layer.data.tenant.procurement.entity;

public class Category {
	private int id;
	private String code;
	private String cname;

	public Category() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Category(int id, String code, String cname) {
		super();
		this.id = id;
		this.code = code;
		this.cname = cname;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCname() {
		return cname;
	}

	public void setCname(String cname) {
		this.cname = cname;
	}

	@Override
	public String toString() {
		return "Category [id=" + id + ", code=" + code + ", cname=" + cname + "]";
	}

}
