package com.supplymint.layer.data.tenant.procurement.entity;

import java.math.BigDecimal;

public class PurtermDet {

	private BigDecimal purTermMainCode;
	private BigDecimal seq;
	private BigDecimal chgCode;
	private BigDecimal rate;
	private String formulae;
	private String sign;
	private String operationLevel;
	private BigDecimal code;
	private String includeInTDS;
	
	public PurtermDet(BigDecimal purTermMainCode, BigDecimal seq, BigDecimal chgCode, BigDecimal rate, String formulae,
			String sign, String operationLevel, BigDecimal code, String includeInTDS) {
		super();
		this.purTermMainCode = purTermMainCode;
		this.seq = seq;
		this.chgCode = chgCode;
		this.rate = rate;
		this.formulae = formulae;
		this.sign = sign;
		this.operationLevel = operationLevel;
		this.code = code;
		this.includeInTDS = includeInTDS;
	}
	
	public BigDecimal getPurTermMainCode() {
		return purTermMainCode;
	}
	public void setPurTermMainCode(BigDecimal purTermMainCode) {
		this.purTermMainCode = purTermMainCode;
	}
	public BigDecimal getSeq() {
		return seq;
	}
	public void setSeq(BigDecimal seq) {
		this.seq = seq;
	}
	public BigDecimal getChgCode() {
		return chgCode;
	}
	public void setChgCode(BigDecimal chgCode) {
		this.chgCode = chgCode;
	}
	public BigDecimal getRate() {
		return rate;
	}
	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}
	public String getFormulae() {
		return formulae;
	}
	public void setFormulae(String formulae) {
		this.formulae = formulae;
	}
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	public String getOperationLevel() {
		return operationLevel;
	}
	public void setOperationLevel(String operationLevel) {
		this.operationLevel = operationLevel;
	}
	public BigDecimal getCode() {
		return code;
	}
	public void setCode(BigDecimal code) {
		this.code = code;
	}
	public String getIncludeInTDS() {
		return includeInTDS;
	}
	public void setIncludeInTDS(String includeInTDS) {
		this.includeInTDS = includeInTDS;
	}

	@Override
	public String toString() {
		return "PurtermDet [purTermMainCode=" + purTermMainCode + ", seq=" + seq + ", chgCode=" + chgCode + ", rate="
				+ rate + ", formulae=" + formulae + ", sign=" + sign + ", operationLevel=" + operationLevel + ", code="
				+ code + ", includeInTDS=" + includeInTDS + "]";
	}
	
	
}
