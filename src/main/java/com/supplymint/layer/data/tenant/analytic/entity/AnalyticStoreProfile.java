package com.supplymint.layer.data.tenant.analytic.entity;

import java.time.OffsetDateTime;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.supplymint.util.StringDateSerializer;

public class AnalyticStoreProfile {
	private String siteCode;
	private String siteName;
	private String totalSalesLY;
	private String totalQTYSoldLY;
	private String avgSalesLY;
	private String totalSalesCM;
	private String storeRank;
	private String bestPermofmingMonth;
	private String worstPerformingMonth;
	private String state;
	private String city;
	private String totalArea;
	private String numberOfSection;
	private String numberOfArticles;
	private String numberOfItems;
	private String lyStartMonth;
	private String status;
	private char active;
	private String ipAddress;
	private String createdBy;
	private OffsetDateTime createdOn;
	private String updatedBy;
	private OffsetDateTime updatedOn;
	private String additional;

	public AnalyticStoreProfile() {
		// TODO Auto-generated constructor stub
	}

	public AnalyticStoreProfile(String siteCode, String siteName, String totalSalesLY, String totalQTYSoldLY,
			String avgSalesLY, String totalSalesCM, String storeRank, String bestPermofmingMonth,
			String worstPerformingMonth, String state, String city, String totalArea, String numberOfSection,
			String numberOfArticles, String numberOfItems, String lyStartMonth, String status, char active,
			String ipAddress, String createdBy, OffsetDateTime createdOn, String updatedBy, OffsetDateTime updatedOn,
			String additional) {
		super();
		this.siteCode = siteCode;
		this.siteName = siteName;
		this.totalSalesLY = totalSalesLY;
		this.totalQTYSoldLY = totalQTYSoldLY;
		this.avgSalesLY = avgSalesLY;
		this.totalSalesCM = totalSalesCM;
		this.storeRank = storeRank;
		this.bestPermofmingMonth = bestPermofmingMonth;
		this.worstPerformingMonth = worstPerformingMonth;
		this.state = state;
		this.city = city;
		this.totalArea = totalArea;
		this.numberOfSection = numberOfSection;
		this.numberOfArticles = numberOfArticles;
		this.numberOfItems = numberOfItems;
		this.lyStartMonth = lyStartMonth;
		this.status = status;
		this.active = active;
		this.ipAddress = ipAddress;
		this.createdBy = createdBy;
		this.createdOn = createdOn;
		this.updatedBy = updatedBy;
		this.updatedOn = updatedOn;
		this.additional = additional;
	}

	public String getSiteCode() {
		return siteCode;
	}

	public void setSiteCode(String siteCode) {
		this.siteCode = siteCode;
	}

	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	public String getTotalSalesLY() {
		return totalSalesLY;
	}

	public void setTotalSalesLY(String totalSalesLY) {
		this.totalSalesLY = totalSalesLY;
	}

	public String getTotalQTYSoldLY() {
		return totalQTYSoldLY;
	}

	public void setTotalQTYSoldLY(String totalQTYSoldLY) {
		this.totalQTYSoldLY = totalQTYSoldLY;
	}

	public String getAvgSalesLY() {
		return avgSalesLY;
	}

	public void setAvgSalesLY(String avgSalesLY) {
		this.avgSalesLY = avgSalesLY;
	}

	public String getTotalSalesCM() {
		return totalSalesCM;
	}

	public void setTotalSalesCM(String totalSalesCM) {
		this.totalSalesCM = totalSalesCM;
	}

	public String getStoreRank() {
		return storeRank;
	}

	public void setStoreRank(String storeRank) {
		this.storeRank = storeRank;
	}

	public String getBestPermofmingMonth() {
		return bestPermofmingMonth;
	}

	public void setBestPermofmingMonth(String bestPermofmingMonth) {
		this.bestPermofmingMonth = bestPermofmingMonth;
	}

	public String getWorstPerformingMonth() {
		return worstPerformingMonth;
	}

	public void setWorstPerformingMonth(String worstPerformingMonth) {
		this.worstPerformingMonth = worstPerformingMonth;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getTotalArea() {
		return totalArea;
	}

	public void setTotalArea(String totalArea) {
		this.totalArea = totalArea;
	}

	public String getNumberOfSection() {
		return numberOfSection;
	}

	public void setNumberOfSection(String numberOfSection) {
		this.numberOfSection = numberOfSection;
	}

	public String getNumberOfArticles() {
		return numberOfArticles;
	}

	public void setNumberOfArticles(String numberOfArticles) {
		this.numberOfArticles = numberOfArticles;
	}

	public String getNumberOfItems() {
		return numberOfItems;
	}

	public void setNumberOfItems(String numberOfItems) {
		this.numberOfItems = numberOfItems;
	}

	public String getLyStartMonth() {
		return lyStartMonth;
	}

	public void setLyStartMonth(String lyStartMonth) {
		this.lyStartMonth = lyStartMonth;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public char getActive() {
		return active;
	}

	public void setActive(char active) {
		this.active = active;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public OffsetDateTime getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(OffsetDateTime createdOn) {
		this.createdOn = createdOn;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public OffsetDateTime getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(OffsetDateTime updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getAdditional() {
		return additional;
	}

	public void setAdditional(String additional) {
		this.additional = additional;
	}

	@Override
	public String toString() {
		return "AnalyticStoreProfile [siteCode=" + siteCode + ", siteName=" + siteName + ", totalSalesLY="
				+ totalSalesLY + ", totalQTYSoldLY=" + totalQTYSoldLY + ", avgSalesLY=" + avgSalesLY + ", totalSalesCM="
				+ totalSalesCM + ", storeRank=" + storeRank + ", bestPermofmingMonth=" + bestPermofmingMonth
				+ ", worstPerformingMonth=" + worstPerformingMonth + ", state=" + state + ", city=" + city
				+ ", totalArea=" + totalArea + ", numberOfSection=" + numberOfSection + ", numberOfArticles="
				+ numberOfArticles + ", numberOfItems=" + numberOfItems + ", lyStartMonth=" + lyStartMonth + ", status="
				+ status + ", active=" + active + ", ipAddress=" + ipAddress + ", createdBy=" + createdBy
				+ ", createdOn=" + createdOn + ", updatedBy=" + updatedBy + ", updatedOn=" + updatedOn + ", additional="
				+ additional + "]";
	}

}
