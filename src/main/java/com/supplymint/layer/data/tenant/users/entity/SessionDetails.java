package com.supplymint.layer.data.tenant.users.entity;

import java.time.OffsetDateTime;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.supplymint.util.LocalDateTimeSerializer;
import com.supplymint.util.StringDateSerializer;

public class SessionDetails {

	private int id;
	private String userName;
	private String emailId;
	private String token;
	private String isLoggedIn;
	// private String duration;
	private String autoSignIn;
	private String mode;
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	private OffsetDateTime loggedIn;
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	private OffsetDateTime loggedOut;
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	private OffsetDateTime lastActiveTime;
	private String keepMeSignIn;
	private String active;
	private String status;
	private String ipAddress;
	private String createdBy;
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	private OffsetDateTime createdTime;
	private String updatedBy;
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	private OffsetDateTime updationTime;
	private String additional;
	private String type;
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	private OffsetDateTime lastTimeOut;

	public SessionDetails() {
		// TODO Auto-generated constructor stub
	}

	public SessionDetails(int id, String userName, String emailId, String token, String isLoggedIn, String autoSignIn,
			String mode, OffsetDateTime loggedIn, OffsetDateTime loggedOut, OffsetDateTime lastActiveTime,
			String keepMeSignIn, String active, String status, String ipAddress, String createdBy,
			OffsetDateTime createdTime, String updatedBy, OffsetDateTime updationTime, String additional, String type,
			OffsetDateTime lastTimeOut) {
		super();
		this.id = id;
		this.userName = userName;
		this.emailId = emailId;
		this.token = token;
		this.isLoggedIn = isLoggedIn;
		this.autoSignIn = autoSignIn;
		this.mode = mode;
		this.loggedIn = loggedIn;
		this.loggedOut = loggedOut;
		this.lastActiveTime = lastActiveTime;
		this.keepMeSignIn = keepMeSignIn;
		this.active = active;
		this.status = status;
		this.ipAddress = ipAddress;
		this.createdBy = createdBy;
		this.createdTime = createdTime;
		this.updatedBy = updatedBy;
		this.updationTime = updationTime;
		this.additional = additional;
		this.type = type;
		this.lastTimeOut = lastTimeOut;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getIsLoggedIn() {
		return isLoggedIn;
	}

	public void setIsLoggedIn(String isLoggedIn) {
		this.isLoggedIn = isLoggedIn;
	}

	public String getAutoSignIn() {
		return autoSignIn;
	}

	public void setAutoSignIn(String autoSignIn) {
		this.autoSignIn = autoSignIn;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public OffsetDateTime getLoggedIn() {
		return loggedIn;
	}

	public void setLoggedIn(OffsetDateTime loggedIn) {
		this.loggedIn = loggedIn;
	}

	public OffsetDateTime getLoggedOut() {
		return loggedOut;
	}

	public void setLoggedOut(OffsetDateTime loggedOut) {
		this.loggedOut = loggedOut;
	}

	public OffsetDateTime getLastActiveTime() {
		return lastActiveTime;
	}

	public void setLastActiveTime(OffsetDateTime lastActiveTime) {
		this.lastActiveTime = lastActiveTime;
	}

	public String getKeepMeSignIn() {
		return keepMeSignIn;
	}

	public void setKeepMeSignIn(String keepMeSignIn) {
		this.keepMeSignIn = keepMeSignIn;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public OffsetDateTime getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(OffsetDateTime createdTime) {
		this.createdTime = createdTime;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public OffsetDateTime getUpdationTime() {
		return updationTime;
	}

	public void setUpdationTime(OffsetDateTime updationTime) {
		this.updationTime = updationTime;
	}

	public String getAdditional() {
		return additional;
	}

	public void setAdditional(String additional) {
		this.additional = additional;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public OffsetDateTime getLastTimeOut() {
		return lastTimeOut;
	}

	public void setLastTimeOut(OffsetDateTime lastTimeOut) {
		this.lastTimeOut = lastTimeOut;
	}

	@Override
	public String toString() {
		return "SessionDetails [id=" + id + ", userName=" + userName + ", emailId=" + emailId + ", token=" + token
				+ ", isLoggedIn=" + isLoggedIn + ", autoSignIn=" + autoSignIn + ", mode=" + mode + ", loggedIn="
				+ loggedIn + ", loggedOut=" + loggedOut + ", lastActiveTime=" + lastActiveTime + ", keepMeSignIn="
				+ keepMeSignIn + ", active=" + active + ", status=" + status + ", ipAddress=" + ipAddress
				+ ", createdBy=" + createdBy + ", createdTime=" + createdTime + ", updatedBy=" + updatedBy
				+ ", updationTime=" + updationTime + ", additional=" + additional + ", type=" + type + ", lastTimeOut="
				+ lastTimeOut + "]";
	}

	
}
