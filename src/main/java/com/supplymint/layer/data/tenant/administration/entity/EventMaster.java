package com.supplymint.layer.data.tenant.administration.entity;

import java.util.Date;
import java.lang.String;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.supplymint.util.DateFormatSerializer;

public class EventMaster {

	private int id;
	private int enterpriseId;
	private String orgId;
	private String eventName;
	private int stockDays;
	private int multiplierROS;
	private String channel;
	private String partner;
	private String zone;
	private String grade;
	private String storeCode;
	private String status;
	private String active;
	private String ipAddress;
	@JsonFormat(pattern = "dd MMM yyyy")
	private Date startDate;
	@JsonFormat(pattern = "dd MMM yyyy")
	private Date endDate;
	@JsonSerialize(using = DateFormatSerializer.class)
	private Date createdOn;
	@JsonSerialize(using = DateFormatSerializer.class)
	private Date updatedOn;
	private String additional;

	public EventMaster() {
		super();
	}

	public EventMaster(int id, int enterpriseId, String orgId, String eventName, int stockDays, int multiplierROS,
			String channel, String partner, String zone, String grade, String storeCode, String status, String active,
			String ipAddress, Date startDate, Date endDate, Date createdOn, Date updatedOn, String additional) {
		super();
		this.id = id;
		this.enterpriseId = enterpriseId;
		this.orgId = orgId;
		this.eventName = eventName;
		this.stockDays = stockDays;
		this.multiplierROS = multiplierROS;
		this.channel = channel;
		this.partner = partner;
		this.zone = zone;
		this.grade = grade;
		this.storeCode = storeCode;
		this.status = status;
		this.active = active;
		this.ipAddress = ipAddress;
		this.startDate = startDate;
		this.endDate = endDate;
		this.createdOn = createdOn;
		this.updatedOn = updatedOn;
		this.additional = additional;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getEnterpriseId() {
		return enterpriseId;
	}

	public void setEnterpriseId(int enterpriseId) {
		this.enterpriseId = enterpriseId;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public int getStockDays() {
		return stockDays;
	}

	public void setStockDays(int stockDays) {
		this.stockDays = stockDays;
	}

	public int getMultiplierROS() {
		return multiplierROS;
	}

	public void setMultiplierROS(int multiplierROS) {
		this.multiplierROS = multiplierROS;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getPartner() {
		return partner;
	}

	public void setPartner(String partner) {
		this.partner = partner;
	}

	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public String getStoreCode() {
		return storeCode;
	}

	public void setStoreCode(String storeCode) {
		this.storeCode = storeCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getAdditional() {
		return additional;
	}

	public void setAdditional(String additional) {
		this.additional = additional;
	}

	@Override
	public String toString() {
		return "EventMaster [id=" + id + ", enterpriseId=" + enterpriseId + ", orgId=" + orgId + ", eventName="
				+ eventName + ", stockDays=" + stockDays + ", multiplierROS=" + multiplierROS + ", channel=" + channel
				+ ", partner=" + partner + ", zone=" + zone + ", grade=" + grade + ", storeCode=" + storeCode
				+ ", status=" + status + ", active=" + active + ", ipAddress=" + ipAddress + ", startDate=" + startDate
				+ ", endDate=" + endDate + ", createdOn=" + createdOn + ", updatedOn=" + updatedOn + ", additional="
				+ additional + "]";
	}

}
