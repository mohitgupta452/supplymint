package com.supplymint.layer.data.tenant.demand.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.mapping.StatementType;

import com.supplymint.layer.data.downloads.entity.DownloadOTBPlanASD;
import com.supplymint.layer.data.downloads.entity.DownloadOTBPlanDFD;
import com.supplymint.layer.data.tenant.demand.entity.DemandForecast;
import com.supplymint.layer.data.tenant.demand.entity.MViewRefreshHistory;
import com.supplymint.layer.data.tenant.demand.entity.OTBPlanASD;
import com.supplymint.layer.data.tenant.demand.entity.OTBPlanDFD;
import com.supplymint.layer.data.tenant.demand.entity.OpenToBuy;

/**
 * This interface will act as Mapper for SQL queries
 * @author Prabhakar Srivastava
 * @Date 20 Mar 2019
 * @version 1.0
 */
@Mapper
public interface OTBMapper {

	/**
	 * This method describes getting OTB plan Info
	 * @param Plan Name
	 * @Table OTB_CREATE_PLAN
	 */
	OpenToBuy getByPlanName(@Param("planName") String planName);

	/**
	 * This method describes create OTB plan
	 * @param OpenToBuy
	 * @Table OTB_CREATE_PLAN, OTB_USER_ASD, OTB_USER_DFD
	 */
	int create(OpenToBuy openToBuy);

	/**
	 * This method describes delete OTB plan
	 * @param ID
	 * @Table OTB_CREATE_PLAN
	 */
	@Delete("DELETE FROM OTB_CREATE_PLAN WHERE ID=#{id}")
	int delete(@Param("id") int id);

	/**
	 * This method describes get last pattern ID
	 * @Table OTB_CREATE_PLAN
	 */
	@Select("SELECT PLANID FROM OTB_CREATE_PLAN ORDER BY ID DESC FETCH FIRST 1 ROWS ONLY")
	String getPlanIdPattern();

	/**
	 * This method describes get all Actual Data for one year
	 * @Table DEMAND_ASSORTMENT_MONTHLY
	 * @deprecated This method is not used forever
	 */
	List<DemandForecast> getAllASD(@Param("startDate") String startDate, @Param("endDate") String endDate);

	/**
	 * This method describes batch insert for Actual data
	 * @Table OTB_PLAN_ASD
	 * @deprecated This method is not used forever
	 */
	int insertASD(@Param("data") List<OTBPlanASD> data);

	/**
	 * This method describes get all Forecast Data for one year
	 * @Table DEMAND_FORECAST_MONTHLY
	 * @deprecated This method is not used forever
	 */
	List<OTBPlanDFD> getAllDFD(@Param("startDate") String startDate, @Param("endDate") String endDate);

	/**
	 * This method describes batch insert for Forecast data
	 * @Table OTB_PLAN_DFD
	 * @deprecated This method is not used forever
	 */
	int insertDFDData(@Param("list") List<OTBPlanDFD> dfdData);
	
	/**
	 * This method describes get all Actual Data for one year
	 * @Table DEMAND_FORECAST_MONTHLY
	 */
	List<DemandForecast> getAllUserASDPlan(@Param("startDate") String startDate,@Param("endDate") String endDate);
	
	/**
	 * This method describes batch insert for Actual data
	 * @Table OTB_USER_ASD
	 */
	int insertUserASD(@Param("data") List<OTBPlanASD> data);
	
	/**
	 * This method describes get all Forecast Data for one year
	 * @Table DEMAND_FORECAST_MONTHLY
	 */
	List<OTBPlanDFD> getAllUserDFD(@Param("startDate") String startDate, @Param("endDate") String endDate);

	/**
	 * This method describes batch insert for Forecast data
	 * @Table OTB_USER_DFD
	 */
	int insertUserDFDData(@Param("list") List<OTBPlanDFD> dfdData);

	/**
	 * This method describes Existence of OTB Plan
	 * @param Plan Name
	 * @Table OTB_CREATE_PLAN
	 */
	@Select("SELECT COUNT(*) FROM OTB_CREATE_PLAN WHERE PLANNAME=#{planName} AND ACTIVE='1'")
	int isExistencePlanName(@Param("planName") String planName);

	/**
	 * This method describes get all available OTB plan
	 * @Table OTB_CREATE_PLAN
	 */
	List<OpenToBuy> getActiveOTBPlan();

	/**
	 * This method describes update active OTB plan status
	 * @param OpenToBuy
	 * @Table OTB_CREATE_PLAN
	 */
	@Update("UPDATE OTB_CREATE_PLAN SET STATUS='TRUE' WHERE PLANID=#{planId} AND PLANNAME=#{planName}")
	int updateActivePlan(OpenToBuy openToBuy);

	/**
	 * This method describes update previous OTB plan status
	 * @param OpenToBuy
	 * @Table OTB_CREATE_PLAN
	 */
	@Update("UPDATE OTB_CREATE_PLAN SET STATUS='FALSE' WHERE PLANID!=#{planId} AND PLANNAME!=#{planName}")
	int updatePreviousActivePlan(OpenToBuy openToBuy);

	/**
	 * This method describes delete OTB plan while update
	 * @param Plan ID
	 * @Table OTB_PLAN_ASD
	 * @deprecated This method is not used forever
	 */
	@Delete("DELETE FROM OTB_PLAN_ASD WHERE PLANID = #{planId}")
	int deleteAllAcivePlaneASD(@Param("planId") String palnId);
	
	/**
	 * This method describes delete OTB plan while update
	 * @param Plan ID
	 * @Table OTB_PLAN_DFD
	 * @deprecated This method is not used forever
	 */
	@Delete("DELETE FROM OTB_PLAN_DFD WHERE PLANID = #{planId}")
	int deleteAllAcivePlaneDFD(@Param("planId") String palnId);

	/**
	 * This method describes delete OTB plan while update
	 * @param Plan ID
	 * @Table OTB_USER_ASD
	 */
	@Delete("DELETE FROM OTB_USER_ASD WHERE PLANID = #{planId}")
	int deleteAllAcivePlaneUserASD(@Param("planId") String palnId);

	/**
	 * This method describes delete OTB plan while update
	 * @param Plan ID
	 * @Table OTB_USER_DFD
	 */
	@Delete("DELETE FROM OTB_USER_DFD WHERE PLANID = #{planId}")
	int deleteAllAcivePlaneUserDFD(@Param("planId") String palnId);

	/**
	 * This method describes update all previous status
	 * @param Plan ID, Plan Name
	 * @Table OTB_CREATE_PLAN
	 */
	@Update("UPDATE OTB_CREATE_PLAN SET STATUS='FALSE' WHERE PLANID != #{planId}")
	int updateAllStatus(@Param("planId") String planId, @Param("planName") String planName);

	/**
	 * This method describes update OTB plan
	 * @param OpenToBuy
	 * @Table OTB_CREATE_PLAN
	 */
	int updatedOTBCreatePlan(OpenToBuy openToBuy);

	/**
	 * This method describes remove active OTB plan by status
	 * @param OpenToBuy
	 * @Table OTB_CREATE_PLAN
	 */
	@Update("UPDATE OTB_CREATE_PLAN  SET ACTIVE='0', STATUS='FALSE' WHERE PLANID=#{planId} AND PLANNAME=#{planName}")
	int removeActivePlan(OpenToBuy openToBuy);

	/**
	 * This method describes get OTB plan Info By ID
	 * @param Plan ID
	 * @Table OTB_CREATE_PLAN
	 */
	OpenToBuy getByPlanId(String planId);

	/**
	 * This method describes get default OTB Assortment
	 * @param Key
	 * @Table SYSTEM_DEFAULT_CONFIG
	 */
	@Select("SELECT VALUE FROM SYSTEM_DEFAULT_CONFIG WHERE KEY=#{key}")
	String getUserAssortment(@Param("key") String key);

	/**
	 * This method describes Drop OTB Actual Data due to query rewrite
	 * @MaterilizedView OTB_ACTUAL_MONTHLY
	 */
	@Delete("DROP MATERIALIZED VIEW OTB_ACTUAL_MONTHLY")
	void dropUserOTBMV();

	/**
	 * This method describes Create OTB Actual Data with new configuration
	 * @MaterilizedView OTB_ACTUAL_MONTHLY
	 */
	void createUserOTBMV(@Param("assortmentCode") String assortmentCode, @Param("billDate") String billDate);
	
	/**
	 * This method describes create OTB Forecast Monthly
	 * @param AssortmentCode
	 * @Table OTB_FORECAST_MONTHLY
	 */
	void createUserOTBMVForecast(@Param("assortmentCode") String assortmentCode);

	/**
	 * This method describes getting date format
	 * @Table INVSTOCK_COGS
	 */
	@Select("SELECT BILLDATE FROM INVSTOCK_COGS FETCH FIRST ROWS ONLY")
	String getBillDateFormat();

	/**
	 * This method describes get all record for OTB Plan Actual Data graph
	 * Assortment wise according to parameter
	 * @Table OTB_USER_ASD
	 */
	@Select("SELECT COUNT(DISTINCT ASSORTMENTCODE) FROM OTB_USER_ASD WHERE PLANID=#{planId} AND ACTIVE='1' AND TO_CHAR(TO_DATE(TO_CHAR(TO_DATE(FUTURE_BILLDATE,'yyyy-MM-dd'),'yyyy-MM'),'yyyy-MM'),'yyyy-MM-dd') BETWEEN TO_CHAR(TO_DATE(TO_CHAR(TO_DATE(#{startDate,jdbcType=VARCHAR},'yyyy-MM-dd'),'yyyy-MM'),'yyyy-MM'),'yyyy-MM-dd') AND TO_CHAR(TO_DATE(TO_CHAR(TO_DATE(#{endDate,jdbcType=VARCHAR},'yyyy-MM-dd'),'yyyy-MM'),'yyyy-MM'),'yyyy-MM-dd')")
	int getASDPlanRecord(@Param("planId") String planId, @Param("startDate") String startDate,
			@Param("endDate") String endDate);

	/**
	 * This method describes get all OTB Plan Actual Data graph
	 * Assortment wise according to parameter
	 * @Table OTB_USER_ASD
	 */
	List<OTBPlanASD> getASDPlan(@Param("offset") Integer offset, @Param("pageSize") Integer pageSize,
			@Param("planId") String planId, @Param("startDate") String startDate, @Param("endDate") String endDate);

	/**
	 * This method describes get all record for OTB Plan 
	 * Assortment wise Forecast Data graph according to parameter
	 * @Table OTB_USER_DFD
	 */
	@Select("SELECT COUNT(DISTINCT ASSORTMENTCODE) FROM OTB_USER_DFD WHERE PLANID=#{planId} AND ACTIVE='1' AND TO_CHAR(TO_DATE(TO_CHAR(TO_DATE(BILLDATE,'yyyy-MM-dd'),'yyyy-MM'),'yyyy-MM'),'yyyy-MM-dd') BETWEEN TO_CHAR(TO_DATE(TO_CHAR(TO_DATE(#{startDate,jdbcType=VARCHAR},'yyyy-MM-dd'),'yyyy-MM'),'yyyy-MM'),'yyyy-MM-dd') AND TO_CHAR(TO_DATE(TO_CHAR(TO_DATE(#{endDate,jdbcType=VARCHAR},'yyyy-MM-dd'),'yyyy-MM'),'yyyy-MM'),'yyyy-MM-dd')")
	int getDFDPlanRecord(@Param("planId") String planId, @Param("startDate") String startDate,
			@Param("endDate") String endDate);

	/**
	 * This method describes get all OTB Plan Forecast Data graph
	 * Assortment wise according to parameter
	 * @Table OTB_USER_DFD
	 */
	List<OTBPlanDFD> getDFDPlan(@Param("offset") Integer offset, @Param("pageSize") Integer pageSize,
			@Param("planId") String planId, @Param("startDate") String startDate, @Param("endDate") String endDate);

	/**
	 * This method describes get all OTB Plan Actual Data graph
	 * Total Assortment wise according to parameter
	 * @Table OTB_USER_ASD
	 */
	List<OTBPlanASD> getTotalASDPlan(@Param("planId") String planId, @Param("startDate") String startDate,
			@Param("endDate") String endDate);

	/**
	 * This method describes get all OTB Plan Forecast Data graph
	 * Total Assortment wise according to parameter
	 * @Table OTB_USER_DFD
	 */
	List<OTBPlanDFD> getTotalDFDPlan(@Param("planId") String planId, @Param("startDate") String startDate,
			@Param("endDate") String endDate);

	/**
	 * This method describes get all OTB Plan Actual Data
	 * for Export according to parameter
	 * @Table OTB_USER_ASD
	 */
	List<DownloadOTBPlanASD> importASDPlan(@Param("planId") String planId, @Param("startDate") String startDate,
			@Param("endDate") String endDate);

	/**
	 * This method describes get all OTB Plan Forecast Data
	 * for Export according to parameter
	 * @Table OTB_USER_DFD
	 */
	List<DownloadOTBPlanDFD> importDFDPlan(@Param("planId") String planId, @Param("startDate") String startDate,
			@Param("endDate") String endDate);

	/**
	 * This method describes get OTB Plan Actual Data
	 * to update percentage due to modified planned sales
	 * @Table OTB_USER_ASD
	 */
	int updateUserDataASD(@Param("percentage") Double percentage, @Param("billDate") String billDate,
			@Param("assortmentCode") String assortmentCode, @Param("newPlanSales") Double newPlanSales,
			@Param("planId") String planId);
	
	/**
	 * This method describes get OTB Plan Forecast Data
	 * to update percentage due to modified planned sales
	 * @Table OTB_USER_DFD
	 */
	int updateUserDataDFD(@Param("percentage") Double percentage, @Param("billDate") String billDate,
			@Param("assortmentCode") String assortmentCode, @Param("newPlanSales") Double newPlanSales,
			@Param("planId") String planId);
	
	/**
	 * This method describes get OTB Plan Actual Data
	 * to update percentage due to modified planned sales
	 * @Table OTB_PLAN_ASD
	 * @deprecated This method is not used forever
	 */
	int updateDataASD(@Param("percentage") Double percentage, @Param("billDate") String billDate,
			@Param("assortmentCode") String assortmentCode, @Param("newPlanSales") Double newPlanSales,
			@Param("planId") String planId);
	
	/**
	 * This method describes get OTB Plan Forecast Data
	 * to update percentage due to modified planned sales
	 * @Table OTB_PLAN_DFD
	 * @deprecated This method is not used forever
	 */
	int updateDataDFD(@Param("percentage") Double percentage, @Param("billDate") String billDate,
			@Param("assortmentCode") String assortmentCode, @Param("newPlanSales") Double newPlanSales,
			@Param("planId") String planId);
	
	/**
	 * This method describes get OTB Plan Actual Data
	 * to update percentage due to modified planned sales
	 * @Table OTB_USER_ASD
	 */
	OTBPlanASD getUserPlanDetailASD(@Param("billDate") String billDate,
			@Param("assortmentCode") String assortmentCode, @Param("planId") String planId);

	/**
	 * This method describes get OTB Plan Forecast Data
	 * to update percentage due to modified planned sales
	 * @Table OTB_USER_DFD
	 */
	OTBPlanDFD getUserPlanDetailDFD(@Param("billDate") String billDate,
			@Param("assortmentCode") String assortmentCode, @Param("planId") String planId);

	/**
	 * This method describes get OTB Plan Actual Data
	 * to update percentage due to modified planned sales
	 * @Table OTB_PLAN_ASD
	 * @deprecated This method is not used forever
	 */
	OTBPlanASD getPlanDetailASD(@Param("billDate") String billDate,
			@Param("assortmentCode") String assortmentCode, @Param("planId") String planId);

	/**
	 * This method describes get OTB Plan Forecast Data
	 * to update percentage due to modified planned sales
	 * @Table OTB_PLAN_DFD
	 * @deprecated This method is not used forever
	 */
	OTBPlanDFD getPlanDetailDFD(@Param("billDate") String billDate,
			@Param("assortmentCode") String assortmentCode, @Param("planId") String planId);

	/**
	 * This method describes update OTB for Actual Data
	 * to update modified planned sales
	 * @Table OTB_USER_ASD
	 */
	int updateByPlanSaleASD(OTBPlanASD otbPlanASD);

	/**
	 * This method describes update OTB for Forecast Data
	 * to update modified planned sales
	 * @Table OTB_USER_DFD
	 */
	int updateByPlanSaleDFD(OTBPlanDFD otbPlanDFD);

	/**
	 * This method describes get OTB plan Info due to modify
	 * Planned Sales according to parameter
	 * @Table OTB_USER_ASD
	 */
	OTBPlanASD getByPlanIdAndAssortmentcodeASD(@Param("planId") String planId,
			@Param("assortmentCode") String assortmentCode, @Param("billDate") String billDate);

	/**
	 * This method describes get OTB plan Info due to modify
	 * Planned Sales according to parameter
	 * @Table OTB_USER_DFD
	 */
	OTBPlanDFD getByPlanIdAndAssortmentcodeDFD(@Param("planId") String planId,
			@Param("assortmentCode") String assortmentCode, @Param("billDate") String billDate);

	/**
	 * This method describes get OTB plan creation status
	 * @Table OTB_CREATE_PLAN
	 */
	@Select("SELECT IS_COPIED FROM OTB_CREATE_PLAN ORDER BY PLANID DESC FETCH FIRST 1 ROWS ONLY")
	String getOTBCreateStatus();

	/**
	 * This method describes update OTB creation status
	 * @param Plan ID
	 * @Table OTB_CREATE_PLAN
	 */
	@Update("UPDATE OTB_CREATE_PLAN SET IS_COPIED=#{status} WHERE PLANID=#{planId,jdbcType=VARCHAR}")
	int updateOTBPlanStatus(@Param("planId") String planId,@Param("status") String status);
	
	/**
	 * This method describes get status for MV creation
	 * @param Module Name, Sub Module Name
	 * @Table MVIEW_REFRESH_HISTORY
	 */
	@Select("SELECT ADDITIONAL FROM MVIEW_REFRESH_HISTORY WHERE MODULE=#{moduleName} AND SUBMODULE=#{subModuleName}")
	String getOTBMVCreateStatus(@Param("moduleName")String moduleName,@Param("subModuleName")String subModuleName);

	/**
	 * This method describes update MV status
	 * @param Module Name, Sub Module Name
	 * @Table MVIEW_REFRESH_HISTORY
	 */
	@Update("UPDATE MVIEW_REFRESH_HISTORY  SET STATUS=#{status,jdbcType=VARCHAR},ADDITIONAL=#{assortmentCode,jdbcType=VARCHAR},ACTIVE=#{active,jdbcType=VARCHAR} WHERE MODULE=#{moduleName} AND SUBMODULE=#{subModuleName}")
	int updateMVStatus(@Param("moduleName")String moduleName,@Param("subModuleName")String subModuleName,@Param("status")String status,@Param("active")char active,@Param("assortmentCode")String assortmentCode);
	
	/**
	 * This method describes Existence of OTB MV Record
	 * @param Module Name, Sub Module Name
	 * @Table MVIEW_REFRESH_HISTORY
	 */
	@Select("SELECT COUNT(*) FROM MVIEW_REFRESH_HISTORY WHERE MODULE=#{moduleName} AND SUBMODULE=#{subModuleName}")
	int isExistMVForOTB(@Param("moduleName")String moduleName,@Param("subModuleName")String subModuleName);
	
	/**
	 * This method describes create OTB MV
	 * @param MViewRefreshHistory
	 * @Table MVIEW_REFRESH_HISTORY
	 */
	int createMVforOTB(MViewRefreshHistory history);
	
	/**
	 * This method describes update OTB creation status Failed
	 * @param Plan ID
	 * @Table OTB_CREATE_PLAN
	 */
	@Update("UPDATE OTB_CREATE_PLAN SET IS_COPIED='FAILED' WHERE PLANID=#{planId,jdbcType=VARCHAR}")
	int updateOTBFailedStatus(@Param("planId") String planId);
	
	@Delete("DROP MATERIALIZED VIEW OTB_FORECAST_MONTHLY")
	void dropUserOTBMVForecast();
	
	@Select("SELECT COUNT(DISTINCT ASSORTMENTCODE) FROM OTB_USER_ASD WHERE PLANID=#{planId}")
	int getQuarterASDPlanRecord(@Param("planId") String planId);
	
	List<OTBPlanASD> getQuarterASDPlan(@Param("offset") Integer offset, @Param("pageSize") Integer pageSize,
			@Param("planId") String planId);
	
	@Select("SELECT COUNT(DISTINCT ASSORTMENTCODE) FROM OTB_USER_DFD WHERE PLANID=#{planId}")
	int getQuarterDFDPlanRecord(@Param("planId") String planId);
	
	List<OTBPlanDFD> getQuarterDFDPlan(@Param("offset") Integer offset, @Param("pageSize") Integer pageSize,
			@Param("planId") String planId);
	
	List<OTBPlanASD> getTotalQuarterASDPlan(@Param("planId") String planId);
	
	List<OTBPlanDFD> getTotalQuarterDFDPlan(@Param("planId") String planId);
	
	@Select(value= "CALL DBMS_MVIEW.REFRESH('QUARTER_OTB_USER_ASD,QUARTER_OTB_USER_DFD','?')")
	@Options(statementType = StatementType.CALLABLE,fetchSize=1000)
	void refreshQuarterMV();
	
	@Select("SELECT VALUE FROM SYSTEM_DEFAULT_CONFIG WHERE KEY=#{key}")
	String getConfig(@Param("key")String key);
	
	List<DemandForecast> getAllCustomUserASDPlan(@Param("startDate") String startDate,@Param("endDate") String endDate);
	
	List<OTBPlanDFD> getAllCustomUserDFD(@Param("startDate") String startDate, @Param("endDate") String endDate);
	
	List<DemandForecast> getAllCustomQuarterUserASDPlan(@Param("startDate") String startDate,@Param("endDate") String endDate);
	
	List<OTBPlanDFD> getAllCustomQuarterUserDFD(@Param("startDate") String startDate, @Param("endDate") String endDate);
	
	List<DownloadOTBPlanASD> importQuarterASDPlan(@Param("planId") String planId, @Param("startDate") String startDate,
			@Param("endDate") String endDate);
	
	List<DownloadOTBPlanDFD> importQuarterDFDPlan(@Param("planId") String planId, @Param("startDate") String startDate,
			@Param("endDate") String endDate);
}
