package com.supplymint.layer.data.tenant.dashboard.entity;

public class ArticleMoving {
	public String articleCode;
	public String articleName;
	public String thirtyDays;
	public String thirtyDaysSign;
	public String fourtyFiveDays;
	public String fourtyFiveDaysSign;
	public String ninetyDays;
	public String ninetyDaysSign;
	public String articleRank;

	public ArticleMoving() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ArticleMoving(String articleCode, String articleName, String thirtyDays, String thirtyDaysSign,
			String fourtyFiveDays, String fourtyFiveDaysSign, String ninetyDays, String ninetyDaysSign,
			String articleRank) {
		super();
		this.articleCode = articleCode;
		this.articleName = articleName;
		this.thirtyDays = thirtyDays;
		this.thirtyDaysSign = thirtyDaysSign;
		this.fourtyFiveDays = fourtyFiveDays;
		this.fourtyFiveDaysSign = fourtyFiveDaysSign;
		this.ninetyDays = ninetyDays;
		this.ninetyDaysSign = ninetyDaysSign;
		this.articleRank = articleRank;
	}

	public String getArticleCode() {
		return articleCode;
	}

	public void setArticleCode(String articleCode) {
		this.articleCode = articleCode;
	}

	public String getArticleName() {
		return articleName;
	}

	public void setArticleName(String articleName) {
		this.articleName = articleName;
	}

	public String getThirtyDays() {
		return thirtyDays;
	}

	public void setThirtyDays(String thirtyDays) {
		this.thirtyDays = thirtyDays;
	}

	public String getThirtyDaysSign() {
		return thirtyDaysSign;
	}

	public void setThirtyDaysSign(String thirtyDaysSign) {
		this.thirtyDaysSign = thirtyDaysSign;
	}

	public String getFourtyFiveDays() {
		return fourtyFiveDays;
	}

	public void setFourtyFiveDays(String fourtyFiveDays) {
		this.fourtyFiveDays = fourtyFiveDays;
	}

	public String getFourtyFiveDaysSign() {
		return fourtyFiveDaysSign;
	}

	public void setFourtyFiveDaysSign(String fourtyFiveDaysSign) {
		this.fourtyFiveDaysSign = fourtyFiveDaysSign;
	}

	public String getNinetyDays() {
		return ninetyDays;
	}

	public void setNinetyDays(String ninetyDays) {
		this.ninetyDays = ninetyDays;
	}

	public String getNinetyDaysSign() {
		return ninetyDaysSign;
	}

	public void setNinetyDaysSign(String ninetyDaysSign) {
		this.ninetyDaysSign = ninetyDaysSign;
	}

	public String getArticleRank() {
		return articleRank;
	}

	public void setArticleRank(String articleRank) {
		this.articleRank = articleRank;
	}

	@Override
	public String toString() {
		return "ArticleMoving [articleCode=" + articleCode + ", articleName=" + articleName + ", thirtyDays="
				+ thirtyDays + ", thirtyDaysSign=" + thirtyDaysSign + ", fourtyFiveDays=" + fourtyFiveDays
				+ ", fourtyFiveDaysSign=" + fourtyFiveDaysSign + ", ninetyDays=" + ninetyDays + ", ninetyDaysSign="
				+ ninetyDaysSign + ", articleRank=" + articleRank + "]";
	}

}
