package com.supplymint.layer.data.tenant.procurement.entity;

public class HLevelData {

	private String hl1name;
	private String hl2name;
	private String hl3name;
	private String hl1code;
	private String hl2code;
	private String hl3code;

	public HLevelData() {
		super();
		// TODO Auto-generated constructor stub
	}

	public HLevelData(String hl1name, String hl2name, String hl3name, String hl1code, String hl2code, String hl3code) {
		super();
		this.hl1name = hl1name;
		this.hl2name = hl2name;
		this.hl3name = hl3name;
		this.hl1code = hl1code;
		this.hl2code = hl2code;
		this.hl3code = hl3code;
	}

	public String getHl1name() {
		return hl1name;
	}

	public void setHl1name(String hl1name) {
		this.hl1name = hl1name;
	}

	public String getHl2name() {
		return hl2name;
	}

	public void setHl2name(String hl2name) {
		this.hl2name = hl2name;
	}

	public String getHl3name() {
		return hl3name;
	}

	public void setHl3name(String hl3name) {
		this.hl3name = hl3name;
	}

	public String getHl1code() {
		return hl1code;
	}

	public void setHl1code(String hl1code) {
		this.hl1code = hl1code;
	}

	public String getHl2code() {
		return hl2code;
	}

	public void setHl2code(String hl2code) {
		this.hl2code = hl2code;
	}

	public String getHl3code() {
		return hl3code;
	}

	public void setHl3code(String hl3code) {
		this.hl3code = hl3code;
	}

	@Override
	public String toString() {
		return "HLevelData [hl1name=" + hl1name + ", hl2name=" + hl2name + ", hl3name=" + hl3name + ", hl1code="
				+ hl1code + ", hl2code=" + hl2code + ", hl3code=" + hl3code + "]";
	}

}
