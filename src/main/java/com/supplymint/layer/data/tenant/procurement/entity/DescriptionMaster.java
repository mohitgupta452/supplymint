package com.supplymint.layer.data.tenant.procurement.entity;

public class DescriptionMaster {

	String code;
	String description;
	String hl4code;

	public DescriptionMaster() {
		super();
	}

	public DescriptionMaster(String code, String description) {
		super();
		this.code = code;
		this.description = description;
	}

	public DescriptionMaster(String code, String description, String hl4code) {
		super();
		this.code = code;
		this.description = description;
		this.hl4code = hl4code;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getHl4code() {
		return hl4code;
	}

	public void setHl4code(String hl4code) {
		this.hl4code = hl4code;
	}

	@Override
	public String toString() {
		return "DescriptionMaster [code=" + code + ", description=" + description + ", hl4code=" + hl4code + "]";
	}

}
