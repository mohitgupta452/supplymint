package com.supplymint.layer.data.tenant.inventory.mapper;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;

import com.supplymint.layer.data.tenant.inventory.entity.Allocation;

@Mapper
public interface AllocationMapper {

	Integer create(Allocation allocation);
	
	@Delete("DELETE from ADC_ALLOCATION WHERE ID = #{id}")
	Integer removeById(Integer id);


}
