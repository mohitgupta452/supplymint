package com.supplymint.layer.data.tenant.administration.entity;

import java.time.OffsetDateTime;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.supplymint.util.PILocalDatetimeSerializer;

public class ADMItem {

	private int id;
	private String invItem;
	private String hL1Code;
	private String hL1Name;
	private String hL2Code;
	private String hL2Name;
	private String hL3Code;
	private String hL3Name;
	private String hL4Code;
	private String hL4Name;
	private String hL5Code;
	private String hL5Name;
	private String hL6Code;
	private String hL6Name;
	private String iCode;
	private String itemName;
	private String displayName;
	private String description;
	private String brand;
	private String color;
	private String size;
	private String pattern;
	private String material;
	private String design;
	private String assortmentBasis;
	private String assortment;
	private String type;
	private String packSize;
	private String barCode;
	private String discount;
	private double costPrice;
	private double sellPrice;
	private String applicableSeason;
	private String applicableEvent;
	private String otherFactor;
	private String uDF1;
	private String uDF2;
	private String uDF3;
	private String uDF4;
	private String uDF5;
	private String uDF6;
	private String lastChanged;
	private double listedMrp;
	private double mrpStart;
	private double mrpEnd;
	private double articleMrp;

	private double listedMRP;
	private double MRPRangeFrom;
	private double MRPRangeTo;
	private double articleMRP;
	private String hsnSacCode;
	private String invhsnSacCode;


	private char active;
	private String status;
	private String ipAddress;
	private String createdBy;

	@JsonSerialize(using = PILocalDatetimeSerializer.class)
//	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	private OffsetDateTime createdTime;
	private String updatedBy;

	@JsonSerialize(using = PILocalDatetimeSerializer.class)
//	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	private OffsetDateTime updationTime;
	private String additional;

	public ADMItem() {
		super();
	}

	public ADMItem(String hL4Code, double costPrice, double sellPrice) {
		super();
		this.hL4Code = hL4Code;
		this.costPrice = costPrice;
		this.sellPrice = sellPrice;
	}
	
	public ADMItem(int id, String invItem, String hL1Code, String hL1Name, String hL2Code, String hL2Name,
			String hL3Code, String hL3Name, String hL4Code, String hL4Name, String hL5Code, String hL5Name,
			String hL6Code, String hL6Name, String iCode, String itemName, String displayName, String description,
			String brand, String color, String size, String pattern, String material, String design,
			String assortmentBasis, String assortment, String type, String packSize, String barCode, String discount,
			double costPrice, double sellPrice, String applicableSeason, String applicableEvent, String otherFactor,
			String uDF1, String uDF2, String uDF3, String uDF4, String uDF5, String uDF6, String lastChanged,
			double listedMrp, double mrpStart, double mrpEnd, double articleMrp, double listedMRP2, double mRPRangeFrom,
			double mRPRangeTo, double articleMRP2, String hsnSacCode, String invhsnSacCode, char active, String status,
			String ipAddress, String createdBy, OffsetDateTime createdTime, String updatedBy,
			OffsetDateTime updationTime, String additional) {
		super();
		this.id = id;
		this.invItem = invItem;
		this.hL1Code = hL1Code;
		this.hL1Name = hL1Name;
		this.hL2Code = hL2Code;
		this.hL2Name = hL2Name;
		this.hL3Code = hL3Code;
		this.hL3Name = hL3Name;
		this.hL4Code = hL4Code;
		this.hL4Name = hL4Name;
		this.hL5Code = hL5Code;
		this.hL5Name = hL5Name;
		this.hL6Code = hL6Code;
		this.hL6Name = hL6Name;
		this.iCode = iCode;
		this.itemName = itemName;
		this.displayName = displayName;
		this.description = description;
		this.brand = brand;
		this.color = color;
		this.size = size;
		this.pattern = pattern;
		this.material = material;
		this.design = design;
		this.assortmentBasis = assortmentBasis;
		this.assortment = assortment;
		this.type = type;
		this.packSize = packSize;
		this.barCode = barCode;
		this.discount = discount;
		this.costPrice = costPrice;
		this.sellPrice = sellPrice;
		this.applicableSeason = applicableSeason;
		this.applicableEvent = applicableEvent;
		this.otherFactor = otherFactor;
		this.uDF1 = uDF1;
		this.uDF2 = uDF2;
		this.uDF3 = uDF3;
		this.uDF4 = uDF4;
		this.uDF5 = uDF5;
		this.uDF6 = uDF6;
		this.lastChanged = lastChanged;
		this.listedMrp = listedMrp;
		this.mrpStart = mrpStart;
		this.mrpEnd = mrpEnd;
		this.articleMrp = articleMrp;
		listedMRP = listedMRP2;
		MRPRangeFrom = mRPRangeFrom;
		MRPRangeTo = mRPRangeTo;
		articleMRP = articleMRP2;
		this.hsnSacCode = hsnSacCode;
		this.invhsnSacCode = invhsnSacCode;
		this.active = active;
		this.status = status;
		this.ipAddress = ipAddress;
		this.createdBy = createdBy;
		this.createdTime = createdTime;
		this.updatedBy = updatedBy;
		this.updationTime = updationTime;
		this.additional = additional;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getInvItem() {
		return invItem;
	}

	public void setInvItem(String invItem) {
		this.invItem = invItem;
	}

	public String gethL1Code() {
		return hL1Code;
	}

	public void sethL1Code(String hL1Code) {
		this.hL1Code = hL1Code;
	}

	public String gethL1Name() {
		return hL1Name;
	}

	public void sethL1Name(String hL1Name) {
		this.hL1Name = hL1Name;
	}

	public String gethL2Code() {
		return hL2Code;
	}

	public void sethL2Code(String hL2Code) {
		this.hL2Code = hL2Code;
	}

	public String gethL2Name() {
		return hL2Name;
	}

	public void sethL2Name(String hL2Name) {
		this.hL2Name = hL2Name;
	}

	public String gethL3Code() {
		return hL3Code;
	}

	public void sethL3Code(String hL3Code) {
		this.hL3Code = hL3Code;
	}

	public String gethL3Name() {
		return hL3Name;
	}

	public void sethL3Name(String hL3Name) {
		this.hL3Name = hL3Name;
	}

	public String gethL4Code() {
		return hL4Code;
	}

	public void sethL4Code(String hL4Code) {
		this.hL4Code = hL4Code;
	}

	public String gethL4Name() {
		return hL4Name;
	}

	public void sethL4Name(String hL4Name) {
		this.hL4Name = hL4Name;
	}

	public String gethL5Code() {
		return hL5Code;
	}

	public void sethL5Code(String hL5Code) {
		this.hL5Code = hL5Code;
	}

	public String gethL5Name() {
		return hL5Name;
	}

	public void sethL5Name(String hL5Name) {
		this.hL5Name = hL5Name;
	}

	public String gethL6Code() {
		return hL6Code;
	}

	public void sethL6Code(String hL6Code) {
		this.hL6Code = hL6Code;
	}

	public String gethL6Name() {
		return hL6Name;
	}

	public void sethL6Name(String hL6Name) {
		this.hL6Name = hL6Name;
	}

	public String getiCode() {
		return iCode;
	}

	public void setiCode(String iCode) {
		this.iCode = iCode;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	public String getMaterial() {
		return material;
	}

	public void setMaterial(String material) {
		this.material = material;
	}

	public String getDesign() {
		return design;
	}

	public void setDesign(String design) {
		this.design = design;
	}

	public String getAssortmentBasis() {
		return assortmentBasis;
	}

	public void setAssortmentBasis(String assortmentBasis) {
		this.assortmentBasis = assortmentBasis;
	}

	public String getAssortment() {
		return assortment;
	}

	public void setAssortment(String assortment) {
		this.assortment = assortment;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPackSize() {
		return packSize;
	}

	public void setPackSize(String packSize) {
		this.packSize = packSize;
	}

	public String getBarCode() {
		return barCode;
	}

	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}

	public String getDiscount() {
		return discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}

	public double getCostPrice() {
		return costPrice;
	}

	public void setCostPrice(double costPrice) {
		this.costPrice = costPrice;
	}

	public double getSellPrice() {
		return sellPrice;
	}

	public void setSellPrice(double sellPrice) {
		this.sellPrice = sellPrice;
	}

	public String getApplicableSeason() {
		return applicableSeason;
	}

	public void setApplicableSeason(String applicableSeason) {
		this.applicableSeason = applicableSeason;
	}

	public String getApplicableEvent() {
		return applicableEvent;
	}

	public void setApplicableEvent(String applicableEvent) {
		this.applicableEvent = applicableEvent;
	}

	public String getOtherFactor() {
		return otherFactor;
	}

	public void setOtherFactor(String otherFactor) {
		this.otherFactor = otherFactor;
	}

	public String getuDF1() {
		return uDF1;
	}

	public void setuDF1(String uDF1) {
		this.uDF1 = uDF1;
	}

	public String getuDF2() {
		return uDF2;
	}

	public void setuDF2(String uDF2) {
		this.uDF2 = uDF2;
	}

	public String getuDF3() {
		return uDF3;
	}

	public void setuDF3(String uDF3) {
		this.uDF3 = uDF3;
	}

	public String getuDF4() {
		return uDF4;
	}

	public void setuDF4(String uDF4) {
		this.uDF4 = uDF4;
	}

	public String getuDF5() {
		return uDF5;
	}

	public void setuDF5(String uDF5) {
		this.uDF5 = uDF5;
	}

	public String getuDF6() {
		return uDF6;
	}

	public void setuDF6(String uDF6) {
		this.uDF6 = uDF6;
	}

	public String getLastChanged() {
		return lastChanged;
	}

	public void setLastChanged(String lastChanged) {
		this.lastChanged = lastChanged;
	}

	public double getListedMrp() {
		return listedMrp;
	}

	public void setListedMrp(double listedMrp) {
		this.listedMrp = listedMrp;
	}

	public double getMrpStart() {
		return mrpStart;
	}

	public void setMrpStart(double mrpStart) {
		this.mrpStart = mrpStart;
	}

	public double getMrpEnd() {
		return mrpEnd;
	}

	public void setMrpEnd(double mrpEnd) {
		this.mrpEnd = mrpEnd;
	}

	public double getArticleMrp() {
		return articleMrp;
	}

	public void setArticleMrp(double articleMrp) {
		this.articleMrp = articleMrp;
	}

	public double getListedMRP() {
		return listedMRP;
	}

	public void setListedMRP(double listedMRP) {
		this.listedMRP = listedMRP;
	}

	public double getMRPRangeFrom() {
		return MRPRangeFrom;
	}

	public void setMRPRangeFrom(double mRPRangeFrom) {
		MRPRangeFrom = mRPRangeFrom;
	}

	public double getMRPRangeTo() {
		return MRPRangeTo;
	}

	public void setMRPRangeTo(double mRPRangeTo) {
		MRPRangeTo = mRPRangeTo;
	}

	public double getArticleMRP() {
		return articleMRP;
	}

	public void setArticleMRP(double articleMRP) {
		this.articleMRP = articleMRP;
	}

	public String getHsnSacCode() {
		return hsnSacCode;
	}

	public void setHsnSacCode(String hsnSacCode) {
		this.hsnSacCode = hsnSacCode;
	}

	public String getInvhsnSacCode() {
		return invhsnSacCode;
	}

	public void setInvhsnSacCode(String invhsnSacCode) {
		this.invhsnSacCode = invhsnSacCode;
	}

	public char getActive() {
		return active;
	}

	public void setActive(char active) {
		this.active = active;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public OffsetDateTime getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(OffsetDateTime createdTime) {
		this.createdTime = createdTime;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public OffsetDateTime getUpdationTime() {
		return updationTime;
	}

	public void setUpdationTime(OffsetDateTime updationTime) {
		this.updationTime = updationTime;
	}

	public String getAdditional() {
		return additional;
	}

	public void setAdditional(String additional) {
		this.additional = additional;
	}

	@Override
	public String toString() {
		return "ADMItem [id=" + id + ", invItem=" + invItem + ", hL1Code=" + hL1Code + ", hL1Name=" + hL1Name
				+ ", hL2Code=" + hL2Code + ", hL2Name=" + hL2Name + ", hL3Code=" + hL3Code + ", hL3Name=" + hL3Name
				+ ", hL4Code=" + hL4Code + ", hL4Name=" + hL4Name + ", hL5Code=" + hL5Code + ", hL5Name=" + hL5Name
				+ ", hL6Code=" + hL6Code + ", hL6Name=" + hL6Name + ", iCode=" + iCode + ", itemName=" + itemName
				+ ", displayName=" + displayName + ", description=" + description + ", brand=" + brand + ", color="
				+ color + ", size=" + size + ", pattern=" + pattern + ", material=" + material + ", design=" + design
				+ ", assortmentBasis=" + assortmentBasis + ", assortment=" + assortment + ", type=" + type
				+ ", packSize=" + packSize + ", barCode=" + barCode + ", discount=" + discount + ", costPrice="
				+ costPrice + ", sellPrice=" + sellPrice + ", applicableSeason=" + applicableSeason
				+ ", applicableEvent=" + applicableEvent + ", otherFactor=" + otherFactor + ", uDF1=" + uDF1 + ", uDF2="
				+ uDF2 + ", uDF3=" + uDF3 + ", uDF4=" + uDF4 + ", uDF5=" + uDF5 + ", uDF6=" + uDF6 + ", lastChanged="
				+ lastChanged + ", listedMrp=" + listedMrp + ", mrpStart=" + mrpStart + ", mrpEnd=" + mrpEnd
				+ ", articleMrp=" + articleMrp + ", listedMRP=" + listedMRP + ", MRPRangeFrom=" + MRPRangeFrom
				+ ", MRPRangeTo=" + MRPRangeTo + ", articleMRP=" + articleMRP + ", hsnSacCode=" + hsnSacCode
				+ ", invhsnSacCode=" + invhsnSacCode + ", active=" + active + ", status=" + status + ", ipAddress="
				+ ipAddress + ", createdBy=" + createdBy + ", createdTime=" + createdTime + ", updatedBy=" + updatedBy
				+ ", updationTime=" + updationTime + ", additional=" + additional + "]";
	}

	
	
	
}
