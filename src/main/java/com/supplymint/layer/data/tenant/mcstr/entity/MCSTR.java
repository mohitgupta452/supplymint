package com.supplymint.layer.data.tenant.mcstr.entity;

import java.time.OffsetDateTime;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.supplymint.util.LocalDateTimeSerializer;
import com.supplymint.util.LocalDatetimeDeserializer;

public class MCSTR {
	private Integer id;
	private Integer siteCode;
	private String siteName;
	private String assortment;
	private String strValue;
	private String status;
	private char active;
	private String ipAddress;
	private String createdBy;

	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	private OffsetDateTime createdTime;

	private String updatedBy;

	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	private OffsetDateTime updationTime;

	private String additional;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getSiteCode() {
		return siteCode;
	}

	public void setSiteCode(Integer siteCode) {
		this.siteCode = siteCode;
	}

	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	public String getAssortment() {
		return assortment;
	}

	public void setAssortment(String assortment) {
		this.assortment = assortment;
	}

	public String getStrValue() {
		return strValue;
	}

	public void setStrValue(String strValue) {
		this.strValue = strValue;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public char getActive() {
		return active;
	}

	public void setActive(char active) {
		this.active = active;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public OffsetDateTime getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(OffsetDateTime createdTime) {
		this.createdTime = createdTime;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public OffsetDateTime getUpdationTime() {
		return updationTime;
	}

	public void setUpdationTime(OffsetDateTime updationTime) {
		this.updationTime = updationTime;
	}

	public String getAdditional() {
		return additional;
	}

	public void setAdditional(String additional) {
		this.additional = additional;
	}
	
	
	public MCSTR() {
		// TODO Auto-generated constructor stub
	}

	public MCSTR(Integer id, Integer siteCode, String siteName, String assortment, String strValue, String status,
			char active, String ipAddress, String createdBy, OffsetDateTime createdTime, String updatedBy,
			OffsetDateTime updationTime, String additional) {
		super();
		this.id = id;
		this.siteCode = siteCode;
		this.siteName = siteName;
		this.assortment = assortment;
		this.strValue = strValue;
		this.status = status;
		this.active = active;
		this.ipAddress = ipAddress;
		this.createdBy = createdBy;
		this.createdTime = createdTime;
		this.updatedBy = updatedBy;
		this.updationTime = updationTime;
		this.additional = additional;
	}

	@Override
	public String toString() {
		return "MCSTR [id=" + id + ", siteCode=" + siteCode + ", siteName=" + siteName + ", assortment=" + assortment
				+ ", strValue=" + strValue + ", status=" + status + ", active=" + active + ", ipAddress=" + ipAddress
				+ ", createdBy=" + createdBy + ", createdTime=" + createdTime + ", updatedBy=" + updatedBy
				+ ", updationTime=" + updationTime + ", additional=" + additional + "]";
	}
	
	
	
	
	

}
