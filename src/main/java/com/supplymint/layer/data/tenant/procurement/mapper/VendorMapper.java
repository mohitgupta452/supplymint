package com.supplymint.layer.data.tenant.procurement.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.supplymint.layer.data.tenant.procurement.entity.Vendor;

@Mapper
public interface VendorMapper {
	
	public Vendor getById(Integer id);
	
	public List<Vendor> getAll(@Param("offset") Integer offset,@Param("pagesize") Integer pageSize);
	
	public Integer create(Vendor vendor);
	
	public Integer update(Vendor vendor);
	
	@Delete("DELETE FROM VENDOR_REG WHERE ID=#{id}")
	public Integer delete(int id);
	
	public List<Vendor> getByOrganisationName(String name);
	
	public Vendor getVendorName(String name);
	
	public Vendor getVendorCode(String code);
	
	@Select("SELECT COUNT(*) FROM VENDOR_REG")
	public int record();
	
	@Select("SELECT COUNT(*) FROM VENDOR_REG WHERE VENDORNAME=#{vendorName} OR VENDORCODE=#{vendorCode}")
	public int isValid(Vendor vendor);

}
