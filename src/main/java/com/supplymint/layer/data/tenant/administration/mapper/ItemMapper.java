/**
 * 
 */
package com.supplymint.layer.data.tenant.administration.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import com.supplymint.layer.data.tenant.administration.entity.ADMItem;

/**
 * @Authhor Manoj Singh
 * @Date 24-Sep-2018
 * @Version 1.0
 */
@Mapper
public interface ItemMapper {

	@Select("SELECT * FROM ADM_ITEM WHERE ID = #{ID}")
	ADMItem findById(Integer id);

	@Select("SELECT * FROM ADM_ITEM")
	List<ADMItem> findAll();

}
