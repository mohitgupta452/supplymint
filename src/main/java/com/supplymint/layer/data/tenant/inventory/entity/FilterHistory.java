package com.supplymint.layer.data.tenant.inventory.entity;

public class FilterHistory {

	private String from;
	private String to;
	private String frequency;

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getFrequency() {
		return frequency;
	}

	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}

	@Override
	public String toString() {
		return "FilterHistory [from=" + from + ", to=" + to + ", frequency=" + frequency + ", getFrom()=" + getFrom()
				+ ", getTo()=" + getTo() + ", getFrequency()=" + getFrequency() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
	}

}
