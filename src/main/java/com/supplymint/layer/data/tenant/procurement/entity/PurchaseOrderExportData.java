package com.supplymint.layer.data.tenant.procurement.entity;

public class PurchaseOrderExportData {

	private String vendorName;
	private String intGCode;
	private String intGHeaderId;
	private String intGLineId;
	private String orderNo;
	private String orderDate;
	private String vendorId;
	private String transporterId;
	private String agentId;
	private String agentRate;
	private String poRemarks;
	private String createdById;
	private String validFrom;
	private String validTo;
	private String merchandiserId;
	private String siteId;
	private String itemId;
	private String setRemarks;
	private String setRatio;
	private String articleId;
	private String itemName;
	private String cCode1;
	private String cCode2;
	private String cCode3;
	private String cCode4;
	private String cCode5;
	private String cCode6;
	private String cName1;
	private String cName2;
	private String cName3;
	private String cName4;
	private String cName5;
	private String cName6;
	private String desc1;
	private String desc2;
	private String desc3;
	private String desc4;
	private String desc5;
	private String desc6;
	private String mrp;
	private String listedMRP;
	private String wsp;
	private String uom;
	private String materialType;
	private String qty;
	private String rate;
	private String poItemRemarks;
	private String isImported;
	private String termCode;
	private String isValidated;
	private String validationError;
	private String docCode;
	private String setHeaderId;
	private String key;
	private String hsnCode;
	private String poudfStrin01;
	private String poudfStrin02;
	private String poudfStrin03;
	private String poudfStrin04;
	private String poudfStrin05;
	private String poudfStrin06;
	private String poudfStrin07;
	private String poudfStrin08;
	private String poudfStrin09;
	private String poudfStrin010;
	private String poudfNum01;
	private String poudfNum02;
	private String poudfNum03;
	private String poudfNum04;
	private String poudfNum05;
	private String poudfDate01;
	private String poudfDate02;
	private String poudfDate03;
	private String poudfDate04;
	private String poudfDate05;
	private String smUDFStrin01;
	private String smUDFStrin02;
	private String smUDFStrin03;
	private String smUDFStrin04;
	private String smUDFStrin05;
	private String smUDFStrin06;
	private String smUDFStrin07;
	private String smUDFStrin08;
	private String smUDFStrin09;
	private String smUDFStrin010;
	private String smUDFNum01;
	private String smUDFNum02;
	private String smUDFNum03;
	private String smUDFNum04;
	private String smUDFNum05;
	private String smUDFDate01;
	private String smUDFDate02;
	private String smUDFDate03;
	private String smUDFDate04;
	private String smUDFDate05;
	private String imUDFString01;
	private String imUDFString02;
	private String imUDFString03;
	private String imUDFString04;
	private String imUDFString05;
	private String imUDFString06;
	private String imUDFString07;
	private String imUDFString08;
	private String imUDFString09;
	private String imUDFString010;
	private String imUDFNum01;
	private String imUDFNum02;
	private String imUDFNum03;
	private String imUDFNum04;
	private String imUDFNum05;
	private String imUDFDate01;
	private String imUDFDate02;
	private String imUDFDate03;
	private String imUDFDate04;
	private String imUDFDate05;
	private String poReferenceId;
	private String orgId;
	private String motherCompany;
	private String sourcing;
	private String errorMsg;

	public PurchaseOrderExportData() {
		// TODO Auto-generated constructor stub
	}

	public PurchaseOrderExportData(String vendorName, String intGCode, String intGHeaderId, String intGLineId,
			String orderNo, String orderDate, String vendorId, String transporterId, String agentId, String agentRate,
			String poRemarks, String createdById, String validFrom, String validTo, String merchandiserId,
			String siteId, String itemId, String setRemarks, String setRatio, String articleId, String itemName,
			String cCode1, String cCode2, String cCode3, String cCode4, String cCode5, String cCode6, String cName1,
			String cName2, String cName3, String cName4, String cName5, String cName6, String desc1, String desc2,
			String desc3, String desc4, String desc5, String desc6, String mrp, String listedMRP, String wsp,
			String uom, String materialType, String qty, String rate, String poItemRemarks, String isImported,
			String termCode, String isValidated, String validationError, String docCode, String setHeaderId, String key,
			String hsnCode, String poudfStrin01, String poudfStrin02, String poudfStrin03, String poudfStrin04,
			String poudfStrin05, String poudfStrin06, String poudfStrin07, String poudfStrin08, String poudfStrin09,
			String poudfStrin010, String poudfNum01, String poudfNum02, String poudfNum03, String poudfNum04,
			String poudfNum05, String poudfDate01, String poudfDate02, String poudfDate03, String poudfDate04,
			String poudfDate05, String smUDFStrin01, String smUDFStrin02, String smUDFStrin03, String smUDFStrin04,
			String smUDFStrin05, String smUDFStrin06, String smUDFStrin07, String smUDFStrin08, String smUDFStrin09,
			String smUDFStrin010, String smUDFNum01, String smUDFNum02, String smUDFNum03, String smUDFNum04,
			String smUDFNum05, String smUDFDate01, String smUDFDate02, String smUDFDate03, String smUDFDate04,
			String smUDFDate05, String imUDFString01, String imUDFString02, String imUDFString03, String imUDFString04,
			String imUDFString05, String imUDFString06, String imUDFString07, String imUDFString08,
			String imUDFString09, String imUDFString010, String imUDFNum01, String imUDFNum02, String imUDFNum03,
			String imUDFNum04, String imUDFNum05, String imUDFDate01, String imUDFDate02, String imUDFDate03,
			String imUDFDate04, String imUDFDate05, String poReferenceId, String orgId, String motherCompany,
			String sourcing, String errorMsg) {
		super();
		this.vendorName = vendorName;
		this.intGCode = intGCode;
		this.intGHeaderId = intGHeaderId;
		this.intGLineId = intGLineId;
		this.orderNo = orderNo;
		this.orderDate = orderDate;
		this.vendorId = vendorId;
		this.transporterId = transporterId;
		this.agentId = agentId;
		this.agentRate = agentRate;
		this.poRemarks = poRemarks;
		this.createdById = createdById;
		this.validFrom = validFrom;
		this.validTo = validTo;
		this.merchandiserId = merchandiserId;
		this.siteId = siteId;
		this.itemId = itemId;
		this.setRemarks = setRemarks;
		this.setRatio = setRatio;
		this.articleId = articleId;
		this.itemName = itemName;
		this.cCode1 = cCode1;
		this.cCode2 = cCode2;
		this.cCode3 = cCode3;
		this.cCode4 = cCode4;
		this.cCode5 = cCode5;
		this.cCode6 = cCode6;
		this.cName1 = cName1;
		this.cName2 = cName2;
		this.cName3 = cName3;
		this.cName4 = cName4;
		this.cName5 = cName5;
		this.cName6 = cName6;
		this.desc1 = desc1;
		this.desc2 = desc2;
		this.desc3 = desc3;
		this.desc4 = desc4;
		this.desc5 = desc5;
		this.desc6 = desc6;
		this.mrp = mrp;
		this.listedMRP = listedMRP;
		this.wsp = wsp;
		this.uom = uom;
		this.materialType = materialType;
		this.qty = qty;
		this.rate = rate;
		this.poItemRemarks = poItemRemarks;
		this.isImported = isImported;
		this.termCode = termCode;
		this.isValidated = isValidated;
		this.validationError = validationError;
		this.docCode = docCode;
		this.setHeaderId = setHeaderId;
		this.key = key;
		this.hsnCode = hsnCode;
		this.poudfStrin01 = poudfStrin01;
		this.poudfStrin02 = poudfStrin02;
		this.poudfStrin03 = poudfStrin03;
		this.poudfStrin04 = poudfStrin04;
		this.poudfStrin05 = poudfStrin05;
		this.poudfStrin06 = poudfStrin06;
		this.poudfStrin07 = poudfStrin07;
		this.poudfStrin08 = poudfStrin08;
		this.poudfStrin09 = poudfStrin09;
		this.poudfStrin010 = poudfStrin010;
		this.poudfNum01 = poudfNum01;
		this.poudfNum02 = poudfNum02;
		this.poudfNum03 = poudfNum03;
		this.poudfNum04 = poudfNum04;
		this.poudfNum05 = poudfNum05;
		this.poudfDate01 = poudfDate01;
		this.poudfDate02 = poudfDate02;
		this.poudfDate03 = poudfDate03;
		this.poudfDate04 = poudfDate04;
		this.poudfDate05 = poudfDate05;
		this.smUDFStrin01 = smUDFStrin01;
		this.smUDFStrin02 = smUDFStrin02;
		this.smUDFStrin03 = smUDFStrin03;
		this.smUDFStrin04 = smUDFStrin04;
		this.smUDFStrin05 = smUDFStrin05;
		this.smUDFStrin06 = smUDFStrin06;
		this.smUDFStrin07 = smUDFStrin07;
		this.smUDFStrin08 = smUDFStrin08;
		this.smUDFStrin09 = smUDFStrin09;
		this.smUDFStrin010 = smUDFStrin010;
		this.smUDFNum01 = smUDFNum01;
		this.smUDFNum02 = smUDFNum02;
		this.smUDFNum03 = smUDFNum03;
		this.smUDFNum04 = smUDFNum04;
		this.smUDFNum05 = smUDFNum05;
		this.smUDFDate01 = smUDFDate01;
		this.smUDFDate02 = smUDFDate02;
		this.smUDFDate03 = smUDFDate03;
		this.smUDFDate04 = smUDFDate04;
		this.smUDFDate05 = smUDFDate05;
		this.imUDFString01 = imUDFString01;
		this.imUDFString02 = imUDFString02;
		this.imUDFString03 = imUDFString03;
		this.imUDFString04 = imUDFString04;
		this.imUDFString05 = imUDFString05;
		this.imUDFString06 = imUDFString06;
		this.imUDFString07 = imUDFString07;
		this.imUDFString08 = imUDFString08;
		this.imUDFString09 = imUDFString09;
		this.imUDFString010 = imUDFString010;
		this.imUDFNum01 = imUDFNum01;
		this.imUDFNum02 = imUDFNum02;
		this.imUDFNum03 = imUDFNum03;
		this.imUDFNum04 = imUDFNum04;
		this.imUDFNum05 = imUDFNum05;
		this.imUDFDate01 = imUDFDate01;
		this.imUDFDate02 = imUDFDate02;
		this.imUDFDate03 = imUDFDate03;
		this.imUDFDate04 = imUDFDate04;
		this.imUDFDate05 = imUDFDate05;
		this.poReferenceId = poReferenceId;
		this.orgId = orgId;
		this.motherCompany = motherCompany;
		this.sourcing = sourcing;
		this.errorMsg = errorMsg;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public String getIntGCode() {
		return intGCode;
	}

	public void setIntGCode(String intGCode) {
		this.intGCode = intGCode;
	}

	public String getIntGHeaderId() {
		return intGHeaderId;
	}

	public void setIntGHeaderId(String intGHeaderId) {
		this.intGHeaderId = intGHeaderId;
	}

	public String getIntGLineId() {
		return intGLineId;
	}

	public void setIntGLineId(String intGLineId) {
		this.intGLineId = intGLineId;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}

	public String getVendorId() {
		return vendorId;
	}

	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}

	public String getTransporterId() {
		return transporterId;
	}

	public void setTransporterId(String transporterId) {
		this.transporterId = transporterId;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public String getAgentRate() {
		return agentRate;
	}

	public void setAgentRate(String agentRate) {
		this.agentRate = agentRate;
	}

	public String getPoRemarks() {
		return poRemarks;
	}

	public void setPoRemarks(String poRemarks) {
		this.poRemarks = poRemarks;
	}

	public String getCreatedById() {
		return createdById;
	}

	public void setCreatedById(String createdById) {
		this.createdById = createdById;
	}

	public String getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(String validFrom) {
		this.validFrom = validFrom;
	}

	public String getValidTo() {
		return validTo;
	}

	public void setValidTo(String validTo) {
		this.validTo = validTo;
	}

	public String getMerchandiserId() {
		return merchandiserId;
	}

	public void setMerchandiserId(String merchandiserId) {
		this.merchandiserId = merchandiserId;
	}

	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getSetRemarks() {
		return setRemarks;
	}

	public void setSetRemarks(String setRemarks) {
		this.setRemarks = setRemarks;
	}

	public String getSetRatio() {
		return setRatio;
	}

	public void setSetRatio(String setRatio) {
		this.setRatio = setRatio;
	}

	public String getArticleId() {
		return articleId;
	}

	public void setArticleId(String articleId) {
		this.articleId = articleId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getcCode1() {
		return cCode1;
	}

	public void setcCode1(String cCode1) {
		this.cCode1 = cCode1;
	}

	public String getcCode2() {
		return cCode2;
	}

	public void setcCode2(String cCode2) {
		this.cCode2 = cCode2;
	}

	public String getcCode3() {
		return cCode3;
	}

	public void setcCode3(String cCode3) {
		this.cCode3 = cCode3;
	}

	public String getcCode4() {
		return cCode4;
	}

	public void setcCode4(String cCode4) {
		this.cCode4 = cCode4;
	}

	public String getcCode5() {
		return cCode5;
	}

	public void setcCode5(String cCode5) {
		this.cCode5 = cCode5;
	}

	public String getcCode6() {
		return cCode6;
	}

	public void setcCode6(String cCode6) {
		this.cCode6 = cCode6;
	}

	public String getcName1() {
		return cName1;
	}

	public void setcName1(String cName1) {
		this.cName1 = cName1;
	}

	public String getcName2() {
		return cName2;
	}

	public void setcName2(String cName2) {
		this.cName2 = cName2;
	}

	public String getcName3() {
		return cName3;
	}

	public void setcName3(String cName3) {
		this.cName3 = cName3;
	}

	public String getcName4() {
		return cName4;
	}

	public void setcName4(String cName4) {
		this.cName4 = cName4;
	}

	public String getcName5() {
		return cName5;
	}

	public void setcName5(String cName5) {
		this.cName5 = cName5;
	}

	public String getcName6() {
		return cName6;
	}

	public void setcName6(String cName6) {
		this.cName6 = cName6;
	}

	public String getDesc1() {
		return desc1;
	}

	public void setDesc1(String desc1) {
		this.desc1 = desc1;
	}

	public String getDesc2() {
		return desc2;
	}

	public void setDesc2(String desc2) {
		this.desc2 = desc2;
	}

	public String getDesc3() {
		return desc3;
	}

	public void setDesc3(String desc3) {
		this.desc3 = desc3;
	}

	public String getDesc4() {
		return desc4;
	}

	public void setDesc4(String desc4) {
		this.desc4 = desc4;
	}

	public String getDesc5() {
		return desc5;
	}

	public void setDesc5(String desc5) {
		this.desc5 = desc5;
	}

	public String getDesc6() {
		return desc6;
	}

	public void setDesc6(String desc6) {
		this.desc6 = desc6;
	}

	public String getMrp() {
		return mrp;
	}

	public void setMrp(String mrp) {
		this.mrp = mrp;
	}

	public String getListedMRP() {
		return listedMRP;
	}

	public void setListedMRP(String listedMRP) {
		this.listedMRP = listedMRP;
	}

	public String getWsp() {
		return wsp;
	}

	public void setWsp(String wsp) {
		this.wsp = wsp;
	}

	public String getUom() {
		return uom;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}

	public String getMaterialType() {
		return materialType;
	}

	public void setMaterialType(String materialType) {
		this.materialType = materialType;
	}

	public String getQty() {
		return qty;
	}

	public void setQty(String qty) {
		this.qty = qty;
	}

	public String getRate() {
		return rate;
	}

	public void setRate(String rate) {
		this.rate = rate;
	}

	public String getPoItemRemarks() {
		return poItemRemarks;
	}

	public void setPoItemRemarks(String poItemRemarks) {
		this.poItemRemarks = poItemRemarks;
	}

	public String getIsImported() {
		return isImported;
	}

	public void setIsImported(String isImported) {
		this.isImported = isImported;
	}

	public String getTermCode() {
		return termCode;
	}

	public void setTermCode(String termCode) {
		this.termCode = termCode;
	}

	public String getIsValidated() {
		return isValidated;
	}

	public void setIsValidated(String isValidated) {
		this.isValidated = isValidated;
	}

	public String getValidationError() {
		return validationError;
	}

	public void setValidationError(String validationError) {
		this.validationError = validationError;
	}

	public String getDocCode() {
		return docCode;
	}

	public void setDocCode(String docCode) {
		this.docCode = docCode;
	}

	public String getSetHeaderId() {
		return setHeaderId;
	}

	public void setSetHeaderId(String setHeaderId) {
		this.setHeaderId = setHeaderId;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getHsnCode() {
		return hsnCode;
	}

	public void setHsnCode(String hsnCode) {
		this.hsnCode = hsnCode;
	}

	public String getPoudfStrin01() {
		return poudfStrin01;
	}

	public void setPoudfStrin01(String poudfStrin01) {
		this.poudfStrin01 = poudfStrin01;
	}

	public String getPoudfStrin02() {
		return poudfStrin02;
	}

	public void setPoudfStrin02(String poudfStrin02) {
		this.poudfStrin02 = poudfStrin02;
	}

	public String getPoudfStrin03() {
		return poudfStrin03;
	}

	public void setPoudfStrin03(String poudfStrin03) {
		this.poudfStrin03 = poudfStrin03;
	}

	public String getPoudfStrin04() {
		return poudfStrin04;
	}

	public void setPoudfStrin04(String poudfStrin04) {
		this.poudfStrin04 = poudfStrin04;
	}

	public String getPoudfStrin05() {
		return poudfStrin05;
	}

	public void setPoudfStrin05(String poudfStrin05) {
		this.poudfStrin05 = poudfStrin05;
	}

	public String getPoudfStrin06() {
		return poudfStrin06;
	}

	public void setPoudfStrin06(String poudfStrin06) {
		this.poudfStrin06 = poudfStrin06;
	}

	public String getPoudfStrin07() {
		return poudfStrin07;
	}

	public void setPoudfStrin07(String poudfStrin07) {
		this.poudfStrin07 = poudfStrin07;
	}

	public String getPoudfStrin08() {
		return poudfStrin08;
	}

	public void setPoudfStrin08(String poudfStrin08) {
		this.poudfStrin08 = poudfStrin08;
	}

	public String getPoudfStrin09() {
		return poudfStrin09;
	}

	public void setPoudfStrin09(String poudfStrin09) {
		this.poudfStrin09 = poudfStrin09;
	}

	public String getPoudfStrin010() {
		return poudfStrin010;
	}

	public void setPoudfStrin010(String poudfStrin010) {
		this.poudfStrin010 = poudfStrin010;
	}

	public String getPoudfNum01() {
		return poudfNum01;
	}

	public void setPoudfNum01(String poudfNum01) {
		this.poudfNum01 = poudfNum01;
	}

	public String getPoudfNum02() {
		return poudfNum02;
	}

	public void setPoudfNum02(String poudfNum02) {
		this.poudfNum02 = poudfNum02;
	}

	public String getPoudfNum03() {
		return poudfNum03;
	}

	public void setPoudfNum03(String poudfNum03) {
		this.poudfNum03 = poudfNum03;
	}

	public String getPoudfNum04() {
		return poudfNum04;
	}

	public void setPoudfNum04(String poudfNum04) {
		this.poudfNum04 = poudfNum04;
	}

	public String getPoudfNum05() {
		return poudfNum05;
	}

	public void setPoudfNum05(String poudfNum05) {
		this.poudfNum05 = poudfNum05;
	}

	public String getPoudfDate01() {
		return poudfDate01;
	}

	public void setPoudfDate01(String poudfDate01) {
		this.poudfDate01 = poudfDate01;
	}

	public String getPoudfDate02() {
		return poudfDate02;
	}

	public void setPoudfDate02(String poudfDate02) {
		this.poudfDate02 = poudfDate02;
	}

	public String getPoudfDate03() {
		return poudfDate03;
	}

	public void setPoudfDate03(String poudfDate03) {
		this.poudfDate03 = poudfDate03;
	}

	public String getPoudfDate04() {
		return poudfDate04;
	}

	public void setPoudfDate04(String poudfDate04) {
		this.poudfDate04 = poudfDate04;
	}

	public String getPoudfDate05() {
		return poudfDate05;
	}

	public void setPoudfDate05(String poudfDate05) {
		this.poudfDate05 = poudfDate05;
	}

	public String getSmUDFStrin01() {
		return smUDFStrin01;
	}

	public void setSmUDFStrin01(String smUDFStrin01) {
		this.smUDFStrin01 = smUDFStrin01;
	}

	public String getSmUDFStrin02() {
		return smUDFStrin02;
	}

	public void setSmUDFStrin02(String smUDFStrin02) {
		this.smUDFStrin02 = smUDFStrin02;
	}

	public String getSmUDFStrin03() {
		return smUDFStrin03;
	}

	public void setSmUDFStrin03(String smUDFStrin03) {
		this.smUDFStrin03 = smUDFStrin03;
	}

	public String getSmUDFStrin04() {
		return smUDFStrin04;
	}

	public void setSmUDFStrin04(String smUDFStrin04) {
		this.smUDFStrin04 = smUDFStrin04;
	}

	public String getSmUDFStrin05() {
		return smUDFStrin05;
	}

	public void setSmUDFStrin05(String smUDFStrin05) {
		this.smUDFStrin05 = smUDFStrin05;
	}

	public String getSmUDFStrin06() {
		return smUDFStrin06;
	}

	public void setSmUDFStrin06(String smUDFStrin06) {
		this.smUDFStrin06 = smUDFStrin06;
	}

	public String getSmUDFStrin07() {
		return smUDFStrin07;
	}

	public void setSmUDFStrin07(String smUDFStrin07) {
		this.smUDFStrin07 = smUDFStrin07;
	}

	public String getSmUDFStrin08() {
		return smUDFStrin08;
	}

	public void setSmUDFStrin08(String smUDFStrin08) {
		this.smUDFStrin08 = smUDFStrin08;
	}

	public String getSmUDFStrin09() {
		return smUDFStrin09;
	}

	public void setSmUDFStrin09(String smUDFStrin09) {
		this.smUDFStrin09 = smUDFStrin09;
	}

	public String getSmUDFStrin010() {
		return smUDFStrin010;
	}

	public void setSmUDFStrin010(String smUDFStrin010) {
		this.smUDFStrin010 = smUDFStrin010;
	}

	public String getSmUDFNum01() {
		return smUDFNum01;
	}

	public void setSmUDFNum01(String smUDFNum01) {
		this.smUDFNum01 = smUDFNum01;
	}

	public String getSmUDFNum02() {
		return smUDFNum02;
	}

	public void setSmUDFNum02(String smUDFNum02) {
		this.smUDFNum02 = smUDFNum02;
	}

	public String getSmUDFNum03() {
		return smUDFNum03;
	}

	public void setSmUDFNum03(String smUDFNum03) {
		this.smUDFNum03 = smUDFNum03;
	}

	public String getSmUDFNum04() {
		return smUDFNum04;
	}

	public void setSmUDFNum04(String smUDFNum04) {
		this.smUDFNum04 = smUDFNum04;
	}

	public String getSmUDFNum05() {
		return smUDFNum05;
	}

	public void setSmUDFNum05(String smUDFNum05) {
		this.smUDFNum05 = smUDFNum05;
	}

	public String getSmUDFDate01() {
		return smUDFDate01;
	}

	public void setSmUDFDate01(String smUDFDate01) {
		this.smUDFDate01 = smUDFDate01;
	}

	public String getSmUDFDate02() {
		return smUDFDate02;
	}

	public void setSmUDFDate02(String smUDFDate02) {
		this.smUDFDate02 = smUDFDate02;
	}

	public String getSmUDFDate03() {
		return smUDFDate03;
	}

	public void setSmUDFDate03(String smUDFDate03) {
		this.smUDFDate03 = smUDFDate03;
	}

	public String getSmUDFDate04() {
		return smUDFDate04;
	}

	public void setSmUDFDate04(String smUDFDate04) {
		this.smUDFDate04 = smUDFDate04;
	}

	public String getSmUDFDate05() {
		return smUDFDate05;
	}

	public void setSmUDFDate05(String smUDFDate05) {
		this.smUDFDate05 = smUDFDate05;
	}

	public String getImUDFString01() {
		return imUDFString01;
	}

	public void setImUDFString01(String imUDFString01) {
		this.imUDFString01 = imUDFString01;
	}

	public String getImUDFString02() {
		return imUDFString02;
	}

	public void setImUDFString02(String imUDFString02) {
		this.imUDFString02 = imUDFString02;
	}

	public String getImUDFString03() {
		return imUDFString03;
	}

	public void setImUDFString03(String imUDFString03) {
		this.imUDFString03 = imUDFString03;
	}

	public String getImUDFString04() {
		return imUDFString04;
	}

	public void setImUDFString04(String imUDFString04) {
		this.imUDFString04 = imUDFString04;
	}

	public String getImUDFString05() {
		return imUDFString05;
	}

	public void setImUDFString05(String imUDFString05) {
		this.imUDFString05 = imUDFString05;
	}

	public String getImUDFString06() {
		return imUDFString06;
	}

	public void setImUDFString06(String imUDFString06) {
		this.imUDFString06 = imUDFString06;
	}

	public String getImUDFString07() {
		return imUDFString07;
	}

	public void setImUDFString07(String imUDFString07) {
		this.imUDFString07 = imUDFString07;
	}

	public String getImUDFString08() {
		return imUDFString08;
	}

	public void setImUDFString08(String imUDFString08) {
		this.imUDFString08 = imUDFString08;
	}

	public String getImUDFString09() {
		return imUDFString09;
	}

	public void setImUDFString09(String imUDFString09) {
		this.imUDFString09 = imUDFString09;
	}

	public String getImUDFString010() {
		return imUDFString010;
	}

	public void setImUDFString010(String imUDFString010) {
		this.imUDFString010 = imUDFString010;
	}

	public String getImUDFNum01() {
		return imUDFNum01;
	}

	public void setImUDFNum01(String imUDFNum01) {
		this.imUDFNum01 = imUDFNum01;
	}

	public String getImUDFNum02() {
		return imUDFNum02;
	}

	public void setImUDFNum02(String imUDFNum02) {
		this.imUDFNum02 = imUDFNum02;
	}

	public String getImUDFNum03() {
		return imUDFNum03;
	}

	public void setImUDFNum03(String imUDFNum03) {
		this.imUDFNum03 = imUDFNum03;
	}

	public String getImUDFNum04() {
		return imUDFNum04;
	}

	public void setImUDFNum04(String imUDFNum04) {
		this.imUDFNum04 = imUDFNum04;
	}

	public String getImUDFNum05() {
		return imUDFNum05;
	}

	public void setImUDFNum05(String imUDFNum05) {
		this.imUDFNum05 = imUDFNum05;
	}

	public String getImUDFDate01() {
		return imUDFDate01;
	}

	public void setImUDFDate01(String imUDFDate01) {
		this.imUDFDate01 = imUDFDate01;
	}

	public String getImUDFDate02() {
		return imUDFDate02;
	}

	public void setImUDFDate02(String imUDFDate02) {
		this.imUDFDate02 = imUDFDate02;
	}

	public String getImUDFDate03() {
		return imUDFDate03;
	}

	public void setImUDFDate03(String imUDFDate03) {
		this.imUDFDate03 = imUDFDate03;
	}

	public String getImUDFDate04() {
		return imUDFDate04;
	}

	public void setImUDFDate04(String imUDFDate04) {
		this.imUDFDate04 = imUDFDate04;
	}

	public String getImUDFDate05() {
		return imUDFDate05;
	}

	public void setImUDFDate05(String imUDFDate05) {
		this.imUDFDate05 = imUDFDate05;
	}

	public String getPoReferenceId() {
		return poReferenceId;
	}

	public void setPoReferenceId(String poReferenceId) {
		this.poReferenceId = poReferenceId;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getMotherCompany() {
		return motherCompany;
	}

	public void setMotherCompany(String motherCompany) {
		this.motherCompany = motherCompany;
	}

	public String getSourcing() {
		return sourcing;
	}

	public void setSourcing(String sourcing) {
		this.sourcing = sourcing;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	@Override
	public String toString() {
		return "PurchaseOrderExportData [vendorName=" + vendorName + ", intGCode=" + intGCode + ", intGHeaderId="
				+ intGHeaderId + ", intGLineId=" + intGLineId + ", orderNo=" + orderNo + ", orderDate=" + orderDate
				+ ", vendorId=" + vendorId + ", transporterId=" + transporterId + ", agentId=" + agentId
				+ ", agentRate=" + agentRate + ", poRemarks=" + poRemarks + ", createdById=" + createdById
				+ ", validFrom=" + validFrom + ", validTo=" + validTo + ", merchandiserId=" + merchandiserId
				+ ", siteId=" + siteId + ", itemId=" + itemId + ", setRemarks=" + setRemarks + ", setRatio=" + setRatio
				+ ", articleId=" + articleId + ", itemName=" + itemName + ", cCode1=" + cCode1 + ", cCode2=" + cCode2
				+ ", cCode3=" + cCode3 + ", cCode4=" + cCode4 + ", cCode5=" + cCode5 + ", cCode6=" + cCode6
				+ ", cName1=" + cName1 + ", cName2=" + cName2 + ", cName3=" + cName3 + ", cName4=" + cName4
				+ ", cName5=" + cName5 + ", cName6=" + cName6 + ", desc1=" + desc1 + ", desc2=" + desc2 + ", desc3="
				+ desc3 + ", desc4=" + desc4 + ", desc5=" + desc5 + ", desc6=" + desc6 + ", mrp=" + mrp + ", listedMRP="
				+ listedMRP + ", wsp=" + wsp + ", uom=" + uom + ", materialType=" + materialType + ", qty=" + qty
				+ ", rate=" + rate + ", poItemRemarks=" + poItemRemarks + ", isImported=" + isImported + ", termCode="
				+ termCode + ", isValidated=" + isValidated + ", validationError=" + validationError + ", docCode="
				+ docCode + ", setHeaderId=" + setHeaderId + ", key=" + key + ", hsnCode=" + hsnCode + ", poudfStrin01="
				+ poudfStrin01 + ", poudfStrin02=" + poudfStrin02 + ", poudfStrin03=" + poudfStrin03 + ", poudfStrin04="
				+ poudfStrin04 + ", poudfStrin05=" + poudfStrin05 + ", poudfStrin06=" + poudfStrin06 + ", poudfStrin07="
				+ poudfStrin07 + ", poudfStrin08=" + poudfStrin08 + ", poudfStrin09=" + poudfStrin09
				+ ", poudfStrin010=" + poudfStrin010 + ", poudfNum01=" + poudfNum01 + ", poudfNum02=" + poudfNum02
				+ ", poudfNum03=" + poudfNum03 + ", poudfNum04=" + poudfNum04 + ", poudfNum05=" + poudfNum05
				+ ", poudfDate01=" + poudfDate01 + ", poudfDate02=" + poudfDate02 + ", poudfDate03=" + poudfDate03
				+ ", poudfDate04=" + poudfDate04 + ", poudfDate05=" + poudfDate05 + ", smUDFStrin01=" + smUDFStrin01
				+ ", smUDFStrin02=" + smUDFStrin02 + ", smUDFStrin03=" + smUDFStrin03 + ", smUDFStrin04=" + smUDFStrin04
				+ ", smUDFStrin05=" + smUDFStrin05 + ", smUDFStrin06=" + smUDFStrin06 + ", smUDFStrin07=" + smUDFStrin07
				+ ", smUDFStrin08=" + smUDFStrin08 + ", smUDFStrin09=" + smUDFStrin09 + ", smUDFStrin010="
				+ smUDFStrin010 + ", smUDFNum01=" + smUDFNum01 + ", smUDFNum02=" + smUDFNum02 + ", smUDFNum03="
				+ smUDFNum03 + ", smUDFNum04=" + smUDFNum04 + ", smUDFNum05=" + smUDFNum05 + ", smUDFDate01="
				+ smUDFDate01 + ", smUDFDate02=" + smUDFDate02 + ", smUDFDate03=" + smUDFDate03 + ", smUDFDate04="
				+ smUDFDate04 + ", smUDFDate05=" + smUDFDate05 + ", imUDFString01=" + imUDFString01 + ", imUDFString02="
				+ imUDFString02 + ", imUDFString03=" + imUDFString03 + ", imUDFString04=" + imUDFString04
				+ ", imUDFString05=" + imUDFString05 + ", imUDFString06=" + imUDFString06 + ", imUDFString07="
				+ imUDFString07 + ", imUDFString08=" + imUDFString08 + ", imUDFString09=" + imUDFString09
				+ ", imUDFString010=" + imUDFString010 + ", imUDFNum01=" + imUDFNum01 + ", imUDFNum02=" + imUDFNum02
				+ ", imUDFNum03=" + imUDFNum03 + ", imUDFNum04=" + imUDFNum04 + ", imUDFNum05=" + imUDFNum05
				+ ", imUDFDate01=" + imUDFDate01 + ", imUDFDate02=" + imUDFDate02 + ", imUDFDate03=" + imUDFDate03
				+ ", imUDFDate04=" + imUDFDate04 + ", imUDFDate05=" + imUDFDate05 + ", poReferenceId=" + poReferenceId
				+ ", orgId=" + orgId + ", motherCompany=" + motherCompany + ", sourcing=" + sourcing + ", errorMsg="
				+ errorMsg + "]";
	}

}
