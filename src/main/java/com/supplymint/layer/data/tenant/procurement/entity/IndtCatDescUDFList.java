package com.supplymint.layer.data.tenant.procurement.entity;

public class IndtCatDescUDFList {

	private int code;
	private String name;
	private String isShow;
	private String catType;

	public IndtCatDescUDFList() {
		// TODO Auto-generated constructor stub
	}

	public IndtCatDescUDFList(int code, String name, String isShow, String catType) {
		super();
		this.code = code;
		this.name = name;
		this.isShow = isShow;
		this.catType = catType;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIsShow() {
		return isShow;
	}

	public void setIsShow(String isShow) {
		this.isShow = isShow;
	}

	public String getCatType() {
		return catType;
	}

	public void setCatType(String catType) {
		this.catType = catType;
	}

	@Override
	public String toString() {
		return "IndtCatDescUDFList [code=" + code + ", name=" + name + ", isShow=" + isShow + ", catType=" + catType
				+ "]";
	}



}
