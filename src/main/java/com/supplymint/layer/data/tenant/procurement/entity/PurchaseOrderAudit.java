package com.supplymint.layer.data.tenant.procurement.entity;

import java.time.OffsetDateTime;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.supplymint.util.LocalDatetimeDeserializer;
import com.supplymint.util.PILocalDatetimeSerializer;

public class PurchaseOrderAudit {

	private int id;
	private String url;
	private String headers;
	private String body;
	private String poReferenceId;
	private int httpStatus;
	private String httpMessage;

	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	private OffsetDateTime createdAt;

	public PurchaseOrderAudit() {
		super();
	}

	public PurchaseOrderAudit(String url, String headers, String body, String poReferenceId, int httpStatus,
			String httpMessage) {
		super();
		this.url = url;
		this.headers = headers;
		this.body = body;
		this.poReferenceId = poReferenceId;
		this.httpStatus = httpStatus;
		this.httpMessage = httpMessage;
	}

	public PurchaseOrderAudit(int id, String url, String headers, String body, String poReferenceId, int httpStatus,
			String httpMessage, OffsetDateTime createdAt) {
		super();
		this.id = id;
		this.url = url;
		this.headers = headers;
		this.body = body;
		this.poReferenceId = poReferenceId;
		this.httpStatus = httpStatus;
		this.httpMessage = httpMessage;
		this.createdAt = createdAt;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getHeaders() {
		return headers;
	}

	public void setHeaders(String headers) {
		this.headers = headers;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getPoReferenceId() {
		return poReferenceId;
	}

	public void setPoReferenceId(String poReferenceId) {
		this.poReferenceId = poReferenceId;
	}

	public int getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(int httpStatus) {
		this.httpStatus = httpStatus;
	}

	public String getHttpMessage() {
		return httpMessage;
	}

	public void setHttpMessage(String httpMessage) {
		this.httpMessage = httpMessage;
	}

	public OffsetDateTime getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(OffsetDateTime createdAt) {
		this.createdAt = createdAt;
	}

	@Override
	public String toString() {
		return "PurchaseOrderAudit [id=" + id + ", url=" + url + ", headers=" + headers + ", body=" + body
				+ ", poReferenceId=" + poReferenceId + ", httpStatus=" + httpStatus + ", httpMessage=" + httpMessage
				+ ", createdAt=" + createdAt + "]";
	}

}
