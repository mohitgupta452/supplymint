package com.supplymint.layer.data.tenant.dashboard.entity;

public class StoreRank {
	public String type;
	public String storeCode;
	public String storeName;
	public String storeRank;
	public String week;
	public String month;
	public String year;

	public StoreRank() {
		super();
		// TODO Auto-generated constructor stub
	}

	public StoreRank(String type, String storeCode, String storeName, String storeRank, String week, String month,
			String year) {
		super();
		this.type = type;
		this.storeCode = storeCode;
		this.storeName = storeName;
		this.storeRank = storeRank;
		this.week = week;
		this.month = month;
		this.year = year;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getStoreCode() {
		return storeCode;
	}

	public void setStoreCode(String storeCode) {
		this.storeCode = storeCode;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getStoreRank() {
		return storeRank;
	}

	public void setStoreRank(String storeRank) {
		this.storeRank = storeRank;
	}

	public String getWeek() {
		return week;
	}

	public void setWeek(String week) {
		this.week = week;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	@Override
	public String toString() {
		return "StoreArticleRank [type=" + type + ", storeCode=" + storeCode + ", storeName=" + storeName
				+ ", storeRank=" + storeRank + ", week=" + week + ", month=" + month + ", year=" + year + "]";
	}

}
