package com.supplymint.layer.data.tenant.administration.entity;

public class FMCGStores {

	public String motherCompany;
	public String articleCode;
	public String articleName;
	public String vendorName;
	public String vendorCode;
	public String storeName;
	public String storeCode;

	public FMCGStores() {
		// TODO Auto-generated constructor stub
	}

	public FMCGStores(String motherCompany, String articleCode, String articleName, String vendorName,
			String vendorCode, String storeName, String storeCode) {
		super();
		this.motherCompany = motherCompany;
		this.articleCode = articleCode;
		this.articleName = articleName;
		this.vendorName = vendorName;
		this.vendorCode = vendorCode;
		this.storeName = storeName;
		this.storeCode = storeCode;
	}

	public String getMotherCompany() {
		return motherCompany;
	}

	public void setMotherCompany(String motherCompany) {
		this.motherCompany = motherCompany;
	}

	public String getArticleCode() {
		return articleCode;
	}

	public void setArticleCode(String articleCode) {
		this.articleCode = articleCode;
	}

	public String getArticleName() {
		return articleName;
	}

	public void setArticleName(String articleName) {
		this.articleName = articleName;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public String getVendorCode() {
		return vendorCode;
	}

	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getStoreCode() {
		return storeCode;
	}

	public void setStoreCode(String storeCode) {
		this.storeCode = storeCode;
	}

	@Override
	public String toString() {
		return "FMCGStores [motherCompany=" + motherCompany + ", articleCode=" + articleCode + ", articleName="
				+ articleName + ", vendorName=" + vendorName + ", vendorCode=" + vendorCode + ", storeName=" + storeName
				+ ", storeCode=" + storeCode + "]";
	}

}
