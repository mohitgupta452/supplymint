package com.supplymint.layer.data.tenant.procurement.entity;

import java.util.List;

public class TempPurchaseOrderChild {

	private int id;
	private List<Category> colors;
	private List<Category> sizes;
	private List<RatioList> ratios;
	private List<String> images;

	private String orderId;
	private String ratio;
	private String size;
	private String color;

	@Override
	public String toString() {
		return "TempPurchaseOrderChild [id=" + id + ", colors=" + colors + ", sizes=" + sizes + ", ratios=" + ratios
				+ ", images=" + images + ", orderId=" + orderId + ", ratio=" + ratio + ", size=" + size + ", color="
				+ color + "]";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<Category> getColors() {
		return colors;
	}

	public void setColors(List<Category> colors) {
		this.colors = colors;
	}

	public List<Category> getSizes() {
		return sizes;
	}

	public void setSizes(List<Category> sizes) {
		this.sizes = sizes;
	}

	public List<RatioList> getRatios() {
		return ratios;
	}

	public void setRatios(List<RatioList> ratios) {
		this.ratios = ratios;
	}

	public List<String> getImages() {
		return images;
	}

	public void setImages(List<String> images) {
		this.images = images;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getRatio() {
		return ratio;
	}

	public void setRatio(String ratio) {
		this.ratio = ratio;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

}
