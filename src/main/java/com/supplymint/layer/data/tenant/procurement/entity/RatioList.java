package com.supplymint.layer.data.tenant.procurement.entity;

public class RatioList {
	private int id;
	private int ratio;

	public RatioList() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RatioList(int id, int ratio) {
		super();
		this.id = id;
		this.ratio = ratio;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getRatio() {
		return ratio;
	}

	public void setRatio(int ratio) {
		this.ratio = ratio;
	}

	@Override
	public String toString() {
		return "RatioList [id=" + id + ", ratio=" + ratio + "]";
	}

}
