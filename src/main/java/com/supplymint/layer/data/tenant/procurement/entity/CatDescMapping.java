package com.supplymint.layer.data.tenant.procurement.entity;

import java.util.Date;

public class CatDescMapping {
	private int id;
	private String orgId;
	private String supplymintName;
	private String supplymintKey;
	private String clientName;
	private String clientKey;
	private String status;
	private char active;
	private String ipAddress;
	private String createdBy;
	private Date createdOn;
	private String updatedBy;
	private Date updatedOn;
	private String additional;

	public CatDescMapping() {
		super();
	}

	public CatDescMapping(int id, String orgId, String supplymintName, String supplymintKey, String clientName,
			String clientKey, String status, char active, String ipAddress, String createdBy, Date createdOn,
			String updatedBy, Date updatedOn, String additional) {
		super();
		this.id = id;
		this.orgId = orgId;
		this.supplymintName = supplymintName;
		this.supplymintKey = supplymintKey;
		this.clientName = clientName;
		this.clientKey = clientKey;
		this.status = status;
		this.active = active;
		this.ipAddress = ipAddress;
		this.createdBy = createdBy;
		this.createdOn = createdOn;
		this.updatedBy = updatedBy;
		this.updatedOn = updatedOn;
		this.additional = additional;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getSupplymintName() {
		return supplymintName;
	}

	public void setSupplymintName(String supplymintName) {
		this.supplymintName = supplymintName;
	}

	public String getSupplymintKey() {
		return supplymintKey;
	}

	public void setSupplymintKey(String supplymintKey) {
		this.supplymintKey = supplymintKey;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getClientKey() {
		return clientKey;
	}

	public void setClientKey(String clientKey) {
		this.clientKey = clientKey;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public char getActive() {
		return active;
	}

	public void setActive(char active) {
		this.active = active;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getAdditional() {
		return additional;
	}

	public void setAdditional(String additional) {
		this.additional = additional;
	}

	@Override
	public String toString() {
		return "CatDescMapping [id=" + id + ", orgId=" + orgId + ", supplymintName=" + supplymintName
				+ ", supplymintKey=" + supplymintKey + ", clientName=" + clientName + ", clientKey=" + clientKey
				+ ", status=" + status + ", active=" + active + ", ipAddress=" + ipAddress + ", createdBy=" + createdBy
				+ ", createdOn=" + createdOn + ", updatedBy=" + updatedBy + ", updatedOn=" + updatedOn + ", additional="
				+ additional + "]";
	}

}
