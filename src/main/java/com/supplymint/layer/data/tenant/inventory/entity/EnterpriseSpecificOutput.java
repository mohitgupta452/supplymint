/**
 * 
 */
package com.supplymint.layer.data.tenant.inventory.entity;

/**
 * @author Manoj Singh
 *
 */
public class EnterpriseSpecificOutput {

	private String UNIQUE_CODE;
	private String WH_CODE;
	private String STORE_CODE;
	private String STORE_NAME;
	private String STORE_RANK;
	private String STORE_ZONE;
	private String STORE_GRADE;
//	private String ARTICLE_NAME;
//	private String ART_SIZE;
	private String BARCODE;
	private String OPTION_TYPE;
	private String OPTION_NAME;
	private String OPTION_SIZE;
	private String OPTION_GRADE;
	private String STOCK_AVAILABLE;
	private String SYNCTIME;
	private String FINAL_MBQ;
	private String FINAL_REQUIREMENT;
	private String ALLOCATED_QTY;

	public EnterpriseSpecificOutput() {

	}

	public EnterpriseSpecificOutput(String wH_CODE) {
		super();
		this.WH_CODE = wH_CODE;
	}

	public String getUNIQUE_CODE() {
		return UNIQUE_CODE;
	}

	public void setUNIQUE_CODE(String uNIQUE_CODE) {
		UNIQUE_CODE = uNIQUE_CODE;
	}

	public String getWH_CODE() {
		return WH_CODE;
	}

	public void setWH_CODE(String wH_CODE) {
		WH_CODE = wH_CODE;
	}

	public String getSTORE_CODE() {
		return STORE_CODE;
	}

	public void setSTORE_CODE(String sTORE_CODE) {
		STORE_CODE = sTORE_CODE;
	}

	public String getSTORE_NAME() {
		return STORE_NAME;
	}

	public void setSTORE_NAME(String sTORE_NAME) {
		STORE_NAME = sTORE_NAME;
	}

	public String getSTORE_RANK() {
		return STORE_RANK;
	}

	public void setSTORE_RANK(String sTORE_RANK) {
		STORE_RANK = sTORE_RANK;
	}

	public String getSTORE_ZONE() {
		return STORE_ZONE;
	}

	public void setSTORE_ZONE(String sTORE_ZONE) {
		STORE_ZONE = sTORE_ZONE;
	}

	public String getSTORE_GRADE() {
		return STORE_GRADE;
	}

	public void setSTORE_GRADE(String sTORE_GRADE) {
		STORE_GRADE = sTORE_GRADE;
	}

	public String getBARCODE() {
		return BARCODE;
	}

	public void setBARCODE(String bARCODE) {
		BARCODE = bARCODE;
	}

	public String getOPTION_TYPE() {
		return OPTION_TYPE;
	}

	public void setOPTION_TYPE(String oPTION_TYPE) {
		OPTION_TYPE = oPTION_TYPE;
	}

	public String getOPTION_NAME() {
		return OPTION_NAME;
	}

	public void setOPTION_NAME(String oPTION_NAME) {
		OPTION_NAME = oPTION_NAME;
	}

	public String getOPTION_SIZE() {
		return OPTION_SIZE;
	}

	public void setOPTION_SIZE(String oPTION_SIZE) {
		OPTION_SIZE = oPTION_SIZE;
	}

	public String getOPTION_GRADE() {
		return OPTION_GRADE;
	}

	public void setOPTION_GRADE(String oPTION_GRADE) {
		OPTION_GRADE = oPTION_GRADE;
	}

	public String getSTOCK_AVAILABLE() {
		return STOCK_AVAILABLE;
	}

	public void setSTOCK_AVAILABLE(String sTOCK_AVAILABLE) {
		STOCK_AVAILABLE = sTOCK_AVAILABLE;
	}

	public String getSYNCTIME() {
		return SYNCTIME;
	}

	public void setSYNCTIME(String sYNCTIME) {
		SYNCTIME = sYNCTIME;
	}

	public String getFINAL_MBQ() {
		return FINAL_MBQ;
	}

	public void setFINAL_MBQ(String fINAL_MBQ) {
		FINAL_MBQ = fINAL_MBQ;
	}

	public String getFINAL_REQUIREMENT() {
		return FINAL_REQUIREMENT;
	}

	public void setFINAL_REQUIREMENT(String fINAL_REQUIREMENT) {
		FINAL_REQUIREMENT = fINAL_REQUIREMENT;
	}

	public String getALLOCATED_QTY() {
		return ALLOCATED_QTY;
	}

	public void setALLOCATED_QTY(String aLLOCATED_QTY) {
		ALLOCATED_QTY = aLLOCATED_QTY;
	}

	public EnterpriseSpecificOutput(String uNIQUE_CODE, String wH_CODE, String sTORE_CODE, String sTORE_NAME,
			String sTORE_RANK, String sTORE_ZONE, String sTORE_GRADE, String bARCODE, String oPTION_TYPE,
			String oPTION_NAME, String oPTION_SIZE, String oPTION_GRADE, String sTOCK_AVAILABLE, String sYNCTIME,
			String fINAL_MBQ, String fINAL_REQUIREMENT, String aLLOCATED_QTY) {
		super();
		UNIQUE_CODE = uNIQUE_CODE;
		WH_CODE = wH_CODE;
		STORE_CODE = sTORE_CODE;
		STORE_NAME = sTORE_NAME;
		STORE_RANK = sTORE_RANK;
		STORE_ZONE = sTORE_ZONE;
		STORE_GRADE = sTORE_GRADE;
		BARCODE = bARCODE;
		OPTION_TYPE = oPTION_TYPE;
		OPTION_NAME = oPTION_NAME;
		OPTION_SIZE = oPTION_SIZE;
		OPTION_GRADE = oPTION_GRADE;
		STOCK_AVAILABLE = sTOCK_AVAILABLE;
		SYNCTIME = sYNCTIME;
		FINAL_MBQ = fINAL_MBQ;
		FINAL_REQUIREMENT = fINAL_REQUIREMENT;
		ALLOCATED_QTY = aLLOCATED_QTY;
	}

	@Override
	public String toString() {
		return "EnterpriseSpecificOutput [UNIQUE_CODE=" + UNIQUE_CODE + ", WH_CODE=" + WH_CODE + ", STORE_CODE="
				+ STORE_CODE + ", STORE_NAME=" + STORE_NAME + ", STORE_RANK=" + STORE_RANK + ", STORE_ZONE="
				+ STORE_ZONE + ", STORE_GRADE=" + STORE_GRADE + ", BARCODE=" + BARCODE + ", OPTION_TYPE=" + OPTION_TYPE
				+ ", OPTION_NAME=" + OPTION_NAME + ", OPTION_SIZE=" + OPTION_SIZE + ", OPTION_GRADE=" + OPTION_GRADE
				+ ", STOCK_AVAILABLE=" + STOCK_AVAILABLE + ", SYNCTIME=" + SYNCTIME + ", FINAL_MBQ=" + FINAL_MBQ
				+ ", FINAL_REQUIREMENT=" + FINAL_REQUIREMENT + ", ALLOCATED_QTY=" + ALLOCATED_QTY + "]";
	}

}
