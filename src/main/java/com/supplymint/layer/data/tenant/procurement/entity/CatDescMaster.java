package com.supplymint.layer.data.tenant.procurement.entity;

public class CatDescMaster {
	private String code;
	private String cname;
	private String hl3Code;
	private String hl3Name;
	private String description;

	public CatDescMaster() {

	}

	public CatDescMaster(String code, String cname, String hl3Code, String hl3Name, String description) {
		super();
		this.code = code;
		this.cname = cname;
		this.hl3Code = hl3Code;
		this.hl3Name = hl3Name;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCname() {
		return cname;
	}

	public void setCname(String cname) {
		this.cname = cname;
	}

	public String getHl3Code() {
		return hl3Code;
	}

	public void setHl3Code(String hl3Code) {
		this.hl3Code = hl3Code;
	}

	public String getHl3Name() {
		return hl3Name;
	}

	public void setHl3Name(String hl3Name) {
		this.hl3Name = hl3Name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "CatDescMaster [code=" + code + ", cname=" + cname + ", hl3Code=" + hl3Code + ", hl3Name=" + hl3Name
				+ ", description=" + description + "]";
	}

}
