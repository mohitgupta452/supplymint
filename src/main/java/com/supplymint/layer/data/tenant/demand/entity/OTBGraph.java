package com.supplymint.layer.data.tenant.demand.entity;

public class OTBGraph {

	private String assortmentCode;
	private String billDate;
	private String qtyForecast;
//	@JsonSerialize(using=NumberFormat.class)
	private double planSales;
	private double markDown;
	private double closingInventory;
	private double openingStock;
	private double openPurchaseOrder;
//	@JsonSerialize(using=NumberFormat.class)
	private double otbValue;
	private String billdateFrom;
	private String billdateTo;
	private String quarter;

	public OTBGraph() {

	}

	public OTBGraph(String assortmentCode, String billDate, String qtyForecast, double planSales, double markDown,
			double closingInventory, double openingStock, double openPurchaseOrder, double otbValue,
			String billdateFrom, String billdateTo, String quarter) {
		super();
		this.assortmentCode = assortmentCode;
		this.billDate = billDate;
		this.qtyForecast = qtyForecast;
		this.planSales = planSales;
		this.markDown = markDown;
		this.closingInventory = closingInventory;
		this.openingStock = openingStock;
		this.openPurchaseOrder = openPurchaseOrder;
		this.otbValue = otbValue;
		this.billdateFrom = billdateFrom;
		this.billdateTo = billdateTo;
		this.quarter = quarter;
	}

	public String getAssortmentCode() {
		return assortmentCode;
	}

	public void setAssortmentCode(String assortmentCode) {
		this.assortmentCode = assortmentCode;
	}

	public String getBillDate() {
		return billDate;
	}

	public void setBillDate(String billDate) {
		this.billDate = billDate;
	}

	public String getQtyForecast() {
		return qtyForecast;
	}

	public void setQtyForecast(String qtyForecast) {
		this.qtyForecast = qtyForecast;
	}

	public double getPlanSales() {
		return planSales;
	}

	public void setPlanSales(double planSales) {
		this.planSales = planSales;
	}

	public double getMarkDown() {
		return markDown;
	}

	public void setMarkDown(double markDown) {
		this.markDown = markDown;
	}

	public double getClosingInventory() {
		return closingInventory;
	}

	public void setClosingInventory(double closingInventory) {
		this.closingInventory = closingInventory;
	}

	public double getOpeningStock() {
		return openingStock;
	}

	public void setOpeningStock(double openingStock) {
		this.openingStock = openingStock;
	}

	public double getOpenPurchaseOrder() {
		return openPurchaseOrder;
	}

	public void setOpenPurchaseOrder(double openPurchaseOrder) {
		this.openPurchaseOrder = openPurchaseOrder;
	}

	public double getOtbValue() {
		return otbValue;
	}

	public void setOtbValue(double otbValue) {
		this.otbValue = otbValue;
	}

	public String getBilldateFrom() {
		return billdateFrom;
	}

	public void setBilldateFrom(String billdateFrom) {
		this.billdateFrom = billdateFrom;
	}

	public String getBilldateTo() {
		return billdateTo;
	}

	public void setBilldateTo(String billdateTo) {
		this.billdateTo = billdateTo;
	}

	public String getQuarter() {
		return quarter;
	}

	public void setQuarter(String quarter) {
		this.quarter = quarter;
	}

	@Override
	public String toString() {
		return "OTBGraph [assortmentCode=" + assortmentCode + ", billDate=" + billDate + ", qtyForecast=" + qtyForecast
				+ ", planSales=" + planSales + ", markDown=" + markDown + ", closingInventory=" + closingInventory
				+ ", openingStock=" + openingStock + ", openPurchaseOrder=" + openPurchaseOrder + ", otbValue="
				+ otbValue + ", billdateFrom=" + billdateFrom + ", billdateTo=" + billdateTo + ", quarter=" + quarter
				+ "]";
	}

}
