package com.supplymint.layer.data.tenant.dashboard.entity;

import java.time.OffsetDateTime;

public class ActualSalesData {

	public String storeCode;
	public String itemCode;
	public String itemName;
	public OffsetDateTime billDate;
	public String year;
	public String month;
	public int qty;
	public int stockInHand;
	public String costPrice;
	public String sellPrice;
	public String syncType;
	public OffsetDateTime syncTime;
	public String status;
	public String ipAddress;
	public OffsetDateTime updatedOn;

	public ActualSalesData() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ActualSalesData(String storeCode, String itemCode, String itemName, OffsetDateTime billDate, String year,
			String month, int qty, int stockInHand, String costPrice, String sellPrice, String syncType,
			OffsetDateTime syncTime, String status, String ipAddress, OffsetDateTime updatedOn) {
		super();
		this.storeCode = storeCode;
		this.itemCode = itemCode;
		this.itemName = itemName;
		this.billDate = billDate;
		this.year = year;
		this.month = month;
		this.qty = qty;
		this.stockInHand = stockInHand;
		this.costPrice = costPrice;
		this.sellPrice = sellPrice;
		this.syncType = syncType;
		this.syncTime = syncTime;
		this.status = status;
		this.ipAddress = ipAddress;
		this.updatedOn = updatedOn;
	}

	public String getStoreCode() {
		return storeCode;
	}

	public void setStoreCode(String storeCode) {
		this.storeCode = storeCode;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public OffsetDateTime getBillDate() {
		return billDate;
	}

	public void setBillDate(OffsetDateTime billDate) {
		this.billDate = billDate;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

	public int getStockInHand() {
		return stockInHand;
	}

	public void setStockInHand(int stockInHand) {
		this.stockInHand = stockInHand;
	}

	public String getCostPrice() {
		return costPrice;
	}

	public void setCostPrice(String costPrice) {
		this.costPrice = costPrice;
	}

	public String getSellPrice() {
		return sellPrice;
	}

	public void setSellPrice(String sellPrice) {
		this.sellPrice = sellPrice;
	}

	public String getSyncType() {
		return syncType;
	}

	public void setSyncType(String syncType) {
		this.syncType = syncType;
	}

	public OffsetDateTime getSyncTime() {
		return syncTime;
	}

	public void setSyncTime(OffsetDateTime syncTime) {
		this.syncTime = syncTime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public OffsetDateTime getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(OffsetDateTime updatedOn) {
		this.updatedOn = updatedOn;
	}

	@Override
	public String toString() {
		return "ActualSalesData [storeCode=" + storeCode + ", itemCode=" + itemCode + ", itemName=" + itemName
				+ ", billDate=" + billDate + ", year=" + year + ", month=" + month + ", qty=" + qty + ", stockInHand="
				+ stockInHand + ", costPrice=" + costPrice + ", sellPrice=" + sellPrice + ", syncType=" + syncType
				+ ", syncTime=" + syncTime + ", status=" + status + ", ipAddress=" + ipAddress + ", updatedOn="
				+ updatedOn + "]";
	}

}
