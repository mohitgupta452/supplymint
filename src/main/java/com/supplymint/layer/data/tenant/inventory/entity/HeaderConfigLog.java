package com.supplymint.layer.data.tenant.inventory.entity;

import java.util.Date;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.supplymint.util.DateFormatSerializer;

public class HeaderConfigLog {

	private String id;
	private String typeConfig;
	private String customHeaders;
	private String config;
	private String displayName;
	private String attributeType;
	private String active;
	private String status;
	private String ipAddress;
	private String isDefault;
	private String createdBy;
	@JsonSerialize(using = DateFormatSerializer.class)
	private Date createdTime;
	private String updatedBy;
	@JsonSerialize(using = DateFormatSerializer.class)
	private Date updationTime;
	private String additional;
	private JsonNode customHeader;

	public HeaderConfigLog() {
	}

	public HeaderConfigLog(String id, String typeConfig, String customHeaders, String config, String displayName,
			String attributeType, String active, String status, String ipAddress, String isDefault, String createdBy,
			Date createdTime, String updatedBy, Date updationTime, String additional, JsonNode customHeader) {
		super();
		this.id = id;
		this.typeConfig = typeConfig;
		this.customHeaders = customHeaders;
		this.config = config;
		this.displayName = displayName;
		this.attributeType = attributeType;
		this.active = active;
		this.status = status;
		this.ipAddress = ipAddress;
		this.isDefault = isDefault;
		this.createdBy = createdBy;
		this.createdTime = createdTime;
		this.updatedBy = updatedBy;
		this.updationTime = updationTime;
		this.additional = additional;
		this.customHeader = customHeader;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTypeConfig() {
		return typeConfig;
	}

	public void setTypeConfig(String typeConfig) {
		this.typeConfig = typeConfig;
	}

	public String getCustomHeaders() {
		return customHeaders;
	}

	public void setCustomHeaders(String customHeaders) {
		this.customHeaders = customHeaders;
	}

	public String getConfig() {
		return config;
	}

	public void setConfig(String config) {
		this.config = config;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getAttributeType() {
		return attributeType;
	}

	public void setAttributeType(String attributeType) {
		this.attributeType = attributeType;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(String isDefault) {
		this.isDefault = isDefault;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdationTime() {
		return updationTime;
	}

	public void setUpdationTime(Date updationTime) {
		this.updationTime = updationTime;
	}

	public String getAdditional() {
		return additional;
	}

	public void setAdditional(String additional) {
		this.additional = additional;
	}

	public JsonNode getCustomHeader() {
		return customHeader;
	}

	public void setCustomHeader(JsonNode customHeader) {
		this.customHeader = customHeader;
	}

	@Override
	public String toString() {
		return "HeaderConfigLog [id=" + id + ", typeConfig=" + typeConfig + ", customHeaders=" + customHeaders
				+ ", config=" + config + ", displayName=" + displayName + ", attributeType=" + attributeType
				+ ", active=" + active + ", status=" + status + ", ipAddress=" + ipAddress + ", isDefault=" + isDefault
				+ ", createdBy=" + createdBy + ", createdTime=" + createdTime + ", updatedBy=" + updatedBy
				+ ", updationTime=" + updationTime + ", additional=" + additional + ", customHeader=" + customHeader
				+ "]";
	}

}
