package com.supplymint.layer.data.tenant.procurement.entity;

public class DeptItemUDFSettings {
	private String id;
	private String hl3Name;
	private String hl3code;
	private String hl4Code;
	private String cat_desc_udf;
	private String cat_desc_udf_type;
	private String hl3Code;
	private String displayName;
	private String isCompulsory;
	private String orderBy;
	private String isLov;
	private String createdAt;
	private String updatedAt;

	public DeptItemUDFSettings() {
		// TODO Auto-generated constructor stub
	}

	public DeptItemUDFSettings(String id, String hl3Name, String hl3code, String hl4Code, String cat_desc_udf,
			String cat_desc_udf_type, String hl3Code2, String displayName, String isCompulsory, String orderBy,
			String isLov, String createdAt, String updatedAt) {
		super();
		this.id = id;
		this.hl3Name = hl3Name;
		this.hl3code = hl3code;
		this.hl4Code = hl4Code;
		this.cat_desc_udf = cat_desc_udf;
		this.cat_desc_udf_type = cat_desc_udf_type;
		hl3Code = hl3Code2;
		this.displayName = displayName;
		this.isCompulsory = isCompulsory;
		this.orderBy = orderBy;
		this.isLov = isLov;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getHl3Name() {
		return hl3Name;
	}

	public void setHl3Name(String hl3Name) {
		this.hl3Name = hl3Name;
	}

	public String getHl3code() {
		return hl3code;
	}

	public void setHl3code(String hl3code) {
		this.hl3code = hl3code;
	}

	public String getHl4Code() {
		return hl4Code;
	}

	public void setHl4Code(String hl4Code) {
		this.hl4Code = hl4Code;
	}

	public String getCat_desc_udf() {
		return cat_desc_udf;
	}

	public void setCat_desc_udf(String cat_desc_udf) {
		this.cat_desc_udf = cat_desc_udf;
	}

	public String getCat_desc_udf_type() {
		return cat_desc_udf_type;
	}

	public void setCat_desc_udf_type(String cat_desc_udf_type) {
		this.cat_desc_udf_type = cat_desc_udf_type;
	}

	public String getHl3Code() {
		return hl3Code;
	}

	public void setHl3Code(String hl3Code) {
		this.hl3Code = hl3Code;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getIsCompulsory() {
		return isCompulsory;
	}

	public void setIsCompulsory(String isCompulsory) {
		this.isCompulsory = isCompulsory;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public String getIsLov() {
		return isLov;
	}

	public void setIsLov(String isLov) {
		this.isLov = isLov;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	@Override
	public String toString() {
		return "DeptItemUDFSettings [id=" + id + ", hl3Name=" + hl3Name + ", hl3code=" + hl3code + ", hl4Code="
				+ hl4Code + ", cat_desc_udf=" + cat_desc_udf + ", cat_desc_udf_type=" + cat_desc_udf_type + ", hl3Code="
				+ hl3Code + ", displayName=" + displayName + ", isCompulsory=" + isCompulsory + ", orderBy=" + orderBy
				+ ", isLov=" + isLov + ", createdAt=" + createdAt + ", updatedAt=" + updatedAt + "]";
	}

	

}
