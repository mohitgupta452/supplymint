package com.supplymint.layer.data.tenant.dashboard.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.mapping.StatementType;

import com.supplymint.layer.data.tenant.dashboard.entity.ArticleMoving;
import com.supplymint.layer.data.tenant.dashboard.entity.DashboardWindow;
import com.supplymint.layer.data.tenant.dashboard.entity.SalesTrend;
import com.supplymint.layer.data.tenant.dashboard.entity.StoreArticleRank;
import com.supplymint.layer.data.tenant.dashboard.entity.StoreRank;

@Mapper
public interface DashboardMapper {

	DashboardWindow getDashboardWindow();

	List<StoreArticleRank> getStoresArticleRank();

	List<SalesTrend> getCurrentMonthSalesTrend(@Param("type") String type);

	List<SalesTrend> getLastMonthSalesTrend(@Param("type") String type);

	List<SalesTrend> getLastThreeMonthSalesTrend(@Param("type") String type);

	List<SalesTrend> getSixMonthSalesTrend(@Param("type") String type);

	List<SalesTrend> getLastTwelfthMonthSalesTrend(@Param("type") String type);

	List<SalesTrend> getLastFinancialYearSalesTrend(@Param("type") String type);

	List<SalesTrend> getCurrentMonthUnitsTrend(@Param("type") String type);

	List<SalesTrend> getLastMonthUnitsTrend(@Param("type") String type);

	List<SalesTrend> getLastThreeMonthUnitsTrend(@Param("type") String type);

	List<SalesTrend> getSixMonthUnitsTrend(@Param("type") String type);

	List<SalesTrend> getLastTwelfthMonthUnitsTrend(@Param("type") String type);

	List<SalesTrend> getLastFinancialYearUnitsTrend(@Param("type") String type);

	List<StoreRank> getStoresRank();

	List<StoreRank> getArticlesRank();

	int createStoreRank(@Param("code") String code,@Param("name") String name, @Param("type") String type, @Param("rank") String rank);

	int createArticleRank(@Param("code") String code,@Param("name") String name, @Param("type") String type, @Param("rank") String rank);

	int createArticleMoving(@Param("code") String code, @Param("rank") String rank);

	int deleteStoreRanks();

	int deleteArticleRank();

	List<ArticleMoving> getArticlesMoving();
	
	@Select(value= "CALL DBMS_MVIEW.REFRESH('STOREARTICLE_RANK_MVIEW,ACTUAL_SALES_MVIEW,SALES_TRENDGRAPH_MVIEW,SALESTRENDDATA_MVIEW,DASHBOARD_WINDOW,DEMAND_ASSORTMENT_DAY_WISE','?')")
	@Options(statementType = StatementType.CALLABLE,fetchSize=1000)
	void resfreshDashBoardMv();

}
