package com.supplymint.layer.data.tenant.analytic.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class SellThrough {

	@JsonIgnore
	private String siteCode;
	private String sellThroughType;
	private String sellThroughValue;

	public SellThrough() {

	}

	public SellThrough(String siteCode, String sellThroughType, String sellThroughValue) {
		super();
		this.siteCode = siteCode;
		this.sellThroughType = sellThroughType;
		this.sellThroughValue = sellThroughValue;
	}

	public String getSiteCode() {
		return siteCode;
	}

	public void setSiteCode(String siteCode) {
		this.siteCode = siteCode;
	}

	public String getSellThroughType() {
		return sellThroughType;
	}

	public void setSellThroughType(String sellThroughType) {
		this.sellThroughType = sellThroughType;
	}

	public String getSellThroughValue() {
		return sellThroughValue;
	}

	public void setSellThroughValue(String sellThroughValue) {
		this.sellThroughValue = sellThroughValue;
	}

	@Override
	public String toString() {
		return "SellThrough [siteCode=" + siteCode + ", sellThroughType=" + sellThroughType + ", sellThroughValue="
				+ sellThroughValue + "]";
	}

}
