package com.supplymint.layer.data.tenant.administration.entity;

import java.time.OffsetDateTime;
import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.supplymint.util.DateFormatSerializer;
import com.supplymint.util.LocalDateTimeSerializer;

public class ADMSiteMap {

	private int id;
	private int fromSiteId;
	private String fromSite;
	private int toSiteId;
	private String toSite;
	@JsonSerialize(using = DateFormatSerializer.class)
	private Date startDate;
	@JsonSerialize(using = DateFormatSerializer.class)
	private Date endDate;
	private String transportationMode;
	private int tptLeadTime;
	private char active;
	private String status;
	private String ipAddress;
	private String createdBy;
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	private OffsetDateTime createdTime;
	private String updatedBy;
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	private OffsetDateTime updationTime;
	private String additional;

	public ADMSiteMap() {

	}

	public ADMSiteMap(int id, int fromSiteId, String fromSite, int toSiteId, String toSite, Date startDate,
			Date endDate, String transportationMode, int tptLeadTime, char active, String status, String ipAddress,
			String createdBy, OffsetDateTime createdTime, String updatedBy, OffsetDateTime updationTime,
			String additional) {
		super();
		this.id = id;
		this.fromSiteId = fromSiteId;
		this.fromSite = fromSite;
		this.toSiteId = toSiteId;
		this.toSite = toSite;
		this.startDate = startDate;
		this.endDate = endDate;
		this.transportationMode = transportationMode;
		this.tptLeadTime = tptLeadTime;
		this.active = active;
		this.status = status;
		this.ipAddress = ipAddress;
		this.createdBy = createdBy;
		this.createdTime = createdTime;
		this.updatedBy = updatedBy;
		this.updationTime = updationTime;
		this.additional = additional;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getFromSiteId() {
		return fromSiteId;
	}

	public void setFromSiteId(int fromSiteId) {
		this.fromSiteId = fromSiteId;
	}

	public String getFromSite() {
		return fromSite;
	}

	public void setFromSite(String fromSite) {
		this.fromSite = fromSite;
	}

	public int getToSiteId() {
		return toSiteId;
	}

	public void setToSiteId(int toSiteId) {
		this.toSiteId = toSiteId;
	}

	public String getToSite() {
		return toSite;
	}

	public void setToSite(String toSite) {
		this.toSite = toSite;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getTransportationMode() {
		return transportationMode;
	}

	public void setTransportationMode(String transportationMode) {
		this.transportationMode = transportationMode;
	}

	public int getTptLeadTime() {
		return tptLeadTime;
	}

	public void setTptLeadTime(int tptLeadTime) {
		this.tptLeadTime = tptLeadTime;
	}

	public char getActive() {
		return active;
	}

	public void setActive(char active) {
		this.active = active;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public OffsetDateTime getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(OffsetDateTime createdTime) {
		this.createdTime = createdTime;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public OffsetDateTime getUpdationTime() {
		return updationTime;
	}

	public void setUpdationTime(OffsetDateTime updationTime) {
		this.updationTime = updationTime;
	}

	public String getAdditional() {
		return additional;
	}

	public void setAdditional(String additional) {
		this.additional = additional;
	}

	@Override
	public String toString() {
		return "ADMSiteMap [id=" + id + ", fromSiteId=" + fromSiteId + ", fromSite=" + fromSite + ", toSiteId="
				+ toSiteId + ", toSite=" + toSite + ", startDate=" + startDate + ", endDate=" + endDate
				+ ", transportationMode=" + transportationMode + ", tptLeadTime=" + tptLeadTime + ", active=" + active
				+ ", status=" + status + ", ipAddress=" + ipAddress + ", createdBy=" + createdBy + ", createdTime="
				+ createdTime + ", updatedBy=" + updatedBy + ", updationTime=" + updationTime + ", additional="
				+ additional + "]";
	}
}
