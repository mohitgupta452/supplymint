package com.supplymint.layer.data.tenant.procurement.entity;

public class ItemBarcode {

	private int sizeId;
	private int colorId;
	private String itemId;

	public ItemBarcode() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ItemBarcode(int sizeId, int colorId, String itemId) {
		super();
		this.sizeId = sizeId;
		this.colorId = colorId;
		this.itemId = itemId;
	}

	public int getSizeId() {
		return sizeId;
	}

	public void setSizeId(int sizeId) {
		this.sizeId = sizeId;
	}

	public int getColorId() {
		return colorId;
	}

	public void setColorId(int colorId) {
		this.colorId = colorId;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	@Override
	public String toString() {
		return "ItemBarcode [sizeId=" + sizeId + ", colorId=" + colorId + ", itemId=" + itemId + "]";
	}

}
