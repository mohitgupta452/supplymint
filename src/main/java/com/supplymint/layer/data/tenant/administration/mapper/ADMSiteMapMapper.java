package com.supplymint.layer.data.tenant.administration.mapper;

import java.time.OffsetDateTime;
import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.supplymint.layer.data.tenant.administration.entity.ADMSiteMap;

@Mapper
public interface ADMSiteMapMapper {

	/**
	 * 
	 * @return POJO data as per @param
	 */
	ADMSiteMap findById(Integer id);

	/**
	 * 
	 * @return List of data as per  @param
	 */
	List<ADMSiteMap> findAll(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize);

	/**
	 * 
	 * @return count of creating the data as per @param
	 */
	Integer create(ADMSiteMap site);

	/**
	 * 
	 * @return count of updating the data as per @param 
	 */
	Integer update(ADMSiteMap site);

	/**
	 *  
	 * @return delete the data as per @param
	 */
	@Delete("DELETE FROM ADM_SITEMAP WHERE ID = #{id}")
	Integer delete(Integer id);

	/**
	 * 
	 * @return get ADMSiteMap details as per @param
	 */
	List<ADMSiteMap> getStatus(String status);

	/**
	 * 
	 * @return count of updating the data as per @param
	 */
	Integer updateStatus(@Param("status") String status, @Param("sid") Integer sid,
			@Param("ipAddress") String ipAddress, @Param("updationTime") OffsetDateTime updationTime);

	/**
	 * 
	 * @return list of ADMSiteMap detail as per @param
	 */
	List<ADMSiteMap> getDetailsOnFromIdtoToId(@Param("fromid") Integer fromid, @Param("toid") Integer toid);

	/**
	 * 
	 * @return list of ADMSiteMap detail as per @param
	 */
	List<ADMSiteMap> getDetailsOnFromSiteId(@Param("fromsiteid") Integer fromsiteid);

	/**
	 * 
	 * @return count of all present record.
	 */
	@Select("SELECT COUNT(*) FROM ADM_SITEMAP")
	Integer record();

	/**
	 * 
	 * @return count of search record in data.
	 */
	int searchRecord(@Param("search") String search);

	/**
	 * 
	 * @return List of searched record as per @param.
	 */
	List<ADMSiteMap> searchAll(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("search") String search);

	/**
	 * 
	 * @return List of filter record as per @param.
	 */
	List<ADMSiteMap> filter(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize,
			@Param("fromSite") String fromSite, @Param("toSite") String toSite, @Param("startDate") String startDate,
			@Param("tptLeadTime") String tptLeadTime, @Param("status") String status);

	/**
	 *
	 * @return count of filtered record as per @param.
	 */
	Integer filterRecord(@Param("fromSite") String fromSite, @Param("toSite") String toSite,
			@Param("startDate") String startDate, @Param("tptLeadTime") String tptLeadTime,
			@Param("status") String status);

	/**
	 * 
	 * @return List of ADM site map records.
	 */
	List<ADMSiteMap> getAllRecord();

}
