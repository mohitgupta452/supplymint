package com.supplymint.layer.data.tenant.procurement.entity;

public class FinChargeRateDetail {
	private String taxName;
	private String gstSlab;
	private String effectiveDate;
	private String igstRate;
	private String cessRate;

	public FinChargeRateDetail() {
		super();
		// TODO Auto-generated constructor stub
	}

	public FinChargeRateDetail(String taxName, String gstSlab, String effectiveDate, String igstRate, String cessRate) {
		super();
		this.taxName = taxName;
		this.gstSlab = gstSlab;
		this.effectiveDate = effectiveDate;
		this.igstRate = igstRate;
		this.cessRate = cessRate;
	}

	public String getTaxName() {
		return taxName;
	}

	public void setTaxName(String taxName) {
		this.taxName = taxName;
	}

	public String getGstSlab() {
		return gstSlab;
	}

	public void setGstSlab(String gstSlab) {
		this.gstSlab = gstSlab;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getIgstRate() {
		return igstRate;
	}

	public void setIgstRate(String igstRate) {
		this.igstRate = igstRate;
	}

	public String getCessRate() {
		return cessRate;
	}

	public void setCessRate(String cessRate) {
		this.cessRate = cessRate;
	}

	@Override
	public String toString() {
		return "FinChargeRateDetail [taxName=" + taxName + ", gstSlab=" + gstSlab + ", effectiveDate=" + effectiveDate
				+ ", igstRate=" + igstRate + ", cessRate=" + cessRate + "]";
	}

}
