package com.supplymint.layer.data.tenant.inventory.entity;

import java.util.Date;
import java.util.Map;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.supplymint.util.DateFormatSerializer;

public class JobTrigger {
	private int id;
	private int enterpriseId;
	private String enterpriseName;
	private String orgId;
	private String jobName;
	private String triggerName;
	private String timeZone;
	private String schedule;
	private String frequency;
	private String dayMonth;
	private String dayWeek;
	private String type;
	private String nextSchedule;
	private String userName;
	private String ipAddress;
	private String createdBy;
	private String updatedBy;
	private Map<String, String> arguments;
	@JsonSerialize(using = DateFormatSerializer.class)
	private Date createdOn;
	@JsonSerialize(using = DateFormatSerializer.class)
	private Date updatedOn;
	private String additional;

	public JobTrigger() {

	}

	public JobTrigger(int id, int enterpriseId, String enterpriseName, String orgId, String jobName, String triggerName,
			String timeZone, String schedule, String frequency, String dayMonth, String dayWeek, String type,
			String nextSchedule, String userName, String ipAddress, String createdBy, String updatedBy,
			Map<String, String> arguments, Date createdOn, Date updatedOn, String additional) {
		super();
		this.id = id;
		this.enterpriseId = enterpriseId;
		this.enterpriseName = enterpriseName;
		this.orgId = orgId;
		this.jobName = jobName;
		this.triggerName = triggerName;
		this.timeZone = timeZone;
		this.schedule = schedule;
		this.frequency = frequency;
		this.dayMonth = dayMonth;
		this.dayWeek = dayWeek;
		this.type = type;
		this.nextSchedule = nextSchedule;
		this.userName = userName;
		this.ipAddress = ipAddress;
		this.createdBy = createdBy;
		this.updatedBy = updatedBy;
		this.arguments = arguments;
		this.createdOn = createdOn;
		this.updatedOn = updatedOn;
		this.additional = additional;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getEnterpriseId() {
		return enterpriseId;
	}

	public void setEnterpriseId(int enterpriseId) {
		this.enterpriseId = enterpriseId;
	}

	public String getEnterpriseName() {
		return enterpriseName;
	}

	public void setEnterpriseName(String enterpriseName) {
		this.enterpriseName = enterpriseName;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getTriggerName() {
		return triggerName;
	}

	public void setTriggerName(String triggerName) {
		this.triggerName = triggerName;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public String getSchedule() {
		return schedule;
	}

	public void setSchedule(String schedule) {
		this.schedule = schedule;
	}

	public String getFrequency() {
		return frequency;
	}

	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}

	public String getDayMonth() {
		return dayMonth;
	}

	public void setDayMonth(String dayMonth) {
		this.dayMonth = dayMonth;
	}

	public String getDayWeek() {
		return dayWeek;
	}

	public void setDayWeek(String dayWeek) {
		this.dayWeek = dayWeek;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getNextSchedule() {
		return nextSchedule;
	}

	public void setNextSchedule(String nextSchedule) {
		this.nextSchedule = nextSchedule;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Map<String, String> getArguments() {
		return arguments;
	}

	public void setArguments(Map<String, String> arguments) {
		this.arguments = arguments;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getAdditional() {
		return additional;
	}

	public void setAdditional(String additional) {
		this.additional = additional;
	}

	@Override
	public String toString() {
		return "JobTrigger [id=" + id + ", enterpriseId=" + enterpriseId + ", enterpriseName=" + enterpriseName
				+ ", orgId=" + orgId + ", jobName=" + jobName + ", triggerName=" + triggerName + ", timeZone="
				+ timeZone + ", schedule=" + schedule + ", frequency=" + frequency + ", dayMonth=" + dayMonth
				+ ", dayWeek=" + dayWeek + ", type=" + type + ", nextSchedule=" + nextSchedule + ", userName="
				+ userName + ", ipAddress=" + ipAddress + ", createdBy=" + createdBy + ", updatedBy=" + updatedBy
				+ ", arguments=" + arguments + ", createdOn=" + createdOn + ", updatedOn=" + updatedOn + ", additional="
				+ additional + "]";
	}

}
