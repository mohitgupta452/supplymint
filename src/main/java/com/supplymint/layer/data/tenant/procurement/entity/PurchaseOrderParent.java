package com.supplymint.layer.data.tenant.procurement.entity;

import java.time.OffsetDateTime;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.supplymint.util.LocalDatetimeDeserializer;
import com.supplymint.util.PILocalDatetimeSerializer;

public class PurchaseOrderParent {

	private int id;
	private String orgId;
	private char intGCode;
	private String intGHeaderId;
	private String poReferenceId;
	private String intGLineId;
	private String orderNo;
	private String prevOrderNo;
	private String setHeaderId;
	private String key;

	private String isHoldPO;
	private String isUDFExist;
	private String isSiteExist;

	private String siteCode;
	private String siteName;

	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	private OffsetDateTime validFrom;

	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	private OffsetDateTime validTo;

	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	private OffsetDateTime orderDate;

	private String hl1Code;
	private String hl2Code;
	private String hl3Code;

	private String hl1Name;
	private String hl2Name;
	private String hl3Name;
	private String hl4Code;
	private String hl4Name;
	private String hsnSacCode;
	private String hsnCode;
	private String slCode;
	private String slName;
	private String slCityName;
	private String slAddr;
	private double leadTime;
	private String termCode;
	private String termName;
	private String transporterCode;
	private String transporterName;
	private String stateCode;
	private String mrpStart;
	private String mrpEnd;

	private double poQuantity;
	private double poAmount;

	private List<PurchaseOrderChild> pol;

	private String active;
	private String status;
	private String ipAddress;
	private String createdBy;
	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	private OffsetDateTime createdTime;
	private String updatedBy;

	@JsonSerialize(using = PILocalDatetimeSerializer.class)
	private OffsetDateTime updationTime;
	private String additional;
	private String poType;
	private String availableKeys;

	public PurchaseOrderParent() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PurchaseOrderParent(int id, String orgId, char intGCode, String intGHeaderId, String poReferenceId,
			String intGLineId, String orderNo, String prevOrderNo, String setHeaderId, String key, String isHoldPO,
			String isUDFExist, String isSiteExist, String siteCode, String siteName, OffsetDateTime validFrom,
			OffsetDateTime validTo, OffsetDateTime orderDate, String hl1Code, String hl2Code, String hl3Code,
			String hl1Name, String hl2Name, String hl3Name, String hl4Code, String hl4Name, String hsnSacCode,
			String hsnCode, String slCode, String slName, String slCityName, String slAddr, double leadTime,
			String termCode, String termName, String transporterCode, String transporterName, String stateCode,
			String mrpStart, String mrpEnd, double poQuantity, double poAmount, List<PurchaseOrderChild> pol,
			String active, String status, String ipAddress, String createdBy, OffsetDateTime createdTime,
			String updatedBy, OffsetDateTime updationTime, String additional, String poType, String availableKeys) {
		super();
		this.id = id;
		this.orgId = orgId;
		this.intGCode = intGCode;
		this.intGHeaderId = intGHeaderId;
		this.poReferenceId = poReferenceId;
		this.intGLineId = intGLineId;
		this.orderNo = orderNo;
		this.prevOrderNo = prevOrderNo;
		this.setHeaderId = setHeaderId;
		this.key = key;
		this.isHoldPO = isHoldPO;
		this.isUDFExist = isUDFExist;
		this.isSiteExist = isSiteExist;
		this.siteCode = siteCode;
		this.siteName = siteName;
		this.validFrom = validFrom;
		this.validTo = validTo;
		this.orderDate = orderDate;
		this.hl1Code = hl1Code;
		this.hl2Code = hl2Code;
		this.hl3Code = hl3Code;
		this.hl1Name = hl1Name;
		this.hl2Name = hl2Name;
		this.hl3Name = hl3Name;
		this.hl4Code = hl4Code;
		this.hl4Name = hl4Name;
		this.hsnSacCode = hsnSacCode;
		this.hsnCode = hsnCode;
		this.slCode = slCode;
		this.slName = slName;
		this.slCityName = slCityName;
		this.slAddr = slAddr;
		this.leadTime = leadTime;
		this.termCode = termCode;
		this.termName = termName;
		this.transporterCode = transporterCode;
		this.transporterName = transporterName;
		this.stateCode = stateCode;
		this.mrpStart = mrpStart;
		this.mrpEnd = mrpEnd;
		this.poQuantity = poQuantity;
		this.poAmount = poAmount;
		this.pol = pol;
		this.active = active;
		this.status = status;
		this.ipAddress = ipAddress;
		this.createdBy = createdBy;
		this.createdTime = createdTime;
		this.updatedBy = updatedBy;
		this.updationTime = updationTime;
		this.additional = additional;
		this.poType = poType;
		this.availableKeys = availableKeys;
	}

	public String getAvailableKeys() {
		return availableKeys;
	}

	public void setAvailableKeys(String availableKeys) {
		this.availableKeys = availableKeys;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public char getIntGCode() {
		return intGCode;
	}

	public void setIntGCode(char intGCode) {
		this.intGCode = intGCode;
	}

	public String getIntGHeaderId() {
		return intGHeaderId;
	}

	public void setIntGHeaderId(String intGHeaderId) {
		this.intGHeaderId = intGHeaderId;
	}

	public String getPoReferenceId() {
		return poReferenceId;
	}

	public void setPoReferenceId(String poReferenceId) {
		this.poReferenceId = poReferenceId;
	}

	public String getIntGLineId() {
		return intGLineId;
	}

	public void setIntGLineId(String intGLineId) {
		this.intGLineId = intGLineId;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getPrevOrderNo() {
		return prevOrderNo;
	}

	public void setPrevOrderNo(String prevOrderNo) {
		this.prevOrderNo = prevOrderNo;
	}

	public String getSetHeaderId() {
		return setHeaderId;
	}

	public void setSetHeaderId(String setHeaderId) {
		this.setHeaderId = setHeaderId;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getIsHoldPO() {
		return isHoldPO;
	}

	public void setIsHoldPO(String isHoldPO) {
		this.isHoldPO = isHoldPO;
	}

	public String getIsUDFExist() {
		return isUDFExist;
	}

	public void setIsUDFExist(String isUDFExist) {
		this.isUDFExist = isUDFExist;
	}

	public String getIsSiteExist() {
		return isSiteExist;
	}

	public void setIsSiteExist(String isSiteExist) {
		this.isSiteExist = isSiteExist;
	}

	public String getSiteCode() {
		return siteCode;
	}

	public void setSiteCode(String siteCode) {
		this.siteCode = siteCode;
	}

	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	public OffsetDateTime getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(OffsetDateTime validFrom) {
		this.validFrom = validFrom;
	}

	public OffsetDateTime getValidTo() {
		return validTo;
	}

	public void setValidTo(OffsetDateTime validTo) {
		this.validTo = validTo;
	}

	public OffsetDateTime getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(OffsetDateTime orderDate) {
		this.orderDate = orderDate;
	}

	public String getHl1Code() {
		return hl1Code;
	}

	public void setHl1Code(String hl1Code) {
		this.hl1Code = hl1Code;
	}

	public String getHl2Code() {
		return hl2Code;
	}

	public void setHl2Code(String hl2Code) {
		this.hl2Code = hl2Code;
	}

	public String getHl3Code() {
		return hl3Code;
	}

	public void setHl3Code(String hl3Code) {
		this.hl3Code = hl3Code;
	}

	public String getHl1Name() {
		return hl1Name;
	}

	public void setHl1Name(String hl1Name) {
		this.hl1Name = hl1Name;
	}

	public String getHl2Name() {
		return hl2Name;
	}

	public void setHl2Name(String hl2Name) {
		this.hl2Name = hl2Name;
	}

	public String getHl3Name() {
		return hl3Name;
	}

	public void setHl3Name(String hl3Name) {
		this.hl3Name = hl3Name;
	}

	public String getHl4Code() {
		return hl4Code;
	}

	public void setHl4Code(String hl4Code) {
		this.hl4Code = hl4Code;
	}

	public String getHl4Name() {
		return hl4Name;
	}

	public void setHl4Name(String hl4Name) {
		this.hl4Name = hl4Name;
	}

	public String getHsnSacCode() {
		return hsnSacCode;
	}

	public void setHsnSacCode(String hsnSacCode) {
		this.hsnSacCode = hsnSacCode;
	}

	public String getHsnCode() {
		return hsnCode;
	}

	public void setHsnCode(String hsnCode) {
		this.hsnCode = hsnCode;
	}

	public String getSlCode() {
		return slCode;
	}

	public void setSlCode(String slCode) {
		this.slCode = slCode;
	}

	public String getSlName() {
		return slName;
	}

	public void setSlName(String slName) {
		this.slName = slName;
	}

	public String getSlCityName() {
		return slCityName;
	}

	public void setSlCityName(String slCityName) {
		this.slCityName = slCityName;
	}

	public String getSlAddr() {
		return slAddr;
	}

	public void setSlAddr(String slAddr) {
		this.slAddr = slAddr;
	}

	public double getLeadTime() {
		return leadTime;
	}

	public void setLeadTime(double leadTime) {
		this.leadTime = leadTime;
	}

	public String getTermCode() {
		return termCode;
	}

	public void setTermCode(String termCode) {
		this.termCode = termCode;
	}

	public String getTermName() {
		return termName;
	}

	public void setTermName(String termName) {
		this.termName = termName;
	}

	public String getTransporterCode() {
		return transporterCode;
	}

	public void setTransporterCode(String transporterCode) {
		this.transporterCode = transporterCode;
	}

	public String getTransporterName() {
		return transporterName;
	}

	public void setTransporterName(String transporterName) {
		this.transporterName = transporterName;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public String getMrpStart() {
		return mrpStart;
	}

	public void setMrpStart(String mrpStart) {
		this.mrpStart = mrpStart;
	}

	public String getMrpEnd() {
		return mrpEnd;
	}

	public void setMrpEnd(String mrpEnd) {
		this.mrpEnd = mrpEnd;
	}

	public double getPoQuantity() {
		return poQuantity;
	}

	public void setPoQuantity(double poQuantity) {
		this.poQuantity = poQuantity;
	}

	public double getPoAmount() {
		return poAmount;
	}

	public void setPoAmount(double poAmount) {
		this.poAmount = poAmount;
	}

	public List<PurchaseOrderChild> getPol() {
		return pol;
	}

	public void setPol(List<PurchaseOrderChild> pol) {
		this.pol = pol;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public OffsetDateTime getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(OffsetDateTime createdTime) {
		this.createdTime = createdTime;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public OffsetDateTime getUpdationTime() {
		return updationTime;
	}

	public void setUpdationTime(OffsetDateTime updationTime) {
		this.updationTime = updationTime;
	}

	public String getAdditional() {
		return additional;
	}

	public void setAdditional(String additional) {
		this.additional = additional;
	}

	public String getPoType() {
		return poType;
	}

	public void setPoType(String poType) {
		this.poType = poType;
	}

	@Override
	public String toString() {
		return "PurchaseOrderParent [id=" + id + ", orgId=" + orgId + ", intGCode=" + intGCode + ", intGHeaderId="
				+ intGHeaderId + ", poReferenceId=" + poReferenceId + ", intGLineId=" + intGLineId + ", orderNo="
				+ orderNo + ", prevOrderNo=" + prevOrderNo + ", setHeaderId=" + setHeaderId + ", key=" + key
				+ ", isHoldPO=" + isHoldPO + ", isUDFExist=" + isUDFExist + ", isSiteExist=" + isSiteExist
				+ ", siteCode=" + siteCode + ", siteName=" + siteName + ", validFrom=" + validFrom + ", validTo="
				+ validTo + ", orderDate=" + orderDate + ", hl1Code=" + hl1Code + ", hl2Code=" + hl2Code + ", hl3Code="
				+ hl3Code + ", hl1Name=" + hl1Name + ", hl2Name=" + hl2Name + ", hl3Name=" + hl3Name + ", hl4Code="
				+ hl4Code + ", hl4Name=" + hl4Name + ", hsnSacCode=" + hsnSacCode + ", hsnCode=" + hsnCode + ", slCode="
				+ slCode + ", slName=" + slName + ", slCityName=" + slCityName + ", slAddr=" + slAddr + ", leadTime="
				+ leadTime + ", termCode=" + termCode + ", termName=" + termName + ", transporterCode="
				+ transporterCode + ", transporterName=" + transporterName + ", stateCode=" + stateCode + ", mrpStart="
				+ mrpStart + ", mrpEnd=" + mrpEnd + ", poQuantity=" + poQuantity + ", poAmount=" + poAmount + ", pol="
				+ pol + ", active=" + active + ", status=" + status + ", ipAddress=" + ipAddress + ", createdBy="
				+ createdBy + ", createdTime=" + createdTime + ", updatedBy=" + updatedBy + ", updationTime="
				+ updationTime + ", additional=" + additional + ", poType=" + poType + ", availableKeys="
				+ availableKeys + "]";
	}

}
