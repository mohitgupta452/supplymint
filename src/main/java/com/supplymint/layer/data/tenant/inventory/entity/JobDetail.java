package com.supplymint.layer.data.tenant.inventory.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.supplymint.util.DateFormatSerializer;

public class JobDetail {

	private int id;
	private String jobId;
	private String orgId;
	private String jobName;
	private String role;
	@JsonSerialize(using = DateFormatSerializer.class)
	private Date jobCreatedOn;
	@JsonSerialize(using = DateFormatSerializer.class)
	private Date jobModifiedOn;
	private String scriptLocationOn;
	@JsonSerialize(using = DateFormatSerializer.class)
	private Date startedOn;
	@JsonSerialize(using = DateFormatSerializer.class)
	private Date lastModifiedOn;
	@JsonSerialize(using = DateFormatSerializer.class)
	private Date completeOn;
	private String jobRunState;
	private String userName;
	private String timeTaken;
	private int allocatedCapacity;
	private String jobType;
	private String schedule;
	private String ipAddress;
	private String createdBy;
	private String updatedBy;
	@JsonSerialize(using = DateFormatSerializer.class)
	private Date createdOn;
	@JsonSerialize(using = DateFormatSerializer.class)
	private Date updatedOn;
	private String additional;
	private int storeCount;
	private int itemCount;
	@JsonSerialize(using = DateFormatSerializer.class)
	private Date repDate;
	private int totalCount;
	private String duration;
	private String triggerName;
	private String status;
	private String summary;
	private String bucketKey;
	@JsonSerialize(using = DateFormatSerializer.class)
	private Date lastEngineRun;

	// Custom Parameters
	private String store;
	private String storeCode;
	private String path;
	private String fileName;
	private String lastEngine;
	private String arguments;
	private String generatedTransferOrder;

	private String isSentMail;
	private int mailCounter;
	private String stockPoint;

	public JobDetail() {
		super();
	}

	public JobDetail(int id, String jobId, String orgId, String jobName, String role, Date jobCreatedOn,
			Date jobModifiedOn, String scriptLocationOn, Date startedOn, Date lastModifiedOn, Date completeOn,
			String jobRunState, String userName, String timeTaken, int allocatedCapacity, String jobType,
			String schedule, String ipAddress, String createdBy, String updatedBy, Date createdOn, Date updatedOn,
			String additional, int storeCount, int itemCount, Date repDate, int totalCount, String duration,
			String triggerName, String status, String summary, String bucketKey, Date lastEngineRun, String store,
			String storeCode, String path, String fileName, String lastEngine, String arguments,
			String generatedTransferOrder, String isSentMail, int mailCounter, String stockPoint) {
		super();
		this.id = id;
		this.jobId = jobId;
		this.orgId = orgId;
		this.jobName = jobName;
		this.role = role;
		this.jobCreatedOn = jobCreatedOn;
		this.jobModifiedOn = jobModifiedOn;
		this.scriptLocationOn = scriptLocationOn;
		this.startedOn = startedOn;
		this.lastModifiedOn = lastModifiedOn;
		this.completeOn = completeOn;
		this.jobRunState = jobRunState;
		this.userName = userName;
		this.timeTaken = timeTaken;
		this.allocatedCapacity = allocatedCapacity;
		this.jobType = jobType;
		this.schedule = schedule;
		this.ipAddress = ipAddress;
		this.createdBy = createdBy;
		this.updatedBy = updatedBy;
		this.createdOn = createdOn;
		this.updatedOn = updatedOn;
		this.additional = additional;
		this.storeCount = storeCount;
		this.itemCount = itemCount;
		this.repDate = repDate;
		this.totalCount = totalCount;
		this.duration = duration;
		this.triggerName = triggerName;
		this.status = status;
		this.summary = summary;
		this.bucketKey = bucketKey;
		this.lastEngineRun = lastEngineRun;
		this.store = store;
		this.storeCode = storeCode;
		this.path = path;
		this.fileName = fileName;
		this.lastEngine = lastEngine;
		this.arguments = arguments;
		this.generatedTransferOrder = generatedTransferOrder;
		this.isSentMail = isSentMail;
		this.mailCounter = mailCounter;
		this.stockPoint = stockPoint;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public Date getJobCreatedOn() {
		return jobCreatedOn;
	}

	public void setJobCreatedOn(Date jobCreatedOn) {
		this.jobCreatedOn = jobCreatedOn;
	}

	public Date getJobModifiedOn() {
		return jobModifiedOn;
	}

	public void setJobModifiedOn(Date jobModifiedOn) {
		this.jobModifiedOn = jobModifiedOn;
	}

	public String getScriptLocationOn() {
		return scriptLocationOn;
	}

	public void setScriptLocationOn(String scriptLocationOn) {
		this.scriptLocationOn = scriptLocationOn;
	}

	public Date getStartedOn() {
		return startedOn;
	}

	public void setStartedOn(Date startedOn) {
		this.startedOn = startedOn;
	}

	public Date getLastModifiedOn() {
		return lastModifiedOn;
	}

	public void setLastModifiedOn(Date lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}

	public Date getCompleteOn() {
		return completeOn;
	}

	public void setCompleteOn(Date completeOn) {
		this.completeOn = completeOn;
	}

	public String getJobRunState() {
		return jobRunState;
	}

	public void setJobRunState(String jobRunState) {
		this.jobRunState = jobRunState;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getTimeTaken() {
		return timeTaken;
	}

	public void setTimeTaken(String timeTaken) {
		this.timeTaken = timeTaken;
	}

	public int getAllocatedCapacity() {
		return allocatedCapacity;
	}

	public void setAllocatedCapacity(int allocatedCapacity) {
		this.allocatedCapacity = allocatedCapacity;
	}

	public String getJobType() {
		return jobType;
	}

	public void setJobType(String jobType) {
		this.jobType = jobType;
	}

	public String getSchedule() {
		return schedule;
	}

	public void setSchedule(String schedule) {
		this.schedule = schedule;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getAdditional() {
		return additional;
	}

	public void setAdditional(String additional) {
		this.additional = additional;
	}

	public int getStoreCount() {
		return storeCount;
	}

	public void setStoreCount(int storeCount) {
		this.storeCount = storeCount;
	}

	public int getItemCount() {
		return itemCount;
	}

	public void setItemCount(int itemCount) {
		this.itemCount = itemCount;
	}

	public Date getRepDate() {
		return repDate;
	}

	public void setRepDate(Date repDate) {
		this.repDate = repDate;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getTriggerName() {
		return triggerName;
	}

	public void setTriggerName(String triggerName) {
		this.triggerName = triggerName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getBucketKey() {
		return bucketKey;
	}

	public void setBucketKey(String bucketKey) {
		this.bucketKey = bucketKey;
	}

	public Date getLastEngineRun() {
		return lastEngineRun;
	}

	public void setLastEngineRun(Date lastEngineRun) {
		this.lastEngineRun = lastEngineRun;
	}

	public String getStore() {
		return store;
	}

	public void setStore(String store) {
		this.store = store;
	}

	public String getStoreCode() {
		return storeCode;
	}

	public void setStoreCode(String storeCode) {
		this.storeCode = storeCode;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getLastEngine() {
		return lastEngine;
	}

	public void setLastEngine(String lastEngine) {
		this.lastEngine = lastEngine;
	}

	public String getArguments() {
		return arguments;
	}

	public void setArguments(String arguments) {
		this.arguments = arguments;
	}

	public String getGeneratedTransferOrder() {
		return generatedTransferOrder;
	}

	public void setGeneratedTransferOrder(String generatedTransferOrder) {
		this.generatedTransferOrder = generatedTransferOrder;
	}


	public String getIsSentMail() {
		return isSentMail;
	}

	public void setIsSentMail(String isSentMail) {
		this.isSentMail = isSentMail;
	}

	public int getMailCounter() {
		return mailCounter;
	}

	public void setMailCounter(int mailCounter) {
		this.mailCounter = mailCounter;
	}

	public String getStockPoint() {
		return stockPoint;
	}

	public void setStockPoint(String stockPoint) {
		this.stockPoint = stockPoint;
	}

	@Override
	public String toString() {
		return "JobDetail [id=" + id + ", jobId=" + jobId + ", orgId=" + orgId + ", jobName=" + jobName + ", role="
				+ role + ", jobCreatedOn=" + jobCreatedOn + ", jobModifiedOn=" + jobModifiedOn + ", scriptLocationOn="
				+ scriptLocationOn + ", startedOn=" + startedOn + ", lastModifiedOn=" + lastModifiedOn + ", completeOn="
				+ completeOn + ", jobRunState=" + jobRunState + ", userName=" + userName + ", timeTaken=" + timeTaken
				+ ", allocatedCapacity=" + allocatedCapacity + ", jobType=" + jobType + ", schedule=" + schedule
				+ ", ipAddress=" + ipAddress + ", createdBy=" + createdBy + ", updatedBy=" + updatedBy + ", createdOn="
				+ createdOn + ", updatedOn=" + updatedOn + ", additional=" + additional + ", storeCount=" + storeCount
				+ ", itemCount=" + itemCount + ", repDate=" + repDate + ", totalCount=" + totalCount + ", duration="
				+ duration + ", triggerName=" + triggerName + ", status=" + status + ", summary=" + summary
				+ ", bucketKey=" + bucketKey + ", lastEngineRun=" + lastEngineRun + ", store=" + store + ", storeCode="
				+ storeCode + ", path=" + path + ", fileName=" + fileName + ", lastEngine=" + lastEngine
				+ ", arguments=" + arguments + ", generatedTransferOrder=" + generatedTransferOrder + ", isSentMail="
				+ isSentMail + ", mailCounter=" + mailCounter + ", stockPoint=" + stockPoint + "]";
	}

}
