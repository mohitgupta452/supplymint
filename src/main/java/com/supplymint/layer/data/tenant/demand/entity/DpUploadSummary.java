package com.supplymint.layer.data.tenant.demand.entity;

import java.time.OffsetDateTime;

public class DpUploadSummary {

	private String id;
	private String demandType;
	private String type;
	private int rowCount;
	private int validRow;
	private int invalidRow;
	private String status;
	private String fileName;
	private String attachUrl;
	private String statusCode;
	private String createdBy;
	private OffsetDateTime createdTime;
	private String updatedBy;
	private OffsetDateTime updationTime;
	private String ipAddress;
	private String url;
	private String lastSummary;
	private String userName;

	public DpUploadSummary() {
		// TODO Auto-generated constructor stub
	}

	public DpUploadSummary(String id, String demandType, String type, int rowCount, int validRow, int invalidRow,
			String status, String fileName, String attachUrl, String statusCode, String createdBy,
			OffsetDateTime createdTime, String updatedBy, OffsetDateTime updationTime, String ipAddress, String url,
			String lastSummary, String userName) {
		super();
		this.id = id;
		this.demandType = demandType;
		this.type = type;
		this.rowCount = rowCount;
		this.validRow = validRow;
		this.invalidRow = invalidRow;
		this.status = status;
		this.fileName = fileName;
		this.attachUrl = attachUrl;
		this.statusCode = statusCode;
		this.createdBy = createdBy;
		this.createdTime = createdTime;
		this.updatedBy = updatedBy;
		this.updationTime = updationTime;
		this.ipAddress = ipAddress;
		this.url = url;
		this.lastSummary = lastSummary;
		this.userName = userName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDemandType() {
		return demandType;
	}

	public void setDemandType(String demandType) {
		this.demandType = demandType;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getRowCount() {
		return rowCount;
	}

	public void setRowCount(int rowCount) {
		this.rowCount = rowCount;
	}

	public int getValidRow() {
		return validRow;
	}

	public void setValidRow(int validRow) {
		this.validRow = validRow;
	}

	public int getInvalidRow() {
		return invalidRow;
	}

	public void setInvalidRow(int invalidRow) {
		this.invalidRow = invalidRow;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getAttachUrl() {
		return attachUrl;
	}

	public void setAttachUrl(String attachUrl) {
		this.attachUrl = attachUrl;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public OffsetDateTime getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(OffsetDateTime createdTime) {
		this.createdTime = createdTime;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public OffsetDateTime getUpdationTime() {
		return updationTime;
	}

	public void setUpdationTime(OffsetDateTime updationTime) {
		this.updationTime = updationTime;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getLastSummary() {
		return lastSummary;
	}

	public void setLastSummary(String lastSummary) {
		this.lastSummary = lastSummary;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Override
	public String toString() {
		return "DpUploadSummary [id=" + id + ", demandType=" + demandType + ", type=" + type + ", rowCount=" + rowCount
				+ ", validRow=" + validRow + ", invalidRow=" + invalidRow + ", status=" + status + ", fileName="
				+ fileName + ", attachUrl=" + attachUrl + ", statusCode=" + statusCode + ", createdBy=" + createdBy
				+ ", createdTime=" + createdTime + ", updatedBy=" + updatedBy + ", updationTime=" + updationTime
				+ ", ipAddress=" + ipAddress + ", url=" + url + ", lastSummary=" + lastSummary + ", userName="
				+ userName + "]";
	}

}
