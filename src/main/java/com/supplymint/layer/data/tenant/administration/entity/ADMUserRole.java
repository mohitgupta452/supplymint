package com.supplymint.layer.data.tenant.administration.entity;

import java.time.OffsetDateTime;
import java.util.Arrays;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.supplymint.util.LocalDateTimeSerializer;
import com.supplymint.util.LocalDatetimeDeserializer;

public class ADMUserRole {

	private int id;
	private int userId;
	private String userName;
	private String name;
	private String roles;
	
	private String[] userRoles;

	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	private OffsetDateTime validFrom;

	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	private OffsetDateTime validTo;
	private char active;
	private String status;
	private String ipAddress;
	private String createdBy;

	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	private OffsetDateTime createdTime;
	private String updatedBy;

	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	private OffsetDateTime updationTime;
	private String additional;
	private String code;
	private int eid;
	
	// this data member only use for pagination
	private int offset;
	private int pagesize;

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public void setPagesize(int pagesize) {
		this.pagesize = pagesize;
	}

	// private ADMUser user;

	public ADMUserRole() {
		// TODO Auto-generated constructor stub
	}

	public int getEid() {
		return eid;
	}

	public void setEid(int eid) {
		this.eid = eid;
	}

	public ADMUserRole(int id, int userId, String userName, String name, String roles, String[] userRoles,
			OffsetDateTime validFrom, OffsetDateTime validTo, char active, String status, String ipAddress,
			String createdBy, OffsetDateTime createdTime, String updatedBy, OffsetDateTime updationTime,
			String additional, String code, int eid, int offset, int pagesize) {
		super();
		this.id = id;
		this.userId = userId;
		this.userName = userName;
		this.name = name;
		this.roles = roles;
		this.userRoles = userRoles;
		this.validFrom = validFrom;
		this.validTo = validTo;
		this.active = active;
		this.status = status;
		this.ipAddress = ipAddress;
		this.createdBy = createdBy;
		this.createdTime = createdTime;
		this.updatedBy = updatedBy;
		this.updationTime = updationTime;
		this.additional = additional;
		this.code = code;
		this.eid = eid;
		this.offset = offset;
		this.pagesize = pagesize;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRoles() {
		return roles;
	}

	public void setRoles(String roles) {
		this.roles = roles;
	}

	public String[] getUserRoles() {
		return userRoles;
	}

	public void setUserRoles(String[] userRoles) {
		this.userRoles = userRoles;
	}

	public OffsetDateTime getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(OffsetDateTime validFrom) {
		this.validFrom = validFrom;
	}

	public OffsetDateTime getValidTo() {
		return validTo;
	}

	public void setValidTo(OffsetDateTime validTo) {
		this.validTo = validTo;
	}

	public char getActive() {
		return active;
	}

	public void setActive(char active) {
		this.active = active;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public OffsetDateTime getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(OffsetDateTime createdTime) {
		this.createdTime = createdTime;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public OffsetDateTime getUpdationTime() {
		return updationTime;
	}

	public void setUpdationTime(OffsetDateTime updationTime) {
		this.updationTime = updationTime;
	}

	public String getAdditional() {
		return additional;
	}

	public void setAdditional(String additional) {
		this.additional = additional;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public int getOffset() {
		return offset;
	}

	public int getPagesize() {
		return pagesize;
	}

	@Override
	public String toString() {
		return "ADMUserRole [id=" + id + ", userId=" + userId + ", userName=" + userName + ", name=" + name + ", roles="
				+ roles + ", userRoles=" + Arrays.toString(userRoles) + ", validFrom=" + validFrom + ", validTo="
				+ validTo + ", active=" + active + ", status=" + status + ", ipAddress=" + ipAddress + ", createdBy="
				+ createdBy + ", createdTime=" + createdTime + ", updatedBy=" + updatedBy + ", updationTime="
				+ updationTime + ", additional=" + additional + ", code=" + code + ", offset=" + offset + ", pagesize="
				+ pagesize + "]";
	}

	
	
	
}
