package com.supplymint.layer.data.tenant.procurement.entity;

import java.math.BigDecimal;

public class PurtermCharge {

	private BigDecimal ptTermMainCode;
	private BigDecimal ptSeq;
	private BigDecimal ptRate;
	private String ptFormulae;
	private String ptSign;
	private String ptOperationLevel;
	private BigDecimal ptCode;
	private BigDecimal chgCode;
	private String chgName;
	private BigDecimal glCode;
	private BigDecimal rate;
	private String sign;
	private String roundOff;
	private String ext;
	private BigDecimal formCode;
	private BigDecimal cmpCode;
	private BigDecimal slCode;
	private String taxable;
	private String basis;
	private String operationLevel;
	private BigDecimal finTradeGroupCode;
	private String isTax;
	private String source;
	private String includeInCost;
	private String excludeInInvoice;
	private String isChangeable;
	private String cashDiscountApplicable;
	private String gstComponent;
	private String isReverse;
	private String isSystem;
	public PurtermCharge(BigDecimal ptTermMainCode, BigDecimal ptSeq, BigDecimal ptRate, String ptFormulae,
			String ptSign, String ptOperationLevel, BigDecimal ptCode, BigDecimal chgCode, String chgName,
			BigDecimal glCode, BigDecimal rate, String sign, String roundOff, String ext, BigDecimal formCode,
			BigDecimal cmpCode, BigDecimal slCode, String taxable, String basis, String operationLevel,
			BigDecimal finTradeGroupCode, String isTax, String source, String includeInCost, String excludeInInvoice,
			String isChangeable, String cashDiscountApplicable, String gstComponent, String isReverse,
			String isSystem) {
		super();
		this.ptTermMainCode = ptTermMainCode;
		this.ptSeq = ptSeq;
		this.ptRate = ptRate;
		this.ptFormulae = ptFormulae;
		this.ptSign = ptSign;
		this.ptOperationLevel = ptOperationLevel;
		this.ptCode = ptCode;
		this.chgCode = chgCode;
		this.chgName = chgName;
		this.glCode = glCode;
		this.rate = rate;
		this.sign = sign;
		this.roundOff = roundOff;
		this.ext = ext;
		this.formCode = formCode;
		this.cmpCode = cmpCode;
		this.slCode = slCode;
		this.taxable = taxable;
		this.basis = basis;
		this.operationLevel = operationLevel;
		this.finTradeGroupCode = finTradeGroupCode;
		this.isTax = isTax;
		this.source = source;
		this.includeInCost = includeInCost;
		this.excludeInInvoice = excludeInInvoice;
		this.isChangeable = isChangeable;
		this.cashDiscountApplicable = cashDiscountApplicable;
		this.gstComponent = gstComponent;
		this.isReverse = isReverse;
		this.isSystem = isSystem;
	}
	public BigDecimal getPtTermMainCode() {
		return ptTermMainCode;
	}
	public void setPtTermMainCode(BigDecimal ptTermMainCode) {
		this.ptTermMainCode = ptTermMainCode;
	}
	public BigDecimal getPtSeq() {
		return ptSeq;
	}
	public void setPtSeq(BigDecimal ptSeq) {
		this.ptSeq = ptSeq;
	}
	public BigDecimal getPtRate() {
		return ptRate;
	}
	public void setPtRate(BigDecimal ptRate) {
		this.ptRate = ptRate;
	}
	public String getPtFormulae() {
		return ptFormulae;
	}
	public void setPtFormulae(String ptFormulae) {
		this.ptFormulae = ptFormulae;
	}
	public String getPtSign() {
		return ptSign;
	}
	public void setPtSign(String ptSign) {
		this.ptSign = ptSign;
	}
	public String getPtOperationLevel() {
		return ptOperationLevel;
	}
	public void setPtOperationLevel(String ptOperationLevel) {
		this.ptOperationLevel = ptOperationLevel;
	}
	public BigDecimal getPtCode() {
		return ptCode;
	}
	public void setPtCode(BigDecimal ptCode) {
		this.ptCode = ptCode;
	}
	public BigDecimal getChgCode() {
		return chgCode;
	}
	public void setChgCode(BigDecimal chgCode) {
		this.chgCode = chgCode;
	}
	public String getChgName() {
		return chgName;
	}
	public void setChgName(String chgName) {
		this.chgName = chgName;
	}
	public BigDecimal getGlCode() {
		return glCode;
	}
	public void setGlCode(BigDecimal glCode) {
		this.glCode = glCode;
	}
	public BigDecimal getRate() {
		return rate;
	}
	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	public String getRoundOff() {
		return roundOff;
	}
	public void setRoundOff(String roundOff) {
		this.roundOff = roundOff;
	}
	public String getExt() {
		return ext;
	}
	public void setExt(String ext) {
		this.ext = ext;
	}
	public BigDecimal getFormCode() {
		return formCode;
	}
	public void setFormCode(BigDecimal formCode) {
		this.formCode = formCode;
	}
	public BigDecimal getCmpCode() {
		return cmpCode;
	}
	public void setCmpCode(BigDecimal cmpCode) {
		this.cmpCode = cmpCode;
	}
	public BigDecimal getSlCode() {
		return slCode;
	}
	public void setSlCode(BigDecimal slCode) {
		this.slCode = slCode;
	}
	public String getTaxable() {
		return taxable;
	}
	public void setTaxable(String taxable) {
		this.taxable = taxable;
	}
	public String getBasis() {
		return basis;
	}
	public void setBasis(String basis) {
		this.basis = basis;
	}
	public String getOperationLevel() {
		return operationLevel;
	}
	public void setOperationLevel(String operationLevel) {
		this.operationLevel = operationLevel;
	}
	public BigDecimal getFinTradeGroupCode() {
		return finTradeGroupCode;
	}
	public void setFinTradeGroupCode(BigDecimal finTradeGroupCode) {
		this.finTradeGroupCode = finTradeGroupCode;
	}
	public String getIsTax() {
		return isTax;
	}
	public void setIsTax(String isTax) {
		this.isTax = isTax;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getIncludeInCost() {
		return includeInCost;
	}
	public void setIncludeInCost(String includeInCost) {
		this.includeInCost = includeInCost;
	}
	public String getExcludeInInvoice() {
		return excludeInInvoice;
	}
	public void setExcludeInInvoice(String excludeInInvoice) {
		this.excludeInInvoice = excludeInInvoice;
	}
	public String getIsChangeable() {
		return isChangeable;
	}
	public void setIsChangeable(String isChangeable) {
		this.isChangeable = isChangeable;
	}
	public String getCashDiscountApplicable() {
		return cashDiscountApplicable;
	}
	public void setCashDiscountApplicable(String cashDiscountApplicable) {
		this.cashDiscountApplicable = cashDiscountApplicable;
	}
	public String getGstComponent() {
		return gstComponent;
	}
	public void setGstComponent(String gstComponent) {
		this.gstComponent = gstComponent;
	}
	public String getIsReverse() {
		return isReverse;
	}
	public void setIsReverse(String isReverse) {
		this.isReverse = isReverse;
	}
	public String getIsSystem() {
		return isSystem;
	}
	public void setIsSystem(String isSystem) {
		this.isSystem = isSystem;
	}
	
	@Override
	public String toString() {
		return "PurtermCharge [ptTermMainCode=" + ptTermMainCode + ", ptSeq=" + ptSeq + ", ptRate=" + ptRate
				+ ", ptFormulae=" + ptFormulae + ", ptSign=" + ptSign + ", ptOperationLevel=" + ptOperationLevel
				+ ", ptCode=" + ptCode + ", chgCode=" + chgCode + ", chgName=" + chgName + ", glCode=" + glCode
				+ ", rate=" + rate + ", sign=" + sign + ", roundOff=" + roundOff + ", ext=" + ext + ", formCode="
				+ formCode + ", cmpCode=" + cmpCode + ", slCode=" + slCode + ", taxable=" + taxable + ", basis=" + basis
				+ ", operationLevel=" + operationLevel + ", finTradeGroupCode=" + finTradeGroupCode + ", isTax=" + isTax
				+ ", source=" + source + ", includeInCost=" + includeInCost + ", excludeInInvoice=" + excludeInInvoice
				+ ", isChangeable=" + isChangeable + ", cashDiscountApplicable=" + cashDiscountApplicable
				+ ", gstComponent=" + gstComponent + ", isReverse=" + isReverse + ", isSystem=" + isSystem + "]";
	}
	
	
}
