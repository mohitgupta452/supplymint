package com.supplymint.layer.data.tenant.analytic.entity;

public class StoreProfileActualData {

	private String siteCode;
	private String totalSalesLY;
	private String totalQtySoldLY;
	private String avgSalesLY;
	private String totalSalesCM;

	public StoreProfileActualData() {

	}

	public StoreProfileActualData(String siteCode, String totalSalesLY, String totalQtySoldLY, String avgSalesLY,
			String totalSalesCM) {
		super();
		this.siteCode = siteCode;
		this.totalSalesLY = totalSalesLY;
		this.totalQtySoldLY = totalQtySoldLY;
		this.avgSalesLY = avgSalesLY;
		this.totalSalesCM = totalSalesCM;
	}

	public String getSiteCode() {
		return siteCode;
	}

	public void setSiteCode(String siteCode) {
		this.siteCode = siteCode;
	}

	public String getTotalSalesLY() {
		return totalSalesLY;
	}

	public void setTotalSalesLY(String totalSalesLY) {
		this.totalSalesLY = totalSalesLY;
	}

	public String getTotalQtySoldLY() {
		return totalQtySoldLY;
	}

	public void setTotalQtySoldLY(String totalQtySoldLY) {
		this.totalQtySoldLY = totalQtySoldLY;
	}

	public String getAvgSalesLY() {
		return avgSalesLY;
	}

	public void setAvgSalesLY(String avgSalesLY) {
		this.avgSalesLY = avgSalesLY;
	}

	public String getTotalSalesCM() {
		return totalSalesCM;
	}

	public void setTotalSalesCM(String totalSalesCM) {
		this.totalSalesCM = totalSalesCM;
	}

	@Override
	public String toString() {
		return "StoreProfileActualData [siteCode=" + siteCode + ", totalSalesLY=" + totalSalesLY + ", totalQtySoldLY="
				+ totalQtySoldLY + ", avgSalesLY=" + avgSalesLY + ", totalSalesCM=" + totalSalesCM + "]";
	}

}
