package com.supplymint.layer.data.tenant.dashboard.entity;

public class DashboardWindow {

	public long totalSales;
	public int totalUnits;
	public int avgSalesUnit;
	public int avgAssortmentUnit;

	public DashboardWindow(long totalSales, int totalUnits, int avgSalesUnit, int avgAssortmentUnit) {
		super();
		this.totalSales = totalSales;
		this.totalUnits = totalUnits;
		this.avgSalesUnit = avgSalesUnit;
		this.avgAssortmentUnit = avgAssortmentUnit;
	}

	public DashboardWindow() {
		super();
		// TODO Auto-generated constructor stub
	}

	public long getTotalSales() {
		return totalSales;
	}

	public void setTotalSales(long totalSales) {
		this.totalSales = totalSales;
	}

	public int getTotalUnits() {
		return totalUnits;
	}

	public void setTotalUnits(int totalUnits) {
		this.totalUnits = totalUnits;
	}

	public int getAvgSalesUnit() {
		return avgSalesUnit;
	}

	public void setAvgSalesUnit(int avgSalesUnit) {
		this.avgSalesUnit = avgSalesUnit;
	}

	public int getAvgAssortmentUnit() {
		return avgAssortmentUnit;
	}

	public void setAvgAssortmentUnit(int avgAssortmentUnit) {
		this.avgAssortmentUnit = avgAssortmentUnit;
	}

	@Override
	public String toString() {
		return "DashboardWindow [totalSales=" + totalSales + ", totalUnits=" + totalUnits + ", avgSalesUnit="
				+ avgSalesUnit + ", avgAssortmentUnit=" + avgAssortmentUnit + "]";
	}

}
