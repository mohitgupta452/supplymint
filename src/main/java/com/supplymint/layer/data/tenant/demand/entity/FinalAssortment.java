package com.supplymint.layer.data.tenant.demand.entity;

public class FinalAssortment {

	private String icode;
	private String assortmentCode;
	private String itemName;
	private String brand;
	private String color;
	private String size;
	private String pattern;
	private String material;
	private String design;
	private String costPrice;

	public FinalAssortment() {
		// TODO Auto-generated constructor stub
	}

	public FinalAssortment(String icode, String assortmentCode, String itemName, String brand, String color,
			String size, String pattern, String material, String design, String costPrice) {
		super();
		this.icode = icode;
		this.assortmentCode = assortmentCode;
		this.itemName = itemName;
		this.brand = brand;
		this.color = color;
		this.size = size;
		this.pattern = pattern;
		this.material = material;
		this.design = design;
		this.costPrice = costPrice;
	}

	public String getIcode() {
		return icode;
	}

	public void setIcode(String icode) {
		this.icode = icode;
	}

	public String getAssortmentCode() {
		return assortmentCode;
	}

	public void setAssortmentCode(String assortmentCode) {
		this.assortmentCode = assortmentCode;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	public String getMaterial() {
		return material;
	}

	public void setMaterial(String material) {
		this.material = material;
	}

	public String getDesign() {
		return design;
	}

	public void setDesign(String design) {
		this.design = design;
	}

	public String getCostPrice() {
		return costPrice;
	}

	public void setCostPrice(String costPrice) {
		this.costPrice = costPrice;
	}

	@Override
	public String toString() {
		return "FinalAssortment [icode=" + icode + ", assortmentCode=" + assortmentCode + ", itemName=" + itemName
				+ ", brand=" + brand + ", color=" + color + ", size=" + size + ", pattern=" + pattern + ", material="
				+ material + ", design=" + design + ", costPrice=" + costPrice + "]";
	}

}
