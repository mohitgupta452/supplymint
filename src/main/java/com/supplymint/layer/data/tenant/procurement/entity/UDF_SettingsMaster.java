package com.supplymint.layer.data.tenant.procurement.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UDF_SettingsMaster {

	private int code;
	private String udfType;
	private String displayName;
	private char isCompulsary;
	private String orderBy;
	private char isLov;

	public UDF_SettingsMaster() {
		// TODO Auto-generated constructor stub
	}

	public UDF_SettingsMaster(String udfType, String displayName, char isCompulsary) {
		super();
		this.udfType = udfType;
		this.displayName = displayName;
		this.isCompulsary = isCompulsary;
	}

	public UDF_SettingsMaster(int code, String udfType, String displayName, char isCompulsary, String orderBy,
			char isLov) {
		super();
		this.code = code;
		this.udfType = udfType;
		this.displayName = displayName;
		this.isCompulsary = isCompulsary;
		this.orderBy = orderBy;
		this.isLov = isLov;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getUdfType() {
		return udfType;
	}

	public void setUdfType(String udfType) {
		this.udfType = udfType;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public char getIsCompulsary() {
		return isCompulsary;
	}

	public void setIsCompulsary(char isCompulsary) {
		this.isCompulsary = isCompulsary;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public char getIsLov() {
		return isLov;
	}

	public void setIsLov(char isLov) {
		this.isLov = isLov;
	}

	@Override
	public String toString() {
		return "UDF_MapMaster [code=" + code + ", udfType=" + udfType + ", displayName=" + displayName
				+ ", isCompulsary=" + isCompulsary + ", orderBy=" + orderBy + ", isLov=" + isLov + "]";
	}

}
