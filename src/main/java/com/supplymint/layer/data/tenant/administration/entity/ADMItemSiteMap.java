package com.supplymint.layer.data.tenant.administration.entity;

import java.time.OffsetDateTime;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class ADMItemSiteMap {

	// data members
	private int id;
	private int siteId;
	private String siteName;
	private int itemId;
	private String itemName;
	private String quantity;
	private char active;
	private String status;
	private String ipAddress;
	private String createdBy;
	
	@JsonIgnore
	private OffsetDateTime createdTime;
	private String updatedBy;
	
	@JsonIgnore
	private OffsetDateTime updationTime;
	private String additional;
	
	
	public ADMItemSiteMap(int id, int siteId, String siteName, int itemId, String itemName, String quantity,
			char active, String status, String ipAddress, String createdBy, OffsetDateTime createdTime,
			String updatedBy, OffsetDateTime updationTime, String additional) {
		super();
		this.id = id;
		this.siteId = siteId;
		this.siteName = siteName;
		this.itemId = itemId;
		this.itemName = itemName;
		this.quantity = quantity;
		this.active = active;
		this.status = status;
		this.ipAddress = ipAddress;
		this.createdBy = createdBy;
		this.createdTime = createdTime;
		this.updatedBy = updatedBy;
		this.updationTime = updationTime;
		this.additional = additional;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public int getSiteId() {
		return siteId;
	}


	public void setSiteId(int siteId) {
		this.siteId = siteId;
	}


	public String getSiteName() {
		return siteName;
	}


	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}


	public int getItemId() {
		return itemId;
	}


	public void setItemId(int itemId) {
		this.itemId = itemId;
	}


	public String getItemName() {
		return itemName;
	}


	public void setItemName(String itemName) {
		this.itemName = itemName;
	}


	public String getQuantity() {
		return quantity;
	}


	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}


	public char getActive() {
		return active;
	}


	public void setActive(char active) {
		this.active = active;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public String getIpAddress() {
		return ipAddress;
	}


	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}


	public String getCreatedBy() {
		return createdBy;
	}


	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}


	public OffsetDateTime getCreatedTime() {
		return createdTime;
	}


	public void setCreatedTime(OffsetDateTime createdTime) {
		this.createdTime = createdTime;
	}


	public String getUpdatedBy() {
		return updatedBy;
	}


	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}


	public OffsetDateTime getUpdationTime() {
		return updationTime;
	}


	public void setUpdationTime(OffsetDateTime updationTime) {
		this.updationTime = updationTime;
	}


	public String getAdditional() {
		return additional;
	}


	public void setAdditional(String additional) {
		this.additional = additional;
	}


	@Override
	public String toString() {
		return "ItemSiteMapping [id=" + id + ", siteId=" + siteId + ", siteName=" + siteName + ", itemId=" + itemId
				+ ", itemName=" + itemName + ", quantity=" + quantity + ", active=" + active + ", status=" + status
				+ ", ipAddress=" + ipAddress + ", createdBy=" + createdBy + ", createdTime=" + createdTime
				+ ", updatedBy=" + updatedBy + ", updationTime=" + updationTime + ", additional=" + additional + "]";
	}

	
	
	
}
