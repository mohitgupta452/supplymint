package com.supplymint.layer.data.tenant.dashboard.entity;

public class SalesTrend {
	public String day;
	public long value;
	public String year;

	public SalesTrend() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SalesTrend(String day, long value, String year) {
		super();
		this.day = day;
		this.value = value;
		this.year = year;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public long getValue() {
		return value;
	}

	public void setValue(long value) {
		this.value = value;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	@Override
	public String toString() {
		return "SalesTrend [day=" + day + ", value=" + value + ", year=" + year + "]";
	}

}
