package com.supplymint.layer.data.tenant.demand.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.supplymint.util.DateFormatSerializer;

public class OTBPlanASD {

	private int id;
	private String planId;
	private String assortmentCode;
	private String billDate;
	private String qty;
	private String billdateFrom;
	private String billdateTo;
	private String quarter;
	private double costPrice;
	private double sellValue;
	private String isModified;
	private double modifiedPercentage;

	private double planSales;
	private double markDown;
	private double closingInventory;
	private double openingStock;
	private double openPurchaseOrder;
	private double otbValue;
	private String status;
	private char active;
	private String ipAddress;
	private String createdBy;
//	@JsonSerialize(using = DateFormatSerializer.class)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd MMM yyyy HH:mm")
	private Date createdOn;
	private String updatedBy;
//	@JsonSerialize(using = DateFormatSerializer.class)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd MMM yyyy HH:mm")
	private Date updatedOn;
	private String additional;
	private String futureBillDate;

	public OTBPlanASD() {

	}

	public OTBPlanASD(int id, String planId, String assortmentCode, String billDate, String qty, String billdateFrom,
			String billdateTo, String quarter, double costPrice, double sellValue, String isModified,
			double modifiedPercentage, double planSales, double markDown, double closingInventory, double openingStock,
			double openPurchaseOrder, double otbValue, String status, char active, String ipAddress, String createdBy,
			Date createdOn, String updatedBy, Date updatedOn, String additional, String futureBillDate) {
		super();
		this.id = id;
		this.planId = planId;
		this.assortmentCode = assortmentCode;
		this.billDate = billDate;
		this.qty = qty;
		this.billdateFrom = billdateFrom;
		this.billdateTo = billdateTo;
		this.quarter = quarter;
		this.costPrice = costPrice;
		this.sellValue = sellValue;
		this.isModified = isModified;
		this.modifiedPercentage = modifiedPercentage;
		this.planSales = planSales;
		this.markDown = markDown;
		this.closingInventory = closingInventory;
		this.openingStock = openingStock;
		this.openPurchaseOrder = openPurchaseOrder;
		this.otbValue = otbValue;
		this.status = status;
		this.active = active;
		this.ipAddress = ipAddress;
		this.createdBy = createdBy;
		this.createdOn = createdOn;
		this.updatedBy = updatedBy;
		this.updatedOn = updatedOn;
		this.additional = additional;
		this.futureBillDate = futureBillDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	public String getAssortmentCode() {
		return assortmentCode;
	}

	public void setAssortmentCode(String assortmentCode) {
		this.assortmentCode = assortmentCode;
	}

	public String getBillDate() {
		return billDate;
	}

	public void setBillDate(String billDate) {
		this.billDate = billDate;
	}

	public String getQty() {
		return qty;
	}

	public void setQty(String qty) {
		this.qty = qty;
	}

	public String getBilldateFrom() {
		return billdateFrom;
	}

	public void setBilldateFrom(String billdateFrom) {
		this.billdateFrom = billdateFrom;
	}

	public String getBilldateTo() {
		return billdateTo;
	}

	public void setBilldateTo(String billdateTo) {
		this.billdateTo = billdateTo;
	}

	public String getQuarter() {
		return quarter;
	}

	public void setQuarter(String quarter) {
		this.quarter = quarter;
	}

	public double getCostPrice() {
		return costPrice;
	}

	public void setCostPrice(double costPrice) {
		this.costPrice = costPrice;
	}

	public double getSellValue() {
		return sellValue;
	}

	public void setSellValue(double sellValue) {
		this.sellValue = sellValue;
	}

	public String getIsModified() {
		return isModified;
	}

	public void setIsModified(String isModified) {
		this.isModified = isModified;
	}

	public double getModifiedPercentage() {
		return modifiedPercentage;
	}

	public void setModifiedPercentage(double modifiedPercentage) {
		this.modifiedPercentage = modifiedPercentage;
	}

	public double getPlanSales() {
		return planSales;
	}

	public void setPlanSales(double planSales) {
		this.planSales = planSales;
	}

	public double getMarkDown() {
		return markDown;
	}

	public void setMarkDown(double markDown) {
		this.markDown = markDown;
	}

	public double getClosingInventory() {
		return closingInventory;
	}

	public void setClosingInventory(double closingInventory) {
		this.closingInventory = closingInventory;
	}

	public double getOpeningStock() {
		return openingStock;
	}

	public void setOpeningStock(double openingStock) {
		this.openingStock = openingStock;
	}

	public double getOpenPurchaseOrder() {
		return openPurchaseOrder;
	}

	public void setOpenPurchaseOrder(double openPurchaseOrder) {
		this.openPurchaseOrder = openPurchaseOrder;
	}

	public double getOtbValue() {
		return otbValue;
	}

	public void setOtbValue(double otbValue) {
		this.otbValue = otbValue;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public char getActive() {
		return active;
	}

	public void setActive(char active) {
		this.active = active;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getAdditional() {
		return additional;
	}

	public void setAdditional(String additional) {
		this.additional = additional;
	}

	public String getFutureBillDate() {
		return futureBillDate;
	}

	public void setFutureBillDate(String futureBillDate) {
		this.futureBillDate = futureBillDate;
	}

	@Override
	public String toString() {
		return "OTBPlanASD [id=" + id + ", planId=" + planId + ", assortmentCode=" + assortmentCode + ", billDate="
				+ billDate + ", qty=" + qty + ", billdateFrom=" + billdateFrom + ", billdateTo=" + billdateTo
				+ ", quarter=" + quarter + ", costPrice=" + costPrice + ", sellValue=" + sellValue + ", isModified="
				+ isModified + ", modifiedPercentage=" + modifiedPercentage + ", planSales=" + planSales + ", markDown="
				+ markDown + ", closingInventory=" + closingInventory + ", openingStock=" + openingStock
				+ ", openPurchaseOrder=" + openPurchaseOrder + ", otbValue=" + otbValue + ", status=" + status
				+ ", active=" + active + ", ipAddress=" + ipAddress + ", createdBy=" + createdBy + ", createdOn="
				+ createdOn + ", updatedBy=" + updatedBy + ", updatedOn=" + updatedOn + ", additional=" + additional
				+ ", futureBillDate=" + futureBillDate + "]";
	}

}
