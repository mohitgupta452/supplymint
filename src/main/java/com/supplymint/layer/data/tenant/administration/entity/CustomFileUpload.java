package com.supplymint.layer.data.tenant.administration.entity;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.supplymint.util.DateFormatSerializer;

public class CustomFileUpload {

	private int id;
	private String channel;
	private String template;
	private String filePath;
	private String activeFile;
	private String bucket;
	private String status;
	private char active;
	private String ipAddress;
	private String createdBy;

	@JsonSerialize(using = DateFormatSerializer.class)
	private Date createdTime;
	private String updatedBy;
	@JsonSerialize(using = DateFormatSerializer.class)
	private Date updationTime;
	private String additional;

	public CustomFileUpload() {

	}

	public CustomFileUpload(int id, String channel, String template, String filePath, String activeFile, String bucket,
			String status, char active, String ipAddress, String createdBy, Date createdTime, String updatedBy,
			Date updationTime, String additional) {
		super();
		this.id = id;
		this.channel = channel;
		this.template = template;
		this.filePath = filePath;
		this.activeFile = activeFile;
		this.bucket = bucket;
		this.status = status;
		this.active = active;
		this.ipAddress = ipAddress;
		this.createdBy = createdBy;
		this.createdTime = createdTime;
		this.updatedBy = updatedBy;
		this.updationTime = updationTime;
		this.additional = additional;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getActiveFile() {
		return activeFile;
	}

	public void setActiveFile(String activeFile) {
		this.activeFile = activeFile;
	}

	public String getBucket() {
		return bucket;
	}

	public void setBucket(String bucket) {
		this.bucket = bucket;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public char getActive() {
		return active;
	}

	public void setActive(char active) {
		this.active = active;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdationTime() {
		return updationTime;
	}

	public void setUpdationTime(Date updationTime) {
		this.updationTime = updationTime;
	}

	public String getAdditional() {
		return additional;
	}

	public void setAdditional(String additional) {
		this.additional = additional;
	}

	@Override
	public String toString() {
		return "CustomFileUpload [id=" + id + ", channel=" + channel + ", template=" + template + ", filePath="
				+ filePath + ", activeFile=" + activeFile + ", bucket=" + bucket + ", status=" + status + ", active="
				+ active + ", ipAddress=" + ipAddress + ", createdBy=" + createdBy + ", createdTime=" + createdTime
				+ ", updatedBy=" + updatedBy + ", updationTime=" + updationTime + ", additional=" + additional + "]";
	}

	
}
