package com.supplymint.layer.data.tenant.demand.entity;

import java.time.OffsetDateTime;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.supplymint.util.LocalDateTimeSerializer;

public class AdHocRequest {

	private int id;
	private String site;

	private String itemCode;
	private String brand;
	private String color;
	private String quantity;
	private String size;
	private String pattern;
	private String material;
	private String design;
	private String assortment;
	private String active;
	private String reqStatus;
	private String itemStatus;

	//@JsonSerialize(using = LocalDateTimeSerializer.class)
//	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	//@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd MMM yyyy")
	private OffsetDateTime requestedDate;
	private String ipAddress;
	private String createdBy;

	@JsonSerialize(using = LocalDateTimeSerializer.class)
	private OffsetDateTime createdTime;

	private String updatedBy;

	@JsonSerialize(using = LocalDateTimeSerializer.class)
	private OffsetDateTime updationTime;
	private String additional;
	private String workOrder;
	private String status;
	private List<AdHocItemRequest> item;

	public AdHocRequest() {

	}

	public AdHocRequest(int id, String site, String itemCode, String brand, String color, String quantity, String size,
			String pattern, String material, String design, String assortment, String active, String reqStatus,
			String itemStatus, OffsetDateTime requestedDate, String ipAddress, String createdBy,
			OffsetDateTime createdTime, String updatedBy, OffsetDateTime updationTime, String additional,
			String workOrder, String status, List<AdHocItemRequest> item) {
		super();
		this.id = id;
		this.site = site;
		this.itemCode = itemCode;
		this.brand = brand;
		this.color = color;
		this.quantity = quantity;
		this.size = size;
		this.pattern = pattern;
		this.material = material;
		this.design = design;
		this.assortment = assortment;
		this.active = active;
		this.reqStatus = reqStatus;
		this.itemStatus = itemStatus;
		this.requestedDate = requestedDate;
		this.ipAddress = ipAddress;
		this.createdBy = createdBy;
		this.createdTime = createdTime;
		this.updatedBy = updatedBy;
		this.updationTime = updationTime;
		this.additional = additional;
		this.workOrder = workOrder;
		this.status = status;
		this.item = item;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	public String getMaterial() {
		return material;
	}

	public void setMaterial(String material) {
		this.material = material;
	}

	public String getDesign() {
		return design;
	}

	public void setDesign(String design) {
		this.design = design;
	}

	public String getAssortment() {
		return assortment;
	}

	public void setAssortment(String assortment) {
		this.assortment = assortment;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getReqStatus() {
		return reqStatus;
	}

	public void setReqStatus(String reqStatus) {
		this.reqStatus = reqStatus;
	}

	public String getItemStatus() {
		return itemStatus;
	}

	public void setItemStatus(String itemStatus) {
		this.itemStatus = itemStatus;
	}

	public OffsetDateTime getRequestedDate() {
		return requestedDate;
	}

	public void setRequestedDate(OffsetDateTime requestedDate) {
		this.requestedDate = requestedDate;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public OffsetDateTime getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(OffsetDateTime createdTime) {
		this.createdTime = createdTime;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public OffsetDateTime getUpdationTime() {
		return updationTime;
	}

	public void setUpdationTime(OffsetDateTime updationTime) {
		this.updationTime = updationTime;
	}

	public String getAdditional() {
		return additional;
	}

	public void setAdditional(String additional) {
		this.additional = additional;
	}

	public String getWorkOrder() {
		return workOrder;
	}

	public void setWorkOrder(String workOrder) {
		this.workOrder = workOrder;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<AdHocItemRequest> getItem() {
		return item;
	}

	public void setItem(List<AdHocItemRequest> item) {
		this.item = item;
	}

	@Override
	public String toString() {
		return "AdHocRequest [id=" + id + ", site=" + site + ", itemCode=" + itemCode + ", brand=" + brand + ", color="
				+ color + ", quantity=" + quantity + ", size=" + size + ", pattern=" + pattern + ", material="
				+ material + ", design=" + design + ", assortment=" + assortment + ", active=" + active + ", reqStatus="
				+ reqStatus + ", itemStatus=" + itemStatus + ", requestedDate=" + requestedDate + ", ipAddress="
				+ ipAddress + ", createdBy=" + createdBy + ", createdTime=" + createdTime + ", updatedBy=" + updatedBy
				+ ", updationTime=" + updationTime + ", additional=" + additional + ", workOrder=" + workOrder
				+ ", status=" + status + ", item=" + item + "]";
	}

}
