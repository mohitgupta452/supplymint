package com.supplymint.layer.data.tenant.administration.entity;

import java.time.OffsetDateTime;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.supplymint.util.DateFormatSerializer;
import com.supplymint.util.LocalDateTimeSerializer;
import com.supplymint.util.LocalDatetimeDeserializer;

public class DataReference {

	private int id;
	private String name;
	private String key;
	@JsonSerialize(using = DateFormatSerializer.class)
	private Date eventStart;
	@JsonSerialize(using = DateFormatSerializer.class)
	private Date eventEnd;
	private String status;
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@JsonDeserialize(using = LocalDatetimeDeserializer.class)
	private OffsetDateTime creationTime;
	private int dataCount;
	@JsonIgnore
	private int pageNo;
	@JsonIgnore
	private int offset;

	public DataReference() {

	}

	public DataReference(int id, String name, String key, Date eventStart, Date eventEnd, String status,
			OffsetDateTime creationTime, int dataCount, int pageNo, int offset) {
		super();
		this.id = id;
		this.name = name;
		this.key = key;
		this.eventStart = eventStart;
		this.eventEnd = eventEnd;
		this.status = status;
		this.creationTime = creationTime;
		this.dataCount = dataCount;
		this.pageNo = pageNo;
		this.offset = offset;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public Date getEventStart() {
		return eventStart;
	}

	public void setEventStart(Date eventStart) {
		this.eventStart = eventStart;
	}

	public Date getEventEnd() {
		return eventEnd;
	}

	public void setEventEnd(Date eventEnd) {
		this.eventEnd = eventEnd;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public OffsetDateTime getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(OffsetDateTime creationTime) {
		this.creationTime = creationTime;
	}

	public int getDataCount() {
		return dataCount;
	}

	public void setDataCount(int dataCount) {
		this.dataCount = dataCount;
	}

	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	@Override
	public String toString() {
		return "DataReference [id=" + id + ", name=" + name + ", key=" + key + ", eventStart=" + eventStart
				+ ", eventEnd=" + eventEnd + ", status=" + status + ", creationTime=" + creationTime + ", dataCount="
				+ dataCount + ", pageNo=" + pageNo + ", offset=" + offset + "]";
	}

}
