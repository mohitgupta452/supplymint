package com.supplymint.layer.data.tenant.procurement.entity;

public class FinChargeDetail {

	private String chargeCode;
	private String seq;
	private String rate;
	private String formula;
	private String sign;
	private String operationLevel;
	private String chargeAmount;
	private String subTotal;

	public FinChargeDetail() {
		super();
		// TODO Auto-generated constructor stub
	}

	public FinChargeDetail(String chgCode, String seq, String rate, String formula, String sign, String operationLevel,
			String chargeAmount, String subTotal) {
		super();
		this.chargeCode = chgCode;
		this.seq = seq;
		this.rate = rate;
		this.formula = formula;
		this.sign = sign;
		this.operationLevel = operationLevel;
		this.chargeAmount = chargeAmount;
		this.subTotal = subTotal;
	}

	public String getChgCode() {
		return chargeCode;
	}

	public void setChgCode(String chgCode) {
		this.chargeCode = chgCode;
	}

	public String getSeq() {
		return seq;
	}

	public void setSeq(String seq) {
		this.seq = seq;
	}

	public String getRate() {
		return rate;
	}

	public void setRate(String rate) {
		this.rate = rate;
	}

	public String getFormula() {
		return formula;
	}

	public void setFormula(String formula) {
		this.formula = formula;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getOperationLevel() {
		return operationLevel;
	}

	public void setOperationLevel(String operationLevel) {
		this.operationLevel = operationLevel;
	}

	public String getChargeAmount() {
		return chargeAmount;
	}

	public void setChargeAmount(String chargeAmount) {
		this.chargeAmount = chargeAmount;
	}

	public String getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(String subTotal) {
		this.subTotal = subTotal;
	}

	@Override
	public String toString() {
		return "FinChargeDetail [chgCode=" + chargeCode + ", seq=" + seq + ", rate=" + rate + ", formula=" + formula
				+ ", sign=" + sign + ", operationLevel=" + operationLevel + ", chargeAmount=" + chargeAmount
				+ ", subTotal=" + subTotal + "]";
	}

}
