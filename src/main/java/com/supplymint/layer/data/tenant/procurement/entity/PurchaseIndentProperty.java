package com.supplymint.layer.data.tenant.procurement.entity;

import java.time.OffsetDateTime;

public class PurchaseIndentProperty {

	public OffsetDateTime createdTime;
	public String pattern;
	
	public PurchaseIndentProperty() {
		super();
	}
	
	public PurchaseIndentProperty(OffsetDateTime createdTime, String pattern) {
		super();
		this.createdTime = createdTime;
		this.pattern = pattern;
	}

	public OffsetDateTime getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(OffsetDateTime createdTime) {
		this.createdTime = createdTime;
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	@Override
	public String toString() {
		return "PurchaseIndentProperty [createdTime=" + createdTime + ", pattern=" + pattern + "]";
	}
	
	
	
}
