package com.supplymint.layer.data.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

/**
 * 
 * 
 * @author Ashish Singh Dev
 * @version 1.0
 * @since 19-Sep-2018
 *
 */
public class TenantAwareRoutingSource extends AbstractRoutingDataSource {

	private final static Logger LOGGER = LoggerFactory.getLogger(TenantAwareRoutingSource.class);

	@Override
	protected Object determineCurrentLookupKey() {
		LOGGER.debug(String.format("Tenant HashKey : %s", ThreadLocalStorage.getTenantHashKey()));
		return ThreadLocalStorage.getTenantHashKey();
	}

}