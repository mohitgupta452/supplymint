package com.supplymint.layer.data.config;

/**
 * 
 * @author Ashish Singh Dev
 * @version 1.0
 * @since Sept 17, 2018
 *
 */
public class ThreadLocalStorage {

	private static ThreadLocal<String> tenantHashKey = new ThreadLocal<>();

	public static void setTenantHashKey(String tenanthashKey) {
		tenantHashKey.set(tenanthashKey);
	}

	public static String getTenantHashKey() {
		return tenantHashKey.get();
	}

	public static void clear() {
		tenantHashKey.set(null);
	  }
	public static void clearTenantHashKey() {
		ThreadLocalStorage.tenantHashKey.remove();
	}

}
