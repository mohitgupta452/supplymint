package com.supplymint.layer.data.config;

import java.util.Map;

public class SaveDbConfig {

	private Map<String, Object> dbConfig;

	public SaveDbConfig() {

	}

	public SaveDbConfig(Map<String, Object> dbConfig) {
		super();
		this.dbConfig = dbConfig;
	}

	public Map<String, Object> getDbConfig() {
		return dbConfig;
	}

	public void setDbConfig(Map<String, Object> dbConfig) {
		this.dbConfig = dbConfig;
	}

	@Override
	public String toString() {
		return "DataSourceConfig [dbConfig=" + dbConfig + "]";
	}

}
