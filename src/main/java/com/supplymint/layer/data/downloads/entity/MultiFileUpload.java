package com.supplymint.layer.data.downloads.entity;

import java.util.List;

public class MultiFileUpload {

	private List<String> file;
	private String enterpriseName;
	private List<String> fileName;
	private String userName;

	public MultiFileUpload() {

	}

	public MultiFileUpload(List<String> file, String enterpriseName, List<String> fileName, String userName) {
		super();
		this.file = file;
		this.enterpriseName = enterpriseName;
		this.fileName = fileName;
		this.userName = userName;
	}

	public List<String> getFile() {
		return file;
	}

	public void setFile(List<String> file) {
		this.file = file;
	}

	public String getEnterpriseName() {
		return enterpriseName;
	}

	public void setEnterpriseName(String enterpriseName) {
		this.enterpriseName = enterpriseName;
	}

	public List<String> getFileName() {
		return fileName;
	}

	public void setFileName(List<String> fileName) {
		this.fileName = fileName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Override
	public String toString() {
		return "FileUpload [file=" + file + ", enterpriseName=" + enterpriseName + ", fileName=" + fileName
				+ ", userName=" + userName + "]";
	}

}
