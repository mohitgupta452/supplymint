package com.supplymint.layer.data.downloads.entity;

public class FileUpload {

	private String file;
	private String enterpriseName;
	private String fileName;
	private String userName;

	public FileUpload() {

	}

	public FileUpload(String file, String enterpriseName, String fileName, String userName) {
		super();
		this.file = file;
		this.enterpriseName = enterpriseName;
		this.fileName = fileName;
		this.userName = userName;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public String getEnterpriseName() {
		return enterpriseName;
	}

	public void setEnterpriseName(String enterpriseName) {
		this.enterpriseName = enterpriseName;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Override
	public String toString() {
		return "FileUpload [file=" + file + ", enterpriseName=" + enterpriseName + ", fileName=" + fileName
				+ ", userName=" + userName + "]";
	}

}
