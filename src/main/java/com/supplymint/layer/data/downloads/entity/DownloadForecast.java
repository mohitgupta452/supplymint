package com.supplymint.layer.data.downloads.entity;

public class DownloadForecast {
	private String ASSORTMENTCODE;
	private String BILLDATE;
	private String QTY_FORECAST;

	public DownloadForecast() {

	}

	public DownloadForecast(String aSSORTMENTCODE, String bILLDATE, String qTY_FORECAST) {
		super();
		ASSORTMENTCODE = aSSORTMENTCODE;
		BILLDATE = bILLDATE;
		QTY_FORECAST = qTY_FORECAST;
	}

	public String getASSORTMENTCODE() {
		return ASSORTMENTCODE;
	}

	public void setASSORTMENTCODE(String aSSORTMENTCODE) {
		ASSORTMENTCODE = aSSORTMENTCODE;
	}

	public String getBILLDATE() {
		return BILLDATE;
	}

	public void setBILLDATE(String bILLDATE) {
		BILLDATE = bILLDATE;
	}

	public String getQTY_FORECAST() {
		return QTY_FORECAST;
	}

	public void setQTY_FORECAST(String qTY_FORECAST) {
		QTY_FORECAST = qTY_FORECAST;
	}

	@Override
	public String toString() {
		return "DownloadForecast [ASSORTMENTCODE=" + ASSORTMENTCODE + ", BILLDATE=" + BILLDATE + ", QTY_FORECAST="
				+ QTY_FORECAST + "]";
	}

}
