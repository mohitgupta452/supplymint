package com.supplymint.layer.data.downloads.entity;

public class DownloadOTBPlanDFD {
	private String ASSORTMENT_CODE;
	private String BILLDATE;
	private String QTY_FORECAST;
	private double PLAN_SALES;
	private double MARK_DOWN;
	private double CLOSING_INVENTORY;
	private double OPENING_STOCK;
	private double OPEN_PURCHASE_ORDER;
	private double OTB_VALUE;

	public DownloadOTBPlanDFD() {

	}

	public DownloadOTBPlanDFD(String aSSORTMENT_CODE, String bILLDATE, String qTY_FORECAST, double pLAN_SALES,
			double mARK_DOWN, double cLOSING_INVENTORY, double oPENING_STOCK, double oPEN_PURCHASE_ORDER,
			double oTB_VALUE) {
		super();
		ASSORTMENT_CODE = aSSORTMENT_CODE;
		BILLDATE = bILLDATE;
		QTY_FORECAST = qTY_FORECAST;
		PLAN_SALES = pLAN_SALES;
		MARK_DOWN = mARK_DOWN;
		CLOSING_INVENTORY = cLOSING_INVENTORY;
		OPENING_STOCK = oPENING_STOCK;
		OPEN_PURCHASE_ORDER = oPEN_PURCHASE_ORDER;
		OTB_VALUE = oTB_VALUE;
	}

	public String getASSORTMENT_CODE() {
		return ASSORTMENT_CODE;
	}

	public void setASSORTMENT_CODE(String aSSORTMENT_CODE) {
		ASSORTMENT_CODE = aSSORTMENT_CODE;
	}

	public String getBILLDATE() {
		return BILLDATE;
	}

	public void setBILLDATE(String bILLDATE) {
		BILLDATE = bILLDATE;
	}

	public String getQTY_FORECAST() {
		return QTY_FORECAST;
	}

	public void setQTY_FORECAST(String qTY_FORECAST) {
		QTY_FORECAST = qTY_FORECAST;
	}

	public double getPLAN_SALES() {
		return PLAN_SALES;
	}

	public void setPLAN_SALES(double pLAN_SALES) {
		PLAN_SALES = pLAN_SALES;
	}

	public double getMARK_DOWN() {
		return MARK_DOWN;
	}

	public void setMARK_DOWN(double mARK_DOWN) {
		MARK_DOWN = mARK_DOWN;
	}

	public double getCLOSING_INVENTORY() {
		return CLOSING_INVENTORY;
	}

	public void setCLOSING_INVENTORY(double cLOSING_INVENTORY) {
		CLOSING_INVENTORY = cLOSING_INVENTORY;
	}

	public double getOPENING_STOCK() {
		return OPENING_STOCK;
	}

	public void setOPENING_STOCK(double oPENING_STOCK) {
		OPENING_STOCK = oPENING_STOCK;
	}

	public double getOPEN_PURCHASE_ORDER() {
		return OPEN_PURCHASE_ORDER;
	}

	public void setOPEN_PURCHASE_ORDER(double oPEN_PURCHASE_ORDER) {
		OPEN_PURCHASE_ORDER = oPEN_PURCHASE_ORDER;
	}

	public double getOTB_VALUE() {
		return OTB_VALUE;
	}

	public void setOTB_VALUE(double oTB_VALUE) {
		OTB_VALUE = oTB_VALUE;
	}

	@Override
	public String toString() {
		return "DownloadOTBPlanDFD [ASSORTMENT_CODE=" + ASSORTMENT_CODE + ", BILLDATE=" + BILLDATE + ", QTY_FORECAST="
				+ QTY_FORECAST + ", PLAN_SALES=" + PLAN_SALES + ", MARK_DOWN=" + MARK_DOWN + ", CLOSING_INVENTORY="
				+ CLOSING_INVENTORY + ", OPENING_STOCK=" + OPENING_STOCK + ", OPEN_PURCHASE_ORDER="
				+ OPEN_PURCHASE_ORDER + ", OTB_VALUE=" + OTB_VALUE + "]";
	}

}
