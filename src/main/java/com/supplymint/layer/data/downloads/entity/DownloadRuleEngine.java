package com.supplymint.layer.data.downloads.entity;

public class DownloadRuleEngine {

	private String UNIQUE_CODE;
	private String WH_CODE;
	private String STORE_CODE;
	private String ITEM_CODE;
	private String AVAILABLE_QTY;
	private String TRANSFER_ORDER_QTY;

	public DownloadRuleEngine() {

	}

	public DownloadRuleEngine(String uNIQUE_CODE, String wH_CODE, String sTORE_CODE, String iTEM_CODE,
			String aVAILABLE_QTY, String tRANSFER_ORDER_QTY) {
		super();
		UNIQUE_CODE = uNIQUE_CODE;
		WH_CODE = wH_CODE;
		STORE_CODE = sTORE_CODE;
		ITEM_CODE = iTEM_CODE;
		AVAILABLE_QTY = aVAILABLE_QTY;
		TRANSFER_ORDER_QTY = tRANSFER_ORDER_QTY;
	}

	public String getUNIQUE_CODE() {
		return UNIQUE_CODE;
	}

	public void setUNIQUE_CODE(String uNIQUE_CODE) {
		UNIQUE_CODE = uNIQUE_CODE;
	}

	public String getWH_CODE() {
		return WH_CODE;
	}

	public void setWH_CODE(String wH_CODE) {
		WH_CODE = wH_CODE;
	}

	public String getSTORE_CODE() {
		return STORE_CODE;
	}

	public void setSTORE_CODE(String sTORE_CODE) {
		STORE_CODE = sTORE_CODE;
	}

	public String getITEM_CODE() {
		return ITEM_CODE;
	}

	public void setITEM_CODE(String iTEM_CODE) {
		ITEM_CODE = iTEM_CODE;
	}

	public String getAVAILABLE_QTY() {
		return AVAILABLE_QTY;
	}

	public void setAVAILABLE_QTY(String aVAILABLE_QTY) {
		AVAILABLE_QTY = aVAILABLE_QTY;
	}

	public String getTRANSFER_ORDER_QTY() {
		return TRANSFER_ORDER_QTY;
	}

	public void setTRANSFER_ORDER_QTY(String tRANSFER_ORDER_QTY) {
		TRANSFER_ORDER_QTY = tRANSFER_ORDER_QTY;
	}

	@Override
	public String toString() {
		return "DownloadRuleEngine [UNIQUE_CODE=" + UNIQUE_CODE + ", WH_CODE=" + WH_CODE + ", STORE_CODE=" + STORE_CODE
				+ ", ITEM_CODE=" + ITEM_CODE + ", AVAILABLE_QTY=" + AVAILABLE_QTY + ", TRANSFER_ORDER_QTY="
				+ TRANSFER_ORDER_QTY + "]";
	}

}
