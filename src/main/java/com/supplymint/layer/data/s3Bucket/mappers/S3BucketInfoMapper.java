package com.supplymint.layer.data.s3Bucket.mappers;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.supplymint.layer.data.tenant.inventory.entity.S3BucketInfo;

/**
 * @author Manoj Singh
 * @author Prabhakar Srivastava
 * @since 25 OCT 2018
 * @version 1.0
 */
@Mapper
public interface S3BucketInfoMapper {

	/**
	 * This method describes get Bucket Info
	 * 
	 * @param Bucket Name
	 * @Table S3BUCKETINFO
	 */
	List<S3BucketInfo> getBucketByName(@Param("bucketName") String bucketName);

	/**
	 * This method describes get Bucket Status
	 * 
	 * @param Status
	 * @Table S3BUCKETINFO
	 */
	List<S3BucketInfo> getBucketStatus(@Param("status") String status);

	/**
	 * This method describes get all Bucket Info
	 * 
	 * @Table S3BUCKETINFO
	 */
	List<S3BucketInfo> getAllBucket();

	/**
	 * This method describes create S3Bucket Entry For AWS Configuration
	 * 
	 * @param S3BucketInfo
	 * @Table S3BUCKETINFO
	 */
	Integer s3InsertFileName(S3BucketInfo s3BucketInfo);

	/**
	 * This method describes get All File Name
	 * 
	 * @Table S3BUCKETINFO
	 */
	List<S3BucketInfo> getAllFileName();

	/**
	 * This method describes update fileName
	 * 
	 * @param S3BucketInfo
	 * @Table S3BUCKETINFO
	 */
	Integer s3UpdateFileName(S3BucketInfo s3BucketInfo);

	/**
	 * This method describes update S3Bucket Information For AWS Configuration
	 * 
	 * @param Bucket Name , Type
	 * @Table S3BUCKETINFO
	 */
	S3BucketInfo getBucketByType(@Param("bucketName") String bucketName, @Param("type") String type,
			@Param("orgId") String orgId);

	/**
	 * This method describes update S3Bucket Information For AWS Configuration
	 * 
	 * @param Type
	 * @Table S3BUCKETINFO
	 */
	S3BucketInfo getBucketType(@Param("type") String type, @Param("orgId") String orgId);

	/**
	 * This method describes update S3Bucket Information For AWS Configuration
	 * 
	 * @param Type
	 * @Table S3BUCKETINFO
	 */
	List<S3BucketInfo> getBucketTypes(@Param("type") String type);

	/**
	 * This method describes check Unique Output File Name
	 * 
	 * @param Output File Name
	 * @Table S3BUCKETINFO
	 */
	@Select("SELECT OUTPUTFILENAME as outputFileName FROM S3BUCKETINFO where OUTPUTFILENAME=#{outputFileName}")
	List<S3BucketInfo> isExistS3FileName(@Param("outputFileName") String outputFileName);

	/**
	 * This method describes insert Bucket Info
	 * 
	 * @param S3BucketInfo
	 * @Table S3BUCKETINFO
	 */
	Integer insertBucketName(S3BucketInfo s3BucketInfo);

	/**
	 * This method describes update Bucket Info By Bucket Name
	 * 
	 * @param S3BucketInfo
	 * @Table S3BUCKETINFO
	 */
	Integer updateBucketName(S3BucketInfo s3BucketInfo);

	/**
	 * This method describes check Unique Bucket Name
	 * 
	 * @param Bucket Name
	 * @Table S3BUCKETINFO
	 */
	@Select("SELECT S3BUCKETNAME as bucketName FROM S3BUCKETINFO where S3BUCKETNAME=#{bucketName}")
	List<S3BucketInfo> isExistBucketName(@Param("bucketName") String bucketName);

	/**
	 * This method describes get Bucket Info
	 * 
	 * @param S3BucketInfo
	 * @Table S3BUCKETINFO
	 */
	List<S3BucketInfo> getByURL(@Param("url") String url);

	/**
	 * This method describes update Bucket Info By Bucket Type
	 * 
	 * @param S3BucketInfo
	 * @Table S3BUCKETINFO
	 */
	int updateBucketByType(S3BucketInfo s3BucketInfo);

	/**
	 * This method describes update Profile Image path
	 * 
	 * @param Path
	 * @Table S3BUCKETINFO
	 */
	int updateBucketByProfile(@Param("path") String path);

	@Update("UPDATE S3BUCKETINFO SET MANUAL_TRANSFER_URL= #{manualTransferUrl, jdbcType=VARCHAR} WHERE TYPE=#{bucketType, jdbcType=VARCHAR}")
	int updateManualTransferOrder(@Param("manualTransferUrl") String manualTransferUrl,
			@Param("bucketType") String bucketType);

}
