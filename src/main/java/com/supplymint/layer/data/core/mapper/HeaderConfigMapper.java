package com.supplymint.layer.data.core.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.supplymint.layer.data.core.entity.HeaderConfig;
import com.supplymint.layer.data.tenant.inventory.entity.HeaderConfigLog;

@Mapper
public interface HeaderConfigMapper {

	int create(HeaderConfig headerConfiguration);

	int createHeaderConfigLogs(HeaderConfigLog headerConfigLogs);

	@Select("SELECT FIXED_HEADER FROM HEADER_CONFIG WHERE ACTIVE ='1' AND ENTERPRISE_NAME=#{enterpriseName}  AND attribute_type=#{attributeType} AND DISPLAY_NAME=#{displayName}")
	String getFixedHeaders(@Param("enterpriseName") String enterpriseName, @Param("attributeType") String attributeType,
			@Param("displayName") String displayName);

	@Select("SELECT DEFAULT_HEADER FROM HEADER_CONFIG WHERE ACTIVE ='1' AND ENTERPRISE_NAME=#{enterpriseName}  AND attribute_type=#{attributeType} AND DISPLAY_NAME=#{displayName} ")
	String getDefaultHeaders(@Param("enterpriseName") String enterpriseName,
			@Param("attributeType") String attributeType, @Param("displayName") String displayName);

	@Update("UPDATE HEADER_CONFIG SET STATUS='FALSE' WHERE ID!=#{uuid} And ENTERPRISE_NAME = #{enterpriseName}")
	int updateStatus(@Param("uuid") String uuid, @Param("enterpriseName") String enterpriseName);

	@Update("UPDATE HEADER_CONFIG_LOG SET STATUS='INACTIVE',ACTIVE ='0' WHERE ID = #{id}")
	int updateHeaderLogsStatus(@Param("id") String id);

	@Select("SELECT VALUE FROM system_default_config WHERE KEY='DEFAULT_HEADER' ")
	String getDHeaders();

	@Select("SELECT CUSTOM_HEADER FROM HEADER_CONFIG WHERE ACTIVE ='1' AND ENTERPRISE_NAME=#{enterpriseName}  AND attribute_type=#{attributeType} AND DISPLAY_NAME=#{displayName}  ")
	String getCHeaders(@Param("enterpriseName") String enterpriseName, @Param("attributeType") String attributeType,
			@Param("displayName") String displayName);

	@Select("SELECT CUSTOM_HEADER  FROM HEADER_CONFIG_LOG ORDER BY CREATEDTIME DESC FETCH FIRST ROW ONLY")
	String getCustomHeaders(String displayName);

	@Update("UPDATE HEADER_CONFIG SET STATUS='FALSE',ACTIVE='0' WHERE ID!=#{uuid} And ENTERPRISE_NAME = #{enterpriseName} And DISPLAY_NAME = #{displayName}")
	void updateStatusMethodTemporary(@Param("uuid") String uuid, @Param("enterpriseName") String enterpriseName,
			@Param("displayName") String displayName);

	int createMethodTemporary(HeaderConfig headerConfiguration);

	int createMethodTemporaryTest(HeaderConfig headerConfiguration);

	@Select("SELECT FIXED_HEADER FROM HEADER_CONFIG WHERE ACTIVE ='1' AND DISPLAY_NAME=#{displayName}")
	String getFHeaders(String displayName);
}
