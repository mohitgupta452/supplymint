package com.supplymint.layer.data.core.entity;

import java.util.Date;

public class InventoryAutoConfig {

	private int eid;
	private String enterpriseName;
	private String orgId;
	private String orgName;
	private String jobName;
	private String triggerName;
	private String host;
	private String bucketName;
	private String bucketPath;
	private String bucketKey;
	private String acceessKeyId;
	private String secureAccessKeyId;
	private String outputFileName;
	private String url;
	private String time;
	private String type;
	private String createZip;
	private String manualTransferPath;
	private String tenantHashKey;
	private String isManualTransferUrl;
	private char active;
	private String status;
	private String createdBy;
	private Date createdTime;
	private String updatedBy;
	private Date updationTime;
	private String additional;

	public InventoryAutoConfig() {
		// TODO Auto-generated constructor stub
	}

	public InventoryAutoConfig(int eid, String enterpriseName, String orgId, String orgName, String jobName,
			String triggerName, String host, String bucketName, String bucketPath, String bucketKey,
			String acceessKeyId, String secureAccessKeyId, String outputFileName, String url, String time, String type,
			String createZip, String manualTransferPath, String tenantHashKey, String isManualTransferUrl, char active,
			String status, String createdBy, Date createdTime, String updatedBy, Date updationTime, String additional) {
		super();
		this.eid = eid;
		this.enterpriseName = enterpriseName;
		this.orgId = orgId;
		this.orgName = orgName;
		this.jobName = jobName;
		this.triggerName = triggerName;
		this.host = host;
		this.bucketName = bucketName;
		this.bucketPath = bucketPath;
		this.bucketKey = bucketKey;
		this.acceessKeyId = acceessKeyId;
		this.secureAccessKeyId = secureAccessKeyId;
		this.outputFileName = outputFileName;
		this.url = url;
		this.time = time;
		this.type = type;
		this.createZip = createZip;
		this.manualTransferPath = manualTransferPath;
		this.tenantHashKey = tenantHashKey;
		this.isManualTransferUrl = isManualTransferUrl;
		this.active = active;
		this.status = status;
		this.createdBy = createdBy;
		this.createdTime = createdTime;
		this.updatedBy = updatedBy;
		this.updationTime = updationTime;
		this.additional = additional;
	}

	public int getEid() {
		return eid;
	}

	public void setEid(int eid) {
		this.eid = eid;
	}

	public String getEnterpriseName() {
		return enterpriseName;
	}

	public void setEnterpriseName(String enterpriseName) {
		this.enterpriseName = enterpriseName;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getTriggerName() {
		return triggerName;
	}

	public void setTriggerName(String triggerName) {
		this.triggerName = triggerName;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getBucketName() {
		return bucketName;
	}

	public void setBucketName(String bucketName) {
		this.bucketName = bucketName;
	}

	public String getBucketPath() {
		return bucketPath;
	}

	public void setBucketPath(String bucketPath) {
		this.bucketPath = bucketPath;
	}

	public String getBucketKey() {
		return bucketKey;
	}

	public void setBucketKey(String bucketKey) {
		this.bucketKey = bucketKey;
	}

	public String getAcceessKeyId() {
		return acceessKeyId;
	}

	public void setAcceessKeyId(String acceessKeyId) {
		this.acceessKeyId = acceessKeyId;
	}

	public String getSecureAccessKeyId() {
		return secureAccessKeyId;
	}

	public void setSecureAccessKeyId(String secureAccessKeyId) {
		this.secureAccessKeyId = secureAccessKeyId;
	}

	public String getOutputFileName() {
		return outputFileName;
	}

	public void setOutputFileName(String outputFileName) {
		this.outputFileName = outputFileName;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCreateZip() {
		return createZip;
	}

	public void setCreateZip(String createZip) {
		this.createZip = createZip;
	}

	public String getManualTransferPath() {
		return manualTransferPath;
	}

	public void setManualTransferPath(String manualTransferPath) {
		this.manualTransferPath = manualTransferPath;
	}

	public String getTenantHashKey() {
		return tenantHashKey;
	}

	public void setTenantHashKey(String tenantHashKey) {
		this.tenantHashKey = tenantHashKey;
	}

	public String getIsManualTransferUrl() {
		return isManualTransferUrl;
	}

	public void setIsManualTransferUrl(String isManualTransferUrl) {
		this.isManualTransferUrl = isManualTransferUrl;
	}

	public char getActive() {
		return active;
	}

	public void setActive(char active) {
		this.active = active;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdationTime() {
		return updationTime;
	}

	public void setUpdationTime(Date updationTime) {
		this.updationTime = updationTime;
	}

	public String getAdditional() {
		return additional;
	}

	public void setAdditional(String additional) {
		this.additional = additional;
	}

	@Override
	public String toString() {
		return "InventoryAutoConfig [eid=" + eid + ", enterpriseName=" + enterpriseName + ", orgId=" + orgId
				+ ", orgName=" + orgName + ", jobName=" + jobName + ", triggerName=" + triggerName + ", host=" + host
				+ ", bucketName=" + bucketName + ", bucketPath=" + bucketPath + ", bucketKey=" + bucketKey
				+ ", acceessKeyId=" + acceessKeyId + ", secureAccessKeyId=" + secureAccessKeyId + ", outputFileName="
				+ outputFileName + ", url=" + url + ", time=" + time + ", type=" + type + ", createZip=" + createZip
				+ ", manualTransferPath=" + manualTransferPath + ", tenantHashKey=" + tenantHashKey
				+ ", isManualTransferUrl=" + isManualTransferUrl + ", active=" + active + ", status=" + status
				+ ", createdBy=" + createdBy + ", createdTime=" + createdTime + ", updatedBy=" + updatedBy
				+ ", updationTime=" + updationTime + ", additional=" + additional + "]";
	}

}
