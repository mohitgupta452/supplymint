package com.supplymint.layer.data.core.entity;

import java.time.OffsetDateTime;

public class Role {

	private int id;
	private String code;
	private String name;
	private String description;
	private OffsetDateTime creationTime;
	private OffsetDateTime updationTime;
	private int active;

	public Role() {

	}

	public Role(int id, String code, String name, String description, OffsetDateTime creationTime,
			OffsetDateTime updationTime, int active) {
		super();
		this.id = id;
		this.code = code;
		this.name = name;
		this.description = description;
		this.creationTime = creationTime;
		this.updationTime = updationTime;
		this.active = active;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public OffsetDateTime getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(OffsetDateTime creationTime) {
		this.creationTime = creationTime;
	}

	public OffsetDateTime getUpdationTime() {
		return updationTime;
	}

	public void setUpdationTime(OffsetDateTime updationTime) {
		this.updationTime = updationTime;
	}

	public int getActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}

	@Override
	public String toString() {
		return "Role [id=" + id + ", code=" + code + ", name=" + name + ", description=" + description
				+ ", creationTime=" + creationTime + ", updationTime=" + updationTime + ", active=" + active + "]";
	}

}
