package com.supplymint.layer.data.core.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.supplymint.layer.data.core.entity.Role;

@Mapper
public interface RoleMapper {
	
	Role findById(Integer id);

	List<Role> findAll();

}
