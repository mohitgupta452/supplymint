package com.supplymint.layer.data.core.entity;

/**
 * 
 * 
 * @author Ashish Singh Dev
 * @version 1.0
 * @since 19-Sep-2018
 *
 */
public class Tenant {

	private Integer id;
	private String uuid;
	private String name;
	private String config;

	public Tenant() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getConfig() {
		return config;
	}

	public void setConfig(String config) {
		this.config = config;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Tenant)) {
			return false;
		}
		Tenant tenant = (Tenant) obj;

		return this.getId().equals(tenant.getId()) && this.getUuid().equals(tenant.getUuid());
	}

	@Override
	public int hashCode() {
		int result = 17;
		result = 31 * result + id;
		result = 31 * result + uuid.hashCode();
		return result;
	}

	@Override
	public String toString() {
		return String.format("Tenant Id: %s, Tenant Uid: %s, Tenant Name: %s", this.id, this.uuid, this.name);
	}

}
