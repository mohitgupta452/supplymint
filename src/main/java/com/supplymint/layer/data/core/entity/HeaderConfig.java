package com.supplymint.layer.data.core.entity;

import java.util.Date;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.supplymint.util.DateFormatSerializer;

public class HeaderConfig {

	private String id;
	private String enterpriseId;
	private String enterpriseName;
	private String module;
	private String subModule;
	private String section;
	private String source;
	private String typeConfig;
	private String displayName;
	private String attributeType;
	private String fixedHeader;
	private String defaultHeader;
	private String active;
	private String status;
	private String ipAddress;
	private String createdBy;
	@JsonSerialize(using = DateFormatSerializer.class)
	private Date createdTime;
	private String updatedBy;
	@JsonSerialize(using = DateFormatSerializer.class)
	private Date updationTime;
	private String additional;
	private JsonNode fixedHeaders;
	private JsonNode defaultHeaders;
	private String token;
	private JsonNode customHeaders;
	private String customHeader;

	public HeaderConfig() {
		super();
	}

	public HeaderConfig(String id, String enterpriseId, String enterpriseName, String module, String subModule,
			String section, String source, String typeConfig, String displayName, String attributeType,
			String fixedHeader, String defaultHeader, String active, String status, String ipAddress, String createdBy,
			Date createdTime, String updatedBy, Date updationTime, String additional, JsonNode fixedHeaders,
			JsonNode defaultHeaders, String token, JsonNode customHeaders, String customHeader) {
		super();
		this.id = id;
		this.enterpriseId = enterpriseId;
		this.enterpriseName = enterpriseName;
		this.module = module;
		this.subModule = subModule;
		this.section = section;
		this.source = source;
		this.typeConfig = typeConfig;
		this.displayName = displayName;
		this.attributeType = attributeType;
		this.fixedHeader = fixedHeader;
		this.defaultHeader = defaultHeader;
		this.active = active;
		this.status = status;
		this.ipAddress = ipAddress;
		this.createdBy = createdBy;
		this.createdTime = createdTime;
		this.updatedBy = updatedBy;
		this.updationTime = updationTime;
		this.additional = additional;
		this.fixedHeaders = fixedHeaders;
		this.defaultHeaders = defaultHeaders;
		this.token = token;
		this.customHeaders = customHeaders;
		this.customHeader = customHeader;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEnterpriseId() {
		return enterpriseId;
	}

	public void setEnterpriseId(String enterpriseId) {
		this.enterpriseId = enterpriseId;
	}

	public String getEnterpriseName() {
		return enterpriseName;
	}

	public void setEnterpriseName(String enterpriseName) {
		this.enterpriseName = enterpriseName;
	}

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public String getSubModule() {
		return subModule;
	}

	public void setSubModule(String subModule) {
		this.subModule = subModule;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getTypeConfig() {
		return typeConfig;
	}

	public void setTypeConfig(String typeConfig) {
		this.typeConfig = typeConfig;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getAttributeType() {
		return attributeType;
	}

	public void setAttributeType(String attributeType) {
		this.attributeType = attributeType;
	}

	public String getFixedHeader() {
		return fixedHeader;
	}

	public void setFixedHeader(String fixedHeader) {
		this.fixedHeader = fixedHeader;
	}

	public String getDefaultHeader() {
		return defaultHeader;
	}

	public void setDefaultHeader(String defaultHeader) {
		this.defaultHeader = defaultHeader;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdationTime() {
		return updationTime;
	}

	public void setUpdationTime(Date updationTime) {
		this.updationTime = updationTime;
	}

	public String getAdditional() {
		return additional;
	}

	public void setAdditional(String additional) {
		this.additional = additional;
	}

	public JsonNode getFixedHeaders() {
		return fixedHeaders;
	}

	public void setFixedHeaders(JsonNode fixedHeaders) {
		this.fixedHeaders = fixedHeaders;
	}

	public JsonNode getDefaultHeaders() {
		return defaultHeaders;
	}

	public void setDefaultHeaders(JsonNode defaultHeaders) {
		this.defaultHeaders = defaultHeaders;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public JsonNode getCustomHeaders() {
		return customHeaders;
	}

	public void setCustomHeaders(JsonNode customHeaders) {
		this.customHeaders = customHeaders;
	}

	public String getCustomHeader() {
		return customHeader;
	}

	public void setCustomHeader(String customHeader) {
		this.customHeader = customHeader;
	}

	@Override
	public String toString() {
		return "HeaderConfig [id=" + id + ", enterpriseId=" + enterpriseId + ", enterpriseName=" + enterpriseName
				+ ", module=" + module + ", subModule=" + subModule + ", section=" + section + ", source=" + source
				+ ", typeConfig=" + typeConfig + ", displayName=" + displayName + ", attributeType=" + attributeType
				+ ", fixedHeader=" + fixedHeader + ", defaultHeader=" + defaultHeader + ", active=" + active
				+ ", status=" + status + ", ipAddress=" + ipAddress + ", createdBy=" + createdBy + ", createdTime="
				+ createdTime + ", updatedBy=" + updatedBy + ", updationTime=" + updationTime + ", additional="
				+ additional + ", fixedHeaders=" + fixedHeaders + ", defaultHeaders=" + defaultHeaders + ", token="
				+ token + ", customHeaders=" + customHeaders + ", customHeader=" + customHeader + "]";
	}

	

}