package com.supplymint.layer.data.core.entity;

/**
 * 
 * 
 * @author Ashish Singh Dev
 * @version 1.0
 * @since 19-Sep-2018
 *
 */
public class Proxy {

	private Integer id;

	private Integer identifier;

	private Integer module;

	private String data;

	public Proxy() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getIdentifier() {
		return identifier;
	}

	public void setIdentifier(Integer identifier) {
		this.identifier = identifier;
	}

	public Integer getModule() {
		return module;
	}

	public void setModule(Integer module) {
		this.module = module;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

}
