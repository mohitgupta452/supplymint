package com.supplymint.layer.data.core.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.supplymint.layer.data.core.entity.Users;

@Mapper
public interface UserMapper {
	
	int updateUser(Users user);
	
	List getAllUsers();
	
	Users findById(int eid);

}
