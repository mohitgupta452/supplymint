/**
 * 
 */
package com.supplymint.layer.data.core.entity;

import java.io.Serializable;

/**
 * @author manoj
 *
 */
public class Users implements Serializable {

	private static final long serialVersionUID = -3759931270846789162L;
	private String firstName;
	private String lastName;
	private String username;
	private String email;
	private int eid;
	private String accessMode = "WEB";
	private String roles;
	private String mobileNumber;
	private int active;

	public int getActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getEid() {
		return eid;
	}

	public void setEid(int eid) {
		this.eid = eid;
	}

	public String getRoles() {
		return roles;
	}

	public void setRoles(String roles) {
		this.roles = roles;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getAccessMode() {
		return accessMode;
	}

	public void setAccessMode(String accessMode) {
		this.accessMode = accessMode;
	}

	@Override
	public String toString() {
		return "Users [firstName=" + firstName + ", lastName=" + lastName + ", username=" + username + ", email="
				+ email + ", eid=" + eid + ", accessMode=" + accessMode + ", roles=" + roles + ", mobileNumber="
				+ mobileNumber + ", active=" + active + "]";
	}

}
