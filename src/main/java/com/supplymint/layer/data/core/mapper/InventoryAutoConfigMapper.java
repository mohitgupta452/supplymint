package com.supplymint.layer.data.core.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.supplymint.layer.data.core.entity.InventoryAutoConfig;

@Mapper
public interface InventoryAutoConfigMapper {
	
	List<InventoryAutoConfig> getAll();

}
