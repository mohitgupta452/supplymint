package com.supplymint.layer.data.core.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

import com.supplymint.layer.data.core.entity.Tenant;

/**
 * 
 * 
 * @author Ashish Singh Dev
 * @version 1.0
 * @since 19-Sep-2018
 *
 */
@Mapper
public interface TenantMapper {

	@Select("SELECT * FROM ENTERPRISE WHERE UUID = #{UUID}")
	Tenant findByUuid(String uuid);

	@Select("SELECT * FROM ENTERPRISE WHERE ID = #{ID}")
	Tenant findById(Integer id);

	@Insert("INSERT into ENTERPRISE (UUID,NAME,CONFIG) values (#{uuid},#{name},#{config})")
	@Options(useGeneratedKeys = true, keyColumn = "ID", keyProperty = "id")
	// @SelectKey(statement = "SELECT LAST_INSERTED_ID()", before = false,
	// resultType = Integer.class, keyColumn = "ID", keyProperty = "id")
	Integer create(Tenant t);

	@Select("SELECT * FROM ENTERPRISE")
	List<Tenant> findAll();
}
