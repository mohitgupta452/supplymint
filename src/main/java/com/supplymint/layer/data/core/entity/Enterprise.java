package com.supplymint.layer.data.core.entity;

import java.time.OffsetDateTime;

public class Enterprise {

	private int id;
	private String code;
	private String name;
	private String description;
	private String domain;
	private OffsetDateTime creationTime;
	private OffsetDateTime updationTime;
	private String uuId;
	private String config;
	private String partner;
	private String gstin;

	private int active;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public OffsetDateTime getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(OffsetDateTime creationTime) {
		this.creationTime = creationTime;
	}

	public OffsetDateTime getUpdationTime() {
		return updationTime;
	}

	public void setUpdationTime(OffsetDateTime updationTime) {
		this.updationTime = updationTime;
	}

	public String getUuId() {
		return uuId;
	}

	public void setUuId(String uuId) {
		this.uuId = uuId;
	}

	public String getConfig() {
		return config;
	}

	public void setConfig(String config) {
		this.config = config;
	}

	public int getActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}

	public Enterprise() {
		// TODO Auto-generated constructor stub
	}

	public String getPartner() {
		return partner;
	}

	public void setPartner(String partner) {
		this.partner = partner;
	}

	public String getGstin() {
		return gstin;
	}

	public void setGstin(String gstin) {
		this.gstin = gstin;
	}

	public Enterprise(int id, String code, String name, String description, String domain, OffsetDateTime creationTime,
			OffsetDateTime updationTime, String uuId, String config, int active) {
		super();
		this.id = id;
		this.code = code;
		this.name = name;
		this.description = description;
		this.domain = domain;
		this.creationTime = creationTime;
		this.updationTime = updationTime;
		this.uuId = uuId;
		this.config = config;
		this.active = active;
	}

	@Override
	public String toString() {
		return "Enterprise [id=" + id + ", code=" + code + ", name=" + name + ", description=" + description
				+ ", domain=" + domain + ", creationTime=" + creationTime + ", updationTime=" + updationTime + ", uuId="
				+ uuId + ", config=" + config + ", active=" + active + "]";
	}

}
