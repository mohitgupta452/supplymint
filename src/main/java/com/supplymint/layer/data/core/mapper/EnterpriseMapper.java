package com.supplymint.layer.data.core.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.supplymint.layer.data.core.entity.Enterprise;

@Mapper
public interface EnterpriseMapper {
	
	Enterprise findById(Integer id);
	

}
