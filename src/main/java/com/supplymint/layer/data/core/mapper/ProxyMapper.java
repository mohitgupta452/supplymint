package com.supplymint.layer.data.core.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.supplymint.layer.data.core.entity.Proxy;

/**
 * 
 * 
 * @author Ashish Singh Dev
 * @version 1.0
 * @since 19-Sep-2018
 *
 */
@Mapper
public interface ProxyMapper {

	@Select("SELECT * FROM PROXY WHERE IDENTIFIER = #{ID} AND MODULE= #{MODULE}")
	Proxy findByIdAndModule(@Param("ID") Integer id, @Param("MODULE") Integer module);
}
