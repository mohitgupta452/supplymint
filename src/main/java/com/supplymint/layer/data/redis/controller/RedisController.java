/**
 * 
 */
package com.supplymint.layer.data.redis.controller;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.supplymint.layer.data.redis.model.User;
import com.supplymint.util.CodeUtils;

/**
 * @author Manoj Singh
 * @version 1.0
 * @since 14-Oct-2018
 *
 */

@RestController
@RequestMapping("/redis")
public class RedisController {

	@Autowired
	private RedisTemplate<String, Object> redisTemplate;

	@Autowired
	private ObjectMapper objectMapper;

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	private User add(@RequestBody User user) throws JsonProcessingException {
		redisTemplate.opsForValue().set("key", objectMapper.writeValueAsString(user));
		return user;

	}

	@RequestMapping(value = "/add/key", method = RequestMethod.POST)
	private String addByKey(@RequestBody String key) throws IOException {
		JsonNode rootNode = objectMapper.readTree(key);
		String _Key = null, value = null;
		Iterator<Map.Entry<String, JsonNode>> iter = rootNode.fields();
		while (iter.hasNext()) {
			Map.Entry<String, JsonNode> entry = iter.next();
			_Key = entry.getKey().toString();
			value = entry.getValue().toString();
		}
//		redisConfig.redisTemplate().opsForValue().set(_Key, objectMapper.writeValueAsString(value));
		redisTemplate.opsForValue().set(_Key, objectMapper.writeValueAsString(value));
		return key;
	}

	@RequestMapping(value = "/get/{key}", method = RequestMethod.GET)
	private String get(@PathVariable String key) throws JsonProcessingException {
		return CodeUtils.convertObjectToJSon(redisTemplate.opsForValue().get(key));

	}

	@RequestMapping(value = "/getall", method = RequestMethod.GET)
	private Set<String> getAll() throws JsonProcessingException {
		return redisTemplate.keys("*");

	}

}
