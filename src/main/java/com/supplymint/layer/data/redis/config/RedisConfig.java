package com.supplymint.layer.data.redis.config;

import java.io.IOException;
import java.time.Duration;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisClientConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericToStringSerializer;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.internal.StaticCredentialsProvider;
import com.amazonaws.services.elasticache.AmazonElastiCache;
import com.amazonaws.services.elasticache.AmazonElastiCacheClientBuilder;
import com.amazonaws.services.elasticache.model.CacheCluster;
import com.amazonaws.services.elasticache.model.CacheNode;
import com.amazonaws.services.elasticache.model.DescribeCacheClustersRequest;
import com.amazonaws.services.elasticache.model.DescribeCacheClustersResult;
import com.fasterxml.jackson.databind.ObjectMapper;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * @author Manoj Singh
 *
 */

@Configuration
@ConfigurationProperties("spring.redis")
@PropertySource("classpath:aws.properties")
@EnableCaching
public class RedisConfig {

	private final static Logger logger = LoggerFactory.getLogger(RedisConfig.class);

	private String host;

	private int port;

	private String region = "aws.elasticCache.region";

	private String access_key_id = "aws.credentails.access_key_id";

	private String secret_access_key = "aws.credentails.secret_access_key";

	private String redisPrefix = "ElasticCache";

	@Autowired
	private Environment env;

	public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}

//	@Bean
//	public JedisConnectionFactory jedisConnectionFactory() {
//		RedisStandaloneConfiguration redisStandaloneConfiguration = new RedisStandaloneConfiguration(host, port);
//		return new JedisConnectionFactory(redisStandaloneConfiguration);
//	}

	@SuppressWarnings("deprecation")
	@Bean
	JedisConnectionFactory jedisConnectionFactory() {
		JedisConnectionFactory jedisConFactory = new JedisConnectionFactory();
		jedisConFactory.setHostName(host);
		jedisConFactory.setPort(6379);
		jedisConFactory.setUsePool(false);
		return jedisConFactory;
	}

	@Bean(value = "redisTemplate")
	public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory) {
		RedisTemplate<String, Object> redisTemplate = new RedisTemplate<>();
		redisTemplate.setConnectionFactory(jedisConnectionFactory());
		return redisTemplate;
	}
//
//	@Primary
//	@Bean(name = "cacheManager") // Default cache manager is infinite
//	public CacheManager cacheManager(RedisConnectionFactory redisConnectionFactory) {
//		return RedisCacheManager.builder(redisConnectionFactory)
//				.cacheDefaults(RedisCacheConfiguration.defaultCacheConfig().prefixKeysWith(redisPrefix)).build();
//	}
//
//	@Bean(name = "cacheManager1Hour")
//	public CacheManager cacheManager1Hour(RedisConnectionFactory redisConnectionFactory) {
//		Duration expiration = Duration.ofHours(1);
//		return RedisCacheManager.builder(redisConnectionFactory)
//				.cacheDefaults(
//						RedisCacheConfiguration.defaultCacheConfig().prefixKeysWith(redisPrefix).entryTtl(expiration))
//				.build();
//	}

//	@Bean
//	public RedisConnectionFactory jedisConnectionFactory() {
//		RedisStandaloneConfiguration configuration = new RedisStandaloneConfiguration(host, port);
//		JedisClientConfiguration jedisClientConfiguration = JedisClientConfiguration.builder().build();
//		JedisConnectionFactory factory = new JedisConnectionFactory(configuration, jedisClientConfiguration);
//		factory.getPoolConfig().setMaxIdle(30);
//		factory.getPoolConfig().setMinIdle(10);
//		factory.afterPropertiesSet();
//		return factory;
//	}
//
//	@Bean
//	public RedisTemplate<String, Object> redisTemplate() {
//		final RedisTemplate<String, Object> redisTemplate = new RedisTemplate<String, Object>();
//		redisTemplate.setKeySerializer(new StringRedisSerializer());
//		redisTemplate.setHashKeySerializer(new GenericToStringSerializer<Object>(Object.class));
//		redisTemplate.setHashValueSerializer(new JdkSerializationRedisSerializer());
//		redisTemplate.setValueSerializer(new JdkSerializationRedisSerializer());
//		redisTemplate.setConnectionFactory(jedisConnectionFactory());
//		redisTemplate.setEnableTransactionSupport(true);
//		return redisTemplate;
//	}

//	public AmazonElastiCache awsElasticCacheClientWithCredentials() {
//		logger.debug(String.format("Amazon ElasticCacheClient for region %s", env.getProperty(region)));
//		return AmazonElastiCacheClientBuilder.standard().withCredentials(getCredentialsProvider())
//				.withRegion(env.getProperty(region)).build();
//
//	}
//
//	@SuppressWarnings("deprecation")
//	private AWSCredentialsProvider getCredentialsProvider() {
//		AWSCredentialsProvider credentialsProvider;
//		if (!env.getProperty(access_key_id).isEmpty() && !env.getProperty(secret_access_key).isEmpty()) {
//			credentialsProvider = new StaticCredentialsProvider(
//					new BasicAWSCredentials(env.getProperty(access_key_id), env.getProperty(secret_access_key)));
//		} else {
//			credentialsProvider = new DefaultAWSCredentialsProviderChain();
//		}
//		return credentialsProvider;
//	}
//
//	public void JedisConnection() throws IOException {
//		DescribeCacheClustersRequest dccRequest = new DescribeCacheClustersRequest();
//		dccRequest.setShowCacheNodeInfo(true);
//
//		DescribeCacheClustersResult clusterResult = awsElasticCacheClientWithCredentials()
//				.describeCacheClusters(dccRequest);
//		List<CacheCluster> cacheClusters = clusterResult.getCacheClusters();
//		for (CacheCluster cacheCluster : cacheClusters) {
//			for (CacheNode cacheNode : cacheCluster.getCacheNodes()) {
//				String host = cacheNode.getEndpoint().getAddress();
//				int port = cacheNode.getEndpoint().getPort();
//				@SuppressWarnings("resource")
//				Jedis jedis = new Jedis(host, port);
//				logger.debug(String.format("Connection to server sucessfully : %s", jedis.isConnected()));
//			}
//		}
//	}

//
//	@SuppressWarnings("deprecation")
//	@Bean
//	JedisConnectionFactory jedisConnectionFactory() {
//		JedisConnectionFactory jedisConFactory = new JedisConnectionFactory();
//		jedisConFactory.setHostName(host);
//		jedisConFactory.setPort(port);
//		jedisConFactory.setUsePool(true);
//		return jedisConFactory;
//	}
//
////
////	@Bean
//	public JedisPoolConfig poolConfig() {
//		JedisPoolConfig config = new JedisPoolConfig();
//		config.setMaxTotal(100);
//		config.setMaxIdle(200);
//		config.setMinIdle(50);
//		config.setMaxWaitMillis(30000);
////		config.setTestOnBorrow(true);
//		config.setBlockWhenExhausted(true);
//		JedisPool jedisPool = new JedisPool();
//		jedisPool = new JedisPool(config, host, port, 300000);
//		return config;
//	}
////
//	@Bean(value = "redisTemplate")
//	public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory) {
//		RedisTemplate<String, Object> redisTemplate = new RedisTemplate<>();
//		redisTemplate.setConnectionFactory(redisConnectionFactory);
//		redisTemplate.setValueSerializer(new GenericToStringSerializer<Object>(Object.class));
//		redisTemplate.setEnableTransactionSupport(true);
//		return redisTemplate;
//	}
//
////	@Primary
////	@Bean(name = "cacheManager") // Default cache manager is infinite
//	public CacheManager cacheManager(RedisConnectionFactory redisConnectionFactory) {
//		return RedisCacheManager.builder(redisConnectionFactory)
//				.cacheDefaults(RedisCacheConfiguration.defaultCacheConfig().prefixKeysWith(redisPrefix)).build();
//	}
//
////	@Bean(name = "cacheManager1Hour")
//	public CacheManager cacheManager1Hour(RedisConnectionFactory redisConnectionFactory) {
//		Duration expiration = Duration.ofHours(1);
//		return RedisCacheManager.builder(redisConnectionFactory)
//				.cacheDefaults(
//						RedisCacheConfiguration.defaultCacheConfig().prefixKeysWith(redisPrefix).entryTtl(expiration))
//				.build();
//	}

//	@Bean
	public ObjectMapper objectMapper() {
		return new ObjectMapper();
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

}