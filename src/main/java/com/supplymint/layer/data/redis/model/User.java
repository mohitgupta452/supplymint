/**
 * 
 */
package com.supplymint.layer.data.redis.model;

import java.io.Serializable;

/**
 * @author manoj
 *
 */
public class User implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5943631258636752918L;

	private String firstName;

	private String userName;

	private String enterPriseId;

	private String lastName;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEnterPriseId() {
		return enterPriseId;
	}

	public void setEnterPriseId(String enterPriseId) {
		this.enterPriseId = enterPriseId;
	}

}
