package com.supplymint.layer.business.service.notification;

import java.net.InetAddress;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.CannotGetJdbcConnectionException;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.JsonArray;
import com.supplymint.config.aws.utils.AWSUtils.EmailModuleSubModule;
import com.supplymint.exception.StatusCodes.Pagination;
import com.supplymint.layer.business.contract.notification.MailManagerService;
import com.supplymint.layer.data.notification.entity.DropDown;
import com.supplymint.layer.data.notification.entity.MailManager;
import com.supplymint.layer.data.notification.entity.MailManagerViewLog;
import com.supplymint.layer.data.notification.mapper.MailManagerMapper;
import com.supplymint.util.ApplicationUtils.Conditions;
import com.supplymint.util.ApplicationUtils.CustomRunState;
import com.supplymint.util.ApplicationUtils.EmailSubject;
import com.supplymint.util.ApplicationUtils.EmailTemplateName;
import com.supplymint.util.CodeUtils;

import oracle.net.aso.o;

/**
 * @Author Prabhakar Srivastava
 * @Since 15-Feb-2019
 */
@Service
public class MailManagerServiceImpl implements MailManagerService{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MailManagerServiceImpl.class);
	
	private MailManagerMapper mailManagerMapper;

	@Autowired
	public MailManagerServiceImpl(MailManagerMapper mailManagerMapper) {
		this.mailManagerMapper = mailManagerMapper;

	}

	@Autowired
	private HttpServletRequest servletRequest;

	@Override
	public MailManager getById(Integer id) {
		return mailManagerMapper.getById(id);
	}

	@Override
	public ObjectNode getAll(Integer pageNo, Integer type, String module, String subModule, String configuration,
			String search,HttpServletRequest request) {
		List<MailManager> data = null;
		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;
		ObjectMapper mapper=new ObjectMapper();
		ObjectNode objectNode = mapper.createObjectNode();
		try {
			String token = ((HttpServletRequest) request).getHeader("X-Auth-Token");
			JsonArray jOrgDetail = CodeUtils.decode(token).get("ORG_DETAILS").getAsJsonArray();
			String orgId = jOrgDetail.get(0).getAsJsonObject().get("orgId").getAsString();
			
			if (type == 1) {
				LOGGER.info("Email Notification get All query start...");
				totalRecord = mailManagerMapper.getAllRecord(orgId);
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo - 1) * pageSize;
				data = mailManagerMapper.getAll(offset, pageSize,orgId);
				LOGGER.info("Email Notification retrive All result...");
			} else if (type == 2) {
				LOGGER.info("Email Notification start filter record count query...");
				totalRecord = mailManagerMapper.filterRecord(module, subModule, configuration,orgId);
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo - 1) * pageSize;
				data = mailManagerMapper.filter(offset, pageSize, module, subModule, configuration,orgId);
				LOGGER.info("Email Notification retrive result for filter data...");
			} else if (type == 3) {

				if (!search.isEmpty()) {
					LOGGER.info("Email Notification start search record count query...");
					totalRecord = mailManagerMapper.searchEmailConfigRecord(search,orgId);
					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo - 1) * pageSize;
					data = mailManagerMapper.searchEmailConfig(offset, pageSize, search,orgId);
					LOGGER.info("Email Notification retrive result for search data...");
				}
			}
			ArrayNode array = mapper.createArrayNode();
			data.forEach(e -> {
				ObjectNode node = mapper.valueToTree(e);
				array.add(node);
			});
			objectNode.put(Pagination.CURRENT_PAGE, pageNo);
			objectNode.put(Pagination.PREVIOUS_PAGE, previousPage);
			objectNode.put(Pagination.MAXIMUM_PAGE, maxPage);
			objectNode.putPOJO(CodeUtils.RESPONSE, array);
		} catch (CannotGetJdbcConnectionException ex) {
			throw ex;
		} catch (DataAccessException ex) {
			throw ex;
		} catch (Exception ex) {
			throw ex;
		}
		return objectNode;

	}

	@Override
	public Integer create(MailManager emailConfig,HttpServletRequest request) {

		String ipAddress = null;
		String templateName=null;
		String subjectName=null;
		int result = 0;
		int isExistModule=0;
		String token = ((HttpServletRequest) request).getHeader("X-Auth-Token");
		JsonArray jOrgDetail = CodeUtils.decode(token).get("ORG_DETAILS").getAsJsonArray();
		String orgId = jOrgDetail.get(0).getAsJsonObject().get("orgId").getAsString();

		try {
			if(emailConfig.getType().equalsIgnoreCase(CustomRunState.SUCCESS.toString())) {
				if(emailConfig.getModule().equalsIgnoreCase(EmailModuleSubModule.EMAIL_DEMANDPLANNING_MODULE.toString())) {
					templateName=EmailTemplateName.FORECAST_SUCCESS_GENERATION.toString();
					subjectName=EmailSubject.FORECAST_SUCCESS_GENERATED.toString();
				}else if(emailConfig.getModule().equalsIgnoreCase(EmailModuleSubModule.EMAIL_INVENTORY_MODULE.toString())) {
					templateName=EmailTemplateName.TO_SUCCESS_GENERATION.toString();
					subjectName=EmailSubject.TO_SUCCESS_GENERATED.toString();
				}
			}else if(emailConfig.getType().equalsIgnoreCase(CustomRunState.FAILED.toString())){
				if(emailConfig.getModule().equalsIgnoreCase(EmailModuleSubModule.EMAIL_DEMANDPLANNING_MODULE.toString())) {
					templateName=EmailTemplateName.FORECAST_FAILED_GENERATION.toString();
					subjectName=EmailSubject.FORECAST_FAILED_GENERATED.toString();
				}else if(emailConfig.getModule().equalsIgnoreCase(EmailModuleSubModule.EMAIL_INVENTORY_MODULE.toString())) {
					templateName=EmailTemplateName.TO_FAILED_GENERATION.toString();
					subjectName=EmailSubject.TO_FAILED_GENERATED.toString();
				}else if(emailConfig.getModule().equalsIgnoreCase(EmailModuleSubModule.EMAIL_ADMINISTRATION_MODULE.toString())) {
					templateName=EmailTemplateName.DATA_SYNC_FAILED.toString();
					subjectName=EmailSubject.DATA_SYNC_FAILED.toString();
				}
			}
			
			
			emailConfig.setCreatedBy(servletRequest.getAttribute("email").toString());
			Date createdOn = CodeUtils.convertDateOnTimeZonePluseForAWS(new Date(), 5, 30);
			emailConfig.setCreatedOn(createdOn);
			ipAddress = InetAddress.getLocalHost().getHostAddress().trim().toString();
			emailConfig.setIpAddress(ipAddress);
			emailConfig.setActive('1');
			emailConfig.setOrgId(orgId);
			emailConfig.setStatus(Conditions.TRUE.toString());
			emailConfig.setTemplateName(templateName);
			emailConfig.setSubject(subjectName);
			LOGGER.info("Email Notification create query start...");
			
			isExistModule=mailManagerMapper.isExistModule(emailConfig.getModule(),emailConfig.getSubModule(),emailConfig.getType(),orgId);
			if(isExistModule==0) {
				result = mailManagerMapper.create(emailConfig);
			LOGGER.debug(String.format("Email Notification create result obtain... : %d", result));
			}else {
				result=mailManagerMapper.update(emailConfig);
			}
		} catch (CannotGetJdbcConnectionException ex) {
			throw ex;
		} catch (DataAccessException ex) {
			throw ex;
		} catch (Exception ex) {
			try {
				throw ex;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	@Override
	public Integer update(MailManager emailConfig,HttpServletRequest request) {
		String ipAddress = null;
		int result = 0;
		try {
			String token = ((HttpServletRequest) request).getHeader("X-Auth-Token");
			JsonArray jOrgDetail = CodeUtils.decode(token).get("ORG_DETAILS").getAsJsonArray();
			String orgId = jOrgDetail.get(0).getAsJsonObject().get("orgId").getAsString();

			Date updatedOn = CodeUtils.convertDateOnTimeZonePluseForAWS(new Date(), 5, 30);
			emailConfig.setUpdatedOn(updatedOn);
			emailConfig.setOrgId(orgId);
			emailConfig.setUpdatedBy(servletRequest.getAttribute("email").toString());
			ipAddress = InetAddress.getLocalHost().getHostAddress().trim().toString();
			emailConfig.setIpAddress(ipAddress);
			LOGGER.info("Email Notification update query start...");
			result = mailManagerMapper.update(emailConfig);
			LOGGER.debug(String.format("Email Notification update result obtain... : %d", result));
		} catch (CannotGetJdbcConnectionException ex) {
			throw ex;
		} catch (DataAccessException ex) {
			throw ex;
		} catch (Exception ex) {
			try {
				throw ex;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return result;

	}

	@Override
	public Integer delete(int id) {
		return mailManagerMapper.delete(id);
	}

	@Override
	public MailManager getByStatus(String templateName,String status,String orgId) {
		return mailManagerMapper.getByStatus(templateName,status,orgId);
	}

	@Override
	public int createEmailActivityLog(MailManagerViewLog mailManagerViewLog,String orgId){
		String ipAddress = null;
		int result = 0;
		try {
			
//			mailManagerViewLog.setCreatedBy(servletRequest.getAttribute("email").toString());
			Date createdOn = CodeUtils.convertDateOnTimeZonePluseForAWS(new Date(), 5, 30);
			mailManagerViewLog.setCreatedOn(createdOn);
			ipAddress = InetAddress.getLocalHost().getHostAddress().trim().toString();
			mailManagerViewLog.setIpAddress(ipAddress);
			mailManagerViewLog.setOrgId(orgId);
			LOGGER.info("Email Notification activity create query start...");
			result = mailManagerMapper.createEmailActivityLog(mailManagerViewLog);
			LOGGER.debug(String.format("Email Notification activity create result obtain... : %d", result));
		} catch (CannotGetJdbcConnectionException ex) {
			throw ex;
		} catch (DataAccessException ex) {
			throw ex;
		} catch (Exception ex) {
			try {
				throw ex;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	@Override
	public DropDown getAllModule(String moduleName) {
		return mailManagerMapper.getAllModule(moduleName);
	}

	@Override
	public DropDown getAllSubModule(String moduleName) {
		return mailManagerMapper.getAllSubModule(moduleName);
	}

	@Override
	public DropDown getAllConfiguration(String subModuleName) {
		return mailManagerMapper.getAllConfiguration(subModuleName);
	}

	@Override
	public int getEmailStatus(String orgId) {
		return mailManagerMapper.getEmailStatus(orgId);
	}
	
	@Override
	public int updateEmailStatus(char active,String orgId) {
		return mailManagerMapper.updateEmailStatus(active,orgId);
	}

	@Override
	public MailManager getEmailConfiguration(String moduleName, String subModuleName, String configurationProperty,String orgId) {
		return mailManagerMapper.getEmailConfiguration(moduleName,subModuleName,configurationProperty,orgId);
	}

	@Override
	public List<MailManager> getAllEmailData(String orgId) {
		return mailManagerMapper.getAllEmailData(orgId);
	}


}
