package com.supplymint.layer.business.service.administration;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.supplymint.config.application.AppCodeConfig;
import com.supplymint.exception.SupplyMintException;
import com.supplymint.layer.business.contract.administration.CustomService;
import com.supplymint.layer.business.contract.setting.GeneralSettingService;
import com.supplymint.layer.data.tenant.administration.entity.CustomDataTemplate;
import com.supplymint.layer.data.tenant.administration.entity.CustomFileUpload;
import com.supplymint.layer.data.tenant.administration.entity.CustomMaster;
import com.supplymint.layer.data.tenant.administration.entity.DataReference;
import com.supplymint.layer.data.tenant.administration.entity.EventMaster;
import com.supplymint.layer.data.tenant.administration.entity.FMCGStores;
import com.supplymint.layer.data.tenant.administration.mapper.CustomMapper;
import com.supplymint.util.CodeUtils;
import com.supplymint.util.DateUtils;
import com.supplymint.util.JsonUtils;

@Service
public class CustomServiceImpl implements CustomService {

	private CustomMapper customMapper;

	@Autowired
	GeneralSettingService generalSettingService;

	public CustomServiceImpl() {

	}

	@Autowired
	private CustomServiceImpl(CustomMapper customMapper) {
		this.customMapper = customMapper;

	}

	@Override
	public int createCustomFileUpload(CustomFileUpload customFileUpload) {
		return customMapper.createCustomFileUpload(customFileUpload);
	}

	@Override
	public int getAllCustomRecord() {
		return customMapper.getAllCustomRecord();
	}

	@Override
	public List<CustomFileUpload> getAllCustomFileUpload(int offset, int pageSize) {
		return customMapper.getAllCustomFileUpload(offset, pageSize);
	}

	@Override
	public List<CustomMaster> getAllZoneByChannel(String partner, String channel) {
		return customMapper.getAllZoneByChannel(partner, channel);
	}

	@Override
	public List<CustomMaster> getAllGradeByZone(String partner, String channel, String zone) {
		return customMapper.getAllGradeByZone(partner, channel, zone);
	}

	@Override
	public List<CustomMaster> getAllStoreCode(String partner, String channel, String zone, String grade) {
		return customMapper.getAllStoreCode(partner, channel, zone, grade);
	}

	@Override
	public List<CustomDataTemplate> getAllTemplate(String channel) {
		return customMapper.getAllTemplate(channel);
	}

	@Override
	public List<DataReference> getAllDataReference(Integer offset, Integer pageSize, String channel) {
		return customMapper.getAllDataReference(offset, pageSize, channel);
	}

	@Override
	public int refRecordCount(String channel) {
		return customMapper.refRecordCount(channel);
	}

	@Override
	public int searchDataReferenceRecord(String name, String key, String eventStart, String eventEnd, String status,
			Integer dataCount, String channel) {
		return customMapper.searchDataReferenceRecord(name, key, eventStart, eventEnd, status, dataCount, channel);
	}

	@Override
	public List<DataReference> searchDataReference(Integer pageSize, Integer offset, String name, String key,
			String eventStart, String eventEnd, String status, Integer dataCount, String channel) {
		return customMapper.searchDataReference(pageSize, offset, name, key, eventStart, eventEnd, status, dataCount,
				channel);
	}

	@Override
	public List<DataReference> getAllRecords() {
		return customMapper.getAllRecords();
	}

	@Override
	public List<DataReference> searchAllDataReference(Integer offset, Integer pageSize, String search, String channel) {
		return customMapper.searchAllDataReference(offset, pageSize, search, channel);
	}

	@Override
	public Integer searchAllRecordDataReference(String search, String channel) {
		return customMapper.searchAllRecordDataReference(search, channel);
	}

	@Override
	public List<CustomMaster> getAllPartnerByChannel(String channel) {
		return customMapper.getAllPartnerByChannel(channel);
	}

	@Override
	public List<DataReference> getTop5DataReference() {
		return customMapper.getTop5DataReference();
	}

	@Override
	public List<CustomMaster> filterStoreCode(String channel, String partner, String zone, String grade) {
		return customMapper.filterStoreCode(channel, partner, zone, grade);
	}

	@Override
	public String getCustomByKey(String key) {
		return customMapper.getCustomByKey(key);
	}

	@Override
	public String getCustomDateFormat() {
		try {
			Map<String, String> data = generalSettingService.getAll();
			String newDateFormat = null;
			String newDate = data.get("dateFormat");
			String timeFormat = data.get("timeFormat");
			String[] parts = new String[newDate.length()];
			if (newDate.contains("HH")) {
				if (timeFormat.equalsIgnoreCase("24 Hour")) {
					newDateFormat = newDate;
				}
				if (timeFormat.equalsIgnoreCase("12 Hour")) {
					parts = newDate.split("H");
					String h2r = "hh" + parts[2];
					newDateFormat = parts[0] + h2r + " a";
				}
			} else {
				newDateFormat = newDate;
			}
			return newDateFormat;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	@Override
	public int createCustomMaster(JsonNode payload, HttpServletRequest request) throws Exception, SupplyMintException {
		int result = 0;
		EventMaster eventMaster = new EventMaster();
		String token = ((HttpServletRequest) request).getHeader("X-Auth-Token");
//		int eid = CodeUtils.decode(token).get("eid").getAsInt();
		JsonArray orgDetails = CodeUtils.decode(token).get("ORG_DETAILS").getAsJsonArray();

		JsonObject jNode = orgDetails.get(0).getAsJsonObject();
		String orgId = jNode.get("orgId").getAsString();
//		String orgId = "201";
		try {

			String channel = payload.get("channel").asText();
			String partner = payload.get("partner").asText();
			String grade = payload.get("grade").asText();
			String zone = payload.get("zone").asText();
			String storeCode = payload.get("storeCode").asText();
			String eventName = payload.get("eventName").asText();
			String multiplierROS = payload.get("multiRos").asText();
			String stockDays = payload.get("stockDays").asText();
			String startDate = payload.get("startDate").asText();
			String endDate = payload.get("endDate").asText();
			String id = payload.get("id").asText();
			String isEventNameChanged = payload.get("isEventNameChanged").asText();
			String ipAddress = InetAddress.getLocalHost().getHostAddress().trim().toString();
			String flag = payload.get("isUpdate").asText();
			if (flag.equalsIgnoreCase("FALSE")) {
				int isValidEventName = customMapper.isValidEventName(eventName, orgId);
				if (isValidEventName > 0) {
					throw new SupplyMintException("Event Name already exists!!");
				}
			}

			eventMaster.setIpAddress(ipAddress);
//			eventMaster.setEnterpriseId(eid);
			eventMaster.setOrgId(orgId);
			eventMaster.setChannel(channel);
			eventMaster.setPartner(partner);
			eventMaster.setGrade(grade);
			eventMaster.setZone(zone);
			eventMaster.setStoreCode(storeCode);
			eventMaster.setStartDate(DateUtils.stringToDate(startDate));
			eventMaster.setEndDate(DateUtils.stringToDate(endDate));
			eventMaster.setEventName(eventName);
			eventMaster.setMultiplierROS(Integer.parseInt(multiplierROS));
			eventMaster.setStockDays(Integer.parseInt(stockDays));
			eventMaster.setCreatedOn(new Date());
			if (flag.equalsIgnoreCase("FALSE")) {
				eventMaster.setStatus("TRUE");
				eventMaster.setActive("1");
				result = customMapper.createEventMaster(eventMaster);
			} else {
				eventMaster.setId(Integer.parseInt(id));
				if (isEventNameChanged.equalsIgnoreCase("TRUE")) {
					int validateEventName = customMapper.validateEventName(eventMaster.getEventName());
					if (validateEventName > 0) {
						throw new SupplyMintException("Event Name already exists!!");
					}
				}
				result = customMapper.updateEventMaster(eventMaster);
			}

		} catch (Exception ex) {
			throw ex;
		}
		return result;
	}

	@Override
	public int getAllCountEventMaster(String orgId) {

		return customMapper.getAllCountEventMaster(orgId);
	}

	@Override
	public ObjectNode getAllEventMasterData(int offset, int pageSize, String orgId) {
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode node = mapper.createObjectNode();
		List<Map<String, Object>> listOfMap = new ArrayList<Map<String, Object>>();
		// getting data from custom_event_master
		List<EventMaster> allEventMasterData = customMapper.getAllEventMasterData(offset, pageSize, orgId);

//		allEventMasterData.stream().forEach(master -> {
//			Map<String, Object> insideMap = new LinkedHashMap<>();
//
//			insideMap.put("Event Name", master.getEventName());
//			insideMap.put("Start Date", master.getStartDate());
//			insideMap.put("End Date", master.getEndDate());
//			insideMap.put("No. Of Days Additional Stock Cover", master.getStockDays());
//			insideMap.put("Multiplier for ROS", master.getMultiplierROS());
//			insideMap.put("Grade", master.getGrade());
//			insideMap.put("Partner", master.getPartner());
//			insideMap.put("Zone", master.getZone());
//			// adding data into list
//			listOfMap.add(insideMap);
//		});
		if (allEventMasterData.size() > 0) {
			node.putPOJO(CodeUtils.RESPONSE, allEventMasterData);
			return node;
		} else {
			return null;
		}

	}

	@Override
	public int filterCountEventMasterRecord(String eventName, String startDate, String endDate, String stockDays,
			String multiplierROS, String partner, String grade, String storeCode, String zone, String orgId) {

		return customMapper.filterCountEventMasterRecord(eventName, startDate, endDate, stockDays, multiplierROS,
				partner, grade, storeCode, zone, orgId);
	}

	@Override
	public ObjectNode getFilterCustomEventMaster(int pageSize, int offset, String eventName, String startDate,
			String endDate, String stockDays, String multiplierROS, String partner, String grade, String storeCode,
			String zone, String orgId) {
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode node = mapper.createObjectNode();
		List<Map<String, Object>> listOfMap = new ArrayList<Map<String, Object>>();

		List<EventMaster> filterEvenMasterData = customMapper.getFilterCustomEventMaster(pageSize, offset, eventName,
				startDate, endDate, stockDays, multiplierROS, partner, grade, storeCode, zone, orgId);
//
//		filterEvenMasterData.stream().forEach(master -> {
//			Map<String, Object> insideMap = new LinkedHashMap<>();
//
//			insideMap.put("Event Name", master.getEventName());
//			insideMap.put("Start Date", master.getStartDate());
//			insideMap.put("End Date", master.getEndDate());
//			insideMap.put("No. Of Days Additional Stock Cover", master.getStockDays());
//			insideMap.put("Multiplier for ROS", master.getMultiplierROS());
//			insideMap.put("Grade", master.getGrade());
//			insideMap.put("Partner", master.getPartner());
//			insideMap.put("Zone", master.getZone());
//			// adding data into list
//			listOfMap.add(insideMap);
//		});
		if (filterEvenMasterData.size() > 0) {
			node.putPOJO(CodeUtils.RESPONSE, filterEvenMasterData);
		} else {
			return null;
		}
		return node;

	}

	@Override
	public int searchCountCustomMasterData(String search, String orgId) {

		return customMapper.searchCountCustomMasterData(search, orgId);
	}

	@Override
	public ObjectNode searchAllCustomMasterData(int offset, int pageSize, String search, String orgId) {
		List<EventMaster> searchedEvenMasterData = customMapper.searchAllCustomMasterData(offset, pageSize, search,
				orgId);
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode node = mapper.createObjectNode();
		List<Map<String, Object>> listOfMap = new ArrayList<Map<String, Object>>();
		try {
//			searchedEvenMasterData.stream().forEach(master -> {
//				Map<String, Object> insideMap = new LinkedHashMap<>();
//
//				insideMap.put("Event Name", master.getEventName());
//				insideMap.put("Start Date", master.getStartDate());
//				insideMap.put("End Date", master.getEndDate());
//				insideMap.put("No. Of Days Additional Stock Cover", master.getStockDays());
//				insideMap.put("Multiplier for ROS", master.getMultiplierROS());
//				insideMap.put("Grade", master.getGrade());
//				insideMap.put("Partner", master.getPartner());
//				insideMap.put("Zone", master.getZone());
//				// adding data into list
//				listOfMap.add(insideMap);
//			});
			if (searchedEvenMasterData.size() > 0) {
				node.putPOJO(CodeUtils.RESPONSE, searchedEvenMasterData);
			} else {
				return null;
			}
		} catch (Exception ex) {
			throw ex;
		}
		return node;
	}

	@Override
	public int deleteEventMaster(JsonNode payload, String orgId) {
		int result = 0;
		try {
			String eventName = payload.get("eventName").asText();
			result = customMapper.deleteEventMaster(eventName, orgId);
		} catch (Exception ex) {
			throw ex;
		}
		return result;
	}

	@Override
	public Map<String, Object> getAllMotherComp(int pageNo, int type, String search) {
		Map<String, Object> data = new HashMap<>();
		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;
		List<String> motherCompData = null;

		try {
			if (type == 1) {
				totalRecord = customMapper.getMotherCompRecord();
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
				motherCompData = customMapper.getAllMotherComp(offset, pageSize);
			} else if (type == 3) {
				totalRecord = customMapper.searchMotherCompRecord(search);
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
				motherCompData = customMapper.searchMotherComp(offset, pageSize, search);

			}

			data.put("previousPage", previousPage);
			data.put("maxPage", maxPage);
			data.put("motherCom", motherCompData);

		}

		catch (Exception e) {
			throw e;
		}

		return data;

	}

	@Override
	public Map<String, Object> getAllArticleByMotherComp(int pageNo, int type, String motherComp, String search) {
		Map<String, Object> data = new HashMap<>();
		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;
		List<FMCGStores> articleData = null;

		try {
			String[] arrMotherComp = motherComp.split("\\|");
			List<String> listOfMotherComp = Arrays.asList(arrMotherComp);
			if (type == 1) {
				if (!CodeUtils.isEmpty(motherComp))
					totalRecord = customMapper.getAllArticleRecords(listOfMotherComp, motherComp);
				else
					totalRecord = customMapper.getAllArticleRecord();

				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
				articleData = customMapper.getAllArticle(offset, pageSize, listOfMotherComp, motherComp);

			} else if (type == 3 && !CodeUtils.isEmpty(search)) {

				totalRecord = customMapper.searchArticleRecords(listOfMotherComp, motherComp, search);
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
				articleData = customMapper.searchArticle(offset, pageSize, listOfMotherComp, motherComp, search);
			}

			data.put("previousPage", previousPage);
			data.put("maxPage", maxPage);
			data.put("articleData", articleData);

		} catch (Exception e) {
			throw e;
		}

		return data;

	}

	@Override
	public Map<String, Object> getAllVendor(int pageNo, int type, String motherComp, String articleCode,
			String search) {
		Map<String, Object> data = new HashMap<>();
		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;

		List<FMCGStores> vendorData = null;

		try {
			String[] arrMotherComp = motherComp.split("\\|");
			List<String> listOfMotherComp = Arrays.asList(arrMotherComp);
			List<String> listOfArticleCode = Arrays.asList(articleCode.split("\\|"));

			if (type == 1) {
				if (!motherComp.isEmpty() || !articleCode.isEmpty())
					totalRecord = customMapper.getAllVendorRecord(listOfMotherComp, motherComp, listOfArticleCode,
							articleCode);
				else
					totalRecord = customMapper.getAllVendorRecords();

				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
				vendorData = customMapper.getAllVendor(offset, pageSize, listOfMotherComp, motherComp,
						listOfArticleCode, articleCode);
			} else if (type == 3 && !CodeUtils.isEmpty(search)) {

				totalRecord = customMapper.searchVendorRecord(listOfMotherComp, motherComp, listOfArticleCode,
						articleCode, search);
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
				vendorData = customMapper.searchVendor(offset, pageSize, listOfMotherComp, motherComp,
						listOfArticleCode, articleCode, search);

			}

			data.put("previousPage", previousPage);
			data.put("maxPage", maxPage);
			data.put("vendorData", vendorData);

		} catch (Exception e) {
			throw e;
		}

		return data;

	}

	@Override
	public Map<String, Object> getAllStore(int pageNo, int type, String motherComp, String articleCode,
			String vendorCode, String search) {
		Map<String, Object> data = new HashMap<>();
		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;
		List<FMCGStores> storeData = null;

		try {
			String[] arrMotherComp = motherComp.split("\\|");
			List<String> listOfMotherComp = Arrays.asList(arrMotherComp);
			List<String> listOfArticleCode = Arrays.asList(articleCode.split("\\|"));
			List<String> listOfVendorCode = Arrays.asList(vendorCode.split("\\|"));

			if (type == 1) {

				if (!motherComp.isEmpty() || !articleCode.isEmpty() || !vendorCode.isEmpty())
					totalRecord = customMapper.getAllStoreRecord(listOfMotherComp, motherComp, listOfArticleCode,
							articleCode, listOfVendorCode, vendorCode);
				else
					totalRecord = customMapper.getAllStoreRecords();

				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
				data.put("previousPage", previousPage);
				data.put("maxPage", maxPage);
				storeData = customMapper.getAllStore(offset, pageSize, listOfMotherComp, motherComp, listOfArticleCode,
						articleCode, listOfVendorCode, vendorCode);

			} else if (type == 3 && !CodeUtils.isEmpty(search)) {

				totalRecord = customMapper.searchStoreRecord(listOfMotherComp, motherComp, listOfArticleCode,
						articleCode, listOfVendorCode, vendorCode, search);
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
				storeData = customMapper.searchStore(offset, pageSize, listOfMotherComp, motherComp, listOfArticleCode,
						articleCode, listOfVendorCode, vendorCode, search);
			}

			data.put("previousPage", previousPage);
			data.put("maxPage", maxPage);
			data.put("storeData", storeData);

		} catch (Exception e) {
			throw e;
		}
		return data;
	}

	@Override
	public String getAllFMCGFilterStore(JsonNode payload) {

		List<FMCGStores> storeCodeDataProvider = null;
		String storeCodeData = "";

		try {
			String motherComp = payload.get("motherComp").asText();
			String articleCode = payload.get("articleCode").asText();
			String vendorCode = payload.get("vendorCode").asText();
			String storeCode = payload.get("storeCode").asText();

			if (storeCode.isEmpty()) {

				if (!motherComp.isEmpty() || !articleCode.isEmpty() || !vendorCode.isEmpty()) {
					String[] arrMotherComp = motherComp.split("\\|");
					List<String> listOfMotherComp = Arrays.asList(arrMotherComp);
					List<String> listOfArticleCode = Arrays.asList(articleCode.split("\\|"));
					List<String> listOfVendorCode = Arrays.asList(vendorCode.split("\\|"));
					storeCodeDataProvider = customMapper.getAllStores(listOfMotherComp, motherComp, listOfArticleCode,
							articleCode, listOfVendorCode, vendorCode);
					for (FMCGStores code : storeCodeDataProvider) {
						storeCodeData += code.getStoreCode() + "|";
					}
					if (storeCodeData.endsWith("|"))
						storeCodeData = storeCodeData.substring(0, storeCodeData.length() - 1);
				} else {
					storeCodeData = "ALL_STORES";
				}
			} else {
				storeCodeData = storeCode;
			}

		} catch (Exception e) {
			throw e;
		}

		return storeCodeData;

	}

}
