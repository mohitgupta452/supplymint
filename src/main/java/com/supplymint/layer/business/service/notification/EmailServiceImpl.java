
package com.supplymint.layer.business.service.notification;

import java.io.File;
import java.io.UnsupportedEncodingException;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import com.supplymint.config.application.EmailConfiguration;
import com.supplymint.layer.business.contract.notification.EmailService;

/**
 * @Author Manoj Singh
 * @Since 12-Feb-2019
 */

@Component
public class EmailServiceImpl implements EmailService {

	private final static Logger LOGGER = LoggerFactory.getLogger(EmailServiceImpl.class);

	private static final String FROM = "no-reply@supplymint.com";

	private static final String FROM_NAME = "SupplyMint Notification";

	@Autowired
	public JavaMailSender emailSender;

	@Autowired
	public EmailConfiguration emailConfig;

	public void sendSimpleMessage(String[] to, String[] cc, String subject, String text) {
		try {
			SimpleMailMessage message = new SimpleMailMessage();
			message.setFrom(FROM);
			message.setTo(to);
			message.setCc(cc);
			message.setSubject(subject);
			message.setText(text);
			emailSender.send(message);
		} catch (MailException exception) {
			exception.printStackTrace();
		}
	}

	@Override
	public void sendSimpleMessageUsingTemplate(SimpleMailMessage template, String... templateArgs) {
		String text = String.format(template.getText(), templateArgs);
		template.setText(text);
		sendSimpleMessage(template.getTo(), template.getCc(), template.getSubject(), text);
	}

	@Override
	public void sendEmailUsingMailTemplate(SimpleMailMessage template, String mailTemplate, String... templateArgs) {

		String t = emailConfig.getTextMessage(mailTemplate);
		LOGGER.info(t);
		String text = String.format(emailConfig.getTextMessage(mailTemplate), templateArgs);
		text = text.replaceAll("--", "%;");
		try {

			MimeMessage message = emailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(message, true, "UTF-8");

			helper.setFrom(FROM, FROM_NAME);
			helper.setTo(template.getTo());
			if (template.getCc() != null)
				helper.setCc(template.getCc());
			helper.setSubject(template.getSubject());
			helper.setText(text, true);
			emailSender.send(message);
		} catch (UnsupportedEncodingException | MessagingException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void sendMessageWithAttachment(String to, String subject, String text, String pathToAttachment) {
		try {
			MimeMessage message = emailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(message, true, "UTF-8");

			helper.setFrom(FROM, FROM_NAME);
			helper.setTo(to);
			helper.setSubject(subject);
			helper.setText(text, true);

			FileSystemResource file = new FileSystemResource(new File(pathToAttachment));
			helper.addAttachment("Attachments", file);

			emailSender.send(message);
		} catch (UnsupportedEncodingException | MessagingException e) {
			e.printStackTrace();
		}

	}
}
