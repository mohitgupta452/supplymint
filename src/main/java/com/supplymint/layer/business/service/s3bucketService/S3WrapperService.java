/**
 * 
 */
package com.supplymint.layer.business.service.s3bucketService;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.HttpMethod;
import com.amazonaws.SdkClientException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.TransferManagerBuilder;
import com.amazonaws.services.s3.transfer.Upload;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.supplymint.config.aws.config.AmazonS3Client;
import com.supplymint.config.aws.utils.AWSUtils;
import com.supplymint.layer.data.config.ThreadLocalStorage;
import com.supplymint.util.ApplicationUtils.BucketParameter;
import com.supplymint.util.ApplicationUtils.CustomParameter;
import com.supplymint.util.ApplicationUtils.TenantHash;
import com.supplymint.util.CodeUtils;
import com.supplymint.util.ExcelUtils;
import com.supplymint.util.FileUtils;

/**
 * @author Manoj Singh
 * @since 20 OCT 2018
 * @version 1.0
 */
@Service
public class S3WrapperService {

	private final static Logger LOGGER = LoggerFactory.getLogger(S3WrapperService.class);

	private static String separator = "/";

	@Autowired
	private AmazonS3Client amazonS3Client;

	private PutObjectResult upload(String filePath, String uploadKey, String bucket) throws FileNotFoundException {
		return upload(new FileInputStream(filePath), uploadKey, bucket);
	}

	private PutObjectResult upload(InputStream inputStream, String uploadKey, String bucket) {
		PutObjectRequest putObjectRequest = new PutObjectRequest(bucket, uploadKey, inputStream, new ObjectMetadata());
		putObjectRequest.setCannedAcl(CannedAccessControlList.PublicRead);
		PutObjectResult putObjectResult = amazonS3Client.awsS3ClientWithCredentials().putObject(putObjectRequest);
		IOUtils.closeQuietly(inputStream);
		return putObjectResult;
	}

	public List<PutObjectResult> upload(MultipartFile[] multipartFiles, String bucket, String fileName) {

		List<PutObjectResult> putObjectResults = new ArrayList<>();
		Arrays.stream(multipartFiles).filter(multipartFile -> !StringUtils.isEmpty(multipartFile.getOriginalFilename()))
				.forEach(multipartFile -> {
					try {
						putObjectResults.add(upload(multipartFile.getInputStream(), fileName, bucket));
					} catch (IOException e) {
						e.printStackTrace();
					}
				});

		return putObjectResults;
	}

	public PutObjectResult upload(File file, String uploadKey, String bucket) {
		PutObjectRequest putObjectRequest = new PutObjectRequest(bucket, uploadKey, file);
		putObjectRequest.setCannedAcl(CannedAccessControlList.PublicRead);
		PutObjectResult putObjectResult = amazonS3Client.awsS3ClientWithCredentials().putObject(bucket, uploadKey,
				file);
		return putObjectResult;
	}

	@SuppressWarnings("deprecation")
	public String uploads(String filePath, String bucket, String enterprise)
			throws AmazonClientException, InterruptedException {
		String filePercent = null;
		try {

			TransferManager tm = TransferManagerBuilder.standard()
					.withS3Client(amazonS3Client.awsS3ClientWithCredentials()).build();

			// TransferManager processes all transfers asynchronously,
			// so this call returns immediately.
			String ex = filePath.split("\\.")[1];
			String[] fileName = filePath.split("\\.")[0].split("\\/");
			String key = fileName[fileName.length - 1] + "." + ex;
			Upload upload = tm.upload(bucket, key, new File(filePath));
			upload.waitForCompletion();
			filePercent = upload.isDone() ? AWSUtils.Percentage.COMPLETED : AWSUtils.Percentage.INPROGRESS;

		} catch (AmazonServiceException e) {
			// The call was transmitted successfully, but Amazon S3 couldn't process
			// it, so it returned an error response.
			e.printStackTrace();
		} catch (SdkClientException e) {
			// Amazon S3 couldn't be contacted for a response, or the client
			// couldn't parse the response from Amazon S3.
			e.printStackTrace();
		}
		return filePercent;
	}

	public ResponseEntity<byte[]> download(String bucket, String key, Date date) throws IOException {
		String bucketKey = amazonS3Client.getLastRunObject(bucket, key, date).getKey();
		LOGGER.debug(String.format("Bucket Key : %s", bucketKey));
		GetObjectRequest getObjectRequest = new GetObjectRequest(bucket, bucketKey);
		S3Object s3Object = amazonS3Client.awsS3ClientWithCredentials().getObject(getObjectRequest);
		S3ObjectInputStream objectInputStream = s3Object.getObjectContent();
		byte[] bytes = IOUtils.toByteArray(objectInputStream);
		String fileName = URLEncoder.encode(key, "UTF-8").replaceAll("\\+", "%20");
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_OCTET_STREAM);
		httpHeaders.setContentLength(bytes.length);
		httpHeaders.setContentDispositionFormData("attachment", "Allocation_Output_" + fileName);
		return new ResponseEntity<>(bytes, httpHeaders, HttpStatus.OK);
	}

	public String downloadPreSigned(String bucket, String key, Date date) {
		String URL = null;
		try {
			// Set the presigned URL to expire after one hour.
			java.util.Date expiration = new java.util.Date();
			long expTimeMillis = expiration.getTime();
			expTimeMillis += 100000 * 60 * 60 * 24;
			expiration.setTime(expTimeMillis);
			// Generate the presigned URL.
			LOGGER.debug("Generating pre-signed URL.");
			String bucketKey = amazonS3Client.getLastRunObject(bucket, key, date).getKey();
			LOGGER.debug(String.format("Bucket Key : %s", bucketKey));
			GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(bucket, bucketKey)
					.withMethod(HttpMethod.GET).withExpiration(expiration);
			URL url = amazonS3Client.awsS3ClientWithCredentials().generatePresignedUrl(generatePresignedUrlRequest);
			URL = url.toString();
			LOGGER.debug(String.format("Pre-Signed URL: ", url.toString()));
		} catch (AmazonServiceException e) {
			e.printStackTrace();
		} catch (SdkClientException e) {
			e.printStackTrace();
		}
		return URL;
	}

	public void downloadS3ToLocalDirectory(String bucket_name, String key, Date date) {
		String key_name = amazonS3Client.getLastRunObject(bucket_name, key, date).getKey();
		LOGGER.debug(String.format("Bucket Key : %s", key_name));
		final String USAGE = "\n" + "To run this example, supply the name of an S3 bucket and object to\n"
				+ "download from it.\n" + "\n" + "Ex: GetObject <bucketname> <filename>\n";

		LOGGER.debug("Downloading %s from S3 bucket %s...\n", key_name, bucket_name);
		final AmazonS3 s3 = amazonS3Client.awsS3ClientWithCredentials();
		try {
			GetObjectRequest getObjectRequest = new GetObjectRequest(bucket_name, key_name);
			S3Object s3Object = amazonS3Client.awsS3ClientWithCredentials().getObject(getObjectRequest);
			S3ObjectInputStream objectInputStream = s3Object.getObjectContent();
			FileOutputStream fos = new FileOutputStream(
					new File(FileUtils.getPlatformBasedParentDir() + "/" + key_name));
			byte[] read_buf = new byte[1024];
			int read_len = 0;
			while ((read_len = objectInputStream.read(read_buf)) > 0) {
				fos.write(read_buf, 0, read_len);
			}
			objectInputStream.close();
			fos.close();
		} catch (AmazonServiceException e) {
			System.err.println(e.getErrorMessage());
			System.exit(1);
		} catch (FileNotFoundException e) {
			System.err.println(e.getMessage());
			System.exit(1);
		} catch (IOException e) {
			System.err.println(e.getMessage());
			System.exit(1);
		}
	}

	public List<S3ObjectSummary> list(String bucket) {
		return amazonS3Client.list(bucket);
	}

	public Bucket createBucket(String bucket) {
		return amazonS3Client.createS3Bucket(bucket);
	}

	public String downloadPreSignedURL(String bucket, String key, int expire) {
		String URL = null;
		try {
			// Set the presigned URL to expire after one hour.

			java.util.Date expiration = new java.util.Date();
			long expTimeMillis = expiration.getTime();
			expTimeMillis += 1000 * 60 * expire;
			expiration.setTime(expTimeMillis);
			// Generate the presigned URL.
			LOGGER.debug("Generating pre-signed URL.");
			String keys = amazonS3Client.getObject(bucket, key).getKey();
			GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(bucket, key)
					.withMethod(HttpMethod.GET).withExpiration(expiration);
			URL url = amazonS3Client.awsS3ClientWithCredentials().generatePresignedUrl(generatePresignedUrlRequest);
			URL = url.toString();
			LOGGER.debug(String.format("Pre-Signed URL: ", url.toString()));
		} catch (AmazonServiceException e) {
			e.printStackTrace();
		} catch (SdkClientException e) {
			e.printStackTrace();
		}
		return URL;
	}

	public Map getPreSignedURLWithMetaData(String bucket, String key, int expire) {
		Map<String, String> metaData = new HashMap<>();
		String URL = null;
		try {
			// Set the presigned URL to expire after one hour.

			java.util.Date expiration = new java.util.Date();
			long expTimeMillis = expiration.getTime();
			expTimeMillis += 1000 * 60 * expire;
			expiration.setTime(expTimeMillis);

			// Generate the presigned URL.
			LOGGER.debug("Generating pre-signed URL.");
			S3Object s3Object = amazonS3Client.getObject(bucket, key);
			GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(bucket, key)
					.withMethod(HttpMethod.GET).withExpiration(expiration);
			URL url = amazonS3Client.awsS3ClientWithCredentials().generatePresignedUrl(generatePresignedUrlRequest);
			URL = url.toString();

			metaData.put("url", URL);
			metaData.put("modifiedOn", s3Object.getObjectMetadata().getLastModified().toGMTString());
			LOGGER.debug(String.format("Pre-Signed URL: ", url.toString()));
		} catch (AmazonServiceException e) {
			e.printStackTrace();
		} catch (SdkClientException e) {
			e.printStackTrace();
		}
		return metaData;
	}

	public String downloadPreSignedURLOnHistory(String bucket, String key, int expire) {
		String URL = null;
		try {
			// Set the presigned URL to expire after one hour.

			java.util.Date expiration = new java.util.Date();
			long expTimeMillis = expiration.getTime();
			expTimeMillis += 1000 * 60 * expire;
			expiration.setTime(expTimeMillis);
			// Generate the presigned URL.
			LOGGER.debug("Generating pre-signed URL.");
			List<S3ObjectSummary> objectSummary = amazonS3Client.getObjectSummaries(bucket, key);
			List<URL> urlContainer = new ArrayList<>();
			for (S3ObjectSummary s3ObjectSummary : objectSummary) {
				GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(bucket,
						s3ObjectSummary.getKey()).withMethod(HttpMethod.GET).withExpiration(expiration);
				URL url = amazonS3Client.awsS3ClientWithCredentials().generatePresignedUrl(generatePresignedUrlRequest);
				URL = url.toString();
				LOGGER.debug(String.format("Pre-Signed URL: ", url.toString()));
			}

		} catch (AmazonServiceException e) {
			e.printStackTrace();
		} catch (SdkClientException e) {
			e.printStackTrace();
		}
		return URL;
	}

	@SuppressWarnings("unused")
	public Map<String, String> downloadListOfPreSignedURL(String bucket, String key, int expire) {
		String URL = null;
		Map<String, String> fileNameWithURL = new HashMap<>();
		try {
			// Set the presigned URL to expire after one hour.

			java.util.Date expiration = new java.util.Date();
			long expTimeMillis = expiration.getTime();
			expTimeMillis += 1000 * 60 * expire;
			expiration.setTime(expTimeMillis);
			// Generate the presigned URL.

			LOGGER.debug("Generating pre-signed URL.");
			key = key.trim().toString();
//			key = key.split("/")[0] + "/93443001-6278-43b8-a306-1ca62e46a40c/";
			String tenantHashKey=ThreadLocalStorage.getTenantHashKey();
			if(tenantHashKey.equalsIgnoreCase(TenantHash.valueOf("CITYKART").getHashKey())) {
				String url=s3Call(bucket, key);
				fileNameWithURL.put(key, url);
				return fileNameWithURL;
			}
			List<S3ObjectSummary> objectSummary = amazonS3Client.getObjectSummaries(bucket, key);
			for (S3ObjectSummary s3ObjectSummary : objectSummary) {
				if (s3ObjectSummary.getSize() != 0 && !s3ObjectSummary.getKey().contains("MANUAL_TRANSFER_ORDER")) {
					String genkey = key.split("/")[1];
					boolean isGenericFolder=false;
					
					String genFolder=s3ObjectSummary.getKey().substring(s3ObjectSummary.getKey().indexOf(key.split("/")[1]), s3ObjectSummary.getKey().length());
					genFolder=genFolder.substring(genFolder.indexOf("/")+1, genFolder.length());
					isGenericFolder=genFolder.startsWith("run");
					String genericFolder=s3ObjectSummary.getKey().substring(s3ObjectSummary.getKey().indexOf(key.split("/")[1]), s3ObjectSummary.getKey().lastIndexOf("/"));
					genericFolder=genericFolder.substring(genericFolder.indexOf("/")+1, genericFolder.length());
					
					if(isGenericFolder || CodeUtils.isEmpty(genFolder)) {
					GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(bucket,
							s3ObjectSummary.getKey()).withMethod(HttpMethod.GET).withExpiration(expiration);
					URL url = amazonS3Client.awsS3ClientWithCredentials()
							.generatePresignedUrl(generatePresignedUrlRequest);
					URL = url.toString();
					fileNameWithURL.put(s3ObjectSummary.getKey().split(separator)[2].toString(), URL);
					LOGGER.debug(String.format("Pre-Signed URL: ", url.toString()));
					}else {
						GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(bucket,
								s3ObjectSummary.getKey()).withMethod(HttpMethod.GET).withExpiration(expiration);
						URL url = amazonS3Client.awsS3ClientWithCredentials()
								.generatePresignedUrl(generatePresignedUrlRequest);
						URL = url.toString();
						fileNameWithURL.put(genericFolder+"_FILE", URL);
						LOGGER.debug(String.format("Pre-Signed URL: ", url.toString()));
					}
				}

			}

		} catch (AmazonServiceException e) {
			e.printStackTrace();
		} catch (SdkClientException e) {
			e.printStackTrace();
		}
		return fileNameWithURL;
	}

	/**
	 * @author Prabhakar Srivastava save file to local disk from s3 storage
	 */
	public File getFileFromS3Bucket(String bucketName, String downloadUrl) throws IOException {
		File file = null;
		String fileLocation = downloadUrl.substring(downloadUrl.indexOf(bucketName), downloadUrl.indexOf("?"));
		String bucketPathWithFileName = fileLocation.substring(fileLocation.indexOf("/") + 1);
		LOGGER.debug("retrieving S3Object from file path : %s", bucketPathWithFileName);

		S3Object object = amazonS3Client.getObject(bucketName, bucketPathWithFileName);

		String savedPath = File.separator + "S3"
				+ bucketPathWithFileName.substring(bucketPathWithFileName.lastIndexOf("/"));

//		if (!savedPath.contains(".")) {
//			file = new File(FileUtils.createFilePath(FileUtils.getPlatformBasedParentDir(), savedPath + ".csv"));
//		} else {
			file = new File(FileUtils.createFilePath(FileUtils.getPlatformBasedParentDir(), savedPath));
//		}
		InputStream reader = new BufferedInputStream(object.getObjectContent());
		OutputStream writer = new BufferedOutputStream(new FileOutputStream(file));
		int read = -1;
		while ((read = reader.read()) != -1) {
			writer.write(read);
		}
		writer.flush();
		writer.close();
		reader.close();

		return file;

	}

	@SuppressWarnings("finally")

	public ArrayNode readDataFromS3(String bucket, String key) throws IOException {
		LOGGER.debug("Generating pre-signed URL.");
		ArrayNode object = null;
		S3Object s3Object = null;
		try {
			key = amazonS3Client.getObjectSummary(bucket, key).getKey();
			LOGGER.info("Get an object and print its contents.");
			s3Object = amazonS3Client.getObjectRequest(bucket, key);
			List<Map<String, String>> data = ExcelUtils.getExcelList(s3Object.getObjectContent());
			object = CodeUtils.toJSON(data, true);
			LOGGER.info("Printing bytes retrieved.");
		} catch (AmazonServiceException e) {
			LOGGER.info(
					"The call was transmitted successfully, but Amazon S3 couldn't process it, so it returned an error response");
			e.printStackTrace();
		} catch (SdkClientException e) {

			LOGGER.info(
					"Amazon S3 couldn't be contacted for a responkse, or the client couldn't parse the response from Amazon S3");
			e.printStackTrace();
		} finally {
			LOGGER.info("To ensure that the network connection doesn't remain open, close any open input streams");
			if (s3Object != null) {
				s3Object.close();
			}
			return object;
		}

	}

	/**
	 * @author Prabhakar Srivastava This method describes get all file list to
	 *         generate URL and create zip file on local disk
	 */
	public File getListFileFromS3Bucket(String bucketName, String bucketKey) throws IOException {

		List<S3ObjectSummary> objectSummary = amazonS3Client.getObjectSummaries(bucketName, bucketKey);
		File[] zipFiles = new File[objectSummary.size()];

		for (int i = 0; i < objectSummary.size(); i++) {
			if (objectSummary.get(i).getSize() != 0) {
				GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(bucketName,
						objectSummary.get(i).getKey()).withMethod(HttpMethod.GET);
				URL url = amazonS3Client.awsS3ClientWithCredentials().generatePresignedUrl(generatePresignedUrlRequest);
				File file = getFileFromS3Bucket(bucketName, url.toString());
				zipFiles[i] = file;
				LOGGER.debug(String.format("Pre-Signed URL: ", url.toString()));
			}

		}
		String savedPath = File.separator + "S3/TRANSFER_ORDER.zip";
		String filePath = FileUtils.createFilePath(FileUtils.getPlatformBasedParentDir(), savedPath);
		File file = new File(filePath);
		LOGGER.debug("creating a transfer order zip files");
		CodeUtils.createZip(file, zipFiles);
		return file;
	}
	
	
	
	public String s3Call(String bucketName,String bucketKey) {
		int count=0;
		List<S3ObjectSummary> objectSummary = amazonS3Client.getObjectSummaries(bucketName, bucketKey);
		for (int i = 0; i < objectSummary.size(); i++) {
			if(objectSummary.get(i).getKey().contains(".zip")){
				String s3BucketFilePath = BucketParameter.BUCKETPREFIX + bucketName
						+ CustomParameter.DELIMETER + bucketKey;
				s3BucketFilePath = s3BucketFilePath.split("\\//")[1]
						.replaceAll(bucketName + CustomParameter.DELIMETER, "")
						+ CustomParameter.DELIMETER.trim().toString();
				String url = this.downloadPreSignedURLOnHistory(bucketName,  bucketKey+"Requirement.zip",2048);
				return url;
			}
			if(!objectSummary.get(i).getKey().contains("MANUAL_TRANSFER_ORDER")) {
				count++;
			}
		};
		File[] zipFiles = new File[count];
		for (int i = 0; i < objectSummary.size(); i++) {
			if (objectSummary.get(i).getSize() != 0 && !objectSummary.get(i).getKey().contains("MANUAL_TRANSFER_ORDER")) {
				GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(bucketName,
						objectSummary.get(i).getKey()).withMethod(HttpMethod.GET);
				URL url = amazonS3Client.awsS3ClientWithCredentials().generatePresignedUrl(generatePresignedUrlRequest);
				File file =null;
				try {
					file = getFileFromS3Bucket(bucketName, url.toString());
				}catch(Exception ex) {
					ex.printStackTrace();
				}
				zipFiles[0] = file;
				LOGGER.debug(String.format("Pre-Signed URL: ", url.toString()));
			}
		}
		String savedPath = File.separator + "S3/REQ/";
		String filePath = FileUtils.createFilePath(FileUtils.getPlatformBasedParentDir(), savedPath);
		filePath=filePath+"REQUIREMENT.zip";
		File file = new File(filePath);
		LOGGER.debug("creating a transfer order zip files");
		CodeUtils.createZip(file, zipFiles);
		for (int i = 0; i < objectSummary.size(); i++) {
			if (!objectSummary.get(i).getKey().contains("MANUAL_TRANSFER_ORDER")) {
				amazonS3Client.awsS3ClientWithCredentials().deleteObject(bucketName, objectSummary.get(i).getKey());
			}
		};
		String etag = this.upload(file, bucketKey+"Requirement.zip", bucketName).getETag();
		if (!CodeUtils.isEmpty(etag)) {
			String s3BucketFilePath = BucketParameter.BUCKETPREFIX + bucketName
					+ CustomParameter.DELIMETER + bucketKey;
			s3BucketFilePath = s3BucketFilePath.split("\\//")[1]
					.replaceAll(bucketName + CustomParameter.DELIMETER, "")
					+ CustomParameter.DELIMETER.trim().toString();
			String url = this.downloadPreSignedURLOnHistory(bucketName,  bucketKey+"Requirement.zip",2048);
			return url;
		}
		return null;
	}
}
