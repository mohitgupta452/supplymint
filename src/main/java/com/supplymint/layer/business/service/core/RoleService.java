package com.supplymint.layer.business.service.core;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.supplymint.layer.data.core.entity.Role;
import com.supplymint.layer.data.core.mapper.RoleMapper;

@Service
public class RoleService {
	
	private RoleMapper roleMapper;

	@Autowired
	public RoleService(RoleMapper roleMapper) {
		this.roleMapper = roleMapper;
	}

	public Role findById(Integer id) {
		return roleMapper.findById(id);
	}

	public List<Role> roleByName() {
		return roleMapper.findAll();
	}

}
