package com.supplymint.layer.business.service.inventory;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.CannotGetJdbcConnectionException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

import com.amazonaws.services.glue.model.GetJobResult;
import com.amazonaws.services.glue.model.GetJobRunResult;
import com.amazonaws.services.glue.model.GetJobsResult;
import com.amazonaws.services.glue.model.Job;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.base.Objects;
import com.supplymint.config.aws.config.AWSGlueConfig;
import com.supplymint.config.aws.utils.AWSUtils;
import com.supplymint.layer.business.contract.inventory.JobDetailService;
import com.supplymint.layer.business.contract.notification.EmailService;
import com.supplymint.layer.business.contract.notification.MailManagerService;
import com.supplymint.layer.data.core.mapper.HeaderConfigMapper;
import com.supplymint.layer.data.downloads.entity.DownloadRuleEngine;
import com.supplymint.layer.data.notification.entity.MailManager;
import com.supplymint.layer.data.notification.entity.MailManagerViewLog;
import com.supplymint.layer.data.tenant.inventory.entity.EnterpriseSpecificOutput;
import com.supplymint.layer.data.tenant.inventory.entity.EnterpriseSpecificOutputNysaa;
import com.supplymint.layer.data.tenant.inventory.entity.HeadersSpecificOutput;
import com.supplymint.layer.data.tenant.inventory.entity.JobDetail;
import com.supplymint.layer.data.tenant.inventory.entity.JobTrigger;
import com.supplymint.layer.data.tenant.inventory.mapper.JobDetailMapper;
import com.supplymint.util.ApplicationUtils.AppEnvironment;
import com.supplymint.util.ApplicationUtils.Module;
import com.supplymint.util.ApplicationUtils.SubModule;
import com.supplymint.util.CodeUtils;

@Service
public class JobDetailServiceImpl implements JobDetailService {

	private JobDetailMapper jobDetailMapper;

	private static final Logger LOGGER = LoggerFactory.getLogger(JobDetailServiceImpl.class);

	@Autowired
	public JobDetailServiceImpl(JobDetailMapper jobDetailMapper) {
		this.jobDetailMapper = jobDetailMapper;

	}
	

	@Autowired
	private EmailService emailService;

	@Autowired
	private AWSGlueConfig awsGlueConfig;

	@Autowired
	private Environment env;

	@Autowired
	private HeaderConfigMapper configMapper;

	@Autowired
	private MailManagerService mailManagerService;

	@Override
	public int createJob(JobDetail jobDetail) {
		return jobDetailMapper.createJob(jobDetail);
	}

	@Override
	public int createTrigger(JobTrigger jobTrigger) {
		return jobDetailMapper.createTrigger(jobTrigger);
	}

	@Override
	public int updateMetaData(JobDetail jobDetail) {
		return jobDetailMapper.updateMetaData(jobDetail);
	}

	@Override
	public JobDetail stopOnDemand() {
		return jobDetailMapper.stopOnDemand();
	}

	@Override
	public List<JobDetail> getAll(Integer offset, Integer pageSize, String orgId, String jobName) {
		return jobDetailMapper.getAll(offset, pageSize, orgId, jobName);
	}

	@Override
	public JobDetail getByJobId(String jobId) {
		return jobDetailMapper.getByJobId(jobId);
	}

	@Override
	public JobDetail getByJobName(String jobName, String orgId) {

		JobDetail jk = jobDetailMapper.getByJobName(jobName, orgId);
		return jk;
	}

	@Override
	public JobDetail updateByJobId(String jobId) {
		return jobDetailMapper.updateByJobId(jobId);
	}

	@Override
	public List<JobDetail> getByState(String jobName,String jobRunState) {
		return jobDetailMapper.getByState(jobName,jobRunState);
	}

	@Override
	public int record(String orgId, String jobName) {
		return jobDetailMapper.record(orgId, jobName);
	}

	@Override
	public JobTrigger getByTriggerId(int id) {
		return jobDetailMapper.getByTriggerId(id);
	}

	@Override
	public int updateState(String jobId, String jobName, String state) {
		return jobDetailMapper.updateState(jobId, jobName, state);
	}

	@Override
	public int validateTrigger(String triggerName) {
		return jobDetailMapper.validateTrigger(triggerName);
	}

	@Override
	public int uTrigger(JobTrigger jobTrigger) {
		return jobDetailMapper.uTrigger(jobTrigger);
	}

	@Override
	public JobTrigger getByTriggerName(String triggerName, String orgId) {
		return jobDetailMapper.getByTriggerName(triggerName, orgId);
	}

	@Override
	public List<JobDetail> filter(Integer offset, Integer pagesize, String from, String to, String orgId,
			String jobName) {

		return jobDetailMapper.filter(offset, pagesize, from, to, orgId, jobName);
	}

	@Override
	public int updateTriggerName(String jobId, String jobName, String triggerName) {
		return jobDetailMapper.updateTriggerName(jobId, jobName, triggerName);
	}

	public int filterRecord(String from, String to, String orgId, String jobName) {
		return jobDetailMapper.filterRecord(from, to, orgId, jobName);
	}

	@SuppressWarnings("unused")
	@Override
	public JobDetail storeJobRunMetaData(GetJobRunResult runResultMetaDeta) {
		JobDetail jobDetails = new JobDetail();
		jobDetails.setSummary("FALSE");
		jobDetails.setJobId(runResultMetaDeta.getJobRun().getId());
		jobDetails.setJobName(runResultMetaDeta.getJobRun().getJobName());
		jobDetails.setJobRunState(runResultMetaDeta.getJobRun().getJobRunState());
		jobDetails.setAllocatedCapacity(runResultMetaDeta.getJobRun().getAllocatedCapacity());
		String ipAddress;
		String bucketKey = null;
		try {
			LOGGER.info("retrive running job meta data to update job details with job Id");
			Map<String, String> arguments = runResultMetaDeta.getJobRun().getArguments();
			for (Entry<String, String> argument : runResultMetaDeta.getJobRun().getArguments().entrySet()) {
				if (argument.getKey().equalsIgnoreCase(AWSUtils.CustomParam.PATH))
					bucketKey = argument.getKey().equalsIgnoreCase(AWSUtils.CustomParam.PATH) ? argument.getValue()
							: AWSUtils.CustomParam.NOT_APPLICABLE;
			}
			jobDetails.setBucketKey(bucketKey);
			String startedOn = CodeUtils._____dateFormat.format(
					CodeUtils.convertDateOnTimeZonePluseForAWS(runResultMetaDeta.getJobRun().getStartedOn(), 5, 30));
			String lastModifiedOn = CodeUtils._____dateFormat.format(CodeUtils
					.convertDateOnTimeZonePluseForAWS(runResultMetaDeta.getJobRun().getLastModifiedOn(), 5, 30));
			jobDetails.setStartedOn(CodeUtils._____dateFormat.parse(startedOn));
			jobDetails.setLastModifiedOn(CodeUtils._____dateFormat.parse(lastModifiedOn));

			ipAddress = InetAddress.getLocalHost().getHostAddress().trim().toString();
			jobDetails.setIpAddress(ipAddress);
		} catch (ParseException | UnknownHostException e) {
			e.printStackTrace();
		}
		Date updationTime = new Date();
		jobDetails.setUpdatedOn(CodeUtils.convertDateOnTimeZonePluseForAWS(updationTime, 5, 30));
		jobDetails.setUpdatedBy("");
		return jobDetails;
	}

	@Override
	public String getLastEngineRun(String jobId) {
		return jobDetailMapper.getLastEngineRun(jobId);
	}

	@Override
	public int validateJobName(String jobName) {
		return jobDetailMapper.validateJobName(jobName);
	}

	@Override
	public int updateStatus(JobDetail jobDetail) {
		return jobDetailMapper.updateStatus(jobDetail);
	}

	@Override
	public int updateNextSchedule(String nextSchedule, String triggerName) {
		return jobDetailMapper.updateNextSchedule(nextSchedule, triggerName);
	}

	@Override
	public JobDetail getLastSummary(String jobName, String orgId) {
		return jobDetailMapper.getLastSummary(jobName, orgId);
	}

	@Override
	public int updateSummary(JobDetail jobDetail) {
		return jobDetailMapper.updateSummary(jobDetail);
	}

	@Override
	public int validateSummaryUpdate(JobDetail jobDetail) {
		return jobDetailMapper.validateSummaryUpdate(jobDetail);
	}

	@Override
	public int validateRow(String jobId) {
		return jobDetailMapper.validateRow(jobId);
	}

	public List<JobDetail> filterSameDate(Integer offset, Integer pagesize, String to, String orgId, String jobName) {
		return jobDetailMapper.filterSameDate(offset, pagesize, to, orgId, jobName);
	}

	public Integer filterSameDateRecord(String to, String orgId, String jobName) {
		return jobDetailMapper.filterSameDateRecord(to, orgId, jobName);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public boolean createJobs(JobDetail jobDetail) {
		LOGGER.info("checking validation for only one row created with job id is null");
		List isValid = this.validatCreateJob(jobDetail.getJobName());
		if (isValid.size() == 0) {
			GetJobResult data = awsGlueConfig.getJobDetails(jobDetail.getJobName());
			jobDetail.setJobName(data.getJob().getName());
			jobDetail.setRole(data.getJob().getRole());
			Date jobCreatedOn = data.getJob().getCreatedOn();
			Date jobModifiedOn = data.getJob().getLastModifiedOn();
			jobDetail.setJobCreatedOn(CodeUtils.convertDateOnTimeZonePluseForAWS(jobCreatedOn, 5, 30));
			jobDetail.setJobModifiedOn(CodeUtils.convertDateOnTimeZonePluseForAWS(jobModifiedOn, 5, 30));
			jobDetail.setScriptLocationOn(data.getJob().getCommand().getScriptLocation());
			String ipAddress;
			try {
				ipAddress = InetAddress.getLocalHost().getHostAddress().trim().toString();
				jobDetail.setIpAddress(ipAddress);
			} catch (UnknownHostException e) {
				e.printStackTrace();
			}
			Date creationTime = new Date();
			jobDetail.setCreatedOn(CodeUtils.convertDateOnTimeZonePluseForAWS(creationTime, 5, 30));
			LOGGER.info("create only one row created with job id is null");
			int result = this.createJob(jobDetail);
			if (!CodeUtils.isEmpty(result)) {
				return true;
			} else {
				return false;
			}
		}
		return false;

	}

	@Override
	public int globalStoreCount(String tableName) {
		return jobDetailMapper.globalStoreCount(tableName);
	}

	@Override
	public int globalitemCount(String tableName) {
		return jobDetailMapper.globalitemCount(tableName);
	}

	@Override
	public String globaltotalCount(String tableName) {
		return jobDetailMapper.globaltotalCount(tableName);
	}

	@Override
	public int globalupdateCount(JobDetail jobDetail) {
		return jobDetailMapper.globalupdateCount(jobDetail);
	}

	@Override
	public int globalrecordRAO(String tableName) {
		return jobDetailMapper.globalrecordRAO(tableName);
	}

	@Override
	public List<HeadersSpecificOutput> globalAllRAO(Integer offset, Integer pagesize, String tableName) {
		return jobDetailMapper.globalAllRAO(offset, pagesize, tableName);
	}

	@Override
	public int globalSummaryFilterRecord(String storeCode, String tableName) {
		return jobDetailMapper.globalSummaryFilterRecord(storeCode, tableName);
	}

	@Override
	public List<HeadersSpecificOutput> globalSummaryFilter(Integer offset, Integer pagesize, String storeCode,
			String tableName) {
		return jobDetailMapper.globalSummaryFilter(offset, pagesize, storeCode, tableName);
	}

	@Override
	public List<String> globalAllStoreCode(String tableName) {
		return jobDetailMapper.globalAllStoreCode(tableName);
	}

	@Override
	public List<String> getWhCode(String tableName) {
		return jobDetailMapper.getWhCode(tableName);
	}

	@Override
	public List<DownloadRuleEngine> downloadExcelSummary(String whCode) {
		return jobDetailMapper.downloadExcelSummary(whCode);
	}

	@Override
	public List<EnterpriseSpecificOutput> downloadExcelSummaryForSkecters(String whCode) {
		return jobDetailMapper.downloadExcelSummaryForSkecters(whCode);
	}

	@Override
	public int updateFileName(String folderName, String arguments, String jobId) {
		return jobDetailMapper.updateFileName(folderName, arguments, jobId);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List validatCreateJob(String jobName) {
		return jobDetailMapper.validateJobCreation(jobName);
	}

	@Override
	public int updateLastEngine(String jobId, String jobName) {
		return jobDetailMapper.updateLastEngine(jobId, jobName);
	}

	@Override
	public int updatePreviousEngine(String jobId) {
		return jobDetailMapper.updatePreviousEngine(jobId);
	}

	@Override
	public int updateTransferOrderKey(String jobId, String bucketKey, int mailCount) {
		return jobDetailMapper.updateJobGenerateTO(jobId, bucketKey, mailCount);
	}

	public JobDetail getLastEngineRunDetails(String jobName) {
		return jobDetailMapper.getLastEngineDetails(jobName);
	}

	@Override
	public String getPreviousJobId() {
		return jobDetailMapper.getPreviousJobId();
	}

	@Override
	public int updatePreviousLastEngine(String jobId, String jobName) {
		return jobDetailMapper.updatePreviousLastEngine(jobId, jobName);
	}

	@Override
	public int updateCurrentLastEngine(String jobId) {
		return jobDetailMapper.updateCurrentLastEngine(jobId);
	}

	@Override
	public List<String> fiveSucceededTime() {
		return jobDetailMapper.fiveSucceededTime();
	}

	@Override
	public int updateEndTimeWithRepDate(String jobId, Date endDate, String duration) {
		return jobDetailMapper.updateEndTimeWithRepDate(jobId, endDate, duration);
	}

	@Override
	public List<EnterpriseSpecificOutputNysaa> downloadExcelSummaryForNysaa1() {
		return jobDetailMapper.downloadExcelSummaryForNysaa1();
	}

	@Override
	public List<EnterpriseSpecificOutputNysaa> downloadExcelSummaryForNysaa2() {
		return jobDetailMapper.downloadExcelSummaryForNysaa2();
	}

	@Override
	public List<HeadersSpecificOutput> getDownloadExcelSummaryData(String whCode, String tableName) {
		return jobDetailMapper.getDownloadExcelSummaryData(whCode, tableName);
	}

	@Override
	public ObjectNode getCustomHeaders(String isDefault, String attributeType, String displayName, String orgId)
			throws Exception {
		ObjectMapper objMapper = new ObjectMapper();
		ObjectNode objectNode = objMapper.createObjectNode();
		try {
			LOGGER.info("query for fetching header has started...");
			String jsonOfFixedHeaders = jobDetailMapper.getCustomHeaders(isDefault, attributeType, displayName, orgId);
			LOGGER.info("query for fetching header has ended...");
			ObjectMapper mapper = new ObjectMapper();
			JsonNode actualObj = mapper.readTree(jsonOfFixedHeaders);
			objectNode.putPOJO(CodeUtils.RESPONSE, actualObj);
		} catch (CannotGetJdbcConnectionException ex) {
			throw ex;
		} catch (DataAccessException ex) {
			throw ex;
		} catch (Exception ex) {
			throw ex;
		}
		return objectNode;
	}

	@SuppressWarnings({ "unchecked" })
	public Map<String, List<String>> getHeadersData() throws Exception {
		Map<String, List<String>> mapOfHeader = new HashMap<String, List<String>>();
		List<String> listOfKey = new ArrayList<>();
		List<String> listOfValue = new ArrayList<>();
		ObjectMapper mapper = new ObjectMapper();
		String jsonOfFixedHeaders = jobDetailMapper.getCustomHeader();
		JsonNode jsonNode = mapper.readTree(jsonOfFixedHeaders);
		Map<String, String> map = mapper.treeToValue(jsonNode, Map.class);
		Set<String> getKey = map.keySet();
		for (String str : getKey) {
			listOfKey.add(str);
			listOfValue.add(map.get(str));
		}
		mapOfHeader.put("key", listOfKey);
		mapOfHeader.put("value", listOfValue);

		return mapOfHeader;
	}

	@Override
	public int updateLastEngineRun() {
		return jobDetailMapper.updateLastEngineRun();
	}

	@Override
	public int updatePreviousSchedule(String previousNextSchedule, String triggerName) {
		return jobDetailMapper.updatePreviousSchedule(previousNextSchedule, triggerName);
	}

	@Override
	public void updateMailStatus(String jobId) {
		jobDetailMapper.updateMailStatus(jobId);

	}

	@Override
	public int globalrecordReq(String tableName) {
		return jobDetailMapper.globalrecordReq(tableName);
	}

	@Override
	public List<HeadersSpecificOutput> globalAllReq(int offset, int pageSize, String tableName) {
		return jobDetailMapper.globalAllReq(offset, pageSize, tableName);
	}

	@Override
	public List<HeadersSpecificOutput> globalSummaryFilterReq(int offset, int pageSize, String storeCode,
			String tableName) {
		return jobDetailMapper.globalSummaryFilterReq(offset, pageSize, storeCode, tableName);
	}

	@Override
	public int globalSummaryFilterRecordReq(String storeCode, String tableName) {
		return jobDetailMapper.globalSummaryFilterRecordReq(storeCode, tableName);
	}

	@SuppressWarnings("unchecked")
	@Override
	public String getLabelOfStoreCode(String value, String displayName) {
		String key = null;
		try {
			ObjectMapper mapper = new ObjectMapper();
			String customJsonString = configMapper.getFHeaders(displayName);
			Map<String, String> toMap = mapper.readValue(customJsonString, Map.class);
			boolean b = toMap.containsValue(value);
			Set<Entry<String, String>> mapEntry = toMap.entrySet();
			if (b) {
				for (Entry<String, String> entry : mapEntry) {
					if (Objects.equal(value, entry.getValue())) {
						key = entry.getKey();

					}
				}
			} else {
				return null;
			}

		} catch (CannotGetJdbcConnectionException ex) {
			throw ex;
		} catch (DataAccessException ex) {
			throw ex;
		} catch (Exception ex) {
			try {
				throw ex;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return key;
	}

	@Override
	public int updateOrgId(String orgId, String jobId) {
		return jobDetailMapper.updateOrgId(orgId, jobId);
	}

	@Override
	public ObjectNode getFiveTriggers(String orgId, int enterpriseId, String anything) {
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode node = mapper.createObjectNode();
		ObjectNode insideNode = mapper.createObjectNode();
		List<JobTrigger> jobTriggers = null;
		List<Job> jobs = null;

		List<String> jobNames = new ArrayList<>();
		try {
			jobTriggers = jobDetailMapper.getFiveTriggers(orgId, enterpriseId);
			GetJobsResult getJobsDetails = awsGlueConfig.getAllJobsDetail();
			jobs = getJobsDetails.getJobs();

			jobs.stream().forEach(e -> {
				String jobName = e.getName();
				String environment = null;
				if (jobName.contains("-")) {
					String[] envDetails = env.getActiveProfiles();
					if (envDetails.length > 0) {
						environment = envDetails[0].equalsIgnoreCase(AppEnvironment.Profiles.DEV.name()) ? "DEV"
								: "PROD";
						String[] arrayOfJobString = jobName.split("-");
						if (anything.equalsIgnoreCase(arrayOfJobString[1])
								&& arrayOfJobString[2].equalsIgnoreCase(environment)) {
							jobNames.add(jobName);
						}

					}
				}

			});
			insideNode.putPOJO("jobNames", jobNames);
			insideNode.putPOJO("jobTriggers", jobTriggers);

			node.putPOJO(CodeUtils.RESPONSE, insideNode);
		} catch (CannotGetJdbcConnectionException ex) {
			throw ex;
		} catch (DataAccessException ex) {
			throw ex;
		} catch (Exception ex) {
			try {
				throw ex;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return node;
	}

	@Override
	public int countTriggerTest(String orgId, int enterpriseId) {
		int result = 0;
		try {
			result = jobDetailMapper.countTriggerTest(orgId, enterpriseId);

		} catch (Exception ex) {
			throw ex;
		}
		return result;
	}

	@Override
	public int deleteTrigger(String triggerName, String orgId) {

		return jobDetailMapper.deleteTrigger(triggerName, orgId);
	}

	@Override
	public String getPresentTriggers(int maxTrigger, String triggerName, String orgId, int enterpriseId) {

		List<String> getPresentTrigger = jobDetailMapper.getPresentTriggers(orgId, enterpriseId);
		List<Integer> totalTrigger = new ArrayList<>();
		List<Integer> addNumber = new ArrayList<>();
		List<Integer> uncommon = new ArrayList<>();
		for (int i = 1; i <= maxTrigger; i++) {
			totalTrigger.add(i);
		}

		if (getPresentTrigger.size() == 0) {
			return triggerName + "1";
		} else {
			getPresentTrigger.stream().forEach(e -> {
				String retrieveNumber = e.substring(e.length() - 1, e.length());
				addNumber.add(Integer.parseInt(retrieveNumber));
			});
			for (Integer s : totalTrigger) {
				if (!addNumber.contains(s))
					uncommon.add(s);
			}
			for (Integer s : addNumber) {
				if (!totalTrigger.contains(s))
					uncommon.add(s);
			}
			Collections.sort(uncommon);
			int element = uncommon.get(0);
			String newTriggerName = triggerName + element;
			return newTriggerName;
		}
	}

	@Override
	public Date getPastScheduleTime(String orgId, int enterpriseId) {
		return jobDetailMapper.getPastSchedultTime(orgId, enterpriseId);
	}


	public boolean sendMail(String generation,String state,String enterprise,String jobId,String orgId) {
		boolean isSendEmail = false;
		String[] cc = null;
		String[] to = null;
		MailManagerViewLog mailManagerViewLog = new MailManagerViewLog();

		try {

			MailManager mailManager = mailManagerService.getByStatus(generation,
					state, orgId);
			String ipAddress = InetAddress.getLocalHost().getHostAddress().trim().toString();
			if (!CodeUtils.isEmpty(mailManager)) {
				if (mailManager.getCc() != null) {
					cc = mailManager.getCc().trim().split(",");
				}
				if (mailManager.getTo() != null) {
					to = mailManager.getTo().trim().split(",");
				}
				mailManagerViewLog.setCc(mailManager.getCc());
				mailManagerViewLog.setBcc(mailManager.getBcc());
				mailManagerViewLog.setTo(mailManager.getTo());
				mailManagerViewLog.setCreatedOn(CodeUtils.convertDateOnTimeZonePluseForAWS(new Date(), 5, 30));
				mailManagerViewLog.setDeliveredOn(CodeUtils.convertDateOnTimeZonePluseForAWS(new Date(), 5, 30));
				mailManagerViewLog.setBody(generation);
				mailManagerViewLog.setModule(Module.INVENTRY_PLANNING.toString());
				mailManagerViewLog.setSubModule(SubModule.RUN_ON_DEMAND.toString());
				mailManagerViewLog.setIpAddress(ipAddress);
				if (!CodeUtils.isEmpty(mailManager)) {
					SimpleMailMessage mailMessage = new SimpleMailMessage();
					mailMessage.setCc(cc);
					mailMessage.setTo(to);
					mailMessage.setBcc(mailManager.getBcc());
					mailMessage.setSubject(mailManager.getSubject());
					Date updationTime = new Date();
					String updatedOn = CodeUtils._____dateFormat
							.format(CodeUtils.convertDateOnTimeZonePluseForAWS(updationTime, 5, 30));

					String url = null;
					emailService.sendEmailUsingMailTemplate(mailMessage, mailManager.getTemplateName(),
							enterprise, jobId, updatedOn, url);
					isSendEmail = true;
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return isSendEmail;

	}
	
	@Override
	public int updateMailCounter(String jobId,int mailCount,String orgId) {
		return jobDetailMapper.updateMailCounter(jobId, mailCount,orgId);
	}
	@Override
	public JobDetail getJobDetailByJobId(String jobId,String orgId) {
		return jobDetailMapper.getJobDetailByJobId(jobId,orgId);
		
		
	}

}
