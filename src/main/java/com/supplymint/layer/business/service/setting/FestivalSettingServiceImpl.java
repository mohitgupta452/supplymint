package com.supplymint.layer.business.service.setting;

import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.ibm.icu.text.SimpleDateFormat;
import com.supplymint.layer.business.contract.setting.FestivalSettingService;
import com.supplymint.layer.data.setting.entity.FestivalSetting;
import com.supplymint.layer.data.setting.mapper.FestivalSettingMapper;
import com.supplymint.util.CodeUtils;
import com.supplymint.util.DateUtils;

@Service
public class FestivalSettingServiceImpl implements FestivalSettingService {

	private static final Logger LOGGER = LoggerFactory.getLogger(FestivalSettingServiceImpl.class);

	private FestivalSettingMapper festivalSettingMapper;

	@Autowired
	public FestivalSettingServiceImpl(FestivalSettingMapper festivalSettingMapper) {
		this.festivalSettingMapper = festivalSettingMapper;

	}

	@SuppressWarnings("unused")
	@Autowired
	private HttpServletRequest servletRequest;


	@SuppressWarnings({ "rawtypes" })
	public int createNew(List<FestivalSetting> defaultFestivalList, List<FestivalSetting> customFestivalList,
			HttpServletRequest servletRequest) {

//		if(CodeUtils.isEmpty(customFestivalList)) {
//			return 0;
//		}
		int insertData = 0;
		String customFestival = "CUSTOM_FESTIVAL";
		FestivalSetting festivals = new FestivalSetting();
		List<Map<String, Map>> fesList1 = new LinkedList<Map<String, Map>>();
		List<Map<String, Map>> fesList2 = new LinkedList<Map<String, Map>>();
		Map<String, Map> festivalMap = new LinkedHashMap<String, Map>();
		Map<String, Map> festivalMap1 = new LinkedHashMap<String, Map>();
		Map<String, Map> festivalMap2 = new LinkedHashMap<String, Map>();
		festivals.setFestivalType(customFestival);
		try {
			if (!CodeUtils.isEmpty(customFestivalList)) {
				
				for (FestivalSetting cs : customFestivalList) {
					if (cs.getIsChecked().equalsIgnoreCase("true")) {
						Map<String, String> fesMap = new LinkedHashMap<String, String>();
						Date startDate = DateUtils.stringToDate(cs.getStartDate());
						Date endDate = DateUtils.stringToDate(cs.getEndDate());
						fesMap.put("startDate", new SimpleDateFormat("yyyy-MM-dd").format(startDate));
						fesMap.put("endDate", new SimpleDateFormat("yyyy-MM-dd").format(endDate));
						festivalMap1.put(cs.getName(), fesMap);
					}
					fesList1.add(festivalMap);
				}
			}
			if (!CodeUtils.isEmpty(defaultFestivalList)) {
				for (FestivalSetting ds : defaultFestivalList) {
					if (ds.getIsChecked().equalsIgnoreCase("true")) {
						Map<String, String> fesMap = new LinkedHashMap<String, String>();
						Date startDate = DateUtils.stringToDate(ds.getStartDate());
						Date endDate = DateUtils.stringToDate(ds.getEndDate());
						fesMap.put("startDate", new SimpleDateFormat("yyyy-MM-DD").format(startDate));
						fesMap.put("endDate", new SimpleDateFormat("yyyy-MM-DD").format(endDate));
						festivalMap2.put(ds.getName(), fesMap);
					}
					fesList2.add(festivalMap);
				}
			}
			festivalMap.putAll(festivalMap1);
			festivalMap.putAll(festivalMap2);
			List<Map<String, Map>> fesList = new LinkedList<Map<String, Map>>();
			fesList.add(festivalMap);
			ObjectMapper objectMapper = new ObjectMapper();
			String newList = objectMapper.writeValueAsString(fesList);

			festivalSettingMapper.updateStatus(festivals.getFestivalType());
			String ipAddress = InetAddress.getLocalHost().getHostAddress().trim().toString();
			String createdBy = servletRequest.getAttribute("email").toString();
			String userName = servletRequest.getAttribute("name").toString();
			Date createdOn = CodeUtils.convertDateOnTimeZonePluseForAWS(new Date(), 5, 30);
			festivals.setStatus("TRUE");
			festivals.setCreatedBy(createdBy);
			festivals.setIpAddress(ipAddress);
			festivals.setCreatedOn(createdOn);
			festivals.setActive("1");
			festivals.setUserName(userName);
			festivals.setFestivalList(newList);
			LOGGER.info("starting method/query to create new festival...");
			insertData = festivalSettingMapper.create(festivals);
			LOGGER.info("ended method/query to create new festival...");
		} catch (IOException e) {

		}
		return insertData;
	}

	@SuppressWarnings("rawtypes")
	public ObjectNode getAllNew() throws NullPointerException {
		ObjectMapper nodeMapper = new ObjectMapper();
		ObjectNode objectNode = nodeMapper.createObjectNode();
		try {
			LOGGER.info("starting method/query to get all checked list...");
			String defaultFestivalName = festivalSettingMapper.getCheckFestivalName();
			LOGGER.info("ending method/query to get all checked list...");
			if(CodeUtils.isEmpty(defaultFestivalName))
				return null;
			ObjectMapper mapper = new ObjectMapper();
			Set<String> set = new HashSet<String>();
			JsonNode actualObj = mapper.readTree(defaultFestivalName);
			Iterator itr = actualObj.iterator();
			Map<String, Map> checkMap = new LinkedHashMap<>();
			while (itr.hasNext()) {
				JsonNode json = (JsonNode) itr.next();
				Map<String, String> toMap = null;
				Iterator<Map.Entry<String, JsonNode>> fields = json.fields();
				while (fields.hasNext()) {
					Map.Entry<String, JsonNode> entry = fields.next();
					if (set.add(entry.getKey())) {
						toMap=new LinkedHashMap<String, String>();
						toMap.put("startDate", new SimpleDateFormat("dd MMM yyyy")
								.format(DateUtils.stringToDate(entry.getValue().get("startDate").asText())));
						toMap.put("endDate", new SimpleDateFormat("dd MMM yyyy")
								.format(DateUtils.stringToDate(entry.getValue().get("endDate").asText())));
						checkMap.put(entry.getKey(), toMap);
					}
				}
			}
			objectNode.putPOJO(CodeUtils.RESPONSE, checkMap);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return objectNode;

	}

	@Override
	public int countCustomList() {
		LOGGER.info("starting method/query to get all custom list...");
		String customFestivalName = festivalSettingMapper.getFestivalName("CUSTOM_FESTIVAL");
		LOGGER.info("ending method/query to get all custom list...");
		String[] arrOfdefaultCustomFestivalName = customFestivalName.trim().split(",");
		return arrOfdefaultCustomFestivalName.length;
	}

	@SuppressWarnings({ "deprecation", "rawtypes" })
	@Override
	public ObjectNode getAllCustomList(int offset, int pageSize) {
		Set<String> set = new HashSet<String>();

		ObjectMapper nodeMapper = new ObjectMapper();
		ObjectNode node = nodeMapper.createObjectNode();
		List<Map<String, String>> jsonTypeMap = new ArrayList<Map<String, String>>();
		List<Map<String, String>> lastList = new ArrayList<Map<String, String>>();
		try {
			LOGGER.info("starting method/query to get all custom & checked list...");
			String customFestivalName = festivalSettingMapper.getFestivalName("CUSTOM_FESTIVAL");
			String checkedFestivalName = festivalSettingMapper.getCheckFestivalName();
			LOGGER.info("endded method/query to get all custom & checked list...");
			String[] arrOfdefaultCustomFestivalName = customFestivalName.trim().split(",");
			int customSize = arrOfdefaultCustomFestivalName.length;
			int totalSize = offset + pageSize;
			if (totalSize > customSize) {
				totalSize = customSize;
			}
			if (!CodeUtils.isEmpty(checkedFestivalName)) {
				JsonNode jsonNode = nodeMapper.readTree(checkedFestivalName);
				Iterator itr = jsonNode.iterator();
				while (itr.hasNext()) {
					JsonNode json = (JsonNode) itr.next();
					Iterator<Map.Entry<String, JsonNode>> fields = json.fields();
					while (fields.hasNext()) {
						Map.Entry<String, JsonNode> entry = fields.next();
						if (set.add(entry.getKey())) {
							Map<String, String> toMap = new LinkedHashMap<String, String>();
							toMap.put("name", entry.getKey());
							toMap.put("isChecked", "true");
							toMap.put("startDate", new SimpleDateFormat("dd MMM yyyy")
									.format(DateUtils.stringToDate(entry.getValue().get("startDate").asText())));
							toMap.put("endDate", new SimpleDateFormat("dd MMM yyyy")
									.format(DateUtils.stringToDate(entry.getValue().get("endDate").asText())));
							jsonTypeMap.add(toMap);
						}
					}
				}
			}
			for (int i = 0; i < customSize; i++) {
				if (set.add(arrOfdefaultCustomFestivalName[i])) {
					Map<String, String> toMap = new LinkedHashMap<String, String>();
					toMap.put("name", arrOfdefaultCustomFestivalName[i]);
					toMap.put("isChecked", "false");
					toMap.put("startDate", "");
					toMap.put("endDate", "");
					jsonTypeMap.add(toMap);
				}
			}
			for (int i = offset; i < totalSize; i++) {
				lastList.add(jsonTypeMap.get(i));

			}
			LOGGER.info("lastList of  custom festival %s" , lastList);
			ObjectNode mainNode = nodeMapper.createObjectNode();
			mainNode.putPOJO("customFestival", lastList);
			node.put(CodeUtils.RESPONSE, mainNode);

		} catch (Exception e) {

		}
		return node;
	}

	@SuppressWarnings("deprecation")
	@Override
	public ObjectNode getAllDefaultListOldMethod() {
		LOGGER.info("starting method/query to get all default list...");
		String defaultFestivalName = festivalSettingMapper.getFestivalName("DEFAULT_FESTIVAL");
		LOGGER.info("ended method/query to get all default list...");
		String[] arrOfdefaultFestivalName = defaultFestivalName.trim().split(",");
		Set<String> set = new HashSet<String>();
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode node = mapper.createObjectNode();
		ArrayNode defaultFestivalArrayNode = mapper.createArrayNode();
		try {
			for (String value : arrOfdefaultFestivalName) {
				ObjectNode tempNode = mapper.createObjectNode();
				if (set.add(value)) {
					tempNode.put("name", value);
				}
				defaultFestivalArrayNode.add(tempNode);
			}
			ObjectNode mainNode = mapper.createObjectNode();
			mainNode.put("defaultFestival", defaultFestivalArrayNode);
			node.put(CodeUtils.RESPONSE, mainNode);
		} catch (Exception e) {

		}

		return node;
	}

	@SuppressWarnings({ "deprecation", "rawtypes" })
	@Override
	public ObjectNode getAllDefaultList() {
		LOGGER.info("starting method/query to get all default list...");
		String defaultFestivalName = festivalSettingMapper.getFestivalName("DEFAULT_FESTIVAL");
		LOGGER.info("ended method/query to get all default list...");
		Set<String> set = new HashSet<String>();
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode node = mapper.createObjectNode();
		List<Map<String, String>> jsonTypeMap = new ArrayList<Map<String, String>>();
		try {
			if (!CodeUtils.isEmpty(defaultFestivalName)) {
				JsonNode jsonNode = mapper.readTree(defaultFestivalName);
				Iterator itr = jsonNode.iterator();
				while (itr.hasNext()) {
					JsonNode json = (JsonNode) itr.next();
					Map<String, String> toMap = new LinkedHashMap<String, String>();
					if (set.add(json.get("name").asText())) {
						toMap.put("name", json.get("name").asText());
						toMap.put("startDate", new SimpleDateFormat("dd MMM yyyy")
								.format(DateUtils.stringToDate(json.get("startDate").asText())));
						toMap.put("endDate", new SimpleDateFormat("dd MMM yyyy")
								.format(DateUtils.stringToDate(json.get("endDate").asText())));
						jsonTypeMap.add(toMap);
					}
				}
			}

			ObjectNode mainNode = mapper.createObjectNode();
			mainNode.putPOJO("defaultFestival", jsonTypeMap);
			node.put(CodeUtils.RESPONSE, mainNode);
		} catch (Exception e) {

		}

		return node;
	}

	

}
