package com.supplymint.layer.business.service.administration;

import static java.nio.file.StandardOpenOption.CREATE_NEW;

import java.io.OutputStream;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.supplymint.layer.business.contract.administration.ADMSiteService;
import com.supplymint.layer.data.tenant.administration.entity.ADMSite;
import com.supplymint.layer.data.tenant.administration.entity.ADMSiteDetail;
import com.supplymint.layer.data.tenant.administration.mapper.ADMSiteMapper;

@Service
public class ADMSiteServiceImpl implements ADMSiteService {

	private ADMSiteMapper admSiteMapper;

	@Autowired
	public ADMSiteServiceImpl(ADMSiteMapper admSiteMapper) {
		this.admSiteMapper = admSiteMapper;
	}

	@Override
	public ADMSite getById(Integer id) {
		return admSiteMapper.findById(id);
	}

	@Override
	public List<ADMSite> findAll(Integer offset, Integer pagesize) {

		return admSiteMapper.findAll(offset, pagesize);
	}

	@Override
	public Integer create(ADMSite aDMSite) {
		return admSiteMapper.create(aDMSite);
	}

	@Override
	public Integer udpate(ADMSite aDMSite) {
		return admSiteMapper.update(aDMSite);
	}

	@Override
	public Integer delete(int id) {
		return admSiteMapper.delete(id);
	}

	@Override
	public Integer createSiteDetail(ADMSiteDetail admSiteDetail) {
		return admSiteMapper.createSiteDetail(admSiteDetail);
	}

	@Override
	public Integer udpateSiteDetail(ADMSiteDetail admSiteDetail) {
		return admSiteMapper.udpateSiteDetail(admSiteDetail);
	}

	@Override
	public Integer deleteSiteDetail(int id) {
		return admSiteMapper.deleteSiteDetail(id);
	}

	@Override
	public Integer record() {
		return admSiteMapper.record();
	}

	@Override
	public List<ADMSite> getAllSite() {
		return admSiteMapper.getAllSite();

	}

	public List<ADMSite> filter(ADMSite admSite) {

		return admSiteMapper.filter(admSite);

	}

	@Override
	public List<ADMSite> searchAll(Integer offset, Integer pagesize, String search) {
		return admSiteMapper.searchAll(offset, pagesize, search);
	}

	@Override
	public int searchRecord(String search) {
		return admSiteMapper.searchRecord(search);
	}

	@Override
	public Integer deleteSiteMap(int id) {
		return admSiteMapper.deleteSiteMap(id);
	}

	@Override
	public List<ADMSiteDetail> checkSiteExistance(ADMSiteDetail admSiteDetail) {
		return admSiteMapper.checkSiteExistance(admSiteDetail);

	}

	@Override
	public List<ADMSiteDetail> checkSiteUpdateExistance(ADMSiteDetail admSiteDetail) {
		return admSiteMapper.checkSiteUpdateExistance(admSiteDetail);
	}

	@Override
	public Integer filterRecord(ADMSite admSite) {
		return admSiteMapper.filterRecord(admSite);
	}

	@Override
	public List<ADMSite> filterSite(int offset, int pageSize, ADMSite admSite) {
		// int totalRecord = 0;
		List<ADMSite> data = null;

		admSite.setName(admSite.getName().trim());
		admSite.setCode(admSite.getCode().trim());
		admSite.setDisplayName(admSite.getDisplayName().trim());
		admSite.setCountry(admSite.getCountry().trim());
		admSite.setState(admSite.getState().trim());
		admSite.setZipCode(admSite.getZipCode().trim());
		admSite.setStatus(admSite.getStatus().trim());

		admSite.setOffset(offset);
		admSite.setPagesize(pageSize);

		data = filter(admSite);

		return data;
	}

	@Override
	public List<ADMSite> getAllRecords() {
		return admSiteMapper.getAllRecord();
	}

	@SuppressWarnings("resource")
	@Override
	public void generateSiteListTOExcel(List<ADMSite> list, String filePath) {
		Workbook workbook = new XSSFWorkbook();
		Sheet sheet = workbook.createSheet("SITE");

		int rowIndex = 1;
		Row row1 = sheet.createRow(0);
		row1.createCell(0).setCellValue("NAME");
		row1.createCell(1).setCellValue("CODE");
		row1.createCell(2).setCellValue("DISPLAY NAME");
		row1.createCell(3).setCellValue("COUNTRY");
		row1.createCell(4).setCellValue("STATE");
		row1.createCell(5).setCellValue("CITY");
		row1.createCell(6).setCellValue("STATUS");

		for (ADMSite site : list) {
			Row row = sheet.createRow(rowIndex++);
			int cellIndex = 0;
			row.createCell(cellIndex++).setCellValue(site.getName());
			row.createCell(cellIndex++).setCellValue(site.getCode());
			row.createCell(cellIndex++).setCellValue(site.getDisplayName());
			row.createCell(cellIndex++).setCellValue(site.getCountry());
			row.createCell(cellIndex++).setCellValue(site.getState());
			row.createCell(cellIndex++).setCellValue(site.getCity());
			row.createCell(cellIndex++).setCellValue(site.getStatus());
		}
		try {
			Path path = Paths.get(filePath);
			try (OutputStream out = Files.newOutputStream(path, CREATE_NEW)) {
				workbook.write(out);
				out.close();
			} catch (FileAlreadyExistsException incrCounterAndRetry) {
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

//		try {
//			FileOutputStream fos = new FileOutputStream(filePath);
//			workbook.write(fos);
//			fos.close();
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
	}

	@Override
	public Map<String, List<String>> getSiteHeaders() {

		Map<String, List<String>> map = new HashMap<String, List<String>>();
		List<String> listOfKey = new ArrayList<String>();
		List<String> listOfValue = new ArrayList<String>();

		listOfValue.add("NAME");
		listOfValue.add("CODE");
		listOfValue.add("DISPLAY NAME");
		listOfValue.add("COUNTRY");
		listOfValue.add("STATE");
		listOfValue.add("CITY");
		listOfValue.add("STATUS");

		listOfKey.add("name");
		listOfKey.add("code");
		listOfKey.add("displayName");
		listOfKey.add("country");
		listOfKey.add("state");
		listOfKey.add("city");
		listOfKey.add("status");

		map.put("key", listOfKey);
		map.put("value", listOfValue);

		return map;

	}

	@Override
	public int batchSiteInsert(List<ADMSite> listAdmSite) {
		return admSiteMapper.batchSiteInsert(listAdmSite);
	}

	@Override
	public int countPOSite(String username) {
		return admSiteMapper.countPOSite(username);
	}

	@Override
	public List<Map<String, String>> getPOSite(String username, int offset, int pagesize) {
		return admSiteMapper.getPOSite(username, offset, pagesize);
	}

	@Override
	public int countSearchPOSite(String username, String search) {
		return admSiteMapper.countSearchPOSite(username, search);
	}

	@Override
	public List<Map<String, String>> getSearchPOSite(String username, String search, int offset, int pagesize) {
		return admSiteMapper.getSearchPOSite(username, search, offset, pagesize);
	}

	@Override
	public Map<String, String> getDefaultPOSite(String username) {
		return admSiteMapper.getDefaultPOSite(username);
	}

}
