package com.supplymint.layer.business.service.inventory;

import org.springframework.stereotype.Service;

import com.supplymint.layer.business.contract.inventory.AllocationService;
import com.supplymint.layer.data.tenant.inventory.entity.Allocation;
import com.supplymint.layer.data.tenant.inventory.mapper.AllocationMapper;

@Service
public class AllocationServiceImpl implements AllocationService {

	private AllocationMapper allocationMapper;

	public AllocationServiceImpl(AllocationMapper allocationMapper) {

		this.allocationMapper = allocationMapper;

	}

	@Override
	public Integer insert(Allocation allocation) {

		return allocationMapper.create(allocation);
	}
	
	public Integer deleteById(Integer id) {
		return allocationMapper.removeById(id);
		
	}
	

}
