package com.supplymint.layer.business.service.core;

import java.net.InetAddress;
import java.util.Date;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.CannotGetJdbcConnectionException;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.supplymint.layer.data.config.ThreadLocalStorage;
import com.supplymint.layer.data.core.entity.HeaderConfig;
import com.supplymint.layer.data.core.mapper.HeaderConfigMapper;
import com.supplymint.layer.data.tenant.inventory.entity.HeaderConfigLog;
import com.supplymint.layer.web.endpoint.abstracts.AbstractEndpoint;
import com.supplymint.layer.web.endpoint.core.HeaderConfigEndpoint;
import com.supplymint.util.CodeUtils;

@Service
public class HeaderConfigService{

	private static final Logger LOGGER = LoggerFactory.getLogger(HeaderConfigEndpoint.class);
	private HeaderConfigMapper configMapper;

	@Autowired
	public HeaderConfigService(HeaderConfigMapper configMapper) {
		this.configMapper = configMapper;
	}

	public int create(HeaderConfig headerConfiguration) throws Exception {
		int result = 0;
		String fixedHeader = null;
		String defaultHeader = null;
		String id = null;
		HeaderConfigLog headerConfigLogs = new HeaderConfigLog();
		JsonNode fixedHeaders = headerConfiguration.getFixedHeaders();
		JsonNode defaultHeaders = headerConfiguration.getDefaultHeaders();

		try {
			Date createdOn = CodeUtils.convertDateOnTimeZonePluseForAWS(new Date(), 5, 30);
			id = UUID.randomUUID().toString();
			String ipAddress = InetAddress.getLocalHost().getHostAddress().trim().toString();
			headerConfiguration.setId(id);
			fixedHeader = fixedHeaders.toString();
			defaultHeader = defaultHeaders.toString();
			headerConfiguration.setFixedHeader(fixedHeader);
			headerConfiguration.setDefaultHeader(defaultHeader);
			headerConfiguration.setCreatedTime(createdOn);
			headerConfiguration.setStatus("ACTIVE");
			headerConfiguration.setActive("1");
			headerConfiguration.setIpAddress(ipAddress);
			LOGGER.info("Header configuratioin save query is started...");
			configMapper.updateStatus(id, headerConfiguration.getEnterpriseName());
			result = configMapper.create(headerConfiguration);
			LOGGER.info("Header configuratioin save query is ended...");
			if (result != 0) {
				headerConfigLogs.setId(headerConfiguration.getId());
				headerConfigLogs.setTypeConfig(headerConfiguration.getTypeConfig());
				headerConfigLogs.setDisplayName(headerConfiguration.getDisplayName());
				headerConfigLogs.setCustomHeaders(headerConfiguration.getDefaultHeader());
				headerConfigLogs.setConfig("TRUE");
				if (!CodeUtils.isEmpty(defaultHeaders)) {
					headerConfigLogs.setIsDefault("TRUE");
				}
				headerConfigLogs.setStatus("ACTIVE");
				headerConfigLogs.setCreatedTime(headerConfiguration.getCreatedTime());
				headerConfigLogs.setActive("1");
				headerConfigLogs.setIpAddress(ipAddress);
				headerConfigLogs.setAttributeType(headerConfiguration.getAttributeType());
				String token = headerConfiguration.getToken();
				String xTenantId = CodeUtils.decode(token).get("X-TENANT-ID").getAsString();
				ThreadLocalStorage.setTenantHashKey(xTenantId);
				configMapper.updateHeaderLogsStatus(headerConfigLogs.getId());
				configMapper.createHeaderConfigLogs(headerConfigLogs);

			}
		} catch (CannotGetJdbcConnectionException ex) {
			throw ex;
		} catch (DataAccessException ex) {
			throw ex;
		} catch (Exception ex) {
			throw ex;
		}
		return result;

	}

	public ObjectNode getHeaders(String enterpriseName, String attributeType, String displayName) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode objectNode=mapper.createObjectNode();
		ObjectNode combinationObjectNode = mapper.createObjectNode();
		try {
			LOGGER.info("Query for fetching data from HEADER_CONFIGURATION_LOGS started....");
			String jsonOfFixedHeaders = configMapper.getFixedHeaders(enterpriseName, attributeType, displayName);
			LOGGER.info("Query for fetching data from HEADER_CONFIGURATION_LOGS ended....");
			String jsonOfDefaultHeaders = configMapper.getDefaultHeaders(enterpriseName, attributeType, displayName);
			String jsonOfCustomHeaders = configMapper.getCHeaders(enterpriseName, attributeType,displayName);
			
			JsonNode fixedNode = !CodeUtils.isEmpty(jsonOfFixedHeaders)?mapper.readTree(jsonOfFixedHeaders):mapper.createObjectNode();
			JsonNode defaultNode =!CodeUtils.isEmpty(jsonOfDefaultHeaders)?mapper.readTree(jsonOfDefaultHeaders):mapper.createObjectNode();
			JsonNode customNode =!CodeUtils.isEmpty(jsonOfCustomHeaders)?mapper.readTree(jsonOfCustomHeaders):mapper.createObjectNode();
			combinationObjectNode.putPOJO("Fixed Headers", fixedNode);
			combinationObjectNode.putPOJO("Default Headers", defaultNode);
			
			combinationObjectNode.putPOJO("Custom Headers", customNode);
			objectNode.putPOJO(CodeUtils.RESPONSE, combinationObjectNode);

		} catch (CannotGetJdbcConnectionException ex) {
			throw ex;
		} catch (DataAccessException ex) {
			throw ex;
		} catch (Exception ex) {
			throw ex;
		}
		return objectNode;
	}

	public int changeHeaders(HeaderConfigLog headerConfigLog) throws Exception {
		int result = 0;
		JsonNode customHeader = headerConfigLog.getCustomHeader();

		try {
			Date currentDate = CodeUtils.convertDateOnTimeZonePluseForAWS(new Date(), 5, 30);
			String ipAddress = InetAddress.getLocalHost().getHostAddress().trim().toString();
			headerConfigLog.setCustomHeaders(customHeader.toString());
			headerConfigLog.setUpdationTime(currentDate);
			headerConfigLog.setIpAddress(ipAddress);
			headerConfigLog.setStatus("ACTIVE");
			headerConfigLog.setActive("1");

			configMapper.updateHeaderLogsStatus(headerConfigLog.getId());
			result = configMapper.createHeaderConfigLogs(headerConfigLog);
		} catch (CannotGetJdbcConnectionException ex) {
			throw ex;
		} catch (DataAccessException ex) {
			throw ex;
		} catch (Exception ex) {
			throw ex;
		}
		return result;

	}
	
	public int createdNewMethod(HeaderConfig headerConfiguration) throws Exception {
		int result = 0;
		String fixedHeader = null;
		String defaultHeader = null;
		String customHeader=null;
		String id = null;
		JsonNode fixedHeaders = headerConfiguration.getFixedHeaders();
		JsonNode defaultHeaders = headerConfiguration.getDefaultHeaders();
		JsonNode customHeaders=headerConfiguration.getCustomHeaders();

		try {
			Date createdOn = CodeUtils.convertDateOnTimeZonePluseForAWS(new Date(), 5, 30);
			id = UUID.randomUUID().toString();
			String ipAddress = InetAddress.getLocalHost().getHostAddress().trim().toString();
			headerConfiguration.setId(id);
			fixedHeader = fixedHeaders.toString();
			defaultHeader = defaultHeaders.toString();
			customHeader=customHeaders.toString();
			headerConfiguration.setFixedHeader(fixedHeader);
			headerConfiguration.setDefaultHeader(defaultHeader);
			headerConfiguration.setCustomHeader(customHeader);
			headerConfiguration.setCreatedTime(createdOn);
			headerConfiguration.setStatus("ACTIVE");
			headerConfiguration.setActive("1");
			headerConfiguration.setIpAddress(ipAddress);
			LOGGER.info("Header configuratioin save query is started...");
			configMapper.updateStatusMethodTemporary(id, headerConfiguration.getEnterpriseName(),headerConfiguration.getDisplayName());
			result = configMapper.createMethodTemporary(headerConfiguration);
		} catch (CannotGetJdbcConnectionException ex) {
			throw ex;
		} catch (DataAccessException ex) {
			throw ex;
		} catch (Exception ex) {
			throw ex;
		}
		return result;

	}

}
