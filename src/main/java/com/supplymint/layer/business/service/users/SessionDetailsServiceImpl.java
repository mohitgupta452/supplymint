package com.supplymint.layer.business.service.users;

import java.net.InetAddress;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.CannotGetJdbcConnectionException;
import org.springframework.stereotype.Service;

import com.supplymint.config.aws.utils.AWSUtils.SessionDetail;
import com.supplymint.layer.business.contract.users.SessionDetailsService;
import com.supplymint.layer.data.tenant.users.entity.SessionDetails;
import com.supplymint.layer.data.tenant.users.mapper.SessionDetailsMapper;
import com.supplymint.layer.web.endpoint.abstracts.AbstractEndpoint;
import com.supplymint.util.CodeUtils;

@Service
public class SessionDetailsServiceImpl extends AbstractEndpoint implements SessionDetailsService {

	private SessionDetailsMapper sessionDetailsMapper;

	private static final Logger LOGGER = LoggerFactory.getLogger(SessionDetailsServiceImpl.class);

	@Autowired
	public SessionDetailsServiceImpl(SessionDetailsMapper sessionDetailsMapper) {
		this.sessionDetailsMapper = sessionDetailsMapper;
	}

	@Override
	public int create(SessionDetails sessionDetails) throws Exception {
		int result = 0;
		String duration = null;
		SessionDetails data = null;
		String isLoggedIn = null;

		try {
			OffsetDateTime createdTime = OffsetDateTime.now();
			createdTime = createdTime.plusHours(5);
			createdTime = createdTime.plusMinutes(30);
			String ipAddress = InetAddress.getLocalHost().getHostAddress().trim().toString();
			sessionDetails.setIpAddress(ipAddress);
			if (sessionDetails.getType().equalsIgnoreCase(SessionDetail.LOGGEDIN.toString())) {
				data = sessionDetailsMapper.getByUserName(sessionDetails.getUserName());
				LOGGER.debug(String.format("Get last record : %s", data));
				if (!CodeUtils.isEmpty(data)) {
					isLoggedIn = data.getIsLoggedIn();
				} else {
					isLoggedIn = SessionDetail.FALSE.toString();
				}

				if (isLoggedIn.equalsIgnoreCase(SessionDetail.FALSE.toString())) {
					sessionDetailsMapper.updateStatus(sessionDetails.getUserName());
					LOGGER.info("Successfully update last record ...");
					sessionDetails.setIsLoggedIn("TRUE");
					sessionDetails.setStatus("ACTIVE");
					sessionDetails.setActive("1");
					sessionDetails.setCreatedTime(createdTime);
					sessionDetails.setLoggedIn(createdTime);
					sessionDetails.setLoggedOut(createdTime);
					duration = getLastData(data);
					if(!CodeUtils.isEmpty(data)) {
					sessionDetails.setLastTimeOut(data.getLoggedOut());
					}
					else {
						sessionDetails.setLastTimeOut(createdTime);
					}
					result = sessionDetailsMapper.create(sessionDetails);
					LOGGER.debug(
							String.format("Successfully Logged In User Name is : %s ", sessionDetails.getUserName()));
				} else {
					sessionDetails.setIsLoggedIn("TRUE");
					sessionDetails.setActive("1");
					sessionDetails.setLoggedIn(createdTime);
					duration = getLastData(data);
					sessionDetails.setLoggedOut(data.getLoggedOut());
					sessionDetails.setLastTimeOut(data.getLoggedOut());

					result = sessionDetailsMapper.updateOtherUser(sessionDetails);
					LOGGER.debug(String.format("Successfully Logged In with out Logged Out User Name is : %s ",
							sessionDetails.getUserName()));
				}

			} else if (sessionDetails.getType().equalsIgnoreCase(SessionDetail.LOGGEDOUT.toString())) {
				sessionDetails.setIsLoggedIn("FALSE");
				sessionDetails.setActive("0");
				sessionDetails.setUpdationTime(createdTime);
				sessionDetails.setLoggedOut(createdTime);
				result = sessionDetailsMapper.update(sessionDetails);
				LOGGER.debug(String.format("Successfully Logged Out User Name is : %s ", sessionDetails.getUserName()));

			}
		} catch (CannotGetJdbcConnectionException ex) {
			throw ex;
		} catch (DataAccessException ex) {
			throw ex;
		} catch (Exception ex) {
			throw ex;
		}
		return result;
	}

	public SessionDetails getByUserName(String userName) {
		return sessionDetailsMapper.getByUserName(userName);
	}

	private String getLastData(SessionDetails data) {
		// SessionDetails data = null;
		long diffSeconds = 0;
		long diffMinutes = 0;
		long diffHours = 0;
		long diffDays = 0;
		long diffMonths = 0;
		String duration = null;

		try {
			if (!CodeUtils.isEmpty(data)) {
				// Date currentTime = new Date();
				// currentTime = CodeUtils.convertDateOnTimeZonePluseForAWS(currentTime, 5, 30);
				OffsetDateTime createdTime = OffsetDateTime.now();
				createdTime = createdTime.plusHours(5);
				createdTime = createdTime.plusMinutes(30);
				Date currentTime = Date.from(createdTime.toInstant());
				Date lastLogInTime = Date.from(data.getLoggedOut().toInstant());
				long diffTime = currentTime.getTime() - lastLogInTime.getTime();
				diffSeconds = diffTime / 1000 % 60;
				diffMinutes = diffTime / (60 * 1000) % 60;
				diffHours = diffTime / (60 * 60 * 1000) % 24;
				diffDays = diffTime / (24 * 60 * 60 * 1000);
				diffMonths = diffTime / ((24 * 60 * 60 * 1000) * 30);
				if (diffMonths >= 1) {
					duration = diffDays + " Months ago";
				} else if (diffDays >= 1) {
					duration = diffDays + " Days ago";
				} else if (diffHours >= 1) {
					duration = diffHours + " Hours ago";
				} else if (diffMinutes >= 1) {
					duration = diffMinutes + " Minutes ago";
				} else if (diffSeconds >= 1) {
					duration = diffSeconds + " Seconds ago";
				}
			} else {
				duration = null;
			}

		} catch (CannotGetJdbcConnectionException ex) {
			throw ex;

		} catch (DataAccessException ex) {
			throw ex;
		} catch (Exception ex) {
			throw ex;
		}
		LOGGER.debug(String.format("Time Duration of User Logged In is : %s", duration));
		return duration;
	}

}
