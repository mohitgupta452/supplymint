package com.supplymint.layer.business.service.s3bucketService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.supplymint.layer.business.contract.s3Bucket.S3BucketInfoService;
import com.supplymint.layer.data.s3Bucket.mappers.S3BucketInfoMapper;
import com.supplymint.layer.data.tenant.inventory.entity.S3BucketInfo;

@Service
public class S3BucketInfoServiceImpl implements S3BucketInfoService {

	private S3BucketInfoMapper s3BucketInfoMapper;

	@Autowired
	public S3BucketInfoServiceImpl(S3BucketInfoMapper s3BucketInfoMapper) {
		this.s3BucketInfoMapper = s3BucketInfoMapper;

	}

	@Override
	public List<S3BucketInfo> getBucketByName(String bucketName) {
		return s3BucketInfoMapper.getBucketByName(bucketName);
	}

	@Override
	public List<S3BucketInfo> getBucketStatus(String status) {
		return s3BucketInfoMapper.getBucketStatus(status);
	}

	@Override
	public List<S3BucketInfo> getAllBucket() {
		return s3BucketInfoMapper.getAllBucket();
	}

	@Override
	public Integer s3InsertFileName(S3BucketInfo s3BucketInfo) {
		return s3BucketInfoMapper.s3InsertFileName(s3BucketInfo);
	}

	@Override
	public List<S3BucketInfo> getAllFileName() {
		return s3BucketInfoMapper.getAllFileName();
	}

	@Override
	public Integer s3UpdateFileName(S3BucketInfo s3BucketInfo) {
		return s3BucketInfoMapper.s3UpdateFileName(s3BucketInfo);
	}

	@Override
	public S3BucketInfo getBucketByType(String bucketName, String type, String orgId) {
		return s3BucketInfoMapper.getBucketByType(bucketName, type, orgId);
	}

	@Override
	public List<S3BucketInfo> getBucketByType(String type) {
		return s3BucketInfoMapper.getBucketTypes(type);
	}

	@Override
	public S3BucketInfo getBucketByType(String type, String orgId) {
		return s3BucketInfoMapper.getBucketType(type, orgId);
	}

	@Override
	public List<S3BucketInfo> isExistS3FileName(String outputFileName) {
		return s3BucketInfoMapper.isExistS3FileName(outputFileName);
	}

	@Override
	public Integer insertBucketName(S3BucketInfo s3BucketInfo) {
		return s3BucketInfoMapper.insertBucketName(s3BucketInfo);
	}

	@Override
	public List<S3BucketInfo> isExistBucketName(String bucketName) {
		return s3BucketInfoMapper.isExistBucketName(bucketName);
	}

	@Override
	public Integer updateBucketName(S3BucketInfo s3BucketInfo) {
		return s3BucketInfoMapper.updateBucketName(s3BucketInfo);
	}

	@Override
	public List<S3BucketInfo> getByURL(String url) {
		return s3BucketInfoMapper.getByURL(url);
	}

	@Override
	public int updateBucketByType(S3BucketInfo s3BucketInfo) {
		return s3BucketInfoMapper.updateBucketByType(s3BucketInfo);
	}

	@Override
	public int updateBucketByProfile(String path) {
		return s3BucketInfoMapper.updateBucketByProfile(path);
	}

	@Override
	public int updateManualTransferOrder(String manualTransferUrl, String bucketType) {
		return s3BucketInfoMapper.updateManualTransferOrder(manualTransferUrl, bucketType);
	}

}
