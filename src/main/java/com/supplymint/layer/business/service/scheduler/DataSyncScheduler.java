package com.supplymint.layer.business.service.scheduler;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.supplymint.layer.business.contract.notification.EmailService;
import com.supplymint.layer.business.contract.notification.MailManagerService;
import com.supplymint.layer.business.service.core.InventoryAutoConfigService;
import com.supplymint.layer.data.config.ThreadLocalStorage;
import com.supplymint.layer.data.core.entity.InventoryAutoConfig;
import com.supplymint.layer.data.notification.entity.MailManager;
import com.supplymint.layer.data.tenant.administration.entity.DataReference;
import com.supplymint.layer.data.tenant.administration.mapper.CustomMapper;
import com.supplymint.util.ApplicationUtils.AppEnvironment;
import com.supplymint.util.ApplicationUtils.CustomRunState;
import com.supplymint.util.ApplicationUtils.EmailTemplateName;
import com.supplymint.util.CodeUtils;
import com.supplymint.util.JsonUtils;

@Service
public class DataSyncScheduler {
	
	@Autowired
	CustomMapper customMapper;
	
	@Autowired
	private EmailService emailService;

	@Autowired
	private MailManagerService mailManagerService;
	
	@Autowired
	Environment environment;

	@Autowired
	private InventoryAutoConfigService inventoryAutoConfigService;

	private final static Logger LOGGER = LoggerFactory.getLogger(DashBoardScheduler.class);
	
	
	@Scheduled(cron = "0 30 11 * * ?")
	public void dataSyncEmailScheduler() {

		try {
			ThreadLocalStorage.setTenantHashKey(null);

			List<InventoryAutoConfig> autoConfigList = inventoryAutoConfigService.getAll();
			LOGGER.info(String.format("Obtaining all Configuration from core DB :%s", autoConfigList));
			LOGGER.debug(String.format("Data Sync start :"));

			if (environment.getActiveProfiles()[0].equalsIgnoreCase(AppEnvironment.Profiles.DEV.name())
					|| environment.getActiveProfiles()[0].equalsIgnoreCase(AppEnvironment.Profiles.PROD.name())) {

				for (int i = 0; i < autoConfigList.size(); i++) {
					try {
						ThreadLocalStorage.setTenantHashKey(autoConfigList.get(i).getTenantHashKey());

						List<DataReference> dfs = customMapper.getAllDataReferenceScheduler();

						if (!CodeUtils.isEmpty(dfs)) {

							MailManager mailManager = new MailManager();
							String[] to = null;
							String[] cc = null;

							int result = mailManagerService.getEmailStatus(autoConfigList.get(i).getOrgId());
							if (result != 0) {
								mailManager = mailManagerService.getByStatus(
										EmailTemplateName.DATA_SYNC_FAILED.toString(), CustomRunState.FAILED.toString(),
										autoConfigList.get(i).getOrgId());
								SimpleMailMessage mailMessage = new SimpleMailMessage();
								if (!CodeUtils.isEmpty(mailManager)) {
									if (mailManager.getCc() != null) {
										cc = mailManager.getCc().trim().split(",");
									}
									if (mailManager.getTo() != null) {
										to = mailManager.getTo().trim().split(",");
									}
									mailMessage.setCc(cc);
									mailMessage.setTo(to);
									mailMessage.setBcc(mailManager.getBcc());
									mailMessage.setSubject(mailManager.getSubject());

									String mailText = null;
									try {
										mailText = autoConfigList.get(i).getEnterpriseName()
												+ JsonUtils.jsonToPrettyJson(JsonUtils.toJsonFromList(dfs));
									} catch (Exception ex) {
										ex.printStackTrace();
									}
									emailService.sendSimpleMessage(to, cc, mailMessage.getSubject(), mailText);
								}
							}
							LOGGER.info(ThreadLocalStorage.getTenantHashKey());
						}
					} catch (Exception ex) {
						LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
					}
				}
			}
			LOGGER.debug(String.format("Data Sync end :"));
		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
		}
	}

}
