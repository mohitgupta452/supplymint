package com.supplymint.layer.business.service.administration;

import static java.nio.file.StandardOpenOption.CREATE_NEW;

import java.io.OutputStream;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.supplymint.layer.business.contract.administration.OrganisationService;
import com.supplymint.layer.business.contract.s3Bucket.S3BucketInfoService;
import com.supplymint.layer.data.tenant.administration.entity.ADMOrganisation;
import com.supplymint.layer.data.tenant.administration.mapper.OrganisationMapper;
import com.supplymint.layer.data.tenant.inventory.entity.S3BucketInfo;
import com.supplymint.util.CodeUtils;
import com.supplymint.util.UploadUtils;

@Service
public final class OrganisationServiceImpl implements OrganisationService {

	private OrganisationMapper organisationMapper;

	public static final String DOWNLOAD_IMAGE = "DOWNLOAD_IMG";
	@Autowired
	private S3BucketInfoService s3BucketInfoService;

	@Autowired
	private OrganisationServiceImpl(OrganisationMapper organisationMapper) {
		this.organisationMapper = organisationMapper;
	}

	public Integer insert(ADMOrganisation admOrganisation) {
		return organisationMapper.create(admOrganisation);
	}

	public ADMOrganisation getById(Integer orgId) {
		return organisationMapper.findById(orgId);
	}

	@Override
	public Integer update(ADMOrganisation admOrganisation) {
		return organisationMapper.update(admOrganisation);
	}

	public List<ADMOrganisation> getAll(Integer offset, Integer pageSize) {
		return organisationMapper.findAll(offset, pageSize);
	}

	public Integer deleteById(Integer orgId) {
		return organisationMapper.removeById(orgId);
	}

	@Override
	public ADMOrganisation convert(String value) {
		return new ADMOrganisation();
	}

	@Override
	public Integer record() {
		return organisationMapper.record();
	}

	@Override
	public List<ADMOrganisation> searchAll(Integer offset, Integer pagesize, String search) {
		return organisationMapper.searchAll(offset, pagesize, search);
	}

	@Override
	public Integer searchRecord(String search) {
		return organisationMapper.searchRecord(search);
	}

	public List<ADMOrganisation> filter(ADMOrganisation admOrganisation) {
		return organisationMapper.filter(admOrganisation);
	}

	public int validationCheck(ADMOrganisation admOrganisation) {
		return organisationMapper.validationCheck(admOrganisation);
	}

	@Override
	public List<ADMOrganisation> checkOrgExistance(ADMOrganisation admOrganisation) {
		return organisationMapper.checkOrgExistance(admOrganisation);
	}

	public List<ADMOrganisation> checkOrgUpdateExistance(ADMOrganisation admOrganisation) {
		return organisationMapper.checkOrgUpdateExistance(admOrganisation);
	}

	public Integer filterRecord(ADMOrganisation admOrganisation) {
		return organisationMapper.filterRecord(admOrganisation);
	}

	@Override
	public List<ADMOrganisation> filterOrganisation(int offset, int pageSize, ADMOrganisation admOrganisation) {
		List<ADMOrganisation> data = null;
		admOrganisation.setOrgName(admOrganisation.getOrgName().trim());
		admOrganisation.setDisplayName(admOrganisation.getDisplayName().trim());
		admOrganisation.setStatus(admOrganisation.getStatus().trim());
		admOrganisation.setOffset(offset);
		admOrganisation.setPagesize(pageSize);
		data = filter(admOrganisation);

		return data;
	}

	@Override
	public List<ADMOrganisation> getAllRecords() {
		return organisationMapper.getAllRecord();
	}

	@Override
	public List<ADMOrganisation> getAllData() {
		return organisationMapper.getAllData();
	}

	@SuppressWarnings({ "resource" })
	@Override
	public void generateOrganisationListTOExcel(List<ADMOrganisation> list, String filePath) {
		Workbook workbook = new XSSFWorkbook();
		Sheet sheet = workbook.createSheet("ORGANISATIONS");

		int rowIndex = 1;
		Row row1 = sheet.createRow(0);
		row1.createCell(0).setCellValue("ORGANISATION NAME");
		row1.createCell(1).setCellValue("DISPLAY NAME");
		row1.createCell(2).setCellValue("CONTACT PERSON");
		row1.createCell(3).setCellValue("CONTACT NUMBER");
		row1.createCell(4).setCellValue("EMAIL");
		row1.createCell(5).setCellValue("BILL TO ADDRESS");
		row1.createCell(6).setCellValue("STATUS");

		for (ADMOrganisation organisation : list) {
			Row row = sheet.createRow(rowIndex++);
			int cellIndex = 0;
			row.createCell(cellIndex++).setCellValue(organisation.getOrgName());
			row.createCell(cellIndex++).setCellValue(organisation.getDisplayName());
			row.createCell(cellIndex++).setCellValue(organisation.getContPerson());
			row.createCell(cellIndex++).setCellValue(organisation.getContNumber());
			row.createCell(cellIndex++).setCellValue(organisation.getContEmail());
			row.createCell(cellIndex++).setCellValue(organisation.getBillToAddress());
			row.createCell(cellIndex++).setCellValue(organisation.getStatus());
		}
		try {
			Path path = Paths.get(filePath);
			try (OutputStream out = Files.newOutputStream(path, CREATE_NEW)) {
				workbook.write(out);
				out.close();
			} catch (FileAlreadyExistsException incrCounterAndRetry) {
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
//		try {
//			fos = new FileOutputStream(filePath);
//			workbook.write(fos);
//			fos.close();
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
	}

	@Override
	public Map<String, List<String>> getOrganisationHeaders() {

		Map<String, List<String>> map = new HashMap<String, List<String>>();
		List<String> listOfKey = new ArrayList<String>();
		List<String> listOfValue = new ArrayList<String>();

		listOfKey.add("orgName");
		listOfKey.add("displayName");
		listOfKey.add("contPerson");
		listOfKey.add("ContNumber");
		listOfKey.add("contEmail");
		listOfKey.add("billToAddress");
		listOfKey.add("status");

		listOfValue.add("ORGANISATION NAME");
		listOfValue.add("DISPLAY NAME");
		listOfValue.add("CONTACT PERSON");
		listOfValue.add("CONTACT NUMBER");
		listOfValue.add("EMAIL");
		listOfValue.add("BILL TO ADDRESS");
		listOfValue.add("STATUS");
		
		map.put("key", listOfKey);
		map.put("value", listOfValue);
		return map;

	}

	public List<ADMOrganisation> checkOrgExistanceValidation(String contEmail, String contNumber) {
		return organisationMapper.checkOrgExistanceValidation(contEmail, contNumber);
	}

	@SuppressWarnings("finally")
	@Override
	public Map<String, String> uploadImage(ADMOrganisation org) {
		String file = org.getFile();
		Map<String, String> uploadImage = null;
		try {
//			S3BucketInfo s3BucketInfo = s3BucketInfoService.getBucketByType(DOWNLOAD_IMAGE);
//			if (!file.isEmpty() && !CodeUtils.isEmpty(s3BucketInfo)) {
//				uploadImage = UploadUtils.uploadBase64Image(file, org.getFileName(), DOWNLOAD_IMAGE);
				/*
				 * if (!CodeUtils.isEmpty(uploadProfileImage)) { bucketPath =
				 * uploadProfileImage.get("bucketKey");
				 * 
				 * }
				 */
//			}
		} catch (ArrayIndexOutOfBoundsException e) {
			throw e;
		} catch (Exception e) {
			throw e;
		} finally {
			return uploadImage;
		}
	}
}
