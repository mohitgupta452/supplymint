
package com.supplymint.layer.business.service.rconnection;

/**
 * @author Manoj Singh
 *
 */

import org.rosuda.REngine.REngine;
import org.rosuda.REngine.REngineCallbacks;
import org.rosuda.REngine.REngineOutputInterface;
import org.slf4j.Logger;

public class REngineOutCallBack implements REngineCallbacks, REngineOutputInterface {

	private Logger logger;

	public REngineOutCallBack(Logger logger) {
		this.logger = logger;
	}

	@Override
	public void RWriteConsole(REngine eng, String text, int oType) {
		if (oType == 0) {
			logger.info(text);
		} else {
			logger.warn(text);
		}
	}

	@Override
	public void RShowMessage(REngine eng, String text) {
		logger.error(text);
	}

	@Override
	public void RFlushConsole(REngine eng) {

	}
}