package com.supplymint.layer.business.service.demand;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Month;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.servlet.http.HttpServletRequest;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.CannotGetJdbcConnectionException;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.supplymint.exception.StatusCodes.OtbMsg;
import com.supplymint.exception.StatusCodes.Pagination;
import com.supplymint.exception.SupplyMintException;
import com.supplymint.layer.business.contract.demand.OTBService;
import com.supplymint.layer.business.contract.s3Bucket.S3BucketInfoService;
import com.supplymint.layer.business.service.s3bucketService.S3WrapperService;
import com.supplymint.layer.data.config.ThreadLocalStorage;
import com.supplymint.layer.data.downloads.entity.DownloadOTBPlanASD;
import com.supplymint.layer.data.downloads.entity.DownloadOTBPlanDFD;
import com.supplymint.layer.data.tenant.demand.entity.DemandForecast;
import com.supplymint.layer.data.tenant.demand.entity.MViewRefreshHistory;
import com.supplymint.layer.data.tenant.demand.entity.OTBGraph;
import com.supplymint.layer.data.tenant.demand.entity.OTBPlanASD;
import com.supplymint.layer.data.tenant.demand.entity.OTBPlanDFD;
import com.supplymint.layer.data.tenant.demand.entity.OpenToBuy;
import com.supplymint.layer.data.tenant.demand.mapper.OTBMapper;
import com.supplymint.layer.data.tenant.inventory.entity.S3BucketInfo;
import com.supplymint.util.ApplicationUtils.BucketParameter;
import com.supplymint.util.ApplicationUtils.Conditions;
import com.supplymint.util.ApplicationUtils.CustomParameter;
import com.supplymint.util.ApplicationUtils.CustomRunState;
import com.supplymint.util.ApplicationUtils.DemandPlanningParameter;
import com.supplymint.util.ApplicationUtils.Module;
import com.supplymint.util.ApplicationUtils.Quarter;
import com.supplymint.util.ApplicationUtils.SubModule;
import com.supplymint.util.ApplicationUtils.TenantHash;
import com.supplymint.util.ApplicationUtils.DateFormatOTB;
import com.supplymint.util.CodeUtils;
import com.supplymint.util.DateUtils;
import com.supplymint.util.FileUtils;
import com.supplymint.util.FiscalDate;

/**
 * This class describes all service for OTB
 * @author Prabhakar Srivastava 
 * @author Bishnu Dutta
 * @author Varun Sharma
 * @Date 20 Mar 2019
 * @version 1.0
 */
@Service
public class OTBServiceImpl implements OTBService {

	private static final Logger LOGGER = LoggerFactory.getLogger(OTBServiceImpl.class);

	/**
	 * This variable describes for OTBMapper instance 
	 */
	private OTBMapper otbMapper;

	private String tenantHashkey;

	private boolean isInserted = false;

	private String assortment = null;

	private Map<String, List<Map<String, String>>> mapOfAssortmentWiseMetricDataCopy = new HashMap<>();
	
	private Set<String> listQuarterValue=new HashSet<>();

	private boolean exit = false;

	/**
	 * This constructor describes instantiating AWS Service Object
	 */
	@Autowired
	private S3WrapperService s3Wrapper;

	/**
	 * This constructor describes instantiating S3Bucket Service Object
	 */
	@Autowired
	private S3BucketInfoService s3BucketInfoService;

	/**
	 * This constructor describes instantiating OTBMapper Object
	 */
	@Autowired
	public OTBServiceImpl(OTBMapper otbMapper) {
		this.otbMapper = otbMapper;

	}

	@Override
	public OpenToBuy getByPlanName(final String planName) {
		return otbMapper.getByPlanName(planName);
	}

	@Override
	public int create(OpenToBuy openToBuy) throws SupplyMintException, Exception {

		String planId = null;
		String actualPattern = null;
		String ipAddress = null;
		int existencePlanName = 0;
		int otbCreatePlan = 0;
		try {
			actualPattern = otbMapper.getPlanIdPattern();
			if (actualPattern == null) {
				planId = DemandPlanningParameter.OTB_PATTERN;
			} else {
				planId = CodeUtils.createPattern(actualPattern);
			}
			openToBuy.setPlanId(planId);
			Date createdOn = CodeUtils.convertDateOnTimeZonePluseForAWS(new Date(), 5, 30);
			openToBuy.setCreatedOn(createdOn);
			ipAddress = InetAddress.getLocalHost().getHostAddress().trim().toString();
			openToBuy.setIpAddress(ipAddress);
			if (openToBuy.getIsUfAsd().equalsIgnoreCase(Conditions.TRUE.toString())) {
				openToBuy.setIsUfAsd(Conditions.YES.toString());
				openToBuy.setIsUfDfd(Conditions.NO.toString());
				openToBuy.setActiveDataOption(DemandPlanningParameter.ACTIVEDATA_ASD);

			} else if (openToBuy.getIsUfDfd().equalsIgnoreCase(Conditions.TRUE.toString())) {
				openToBuy.setIsUfDfd(Conditions.YES.toString());
				openToBuy.setIsUfAsd(Conditions.NO.toString());
				openToBuy.setActiveDataOption(DemandPlanningParameter.ACTIVEDATA_DFD);
			}
			if (CodeUtils.isEmpty(openToBuy.getIncreasePercentageAsd())) {
				openToBuy.setIncreasePercentageAsd(Conditions.NA.toString());
			} else if (CodeUtils.isEmpty(openToBuy.getIncreasePercentageDfd())) {
				openToBuy.setIncreasePercentageDfd(Conditions.NA.toString());
			}
			LOGGER.info("checking validation for OTB PlanName With Active");
			existencePlanName = otbMapper.isExistencePlanName(openToBuy.getPlanName());
			if (existencePlanName == 0) {
				otbMapper.updateAllStatus(openToBuy.getPlanId(), openToBuy.getPlanName());
				openToBuy.setStatus("TRUE");
				openToBuy.setActive('1');
				LOGGER.info("create");
				otbCreatePlan = otbMapper.create(openToBuy);
				if (otbCreatePlan != 0) {
					tenantHashkey = ThreadLocalStorage.getTenantHashKey();
					ExecutorService executorService = Executors.newSingleThreadExecutor();
					executorService.execute(new Runnable() {
						@SuppressWarnings("unused")
						@Override
						public void run() {
							try {
								ThreadLocalStorage.setTenantHashKey(tenantHashkey);
								if (openToBuy.getIsUfAsd().equalsIgnoreCase(Conditions.YES.toString())) {
									LOGGER.info("create actual Sale Data for OTB Plan");
									int result = createActualSaleData(openToBuy);
								} else if (openToBuy.getIsUfDfd().equalsIgnoreCase(Conditions.YES.toString())) {
									LOGGER.info("create Demand forecast Data for OTB Plan");
									int result = createDemandForecastData(openToBuy);
								}
							} catch (Exception ex) {
								int updateOTBPlanStatus = otbMapper.updateOTBPlanStatus(openToBuy.getPlanId(),"FAILED");
								throw ex;
							}
						}
					});
				}
			} else {
				// getbyid
				// plan name & modified percentage
				if (openToBuy.getMakeCopy().equalsIgnoreCase(Conditions.TRUE.toString())) {
//						openToBuy.setPlanName(openToBuy.getPlanName()+"-COPY");
					existencePlanName = otbMapper.isExistencePlanName(openToBuy.getPlanName());
					if (existencePlanName == 0) {
						otbCreatePlan = otbMapper.create(openToBuy);
						if (otbCreatePlan != 0) {
							tenantHashkey = ThreadLocalStorage.getTenantHashKey();
							ExecutorService executorService = Executors.newSingleThreadExecutor();
							executorService.execute(new Runnable() {
								@SuppressWarnings("unused")
								@Override
								public void run() {
									try {
										ThreadLocalStorage.setTenantHashKey(tenantHashkey);
										if (openToBuy.getIsUfAsd().equalsIgnoreCase(Conditions.YES.toString())) {
											int result = createActualSaleData(openToBuy);
										} else if (openToBuy.getIsUfDfd().equalsIgnoreCase(Conditions.YES.toString())) {
											int result = createDemandForecastData(openToBuy);
										}
									} catch (Exception ex) {
										int updateOTBPlanStatus = otbMapper.updateOTBPlanStatus(openToBuy.getPlanId(),"FAILED");
										throw ex;
									}
								}
							});
						}
					}
				}
				throw new SupplyMintException();
			}
		} catch (CannotGetJdbcConnectionException ex) {
			throw ex;
		} catch (DataAccessException ex) {
			throw ex;
		} catch (Exception ex) {
			throw ex;
		}
		return otbCreatePlan;
	}

	@Override
	public int updateASDAndDFD(final OpenToBuy openToBuy) {
		int updateCreateOTB = 0;
		if (openToBuy.getIsUfAsd().equalsIgnoreCase(Conditions.TRUE.toString())) {
			if (openToBuy.getIsUserOTBPlan().equalsIgnoreCase(Conditions.FALSE.toString())) {
				otbMapper.deleteAllAcivePlaneASD(openToBuy.getPlanId());
			} else {
				// user otb change
				otbMapper.deleteAllAcivePlaneUserASD(openToBuy.getPlanId());
			}
			openToBuy.setIncreasePercentageDfd("NA");
			openToBuy.setActiveDataOption("UF_ASD");
			openToBuy.setIsUfAsd(Conditions.YES.toString());
			openToBuy.setIsUfDfd(Conditions.NO.toString());
			updateCreateOTB = otbMapper.updatedOTBCreatePlan(openToBuy);
			tenantHashkey = ThreadLocalStorage.getTenantHashKey();
			ExecutorService executorService = Executors.newSingleThreadExecutor();
			executorService.execute(new Runnable() {
				@SuppressWarnings("unused")
				@Override
				public void run() {
					try {
						ThreadLocalStorage.setTenantHashKey(tenantHashkey);
						int result = createActualSaleData(openToBuy);

					} catch (Exception ex) {
						int updateOTBPlanStatus = otbMapper.updateOTBPlanStatus(openToBuy.getPlanId(),"FAILED");
						throw ex;
					}
				}
			});

		} else if (openToBuy.getIsUfDfd().equalsIgnoreCase(Conditions.TRUE.toString())) {
			if (openToBuy.getIsUserOTBPlan().equalsIgnoreCase(Conditions.FALSE.toString())) {
				otbMapper.deleteAllAcivePlaneDFD(openToBuy.getPlanId());
			} else {
				// user otb change
				otbMapper.deleteAllAcivePlaneUserDFD(openToBuy.getPlanId());
			}
			openToBuy.setIncreasePercentageAsd("NA");
			openToBuy.setActiveDataOption("UF_DFD");
			openToBuy.setIsUfAsd(Conditions.NO.toString());
			openToBuy.setIsUfDfd(Conditions.YES.toString());
			updateCreateOTB = otbMapper.updatedOTBCreatePlan(openToBuy);
			tenantHashkey = ThreadLocalStorage.getTenantHashKey();
			ExecutorService executorService = Executors.newSingleThreadExecutor();
			executorService.execute(new Runnable() {
				@SuppressWarnings("unused")
				@Override
				public void run() {
					try {
						ThreadLocalStorage.setTenantHashKey(tenantHashkey);
						int result = createDemandForecastData(openToBuy);
					} catch (Exception ex) {
						int updateOTBPlanStatus = otbMapper.updateOTBPlanStatus(openToBuy.getPlanId(),"FAILED");
						throw ex;
					}
				}
			});
		}

		return updateCreateOTB;
	}

	@Override
	public int delete(int id) {
		return otbMapper.delete(id);
	}

	@SuppressWarnings({ "deprecation", "unused", "rawtypes" })
	public int createActualSaleData(final OpenToBuy openToBuy) {
		List<OTBPlanASD> data = new ArrayList<OTBPlanASD>();
		Calendar startCalender = Calendar.getInstance();
		Calendar endCalender = Calendar.getInstance();
		endCalender.add(Calendar.MONTH, -1);
		
		String endDate = CodeUtils.__dateFormat.format(endCalender.getTime());
		startCalender.add(Calendar.YEAR, -1); // to get previous year add -1
		startCalender.add(Calendar.MONTH, 1);
		String startDate = CodeUtils.__dateFormat.format(startCalender.getTime());
		List<DemandForecast> listDemandForecast = null;
		String assortmentCode = "";
		int insertAllData = 0;
		int updateOTBPlanStatus = 0;
		int throwError = 0;
		String billDate = null;
		int updateOTBFailedStatus = 0;
		try {

			if (openToBuy.getIsUserOTBPlan().equalsIgnoreCase(Conditions.FALSE.toString())) {
				listDemandForecast = otbMapper.getAllASD(startDate, endDate);
			} else {

				// this tenant is set for specific style baazar query
				if (!tenantHashkey.equalsIgnoreCase(TenantHash.valueOf("STYLE_BAAZAR").getHashKey())) {
					
					MViewRefreshHistory mvHistory = new MViewRefreshHistory();
					mvHistory.setModule(Module.DEMAND_PLANNING.toString());
					mvHistory.setSubModule(SubModule.OTB.toString());
					mvHistory.setStatus(CustomRunState.INPROGRESS.toString());
					mvHistory.setActive('0');
					mvHistory.setRefreshMView("OTB_ACTUAL_MONTHLY,OTB_FORECAST_MONTHLY");

					int isExistMVForOTB = otbMapper.isExistMVForOTB(Module.DEMAND_PLANNING.toString(),
							SubModule.OTB.toString());
					if (isExistMVForOTB == 0) {
						int createMVforOTB = otbMapper.createMVforOTB(mvHistory);
					} else {
						int updateMVStatus = otbMapper.updateMVStatus(Module.DEMAND_PLANNING.toString(),
								SubModule.OTB.toString(), CustomRunState.INPROGRESS.toString(), '0',
								assortmentCode);

					}

					String otbMVCreateStatus = otbMapper.getOTBMVCreateStatus(Module.DEMAND_PLANNING.toString(),
							SubModule.OTB.toString());

					String assortment = otbMapper.getUserAssortment("OTB_USER_ASSORTMENT");

					List<String> assortmentList = new LinkedList<>();

					JSONObject jsonObject = new JSONObject(assortment);
					Iterator iterator = jsonObject.keys();
					while (iterator.hasNext()) {
						String hlname = (String) iterator.next();
						assortmentList.add(hlname);
					}
					for (String list : assortmentList) {
						if (!CodeUtils.isEmpty(list)) {
							assortmentCode += list + "||'-'||";
						}
					}
					assortmentCode = assortmentCode.substring(0, assortmentCode.length() - 7);

					LOGGER.info("checking condition for otb status succeeded or failed");
//				for (int i = 0;; i++) {
//					try {
//						String otbMVCreateStatus = otbMapper.getOTBMVCreateStatus(Module.DEMAND_PLANNING.toString(),
//								SubModule.OTB.toString());
//						if (otbMVCreateStatus.equals(CustomRunState.SUCCEEDED.toString())) {
//							break;
//						} else if (otbMVCreateStatus.equals(CustomRunState.FAILED.toString())) {
//							updateOTBFailedStatus = otbMapper.updateOTBFailedStatus(openToBuy.getPlanId());
//							break;
//						} else {
//							Thread.sleep(5000);
//							LOGGER.debug("Error occoured from unable to create plan :%d", throwError);
//							throwError += 5;
//							if (throwError == 600) {
//								updateOTBFailedStatus = otbMapper.updateOTBFailedStatus(openToBuy.getPlanId());
//							}
//						}
//					} catch (Exception ex) {
//						LOGGER.debug("Error occoured from otb status Error :%s", ex);
//						updateOTBFailedStatus = otbMapper.updateOTBFailedStatus(openToBuy.getPlanId());
//					}
//				}

					if (otbMVCreateStatus!=assortmentCode || otbMVCreateStatus==null) {

						try {

							LOGGER.info("Query start for getting data...");
							String billDateFormat = otbMapper.getBillDateFormat();
							LOGGER.info("Query stopped for getting data...");

							if (CodeUtils.validateDate(billDateFormat, DateFormatOTB.DATEWITHFORWORDSLASH)) {
								billDate = "to_char(to_date(to_char(to_date(billDate,'yyyy-mm-dd'),'mm-yyyy'),'mm-yyyy'),'yyyy-mm-dd')";
							} else if (CodeUtils.validateDate(billDateFormat, DateFormatOTB.DATEWITHDATEMONTHYEAR)) {
								// for bazaarKolkata date format
								billDate = "to_char(to_date(to_date(to_char(to_date(billdate,'dd-mm-yyyy'),'MM-yyyy'),'MM-yyyy'),'dd-MM-yy'),'yyyy-MM-dd')";
							} else if (CodeUtils.validateDate(billDateFormat, DateFormatOTB.DATEWITHMONTHDATEYEAR)) {
								// for megashop date format
								billDate = "to_char(to_date(to_char(to_date(billdate,'MM/dd/yyyy'),'mm-yyyy'),'mm-yyyy'),'yyyy-mm-dd')";
							} else if (CodeUtils.validateDate(billDateFormat, DateFormatOTB.DATEWITHYEARMONTHDATE)) {
								// for mufti and style_bazaar date format
								billDate = "to_char(to_date(to_char(to_date(billDate,'yyyy-mm-dd'),'mm-yyyy'),'mm-yyyy'),'yyyy-mm-dd')";
							}

							try {
								
								try {
									LOGGER.info("droping materialized view Actual OTB...");
									otbMapper.dropUserOTBMV();

									LOGGER.info("creating materialized view Actual OTB...");
									otbMapper.createUserOTBMV(assortmentCode, billDate);
									LOGGER.info("creation of materialized view done...");
								} catch (Exception ex) {
									LOGGER.info("creating materialized view Actual OTB...");
									otbMapper.createUserOTBMV(assortmentCode, billDate);
									LOGGER.info("creation of materialized view done...");
								}

								try {
									LOGGER.info("droping materialized view Forecast OTB...");
									otbMapper.dropUserOTBMVForecast();

									LOGGER.info("creating materialized view Forecast OTB...");
									otbMapper.createUserOTBMVForecast(assortmentCode);
								} catch (Exception ex) {
									LOGGER.info("creating materialized view Forecast OTB...");
									otbMapper.createUserOTBMVForecast(assortmentCode);
								}

								int updateMVStatus = otbMapper.updateMVStatus(Module.DEMAND_PLANNING.toString(),
										SubModule.OTB.toString(), CustomRunState.SUCCEEDED.toString(), '1',
										assortmentCode);
							} catch (Exception ex) {
								otbMapper.createUserOTBMV(assortmentCode, billDate);
								int updateMVStatus = otbMapper.updateMVStatus(Module.DEMAND_PLANNING.toString(),
										SubModule.OTB.toString(), CustomRunState.SUCCEEDED.toString(), '1',
										assortmentCode);
							}
						} catch (Exception ex) {
							int updateMVStatus = otbMapper.updateMVStatus(Module.DEMAND_PLANNING.toString(),
									SubModule.OTB.toString(), CustomRunState.FAILED.toString(), '0', assortmentCode);
						}
					}
					listDemandForecast = otbMapper.getAllUserASDPlan(startDate, endDate);
				} else {
//					listDemandForecast = otbMapper.getAllCustomUserASDPlan(startDate, endDate);
					return createActualDataForStyleBaazar(openToBuy);
				}
			}
			LOGGER.info("manipulating data for otb actual monthly");
			for (DemandForecast demandForecast : listDemandForecast) {
				OTBPlanASD otbPlanASD = new OTBPlanASD();
				otbPlanASD.setPlanId(openToBuy.getPlanId());
				otbPlanASD.setAssortmentCode(demandForecast.getAssortmentCode());
				otbPlanASD.setBillDate(demandForecast.getBillDate());
				otbPlanASD.setFutureBillDate(CodeUtils.__dateFormat.format(CodeUtils
						.addYear(new Date(CodeUtils.convertDataReferenceDateFormat(otbPlanASD.getBillDate())), 1)));

				otbPlanASD.setQty(demandForecast.getQty());
				otbPlanASD.setCostPrice(demandForecast.getCostPrice());
				otbPlanASD.setSellValue(demandForecast.getSellValue());
				otbPlanASD.setIsModified("N");
				otbPlanASD.setModifiedPercentage(0);
//				float sales = demandForecast.getCostPrice() * Float.parseFloat(otbPlanASD.getQtyForecast());
				double planSale = demandForecast.getSellValue() + demandForecast.getSellValue()
						* (Float.parseFloat(openToBuy.getIncreasePercentageAsd()) / (double) 100);
				otbPlanASD.setPlanSales(Math.round(planSale * 100.0) / 100.0);
				otbPlanASD.setMarkDown(0);
				otbPlanASD.setClosingInventory(0);
				otbPlanASD.setOpeningStock(0);
				otbPlanASD.setOpenPurchaseOrder(demandForecast.getOpenPurchaseOrder());
				double otbValue = otbPlanASD.getPlanSales() + otbPlanASD.getMarkDown()
						+ otbPlanASD.getClosingInventory()
						- (otbPlanASD.getOpeningStock() + otbPlanASD.getOpenPurchaseOrder());
				otbPlanASD.setOtbValue(otbValue);
				otbPlanASD.setStatus(Conditions.TRUE.toString());
				otbPlanASD.setActive('1');
				data.add(otbPlanASD);
			}
			int size = 1000;
			LOGGER.info("inserting data from otb actual monthly with size of 1000 at a time");
			for (int start = 0; start < data.size(); start += size) {
				int end = Math.min(start + size, data.size());
				List<OTBPlanASD> sublist = data.subList(start, end);

				if (openToBuy.getIsUserOTBPlan().equalsIgnoreCase(Conditions.TRUE.toString())) {
					insertAllData = otbMapper.insertUserASD(sublist);
				}
			}
			try {
				otbMapper.refreshQuarterMV();
			} catch (Exception ex) {
				LOGGER.debug("Error due to Refresh Quarter MV",ex);
			}
			LOGGER.info("inserting data from otb actual monthly completed...");
			updateOTBPlanStatus = otbMapper.updateOTBPlanStatus(openToBuy.getPlanId(),"SUCCEEDED");
			LOGGER.info("update otb status inprogress to success...");
		} catch (Exception e) {
			updateOTBPlanStatus = otbMapper.updateOTBPlanStatus(openToBuy.getPlanId(),"FAILED");
			try {
				throw e;
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
		return insertAllData;
	}

	@SuppressWarnings({ "unused", "deprecation", "rawtypes" })
	public int createDemandForecastData(final OpenToBuy openToBuy) {

		int result = 0;
		int updateOTBPlanStatus = 0;
		String assortmentCode = "";
		String billDate = null;
		try {
			List<OTBPlanDFD> data = null;
			Calendar cal = Calendar.getInstance();
			String startDate = CodeUtils.__dateFormat.format(cal.getTime());
			cal.add(Calendar.YEAR, 1); // to get previous year add -1
			cal.add(Calendar.MONTH, -1);
			String endDate = CodeUtils.__dateFormat.format(cal.getTime());

			if (openToBuy.getIsUserOTBPlan().equalsIgnoreCase(Conditions.FALSE.toString())) {
				data = otbMapper.getAllDFD(startDate, endDate);
			} else {

				// this tenant is set for specific style baazar query
				if (!tenantHashkey.equalsIgnoreCase(TenantHash.valueOf("STYLE_BAAZAR").getHashKey())) {
					
					MViewRefreshHistory mvHistory = new MViewRefreshHistory();
					mvHistory.setModule(Module.DEMAND_PLANNING.toString());
					mvHistory.setSubModule(SubModule.OTB.toString());
					mvHistory.setStatus(CustomRunState.INPROGRESS.toString());
					mvHistory.setActive('0');
					mvHistory.setRefreshMView("OTB_ACTUAL_MONTHLY,OTB_FORECAST_MONTHLY");

					int isExistMVForOTB = otbMapper.isExistMVForOTB(Module.DEMAND_PLANNING.toString(),
							SubModule.OTB.toString());
					if (isExistMVForOTB == 0) {
						int createMVforOTB = otbMapper.createMVforOTB(mvHistory);
					} else {
						int updateMVStatus = otbMapper.updateMVStatus(Module.DEMAND_PLANNING.toString(),
								SubModule.OTB.toString(), CustomRunState.INPROGRESS.toString(), '0',
								assortmentCode);

					}

					String otbMVCreateStatus = otbMapper.getOTBMVCreateStatus(Module.DEMAND_PLANNING.toString(),
							SubModule.OTB.toString());

					String assortment = otbMapper.getUserAssortment("OTB_USER_ASSORTMENT");

					List<String> assortmentList = new LinkedList<>();

					JSONObject jsonObject = new JSONObject(assortment);
					Iterator iterator = jsonObject.keys();
					while (iterator.hasNext()) {
						String hlname = (String) iterator.next();
						assortmentList.add(hlname);
					}
					for (String list : assortmentList) {
						if (!CodeUtils.isEmpty(list)) {
							assortmentCode += list + "||'-'||";
						}
					}
					assortmentCode = assortmentCode.substring(0, assortmentCode.length() - 7);

					LOGGER.info("checking condition for otb status succeeded or failed");

					if (otbMVCreateStatus!=assortmentCode || otbMVCreateStatus==null) {
						try {
							LOGGER.info("Query start for getting data...");
							String billDateFormat = otbMapper.getBillDateFormat();
							LOGGER.info("Query stopped for getting data...");

							if (CodeUtils.validateDate(billDateFormat, DateFormatOTB.DATEWITHFORWORDSLASH)) {
								billDate = "to_char(to_date(to_char(to_date(billDate,'yyyy-mm-dd'),'mm-yyyy'),'mm-yyyy'),'yyyy-mm-dd')";
							} else if (CodeUtils.validateDate(billDateFormat, DateFormatOTB.DATEWITHDATEMONTHYEAR)) {
								// for bazaarKolkata date format
								billDate = "to_char(to_date(to_date(to_char(to_date(billdate,'dd-mm-yyyy'),'MM-yyyy'),'MM-yyyy'),'dd-MM-yy'),'yyyy-MM-dd')";
							} else if (CodeUtils.validateDate(billDateFormat, DateFormatOTB.DATEWITHMONTHDATEYEAR)) {
								// for megashop date format
								billDate = "to_char(to_date(to_char(to_date(billdate,'MM/dd/yyyy'),'mm-yyyy'),'mm-yyyy'),'yyyy-mm-dd')";
							} else if (CodeUtils.validateDate(billDateFormat, DateFormatOTB.DATEWITHYEARMONTHDATE)) {
								// for mufti and style_bazaar date format
								billDate = "to_char(to_date(to_char(to_date(billDate,'yyyy-mm-dd'),'mm-yyyy'),'mm-yyyy'),'yyyy-mm-dd')";
							}

							try {
								
								try {
									LOGGER.info("droping materialized view Actual OTB...");
									otbMapper.dropUserOTBMV();

									LOGGER.info("creating materialized view Actual OTB...");
									otbMapper.createUserOTBMV(assortmentCode, billDate);
									LOGGER.info("creation of materialized view done...");
								} catch (Exception ex) {
									LOGGER.info("creating materialized view Actual OTB...");
									otbMapper.createUserOTBMV(assortmentCode, billDate);
									LOGGER.info("creation of materialized view done...");
								}

								try {
									LOGGER.info("droping materialized view Forecast OTB...");
									otbMapper.dropUserOTBMVForecast();

									LOGGER.info("creating materialized view Forecast OTB...");
									otbMapper.createUserOTBMVForecast(assortmentCode);
								} catch (Exception ex) {
									LOGGER.info("creating materialized view Forecast OTB...");
									otbMapper.createUserOTBMVForecast(assortmentCode);
								}
								int updateMVStatus = otbMapper.updateMVStatus(Module.DEMAND_PLANNING.toString(),
										SubModule.OTB.toString(), CustomRunState.SUCCEEDED.toString(), '1',
										assortmentCode);
							} catch (Exception ex) {
								otbMapper.createUserOTBMV(assortmentCode, billDate);
								int updateMVStatus = otbMapper.updateMVStatus(Module.DEMAND_PLANNING.toString(),
										SubModule.OTB.toString(), CustomRunState.SUCCEEDED.toString(), '1',
										assortmentCode);
							}
						} catch (Exception ex) {
							int updateMVStatus = otbMapper.updateMVStatus(Module.DEMAND_PLANNING.toString(),
									SubModule.OTB.toString(), CustomRunState.FAILED.toString(), '0', assortmentCode);
						}
					}

					LOGGER.info("retrive OTB Forecast Monthly data...");
					data = otbMapper.getAllUserDFD(startDate, endDate);
				} else {
//					data = otbMapper.getAllCustomUserDFD(startDate, endDate);
					return createForecastDataForStyleBaazar(openToBuy);
				}
			}
			for (OTBPlanDFD dfd : data) {
				dfd.setPlanId(openToBuy.getPlanId());
				dfd.setIsModified("N");
				dfd.setModifiedPercentage(0);
				double planSale = dfd.getSellValue()
						+ (dfd.getSellValue() * Double.parseDouble(openToBuy.getIncreasePercentageDfd()))
								/ (double) 100;
				dfd.setPlanSales(Math.round(planSale * 100.0) / 100.0);

				dfd.setMarkDown(0);
				dfd.setClosingInventory(0);
				dfd.setOpeningStock(0);
				dfd.setOpenPurchaseOrder(0);
				double otbValue = dfd.getPlanSales() + dfd.getMarkDown() + dfd.getClosingInventory()
						- (dfd.getOpeningStock() + dfd.getOpenPurchaseOrder());
				dfd.setOtbValue(otbValue);
				dfd.setActive('1');
				dfd.setStatus(Conditions.TRUE.toString());
			}
			int size = 1000;
			LOGGER.info("OTB Forecast Monthly:-batch insert start...");
			for (int start = 0; start < data.size(); start += size) {
				int end = Math.min(start + size, data.size());
				List<OTBPlanDFD> sublist = data.subList(start, end);
				if (openToBuy.getIsUserOTBPlan().equalsIgnoreCase(Conditions.FALSE.toString())) {
					result = otbMapper.insertDFDData(sublist);
				} else {
					result = otbMapper.insertUserDFDData(sublist);
				}
			}
			try {
				otbMapper.refreshQuarterMV();
			} catch (Exception ex) {
				LOGGER.debug("Error due to Refresh Quarter MV",ex);
			}
			
			updateOTBPlanStatus = otbMapper.updateOTBPlanStatus(openToBuy.getPlanId(),"SUCCEEDED");
			LOGGER.info("OTB Forecast Monthly:-batch insert completed...");
		} catch (Exception ex) {
			updateOTBPlanStatus = otbMapper.updateOTBPlanStatus(openToBuy.getPlanId(),"FAILED");
			try {
				throw ex;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	@Override
	public List<OpenToBuy> getActiveOTBPlan() {
		return otbMapper.getActiveOTBPlan();
	}

	@Override
	public int updateActivePlan(final OpenToBuy openToBuy) {
		return otbMapper.updateActivePlan(openToBuy);
	}

	@Override
	public int updatePreviousActivePlan(final OpenToBuy openToBuy) {
		return otbMapper.updatePreviousActivePlan(openToBuy);
	}

	@Override
	public int removeActivePlan(final OpenToBuy openToBuy) {
		return otbMapper.removeActivePlan(openToBuy);
	}

	@Override
	public OpenToBuy getByPlanId(final String planId) {
		return otbMapper.getByPlanId(planId);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public ObjectNode getOTBPlan(Integer pageNo, String activePlan, String frequency, String sDate, String eDate,
			String planId) {
		List<OTBPlanDFD> dfdData = null;
		List<OTBPlanASD> asdData = null;
		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode objectNode = mapper.createObjectNode();
		ObjectNode graphNode = mapper.createObjectNode();
		try {
			String startDate=sDate;
			String endDate=eDate;
			Set distinctAssortmentCode = new LinkedHashSet<String>();
//			Set distinctBilldate = new LinkedHashSet<String>();
			if (frequency.equalsIgnoreCase("3_MONTHS")) {
				if (activePlan.equalsIgnoreCase("ASD")) {
					startDate = CodeUtils.__dateFormat.format(CodeUtils.addMonth(new Date(), 1));
					endDate = CodeUtils.__dateFormat.format(CodeUtils.addMonth(new Date(), 3));
				} else if (activePlan.equalsIgnoreCase("DFD")) {
					startDate = CodeUtils.__dateFormat.format(CodeUtils.addMonth(new Date(), 1));
					endDate = CodeUtils.__dateFormat.format(CodeUtils.addMonth(new Date(), 3));
				}
			} else if (frequency.equalsIgnoreCase("6_MONTHS")) {
				if (activePlan.equalsIgnoreCase("ASD")) {
					startDate = CodeUtils.__dateFormat.format(CodeUtils.addMonth(new Date(), 1));
					endDate = CodeUtils.__dateFormat.format(CodeUtils.addMonth(new Date(), 6));
				} else if (activePlan.equalsIgnoreCase("DFD")) {
					startDate = CodeUtils.__dateFormat.format(CodeUtils.addMonth(new Date(), 1));
					endDate = CodeUtils.__dateFormat.format(CodeUtils.addMonth(new Date(), 6));
				}
			} else if (frequency.equalsIgnoreCase("12_MONTHS")) {
				if (activePlan.equalsIgnoreCase("ASD")) {
					startDate = CodeUtils.__dateFormat.format(CodeUtils.addMonth(new Date(), 1));
					endDate = CodeUtils.__dateFormat.format(CodeUtils.addMonth(new Date(), 12));
				} else if (activePlan.equalsIgnoreCase("DFD")) {
					startDate = CodeUtils.__dateFormat.format(CodeUtils.addMonth(new Date(), 1));
					endDate = CodeUtils.__dateFormat.format(CodeUtils.addMonth(new Date(), 12));
				}
			} else if (frequency.equalsIgnoreCase("QUARTERLY")) {
				startDate = CodeUtils.__dateFormat.format(CodeUtils.addMonth(new Date(), 1));
				endDate = CodeUtils.__dateFormat.format(CodeUtils.addMonth(new Date(), 12));
			}else {
				
				String[] splitStartDate=startDate.split(" - ");
				String[] splitEndDate=endDate.split(" - ");
				int startMonth=Month.valueOf(splitStartDate[0].toUpperCase().trim()).getValue();
				int endMonth=Month.valueOf(splitEndDate[0].toUpperCase().trim()).getValue();
				startDate=splitStartDate[1].trim()+ "-"+(startMonth>9?startMonth+"":"0"+startMonth )+"-01";
				endDate=splitEndDate[1].trim()+"-"+ (endMonth>9?endMonth+"":"0"+endMonth )+"-01";
				
				
				
//				startDate = CodeUtils.__dateFormat
//						.format(new Date(CodeUtils.convertDataReferenceDateFormat(startDate)));
//				endDate = CodeUtils.__dateFormat
//						.format(CodeUtils.addMonth(new Date(CodeUtils.convertDataReferenceDateFormat(endDate)), 0));
			}
			List<String> totalBillDateMonth = new LinkedList<String>();
			LOGGER.info("OTB  get All query start...");
			if (activePlan.equalsIgnoreCase("ASD")) {
				
				if (!frequency.equalsIgnoreCase("QUARTERLY")) {

					totalRecord = otbMapper.getASDPlanRecord(planId, startDate, endDate);
					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo - 1) * pageSize;
					asdData = otbMapper.getASDPlan(offset, pageSize, planId, startDate, endDate);
					for (OTBPlanASD asdPlan : asdData) {
						String assortmentCode = asdPlan.getAssortmentCode();
						List<OTBGraph> finalGraph = new ArrayList<OTBGraph>();
						if (distinctAssortmentCode.add(assortmentCode) && distinctAssortmentCode.size() > offset
								&& (distinctAssortmentCode.size() - offset) <= 10) {
							Set distinctBillDate = new HashSet<>();
							for (int i = 0; i < asdData.size(); i++) {
								OTBGraph otbGraph = new OTBGraph();
								if (assortmentCode.equalsIgnoreCase(asdData.get(i).getAssortmentCode())) {
									otbGraph.setBillDate(
											CodeUtils.convertOTBMonthlyDateFormat(asdData.get(i).getFutureBillDate()));
									otbGraph.setAssortmentCode(asdData.get(i).getAssortmentCode());
									otbGraph.setQtyForecast(asdData.get(i).getQty());
									otbGraph.setPlanSales(asdData.get(i).getPlanSales());
									otbGraph.setClosingInventory(asdData.get(i).getClosingInventory());
									otbGraph.setMarkDown(asdData.get(i).getMarkDown());
									otbGraph.setOpeningStock(asdData.get(i).getOpeningStock());
									otbGraph.setOpenPurchaseOrder(asdData.get(i).getOpenPurchaseOrder());
									otbGraph.setOtbValue(asdData.get(i).getOtbValue());
									finalGraph.add(otbGraph);
								}
							}

							// code for assortment where billDate null

							finalGraph.stream().forEach(e -> {
								distinctBillDate.add(e.getBillDate());
							});
							List getAllMonth = null;
							if (!frequency.equalsIgnoreCase("NA")) {
								getAllMonth = CodeUtils.getAllMonths(startDate, endDate);
							} else {
								getAllMonth = CodeUtils.getAllMonths(startDate, endDate);
							}
							totalBillDateMonth.addAll(distinctBillDate);
							List getAllMisMatchBillDate = CodeUtils.returnMisMatchList(getAllMonth, totalBillDateMonth);
							for (int i = 0; i < getAllMisMatchBillDate.size(); i++) {
								OTBGraph otbGraph = new OTBGraph();
								otbGraph.setBillDate(getAllMisMatchBillDate.get(i).toString());
								otbGraph.setAssortmentCode(assortmentCode);
								otbGraph.setPlanSales(0);
								otbGraph.setClosingInventory(0);
								otbGraph.setMarkDown(0);
								otbGraph.setOpeningStock(0);
								otbGraph.setOpenPurchaseOrder(0);
								otbGraph.setOtbValue(0);
								finalGraph.add(otbGraph);
							}
							Collections.sort(finalGraph, new Comparator<OTBGraph>() {
								public int compare(OTBGraph s1, OTBGraph s2) {
									return CodeUtils.convertOTBMonthlyFormat(s1.getBillDate())
											.compareTo(CodeUtils.convertOTBMonthlyFormat(s2.getBillDate()));
								}
							});
							graphNode.putPOJO(assortmentCode, finalGraph);
							distinctBillDate.clear();
							totalBillDateMonth.clear();
						}
					}
				} else {
					totalRecord = otbMapper.getQuarterASDPlanRecord(planId);
					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo - 1) * pageSize;
					asdData = otbMapper.getQuarterASDPlan(offset, pageSize, planId);
					for (OTBPlanASD asdPlan : asdData) {
						String assortmentCode = asdPlan.getAssortmentCode();
						List<OTBGraph> finalGraph = new ArrayList<OTBGraph>();
						if (distinctAssortmentCode.add(assortmentCode) && distinctAssortmentCode.size() > offset
								&& (distinctAssortmentCode.size() - offset) <= 10) {
							List<String> distinctQuarter = new ArrayList<>();
							for (int i = 0; i < asdData.size(); i++) {
								
								String quarterValue=asdData.get(i).getQuarter().equals("1")?"Q1":
									asdData.get(i).getQuarter().equals("2")?"Q2":
										asdData.get(i).getQuarter().equals("3")?"Q3":"Q4";
								
								
								OTBGraph otbGraph = new OTBGraph();
								if (assortmentCode.equalsIgnoreCase(asdData.get(i).getAssortmentCode())) {
									otbGraph.setBillDate(quarterValue);
									
//									otbGraph.setBilldateFrom(
//											CodeUtils.convertOTBMonthlyDateFormat(asdData.get(i).getBilldateFrom()));
//									otbGraph.setBilldateTo(
//											CodeUtils.convertOTBMonthlyDateFormat(asdData.get(i).getBilldateTo()));
									otbGraph.setQuarter(quarterValue);
									
									otbGraph.setAssortmentCode(asdData.get(i).getAssortmentCode());
									otbGraph.setQtyForecast(asdData.get(i).getQty());
									otbGraph.setPlanSales(Math.round(asdData.get(i).getPlanSales() * 100.0) / 100.0);
									otbGraph.setClosingInventory(Math.round(asdData.get(i).getClosingInventory() * 100.0) / 100.0);
									otbGraph.setMarkDown(Math.round(asdData.get(i).getMarkDown() * 100.0) / 100.0);
									otbGraph.setOpeningStock(Math.round(asdData.get(i).getOpeningStock() * 100.0) / 100.0);
									otbGraph.setOpenPurchaseOrder(Math.round(asdData.get(i).getOpenPurchaseOrder() * 100.0) / 100.0);
									otbGraph.setOtbValue(Math.round(asdData.get(i).getOtbValue() * 100.0) / 100.0);
									finalGraph.add(otbGraph);
								}
							}
							
							finalGraph.stream().distinct().forEach(e -> {
								distinctQuarter.add(e.getQuarter());
							});
//							
							List<String> totalQuarter= new ArrayList<>();
							totalQuarter.add("Q1");
							totalQuarter.add("Q2");
							totalQuarter.add("Q3");
							totalQuarter.add("Q4");
							List getAllMisMatchBillDate=new ArrayList<>();
//							
//							
							totalQuarter.removeAll(distinctQuarter);
							getAllMisMatchBillDate.addAll(totalQuarter);
							
							
							for (int i = 0; i < getAllMisMatchBillDate.size(); i++) {
								OTBGraph otbGraph = new OTBGraph();
								otbGraph.setBillDate(getAllMisMatchBillDate.get(i).toString());
								otbGraph.setAssortmentCode(assortmentCode);
								otbGraph.setPlanSales(0);
								otbGraph.setClosingInventory(0);
								otbGraph.setMarkDown(0);
								otbGraph.setOpeningStock(0);
								otbGraph.setOpenPurchaseOrder(0);
								otbGraph.setOtbValue(0);
								otbGraph.setBilldateFrom(null);
								otbGraph.setBilldateTo(null);
								otbGraph.setQuarter(getAllMisMatchBillDate.get(i).toString());
								finalGraph.add(otbGraph);
							}
							Collections.sort(finalGraph, new Comparator<OTBGraph>() {
								public int compare(OTBGraph s1, OTBGraph s2) {
									return s1.getQuarter()
											.compareTo(s2.getQuarter());
								}
							});
							
							graphNode.putPOJO(assortmentCode, finalGraph);
							distinctQuarter.clear();
							totalBillDateMonth.clear();
						}
					}
				}
			} else if (activePlan.equalsIgnoreCase("DFD")) {

				if (!frequency.equalsIgnoreCase("QUARTERLY")) {

					totalRecord = otbMapper.getDFDPlanRecord(planId, startDate, endDate);
					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo - 1) * pageSize;
					dfdData = otbMapper.getDFDPlan(offset, pageSize, planId, startDate, endDate);
					for (OTBPlanDFD dfdPlan : dfdData) {
						String assortmentCode = dfdPlan.getAssortmentCode();
						List<OTBGraph> finalGraph = new LinkedList<OTBGraph>();
						if (distinctAssortmentCode.add(assortmentCode) && distinctAssortmentCode.size() > offset
								&& (distinctAssortmentCode.size() - offset) <= 10) {
							Set distinctBillDate = new HashSet<>();
							for (int i = 0; i < dfdData.size(); i++) {
								OTBGraph otbGraph = new OTBGraph();
								if (assortmentCode.equalsIgnoreCase(dfdData.get(i).getAssortmentCode())) {
									otbGraph.setBillDate(
											CodeUtils.convertOTBMonthlyDateFormat(dfdData.get(i).getBillDate()));
									otbGraph.setAssortmentCode(dfdData.get(i).getAssortmentCode());
									otbGraph.setQtyForecast(dfdData.get(i).getQtyForecast());
									otbGraph.setPlanSales(dfdData.get(i).getPlanSales());
									otbGraph.setClosingInventory(dfdData.get(i).getClosingInventory());
									otbGraph.setMarkDown(dfdData.get(i).getMarkDown());
									otbGraph.setOpeningStock(dfdData.get(i).getOpeningStock());
									otbGraph.setOpenPurchaseOrder(dfdData.get(i).getOpenPurchaseOrder());
									otbGraph.setOtbValue(dfdData.get(i).getOtbValue());
									finalGraph.add(otbGraph);
								}
							}

							// code for assortment where billDate null
							finalGraph.stream().forEach(e -> {
								distinctBillDate.add(e.getBillDate());
							});

							List getAllMonth = null;
							if (!frequency.equalsIgnoreCase("NA")) {
								getAllMonth = CodeUtils.getAllMonths(startDate, endDate);
							} else {
								getAllMonth = CodeUtils.getAllMonths(startDate, endDate);
							}
							totalBillDateMonth.addAll(distinctBillDate);
							List getAllMisMatchBillDate = CodeUtils.returnMisMatchList(getAllMonth, totalBillDateMonth);
							for (int i = 0; i < getAllMisMatchBillDate.size(); i++) {
								OTBGraph otbGraph = new OTBGraph();
								otbGraph.setBillDate(getAllMisMatchBillDate.get(i).toString());
								otbGraph.setAssortmentCode(assortmentCode);
								otbGraph.setPlanSales(0);
								otbGraph.setClosingInventory(0);
								otbGraph.setMarkDown(0);
								otbGraph.setOpeningStock(0);
								otbGraph.setOpenPurchaseOrder(0);
								otbGraph.setOtbValue(0);
								finalGraph.add(otbGraph);
							}
							Collections.sort(finalGraph, new Comparator<OTBGraph>() {
								public int compare(OTBGraph s1, OTBGraph s2) {
									return CodeUtils.convertOTBMonthlyFormat(s1.getBillDate())
											.compareTo(CodeUtils.convertOTBMonthlyFormat(s2.getBillDate()));
								}
							});
							graphNode.putPOJO(assortmentCode, finalGraph);
							distinctBillDate.clear();
							totalBillDateMonth.clear();
						}
					}
				} else {
					totalRecord = otbMapper.getQuarterDFDPlanRecord(planId);
					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo - 1) * pageSize;
					dfdData = otbMapper.getQuarterDFDPlan(offset, pageSize, planId);
					for (OTBPlanDFD dfdPlan : dfdData) {
						String assortmentCode = dfdPlan.getAssortmentCode();
						List<OTBGraph> finalGraph = new ArrayList<OTBGraph>();
						if (distinctAssortmentCode.add(assortmentCode) && distinctAssortmentCode.size() > offset
								&& (distinctAssortmentCode.size() - offset) <= 10) {
							List<String> distinctQuarter = new ArrayList<>();
							for (int i = 0; i < dfdData.size(); i++) {
								
								String quarterValue=dfdData.get(i).getQuarter().equals("1")?"Q1":
									dfdData.get(i).getQuarter().equals("2")?"Q2":
										dfdData.get(i).getQuarter().equals("3")?"Q3":"Q4";
								
								OTBGraph otbGraph = new OTBGraph();
								if (assortmentCode.equalsIgnoreCase(dfdData.get(i).getAssortmentCode())) {
									otbGraph.setBillDate(quarterValue);
									
//									otbGraph.setBilldateFrom(
//											CodeUtils.convertOTBMonthlyDateFormat(dfdData.get(i).getBilldateFrom()));
//									otbGraph.setBilldateTo(
//											CodeUtils.convertOTBMonthlyDateFormat(dfdData.get(i).getBilldateTo()));
									otbGraph.setQuarter(quarterValue);
									
									otbGraph.setAssortmentCode(dfdData.get(i).getAssortmentCode());
									otbGraph.setQtyForecast(dfdData.get(i).getQtyForecast());
									otbGraph.setPlanSales(dfdData.get(i).getPlanSales());
									otbGraph.setClosingInventory(dfdData.get(i).getClosingInventory());
									otbGraph.setMarkDown(dfdData.get(i).getMarkDown());
									otbGraph.setOpeningStock(dfdData.get(i).getOpeningStock());
									otbGraph.setOpenPurchaseOrder(dfdData.get(i).getOpenPurchaseOrder());
									otbGraph.setOtbValue(dfdData.get(i).getOtbValue());
									finalGraph.add(otbGraph);
								}
							}
							
							finalGraph.stream().distinct().forEach(e -> {
								distinctQuarter.add(e.getQuarter());
							});
//							
							List<String> totalQuarter= new ArrayList<>();
							totalQuarter.add("Q1");
							totalQuarter.add("Q2");
							totalQuarter.add("Q3");
							totalQuarter.add("Q4");
							List getAllMisMatchBillDate=new ArrayList<>();
//							
							totalQuarter.removeAll(distinctQuarter);
							getAllMisMatchBillDate.addAll(totalQuarter);
							
							for (int i = 0; i < getAllMisMatchBillDate.size(); i++) {
								OTBGraph otbGraph = new OTBGraph();
								otbGraph.setBillDate(getAllMisMatchBillDate.get(i).toString());
								otbGraph.setAssortmentCode(assortmentCode);
								otbGraph.setPlanSales(0);
								otbGraph.setClosingInventory(0);
								otbGraph.setMarkDown(0);
								otbGraph.setOpeningStock(0);
								otbGraph.setOpenPurchaseOrder(0);
								otbGraph.setOtbValue(0);
								otbGraph.setBilldateFrom(null);
								otbGraph.setBilldateTo(null);
								otbGraph.setQuarter(getAllMisMatchBillDate.get(i).toString());
								finalGraph.add(otbGraph);
							}
							Collections.sort(finalGraph, new Comparator<OTBGraph>() {
								public int compare(OTBGraph s1, OTBGraph s2) {
									return s1.getQuarter()
											.compareTo(s2.getQuarter());
								}
							});
							
							graphNode.putPOJO(assortmentCode, finalGraph);
							distinctQuarter.clear();
							totalBillDateMonth.clear();
						}
					}
				}
			}
			LOGGER.info("OTB retrive All result...");
			objectNode.put(Pagination.CURRENT_PAGE, pageNo);
			objectNode.put(Pagination.PREVIOUS_PAGE, previousPage);
			objectNode.put(Pagination.MAXIMUM_PAGE, maxPage);
			objectNode.putPOJO(CodeUtils.RESPONSE, graphNode);
		} catch (CannotGetJdbcConnectionException ex) {
			throw ex;
		} catch (DataAccessException ex) {
			throw ex;
		} catch (Exception ex) {
			throw ex;
		}
		return objectNode;
	}

	@SuppressWarnings({ "unused", "rawtypes", "unchecked" })
	@Override
	public ObjectNode getTotalOTBPlan(String activePlan, String frequency, String sDate, String eDate,
			String planId) {
		List<OTBPlanDFD> dfdData = null;
		List<OTBPlanASD> asdData = null;
		List getAllMonth = null;
		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode objectNode = mapper.createObjectNode();
		try {
			String startDate = sDate;
			String endDate = eDate;
			Set distinctAssortmentCode = new LinkedHashSet<String>();
			Set distinctBillDate = new LinkedHashSet<String>();
			if (frequency.equalsIgnoreCase("3_MONTHS")) {
				if (activePlan.equalsIgnoreCase("ASD")) {
					startDate = CodeUtils.__dateFormat.format(CodeUtils.addMonth(new Date(), 1));
					endDate = CodeUtils.__dateFormat.format(CodeUtils.addMonth(new Date(), 3));

				} else if (activePlan.equalsIgnoreCase("DFD")) {
					startDate = CodeUtils.__dateFormat.format(CodeUtils.addMonth(new Date(), 1));
					endDate = CodeUtils.__dateFormat.format(CodeUtils.addMonth(new Date(), 3));
				}
			} else if (frequency.equalsIgnoreCase("6_MONTHS")) {
				if (activePlan.equalsIgnoreCase("ASD")) {
					startDate = CodeUtils.__dateFormat.format(CodeUtils.addMonth(new Date(), 1));
					endDate = CodeUtils.__dateFormat.format(CodeUtils.addMonth(new Date(), 6));
				} else if (activePlan.equalsIgnoreCase("DFD")) {
					startDate = CodeUtils.__dateFormat.format(CodeUtils.addMonth(new Date(), 1));
					endDate = CodeUtils.__dateFormat.format(CodeUtils.addMonth(new Date(), 6));
				}
			} else if (frequency.equalsIgnoreCase("12_MONTHS")) {
				if (activePlan.equalsIgnoreCase("ASD")) {
					startDate = CodeUtils.__dateFormat.format(CodeUtils.addMonth(new Date(), 1));
					endDate = CodeUtils.__dateFormat.format(CodeUtils.addMonth(new Date(), 12));
				} else if (activePlan.equalsIgnoreCase("DFD")) {
					startDate = CodeUtils.__dateFormat.format(CodeUtils.addMonth(new Date(), 1));
					endDate = CodeUtils.__dateFormat.format(CodeUtils.addMonth(new Date(), 12));
				}
			} else if (frequency.equalsIgnoreCase("QUARTERLY")) {
				startDate = CodeUtils.__dateFormat.format(CodeUtils.addMonth(new Date(), 1));
				endDate = CodeUtils.__dateFormat.format(CodeUtils.addMonth(new Date(), 12));
			} else {

				String[] splitStartDate = startDate.split(" - ");
				String[] splitEndDate = endDate.split(" - ");
				int startMonth = Month.valueOf(splitStartDate[0].toUpperCase().trim()).getValue();
				int endMonth = Month.valueOf(splitEndDate[0].toUpperCase().trim()).getValue();
				startDate = splitStartDate[1].trim() + "-" + (startMonth > 9 ? startMonth + "" : "0" + startMonth)
						+ "-01";
				endDate = splitEndDate[1].trim() + "-" + (endMonth > 9 ? endMonth + "" : "0" + endMonth) + "-01";

//				startDate = CodeUtils.__dateFormat
//						.format(new Date(CodeUtils.convertDataReferenceDateFormat(startDate)));
//				endDate = CodeUtils.__dateFormat
//						.format(CodeUtils.addMonth(new Date(CodeUtils.convertDataReferenceDateFormat(endDate)), 0));
			}
			getAllMonth = CodeUtils.getAllMonths(startDate, endDate);
			List<String> totalBillDateMonth = new LinkedList<String>();
			LOGGER.info("OTB  get All query start...");
			String asdBillDate = null;
			double planSales = 0L;
			double closingInventory = 0L;
			double markDown = 0L;
			double openingStock = 0L;
			double openPurchaseOrder = 0L;
			double otbValue = 0L;
			List<OTBGraph> finalGraph = new LinkedList<OTBGraph>();
			TreeMap<OTBGraph, String> tm = new TreeMap<OTBGraph, String>();
			if (activePlan.equalsIgnoreCase("ASD")) {

				if (!frequency.equalsIgnoreCase("QUARTERLY")) {

					asdData = otbMapper.getTotalASDPlan(planId, startDate, endDate);
					for (OTBPlanASD asdPlan : asdData) {
						String billDate = asdPlan.getFutureBillDate();
						if (distinctBillDate.add(CodeUtils.convertOTBMonthlyDateFormat(billDate))) {
							OTBGraph otbGraph = new OTBGraph();
							for (int i = 0; i < asdData.size(); i++) {
								if (billDate.equalsIgnoreCase(asdData.get(i).getFutureBillDate())) {
									planSales += asdData.get(i).getPlanSales();
									closingInventory += asdData.get(i).getClosingInventory();
									markDown += asdData.get(i).getMarkDown();
									openingStock += asdData.get(i).getOpeningStock();
									openPurchaseOrder += asdData.get(i).getOpenPurchaseOrder();
									otbValue += asdData.get(i).getOtbValue();
								}
							}
							otbGraph.setBillDate(CodeUtils.convertOTBMonthlyDateFormat(billDate));
							otbGraph.setPlanSales(Math.round(planSales * 100.00) / 100.00);
							otbGraph.setClosingInventory(Math.round(closingInventory * 100.00) / 100.00);
							otbGraph.setMarkDown(Math.round(markDown * 100.00) / 100.00);
							otbGraph.setOpeningStock(Math.round(openingStock * 100.00) / 100.00);
							otbGraph.setOpenPurchaseOrder(Math.round(openPurchaseOrder * 100.00) / 100.00);
							otbGraph.setOtbValue(Math.round(otbValue * 100.00) / 100.00);
							finalGraph.add(otbGraph);
							planSales=0;
							closingInventory=0;
							markDown=0;
							openingStock=0;
							openPurchaseOrder=0;
							otbValue=0;
						}
					}
					totalBillDateMonth.addAll(distinctBillDate);
					List getAllMisMatchBillDate = CodeUtils.returnMisMatchList(totalBillDateMonth, getAllMonth);
					for (int i = 0; i < getAllMisMatchBillDate.size(); i++) {
						OTBGraph otbGraph = new OTBGraph();
						otbGraph.setBillDate(getAllMisMatchBillDate.get(i).toString());
						otbGraph.setPlanSales(0);
						otbGraph.setClosingInventory(0);
						otbGraph.setMarkDown(0);
						otbGraph.setOpeningStock(0);
						otbGraph.setOpenPurchaseOrder(0);
						otbGraph.setOtbValue(0);
						finalGraph.add(otbGraph);
					}

				} else {

					asdData = otbMapper.getTotalQuarterASDPlan(planId);
					for (OTBPlanASD asdPlan : asdData) {
						String quarterValue = asdPlan.getQuarter().equals("1") ? "Q1"
								: asdPlan.getQuarter().equals("2") ? "Q2"
										: asdPlan.getQuarter().equals("3") ? "Q3" : "Q4";

						String quarter = quarterValue;
						if (distinctBillDate.add(quarter)) {
							OTBGraph otbGraph = new OTBGraph();
							for (int i = 0; i < asdData.size(); i++) {
								String qtrValue = asdData.get(i).getQuarter().equals("1") ? "Q1"
										: asdData.get(i).getQuarter().equals("2") ? "Q2"
												: asdData.get(i).getQuarter().equals("3") ? "Q3" : "Q4";

								if (quarter.equalsIgnoreCase(qtrValue)) {
									planSales += asdData.get(i).getPlanSales();
									closingInventory += asdData.get(i).getClosingInventory();
									markDown += asdData.get(i).getMarkDown();
									openingStock += asdData.get(i).getOpeningStock();
									openPurchaseOrder += asdData.get(i).getOpenPurchaseOrder();
									otbValue += asdData.get(i).getOtbValue();
								}
							}
							otbGraph.setBillDate(quarter);
							otbGraph.setBilldateFrom(null);
							otbGraph.setBilldateTo(null);
							otbGraph.setQuarter(quarter);
							otbGraph.setPlanSales(Math.round(planSales * 100.00) / 100.00);
							otbGraph.setClosingInventory(Math.round(closingInventory * 100.00) / 100.00);
							otbGraph.setMarkDown(Math.round(markDown * 100.00) / 100.00);
							otbGraph.setOpeningStock(Math.round(openingStock * 100.00) / 100.00);
							otbGraph.setOpenPurchaseOrder(Math.round(openPurchaseOrder * 100.00) / 100.00);
							otbGraph.setOtbValue(Math.round(otbValue * 100.00) / 100.00);
							finalGraph.add(otbGraph);
							planSales=0;
							closingInventory=0;
							markDown=0;
							openingStock=0;
							openPurchaseOrder=0;
							otbValue=0;
						}
					}
					finalGraph.stream().distinct().forEach(e -> {
						distinctBillDate.add(e.getQuarter());
					});
//					
					List<String> totalQuarter = new ArrayList<>();
					totalQuarter.add("Q1");
					totalQuarter.add("Q2");
					totalQuarter.add("Q3");
					totalQuarter.add("Q4");
					List getAllMisMatchBillDate = new ArrayList<>();
//					
					totalQuarter.removeAll(distinctBillDate);
					getAllMisMatchBillDate.addAll(totalQuarter);

					for (int i = 0; i < getAllMisMatchBillDate.size(); i++) {
						OTBGraph otbGraph = new OTBGraph();
						otbGraph.setBillDate(getAllMisMatchBillDate.get(i).toString());
						otbGraph.setBilldateFrom(null);
						otbGraph.setBilldateTo(null);
						otbGraph.setQuarter(getAllMisMatchBillDate.get(i).toString());
						otbGraph.setPlanSales(0);
						otbGraph.setClosingInventory(0);
						otbGraph.setMarkDown(0);
						otbGraph.setOpeningStock(0);
						otbGraph.setOpenPurchaseOrder(0);
						otbGraph.setOtbValue(0);
						finalGraph.add(otbGraph);
					}

				}

			} else if (activePlan.equalsIgnoreCase("DFD")) {

				if (!frequency.equalsIgnoreCase("QUARTERLY")) {
					dfdData = otbMapper.getTotalDFDPlan(planId, startDate, endDate);
					for (OTBPlanDFD dfdPlan : dfdData) {
						String billDate = dfdPlan.getBillDate();
						if (distinctBillDate.add(CodeUtils.convertOTBMonthlyDateFormat(billDate))) {
							OTBGraph otbGraph = new OTBGraph();
							for (int i = 0; i < dfdData.size(); i++) {
								if (billDate.equalsIgnoreCase(dfdData.get(i).getBillDate())) {
									planSales += dfdData.get(i).getPlanSales();
									closingInventory += dfdData.get(i).getClosingInventory();
									markDown += dfdData.get(i).getMarkDown();
									openingStock += dfdData.get(i).getOpeningStock();
									openPurchaseOrder += dfdData.get(i).getOpenPurchaseOrder();
									otbValue += dfdData.get(i).getOtbValue();
								}
							}

							otbGraph.setBillDate(CodeUtils.convertOTBMonthlyDateFormat(billDate));
							otbGraph.setPlanSales(Math.round(planSales * 100.00) / 100.00);
							otbGraph.setClosingInventory(Math.round(closingInventory * 100.00) / 100.00);
							otbGraph.setMarkDown(Math.round(markDown * 100.00) / 100.00);
							otbGraph.setOpeningStock(Math.round(openingStock * 100.00) / 100.00);
							otbGraph.setOpenPurchaseOrder(Math.round(openPurchaseOrder * 100.00) / 100.00);
							otbGraph.setOtbValue(Math.round(otbValue * 100.00) / 100.00);
							finalGraph.add(otbGraph);
							planSales=0;
							closingInventory=0;
							markDown=0;
							openingStock=0;
							openPurchaseOrder=0;
							otbValue=0;
						}
					}
					totalBillDateMonth.addAll(distinctBillDate);
					List getAllMisMatchBillDate = CodeUtils.returnMisMatchList(getAllMonth, totalBillDateMonth);
					for (int i = 0; i < getAllMisMatchBillDate.size(); i++) {
						OTBGraph otbGraph = new OTBGraph();
						otbGraph.setBillDate(getAllMisMatchBillDate.get(i).toString());
						otbGraph.setPlanSales(0);
						otbGraph.setClosingInventory(0);
						otbGraph.setMarkDown(0);
						otbGraph.setOpeningStock(0);
						otbGraph.setOpenPurchaseOrder(0);
						otbGraph.setOtbValue(0);
						finalGraph.add(otbGraph);
						planSales=0;
						closingInventory=0;
						markDown=0;
						openingStock=0;
						openPurchaseOrder=0;
						otbValue=0;
					}
				} else {

					dfdData = otbMapper.getTotalQuarterDFDPlan(planId);
					for (OTBPlanDFD dfdPlan : dfdData) {
						String quarterValue = dfdPlan.getQuarter().equals("1") ? "Q1"
								: dfdPlan.getQuarter().equals("2") ? "Q2"
										: dfdPlan.getQuarter().equals("3") ? "Q3" : "Q4";

						String quarter = quarterValue;
						if (distinctBillDate.add(quarter)) {
							OTBGraph otbGraph = new OTBGraph();
							for (int i = 0; i < dfdData.size(); i++) {

								String qtrValue = dfdData.get(i).getQuarter().equals("1") ? "Q1"
										: dfdData.get(i).getQuarter().equals("2") ? "Q2"
												: dfdData.get(i).getQuarter().equals("3") ? "Q3" : "Q4";

								if (quarter.equalsIgnoreCase(qtrValue)) {
									planSales += dfdData.get(i).getPlanSales();
									closingInventory += dfdData.get(i).getClosingInventory();
									markDown += dfdData.get(i).getMarkDown();
									openingStock += dfdData.get(i).getOpeningStock();
									openPurchaseOrder += dfdData.get(i).getOpenPurchaseOrder();
									otbValue += dfdData.get(i).getOtbValue();
								}
							}

							otbGraph.setBillDate(quarter);
							otbGraph.setBilldateFrom(null);
							otbGraph.setBilldateTo(null);
							otbGraph.setQuarter(quarter);
							otbGraph.setPlanSales(Math.round(planSales * 100.00) / 100.00);
							otbGraph.setClosingInventory(Math.round(closingInventory * 100.00) / 100.00);
							otbGraph.setMarkDown(Math.round(markDown * 100.00) / 100.00);
							otbGraph.setOpeningStock(Math.round(openingStock * 100.00) / 100.00);
							otbGraph.setOpenPurchaseOrder(Math.round(openPurchaseOrder * 100.00) / 100.00);
							otbGraph.setOtbValue(Math.round(otbValue * 100.00) / 100.00);
							finalGraph.add(otbGraph);
							planSales=0L;
							closingInventory=0L;
							markDown=0L;
							openingStock=0L;
							openPurchaseOrder=0L;
							otbValue=0L;
						}
					}
					finalGraph.stream().distinct().forEach(e -> {
						distinctBillDate.add(e.getQuarter());
					});
//					
					List<String> totalQuarter = new ArrayList<>();
					totalQuarter.add("Q1");
					totalQuarter.add("Q2");
					totalQuarter.add("Q3");
					totalQuarter.add("Q4");
					List getAllMisMatchBillDate = new ArrayList<>();
//					
					totalQuarter.removeAll(distinctBillDate);
					getAllMisMatchBillDate.addAll(totalQuarter);

					for (int i = 0; i < getAllMisMatchBillDate.size(); i++) {
						OTBGraph otbGraph = new OTBGraph();
						otbGraph.setBillDate(getAllMisMatchBillDate.get(i).toString());
						otbGraph.setBilldateFrom(null);
						otbGraph.setBilldateTo(null);
						otbGraph.setQuarter(getAllMisMatchBillDate.get(i).toString());
						otbGraph.setPlanSales(0);
						otbGraph.setClosingInventory(0);
						otbGraph.setMarkDown(0);
						otbGraph.setOpeningStock(0);
						otbGraph.setOpenPurchaseOrder(0);
						otbGraph.setOtbValue(0);
						finalGraph.add(otbGraph);
					}
				}
			}
			if (!frequency.equalsIgnoreCase("QUARTERLY")) {
				Collections.sort(finalGraph, new Comparator<OTBGraph>() {
					public int compare(OTBGraph s1, OTBGraph s2) {
						return CodeUtils.convertOTBMonthlyFormat(s1.getBillDate())
								.compareTo(CodeUtils.convertOTBMonthlyFormat(s2.getBillDate()));
					}
				});
			} else {
				Collections.sort(finalGraph, new Comparator<OTBGraph>() {
					public int compare(OTBGraph s1, OTBGraph s2) {
						return s1.getQuarter().compareTo(s2.getQuarter());
					}
				});
			}

			LOGGER.info("OTB retrive All result...");
			objectNode.putPOJO(CodeUtils.RESPONSE, finalGraph);
		} catch (CannotGetJdbcConnectionException ex) {
			throw ex;
		} catch (DataAccessException ex) {
			throw ex;
		} catch (Exception ex) {
			throw ex;
		}
		return objectNode;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public String exportOTBPlan(String planId, String activePlan, String frequency, String sDate, String eDate,
			HttpServletRequest request) {
		
		mapOfAssortmentWiseMetricDataCopy.clear();
		isInserted = false;
		List<DownloadOTBPlanASD> asdData = null;
		List<DownloadOTBPlanDFD> dfdData = null;
		String fileName = null;
		String filePath = null;
		File file = null;
		String etag = null;
		String url = null;
		double planSales = 0L;
		double closingInventory = 0L;
		double markDown = 0L;
		double openingStock = 0L;
		double openPurchaseOrder = 0L;
		double otbValue = 0L;
		Set distinctBillDate = new LinkedHashSet<String>();
		List<OTBGraph> finalGraph = new LinkedList<OTBGraph>();
		List<String> totalQtr=new ArrayList<>();
		totalQtr.add("1");
		totalQtr.add("2");
		totalQtr.add("3");
		totalQtr.add("4");
		listQuarterValue=new HashSet<>();

		try {
			String token = ((HttpServletRequest) request).getHeader("X-Auth-Token");
			JsonArray orgDetails=CodeUtils.decode(token).get("ORG_DETAILS").getAsJsonArray();
			
			JsonObject jNode=orgDetails.get(0).getAsJsonObject();
			String orgId = jNode.get("orgId").getAsString();
			
			
			String tenantHashkey=ThreadLocalStorage.getTenantHashKey();
			String startDate=sDate;
			String endDate=eDate;
			if (frequency.equalsIgnoreCase("3_MONTHS")) {
				if (activePlan.equalsIgnoreCase("ASD")) {
					startDate = CodeUtils.__dateFormat.format(CodeUtils.addMonth(new Date(), 1));
					endDate = CodeUtils.__dateFormat.format(CodeUtils.addMonth(new Date(), 3));

				} else if (activePlan.equalsIgnoreCase("DFD")) {
					startDate = CodeUtils.__dateFormat.format(CodeUtils.addMonth(new Date(), 1));
					endDate = CodeUtils.__dateFormat.format(CodeUtils.addMonth(new Date(), 3));
				}
			} else if (frequency.equalsIgnoreCase("6_MONTHS")) {
				if (activePlan.equalsIgnoreCase("ASD")) {
					startDate = CodeUtils.__dateFormat.format(CodeUtils.addMonth(new Date(), 1));
					endDate = CodeUtils.__dateFormat.format(CodeUtils.addMonth(new Date(), 6));
				} else if (activePlan.equalsIgnoreCase("DFD")) {
					startDate = CodeUtils.__dateFormat.format(CodeUtils.addMonth(new Date(), 1));
					endDate = CodeUtils.__dateFormat.format(CodeUtils.addMonth(new Date(), 6));
				}
			} else if (frequency.equalsIgnoreCase("12_MONTHS")) {
				if (activePlan.equalsIgnoreCase("ASD")) {
					startDate = CodeUtils.__dateFormat.format(CodeUtils.addMonth(new Date(), 1));
					endDate = CodeUtils.__dateFormat.format(CodeUtils.addMonth(new Date(), 12));
				} else if (activePlan.equalsIgnoreCase("DFD")) {
					startDate = CodeUtils.__dateFormat.format(CodeUtils.addMonth(new Date(), 1));
					endDate = CodeUtils.__dateFormat.format(CodeUtils.addMonth(new Date(), 12));
				}
			}  else if (frequency.equalsIgnoreCase("QUARTERLY")) {
					startDate = "1";
					endDate = "4";
			} else {
				
				String[] splitStartDate=startDate.split(" - ");
				String[] splitEndDate=endDate.split(" - ");
				int startMonth=Month.valueOf(splitStartDate[0].toUpperCase().trim()).getValue();
				int endMonth=Month.valueOf(splitEndDate[0].toUpperCase().trim()).getValue();
				startDate=splitStartDate[1].trim()+ "-"+(startMonth>9?startMonth+"":"0"+startMonth )+"-01";
				endDate=splitEndDate[1].trim()+"-"+ (endMonth>9?endMonth+"":"0"+endMonth )+"-01";
				
//				startDate = CodeUtils.__dateFormat
//						.format(new Date(CodeUtils.convertDataReferenceDateFormat(startDate)));
//				endDate = CodeUtils.__dateFormat
//						.format(CodeUtils.addMonth(new Date(CodeUtils.convertDataReferenceDateFormat(endDate)), -1));
			}

			S3BucketInfo s3BucketInfo = s3BucketInfoService.getBucketByType(BucketParameter.GENERATE_EXCELCSV,orgId);
			
			// this tenant is set for specific style baazar query
			if (!tenantHashkey.equalsIgnoreCase(TenantHash.valueOf("STYLE_BAAZAR").getHashKey())) {
				if (activePlan.equalsIgnoreCase("ASD")) {
					asdData = otbMapper.importASDPlan(planId, startDate, endDate);
					fileName = "ACTUAL_SALES_DATA_REPORT";
				} else if (activePlan.equalsIgnoreCase("DFD")) {
					dfdData = otbMapper.importDFDPlan(planId, startDate, endDate);
					fileName = "DEMAND_FORECAST_DATA_REPORT";
				}
			}else {
				if (activePlan.equalsIgnoreCase("ASD")) {
					asdData = otbMapper.importQuarterASDPlan(planId, startDate, endDate);
					fileName = "ACTUAL_SALES_DATA_REPORT";
				} else if (activePlan.equalsIgnoreCase("DFD")) {
					dfdData = otbMapper.importQuarterDFDPlan(planId, startDate, endDate);
					fileName = "DEMAND_FORECAST_DATA_REPORT";
				}
			}
			String randomFolder = UUID.randomUUID().toString();
			String s3BucketFilePath = s3BucketInfo.getBucketPath() + CustomParameter.DELIMETER + randomFolder;
			String savedPath = File.separator + s3BucketInfo.getEnterprise() + File.separator + "EXCEL" + File.separator
					+ fileName + FileUtils.EXCELExtension;
			filePath = FileUtils.createFilePath(FileUtils.getPlatformBasedParentDir(), savedPath);
			if (activePlan.equalsIgnoreCase("ASD")) {

				Set distinctAssortmentWise = new HashSet<>();
				List<Map<String, Map<String, List<Map<String, String>>>>> assortmentWiseMetricsData = new LinkedList<>();
				asdData.stream().forEach(a -> {
					Map<String, Map<String, List<Map<String, String>>>> mapOfAssortmentWiseMetricData = new LinkedHashMap<>();
					assortment = a.getASSORTMENT_CODE();
					String billMonth = a.getBILLDATE();

					Map<String, List<Map<String, String>>> mapOfMetrics = new LinkedHashMap<>();
					List<Map<String, String>> listofMonthWiseData = new LinkedList<>();
					Map<String, String> mapofMonthData = new LinkedHashMap<>();

					assortmentWiseMetricsData.forEach(e -> {
						if (e.containsKey(assortment)) {
							isInserted = true;
							exit = true;
							mapOfAssortmentWiseMetricDataCopy = e.get(assortment);
						} else {
							if (!exit)
								isInserted = false;
						}
					});

					if (isInserted) {
						listofMonthWiseData = mapOfAssortmentWiseMetricDataCopy.get(OtbMsg.PLAN_SALES);
						mapofMonthData.put(billMonth, Double.toString(a.getPLAN_SALES()));
						mapOfMetrics.remove(OtbMsg.PLAN_SALES);
						listofMonthWiseData.add(mapofMonthData);
						mapOfMetrics.put(OtbMsg.PLAN_SALES, listofMonthWiseData);

						mapofMonthData = new LinkedHashMap<>();
						listofMonthWiseData = new LinkedList<>();
						mapOfMetrics.remove(OtbMsg.PLAN_MARKDOWN);
						listofMonthWiseData = mapOfAssortmentWiseMetricDataCopy.get(OtbMsg.PLAN_MARKDOWN);
						mapofMonthData.put(billMonth, Double.toString(a.getMARK_DOWN()));
						listofMonthWiseData.add(mapofMonthData);
						mapOfMetrics.put(OtbMsg.PLAN_MARKDOWN, listofMonthWiseData);

						mapofMonthData = new LinkedHashMap<>();
						listofMonthWiseData = new LinkedList<>();
						mapOfMetrics.remove(OtbMsg.PLAN_CLOSING_INVENTORY);
						listofMonthWiseData = mapOfAssortmentWiseMetricDataCopy.get(OtbMsg.PLAN_CLOSING_INVENTORY);
						mapofMonthData.put(billMonth, Double.toString(a.getCLOSING_INVENTORY()));
						listofMonthWiseData.add(mapofMonthData);
						mapOfMetrics.put(OtbMsg.PLAN_CLOSING_INVENTORY, listofMonthWiseData);

						mapofMonthData = new LinkedHashMap<>();
						listofMonthWiseData = new LinkedList<>();
						mapOfMetrics.remove(OtbMsg.PLAN_OPENING_STOCK);
						listofMonthWiseData = mapOfAssortmentWiseMetricDataCopy.get(OtbMsg.PLAN_OPENING_STOCK);
						mapofMonthData.put(billMonth, Double.toString(a.getOPENING_STOCK()));
						listofMonthWiseData.add(mapofMonthData);
						mapOfMetrics.put(OtbMsg.PLAN_OPENING_STOCK, listofMonthWiseData);

						mapofMonthData = new LinkedHashMap<>();
						listofMonthWiseData = new LinkedList<>();
						mapOfMetrics.remove(OtbMsg.PLAN_PURCHASE_ORDER);
						listofMonthWiseData = mapOfAssortmentWiseMetricDataCopy.get(OtbMsg.PLAN_PURCHASE_ORDER);
						mapofMonthData.put(billMonth, Double.toString(a.getOPEN_PURCHASE_ORDER()));
						listofMonthWiseData.add(mapofMonthData);
						mapOfMetrics.put(OtbMsg.PLAN_PURCHASE_ORDER, listofMonthWiseData);

						mapofMonthData = new LinkedHashMap<>();
						listofMonthWiseData = new LinkedList<>();
						mapOfMetrics.remove(OtbMsg.PLAN_OTB_VALUE);
						listofMonthWiseData = mapOfAssortmentWiseMetricDataCopy.get(OtbMsg.PLAN_OTB_VALUE);
						mapofMonthData.put(billMonth, Double.toString(a.getOTB_VALUE()));
						listofMonthWiseData.add(mapofMonthData);
						mapOfMetrics.put(OtbMsg.PLAN_OTB_VALUE, listofMonthWiseData);

					} else {
						listofMonthWiseData = new LinkedList<>();
						mapofMonthData.put(billMonth, Double.toString(a.getPLAN_SALES()));
						listofMonthWiseData.add(mapofMonthData);
						mapOfMetrics.put(OtbMsg.PLAN_SALES, listofMonthWiseData);

						mapofMonthData = new LinkedHashMap<>();
						listofMonthWiseData = new LinkedList<>();
						mapofMonthData.put(billMonth, Double.toString(a.getMARK_DOWN()));
						listofMonthWiseData.add(mapofMonthData);
						mapOfMetrics.put(OtbMsg.PLAN_MARKDOWN, listofMonthWiseData);

						mapofMonthData = new LinkedHashMap<>();
						listofMonthWiseData = new LinkedList<>();
						mapofMonthData.put(billMonth, Double.toString(a.getCLOSING_INVENTORY()));
						listofMonthWiseData.add(mapofMonthData);
						mapOfMetrics.put(OtbMsg.PLAN_CLOSING_INVENTORY, listofMonthWiseData);

						mapofMonthData = new LinkedHashMap<>();
						listofMonthWiseData = new LinkedList<>();
						mapofMonthData.put(billMonth, Double.toString(a.getOPENING_STOCK()));
						listofMonthWiseData.add(mapofMonthData);
						mapOfMetrics.put(OtbMsg.PLAN_OPENING_STOCK, listofMonthWiseData);

						mapofMonthData = new LinkedHashMap<>();
						listofMonthWiseData = new LinkedList<>();
						mapofMonthData.put(billMonth, Double.toString(a.getOPEN_PURCHASE_ORDER()));
						listofMonthWiseData.add(mapofMonthData);
						mapOfMetrics.put(OtbMsg.PLAN_PURCHASE_ORDER, listofMonthWiseData);

						mapofMonthData = new LinkedHashMap<>();
						listofMonthWiseData = new LinkedList<>();
						mapofMonthData.put(billMonth, Double.toString(a.getOTB_VALUE()));
						listofMonthWiseData.add(mapofMonthData);
						mapOfMetrics.put(OtbMsg.PLAN_OTB_VALUE, listofMonthWiseData);

					}
					exit = false;
					// Create one row item with metrics wise data for year .
					mapOfAssortmentWiseMetricData.put(assortment, mapOfMetrics);
					if (distinctAssortmentWise.add(assortment))
						assortmentWiseMetricsData.add(mapOfAssortmentWiseMetricData);
				});

				// Code for Calculate Total OTB for Actual Sale Data

				for (DownloadOTBPlanASD asdPlan : asdData) {
					String billDate = asdPlan.getBILLDATE();
					if (distinctBillDate.add(billDate)) {
						OTBGraph otbGraph = new OTBGraph();
						for (int i = 0; i < asdData.size(); i++) {
							if (billDate.equalsIgnoreCase(asdData.get(i).getBILLDATE())) {
								planSales += asdData.get(i).getPLAN_SALES();
								closingInventory += asdData.get(i).getCLOSING_INVENTORY();
								markDown += asdData.get(i).getMARK_DOWN();
								openingStock += asdData.get(i).getOPENING_STOCK();
								openPurchaseOrder += asdData.get(i).getOPEN_PURCHASE_ORDER();
								otbValue += asdData.get(i).getOTB_VALUE();
							}
						}
						otbGraph.setBillDate(billDate);
						otbGraph.setAssortmentCode("Total");
						otbGraph.setPlanSales(Math.round(planSales * 100.00) / 100.00);
						otbGraph.setClosingInventory(Math.round(closingInventory * 100.00) / 100.00);
						otbGraph.setMarkDown(Math.round(markDown * 100.00) / 100.00);
						otbGraph.setOpeningStock(Math.round(openingStock * 100.00) / 100.00);
						otbGraph.setOpenPurchaseOrder(Math.round(openPurchaseOrder * 100.00) / 100.00);
						otbGraph.setOtbValue(Math.round(otbValue * 100.00) / 100.00);
						finalGraph.add(otbGraph);
						planSales=0L;
						closingInventory=0L;
						markDown=0L;
						openingStock=0L;
						openPurchaseOrder=0L;
						otbValue=0L;
					}
				}
				isInserted = false;

				List<Map<String, Map<String, List<Map<String, String>>>>> totalAssortmentWiseMetricsData = new LinkedList<>();
				Set dinstinctTotalAssortmentWise = new HashSet<>();
				for (int i = 0; i < finalGraph.size(); i++) {
					String billDate = finalGraph.get(i).getBillDate();
					Map<String, Map<String, List<Map<String, String>>>> mapOfTotalAssortmentWiseMetricData = new LinkedHashMap<>();
					Map<String, List<Map<String, String>>> totalAssortment = new LinkedHashMap<>();
					Map<String, String> totalBillDateWise = new LinkedHashMap<>();
					List<Map<String, String>> finalData = new LinkedList<>();
					assortment = finalGraph.get(i).getAssortmentCode();

					totalAssortmentWiseMetricsData.forEach(e1 -> {
						if (e1.containsKey(assortment)) {
							isInserted = true;
							exit = true;
							mapOfAssortmentWiseMetricDataCopy = e1.get(assortment);
						} else {
							if (!exit)
								isInserted = false;
						}
					});

					if (isInserted) {

						totalBillDateWise = new LinkedHashMap<>();
						finalData = new LinkedList<>();
						totalAssortment.remove(OtbMsg.PLAN_SALES);
						finalData = mapOfAssortmentWiseMetricDataCopy.get(OtbMsg.PLAN_SALES);
						totalBillDateWise.put(billDate, Double.toString(finalGraph.get(i).getPlanSales()));
						finalData.add(totalBillDateWise);
						totalAssortment.put(OtbMsg.PLAN_SALES, finalData);

						totalBillDateWise = new LinkedHashMap<>();
						finalData = new LinkedList<>();
						totalAssortment.remove(OtbMsg.PLAN_MARKDOWN);
						finalData = mapOfAssortmentWiseMetricDataCopy.get(OtbMsg.PLAN_MARKDOWN);
						totalBillDateWise.put(billDate, Double.toString(finalGraph.get(i).getMarkDown()));
						finalData.add(totalBillDateWise);
						totalAssortment.put(OtbMsg.PLAN_MARKDOWN, finalData);

						totalBillDateWise = new LinkedHashMap<>();
						finalData = new LinkedList<>();
						totalAssortment.remove(OtbMsg.PLAN_CLOSING_INVENTORY);
						finalData = mapOfAssortmentWiseMetricDataCopy.get(OtbMsg.PLAN_CLOSING_INVENTORY);
						totalBillDateWise.put(billDate, Double.toString(finalGraph.get(i).getClosingInventory()));
						finalData.add(totalBillDateWise);
						totalAssortment.put(OtbMsg.PLAN_CLOSING_INVENTORY, finalData);

						totalBillDateWise = new LinkedHashMap<>();
						finalData = new LinkedList<>();
						totalAssortment.remove(OtbMsg.PLAN_OPENING_STOCK);
						finalData = mapOfAssortmentWiseMetricDataCopy.get(OtbMsg.PLAN_OPENING_STOCK);
						totalBillDateWise.put(billDate, Double.toString(finalGraph.get(i).getOpeningStock()));
						finalData.add(totalBillDateWise);
						totalAssortment.put(OtbMsg.PLAN_OPENING_STOCK, finalData);

						totalBillDateWise = new LinkedHashMap<>();
						finalData = new LinkedList<>();
						totalAssortment.remove(OtbMsg.PLAN_PURCHASE_ORDER);
						finalData = mapOfAssortmentWiseMetricDataCopy.get(OtbMsg.PLAN_PURCHASE_ORDER);
						totalBillDateWise.put(billDate, Double.toString(finalGraph.get(i).getOpenPurchaseOrder()));
						finalData.add(totalBillDateWise);
						totalAssortment.put(OtbMsg.PLAN_PURCHASE_ORDER, finalData);

						totalBillDateWise = new LinkedHashMap<>();
						finalData = new LinkedList<>();
						totalAssortment.remove(OtbMsg.PLAN_OTB_VALUE);
						finalData = mapOfAssortmentWiseMetricDataCopy.get(OtbMsg.PLAN_OTB_VALUE);
						totalBillDateWise.put(billDate, Double.toString(finalGraph.get(i).getOtbValue()));
						finalData.add(totalBillDateWise);
						totalAssortment.put(OtbMsg.PLAN_OTB_VALUE, finalData);
					} else {
						totalBillDateWise = new LinkedHashMap<>();
						finalData = new LinkedList<>();
						totalBillDateWise.put(billDate, Double.toString(finalGraph.get(i).getPlanSales()));
						finalData.add(totalBillDateWise);
						totalAssortment.put(OtbMsg.PLAN_SALES, finalData);

						totalBillDateWise = new LinkedHashMap<>();
						finalData = new LinkedList<>();
						totalBillDateWise.put(billDate, Double.toString(finalGraph.get(i).getMarkDown()));
						finalData.add(totalBillDateWise);
						totalAssortment.put(OtbMsg.PLAN_MARKDOWN, finalData);

						totalBillDateWise = new LinkedHashMap<>();
						finalData = new LinkedList<>();
						totalBillDateWise.put(billDate, Double.toString(finalGraph.get(i).getClosingInventory()));
						finalData.add(totalBillDateWise);
						totalAssortment.put(OtbMsg.PLAN_CLOSING_INVENTORY, finalData);

						totalBillDateWise = new LinkedHashMap<>();
						finalData = new LinkedList<>();
						totalBillDateWise.put(billDate, Double.toString(finalGraph.get(i).getOpeningStock()));
						finalData.add(totalBillDateWise);
						totalAssortment.put(OtbMsg.PLAN_OPENING_STOCK, finalData);

						totalBillDateWise = new LinkedHashMap<>();
						finalData = new LinkedList<>();
						totalBillDateWise.put(billDate, Double.toString(finalGraph.get(i).getOpenPurchaseOrder()));
						finalData.add(totalBillDateWise);
						totalAssortment.put(OtbMsg.PLAN_PURCHASE_ORDER, finalData);

						totalBillDateWise = new LinkedHashMap<>();
						finalData = new LinkedList<>();
						totalBillDateWise.put(billDate, Double.toString(finalGraph.get(i).getOtbValue()));
						finalData.add(totalBillDateWise);
						totalAssortment.put(OtbMsg.PLAN_OTB_VALUE, finalData);

					}
					exit = false;

					mapOfTotalAssortmentWiseMetricData.put(assortment, totalAssortment);
					if (dinstinctTotalAssortmentWise.add(assortment))
						totalAssortmentWiseMetricsData.add(mapOfTotalAssortmentWiseMetricData);

				}

				LOGGER.debug(String.format("Total Assortment %s", totalAssortmentWiseMetricsData));
				exportListTOExcel(assortmentWiseMetricsData, totalAssortmentWiseMetricsData, filePath, startDate,
						endDate);

			} else if (activePlan.equalsIgnoreCase("DFD")) {
				
				listQuarterValue=new HashSet<>();

				Set distinctAssortmentWise = new HashSet<>();
				List<Map<String, Map<String, List<Map<String, String>>>>> assortmentWiseMetricsData = new LinkedList<>();
				dfdData.stream().forEach(a -> {
					Map<String, Map<String, List<Map<String, String>>>> mapOfAssortmentWiseMetricData = new LinkedHashMap<>();
					assortment = a.getASSORTMENT_CODE();
					String billMonth = a.getBILLDATE();

					Map<String, List<Map<String, String>>> mapOfMetrics = new LinkedHashMap<>();
					List<Map<String, String>> listofMonthWiseData = new LinkedList<>();
					Map<String, String> mapofMonthData = new LinkedHashMap<>();

					assortmentWiseMetricsData.forEach(e -> {
						listQuarterValue.clear();
						if (e.containsKey(assortment)) {
							isInserted = true;
							exit = true;
							mapOfAssortmentWiseMetricDataCopy = e.get(assortment);
						} else {
							if (!exit)
								isInserted = false;
						}
					});

					if (isInserted) {
						listofMonthWiseData = mapOfAssortmentWiseMetricDataCopy.get(OtbMsg.PLAN_SALES);
						mapofMonthData.put(billMonth, Double.toString(a.getPLAN_SALES()));
						mapOfMetrics.remove(OtbMsg.PLAN_SALES);
						listofMonthWiseData.add(mapofMonthData);
						mapOfMetrics.put(OtbMsg.PLAN_SALES, listofMonthWiseData);

						mapofMonthData = new LinkedHashMap<>();
						listofMonthWiseData = new LinkedList<>();
						mapOfMetrics.remove(OtbMsg.PLAN_MARKDOWN);
						listofMonthWiseData = mapOfAssortmentWiseMetricDataCopy.get(OtbMsg.PLAN_MARKDOWN);
						mapofMonthData.put(billMonth, Double.toString(a.getMARK_DOWN()));
						listofMonthWiseData.add(mapofMonthData);
						mapOfMetrics.put(OtbMsg.PLAN_MARKDOWN, listofMonthWiseData);

						mapofMonthData = new LinkedHashMap<>();
						listofMonthWiseData = new LinkedList<>();
						mapOfMetrics.remove(OtbMsg.PLAN_CLOSING_INVENTORY);
						listofMonthWiseData = mapOfAssortmentWiseMetricDataCopy.get(OtbMsg.PLAN_CLOSING_INVENTORY);
						mapofMonthData.put(billMonth, Double.toString(a.getCLOSING_INVENTORY()));
						listofMonthWiseData.add(mapofMonthData);
						mapOfMetrics.put(OtbMsg.PLAN_CLOSING_INVENTORY, listofMonthWiseData);

						mapofMonthData = new LinkedHashMap<>();
						listofMonthWiseData = new LinkedList<>();
						mapOfMetrics.remove(OtbMsg.PLAN_OPENING_STOCK);
						listofMonthWiseData = mapOfAssortmentWiseMetricDataCopy.get(OtbMsg.PLAN_OPENING_STOCK);
						mapofMonthData.put(billMonth, Double.toString(a.getOPENING_STOCK()));
						listofMonthWiseData.add(mapofMonthData);
						mapOfMetrics.put(OtbMsg.PLAN_OPENING_STOCK, listofMonthWiseData);

						mapofMonthData = new LinkedHashMap<>();
						listofMonthWiseData = new LinkedList<>();
						mapOfMetrics.remove(OtbMsg.PLAN_PURCHASE_ORDER);
						listofMonthWiseData = mapOfAssortmentWiseMetricDataCopy.get(OtbMsg.PLAN_PURCHASE_ORDER);
						mapofMonthData.put(billMonth, Double.toString(a.getOPEN_PURCHASE_ORDER()));
						listofMonthWiseData.add(mapofMonthData);
						mapOfMetrics.put(OtbMsg.PLAN_PURCHASE_ORDER, listofMonthWiseData);

						mapofMonthData = new LinkedHashMap<>();
						listofMonthWiseData = new LinkedList<>();
						mapOfMetrics.remove(OtbMsg.PLAN_OTB_VALUE);
						listofMonthWiseData = mapOfAssortmentWiseMetricDataCopy.get(OtbMsg.PLAN_OTB_VALUE);
						mapofMonthData.put(billMonth, Double.toString(a.getOTB_VALUE()));
						listofMonthWiseData.add(mapofMonthData);
						mapOfMetrics.put(OtbMsg.PLAN_OTB_VALUE, listofMonthWiseData);

					}

					else {
						listofMonthWiseData = new LinkedList<>();
						mapofMonthData.put(billMonth, Double.toString(a.getPLAN_SALES()));
						listofMonthWiseData.add(mapofMonthData);
						mapOfMetrics.put(OtbMsg.PLAN_SALES, listofMonthWiseData);

						mapofMonthData = new LinkedHashMap<>();
						listofMonthWiseData = new LinkedList<>();
						mapofMonthData.put(billMonth, Double.toString(a.getMARK_DOWN()));
						listofMonthWiseData.add(mapofMonthData);
						mapOfMetrics.put(OtbMsg.PLAN_MARKDOWN, listofMonthWiseData);

						mapofMonthData = new LinkedHashMap<>();
						listofMonthWiseData = new LinkedList<>();
						mapofMonthData.put(billMonth, Double.toString(a.getCLOSING_INVENTORY()));
						listofMonthWiseData.add(mapofMonthData);
						mapOfMetrics.put(OtbMsg.PLAN_CLOSING_INVENTORY, listofMonthWiseData);

						mapofMonthData = new LinkedHashMap<>();
						listofMonthWiseData = new LinkedList<>();
						mapofMonthData.put(billMonth, Double.toString(a.getOPENING_STOCK()));
						listofMonthWiseData.add(mapofMonthData);
						mapOfMetrics.put(OtbMsg.PLAN_OPENING_STOCK, listofMonthWiseData);

						mapofMonthData = new LinkedHashMap<>();
						listofMonthWiseData = new LinkedList<>();
						mapofMonthData.put(billMonth, Double.toString(a.getOPEN_PURCHASE_ORDER()));
						listofMonthWiseData.add(mapofMonthData);
						mapOfMetrics.put(OtbMsg.PLAN_PURCHASE_ORDER, listofMonthWiseData);

						mapofMonthData = new LinkedHashMap<>();
						listofMonthWiseData = new LinkedList<>();
						mapofMonthData.put(billMonth, Double.toString(a.getOTB_VALUE()));
						listofMonthWiseData.add(mapofMonthData);
						mapOfMetrics.put(OtbMsg.PLAN_OTB_VALUE, listofMonthWiseData);

					}
					listQuarterValue.add(billMonth);
					exit = false;
					// Create one row item with metrics wise data for year .
					mapOfAssortmentWiseMetricData.put(assortment, mapOfMetrics);
					if (distinctAssortmentWise.add(assortment)) {
						if(assortment.equalsIgnoreCase("ACCESSORIES-INFANTS-BABY CAP-0-99999")) {
							List<String> av=new ArrayList<>(listQuarterValue);
							List<String> misMatch=CodeUtils.returnMisMatchList(totalQtr, av);
//							for(int i=0;i<misMatch.size();i++) {
//								listofMonthWiseData = new LinkedList<>();
//								mapofMonthData.put(misMatch.get(i), "0.0");
//								listofMonthWiseData.add(mapofMonthData);
//								mapOfMetrics.put(OtbMsg.PLAN_SALES, listofMonthWiseData);
//
//								mapofMonthData = new LinkedHashMap<>();
//								listofMonthWiseData = new LinkedList<>();
//								mapofMonthData.put(misMatch.get(i), "0.0");
//								listofMonthWiseData.add(mapofMonthData);
//								mapOfMetrics.put(OtbMsg.PLAN_MARKDOWN, listofMonthWiseData);
//
//								mapofMonthData = new LinkedHashMap<>();
//								listofMonthWiseData = new LinkedList<>();
//								mapofMonthData.put(misMatch.get(i), "0.0");
//								listofMonthWiseData.add(mapofMonthData);
//								mapOfMetrics.put(OtbMsg.PLAN_CLOSING_INVENTORY, listofMonthWiseData);
//
//								mapofMonthData = new LinkedHashMap<>();
//								listofMonthWiseData = new LinkedList<>();
//								mapofMonthData.put(misMatch.get(i), "0.0");
//								listofMonthWiseData.add(mapofMonthData);
//								mapOfMetrics.put(OtbMsg.PLAN_OPENING_STOCK, listofMonthWiseData);
//
//								mapofMonthData = new LinkedHashMap<>();
//								listofMonthWiseData = new LinkedList<>();
//								mapofMonthData.put(misMatch.get(i), "0.0");
//								listofMonthWiseData.add(mapofMonthData);
//								mapOfMetrics.put(OtbMsg.PLAN_PURCHASE_ORDER, listofMonthWiseData);
//
//								mapofMonthData = new LinkedHashMap<>();
//								listofMonthWiseData = new LinkedList<>();
//								mapofMonthData.put(misMatch.get(i), "0.0");
//								listofMonthWiseData.add(mapofMonthData);
//								mapOfMetrics.put(OtbMsg.PLAN_OTB_VALUE, listofMonthWiseData);
//
//							}
						}
//						mapOfAssortmentWiseMetricData.put(assortment, mapOfMetrics);
						assortmentWiseMetricsData.add(mapOfAssortmentWiseMetricData);
						listQuarterValue.clear();
					}
				});

				isInserted = false;

				// Code for Calculate Total OTB for Demand Forecast Data
				
				for (DownloadOTBPlanDFD dfdPlan : dfdData) {
					String billDate = dfdPlan.getBILLDATE();
					if (distinctBillDate.add(billDate)) {
						OTBGraph otbGraph = new OTBGraph();
						for (int i = 0; i < dfdData.size(); i++) {
							if (billDate.equalsIgnoreCase(dfdData.get(i).getBILLDATE())) {
								planSales =planSales+ dfdData.get(i).getPLAN_SALES();
								closingInventory =closingInventory+ dfdData.get(i).getCLOSING_INVENTORY();
								markDown =markDown+ dfdData.get(i).getMARK_DOWN();
								openingStock = openingStock+dfdData.get(i).getOPENING_STOCK();
								openPurchaseOrder =openPurchaseOrder+ dfdData.get(i).getOPEN_PURCHASE_ORDER();
								otbValue += dfdData.get(i).getOTB_VALUE();
							}
						}
						otbGraph.setBillDate(billDate);
						otbGraph.setAssortmentCode("Total");
						otbGraph.setPlanSales(Math.round(planSales * 100.00) / 100.00);
						otbGraph.setClosingInventory(Math.round(closingInventory * 100.00) / 100.00);
						otbGraph.setMarkDown(Math.round(markDown * 100.00) / 100.00);
						otbGraph.setOpeningStock(Math.round(openingStock * 100.00) / 100.00);
						otbGraph.setOpenPurchaseOrder(Math.round(openPurchaseOrder * 100.00) / 100.00);
						otbGraph.setOtbValue(Math.round(otbValue * 100.00) / 100.00);
						finalGraph.add(otbGraph);
						planSales=0L;
						closingInventory=0L;
						markDown=0L;
						openingStock=0L;
						openPurchaseOrder=0L;
						otbValue=0L;
					}
				}

				List<Map<String, Map<String, List<Map<String, String>>>>> totalAssortmentWiseMetricsData = new LinkedList<>();
				Set dinstinctTotalAssortmentWise = new HashSet<>();
				finalGraph.stream().forEach(e -> {
					String billDate = e.getBillDate();
					Map<String, Map<String, List<Map<String, String>>>> mapOfTotalAssortmentWiseMetricData = new LinkedHashMap<>();
					Map<String, List<Map<String, String>>> totalAssortment = new LinkedHashMap<>();
					Map<String, String> billDateWise = new LinkedHashMap<>();
					List<Map<String, String>> finalData = new LinkedList<>();
					assortment = e.getAssortmentCode();

					totalAssortmentWiseMetricsData.forEach(e1 -> {
						if (e1.containsKey(assortment)) {
							isInserted = true;
							exit = true;
							mapOfAssortmentWiseMetricDataCopy = e1.get(assortment);
						} else {
							if (!exit)
								isInserted = false;
						}
					});

					if (isInserted) {

						billDateWise = new LinkedHashMap<>();
						finalData = new LinkedList<>();
						totalAssortment.remove(OtbMsg.PLAN_SALES);
						finalData = mapOfAssortmentWiseMetricDataCopy.get(OtbMsg.PLAN_SALES);
						billDateWise.put(billDate, Double.toString(e.getPlanSales()));
						finalData.add(billDateWise);
						totalAssortment.put(OtbMsg.PLAN_SALES, finalData);

						billDateWise = new LinkedHashMap<>();
						finalData = new LinkedList<>();
						totalAssortment.remove(OtbMsg.PLAN_MARKDOWN);
						finalData = mapOfAssortmentWiseMetricDataCopy.get(OtbMsg.PLAN_MARKDOWN);
						billDateWise.put(billDate, Double.toString(e.getMarkDown()));
						finalData.add(billDateWise);
						totalAssortment.put(OtbMsg.PLAN_MARKDOWN, finalData);

						billDateWise = new LinkedHashMap<>();
						finalData = new LinkedList<>();
						totalAssortment.remove(OtbMsg.PLAN_CLOSING_INVENTORY);
						finalData = mapOfAssortmentWiseMetricDataCopy.get(OtbMsg.PLAN_CLOSING_INVENTORY);
						billDateWise.put(billDate, Double.toString(e.getClosingInventory()));
						finalData.add(billDateWise);
						totalAssortment.put(OtbMsg.PLAN_CLOSING_INVENTORY, finalData);

						billDateWise = new LinkedHashMap<>();
						finalData = new LinkedList<>();
						totalAssortment.remove(OtbMsg.PLAN_OPENING_STOCK);
						finalData = mapOfAssortmentWiseMetricDataCopy.get(OtbMsg.PLAN_OPENING_STOCK);
						billDateWise.put(billDate, Double.toString(e.getOpeningStock()));
						finalData.add(billDateWise);
						totalAssortment.put(OtbMsg.PLAN_OPENING_STOCK, finalData);

						billDateWise = new LinkedHashMap<>();
						finalData = new LinkedList<>();
						totalAssortment.remove(OtbMsg.PLAN_PURCHASE_ORDER);
						finalData = mapOfAssortmentWiseMetricDataCopy.get(OtbMsg.PLAN_PURCHASE_ORDER);
						billDateWise.put(billDate, Double.toString(e.getOpenPurchaseOrder()));
						finalData.add(billDateWise);
						totalAssortment.put(OtbMsg.PLAN_PURCHASE_ORDER, finalData);

						billDateWise = new LinkedHashMap<>();
						finalData = new LinkedList<>();
						totalAssortment.remove(OtbMsg.PLAN_OTB_VALUE);
						finalData = mapOfAssortmentWiseMetricDataCopy.get(OtbMsg.PLAN_OTB_VALUE);
						billDateWise.put(billDate, Double.toString(e.getOtbValue()));
						finalData.add(billDateWise);
						totalAssortment.put(OtbMsg.PLAN_OTB_VALUE, finalData);
					} else {
						billDateWise = new LinkedHashMap<>();
						finalData = new LinkedList<>();
						billDateWise.put(billDate, Double.toString(e.getPlanSales()));
						finalData.add(billDateWise);
						totalAssortment.put(OtbMsg.PLAN_SALES, finalData);

						billDateWise = new LinkedHashMap<>();
						finalData = new LinkedList<>();
						billDateWise.put(billDate, Double.toString(e.getMarkDown()));
						finalData.add(billDateWise);
						totalAssortment.put(OtbMsg.PLAN_MARKDOWN, finalData);

						billDateWise = new LinkedHashMap<>();
						finalData = new LinkedList<>();
						billDateWise.put(billDate, Double.toString(e.getClosingInventory()));
						finalData.add(billDateWise);
						totalAssortment.put(OtbMsg.PLAN_CLOSING_INVENTORY, finalData);

						billDateWise = new LinkedHashMap<>();
						finalData = new LinkedList<>();
						billDateWise.put(billDate, Double.toString(e.getOpeningStock()));
						finalData.add(billDateWise);
						totalAssortment.put(OtbMsg.PLAN_OPENING_STOCK, finalData);

						billDateWise = new LinkedHashMap<>();
						finalData = new LinkedList<>();
						billDateWise.put(billDate, Double.toString(e.getOpenPurchaseOrder()));
						finalData.add(billDateWise);
						totalAssortment.put(OtbMsg.PLAN_PURCHASE_ORDER, finalData);

						billDateWise = new LinkedHashMap<>();
						finalData = new LinkedList<>();
						billDateWise.put(billDate, Double.toString(e.getOtbValue()));
						finalData.add(billDateWise);
						totalAssortment.put(OtbMsg.PLAN_OTB_VALUE, finalData);

					}
					exit = false;
					mapOfTotalAssortmentWiseMetricData.put(assortment, totalAssortment);
//					if(dinstinctTotalAssortmentWise.add(assortment))
					if (!isInserted)
						totalAssortmentWiseMetricsData.add(mapOfTotalAssortmentWiseMetricData);

				});

				LOGGER.debug(String.format("Total Assortment %s", totalAssortmentWiseMetricsData));
				exportListTOExcel(assortmentWiseMetricsData, totalAssortmentWiseMetricsData, filePath, startDate,
						endDate);

			}
			file = new File(filePath);
			etag = s3Wrapper.upload(file, s3BucketFilePath + CustomParameter.DELIMETER + fileName + FileUtils.EXCELExtension,
					s3BucketInfo.getS3bucketName()).getETag();
			if (!CodeUtils.isEmpty(etag)) {
				url = s3Wrapper.downloadPreSignedURLOnHistory(s3BucketInfo.getS3bucketName(),
						s3BucketFilePath + CustomParameter.DELIMETER + fileName + FileUtils.EXCELExtension,
						Integer.parseInt(s3BucketInfo.getTime()));
			}
		} catch (CannotGetJdbcConnectionException ex) {
			throw ex;
		} catch (DataAccessException ex) {
			throw ex;
		} catch (Exception ex) {
			throw ex;
		}
		return url;
	}

	/**
	 * method to update plan sales and percentage
	 */

	@SuppressWarnings("deprecation")
	@Override
	public int modificationDetails(JsonNode jsonNode) {
		Integer updateData = 0;

		double fullPercentage = 0;
		float percentage = 0;
		
		OTBPlanDFD otbPlanDFD = new OTBPlanDFD();
		OTBPlanASD otbPlanASD = new OTBPlanASD();
		String planId = jsonNode.get("planId").asText();
		double newPlanSales = jsonNode.get("newPlanSales").asDouble();
		double markDown = jsonNode.get("markDown").asDouble();
		double closingInventory = jsonNode.get("closingInventory").asDouble();
		double openingStock = jsonNode.get("openingStock").asDouble();
		double openPurchaseOrder = jsonNode.get("openPurchaseOrder").asDouble();
		double otbValue = jsonNode.get("otbValue").asDouble();
		String billDate=jsonNode.get("billDate").asText();
		String assortmentCode = jsonNode.get("assortmentCode").asText();
		String activePlan = jsonNode.get("activePlan").asText();
		String isUserCreatePlan = jsonNode.get("isUserCreatePlan").asText();
		String tenantHashkey=ThreadLocalStorage.getTenantHashKey();
		if (isUserCreatePlan.equalsIgnoreCase("TRUE")) {
			if (activePlan.equalsIgnoreCase("ASD")) {
				// this tenant is set for specific style baazar query
				if (!tenantHashkey.equalsIgnoreCase(TenantHash.valueOf("STYLE_BAAZAR").getHashKey())) {
					billDate = CodeUtils.convertOTBMonthlyFormat(billDate);
				}else {
					billDate=Quarter.valueOf(billDate).getQtr();
				}
				OTBPlanASD data = otbMapper.getUserPlanDetailASD(billDate, assortmentCode, planId);

				if (!CodeUtils.isEmpty(data)) {
					double planSales = data.getPlanSales();
					fullPercentage = 100 - (planSales * 100 / newPlanSales);
					percentage = Math.round(fullPercentage);
					// updateData=otbMapper.updateUserDataASD(percentage,billDate,assortmentCode,Double.parseDouble(newPlanSales),planId);

					otbPlanASD.setAssortmentCode(assortmentCode);
					otbPlanASD.setModifiedPercentage(percentage);
					otbPlanASD.setPlanId(planId);
					otbPlanASD.setBillDate(billDate);
					otbPlanASD.setModifiedPercentage(percentage);
					otbPlanASD.setPlanSales(newPlanSales);
					otbPlanASD.setMarkDown(markDown);
					otbPlanASD.setClosingInventory(closingInventory);
					otbPlanASD.setOpeningStock(openingStock);
					otbPlanASD.setOpenPurchaseOrder(openPurchaseOrder);
					otbValue = otbPlanASD.getPlanSales() + otbPlanASD.getMarkDown()
							+ otbPlanASD.getClosingInventory()
							- (otbPlanASD.getOpeningStock() + otbPlanASD.getOpenPurchaseOrder());
					otbPlanASD.setOtbValue(otbValue);
					updateData = otbMapper.updateByPlanSaleASD(otbPlanASD);
				}
			} else if (activePlan.equalsIgnoreCase("DFD")) {
				// this tenant is set for specific style baazar query
				if (!tenantHashkey.equalsIgnoreCase(TenantHash.valueOf("STYLE_BAAZAR").getHashKey())) {
					billDate = CodeUtils.convertOTBMonthlyFormat(billDate);
				}else {
					billDate=Quarter.valueOf(billDate).getQtr();
				}
				OTBPlanDFD data = otbMapper.getUserPlanDetailDFD(billDate, assortmentCode, planId);
				if (!CodeUtils.isEmpty(data)) {
					double planSales = data.getPlanSales();
					fullPercentage = 100 - (planSales * 100 / newPlanSales);
					percentage = Math.round(fullPercentage);
					otbPlanDFD.setAssortmentCode(assortmentCode);
					otbPlanDFD.setPlanId(planId);
					otbPlanDFD.setModifiedPercentage(percentage);
					otbPlanDFD.setBillDate(billDate);
					otbPlanDFD.setModifiedPercentage(percentage);
					otbPlanDFD.setPlanSales(newPlanSales);
					otbPlanDFD.setMarkDown(markDown);
					otbPlanDFD.setClosingInventory(closingInventory);
					otbPlanDFD.setOpeningStock(openingStock);
					otbPlanDFD.setOpenPurchaseOrder(openPurchaseOrder);
					otbValue = otbPlanDFD.getPlanSales() + otbPlanDFD.getMarkDown()
							+ otbPlanDFD.getClosingInventory()
							- (otbPlanDFD.getOpeningStock() + otbPlanDFD.getOpenPurchaseOrder());
					otbPlanDFD.setOtbValue(otbValue);
					updateData = otbMapper.updateByPlanSaleDFD(otbPlanDFD);
				}
			}
		} else if (isUserCreatePlan.equalsIgnoreCase("FALSE")) {
			if (activePlan.equalsIgnoreCase("ASD")) {
				// this tenant is set for specific style baazar query
				if (!tenantHashkey.equalsIgnoreCase(TenantHash.valueOf("STYLE_BAAZAR").getHashKey())) {
					billDate = CodeUtils.convertOTBMonthlyFormat(billDate);
				}else {
					billDate=Quarter.valueOf(billDate).getQtr();
				}
				OTBPlanASD data = otbMapper.getPlanDetailASD(billDate, assortmentCode, planId);

				double planSales = data.getPlanSales();
				fullPercentage = 100 - ((planSales / newPlanSales) * 100);
				percentage = Math.round(fullPercentage * 100) / (float) 100;
				otbPlanASD.setPlanId(planId);
				otbPlanASD.setBillDate(billDate);
				otbPlanASD.setModifiedPercentage(percentage);
				otbPlanASD.setPlanSales(newPlanSales);
				otbPlanASD.setMarkDown(markDown);
				otbPlanASD.setClosingInventory(closingInventory);
				otbPlanASD.setOpeningStock(openingStock);
				otbPlanASD.setOpenPurchaseOrder(openPurchaseOrder);
				otbValue = otbPlanASD.getPlanSales() + otbPlanASD.getMarkDown()
						+ otbPlanASD.getClosingInventory()
						- (otbPlanASD.getOpeningStock() + otbPlanASD.getOpenPurchaseOrder());
				otbPlanASD.setOtbValue(otbValue);
			} else if (activePlan.equalsIgnoreCase("DFD")) {
				// this tenant is set for specific style baazar query
				if (!tenantHashkey.equalsIgnoreCase(TenantHash.valueOf("STYLE_BAAZAR").getHashKey())) {
					billDate = CodeUtils.convertOTBMonthlyFormat(billDate);
				}else {
					billDate=Quarter.valueOf(billDate).getQtr();
				}
				OTBPlanDFD data = otbMapper.getPlanDetailDFD(billDate, assortmentCode, planId);
				double planSales = data.getPlanSales();
				fullPercentage = 100 - ((planSales / newPlanSales) * 100);
				percentage = Math.round(fullPercentage * 100) / (float) 100;
				otbPlanASD.setPlanId(planId);
				otbPlanASD.setBillDate(billDate);
				otbPlanASD.setModifiedPercentage(percentage);
				otbPlanASD.setPlanSales(newPlanSales);
				otbPlanASD.setMarkDown(markDown);
				otbPlanASD.setClosingInventory(closingInventory);
				otbPlanASD.setOpeningStock(openingStock);
				otbPlanASD.setOpenPurchaseOrder(openPurchaseOrder);
				otbValue = otbPlanASD.getPlanSales() + otbPlanASD.getMarkDown()
						+ otbPlanASD.getClosingInventory()
						- (otbPlanASD.getOpeningStock() + otbPlanASD.getOpenPurchaseOrder());
				otbPlanASD.setOtbValue(otbValue);
//				updateData=otbMapper.updateDataDFD(percentage,billDate,assortmentCode,Double.parseDouble(newPlanSales),planId);
			}
		}

		return updateData;
	}

//	public List allMonths(String sDate, String eDate) {
//		LocalDate date1 = new LocalDate(sDate);
//		LocalDate date2 = new LocalDate(eDate);
//		List l = new LinkedList();
//		while (date1.isBefore(date2.plusMonths(1))) {
//			l.add(date1.toString("yyyy-MM"));
//			date1 = date1.plus(Period.months(1));
//		}
//		return l;
//	}

	@Override
	public OTBPlanASD getByPlanIdAndAssortmentcodeASD(String planId, String assortmentCode, String billDate) {
		return otbMapper.getByPlanIdAndAssortmentcodeASD(planId, assortmentCode, billDate);
	}

	@Override
	public OTBPlanDFD getByPlanIdAndAssortmentcodeDFD(String planId, String assortmentCode, String billDate) {
		return otbMapper.getByPlanIdAndAssortmentcodeDFD(planId, assortmentCode, billDate);
	}

	@SuppressWarnings({ "rawtypes", "resource" })
	public void exportListTOExcel(List<Map<String, Map<String, List<Map<String, String>>>>> assortmentWiseMetricsData,
			List<Map<String, Map<String, List<Map<String, String>>>>> totalAsortmentWiseMetricsData, String filePath,
			String startDate, String endDate) {
		
		String tenantHashkey=ThreadLocalStorage.getTenantHashKey();
		List<String> dateList=null;
		// this tenant is set for specific style baazar query
		if (!tenantHashkey.equalsIgnoreCase(TenantHash.valueOf("STYLE_BAAZAR").getHashKey())) {
			dateList = CodeUtils.getAllMonths(startDate, endDate);
		}else {
			dateList=new ArrayList<>();
			dateList.add("1");
			dateList.add("2");
			dateList.add("3");
			dateList.add("4");
		}
		Set distinctBillDate = new HashSet<>();
		Set distinctBillDateAssortmentWise = new HashSet<>();

		Workbook workbook = new XSSFWorkbook();
		Sheet sheet = workbook.createSheet("OTB_Report");

		int rowIndex = 1;
		Row row1 = sheet.createRow(0);
		row1.createCell(0).setCellValue("Name");
		row1.createCell(1).setCellValue("Metric");
		for (int i = 2; i < dateList.size() + 2; i++) {
			if (tenantHashkey.equalsIgnoreCase(TenantHash.valueOf("STYLE_BAAZAR").getHashKey())) {
				String headerValue=dateList.get(i - 2).toString().equals("1")?"Q1":dateList.get(i - 2).toString().equals("2")?"Q2":
					dateList.get(i - 2).toString().equals("3")?"Q3":"Q4";
				row1.createCell(i).setCellValue(headerValue);
			}else {
				row1.createCell(i).setCellValue(dateList.get(i - 2).toString());
			}
		}
		// Code for total Assortment to write in the excel
		LOGGER.info("start Writing to excel for total OTB...");

		for (int i = 0; i < totalAsortmentWiseMetricsData.size(); i++) {
			Map<String, Map<String, List<Map<String, String>>>> allAssortmentCode = totalAsortmentWiseMetricsData
					.get(i);
			Set keys = allAssortmentCode.keySet();
			Iterator itr = keys.iterator();
			String key11;
			Map<String, List<Map<String, String>>> value11;
			String keyvalue11;
			List<Map<String, String>> valueOf11;
			Row row = sheet.createRow(rowIndex++);
			int cellIndex = 0;

			while (itr.hasNext()) {
				cellIndex = 0;
				key11 = (String) itr.next();
				value11 = allAssortmentCode.get(key11);
				Set valuekeys11 = value11.keySet();
				Iterator valueitr = valuekeys11.iterator();
				while (valueitr.hasNext()) {
					cellIndex = 0;
					keyvalue11 = (String) valueitr.next();
					valueOf11 = value11.get(keyvalue11);
					row.createCell(cellIndex++).setCellValue(key11);
					row.createCell(cellIndex++).setCellValue(keyvalue11);

					for (Map<String, String> value : valueOf11) {
						Set<String> keysv = value.keySet();
						String key = keysv.iterator().next();
						String newValue = value.get(key);
						String mapValue=null;
						// this tenant is set for specific style baazar query
						if (!tenantHashkey.equalsIgnoreCase(TenantHash.valueOf("STYLE_BAAZAR").getHashKey())) {
							mapValue=CodeUtils.convertOTBMonthlyDateFormat(key);
						}else {
							mapValue=key;
						}
						if (!dateList.contains(mapValue)) {
							row.createCell(cellIndex++).setCellValue("0.0");
						} else {
							if (CodeUtils.checkStringType(newValue).equals("Float")) {
								double cellValue = Double.parseDouble(newValue);
								String convertExponetialToPlain = BigDecimal.valueOf(cellValue).toPlainString();
								if (distinctBillDate.add(key + keyvalue11))
									row.createCell(cellIndex++).setCellValue(convertExponetialToPlain);
							} else {
								if (distinctBillDate.add(key + keyvalue11))
									row.createCell(cellIndex++).setCellValue(newValue);
							}
						}
					}
					row = sheet.createRow(rowIndex++);
				}
				row = sheet.createRow(rowIndex++);
			}
		}

		LOGGER.info("completed Writing to excel for total OTB...");

		// Code for Assortment Wise to write in the excel

		LOGGER.info("start Writing to excel for assortment wise OTB...");

		for (int i = 0; i < assortmentWiseMetricsData.size(); i++) {
			Map<String, Map<String, List<Map<String, String>>>> allAssortmentCode = assortmentWiseMetricsData.get(i);
			Set keys = allAssortmentCode.keySet();
//			LOGGER.info(keys+" : ");
			Iterator itr = keys.iterator();
			String key11;
			Map<String, List<Map<String, String>>> value11;
			String keyvalue11;
			List<Map<String, String>> valueOf11;
			Row row = sheet.createRow(rowIndex++);
			int cellIndex = 0;

			while (itr.hasNext()) {
				cellIndex = 0;
				key11 = (String) itr.next();
				value11 = allAssortmentCode.get(key11);
				Set valuekeys11 = value11.keySet();
				Iterator valueitr = valuekeys11.iterator();
				while (valueitr.hasNext()) {
					cellIndex = 0;
					keyvalue11 = (String) valueitr.next();
					valueOf11 = value11.get(keyvalue11);
					row.createCell(cellIndex++).setCellValue(key11);
					row.createCell(cellIndex++).setCellValue(keyvalue11);

					for (Map<String, String> value : valueOf11) {
						Set<String> keysv = value.keySet();
						String key = keysv.iterator().next();
						String newValue = value.get(key);

						String mapValue = null;
						// this tenant is set for specific style baazar query
						if (!tenantHashkey.equalsIgnoreCase(TenantHash.valueOf("STYLE_BAAZAR").getHashKey())) {
							mapValue = CodeUtils.convertOTBMonthlyDateFormat(key);
						} else {
							mapValue = key;
							cellIndex = Integer.parseInt(mapValue) + 1;
						}
						if (!dateList.contains(mapValue)) {
							row.createCell(cellIndex++).setCellValue("0.0");
						} else {
							if (!tenantHashkey.equalsIgnoreCase(TenantHash.valueOf("STYLE_BAAZAR").getHashKey())) {
								if (CodeUtils.checkStringType(newValue).equals("Float")) {
									double cellValue = Double.parseDouble((newValue));
									String convertExponetialToPlain = BigDecimal.valueOf(cellValue).toPlainString();
									if (distinctBillDateAssortmentWise.add(key + keyvalue11 + key11))
										row.createCell(cellIndex++).setCellValue(convertExponetialToPlain);
								} else {
									if (distinctBillDateAssortmentWise.add(key + keyvalue11 + key11))
										row.createCell(cellIndex++).setCellValue(newValue);
								}
							} else {
								if (CodeUtils.checkStringType(newValue).equals("Float")) {
									double cellValue = Double.parseDouble((newValue));
									String convertExponetialToPlain = BigDecimal.valueOf(cellValue).toPlainString();
									if (distinctBillDateAssortmentWise.add(key + keyvalue11 + key11))
										row.createCell(cellIndex).setCellValue(convertExponetialToPlain);
								} else {
									if (distinctBillDateAssortmentWise.add(key + keyvalue11 + key11))
										row.createCell(cellIndex).setCellValue(newValue);
								}
							}
						}
					}
					row = sheet.createRow(rowIndex++);
				}
				row = sheet.createRow(rowIndex++);
			}
		}
		LOGGER.info("completed Writing to excel for assortment wise OTB...");
		try {
			Path path = Paths.get(filePath);
			try (OutputStream out = Files.newOutputStream(path)) {
				workbook.write(out);
				out.close();
			} catch (FileAlreadyExistsException incrCounterAndRetry) {
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
//		try {
//
//			FileOutputStream fos = new FileOutputStream(filePath);
//
//			workbook.write(fos);
//
//			fos.close();
//
//		} catch (FileNotFoundException e) {
//
//			e.printStackTrace();
//
//		} catch (IOException e) {
//
//			e.printStackTrace();
//
//		}

	}

	@Override
	public String getOTBCreateStatus() throws SupplyMintException, InterruptedException {
		String status = null;
		try {
			status = otbMapper.getOTBCreateStatus();
		} catch (CannotGetJdbcConnectionException ex) {
			throw ex;
		} catch (DataAccessException ex) {
			throw ex;
		} catch (Exception ex) {
			throw ex;
		}

		return status;

	}

	@Override
	public String getConfig(String key) {
		return otbMapper.getConfig(key);
	}
	
	private int createActualDataForStyleBaazar(OpenToBuy openToBuy) {

		List<OTBPlanASD> data = new ArrayList<OTBPlanASD>();
		int insertAllData = 0;
		Calendar startCalender = Calendar.getInstance();
		Calendar endCalender = Calendar.getInstance();
		endCalender.add(Calendar.MONTH, -1);

		String endDate = CodeUtils.__dateFormat.format(endCalender.getTime());
		Date endQuarter=FiscalDate.getLastDayOfQuarter(DateUtils.stringToDate(endDate));
		endDate=CodeUtils.__dateFormat.format(endQuarter);
		startCalender.add(Calendar.YEAR, -1); // to get previous year add -1
		startCalender.add(Calendar.MONTH, 1);
		String startDate = CodeUtils.__dateFormat.format(startCalender.getTime());
		Date currentQuarter=FiscalDate.getFirstDayOfQuarter(DateUtils.stringToDate(startDate));
		startDate=CodeUtils.__dateFormat.format(currentQuarter);

		List<DemandForecast> listDemandForecast = otbMapper.getAllCustomQuarterUserASDPlan(startDate, endDate);

		LOGGER.info("manipulating data for otb actual monthly");
		for (DemandForecast demandForecast : listDemandForecast) {
			OTBPlanASD otbPlanASD = new OTBPlanASD();
			otbPlanASD.setPlanId(openToBuy.getPlanId());
			otbPlanASD.setAssortmentCode(demandForecast.getAssortmentCode());
			otbPlanASD.setBillDate(demandForecast.getQuarter());
			otbPlanASD.setFutureBillDate(demandForecast.getQuarter());
//			otbPlanASD.setFutureBillDate(CodeUtils.__dateFormat.format(CodeUtils
//					.addYear(new Date(CodeUtils.convertDataReferenceDateFormat(otbPlanASD.getBillDate())), 1)));

			otbPlanASD.setQty(demandForecast.getQty());
			otbPlanASD.setCostPrice(demandForecast.getCostPrice());
			otbPlanASD.setSellValue(demandForecast.getSellValue());
			otbPlanASD.setIsModified("N");
			otbPlanASD.setModifiedPercentage(0);
//			float sales = demandForecast.getCostPrice() * Float.parseFloat(otbPlanASD.getQtyForecast());
			double planSale = demandForecast.getSellValue() + demandForecast.getSellValue()
					* (Float.parseFloat(openToBuy.getIncreasePercentageAsd()) / (double) 100);
			otbPlanASD.setPlanSales(Math.round(planSale * 100.0) / 100.0);
			otbPlanASD.setMarkDown(0);
			otbPlanASD.setClosingInventory(demandForecast.getClosingInventory());
			otbPlanASD.setOpeningStock(demandForecast.getOpeningStock());
			otbPlanASD.setOpenPurchaseOrder(demandForecast.getOpenPurchaseOrder());
			double otbValue = otbPlanASD.getPlanSales() + otbPlanASD.getMarkDown() + otbPlanASD.getClosingInventory()
					- (otbPlanASD.getOpeningStock() + otbPlanASD.getOpenPurchaseOrder());
			otbPlanASD.setOtbValue(Math.round(otbValue * 100.0) / 100.0);
			otbPlanASD.setStatus(Conditions.TRUE.toString());
			otbPlanASD.setActive('1');
			data.add(otbPlanASD);
		}

		int size = 1000;
		LOGGER.info("inserting data from otb actual monthly with size of 1000 at a time");
		for (int start = 0; start < data.size(); start += size) {
			int end = Math.min(start + size, data.size());
			List<OTBPlanASD> sublist = data.subList(start, end);

			if (openToBuy.getIsUserOTBPlan().equalsIgnoreCase(Conditions.TRUE.toString())) {
				insertAllData = otbMapper.insertUserASD(sublist);
			}
		}
		LOGGER.info("inserting data from otb actual monthly completed...");
		int updateOTBPlanStatus = otbMapper.updateOTBPlanStatus(openToBuy.getPlanId(),"SUCCEEDED");
		LOGGER.info("update otb status inprogress to success...");

		return insertAllData;
	}
	
	private int createForecastDataForStyleBaazar(OpenToBuy openToBuy) {
		List<OTBPlanDFD> data = null;
		int result=0;
		Calendar cal = Calendar.getInstance();
		String startDate = CodeUtils.__dateFormat.format(cal.getTime());
		Date currentQuarter=FiscalDate.getFirstDayOfQuarter(DateUtils.stringToDate(startDate));
		startDate=CodeUtils.__dateFormat.format(currentQuarter);
		
		cal.add(Calendar.YEAR, 1); // to get previous year add -1
		cal.add(Calendar.MONTH, -1);
		String endDate = CodeUtils.__dateFormat.format(cal.getTime());
		Date endQuarter=FiscalDate.getFirstDayOfQuarter(DateUtils.stringToDate(endDate));
		endDate=CodeUtils.__dateFormat.format(endQuarter);
		data = otbMapper.getAllCustomQuarterUserDFD(startDate, endDate);
		
		for (OTBPlanDFD dfd : data) {
			dfd.setPlanId(openToBuy.getPlanId());
			dfd.setIsModified("N");
			dfd.setModifiedPercentage(0);
			dfd.setBillDate(dfd.getQuarter());
			double planSale = dfd.getSellValue()
					+ (dfd.getSellValue() * Double.parseDouble(openToBuy.getIncreasePercentageDfd()))
							/ (double) 100;
			dfd.setPlanSales(Math.round(planSale * 100.0) / 100.0);

			dfd.setMarkDown(0);
			dfd.setClosingInventory(Math.round(dfd.getClosingInventory() * 100.0) / 100.0);
			dfd.setOpeningStock(Math.round(dfd.getOpeningStock() * 100.0) / 100.0);
			dfd.setOpenPurchaseOrder(Math.round(dfd.getOpenPurchaseOrder() * 100.0) / 100.0);
			double otbValue = dfd.getPlanSales() + dfd.getMarkDown() + dfd.getClosingInventory()
					- (dfd.getOpeningStock() + dfd.getOpenPurchaseOrder());
			dfd.setOtbValue(Math.round(otbValue * 100.0) / 100.0);
			dfd.setActive('1');
			dfd.setStatus(Conditions.TRUE.toString());
		}
		int size = 1000;
		LOGGER.info("OTB Forecast Monthly:-batch insert start...");
		for (int start = 0; start < data.size(); start += size) {
			int end = Math.min(start + size, data.size());
			List<OTBPlanDFD> sublist = data.subList(start, end);
			if (openToBuy.getIsUserOTBPlan().equalsIgnoreCase(Conditions.FALSE.toString())) {
				result = otbMapper.insertDFDData(sublist);
			} else {
				result = otbMapper.insertUserDFDData(sublist);
			}
		}
		int updateOTBPlanStatus = otbMapper.updateOTBPlanStatus(openToBuy.getPlanId(),"SUCCEEDED");
		LOGGER.info("OTB Forecast Monthly:-batch insert completed...");
		return result;
	}

}
