package com.supplymint.layer.business.service.procurement;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.supplymint.layer.business.contract.procurement.VendorService;
import com.supplymint.layer.data.tenant.procurement.entity.Vendor;
import com.supplymint.layer.data.tenant.procurement.mapper.VendorMapper;

@Service
public class VendorServiceImpl implements VendorService{
	
	private VendorMapper vendorMapper;

	@Autowired
	public VendorServiceImpl(VendorMapper vendorMapper) {
		this.vendorMapper = vendorMapper;
	}

	@Override
	public Vendor getById(Integer id) {
		return vendorMapper.getById(id);
	}

	@Override
	public List<Vendor> getAll(Integer offset, Integer pageSize) {
		return vendorMapper.getAll(offset,pageSize);
	}

	@Override
	public Integer create(Vendor vendor) {
		return vendorMapper.create(vendor);
	}

	@Override
	public Integer update(Vendor vendor) {
		return vendorMapper.update(vendor);
	}

	@Override
	public Integer delete(int id) {
		return vendorMapper.delete(id);
	}

	@Override
	public List<Vendor> getByOrganisationName(String name) {
		return vendorMapper.getByOrganisationName(name);
	}

	@Override
	public Vendor getVendorName(String name) {
		return vendorMapper.getVendorName(name);
	}
	
	@Override
	public Vendor getVendorCode(String code) {
		return vendorMapper.getVendorCode(code);
	}

	
	@Override
	public int record() {
		return vendorMapper.record();
	}

	@Override
	public int isValid(Vendor vendor) {
		return vendorMapper.isValid(vendor);
	}

}
