package com.supplymint.layer.business.service.procurement;

import java.io.File;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.net.URL;
import java.text.DecimalFormat;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import javax.servlet.http.HttpServletRequest;

import org.apache.ibatis.annotations.Param;
import org.mybatis.spring.MyBatisSystemException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.supplymint.exception.StatusCodes.Pagination;
import com.supplymint.exception.SupplyMintException;
import com.supplymint.layer.business.contract.procurement.PurchaseIndentService;
import com.supplymint.layer.business.contract.s3Bucket.S3BucketInfoService;
import com.supplymint.layer.business.service.s3bucketService.S3WrapperService;
import com.supplymint.layer.data.config.ThreadLocalStorage;
import com.supplymint.layer.data.metadata.PurchaseIndentMetaData;
import com.supplymint.layer.data.tenant.administration.entity.ADMItem;
import com.supplymint.layer.data.tenant.inventory.entity.S3BucketInfo;
import com.supplymint.layer.data.tenant.procurement.entity.CatDescMapping;
import com.supplymint.layer.data.tenant.procurement.entity.CatDescMaster;
import com.supplymint.layer.data.tenant.procurement.entity.CatMaster;
import com.supplymint.layer.data.tenant.procurement.entity.Category;
import com.supplymint.layer.data.tenant.procurement.entity.DeptItemUDFSettings;
import com.supplymint.layer.data.tenant.procurement.entity.DescriptionMaster;
import com.supplymint.layer.data.tenant.procurement.entity.FinCharge;
import com.supplymint.layer.data.tenant.procurement.entity.HLevelData;
import com.supplymint.layer.data.tenant.procurement.entity.HsnData;
import com.supplymint.layer.data.tenant.procurement.entity.HsnGstData;
import com.supplymint.layer.data.tenant.procurement.entity.ItemValidation;
import com.supplymint.layer.data.tenant.procurement.entity.PurchaseIndent;
import com.supplymint.layer.data.tenant.procurement.entity.PurchaseIndentDetail;
import com.supplymint.layer.data.tenant.procurement.entity.PurchaseIndentHistory;
import com.supplymint.layer.data.tenant.procurement.entity.PurchaseIndentMaster;
import com.supplymint.layer.data.tenant.procurement.entity.PurchaseIndentProperty;
import com.supplymint.layer.data.tenant.procurement.entity.PurchaseTermData;
import com.supplymint.layer.data.tenant.procurement.entity.PurtermCharge;
import com.supplymint.layer.data.tenant.procurement.entity.PurtermDet;
import com.supplymint.layer.data.tenant.procurement.entity.PurtermMaster;
import com.supplymint.layer.data.tenant.procurement.entity.RatioList;
import com.supplymint.layer.data.tenant.procurement.entity.SupplierData;
import com.supplymint.layer.data.tenant.procurement.entity.Transporters;
import com.supplymint.layer.data.tenant.procurement.mapper.PurchaseIndentMapper;
import com.supplymint.layer.web.endpoint.abstracts.AbstractEndpoint;
import com.supplymint.util.ApplicationUtils.TenantHash;
import com.supplymint.util.CodeUtils;
import com.supplymint.util.CodeUtils.PICarDesType;
import com.supplymint.util.FileUtils;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;

@Service
public class PurchaseIndentServiceImpl extends AbstractEndpoint implements PurchaseIndentService {

	private PurchaseIndentMapper piMapper;

	@Autowired
	private HttpServletRequest request;

	@Autowired
	private S3BucketInfoService s3BucketInfoService;

	@Autowired
	private S3WrapperService s3Wrapper;

	static String delimeter = "/";

	static String separator = "_";

	public static final String DOWNLOAD_ARTICLE = "DOWNLOAD_ARTICLE";

	public static final String DOWNLOAD_LOGO = "DOWNLOAD_LOGO";

	private static final Logger LOGGER = LoggerFactory.getLogger(PurchaseIndentServiceImpl.class);

	public PurchaseIndentServiceImpl(PurchaseIndentMapper piMapper) {
		this.piMapper = piMapper;
	}

	@Override
	public List<HLevelData> getDistinctHierarachyLevels(Integer offset, Integer pagesize) {
		return piMapper.findDistinctHierarachyLevels(offset, pagesize);
	}

	@Override
	public Integer recordDistinctHierarachyLevels() {
		return piMapper.recordDistinctHierarachyLevels();
	}

	@Override
	public List<Transporters> getTransportersBySlcode(String slCode, Integer offset, Integer pagesize) {
		return piMapper.getTransportersBySlcode(slCode, offset, pagesize);
	}

	@Override
	public Integer getRecordTransportersBySlcode(String slCode) {
		return piMapper.getRecordTransportersBySlcode(slCode);
	}

	@Override
	public List<SupplierData> getSupplierData(String department, String siteCode, Integer offset, Integer pagesize) {
		return piMapper.getSupplierData(department, siteCode, offset, pagesize);
	}

	@Override
	public Integer getRecordSupplierData(String department, String siteCode) {
		return piMapper.getRecordSupplierData(department, siteCode);
	}

	@Override
	public Integer getSupplierLeadTime(String slCode) {
		return piMapper.getSupplierLeadTime(slCode);
	}

	@Override
	public List<ADMItem> getPricePoint(String slCode, String department, Integer offset, Integer pagesize) {
		return piMapper.getArticlePricePoint(slCode, department, offset, pagesize);
	}

	@Override
	public Integer getRecordPricePoint(String slCode, String department) {
		return piMapper.getRecordArticlePricePoint(slCode, department);
	}

	@Override
	public List<CatMaster> getAllSizeByArticleCode(Integer offset, Integer pagesize, String hl4Code) {
		return null;
	}

	@Override
	public Integer getRecordAllSizeByArticleCode(String hl4Code) {
		return piMapper.getRecordAllSizeByArticleCode(hl4Code);
	}

	@Override
	public List<CatMaster> getAllUniqueCodeByArticleCode(Integer offset, Integer pagesize, String hl4Code) {
		return null;
	}

	@Override
	public Integer getRecordAllUniqueCodeByArticleCode(String hl4Code) {
		return piMapper.getRecordAllUniqueCodeByArticleCode(hl4Code);
	}

	@Override
	public List<CatMaster> getAllBrandsByArticleCode(Integer offset, Integer pagesize, String hl4Code) {
		return null;
	}

	@Override
	public Integer getRecordAllBrandsByArticleCode(String hl4Code) {
		return piMapper.getRecordAllBrandsByArticleCode(hl4Code);
	}

	@Override
	public List<CatMaster> getAllPatternsByArticleCode(Integer offset, Integer pagesize, String hl4Code) {
		return null;
	}

	@Override
	public Integer getRecordAllPatternsByArticleCode(String hl4Code) {
		return piMapper.getRecordAllPatternsByArticleCode(hl4Code);

	}

	@Override
	public List<CatMaster> getAllAssortmentsByArticleCode(Integer offset, Integer pagesize, String hl4Code) {
		return null;
	}

	@Override
	public Integer getRecordAllAssortmentsByArticleCode(String hl4Code) {
		return piMapper.getRecordAllAssortmentsByArticleCode(hl4Code);
	}

	@Override
	public List<CatMaster> getAllColors(Integer offset, Integer pagesize, String hl4Code) {
		return null;
	}

	@Override
	public Integer getRecordAllColors(String hl4Code) {
		return piMapper.getRecordAllColors(hl4Code);
	}

	@Override
	public List<ADMItem> getArticleData(String hl4Code) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer calculateQtyForArticleData(ObjectNode node) {

		Integer sumofRatios = 0;

		if (!node.isNull()) {
			String[] ratio = node.get("ratio").asText().split(",");

			List<String> list = Arrays.asList(ratio);

//			sumofRatios = list.parallelStream().mapToInt( i -> Integer.parseInt(i) ).sum();

			for (String s : list) {
				sumofRatios = sumofRatios + Integer.parseInt(s);
			}

			Integer numOfColors = Integer.parseInt(node.get("numOfColors").asText());
			Integer numOfSets = Integer.parseInt(node.get("numOfSets").asText());

			if (sumofRatios != null && numOfColors != null && numOfSets != null) {
				Integer qty = sumofRatios * numOfColors * numOfSets;
				return qty;
			} else {
				return null;
			}
		} else
			return null;
	}

	@Override
	public Map<String, Double> calculateInTakeMarginForArticleData(ObjectNode node) {

		Map<String, Double> map = null;

		if (!node.isNull()) {

			Double rate = Double.parseDouble(node.get("rate").asText());
			Double rsp = Double.parseDouble(node.get("rsp").asText());
			Double gst = Double.parseDouble(node.get("gst").asText());

			if (rate != null && rsp != null && gst != null) {

				double gstAmount = rate * gst / 100;

				double rateDiff = rsp - (rate + gstAmount);

				double actualMarkUp = (rateDiff / rsp) * 100;

				double calculatedMargin = ((rsp - rate) / rsp) * 100;

				DecimalFormat df = new DecimalFormat("###.##");

				map = new HashMap<String, Double>();
				map.put("actualMarkUp", Double.parseDouble(df.format(actualMarkUp)));
				map.put("calculatedMargin", Double.parseDouble(df.format(calculatedMargin)));

				LOGGER.info("Margins obtained are as follow: {}", map);
				return map;
			} else {
				return null;
			}
		} else
			return null;
	}

	@Override
	public List<DescriptionMaster> getAllSeasonByArticleCode(Integer offset, @Param("pagesize") Integer pagesize,
			String hl4Code) {
		return null;
	}

	@Override
	public Integer getRecordAllSeasonByArticleCode(String hl4Code) {
		return piMapper.getRecordAllSeasonByArticleCode(hl4Code);

	}

	@Override
	public List<DescriptionMaster> getAllFabricByArticleCode(Integer offset, Integer pagesize, String hl4Code) {
		return null;
	}

	@Override
	public Integer getRecordAllFabricByArticleCode(String hl4Code) {
		return piMapper.getRecordAllFabricByArticleCode(hl4Code);
	}

	@Override
	public List<DescriptionMaster> getAllDarwinByArticleCode(Integer offset, Integer pagesize, String hl4Code) {
		return null;
	}

	@Override
	public Integer getRecordAllDarwinByArticleCode(String hl4Code) {
		return piMapper.getRecordAllDarwinByArticleCode(hl4Code);

	}

	@Override
	public List<DescriptionMaster> getAllWeavedByArticleCode(Integer offset, Integer pagesize, String hl4Code) {
		return null;
	}

	@Override
	public Integer getRecordAllWeavedByArticleCode(String hl4Code) {
		return piMapper.getRecordAllWeavedByArticleCode(hl4Code);
	}

	@Override
	public int validateItem(ItemValidation item) {
		int serviceCount = piMapper.validateItem(item);

		LOGGER.info("service count :%s", serviceCount);
		return serviceCount;
	}

	@Override
	public List<HsnGstData> getHsnGstDataByHsnSacCode(@Param("hsnSacCode") String hsnSacCode,
			@Param("piDate") String piDate) {
		return piMapper.getHsnGstDataByHsnSacCode(hsnSacCode, piDate);
	}

	@Override
	public List<PurtermMaster> getPurchaseTermDataForCGST(String tradeGrpCode) {
		return piMapper.getPurchaseTermDataForCGST(tradeGrpCode);
	}

	@Override
	public List<PurtermMaster> getPurchaseTermDataForIgst(String tradeGrpCode) {
		return piMapper.getPurchaseTermDataForIgst(tradeGrpCode);
	}

	@Override
	public Integer insertPI(PurchaseIndent item) {
		return piMapper.createPI(item);
	}

	@Override
	public Integer insertPIDetails(PurchaseIndentDetail items) {
		return piMapper.createPIDetails(items);
	}

	@Override
	public PurtermDet getPurchaseTermDetailsByChgCode(Integer purchaseTermCode, BigDecimal chgCode) {
		return piMapper.getPurchaseTermDetailsByChgCode(purchaseTermCode, chgCode);
	}

	@Override
	public List<PurtermCharge> getPurchaseTermChargeDetailsByPurchaseTermCode(Integer purchaseTermCode) {
		return piMapper.getPurchaseTermChargeDetailByPurtermCode(purchaseTermCode);
	}

	@Override
	public boolean compareSupplierAndClientGstin(String clientGstInNo, Integer supplierGstInStateCode) {

		LOGGER.info("compareSupplierAndClientGstin method starts here....");

		Integer clientStateCode = Integer.parseInt(clientGstInNo.substring(0, 2));

		if (clientStateCode == supplierGstInStateCode) {
			return true;
		}

		LOGGER.info("compareSupplierAndClientGstin method ends here....");
		return false;
	}

	@Override
	public HsnGstData getMaxEffectiveDateByHsnSacCode(String hsnSacCode, String piDate) {
		return piMapper.getMaxEffectiveDateByHsnSacCode(hsnSacCode, piDate);
	}

	@Override
	public HsnGstData getMaxAmountFrom(String hsnSacCode, String piDate) {
		return piMapper.getMaxAmountFrom(hsnSacCode, piDate);
	}

	@Override
	public HsnGstData getMinAmountFrom(String hsnSacCode, String piDate) {
		return piMapper.getMinAmountFrom(hsnSacCode, piDate);
	}

	@Override
	public HsnGstData getApplicableGstSlabForTransactionPrice(String hsnSacCode, String piDate,
			Double transactionPrice) {
		return piMapper.getGstSlabUsingTransactionPrice(hsnSacCode, piDate, transactionPrice);
	}

	@Override
	public Map<String, Double> calulcateTotalForCgstSgstRates(Double subTotal, Double basicValue, Double cgstRate,
			Double sgstRate, Double cessRate, Double ch1, Double ch2, Double ch3, String lineItermSign,
			String formulae) {

		LOGGER.info("calulcateTotalForCgstSgstRates method starts here....");
		Map<String, Double> map = new HashMap<String, Double>();

		// these two lines is written by Prabhakar Srivastava
		// Style Baazar has some extra formula to calculate total cgst n sgst rate.
		String tenantHashKey = ThreadLocalStorage.getTenantHashKey();

		if (tenantHashKey.equalsIgnoreCase(TenantHash.valueOf("VMART").getHashKey())) {

			switch (formulae) {
			// calculate charges and basic value using formula starts here

			case PurchaseIndentMetaData.Formula.B:
				ch1 = (basicValue * cgstRate) / 100;
				ch2 = (basicValue * sgstRate) / 100;
				ch3 = (basicValue * cessRate) / 100;

				double sumOfCharges = ch1 + ch2 + ch3;
				LOGGER.info("Total charge obtained for formula B : {}", sumOfCharges);

				if (lineItermSign.equals("+")) {

					subTotal = subTotal + sumOfCharges;
				} else {
					subTotal = subTotal - sumOfCharges;
				}

				map.put("chargeAmount", sumOfCharges);
				map.put("subTotal", subTotal);
				LOGGER.info("Line Item basic value after Formula B : {}", subTotal);
				break;

			case PurchaseIndentMetaData.Formula.B1:
				ch1 = (basicValue * cgstRate) / 100;
				ch2 = (basicValue * sgstRate) / 100;
				ch3 = (basicValue * cessRate) / 100;

				if (lineItermSign.equals("+")) {
					subTotal = subTotal + (ch1 + ch2 + ch3);
				} else {
					subTotal = subTotal - (ch1 + ch2 + ch3);
				}

				map.put("chargeAmount", ch1 + ch2 + ch3);
				map.put("subTotal", subTotal);
				LOGGER.info("Line Item basic value after Formula B+1 : {} ", subTotal);
				break;

			case PurchaseIndentMetaData.Formula.B12:
				ch1 = (basicValue * cgstRate) / 100;
				ch2 = (basicValue * sgstRate) / 100;
				ch3 = (basicValue * cessRate) / 100;

				if (lineItermSign.equals("+")) {
					subTotal = subTotal + (ch1 + ch2 + ch3);
				} else {
					subTotal = subTotal - (ch1 + ch2 + ch3);
				}

				map.put("chargeAmount", ch1 + ch2 + ch3);
				map.put("subTotal", subTotal);
				LOGGER.info("Line Item basic value after Formula B+1+2 : {},", subTotal);
				break;
			case PurchaseIndentMetaData.Formula.B123:
				ch1 = (basicValue * cgstRate) / 100;
				ch2 = (basicValue * sgstRate) / 100;
				ch3 = (basicValue * cessRate) / 100;

				if (lineItermSign.equals("+")) {
					subTotal = subTotal + (ch1 + ch2 + ch3);
				} else {
					subTotal = subTotal - (ch1 + ch2 + ch3);
				}

				map.put("chargeAmount", ch1 + ch2 + ch3);
				map.put("subTotal", subTotal);
				LOGGER.info("Line Item basic value after Formula B+1+2+3 : {} ", subTotal);
				break;
			case PurchaseIndentMetaData.Formula._B1:
				ch1 = (basicValue * cgstRate) / 100;
				ch2 = (basicValue * sgstRate) / 100;
				ch3 = (basicValue * cessRate) / 100;

				if (lineItermSign.equals("+")) {
					subTotal = subTotal + (ch1 + ch2 + ch3);
				} else {
					subTotal = subTotal - (ch1 + ch2 + ch3);
				}

				map.put("chargeAmount", ch1 + ch2 + ch3);
				map.put("subTotal", subTotal);
				LOGGER.info("Line Item basic value after Formula B-1 : {} ", subTotal);
				break;
			case PurchaseIndentMetaData.Formula.B_12:
				ch1 = (basicValue * cgstRate) / 100;
				ch2 = (basicValue * sgstRate) / 100;
				ch3 = (basicValue * cessRate) / 100;

				if (lineItermSign.equals("+")) {
					subTotal = subTotal + (ch1 + ch2 + ch3);
				} else {
					subTotal = subTotal - (ch1 + ch2 + ch3);
				}

				map.put("chargeAmount", ch1 + ch2 + ch3);
				map.put("subTotal", subTotal);
				LOGGER.info("Line Item basic value after Formula B-1+2 : {} ", subTotal);
				break;
			default:
				LOGGER.info("No formula found");
			}
		} else {
			switch (formulae) {
			// calculate charges and basic value using formula starts here
			case PurchaseIndentMetaData.Formula.B:
				ch1 = (basicValue * cgstRate) / 100;
				ch2 = (basicValue * sgstRate) / 100;
				ch3 = (basicValue * cessRate) / 100;

				double sumOfCharges = ch1 + ch2 + ch3;
				LOGGER.info("Total charge obtained for formula B : {}", sumOfCharges);

				if (lineItermSign.equals("+")) {

					subTotal = subTotal + sumOfCharges;
				} else {
					subTotal = subTotal - sumOfCharges;
				}

				map.put("chargeAmount", sumOfCharges);
				map.put("subTotal", subTotal);
				LOGGER.info("Line Item basic value after Formula B : {}", subTotal);
				break;

			case PurchaseIndentMetaData.Formula.B1:
				ch1 = (basicValue * cgstRate) / 100;
				ch2 = (basicValue * sgstRate) / 100;
				ch3 = (basicValue * cessRate) / 100;

				if (lineItermSign.equals("+")) {
					subTotal = subTotal + (ch1 + ch2 + ch3);
				} else {
					subTotal = subTotal - (ch1 + ch2 + ch3);
				}

				map.put("chargeAmount", ch1 + ch2 + ch3);
				map.put("subTotal", subTotal);
				LOGGER.info("Line Item basic value after Formula B+1 : {} ", subTotal);
				break;

			case PurchaseIndentMetaData.Formula.B12:
				ch1 = (basicValue * cgstRate) / 100;
				ch2 = (basicValue * sgstRate) / 100;
				ch3 = (basicValue * cessRate) / 100;

				if (lineItermSign.equals("+")) {
					subTotal = subTotal + (ch1 + ch2 + ch3);
				} else {
					subTotal = subTotal - (ch1 + ch2 + ch3);
				}

				map.put("chargeAmount", ch1 + ch2 + ch3);
				map.put("subTotal", subTotal);
				LOGGER.info("Line Item basic value after Formula B+1+2 : {},", subTotal);
				break;
			case PurchaseIndentMetaData.Formula.B123:
				ch1 = (basicValue * cgstRate) / 100;
				ch2 = (basicValue * sgstRate) / 100;
				ch3 = (basicValue * cessRate) / 100;

				if (lineItermSign.equals("+")) {
					subTotal = subTotal + (ch1 + ch2 + ch3);
				} else {
					subTotal = subTotal - (ch1 + ch2 + ch3);
				}

				map.put("chargeAmount", ch1 + ch2 + ch3);
				map.put("subTotal", subTotal);
				LOGGER.info("Line Item basic value after Formula B+1+2+3 : {} ", subTotal);
				break;
			case PurchaseIndentMetaData.Formula._B1:
				ch1 = (basicValue * cgstRate) / 100;
				ch2 = (basicValue * sgstRate) / 100;
				ch3 = (basicValue * cessRate) / 100;

				if (lineItermSign.equals("+")) {
					subTotal = subTotal + (ch1 + ch2 + ch3);
				} else {
					subTotal = subTotal - (ch1 + ch2 + ch3);
				}

				map.put("chargeAmount", ch1 + ch2 + ch3);
				map.put("subTotal", subTotal);
				LOGGER.info("Line Item basic value after Formula B-1 : {} ", subTotal);
				break;
			case PurchaseIndentMetaData.Formula.B_12:
				ch1 = (basicValue * cgstRate) / 100;
				ch2 = (basicValue * sgstRate) / 100;
				ch3 = (basicValue * cessRate) / 100;

				if (lineItermSign.equals("+")) {
					subTotal = subTotal + (ch1 + ch2 + ch3);
				} else {
					subTotal = subTotal - (ch1 + ch2 + ch3);
				}

				map.put("chargeAmount", ch1 + ch2 + ch3);
				map.put("subTotal", subTotal);
				LOGGER.info("Line Item basic value after Formula B-1+2 : {} ", subTotal);
				break;
			case PurchaseIndentMetaData.Formula.B15:
				ch1 = (basicValue * cgstRate) / 100;
				ch2 = (basicValue * sgstRate) / 100;
				ch3 = (basicValue * cessRate) / 100;

				if (lineItermSign.equals("+")) {
					subTotal = subTotal + (ch1 + ch2 + ch3);
				} else {
					subTotal = subTotal - (ch1 + ch2 + ch3);
				}

				map.put("chargeAmount", ch1 + ch2 + ch3);
				map.put("subTotal", subTotal);
				LOGGER.info("Line Item basic value after Formula B-1+2 : {} ", subTotal);
				break;
			case PurchaseIndentMetaData.Formula.B14:
				ch1 = (basicValue * cgstRate) / 100;
				ch2 = (basicValue * sgstRate) / 100;
				ch3 = (basicValue * cessRate) / 100;

				if (lineItermSign.equals("+")) {
					subTotal = subTotal + (ch1 + ch2 + ch3);
				} else {
					subTotal = subTotal - (ch1 + ch2 + ch3);
				}

				map.put("chargeAmount", ch1 + ch2 + ch3);
				map.put("subTotal", subTotal);
				LOGGER.info("Line Item basic value after Formula B-1+2 : {} ", subTotal);
				break;
			default:
				ch1 = (basicValue * cgstRate) / 100;
				ch2 = (basicValue * sgstRate) / 100;
				ch3 = (basicValue * cessRate) / 100;

				if (lineItermSign.equals("+")) {
					subTotal = subTotal + (ch1 + ch2 + ch3);
				} else {
					subTotal = subTotal - (ch1 + ch2 + ch3);
				}

				map.put("chargeAmount", ch1 + ch2 + ch3);
				map.put("subTotal", subTotal);
				LOGGER.info("No formula found");
			}
		}
		LOGGER.info("calulcateTotalForCgstSgstRates method ends here....");
		return map;
	}

	@Override
	public Map<String, Double> calulcateTotalForIgstRates(Double subTotal, Double basicValue, Double igstRate,
			Double cessRate, Double ch1, Double ch2, String lineItermSign, String formulae) {

		LOGGER.info("calulcateTotalForIgstRates method starts here....");
		Map<String, Double> map = new HashMap<String, Double>();

		double sumOfCharges = 0.0;
		switch (formulae) {

		case PurchaseIndentMetaData.Formula.B:
			ch1 = (basicValue * igstRate) / 100;
			ch2 = (basicValue * cessRate) / 100;

			sumOfCharges = ch1 + ch2;
			if (lineItermSign.equals("+")) {
				subTotal = subTotal + sumOfCharges;
			} else {
				subTotal = subTotal + (-sumOfCharges);
			}

			map.put("chargeAmount", ch1 + ch2);
			map.put("subTotal", subTotal);
			LOGGER.info("Line Item basic value after Formula B : {} ", subTotal);
			break;

		case PurchaseIndentMetaData.Formula.B1:
			ch1 = (basicValue * igstRate) / 100;
			ch2 = (basicValue * cessRate) / 100;

			sumOfCharges = ch1 + ch2;
			if (lineItermSign.equals("+")) {
				subTotal = subTotal + sumOfCharges;
			} else {
				subTotal = subTotal + (-sumOfCharges);
			}

			map.put("chargeAmount", ch1 + ch2);
			map.put("subTotal", subTotal);
			LOGGER.info("Line Item basic value after Formula B+1 : {} ", subTotal);
			break;

		case PurchaseIndentMetaData.Formula.B12:
			ch1 = (basicValue * igstRate) / 100;
			ch2 = (basicValue * cessRate) / 100;

			sumOfCharges = ch1 + ch2;
			if (lineItermSign.equals("+")) {
				subTotal = subTotal + sumOfCharges;
			} else {
				subTotal = subTotal + (-sumOfCharges);
			}

			map.put("chargeAmount", ch1 + ch2);
			map.put("subTotal", subTotal);
			LOGGER.info("Line Item basic value after Formula B+1+2 : {},", subTotal);
			break;

		case PurchaseIndentMetaData.Formula.B123:
			ch1 = (basicValue * igstRate) / 100;
			ch2 = (basicValue * cessRate) / 100;

			sumOfCharges = ch1 + ch2;
			if (lineItermSign.equals("+")) {
				subTotal = subTotal + sumOfCharges;
			} else {
				subTotal = subTotal + (-sumOfCharges);
			}

			map.put("chargeAmount", ch1 + ch2);
			map.put("subTotal", subTotal);
			LOGGER.info("Line Item basic value after Formula B+1+2+3 : {} ", subTotal);
			break;

		case PurchaseIndentMetaData.Formula._B1:
			ch1 = (basicValue * igstRate) / 100;
			ch2 = (basicValue * cessRate) / 100;

			sumOfCharges = ch1 + ch2;
			if (lineItermSign.equals("+")) {
				subTotal = subTotal + sumOfCharges;
			} else {
				subTotal = subTotal + (-sumOfCharges);
			}

			map.put("chargeAmount", ch1 + ch2);
			map.put("subTotal", subTotal);
			LOGGER.info("Line Item basic value after Formula B-1 : {} ", subTotal);
			break;
		default:
			LOGGER.info("No formula found");
		}

		return map;
	}

	@Override
	public ObjectNode getGstSlabForIgstRates(HsnGstData applicableGstSlab) {

		String taxName = applicableGstSlab.getTaxName();
		String gstSlab = applicableGstSlab.getSlab();
		String slabEffectiveDate = applicableGstSlab.getEffectiveDate().toString();
		Double igstRate = applicableGstSlab.getIgstRate().doubleValue();
		Double cessRate = applicableGstSlab.getCessRate().doubleValue();

		ObjectNode rateNode = getMapper().createObjectNode();
		rateNode.put("taxName", taxName);
		rateNode.put("gstSlab", gstSlab);
		rateNode.put("effectiveDate", slabEffectiveDate);
		rateNode.put("igstRate", igstRate);
		rateNode.put("cessRate", cessRate);

		return rateNode;
	}

	@Override
	public ObjectNode getPurtermChargeObjectNode(PurtermCharge purtermCharge) {

		ObjectNode tempNode = getMapper().createObjectNode();

		int seq = purtermCharge.getPtSeq().intValue();
		Double finChgRate = purtermCharge.getRate().doubleValue();
		String source = purtermCharge.getSource();
		String gstComponent = purtermCharge.getGstComponent();
		String isTax = purtermCharge.getIsTax();
		String sign = purtermCharge.getSign();
		String formula = purtermCharge.getPtFormulae();
		String finChgOperationLevel = purtermCharge.getOperationLevel();
		String calculationBasis = purtermCharge.getBasis();
		BigDecimal chgCode = purtermCharge.getChgCode();

		tempNode.put("chgCode", chgCode);
		tempNode.put("seq", seq);
		tempNode.put("finFormula", formula);
		tempNode.put("finChgRate", finChgRate);
		tempNode.put("finSource", source);
		tempNode.put("gstComponent", gstComponent);
		tempNode.put("isTax", isTax);
		tempNode.put("sign", sign);
		tempNode.put("finChgOperationLevel", finChgOperationLevel);
		tempNode.put("calculationBasis", calculationBasis);

		return tempNode;
	}

	@Override
	public ObjectNode getPurtermDetObjectNode(PurtermDet purTermDet) {

		ObjectNode chargeNode = getMapper().createObjectNode();

		BigDecimal chargeCode = purTermDet.getChgCode();
		String formulae = purTermDet.getFormulae();
		String lineItermSign = purTermDet.getSign();
		BigDecimal rate = purTermDet.getRate();
		BigDecimal sequence = purTermDet.getSeq();
		String chargeOperationLevel = purTermDet.getOperationLevel();

		chargeNode.put("seq", sequence);
		chargeNode.put("chgcode", chargeCode);
		chargeNode.put("rate", rate);
		chargeNode.put("formula", formulae);
		chargeNode.put("sign", lineItermSign);
		chargeNode.put("operationLevel", chargeOperationLevel);

		return chargeNode;
	}

	@Override
	public Integer insertItem(ADMItem item) {
		return piMapper.createItem(item);
	}

	@Override
	public Integer getGstRateByTaxName(String taxName) {

		int gstRate = 0;

		switch (taxName) {

		case "GST 3%":
			gstRate = 3;
			break;

		case "GST 5%":
			gstRate = 5;
			break;

		case "GST 12%":
			gstRate = 12;
			break;

		case "GST 18%":
			gstRate = 18;
			break;

		case "GST 28% with (12% CESS)":
			gstRate = 28;
			break;

		case "Exempted":
			gstRate = 0;
			break;

		case "GST without CESS 28%":
			gstRate = 28;
			break;

		case "GST 28% with (15% CESS)":
			gstRate = 28;
			break;

		default:
			LOGGER.info("No matching gst rate found");
		}

		return gstRate;
	}

	@Override
	public PurchaseIndentDetail setPurchaseIndentDetail(PurchaseIndentDetail detail) {
		PurchaseIndentDetail pid = new PurchaseIndentDetail();
		pid.setOrderDetailId(detail.getOrderDetailId());
//		LOGGER.info("Order detail id :" + detail.getOrderDetailId());
		pid.setDetailJsonNode(detail.getDetailJsonNode());
		pid.setCat4Code(detail.getCat4Code());
		pid.setCat4Name(detail.getCat4Name());
		pid.setCat2Code(detail.getCat2Code());
		pid.setCat2Name(detail.getCat2Name());
		pid.setCalculatedMargin(detail.getCalculatedMargin());
		pid.setDesc4Code(detail.getDesc4Code());
		pid.setDesc4Name(detail.getDesc4Name());
		pid.setDeliveryDate(detail.getDeliveryDate());
		pid.setDesign(detail.getDesign());
		pid.setDesc3Code(detail.getDesc3Code());
		pid.setDesc3Name(detail.getDesc3Name());
		pid.setGst(detail.getGst());
		pid.setHl4Code(detail.getHl4Code());
		pid.setHl4Name(detail.getHl4Name());
		pid.setImage(detail.getImage());
		pid.setIntakeMargin(detail.getIntakeMargin());
		pid.setNetAmountTotal(detail.getNetAmountTotal());
		pid.setNoOfSets(detail.getNoOfSets());
		pid.setOtb(detail.getOtb());
		pid.setCat3Code(detail.getCat3Code());
		pid.setCat3Name(detail.getCat3Name());
		pid.setDesc2Code(detail.getDesc2Code());
		pid.setDesc2Name(detail.getDesc2Name());

		pid.setQuantity(detail.getQuantity());
		pid.setRate(detail.getRate());
		pid.setRemarks(detail.getRemarks());
		pid.setTax(detail.getTax());

		pid.setCat1Code(detail.getCat1Code());
		pid.setCat1Name(detail.getCat1Name());
		pid.setDesc5Code(detail.getDesc5Code());
		pid.setDesc5Name(detail.getDesc5Name());

		pid.setFinCharge(detail.getFinCharge());
		pid.setImage(detail.getImage());
		pid.setContainsImage(detail.getContainsImage());

		pid.setHsnSacCode(detail.getHsnSacCode());
		pid.setRsp(detail.getRsp());
		pid.setMrp(detail.getMrp());
		pid.setMrpStart(detail.getMrpStart());
		pid.setMrpEnd(detail.getMrpEnd());
		pid.setMarginRule(detail.getMarginRule());
		pid.setTotalAmount(detail.getTotalAmount());

		pid.setItemudf1(detail.getItemudf1());
		pid.setItemudf2(detail.getItemudf2());
		pid.setItemudf3(detail.getItemudf3());
		pid.setItemudf4(detail.getItemudf4());
		pid.setItemudf5(detail.getItemudf5());
		pid.setItemudf6(detail.getItemudf6());
		pid.setItemudf7(detail.getItemudf7());
		pid.setItemudf8(detail.getItemudf8());
		pid.setItemudf9(detail.getItemudf9());
		pid.setItemudf10(detail.getItemudf10());
		pid.setItemudf11(detail.getItemudf11());
		pid.setItemudf12(detail.getItemudf12());
		pid.setItemudf13(detail.getItemudf13());
		pid.setItemudf14(detail.getItemudf14());
		pid.setItemudf15(detail.getItemudf15());
		pid.setItemudf16(detail.getItemudf16());
		pid.setItemudf17(detail.getItemudf17());
		pid.setItemudf18(detail.getItemudf18());
		pid.setItemudf19(detail.getItemudf19());
		pid.setItemudf20(detail.getItemudf20());

		pid.setUdf1(detail.getUdf1());
		pid.setUdf2(detail.getUdf2());
		pid.setUdf3(detail.getUdf3());
		pid.setUdf4(detail.getUdf4());
		pid.setUdf5(detail.getUdf5());
		pid.setUdf6(detail.getUdf6());
		pid.setUdf7(detail.getUdf7());
		pid.setUdf8(detail.getUdf8());
		pid.setUdf9(detail.getUdf9());
		pid.setUdf10(detail.getUdf10());

		pid.setUdf11(detail.getUdf11());
		pid.setUdf12(detail.getUdf12());
		pid.setUdf13(detail.getUdf13());
		pid.setUdf14(detail.getUdf14());
		pid.setUdf15(detail.getUdf15());
		pid.setUdf16(detail.getUdf16());
		pid.setUdf17(detail.getUdf17());
		pid.setUdf18(detail.getUdf18());
		pid.setUdf19(detail.getUdf19());
		pid.setUdf20(detail.getUdf20());

		pid.setActive(PurchaseIndentMetaData.PIStatus.INACTIVE);
		pid.setStatus(PurchaseIndentMetaData.PIStatus.STATUS);

		pid.setCreatedBy(detail.getCreatedBy());
		pid.setCreatedTime(detail.getCreatedTime());
		pid.setIpAddress(detail.getIpAddress());

		for (int k = 0; k < detail.getImage().size(); k++) {
			if (k == 0)
				pid.setImage1(detail.getImage().get(k));
			else if (k == 1)
				pid.setImage2(detail.getImage().get(k));
			else if (k == 2)
				pid.setImage3(detail.getImage().get(k));
			else if (k == 3)
				pid.setImage4(detail.getImage().get(k));
			else if (k == 4)
				pid.setImage5(detail.getImage().get(k));

		}
		return pid;
	}

	@Override
	public ItemValidation setItemValidation(int i, PurchaseIndent pi) {
		ItemValidation item = new ItemValidation();
		item.setHl1Name(pi.getHl1Code());
		item.setHl2Name(pi.getHl2Code());
		item.setHl3Name(pi.getHl3Code());
		item.setHl4Code(pi.getPiDetails().get(i).getHl4Code());

		item.setBrand(pi.getPiDetails().get(i).getCat2Name());
		item.setPattern(pi.getPiDetails().get(i).getCat3Name());
		item.setAssortment(pi.getPiDetails().get(i).getCat4Name());
		item.setSize(pi.getPiDetails().get(i).getSizeName());
		item.setColor(pi.getPiDetails().get(i).getColorName());
		LOGGER.info("Item Validation :" + item.toString());
		return item;
	}

	@Override
	public ADMItem setItem(int i, PurchaseIndent pi) throws Exception {
		OffsetDateTime currentTime = OffsetDateTime.now();
		currentTime = currentTime.plusHours(new Long(5));
		currentTime = currentTime.plusMinutes(new Long(30));

		String ipAddress = InetAddress.getLocalHost().getHostAddress().trim().toString();

		ADMItem item = new ADMItem();
		item.sethL1Name(pi.getHl1Name());
		item.sethL2Name(pi.getHl2Name());
		item.sethL3Name(pi.getHl3Name());
		item.sethL4Code(pi.getPiDetails().get(i).getHl4Code());
		item.setBrand(pi.getPiDetails().get(i).getCat2Name());
		item.setPattern(pi.getPiDetails().get(i).getCat3Name());
		item.setAssortment(pi.getPiDetails().get(i).getCat4Name());
		item.setSize(pi.getPiDetails().get(i).getSizeName());
		item.setColor(pi.getPiDetails().get(i).getColorName());
		LOGGER.info("color name : %s", pi.getPiDetails().get(i).getColorName());
		item.setDesign(pi.getPiDetails().get(i).getDesign());
		item.setCostPrice(pi.getPiDetails().get(i).getRsp());
		item.setSellPrice(pi.getPiDetails().get(i).getMrp());

		item.setCreatedBy(pi.getCreatedBy());
		item.setCreatedTime(currentTime);
		item.setIpAddress(ipAddress);
		item.setActive('t');
		item.setStatus("active");
		/*
		 * item.setUnique(pi.getPiDetails().get(i).getUniqueCode());
		 * item.setSeason(pi.getPiDetails().get(i).getSeasonCode());
		 * item.setFabric(pi.getPiDetails().get(i).getFabricCode());
		 * item.setDarwin(pi.getPiDetails().get(i).getDarwinCode());
		 * item.setWeaved(pi.getPiDetails().get(i).getWeavedCode());
		 * item.setMrp(pi.getPiDetails().get(i).getCostPrice());
		 */
		return item;
	}

	@Override
	public List<String> findAll(Integer offset, Integer pagesize) {

		return piMapper.findAll(offset, pagesize);
	}

	@Override
	public Integer record(String orgId) {
		return piMapper.record(orgId);
	}

	@Override
	public List<ADMItem> searchByArticlePricePoint(Integer offset, Integer pagesize, String slCode, String articleName,
			String department, String costPrice, String sellPrice) {
		return piMapper.searchByArticlePricePoint(offset, pagesize, slCode, articleName, department, costPrice,
				sellPrice);
	}

	@Override
	public Integer recordSearchByArticlePricePoint(String slCode, String articleName, String department,
			String costPrice, String sellPrice) {
		return piMapper.recordSearchByArticlePricePoint(slCode, articleName, department, costPrice, sellPrice);
	}

	@Override
	public List<Transporters> searchTransportersBySlcode(Integer offset, Integer pagesize, String slCode,
			String transporterName) {
		return piMapper.searchTransportersBySlcode(offset, pagesize, slCode, transporterName);
	}

	@Override
	public List<ADMItem> searchArticlePricePoint(Integer offset, Integer pagesize, String slCode, String department,
			String search) {
		return piMapper.searchArticlePricePoint(offset, pagesize, slCode, department, search);
	}

	@Override
	public Integer recordArticlePricePoint(String slCode, String department, String search) {
		return piMapper.recordArticlePricePoint(slCode, department, search);
	}

	@Override
	public Integer recordTransportersBySlcode(String slCode, String transporterName) {
		return piMapper.recordTransportersBySlcode(slCode, transporterName);

	}

	@Override
	public List<CatMaster> searchAllColors(Integer offset, Integer pagesize, String hl4Code, String cname) {
		return null;
	}

	@Override
	public Integer recordAllColors(String hl4Code, String cname) {
		return piMapper.recordAllColors(hl4Code, cname);
	}

	@Override
	public List<CatMaster> searchAllBrandsByArticleCode(Integer offset, Integer pagesize, String hl4Code,
			String cname) {
		return null;
	}

	@Override
	public Integer recordAllBrandsByArticleCode(@Param("hl4Code") String hl4Code, @Param("cname") String cname) {
		return piMapper.recordAllBrandsByArticleCode(hl4Code, cname);
	}

	@Override
	public List<CatMaster> searchAllPatternsByArticleCode(Integer offset, Integer pagesize, String hl4Code,
			String cname) {
		return null;
	}

	@Override
	public Integer recordAllPatternsByArticleCode(String hl4Code, String cname) {
		return piMapper.recordAllPatternsByArticleCode(hl4Code, cname);
	}

	@Override
	public List<CatMaster> searchAllUniqueCodeByArticleCode(Integer offset, Integer pagesize, String hl4Code,
			String cname) {
		return null;
	}

	@Override
	public Integer recordAllUniqueCodeByArticleCode(String hl4Code, String cname) {
		return piMapper.recordAllUniqueCodeByArticleCode(hl4Code, cname);
	}

	@Override
	public List<CatMaster> searchAllAssortmentsByArticleCode(Integer offset, Integer pagesize, String hl4Code,
			String cname) {
		return null;
	}

	@Override
	public Integer recordAllAssortmentsByArticleCode(String hl4Code, String cname) {
		return piMapper.recordAllAssortmentsByArticleCode(hl4Code, cname);
	}

	@Override
	public List<CatMaster> searchAllSizeByArticleCode(Integer offset, Integer pagesize, String hl4Code, String cname) {
		return null;
	}

	@Override
	public Integer recordAllSizeByArticleCode(String hl4Code, String cname) {
		return piMapper.recordAllSizeByArticleCode(hl4Code, cname);
	}

	@Override
	public List<DescriptionMaster> searchAllSeasonByArticleCode(Integer offset, Integer pagesize, String hl4Code,
			String description) {
		return null;
	}

	@Override
	public Integer recordAllSeasonByArticleCode(String hl4Code, String description) {

		return piMapper.recordAllSeasonByArticleCode(hl4Code, description);
	}

	@Override
	public List<DescriptionMaster> searchAllFabricByArticleCode(Integer offset, Integer pagesize, String hl4Code,
			String description) {
		return null;
	}

	@Override
	public Integer recordAllFabricByArticleCode(String hl4Code, String description) {
		return piMapper.recordAllFabricByArticleCode(hl4Code, description);
	}

	@Override
	public List<DescriptionMaster> searchAllDarwinByArticleCode(Integer offset, Integer pagesize, String hl4code,
			String description) {
		return null;
	}

	@Override
	public Integer recordAllDarwinByArticleCode(String hl4Code, String description) {
		return piMapper.recordAllDarwinByArticleCode(hl4Code, description);
	}

	@Override
	public List<DescriptionMaster> searchAllWeavedByArticleCode(Integer offset, Integer pagesize, String hl4code,
			String description) {
		return null;
	}

	@Override
	public Integer recordAllWeavedByArticleCode(String hl4Code, String description) {
		return piMapper.recordAllWeavedByArticleCode(hl4Code, description);
	}

	@Override
	public List<HLevelData> searchByDistinctHierarachyLevels(Integer offset, Integer pagesize, String hl1name,
			String hl2name, String hl3name) {
		return piMapper.searchByDistinctHierarachyLevels(offset, pagesize, hl1name, hl2name, hl3name);

	}

	@Override
	public Integer recordSearchByDistinctHierarachyLevels(String hl1name, String hl2name, String hl3name) {
		return piMapper.recordSearchByDistinctHierarachyLevels(hl1name, hl2name, hl3name);
	}

	@Override
	public Integer insertFinCharge(FinCharge charge) {
		return piMapper.createFinCharge(charge);
	}

	@Override
	public List<SupplierData> searchBySupplierData(Integer offset, Integer pagesize, String slCode, String slName,
			String slAddr, String department) {
		return piMapper.searchBySupplierData(offset, pagesize, slCode, slName, slAddr, department);
	}

	@Override
	public Integer recordsearchBySupplierData(String slCode, String slName, String slAddr, String departmentName) {
		return piMapper.recordsearchBySupplierData(slCode, slName, slAddr, departmentName);
	}

	@Override
	public List<SupplierData> searchSupplierData(String department, String siteCode, Integer offset, Integer pagesize,
			String search) {
		return piMapper.searchSupplierData(department, siteCode, offset, pagesize, search);
	}

	@Override
	public Integer recordSearchSupplierData(String department, String siteCode, String search) {
		return piMapper.recordSearchSupplierData(department, siteCode, search);

	}

	@Override
	public List<HLevelData> searchAllDistinctHierarachyLevels(Integer offset, Integer pagesize, String search) {
		return piMapper.searchAllDistinctHierarachyLevels(offset, pagesize, search);

	}

	@Override
	public Integer recordALLSearchDistinctHierarachyLevels(String search) {
		return piMapper.recordALLSearchDistinctHierarachyLevels(search);
	}

	@Override
	public String createPattern(String pattern) {

		char starting = 65;
		char ending = 90;

		int tempId = Integer.parseInt(pattern.substring(pattern.lastIndexOf("/") + 3)); // 0001
		String newId;
		if (tempId < 9) {
			newId = "000" + (++tempId);
		} else if (tempId < 99) {
			newId = "00" + (++tempId);
		} else if (tempId < 999) {
			newId = "0" + (++tempId);
		} else {
			newId = "" + (++tempId);
		}

		String newPattern = "S/PR-PID/";
		String charString = pattern.substring(pattern.lastIndexOf("/") + 1, pattern.lastIndexOf("/") + 3); // AA
		char firstChar = charString.charAt(0); // A
		char secondChar = charString.charAt(1); // B

		if (tempId <= 9999) {
			newPattern = newPattern + firstChar + secondChar + newId;
		} else {
			if (secondChar < ending) {
				newPattern = pattern + firstChar + (++secondChar) + newId;
			} else {
				secondChar = starting;
				newPattern = newPattern + (++firstChar) + secondChar + newId;
			}

		}

		return newPattern;
	}

	@Override
	public String getLastPattern() {
		return piMapper.getLastPattern();
	}

	@Override
	public PurchaseTermData getPurchaseTermDataBySlCode(Integer slCode) {
		return piMapper.getPurchaseTermDataBySlCode(slCode);
	}

	@Override
	public PurchaseIndent getPIByPattern(String pattern) {
		return piMapper.getPIByPattern(pattern);
	}

	public String getStringCatMaster(List<Category> catMaster) {
		String name = "";

		for (Category cat : catMaster) {
			name = name + ", " + cat.getCname();
		}
		return name.substring(1);
	}

	public String getStringRatioMaster(List<RatioList> catMaster) {
		String ratio = "";

		for (RatioList rat : catMaster) {
			ratio = ratio + ", " + rat.getRatio();
		}
		return ratio.substring(1);
	}

	@Override
	public String buildReport(PurchaseIndent pi, HttpServletRequest request) throws SupplyMintException {
		S3BucketInfo s3BucketInfo = null;
		String enterpriseName = null;
		String downloadPDF = "DOWNLOAD_PDF";

		String fileName = null;
		String filePath = null;
		File saveBucket = null;
		String extension = "pdf";
		String pattern;

		ClassLoader classloader = Thread.currentThread().getContextClassLoader();
		InputStream invoiceJRXMLStream = classloader.getResourceAsStream("jrxml/PI_Invoice_new.jrxml");

		// classloader.
//        invoiceParameters.put("imagesDir", request.getSession().getServletContext().getRealPath("/resources/images/logo.jpg"));

		if (!CodeUtils.isEmpty(request)) {
			enterpriseName = request.getAttribute("name").toString();
//			enterpriseName = "Admin";
		}

		LOGGER.debug("setting all parameters for invoice.....{}");

		List<JasperPrint> jasperPrintList = new ArrayList<JasperPrint>();
		/*
		 * Map<String, Object> invoiceParameters = new HashMap<String, Object>();
		 */
//		try {
//			logoImageURL = getImageURL(DOWNLOAD_LOGO);

//			for (int i = 0; i < pi.getPiDetails().size(); i++) {
//
//				if (pi.getPiDetails().get(i).getContainsImage().contains("true")) {
//					pi.getPiDetails().get(i).setImage(getImageURL(DOWNLOAD_ARTICLE));
//				}
//			}
//		}catch(NullPointerException ne) {
//		}
		JasperReport jasperFilePath = null;
		try {
			String token = request.getHeader("X-Auth-Token");

			JsonArray orgDetails = CodeUtils.decode(token).get("ORG_DETAILS").getAsJsonArray();

			JsonObject jNode = orgDetails.get(0).getAsJsonObject();
			String orgId = jNode.get("orgId").getAsString();

			jasperFilePath = JasperCompileManager.compileReport(invoiceJRXMLStream);

			List<PurchaseIndentDetail> purchaseLists = pi.getPiDetails();

			AtomicInteger count = new AtomicInteger(1);

			boolean isOne = false;
			for (PurchaseIndentDetail purchaseIndentDetail : purchaseLists) {
				boolean isStart = purchaseLists.indexOf(purchaseIndentDetail) == 0;
				boolean isEnd = purchaseLists.indexOf(purchaseIndentDetail) == purchaseLists.size() - 1;
				String logoImageURL;

				purchaseIndentDetail.setSizeName(getStringCatMaster(purchaseIndentDetail.getSizeList()));
				purchaseIndentDetail.setColorName(getStringCatMaster(purchaseIndentDetail.getColorList()));
				purchaseIndentDetail.setRatioName(getStringRatioMaster(purchaseIndentDetail.getRatioList()));

				Map<String, Object> invoiceParameters = new HashMap<String, Object>();
				if (isStart) {

					InputStream imageInput = null;
					try {
						logoImageURL = getImageURL(DOWNLOAD_LOGO, orgId);
						imageInput = new URL(logoImageURL).openStream();

					} catch (Exception ex) {
						LOGGER.info(ex.getMessage());
					}
					invoiceParameters.put("poAmount", String.valueOf(pi.getPoAmount()));
					invoiceParameters.put("image0", imageInput);
					invoiceParameters.put("enterpriseName", enterpriseName);
					LOGGER.info("created time in pdf :" + pi.getCreatedTime());
					invoiceParameters.put("date", pi.getCreatedTime().toString().substring(0, 10));
					invoiceParameters.put("vendor", pi.getSlName());
					invoiceParameters.put("division", pi.getHl1Name());
					invoiceParameters.put("section", pi.getHl2Name());
					invoiceParameters.put("department", pi.getHl3Name());
					invoiceParameters.put("transporter", pi.getTransporterName());
					invoiceParameters.put("articleCode", pi.getPiDetails().get(0).getHl4Code());
					invoiceParameters.put("articleName", pi.getPiDetails().get(0).getHl4Name());
					invoiceParameters.put("deliveryDate",
							!CodeUtils.isEmpty(pi.getPiDetails().get(0).getDeliveryDate())
									? pi.getPiDetails().get(0).getDeliveryDate().toString().substring(0, 10)
									: null);
					invoiceParameters.put("remarks", pi.getPiDetails().get(0).getRemarks());

				}
				isOne = true;
				invoiceParameters.putAll(
						this.genrateinvoiceParameters(purchaseIndentDetail, isStart, isEnd, isOne, count.get()));
				jasperPrintList
						.add(JasperFillManager.fillReport(jasperFilePath, invoiceParameters, new JREmptyDataSource()));

				count.getAndIncrement();
			}

			s3BucketInfo = s3BucketInfoService.getBucketByType(downloadPDF, orgId);

			if (!CodeUtils.isEmpty(s3BucketInfo)) {
				String s3BucketFilePath = s3BucketInfo.getBucketPath();
				pattern = pi.getPiDetails().get(0).getOrderDetailId().replaceAll("/", "_");
				fileName = pattern + separator + CodeUtils.________dateTimeFormat.format(new Date());

				String savedPath = s3BucketFilePath + delimeter + fileName + "." + extension;
				filePath = FileUtils.createFilePath(FileUtils.getPlatformBasedParentDir(), savedPath);
				saveBucket = new File(filePath);
				JRPdfExporter exporter = new JRPdfExporter();
				exporter.setExporterInput(SimpleExporterInput.getInstance(jasperPrintList)); // Set as export input my
																								// list with JasperPrint
																								// s
				exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(filePath));
				SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
				configuration.setCreatingBatchModeBookmarks(true); // add this so your bookmarks work, you may set other
																	// parameters
				exporter.setConfiguration(configuration);
				exporter.exportReport();

				// upload pdf on s3
				String etag = s3Wrapper.upload(saveBucket, savedPath, s3BucketInfo.getS3bucketName()).getETag();

				int updatePI = updatePIFilePath(pi.getPiDetails().get(0).getOrderDetailId(), savedPath);
				if (updatePI == 0) {
					filePath = null;
				}
				if (!CodeUtils.isEmpty(etag)) {
					LOGGER.info(etag.toString());
				}
			}

			return filePath;
		} catch (JRException ex) {
			ex.printStackTrace();
			throw new SupplyMintException(ex.getMessage());

		} catch (Exception e) {
			throw new SupplyMintException(e.getMessage());

		}
	}

	private Map<String, Object> genrateinvoiceParameters(PurchaseIndentDetail purchaseIndent, boolean isStart,
			boolean isEnd, boolean isOne, int count) {
		Map<String, Object> invoiceParameters = new HashMap<String, Object>();

		if (isStart)
			invoiceParameters.put("start", "start");
		if (isEnd)
			invoiceParameters.put("last", "last");
		if (isOne)
			invoiceParameters.put("one", "one");

		InputStream imageInput = null;
		try {
			if (purchaseIndent.getContainsImage().contains("true")) {
				// purchaseIndent.setImage(getImageURL(DOWNLOAD_ARTICLE));

				LOGGER.info("Image : " + purchaseIndent.getImage1());
				if (!CodeUtils.isEmpty(purchaseIndent.getImage1())) {
					imageInput = new URL(purchaseIndent.getImage1()).openStream();
					invoiceParameters.put("image1", imageInput);

					if (!CodeUtils.isEmpty(purchaseIndent.getImage2())) {
						imageInput = new URL(purchaseIndent.getImage2()).openStream();
						invoiceParameters.put("image2", imageInput);

						if (!CodeUtils.isEmpty(purchaseIndent.getImage3())) {
							imageInput = new URL(purchaseIndent.getImage3()).openStream();
							invoiceParameters.put("image3", imageInput);

							if (!CodeUtils.isEmpty(purchaseIndent.getImage4())) {
								imageInput = new URL(purchaseIndent.getImage4()).openStream();
								invoiceParameters.put("image4", imageInput);

								if (!CodeUtils.isEmpty(purchaseIndent.getImage5())) {
									imageInput = new URL(purchaseIndent.getImage5()).openStream();
									invoiceParameters.put("image5", imageInput);
								}
							}
						}
					}
				}
			}
		} catch (Exception ex) {
			LOGGER.info(ex.getMessage());
		}
//		invoiceParameters.put("image1", ClassLoader.getSystemResourceAsStream("Image/VMART_LOGO.png"));

		invoiceParameters.put("itemDetails", "" + count);
		invoiceParameters.put("cat1", purchaseIndent.getCat1Name());
		LOGGER.info("cat1 : " + purchaseIndent.getCat1Name());
		invoiceParameters.put("cat2", purchaseIndent.getCat2Name());
		invoiceParameters.put("cat3", purchaseIndent.getCat3Name());
		invoiceParameters.put("cat4", purchaseIndent.getCat4Name());
		invoiceParameters.put("desc1", purchaseIndent.getDesign());
		invoiceParameters.put("desc2", purchaseIndent.getDesc2Name());
		invoiceParameters.put("desc3", purchaseIndent.getDesc3Name());
		invoiceParameters.put("desc5", purchaseIndent.getDesc5Name());
		invoiceParameters.put("desc6", String.valueOf(purchaseIndent.getMrp()));
		invoiceParameters.put("noSet", purchaseIndent.getNoOfSets());
		invoiceParameters.put("gst", purchaseIndent.getGst());
		invoiceParameters.put("ppQyt", String.valueOf(purchaseIndent.getQuantity()));
		invoiceParameters.put("act", purchaseIndent.getIntakeMargin());
		invoiceParameters.put("netAmt", String.valueOf(purchaseIndent.getNetAmountTotal()));
		invoiceParameters.put("rate", String.valueOf(purchaseIndent.getRate()));
		invoiceParameters.put("color", purchaseIndent.getColorName());
		invoiceParameters.put("size", purchaseIndent.getSizeName());
		invoiceParameters.put("ratio", purchaseIndent.getRatioName());

		DecimalFormat format = new DecimalFormat("##.00");
		invoiceParameters.put("total1", String.valueOf(format.format(purchaseIndent.getNetAmountTotal())));
		return invoiceParameters;
	}

	/*
	 * public List<PurchaseIndentRepeatedData>
	 * getRepeatedItems(List<PurchaseIndentDetail> piDetail) {
	 * 
	 * List<PurchaseIndentRepeatedData> pirData = new
	 * ArrayList<PurchaseIndentRepeatedData>(); LOGGER.info("PIDETAIL size :" +
	 * piDetail.size()); for (int i = 0; i < piDetail.size(); i++) {
	 * PurchaseIndentRepeatedData repeatedData = new PurchaseIndentRepeatedData();
	 * repeatedData.setItemDetails(i + 1);
	 * repeatedData.setCat1(piDetail.get(i).getUniqueName());
	 * repeatedData.setCat2(piDetail.get(i).getBrandName());
	 * repeatedData.setCat3(piDetail.get(i).getPatternName());
	 * repeatedData.setCat4(piDetail.get(i).getAssortmentName());
	 * repeatedData.setUDF("UDF");
	 * repeatedData.setDesc1(piDetail.get(i).getDesign());
	 * repeatedData.setDesc2(piDetail.get(i).getSeasonName());
	 * repeatedData.setDesc3(piDetail.get(i).getFabricName());
	 * repeatedData.setDesc4(piDetail.get(i).getDarwinName());
	 * repeatedData.setDesc5(piDetail.get(i).getWeavedName());
	 * repeatedData.setDesc6(String.valueOf(piDetail.get(i).getCostPrice()));
	 * repeatedData.setNoSet(piDetail.get(i).getNoOfSets());
	 * repeatedData.setGst(piDetail.get(i).getGst());
	 * repeatedData.setPpQyt(String.valueOf(piDetail.get(i).getQuantity()));
	 * repeatedData.setRate(String.valueOf(piDetail.get(i).getRate()));
	 * repeatedData.setnetAmt(String.valueOf(piDetail.get(i).getNetAmountTotal()));
	 * repeatedData.setAct(piDetail.get(i).getIntakeMargin());
	 * repeatedData.setColor(piDetail.get(i).getColorName());
	 * repeatedData.setSize(piDetail.get(i).getSizeName());
	 * repeatedData.settotal1(String.valueOf(piDetail.get(0).getNetAmountTotal()));
	 * 
	 * pirData.add(repeatedData); } return pirData; }
	 */
	public String getImageURL(String type, String orgId) {
		String url;
		S3BucketInfo s3BucketInfo = null;
		if (type.contains(DOWNLOAD_LOGO)) {
			s3BucketInfo = s3BucketInfoService.getBucketByType(DOWNLOAD_LOGO, orgId);
		} else {
			if (type.contains(DOWNLOAD_ARTICLE)) {
				s3BucketInfo = s3BucketInfoService.getBucketByType(DOWNLOAD_ARTICLE, orgId);
			}
		}

		if (!CodeUtils.isEmpty(s3BucketInfo.getBucketPath())) {
			url = s3Wrapper.downloadPreSignedURLOnHistory(s3BucketInfo.getS3bucketName(), s3BucketInfo.getBucketKey(),
					Integer.parseInt(s3BucketInfo.getTime()));

		} else {
			url = null;

		}
		return url;
	}

	public Integer updatePIFilePath(String pattern, String path) {
		Integer updateInt = piMapper.updatePDFPath(pattern, path);
		return updateInt;
	}

	@Override
	public String getPDFPathByPattern(String pattern) {

		String pdfPath = piMapper.getPDFPath(pattern);
		return pdfPath;
	}

	@Override
	public Integer updateStatus(String status, String pattern, String ipAddress, OffsetDateTime updationTime,
			String updatedBy) {
		return piMapper.updateStatus(status, pattern, ipAddress, updationTime, updatedBy);
	}

	/*
	 * @Override public int countColor(CatMaster catMaster) { return
	 * piMapper.countColor(catMaster); }
	 */
	/*
	 * @Override public int countSize(CatMaster catMaster) { return
	 * piMapper.countSize(catMaster); }
	 */
	@Override
	public int countBrand(CatMaster catMaster) {
		return piMapper.countBrand(catMaster);
	}

	@Override
	public int countUniqueCode(CatMaster catMaster) {
		return piMapper.countUniqueCode(catMaster);
	}

	@Override
	public int insertColor(CatMaster catMaster) {
		return piMapper.insertColor(catMaster);
	}

	@Override
	public int insertBrand(CatMaster catMaster) {
		return piMapper.insertBrand(catMaster);
	}

	@Override
	public int insertSize(CatMaster catMaster) {
		return piMapper.insertSize(catMaster);
	}

	@Override
	public int insertUniqueCode(CatMaster catMaster) {
		return piMapper.insertUniqueCode(catMaster);
	}

	@Override
	public List<PurchaseIndent> filter(Integer offset, Integer pagesize, String pattern, String supplierName,
			String articleCode, String division, String status, String section, String department,
			String generatedDate) {
		return piMapper.filter(offset, pagesize, pattern, supplierName, articleCode, division, status, section,
				department, generatedDate);

	}

	@Override
	public Integer filterRecord(String pattern, String supplierName, String articleCode, String division, String status,
			String section, String department, String generatedDate) {
		return piMapper.filterRecord(pattern, supplierName, articleCode, division, status, section, department,
				generatedDate);

	}

	@Override
	public List<PurchaseIndent> search(Integer offset, Integer pagesize, String search) {
		return piMapper.search(offset, pagesize, search);

	}

	@Override
	public Integer searchRecord(String search) {

		return piMapper.searchRecord(search);
	}

	@Override
	public List<Transporters> getAllTransporters(Integer offset, Integer pagesize) {
		return piMapper.getAllTransporters(offset, pagesize);
	}

	@Override
	public Integer getAllTransportersRecord() {
		return piMapper.getAllTransportersRecord();
	}

	@Override
	public List<Transporters> getAllFilterTransporters(Integer offset, Integer pagesize, String transporterName,
			String transporterCode) {
		return piMapper.getAllFilterTransporters(offset, pagesize, transporterName, transporterCode);

	}

	@Override
	public Integer getAllFilterTransportersRecord(String transporterName, String transporterCode) {
		return piMapper.getAllFilterTransportersRecord(transporterName, transporterCode);
	}

	@Override
	public List<Transporters> getAllSearchTransporters(Integer offset, Integer pagesize, String search) {

		return piMapper.getAllSearchTransporters(offset, pagesize, search);
	}

	@Override
	public Integer getAllSearchTransportersRecord(String search) {
		return piMapper.getAllSearchTransportersRecord(search);

	}

	@Override
	public List<PurtermMaster> getAllPurchaseTerms() {

		return piMapper.getAllPurchaseTerms();
	}

	@Override
	public PurchaseIndentProperty getPIProperty(int id) {
		return piMapper.getPIMainProperty(id);
	}

	@Override
	public ObjectNode getIgstChargeRateNode(String taxName, String gstSlab, String slabEffectiveDate, Double igstRate,
			Double cessRate) {

		ObjectNode rateNode = getMapper().createObjectNode();
		rateNode.put("taxName", taxName);
		rateNode.put("gstSlab", gstSlab);
		rateNode.put("effectiveDate", slabEffectiveDate);
		rateNode.put("igstRate", igstRate);
		rateNode.put("cessRate", cessRate);

		return rateNode;
	}

	@Override
	public ObjectNode getCgstChargeRateNode(String taxName, String gstSlab, String slabEffectiveDate, Double cgstRate,
			Double sgstRate, Double cessRate) {

		ObjectNode rateNode = getMapper().createObjectNode();
		rateNode.put("taxName", taxName);
		rateNode.put("gstSlab", gstSlab);
		rateNode.put("effectiveDate", slabEffectiveDate);
		rateNode.put("cgstRate", cgstRate);
		rateNode.put("sgstRate", sgstRate);
		rateNode.put("cessRate", cessRate);

		return rateNode;
	}

	@Override
	public int getExistanceUniqueCodeByArticleCode(String hl3Code, String categoryName) {
		return piMapper.getExistanceUniqueCodeByArticleCode(hl3Code, categoryName);
	}

	@Override
	public int getExistanceBrandsByArticleCode(String hl3Code, String categoryName) {
		return piMapper.getExistanceBrandsByArticleCode(hl3Code, categoryName);
	}

	@Override
	public int getExistancePatternsByArticleCode(String hl3Code, String categoryName) {
		return piMapper.getExistancePatternsByArticleCode(hl3Code, categoryName);
	}

	@Override
	public int getExistanceAssortmentsByArticleCode(String hl3Code, String categoryName) {
		return piMapper.getExistanceAssortmentsByArticleCode(hl3Code, categoryName);
	}

	@Override
	public int getExistanceSeasonByArticleCode(String hl3Code, String description) {
		return piMapper.getExistanceSeasonByArticleCode(hl3Code, description);

	}

	@Override

	public int getExistanceFabricByArticleCode(String hl3Code, String description) {
		return piMapper.getExistanceFabricByArticleCode(hl3Code, description);

	}

	@Override

	public int getExistanceDarwinByArticleCode(String hl3Code, String description) {
		return piMapper.getExistanceDarwinByArticleCode(hl3Code, description);
	}

	@Override
	public int getExistanceWeavedByArticleCode(String hl3Code, String description) {
		return piMapper.getExistanceWeavedByArticleCode(hl3Code, description);
	}

	@Override
	public int getExistanceSizeByArticleCode(String hl3name, String categoryName) {
		return piMapper.getExistanceSizeByArticleCode(hl3name, categoryName);
	}

	@Override
	public int getExistanceColorByArticleCode(String hl3Code, String categoryName) {
		return piMapper.getExistanceColorByArticleCode(hl3Code, categoryName);
	}

	@Override
	public int getExistanceHierarachyLevels(String hl1name, String hl2name, String hl3name) {
		return piMapper.getExistanceHierarachyLevels(hl1name, hl2name, hl3name);
	}

	@Override
	public String marginRule(String slCode, String articleCode, String tenantHashKey, String siteCode) {
		if (tenantHashKey.equalsIgnoreCase(TenantHash.valueOf("VMART").getHashKey()))
			return piMapper.marginRuleValue(slCode, articleCode, siteCode);
		else
			return piMapper.marginRule(slCode, articleCode);

	}

	@Override
	public String getActualOtbData(String articleCode, String mrp, String month, String year) {
		return piMapper.getActualOtbData(articleCode, mrp, month, year);
	}

	@SuppressWarnings("deprecation")
	@Override
	public ObjectNode getItemCatDescData(Integer pageNo, String hl3Code, String catDescType, String hl3Name,
			String tenantHashKey) {
		List<CatDescMaster> data = null;
		int totalRecord = 0;
		int previousPage = 0;
		int pageSize = 10;
		int maxPage = 0;
		Integer offset = 0;
		Integer pagesize = 10;
		ArrayNode arrNode = getMapper().createArrayNode();
		ObjectNode objectNode = getMapper().createObjectNode();
		try {
			String categoryDescType = catDescType;
			String tableName = "PRPO_" + catDescType.toUpperCase();
			if (tenantHashKey.equalsIgnoreCase(TenantHash.valueOf("VMART").getHashKey())) {
				if (catDescType.equalsIgnoreCase("COLOR")) {
					catDescType = PICarDesType.CAT6.toString();
				} else if (catDescType.equalsIgnoreCase("SIZE")) {
					catDescType = PICarDesType.CAT5.toString();
				}
				if (catDescType.toLowerCase().contains(PICarDesType.CAT.toString().toLowerCase())) {
					if (catDescType.equalsIgnoreCase(PICarDesType.CAT1.toString())) {
						totalRecord = piMapper.getRecordAllUniqueCodeByArticleCode(hl3Code);
						maxPage = (totalRecord + pageSize - 1) / pageSize;
						previousPage = pageNo - 1;
						offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
						data = piMapper.getAllUniqueCodeByArticleCode(offset, pagesize, hl3Code);
					} else if (catDescType.equalsIgnoreCase(PICarDesType.CAT2.toString())) {
						totalRecord = piMapper.getRecordAllBrandsByArticleCode(hl3Code);
						maxPage = (totalRecord + pageSize - 1) / pageSize;
						previousPage = pageNo - 1;
						offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
						data = piMapper.getAllBrandsByArticleCode(offset, pagesize, hl3Code);
					} else if (catDescType.equalsIgnoreCase(PICarDesType.CAT3.toString())) {
						totalRecord = piMapper.getRecordAllPatternsByArticleCode(hl3Code);
						maxPage = (totalRecord + pageSize - 1) / pageSize;
						previousPage = pageNo - 1;
						offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
						data = piMapper.getAllPatternsByArticleCode(offset, pagesize, hl3Code);
					} else if (catDescType.equalsIgnoreCase(PICarDesType.CAT4.toString())) {
						totalRecord = piMapper.getRecordAllAssortmentsByArticleCode(hl3Code);
						maxPage = (totalRecord + pageSize - 1) / pageSize;
						previousPage = pageNo - 1;
						offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
						data = piMapper.getAllAssortmentsByArticleCode(offset, pagesize, hl3Code);
					} else if (catDescType.equalsIgnoreCase(PICarDesType.CAT6.toString())) {
						totalRecord = piMapper.countColor(hl3Code);
						maxPage = (totalRecord + pageSize - 1) / pageSize;
						previousPage = pageNo - 1;
						offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
						data = piMapper.getAllColors(offset, pagesize, hl3Code);
					} else if (catDescType.equalsIgnoreCase(PICarDesType.CAT5.toString())) {
						totalRecord = this.countAllSizes(tenantHashKey, hl3Code, hl3Name);
						maxPage = (totalRecord + pageSize - 1) / pageSize;
						previousPage = pageNo - 1;
						offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
						data = this.getAllSizes(tenantHashKey, hl3Code, hl3Name, offset, pagesize);
					}
				} else if (catDescType.toLowerCase().contains(PICarDesType.DESC.toString().toLowerCase())) {
					if (catDescType.equalsIgnoreCase(PICarDesType.DESC2.toString())) {
						totalRecord = piMapper.getRecordAllSeasonByArticleCode(hl3Code);
						maxPage = (totalRecord + pageSize - 1) / pageSize;
						previousPage = pageNo - 1;
						offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
						data = piMapper.getAllSeasonByArticleCode(offset, pagesize, hl3Code);
					} else if (catDescType.equalsIgnoreCase(PICarDesType.DESC3.toString())) {
						totalRecord = piMapper.getRecordAllFabricByArticleCode(hl3Code);
						maxPage = (totalRecord + pageSize - 1) / pageSize;
						previousPage = pageNo - 1;
						offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
						data = piMapper.getAllFabricByArticleCode(offset, pagesize, hl3Code);
					} else if (catDescType.equalsIgnoreCase(PICarDesType.DESC4.toString())) {
						totalRecord = piMapper.getRecordAllDarwinByArticleCode(hl3Code);
						maxPage = (totalRecord + pageSize - 1) / pageSize;
						previousPage = pageNo - 1;
						offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
						data = piMapper.getAllDarwinByArticleCode(offset, pagesize, hl3Code);
					} else if (catDescType.equalsIgnoreCase(PICarDesType.DESC5.toString())) {
						totalRecord = piMapper.getRecordAllWeavedByArticleCode(hl3Code);
						maxPage = (totalRecord + pageSize - 1) / pageSize;
						previousPage = pageNo - 1;
						offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
						data = piMapper.getAllWeavedByArticleCode(offset, pagesize, hl3Code);
					}
				}
			} else {
//				@Author Prabhakar Srivastava
//				Generics cat desc mapping code start...

				if (catDescType.equalsIgnoreCase("COLOR") || catDescType.equalsIgnoreCase("SIZE")) {
					List<CatDescMapping> descMappings = piMapper.getAllCatDescMapping();
					if (!CodeUtils.isEmpty(descMappings)) {
						for (int i = 0; i < descMappings.size(); i++) {
							if (descMappings.get(i).getSupplymintName().equalsIgnoreCase(catDescType)) {
								catDescType = descMappings.get(i).getClientKey();
								tableName = "PRPO_" + catDescType.toUpperCase();
							}
						}
					}
				}
				if (catDescType.toLowerCase().contains(PICarDesType.CAT.toString().toLowerCase())) {
					totalRecord = piMapper.getAllRecordByCategaory(hl3Code, tableName);
					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
					data = piMapper.getAllByCategaory(offset, pagesize, hl3Code, tableName);
				} else if (catDescType.toLowerCase().contains(PICarDesType.DESC.toString().toLowerCase())) {
					totalRecord = piMapper.getAllRecordByDescription(hl3Code, tableName);
					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
					data = piMapper.getAllByDescription(offset, pagesize, hl3Code, tableName);
				}

//				---end
			}
			if (data.size() > 0 && !data.isEmpty()) {
				data.stream().forEach(e -> {
					ObjectNode tempNode = getMapper().createObjectNode();
					tempNode.put("code", e.getCode());
					if (categoryDescType.toLowerCase().contains(PICarDesType.CAT.toString().toLowerCase())
							|| categoryDescType.toLowerCase().equalsIgnoreCase("COLOR")
							|| categoryDescType.toLowerCase().equalsIgnoreCase("SIZE")) {
						tempNode.put("cname", e.getCname());
					} else if (categoryDescType.toLowerCase().contains(PICarDesType.DESC.toString().toLowerCase())
							|| categoryDescType.toLowerCase().equalsIgnoreCase("COLOR")
							|| categoryDescType.toLowerCase().equalsIgnoreCase("SIZE")) {
						tempNode.put("cname", e.getDescription());
					}
					tempNode.put("hl3Name", hl3Name);
					// tempNode.put("desc", e.getDescription());
					arrNode.add(tempNode);
				});
				objectNode.put(Pagination.CURRENT_PAGE, pageNo);
				objectNode.put(Pagination.PREVIOUS_PAGE, previousPage);
				objectNode.put(Pagination.MAXIMUM_PAGE, maxPage);
				objectNode.put(CodeUtils.RESPONSE, arrNode);
			}

		} catch (MyBatisSystemException ex) {
			throw ex;
		} catch (Exception ex) {
			try {
				throw ex;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return objectNode;
	}

	int countAllSizes(String tenantHashKey, String hl3Code, String hl3Name) {
		if (tenantHashKey.equalsIgnoreCase(TenantHash.valueOf("VMART").getHashKey()))
			return piMapper.getRecordAllSizeByArticleCode(hl3Code);
		else
			return piMapper.getOtherRecordAllSizeByArticleCode(hl3Name);
	}

	List<CatDescMaster> getAllSizes(String tenantHashKey, String hl3Code, String hl3Name, int offset, int pagesize) {
		if (tenantHashKey.equalsIgnoreCase(TenantHash.valueOf("VMART").getHashKey()))
			return piMapper.getAllSizeByArticleCode(offset, pagesize, hl3Code);
		else
			return piMapper.getOtherAllSizeByArticleCode(offset, pagesize, hl3Name);
	}

	@SuppressWarnings("deprecation")
	@Override
	public ObjectNode searchItemCatDescData(Integer pageNo, String hl3Code, String catDescType, String hl3Name,
			String search, String tenantHashKey) {
		List<CatDescMaster> data = null;
		int totalRecord = 0;
		int previousPage = 0;
		int pageSize = 10;
		int maxPage = 0;
		Integer offset = 0;
		Integer pagesize = 10;
		ArrayNode arrNode = getMapper().createArrayNode();
		ObjectNode objectNode = getMapper().createObjectNode();
		try {
			String categoryDescType = catDescType;
			String tableName = "PRPO_" + catDescType.toUpperCase();
			if (tenantHashKey.equalsIgnoreCase(TenantHash.valueOf("VMART").getHashKey())) {
				if (catDescType.toLowerCase().contains(PICarDesType.CAT.toString().toLowerCase())) {
					if (catDescType.equalsIgnoreCase(PICarDesType.CAT1.toString())) {
						totalRecord = piMapper.recordAllUniqueCodeByArticleCode(hl3Code, search);
						maxPage = (totalRecord + pageSize - 1) / pageSize;
						previousPage = pageNo - 1;
						offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
						data = piMapper.searchAllUniqueCodeByArticleCode(offset, pagesize, hl3Code, search);
					} else if (catDescType.equalsIgnoreCase(PICarDesType.CAT2.toString())) {
						totalRecord = piMapper.recordAllBrandsByArticleCode(hl3Code, search);
						maxPage = (totalRecord + pageSize - 1) / pageSize;
						previousPage = pageNo - 1;
						offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
						data = piMapper.searchAllBrandsByArticleCode(offset, pagesize, hl3Code, search);
					} else if (catDescType.equalsIgnoreCase(PICarDesType.CAT3.toString())) {
						totalRecord = piMapper.recordAllPatternsByArticleCode(hl3Code, search);
						maxPage = (totalRecord + pageSize - 1) / pageSize;
						previousPage = pageNo - 1;
						offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
						data = piMapper.searchAllPatternsByArticleCode(offset, pagesize, hl3Code, search);
					} else if (catDescType.equalsIgnoreCase(PICarDesType.CAT4.toString())) {
						totalRecord = piMapper.recordAllAssortmentsByArticleCode(hl3Code, search);
						maxPage = (totalRecord + pageSize - 1) / pageSize;
						previousPage = pageNo - 1;
						offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
						data = piMapper.searchAllAssortmentsByArticleCode(offset, pagesize, hl3Code, search);
					} else if (catDescType.equalsIgnoreCase(PICarDesType.CAT6.toString())) {
						totalRecord = piMapper.recordAllColors(hl3Code, search);
						maxPage = (totalRecord + pageSize - 1) / pageSize;
						previousPage = pageNo - 1;
						offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
						data = piMapper.searchAllColors(offset, pagesize, hl3Code, search);
					} else if (catDescType.equalsIgnoreCase(PICarDesType.CAT5.toString())) {
						totalRecord = piMapper.recordAllSizeByArticleCode(hl3Name, search);
						maxPage = (totalRecord + pageSize - 1) / pageSize;
						previousPage = pageNo - 1;
						offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
						data = piMapper.searchAllSizeByArticleCode(offset, pagesize, hl3Name, search);
					}
				} else if (catDescType.toLowerCase().contains(PICarDesType.DESC.toString().toLowerCase())) {
					if (catDescType.equalsIgnoreCase(PICarDesType.DESC2.toString())) {

						totalRecord = piMapper.recordAllSeasonByArticleCode(hl3Code, search);
						maxPage = (totalRecord + pageSize - 1) / pageSize;
						previousPage = pageNo - 1;
						offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
						data = piMapper.searchAllSeasonByArticleCode(offset, pagesize, hl3Code, search);
					} else if (catDescType.equalsIgnoreCase(PICarDesType.DESC3.toString())) {
						totalRecord = piMapper.recordAllFabricByArticleCode(hl3Code, search);
						maxPage = (totalRecord + pageSize - 1) / pageSize;
						previousPage = pageNo - 1;
						offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
						data = piMapper.searchAllFabricByArticleCode(offset, pagesize, hl3Code, search);
					} else if (catDescType.equalsIgnoreCase(PICarDesType.DESC4.toString())) {
						totalRecord = piMapper.recordAllDarwinByArticleCode(hl3Code, search);
						maxPage = (totalRecord + pageSize - 1) / pageSize;
						previousPage = pageNo - 1;
						offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
						data = piMapper.searchAllDarwinByArticleCode(offset, pagesize, hl3Code, search);
					} else if (catDescType.equalsIgnoreCase(PICarDesType.DESC5.toString())) {
						totalRecord = piMapper.recordAllWeavedByArticleCode(hl3Code, search);
						maxPage = (totalRecord + pageSize - 1) / pageSize;
						previousPage = pageNo - 1;
						offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
						data = piMapper.searchAllWeavedByArticleCode(offset, pagesize, hl3Code, search);
					}
				}
			} else {
//				@Author Prabhakar Srivastava 
//				generics cat desc mapping code start...

				if (catDescType.equalsIgnoreCase("COLOR") || catDescType.equalsIgnoreCase("SIZE")) {
					List<CatDescMapping> descMappings = piMapper.getAllCatDescMapping();
					if (!CodeUtils.isEmpty(descMappings)) {
						for (int i = 0; i < descMappings.size(); i++) {
							if (descMappings.get(i).getSupplymintName().equalsIgnoreCase(catDescType)) {
								catDescType = descMappings.get(i).getClientKey();
								tableName = "PRPO_" + catDescType.toUpperCase();
							}
						}
					}
				}
				if (catDescType.toLowerCase().contains(PICarDesType.CAT.toString().toLowerCase())) {
					totalRecord = piMapper.searchAllRecordByCategaory(hl3Code, search, tableName);
					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
					data = piMapper.searchAllByCategaory(offset, pagesize, hl3Code, search, tableName);
				} else if (catDescType.toLowerCase().contains(PICarDesType.DESC.toString().toLowerCase())) {
					totalRecord = piMapper.searchAllRecordByDescription(hl3Code, search, tableName);
					maxPage = (totalRecord + pageSize - 1) / pageSize;
					previousPage = pageNo - 1;
					offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
					data = piMapper.searchAllByDescription(offset, pagesize, hl3Code, search, tableName);
				}

//				---end
			}
			if (data.size() > 0 && !data.isEmpty()) {
				data.stream().forEach(e -> {
					ObjectNode tempNode = getMapper().createObjectNode();
					tempNode.put("code", e.getCode());
					if (categoryDescType.toLowerCase().contains(PICarDesType.CAT.toString().toLowerCase())
							|| categoryDescType.toLowerCase().equalsIgnoreCase("COLOR")
							|| categoryDescType.toLowerCase().equalsIgnoreCase("SIZE")) {
						tempNode.put("cname", e.getCname());
					} else if (categoryDescType.toLowerCase().contains(PICarDesType.DESC.toString().toLowerCase())
							|| categoryDescType.toLowerCase().equalsIgnoreCase("COLOR")
							|| categoryDescType.toLowerCase().equalsIgnoreCase("SIZE")) {
						tempNode.put("cname", e.getDescription());
					}
					tempNode.put("hl3Name", e.getHl3Name());
					// tempNode.put("desc", e.getDescription());
					arrNode.add(tempNode);
				});
				objectNode.put(Pagination.CURRENT_PAGE, pageNo);
				objectNode.put(Pagination.PREVIOUS_PAGE, previousPage);
				objectNode.put(Pagination.MAXIMUM_PAGE, maxPage);
				objectNode.put(CodeUtils.RESPONSE, arrNode);
			}
		} catch (MyBatisSystemException ex) {
			ex.printStackTrace();
			throw ex;
		} catch (Exception ex) {
			throw ex;
		}
		return objectNode;
	}

	@Override
	public List<DeptItemUDFSettings> getCatDescHeader(String hl3code) {
		return piMapper.getCatDescByHl3Name(hl3code);
	}

	@Override
	public ObjectNode getCatDescHeaderNode(List<DeptItemUDFSettings> dept) {
		ObjectNode node = getMapper().createObjectNode();
		ArrayNode arrNode = getMapper().createArrayNode();

		ArrayNode categoriesData = getMapper().createArrayNode();
		ArrayNode udfData = getMapper().createArrayNode();

		dept.stream().forEach(e -> {
			ObjectNode tempNode = getMapper().createObjectNode();
			tempNode.put(PurchaseIndentMetaData.DeptItemUDFSettingsMeta.CATDESC, e.getCat_desc_udf());
			tempNode.put(PurchaseIndentMetaData.DeptItemUDFSettingsMeta.DISPLAY_NAME, e.getDisplayName());
			tempNode.put(PurchaseIndentMetaData.DeptItemUDFSettingsMeta.IS_COMPULSORY, e.getIsCompulsory());
			tempNode.put(PurchaseIndentMetaData.DeptItemUDFSettingsMeta.IS_LOV, e.getIsLov());

			categoriesData.add(tempNode);

		});

		ObjectNode mainNode = getMapper().createObjectNode();
		mainNode.put(PurchaseIndentMetaData.DeptItemUDFSettingsMeta.CATEGORIES, categoriesData);
		node.put(CodeUtils.RESPONSE, categoriesData);

		return node;
	}

	@Override
	public PurchaseIndentMaster getPurchaseIndentMaster(PurchaseIndentMaster pim) {
		PurchaseIndentMaster purchaseIndentMaster = new PurchaseIndentMaster();
		PurchaseIndent pi = new PurchaseIndent();
		pi.setHl1Code(pim.getPiKey().getHl1Code());
		pi.setHl1Name(pim.getPiKey().getHl1Name());
		pi.setHl2Code(pim.getPiKey().getHl2Code());
		pi.setHl2Name(pim.getPiKey().getHl2Name());
		pi.setHl3Code(pim.getPiKey().getHl3Code());
		pi.setHl3Name(pim.getPiKey().getHl3Name());
		pi.setSlCode(pim.getPiKey().getSlCode());
		pi.setSlAddr(pim.getPiKey().getSlAddr());
		pi.setSlName(pim.getPiKey().getSlName());
		pi.setLeadTime(pim.getPiKey().getLeadTime());
		pi.setTermCode(pim.getPiKey().getTermCode());
		pi.setTermName(pim.getPiKey().getTermName());
		pi.setPoQuantity(pim.getPiKey().getPoQuantity());
		pi.setPoAmount(pim.getPiKey().getPoAmount());
		pi.setStateCode(pim.getPiKey().getStateCode());
		pi.setPiDetails(new ArrayList<PurchaseIndentDetail>());
		purchaseIndentMaster.setPiKey(pi);

		return purchaseIndentMaster;
	}

	@Override
	public List<PurchaseIndentHistory> getIndentHistory(Integer offset, Integer pagesize, String orgId) {

		return piMapper.getPIHistory(offset, pagesize, orgId);
	}

	@Override
	public Integer countIndentHistoryFilter(String pattern, String supplierName, String articleCode, String division,
			String status, String section, String department, String generatedDate, String slCityName,
			String deliveryDateFrom, String deliveryDateTo, String desc2Code, String orgId) {
		return piMapper.countIndentHistoryFilter(pattern, supplierName, articleCode, division, status, section,
				department, generatedDate, slCityName, desc2Code, deliveryDateFrom, deliveryDateTo, orgId);
	}

	@Override
	public List<PurchaseIndentHistory> getIndentHistoryFilter(Integer offset, Integer pagesize, String pattern,
			String supplierName, String articleCode, String division, String status, String section, String department,
			String generatedDate, String slCityName, String deliveryDateFrom, String deliveryDateTo, String desc2Code,
			String orgId) {
		return piMapper.getIndentHistoryFilter(offset, pagesize, pattern, supplierName, articleCode, division, status,
				section, department, generatedDate, slCityName, desc2Code, deliveryDateFrom, deliveryDateTo, orgId);

	}

	@Override
	public Integer countIndentHistorySearch(String search, String orgId) {
		return piMapper.countIndentHistorySearch(search, orgId);
	}

	@Override
	public List<PurchaseIndentHistory> getIndentHistorySearch(Integer offset, Integer pagesize, String search,
			String orgId) {

		return piMapper.getIndentHistorySearch(offset, pagesize, search, orgId);
	}

	@Override
	public PurchaseIndent setPurchaseIndent(PurchaseIndent pi) {

		PurchaseIndent newPI = new PurchaseIndent();
		newPI.setSlName(pi.getSlName());
		newPI.setHl1Name(pi.getHl1Name());
		newPI.setHl2Name(pi.getHl2Name());
		newPI.setHl3Name(pi.getHl3Name());
		newPI.setTransporterName(pi.getTransporterName());

		newPI.setPiDetails(new ArrayList<PurchaseIndentDetail>());

		return newPI;
	}

	@Override
	public boolean isPIQuanityValues(ObjectNode node) {

		boolean isRatio = true;
		boolean isNumOfColors = true;
		boolean isNumOfSets = true;

		if (!node.isNull()) {
			String[] ratio = node.get("ratio").asText().split(",");

			isRatio = !CodeUtils.isEmpty(ratio);

			Integer numOfColors = Integer.parseInt(node.get("numOfColors").asText());

			isNumOfColors = !CodeUtils.isEmpty(numOfColors) && numOfColors != 0;

			Integer numOfSets = Integer.parseInt(node.get("numOfSets").asText());

			isNumOfSets = !CodeUtils.isEmpty(numOfSets) && numOfSets != 0;

			if (isRatio && isNumOfColors && isNumOfSets) {
				return true;
			} else {
				return false;
			}

		} else {
			return false;
		}
	}

	@Override
	public PurchaseIndent getPIMain(int orderId) {
		return piMapper.getPurchaseIndentMain(orderId);
	}

	@Override
	public List<PurchaseIndentDetail> getPIDetail(int orderId, String orderDetailId) {
		return piMapper.getPurchaseIndentDetail(orderId, orderDetailId);
	}

	@Override
	public Integer countHSNCode() {
		return piMapper.countHSNCode();
	}

	@Override
	public List<HsnData> getHSNCode(Integer offset, Integer pagesize) {
		return piMapper.getHSNCode(offset, pagesize);
	}

	@Override
	public Integer countHSNCodeSearch(String search) {
		return piMapper.countHSNCodeSearch(search);
	}

	@Override
	public List<HsnData> getHSNCodeSearch(Integer offset, Integer pagesize, String search) {
		return piMapper.getHSNCodeSearch(offset, pagesize, search);
	}

	@Override
	public String getUDFExistenceStatus(String orgId) {
		return piMapper.getUDFStatus(orgId);
	}

	@Override
	public List<SupplierData> getCustomSupplierData(String department, Integer offset, Integer pagesize) {

		List<SupplierData> suppliers = null;
		suppliers = piMapper.getCustomSupplierData(department, offset, pagesize);
		if (CodeUtils.isEmpty(suppliers)) {
			suppliers = piMapper.getAllCustomSupplierData(offset, pagesize);
		}
		return suppliers;
	}

	@Override
	public Integer getRecordCustomSupplierData(String department) {
		Integer supplierCount = piMapper.getRecordCustomSupplierData(department);
		if (supplierCount == 0) {
			supplierCount = piMapper.getAllRecordCustomSupplierData();
		}
		return supplierCount;
	}

	@Override
	public List<SupplierData> searchByCustomSupplierData(Integer offset, Integer pagesize, String slCode, String slName,
			String slAddr, String department) {
		List<SupplierData> suppliers = null;
		suppliers = piMapper.searchByCustomSupplierData(offset, pagesize, slCode, slName, slAddr, department);
		if (CodeUtils.isEmpty(suppliers)) {
			suppliers = piMapper.searchAllByCustomSupplierData(offset, pagesize, slCode, slName, slAddr);
		}
		return suppliers;
	}

	@Override
	public Integer recordsearchByCustomSupplierData(String slCode, String slName, String slAddr, String department) {

		Integer supplierCount = 0;
		supplierCount = piMapper.recordsearchByCustomSupplierData(slCode, slName, slAddr, department);
		if (supplierCount == 0) {
			supplierCount = piMapper.recordsearchAllByCustomSupplierData(slCode, slName, slAddr);
		}
		return supplierCount;
	}

	@Override
	public List<SupplierData> searchCustomSupplierData(String department, Integer offset, Integer pagesize,
			String search) {

		List<SupplierData> suppliers = null;
		suppliers = piMapper.searchCustomSupplierData(department, offset, pagesize, search);
		if (CodeUtils.isEmpty(suppliers)) {
			suppliers = piMapper.searchAllCustomSupplierData(offset, pagesize, search);
		}

		return suppliers;
	}

	@Override
	public Integer recordSearchCustomSupplierData(String department, String search) {

		Integer supplierCount = 0;
		supplierCount = piMapper.recordSearchCustomSupplierData(department, search);
		if (supplierCount == 0) {
			supplierCount = piMapper.recordSearchAllCustomSupplierData(search);
		}
		return supplierCount;
	}

	@Override
	public Integer getCustomRecordPricePoint(String department) {
		return piMapper.getCustomRecordArticlePricePoint(department);
	}

	@Override
	public List<ADMItem> getCustomPricePoint(String department, Integer offset, Integer pagesize) {
		return piMapper.getCustomPricePoint(department, offset, pagesize);
	}

	@Override
	public Integer recordCustomSearchByArticlePricePoint(String hL4Code, String department, String costPrice,
			String sellPrice) {
		return piMapper.recordCustomSearchByArticlePricePoint(hL4Code, department, costPrice, sellPrice);
	}

	@Override
	public List<ADMItem> searchByCustomArticlePricePoint(Integer offset, Integer pagesize, String hL4Code,
			String department, String costPrice, String sellPrice) {
		return piMapper.searchByCustomArticlePricePoint(offset, pagesize, hL4Code, department, costPrice, sellPrice);
	}

	@Override
	public Integer recordCustomArticlePricePoint(String department, String search) {
		return piMapper.recordCustomArticlePricePoint(department, search);
	}

	@Override
	public List<ADMItem> searchCustomArticlePricePoint(Integer offset, Integer pagesize, String department,
			String search) {
		return piMapper.searchCustomArticlePricePoint(offset, pagesize, department, search);
	}

	@Override
	public String getSystemDefaultConfig(String key, String orgId) {
		return piMapper.getSystemDefaultConfig(key, orgId);
	}

	///// code is written by varun
	@Override
	public int getRecordSuppliersByCity(String cityName) {
		return piMapper.getRecordSuppliersByCity(cityName);
	}

	@Override
	public List<SupplierData> searchByCustomSuppliersByCity(int offset, int pageSize, String slCode, String slName,
			String slAddr, String cityName) {
		return piMapper.searchByCustomSuppliersByCity(offset, pageSize, slCode, slName, slAddr, cityName);
	}

	@Override
	public int recordsearchByCustomSuppliersByCity(String slCode, String slName, String slAddr, String cityName) {
		return piMapper.recordsearchByCustomSuppliersByCity(slCode, slName, slAddr, cityName);
	}

	@Override
	public List<SupplierData> getCustomSuppliersByCity(int offset, int pageSize, String cityName) {
		return piMapper.getCustomSuppliersByCity(cityName, offset, pageSize);
	}

	@Override
	public int recordSearchCustomSuppliersByCity(String cityName, String search) {
		return piMapper.recordSearchCustomSuppliersByCity(cityName, search);
	}

	@Override
	public List<SupplierData> searchCustomSuppliersByCity(String cityName, int offset, int pageSize, String search) {
		return piMapper.searchCustomSuppliersByCity(cityName, offset, pageSize, search);
	}

	@Override
	public int getAllTransportersRecordByCity(String cityName) {
		return piMapper.getAllTransportersRecordByCity(cityName);
	}

	@Override
	public List<Transporters> getAllTransportersByCity(int offset, int pageSize, String cityName) {
		return piMapper.getAllTransportersByCity(offset, pageSize, cityName);
	}

	@Override
	public int getAllFilterTransportersRecordByCity(String transporterName, String transporterCode, String cityName) {
		return piMapper.getAllFilterTransportersRecordByCity(transporterName, transporterCode, cityName);
	}

	@Override
	public List<Transporters> getAllFilterTransportersByCity(int offset, int pageSize, String transporterName,
			String transporterCode, String cityName) {
		return piMapper.getAllFilterTransportersByCity(offset, pageSize, transporterName, transporterCode, cityName);
	}

	@Override
	public int getAllSearchTransportersRecordByCity(String search, String cityName) {
		return piMapper.getAllSearchTransportersRecordByCity(search, cityName);
	}

	@Override
	public List<Transporters> getAllSearchTransportersByCity(int offset, int pageSize, String search, String cityName) {
		return piMapper.getAllSearchTransportersByCity(offset, pageSize, search, cityName);
	}

	@Override
	public int insertPIDetailsForOthers(PurchaseIndentDetail piItems) {
		return piMapper.createPIDetailsForOthers(piItems);
	}

	@Override
	public int insertFinChargeForOthers(FinCharge finCharge) {
		return piMapper.createFinChargeForOthers(finCharge);
	}

	@Override
	public List<PurchaseIndentDetail> getPIDetailForOthers(int orderId, String orderDetailId) {
		return piMapper.getPurchaseIndentDetailForOthers(orderId, orderDetailId);
	}

	@Override
	public int recordDistinctArticle(String hl3Code) {
		return piMapper.recordDistinctArticle(hl3Code);
	}


	@Override
	public int searchRecordDistinctArticle(String search) {
		return piMapper.searchRecordDistinctArticle(search);
	}

	@Override
	public List<Map<String, String>> getByDepartmentCode(String hl3Code, int offset, int pageSize) {
		return piMapper.getByDepartmentCode(hl3Code, offset, pageSize);
	}

	@Override
	public List<Map<String, String>> searchByDepartmentCode(int offset, int pageSize, String search) {
		return piMapper.searchByDepartmentCode(offset,pageSize,search);		
	}

}
