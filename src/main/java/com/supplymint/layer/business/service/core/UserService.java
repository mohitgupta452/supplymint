package com.supplymint.layer.business.service.core;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.supplymint.layer.data.core.entity.Users;
import com.supplymint.layer.data.core.mapper.UserMapper;

@Service
public class UserService {
	
private UserMapper userMapper;
	
	@Autowired
	public UserService(UserMapper userMapper) {
		
		this.userMapper=userMapper;
	}
	
	public int updateUser(Users user) {
		
		return userMapper.updateUser(user); 
	}	
	
	public List getAllUsers() {
		
		return userMapper.getAllUsers(); 
	}
	
	public Users findById(int eid) {
		
		return userMapper.findById(eid); 
	}

}
