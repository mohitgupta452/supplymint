package com.supplymint.layer.business.service.scheduler;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.amazonaws.services.glue.model.GetJobRunResult;
import com.amazonaws.services.glue.model.JobRun;
import com.supplymint.config.aws.config.AWSGlueConfig;
import com.supplymint.config.aws.utils.AWSUtils.CustomParam;
import com.supplymint.config.aws.utils.AWSUtils.RunState;
import com.supplymint.layer.business.contract.inventory.JobDetailService;
import com.supplymint.layer.business.contract.s3Bucket.S3BucketInfoService;
import com.supplymint.layer.business.service.core.InventoryAutoConfigService;
import com.supplymint.layer.data.config.ThreadLocalStorage;
import com.supplymint.layer.data.core.entity.InventoryAutoConfig;
import com.supplymint.layer.data.tenant.inventory.entity.JobDetail;
import com.supplymint.layer.data.tenant.inventory.entity.JobTrigger;
import com.supplymint.layer.web.endpoint.tenant.inventory.JobDetailEndpoint;
import com.supplymint.util.ApplicationUtils.AppEnvironment;
import com.supplymint.util.ApplicationUtils.CustomParameter;
import com.supplymint.util.CodeUtils;
import com.supplymint.util.FileUtils;
import com.supplymint.util.JsonUtils;

@Service
public class JobdetailScheduler {

	private final static Logger LOGGER = LoggerFactory.getLogger(JobdetailScheduler.class);

	@Autowired
	private AWSGlueConfig awsGlueConfig;

	@Autowired
	private JobDetailService jobDetailService;

	@Autowired
	private JobDetailEndpoint detailEndpoint;

	@Autowired
	private S3BucketInfoService s3BucketInfoService;
	
	@Autowired
	Environment environment;

	@Autowired
	private InventoryAutoConfigService inventoryAutoConfigService;

	@SuppressWarnings("unused")
	@Scheduled(cron = "0 0/30 * * * ?")
//	@Scheduled(cron = "0 33 12 * * ?")
	public void autoManageJobDetailsScheduler() {

		LOGGER.debug("Job Scheduler Start...");

		try {
			ThreadLocalStorage.setTenantHashKey(null);

			List<InventoryAutoConfig> autoConfigList = inventoryAutoConfigService.getAll();
			LOGGER.debug(String.format("Obtaining all job Configuration from core DB :%s", autoConfigList));
//			
			if (environment.getActiveProfiles()[0].equalsIgnoreCase(AppEnvironment.Profiles.DEV.name())
					|| environment.getActiveProfiles()[0].equalsIgnoreCase(AppEnvironment.Profiles.PROD.name())) {

				for (int i = 0; i < autoConfigList.size(); i++) {

					if (autoConfigList.get(i).getStatus().equals("TRUE") && autoConfigList.get(i).getActive() == '1') {
						try {
							ThreadLocalStorage.setTenantHashKey(autoConfigList.get(i).getTenantHashKey());

							LOGGER.info(ThreadLocalStorage.getTenantHashKey());
							String jobName = autoConfigList.get(i).getJobName();
							String orgId = autoConfigList.get(i).getOrgId();

							LOGGER.info("Checking condition for active running job");
							List<JobDetail> activeJob = jobDetailService.getByState(jobName,RunState.RUNNING.toString());
							if (activeJob.size() == 0) {
								JobTrigger jobTrigger = jobDetailService
										.getByTriggerName(autoConfigList.get(i).getTriggerName(), orgId);
								if (!CodeUtils.isEmpty(jobTrigger)) {
									String isManualTransfer = autoConfigList.get(i).getIsManualTransferUrl();
									List<JobRun> jobRunsResults = awsGlueConfig.listAllJobRunsResult(jobName);
									LOGGER.debug(String.format("jobResult : %s", jobRunsResults));
									Date scheduleTime = CodeUtils._____dateFormat.parse(jobTrigger.getSchedule());

									for (int j = jobRunsResults.size() - 1; j >= 0; j--) {
										try {
											String jobStartedOn = CodeUtils._____dateFormat
													.format(CodeUtils.convertDateOnTimeZonePluseForAWS(
															jobRunsResults.get(j).getStartedOn(), 5, 30));
											Date startedOn = CodeUtils._____dateFormat.parse(jobStartedOn);
											if (jobRunsResults.get(j).toString().contains("TriggerName")) {

												if (scheduleTime.equals(startedOn) || scheduleTime.before(startedOn)
														&& !jobRunsResults.get(j).getJobRunState()
																.equalsIgnoreCase(RunState.RUNNING.toString())) {

													LOGGER.debug(String.format("startedOn ",
															jobRunsResults.get(j).getStartedOn()));
													LOGGER.debug(String.format("scheduleOn ", scheduleTime));

													JobDetail jobDetail = new JobDetail();
													jobDetail.setJobName(jobName);
													jobDetail.setJobId(jobRunsResults.get(j).getId());
													int validateRow = jobDetailService
															.validateRow(jobDetail.getJobId());
													if (validateRow == 0) {

														boolean createJob = jobDetailService.createJobs(jobDetail);
														LOGGER.debug(String.format("update jobDetails of Job Id : %s",
																jobDetail.getJobId()));
														updateJobDetailInfomation(jobName,
																jobRunsResults.get(j).getId(), isManualTransfer,
																jobTrigger, orgId);
													}
												}
											}
										} catch (Exception ex) {
											LOGGER.debug("Exception Occoured from Exception : %s", ex);
										}
									}
								}
							}
						} catch (Exception ex) {
							LOGGER.debug("Exception Occoured from Exception : %s", ex);
						}
					}
				}
			}
		} catch (Exception ex) {
			LOGGER.debug("Exception Occoured from Exception : %s", ex);
		}
		LOGGER.debug("Job Scheduler End...");
	}

	@SuppressWarnings({ "unused", "deprecation" })
	void updateJobDetailInfomation(String jobName, String jobId, String isManualTransfer, JobTrigger trigger,String orgId) {

		GetJobRunResult jobRunResult = null;
		Date completedOn = null;
		long diffMinutes = 0L;
		try {
			LOGGER.debug("update JobDetail Meta Data");
			jobRunResult = awsGlueConfig.getJobRunResult(jobId, jobName);
			JobDetail jobDetails = jobDetailService.storeJobRunMetaData(jobRunResult);
			jobDetails.setArguments(JsonUtils.objectToSingleLineJson(jobRunResult.getJobRun().getArguments()));
			jobDetails.setJobType(jobRunResult.getJobRun().getArguments().get(CustomParam.TRIGGER));
			jobDetails.setTriggerName(trigger.getTriggerName());
			jobDetails.setLastEngine("TRUE");

			String configDate = CodeUtils.________dateTimeFormat.format(new Date());
			String Key = jobDetails.getBucketKey() + CustomParameter.DELIMETER + configDate;
			jobDetails.setBucketKey(Key);
			String manualTransferOrder = Key + "/MANUAL_TRANSFER_ORDER";
			jobDetails.setOrgId(orgId);

			int response = jobDetailService.updateMetaData(jobDetails);
			int updateOrgId = jobDetailService.updateOrgId(jobDetails.getOrgId(), jobDetails.getJobId());
			int updateFileName = jobDetailService.updateFileName(configDate, jobDetails.getArguments(),
					jobDetails.getJobId());
			int updateTriggerName = jobDetailService.updateTriggerName(jobId, jobName, jobDetails.getTriggerName());
			
			// update next schedule
			LOGGER.debug("Udpate schedule and next Schedule");
			Date awsschedule = CodeUtils.convertDateOnTimeZonePluseForAWS(new Date(trigger.getNextSchedule()), 5, 30);
//			String previousNextSchedule=trigger.getNextSchedule();
			String convertSchedule = CodeUtils.convertISTtoNormalDateFormat(awsschedule.toString()).toString();

			String schedule = CodeUtils.convertStandardToNormalDateFormat(convertSchedule);
			String schduleTriggerResult = awsGlueConfig.retrieveNextScheduled(trigger.getFrequency(), schedule,
					trigger.getTimeZone(), trigger.getDayWeek(), trigger.getDayMonth());

			
			int updatePreviousSchedule = jobDetailService.updatePreviousSchedule(trigger.getNextSchedule(),
					trigger.getTriggerName());
			trigger.setNextSchedule(schduleTriggerResult);
			int updateSchedule = jobDetailService.updateNextSchedule(schduleTriggerResult,
					jobRunResult.getJobRun().getTriggerName());
			
			
			if (jobDetails.getJobRunState().equals(RunState.SUCCEEDED.toString())) {
				
				String pattern=jobName.substring(jobName.indexOf("IP"),jobName.lastIndexOf("-"));
				String tableName="REP_ALLOC_OUTPUT_"+pattern;

				int storeCount = jobDetailService.globalStoreCount(tableName);

				jobDetails.setStoreCount(storeCount);
				int itemCount = jobDetailService.globalitemCount(tableName);
				jobDetails.setItemCount(itemCount);
				String tCount = jobDetailService.globaltotalCount(tableName);

				if (tCount == null) {
					int totalCount = 0;
					jobDetails.setTotalCount(totalCount);
				} else {
					jobDetails.setTotalCount(Integer.parseInt(tCount));
				}

				completedOn = CodeUtils.convertDateOnTimeZonePluseForAWS(jobRunResult.getJobRun().getCompletedOn(), 5,
						30);
				jobDetails.setCompleteOn(completedOn);
				long difference = completedOn.getTime() - CodeUtils
						.convertDateOnTimeZonePluseForAWS(jobRunResult.getJobRun().getStartedOn(), 5, 30).getTime();
				diffMinutes = difference / (60 * 1000) % 60;
				String duration = Long.toString(diffMinutes);
				jobDetails.setDuration(duration);
				Date updationTime = new Date();
				jobDetails.setRepDate(CodeUtils.convertDateOnTimeZonePluseForAWS(updationTime, 5, 30));

				int validateSummaryUpdate = jobDetailService.validateSummaryUpdate(jobDetails);
				if (validateSummaryUpdate > 1) {
					int updateSummary = jobDetailService.updateSummary(jobDetails);
				} else {
					jobDetails.setSummary("TRUE");
					int updateSummary = jobDetailService.updateSummary(jobDetails);
				}
				int updateCount = jobDetailService.globalupdateCount(jobDetails);

				int updateManualTransferOrder = s3BucketInfoService.updateManualTransferOrder(manualTransferOrder,
						FileUtils.FileDownload);
				int updateState = jobDetailService.updateStatus(jobDetails);
				int updatePreviousLastEngine = jobDetailService.updatePreviousLastEngine(jobId,jobName);
				int updateLastEngine = jobDetailService.updateLastEngine(jobId, jobName);
				LOGGER.debug("generating Transfer Order...");
				detailEndpoint.uploadSummaryZip(ThreadLocalStorage.getTenantHashKey());
				for (int i = 0;; i++) {
					
					int count = 0;
					JobDetail data = jobDetailService.getByJobId(jobId);
					if (data.getGeneratedTransferOrder() != null) {
						break;
					} else {
						// sleep for 3 minutes
						Thread.sleep(180000);
						count++;
						if (count == 4) {
							// terminate after 12 minutes
							break;
						}
					}
				}
				LOGGER.debug("Transfer Order generated");
			} else if (jobDetails.getJobRunState().equals(RunState.FAILED.toString())) {
				LOGGER.debug("Job Run State Failed !");
				int updateState = jobDetailService.updateStatus(jobDetails);
				int updatePreviousLastEngine = jobDetailService.updatePreviousLastEngine(jobId,jobName);
				int updateLastEngine = jobDetailService.updateLastEngine(jobId, jobName);

			}
		} catch (Exception ex) {
			LOGGER.debug("Exception Occoured from Exception : %s", ex);
		}
		LOGGER.debug("Udpate all entry of Job Scheduler");
	}
}
