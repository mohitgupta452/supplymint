
package com.supplymint.layer.business.service.tenant;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.supplymint.layer.data.core.entity.Tenant;
import com.supplymint.layer.data.core.mapper.TenantMapper;
import com.supplymint.util.SecurityUtils;

/**
 * 
 * 
 * @author Ashish Singh Dev
 * @version 1.0
 * @since 19-Sep-2018
 *
 */
@Service
public class TenantService {

	private final static Logger LOGGER = LoggerFactory.getLogger(TenantService.class);

	private TenantMapper tenantMapper;

	private Map<String, String> tenantProperty;

	@Autowired
	public TenantService(TenantMapper tenantMapper) {
		this.tenantMapper = tenantMapper;
	}

	public Tenant getTenantById(Integer id) {
		return tenantMapper.findById(id);
	}

	public Tenant getTenantByUuid(String uuid) {
		return tenantMapper.findByUuid(uuid);
	}

	public Integer save(String name, String config) {
		Tenant t = new Tenant();
		t.setUuid(UUID.randomUUID().toString());
		t.setName(name);
		t.setConfig(config);

		return tenantMapper.create(t);
	}

	public List<Tenant> findAll() {
		return tenantMapper.findAll();
	}

	public String getTenantHashKey(Tenant tenant) {
		try {
			return SecurityUtils.hashSHA256(String.format("%s:%s", tenant.getId(), tenant.getUuid()));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		return null;
	}

	public Map<String, DataSource> getTenantsDS() {
		Map<String, DataSource> tenants = new HashMap<>();
		tenantProperty = new HashMap<>();
		findAll().forEach(t -> {
			JsonNode dbConfig;
			try {
				dbConfig = new ObjectMapper().readTree(t.getConfig());
				DataSource tenantDS = DataSourceBuilder.create()
						.driverClassName(dbConfig.get("databaseDriverName").asText())
						.username(dbConfig.get("username").asText()).password(dbConfig.get("password").asText())
						.url(dbConfig.get("url").asText()).build();
				tenants.put(getTenantHashKey(t), tenantDS);
				tenantProperty.put(getTenantHashKey(t), dbConfig.get("username").asText());
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
		return tenants;
	}

	// @Author Manoj - Method to retrieve config property of the tenant's
	public Map<String, String> getTenantProperty() {
		return tenantProperty;
	}

	public void getReleaseTenantsDS(Connection connection) {
		try {
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
