package com.supplymint.layer.business.service.core;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.supplymint.layer.data.core.entity.InventoryAutoConfig;
import com.supplymint.layer.data.core.mapper.InventoryAutoConfigMapper;

@Service
public class InventoryAutoConfigService {
	
	@Autowired
	private InventoryAutoConfigMapper mapper;
	
	public List<InventoryAutoConfig> getAll() {
		return mapper.getAll();
	}
	

	
}
