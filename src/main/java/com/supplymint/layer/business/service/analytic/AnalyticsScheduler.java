package com.supplymint.layer.business.service.analytic;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.supplymint.layer.data.config.ThreadLocalStorage;
import com.supplymint.layer.data.tenant.analytic.entity.StoreProfileActualData;
import com.supplymint.layer.data.tenant.analytic.mapper.AnalyticsMapper;

@Service
public class AnalyticsScheduler {
	
	private final static Logger LOGGER = LoggerFactory.getLogger(AnalyticsScheduler.class);
	
	@Autowired
	private AnalyticsMapper analyticsMapper;
	
//	@Scheduled(cron="*/10 * * * * *")
	public void storeProfileScheduler() {
		List<StoreProfileActualData> data =null;
		//scheduler for bazaar kolkata
		ThreadLocalStorage.setTenantHashKey("3bb0ce6e749ee42dc42e8a15d14615c8441489005916cd843a331e660a1a0416");
		
		data=analyticsMapper.getAnalyticsActualData();
		
		LOGGER.debug("actual sale data:%d",data);
		
		
	}

}
