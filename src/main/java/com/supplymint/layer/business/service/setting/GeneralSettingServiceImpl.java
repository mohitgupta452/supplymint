
package com.supplymint.layer.business.service.setting;

import java.net.InetAddress;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.CannotGetJdbcConnectionException;
import org.springframework.stereotype.Service;

import com.ibm.icu.text.SimpleDateFormat;
import com.supplymint.layer.business.contract.setting.GeneralSettingService;
import com.supplymint.layer.data.setting.entity.GeneralSetting;
import com.supplymint.layer.data.setting.mapper.GeneralSettingMapper;
import com.supplymint.layer.web.endpoint.abstracts.AbstractEndpoint;
import com.supplymint.util.ApplicationUtils;
import com.supplymint.util.CodeUtils;

@Service
public class GeneralSettingServiceImpl extends AbstractEndpoint
		implements GeneralSettingService {

	public GeneralSettingServiceImpl() {
		super();
	}

	private static final Logger LOGGER = LoggerFactory.getLogger(GeneralSettingServiceImpl.class);

	public GeneralSettingMapper generalSettingMapper;

	@Autowired
	private HttpServletRequest servletRequest;

	@Autowired
	public GeneralSettingServiceImpl(GeneralSettingMapper generalSettingMapper) {
		this.generalSettingMapper = generalSettingMapper;

	}

	@SuppressWarnings("unused")
	@Override
	public int create(GeneralSetting generalSetting, HttpServletRequest request) throws Exception {
		String ipAddress = null;
		String setting = null;
		String setupValue = null;
		String representation = null;
		int result = 0;
		try {

			if (generalSetting.getNumberFormat().equalsIgnoreCase("CUSTOM")) {
				if (!generalSetting.getUseSeperator().equalsIgnoreCase("TRUE")) {
					setting = generalSetting.getNumberFormat() + "_" + "DECIMAL";
					setupValue = generalSetting.getDecimalPlaces();
					String sampleValue = "X";
					representation = "XXXXX."
							+ String.join("", Collections.nCopies(Integer.parseInt(setupValue), sampleValue));
				} else {
					setting = generalSetting.getNumberFormat() + "_" + "DECIMAL_SEPERATOR";
					setupValue = generalSetting.getDecimalPlaces();
					String sampleValue = "X";
					representation = "XX,XXX."
							+ String.join("", Collections.nCopies(Integer.parseInt(setupValue), sampleValue));
					;
				}
			} else {
				setting = generalSetting.getNumberFormat().toUpperCase();
				setupValue = generalSetting.getDecimalPlaces();
				representation = "XXXXX";
			}
			generalSetting.setSetting("NUMBER_FORMAT");
			generalSetting.setGeneralConfig(setting);
			generalSetting.setSetupValue(setupValue);
			generalSetting.setRepresentation(representation);
			generalSetting.setCreatedBy(servletRequest.getAttribute("email").toString());
			Date createdOn = CodeUtils.convertDateOnTimeZonePluseForAWS(new Date(), 5, 30);
			generalSetting.setCreatedOn(createdOn);
			ipAddress = InetAddress.getLocalHost().getHostAddress().trim().toString();
			generalSetting.setIpAddress(ipAddress);
			generalSetting.setStatus("TRUE");
			generalSetting.setActive('1');
			int updateStatus = generalSettingMapper.updateStatus();
			LOGGER.info("Notification create query start...");
			result = generalSettingMapper.create(generalSetting);

			if (generalSetting.getUseSymbol().equalsIgnoreCase("TRUE")) {
				setting = generalSetting.getCurrency().toUpperCase();
				setupValue = "0";
				representation = ApplicationUtils.CurrencyParameters.valueOf(setting).getCurrency() + " " + setupValue;
				setting = generalSetting.getCurrency().toUpperCase() + "_" + "SYMBOL";
			} else {
				setting = generalSetting.getCurrency().toUpperCase();
				setupValue = "0";
				representation = setupValue;
			}

			generalSetting.setSetting("CURRENCY");
			generalSetting.setGeneralConfig(setting);
			generalSetting.setSetupValue(setupValue);
			generalSetting.setRepresentation(representation);
			result = generalSettingMapper.create(generalSetting);

			if (!generalSetting.getTimeFormat().equalsIgnoreCase("24 Hour")) {
				setting = generalSetting.getTimeFormat();
				setupValue = "hh:mm a";
				Calendar cal = Calendar.getInstance();
				SimpleDateFormat sdf = new SimpleDateFormat(setupValue);
				representation = sdf.format(cal.getTime());
			} else {
				setting = generalSetting.getTimeFormat();
				setupValue = "HH:MM";
				Calendar cal = Calendar.getInstance();
				SimpleDateFormat sdf = new SimpleDateFormat(setupValue);
				representation = sdf.format(cal.getTime());
			}
			generalSetting.setSetting("TIME_FORMAT");
			generalSetting.setGeneralConfig(setting);
			generalSetting.setSetupValue(setupValue);
			generalSetting.setRepresentation(representation);
			result = generalSettingMapper.create(generalSetting);

			setting = generalSetting.getDateFormat();
			setupValue = generalSetting.getDateFormat();
			SimpleDateFormat formatter = new SimpleDateFormat(setupValue);
			String updatedOn = formatter.format(CodeUtils.convertDateOnTimeZonePluseForAWS(new Date(), 5, 30));
			representation = updatedOn;

			generalSetting.setSetting("DATE_FORMAT");
			generalSetting.setGeneralConfig(setting);
			generalSetting.setSetupValue(setupValue);
			generalSetting.setRepresentation(representation);
			result = generalSettingMapper.create(generalSetting);

			// end code

			LOGGER.debug(String.format("Notification create result obtain... : %d", result));
		} catch (CannotGetJdbcConnectionException ex) {
			throw ex;
		} catch (DataAccessException ex) {
			throw ex;
		} catch (Exception ex) {
			throw ex;
		}
		return result;
	}

	@Override
	public Map<String, String> getAll() {
		List<GeneralSetting> data = null;

		LOGGER.info("General Setting get All query start...");
		data= generalSettingMapper.getAll();
		Map<String,String> list1=new HashMap<String,String>();
		for(int i=0;i<data.size();i++) {
			if(data.get(i).getSetting().equalsIgnoreCase("NUMBER_FORMAT")) {
//				String numberFormat =data.get(i).getGeneralConfig().replaceAll("_", " ");
//				numberFormat=numberFormat.substring(0, numberFormat.indexOf(" "));
				String numberFormat = data.get(i).getGeneralConfig().split("_")[0];
				list1.put("numberFormat", numberFormat + "");
				list1.put("numberFormatDecimalValue", data.get(i).getSetupValue());
				list1.put("numberFormatUseSeperator",
						data.get(i).getGeneralConfig().contains("SEPERATOR") ? "true" : "false");
			} else if (data.get(i).getSetting().equalsIgnoreCase("CURRENCY")) {
				list1.put("currency", data.get(i).getGeneralConfig().split("_")[0]);
				list1.put("currencyUseSymbol", data.get(i).getGeneralConfig().contains("SYMBOL") ? "true" : "false");
			} else if (data.get(i).getSetting().equalsIgnoreCase("DATE_FORMAT")) {
				list1.put("dateFormat", data.get(i).getGeneralConfig());
			} else if (data.get(i).getSetting().equalsIgnoreCase("TIME_FORMAT")) {
				list1.put("timeFormat", data.get(i).getGeneralConfig());
				list1.put("timeSetValue", data.get(i).getSetupValue());
			}
		}
		LOGGER.info("General Setting retrive All result...%s",list1);
		return list1;
	}

//	@Override
//	public int update(GeneralSetting generalSetting) {
//		String ipAddress = null;
//		String setting = null;
//		String setupValue = null;
//		String representation = null;
//		int result = 0;
//		try {
//			if (generalSetting.getNumberFormat().equalsIgnoreCase("CUSTOM")) {
//				if (!generalSetting.getUseSeperator().equalsIgnoreCase("TRUE")) {
//					setting = generalSetting.getNumberFormat().toUpperCase() + "_" + "DECIMAL";
//					setupValue = generalSetting.getDecimalPlaces();
//					representation = "XX.XX";
//				} else {
//					setting = generalSetting.getNumberFormat().toUpperCase() + "_" + "DECIMAL_SEPERATOR";
//					setupValue = generalSetting.getDecimalPlaces();
//					representation = "XX,XXX.XX";
//				}
//			} else {
//				setting = generalSetting.getNumberFormat().toUpperCase();
//				setupValue = "0";
//				representation = "XX";
//			}
//			generalSetting.setSetting("NUMBER_FORMAT");
//			generalSetting.setGeneralConfig(setting);
//			generalSetting.setSetupValue(setupValue);
//			generalSetting.setRepresentation(representation);
//			generalSetting.setCreatedBy(servletRequest.getAttribute("email").toString());
//			Date createdOn = CodeUtils.convertDateOnTimeZonePluseForAWS(new Date(), 5, 30);
//			generalSetting.setCreatedOn(createdOn);
//			ipAddress = InetAddress.getLocalHost().getHostAddress().trim().toString();
//			generalSetting.setIpAddress(ipAddress);
//			LOGGER.info("Notification update query start...");
//			result = generalSettingMapper.update(generalSetting);
//			if (generalSetting.getUseSymbol().equalsIgnoreCase("TRUE")) {
//				setting = generalSetting.getCurrency().toUpperCase();
//				setupValue = "0";
//				representation = ApplicationUtils.CurrencyParameters.valueOf(setting).getCurrency() + " " + setupValue;
//			} else {
//				setting = generalSetting.getCurrency().toUpperCase();
//				setupValue = "0";
//				representation = setupValue;
//			}
//			generalSetting.setSetting("CURRENCY");
//			generalSetting.setGeneralConfig(setting);
//			generalSetting.setSetupValue(setupValue);
//			generalSetting.setRepresentation(representation);
//			result = generalSettingMapper.update(generalSetting);
//			if (!generalSetting.getTimeFormat().equalsIgnoreCase("12_FORMAT")) {
//				setting = generalSetting.getTimeFormat();
//				setupValue = "HH:mm";
//				representation = "00:00";
//			} else {
//				setting = generalSetting.getTimeFormat();
//				setupValue = "HH:MM a";
//				representation = "00:00 AM";
//			}
//			generalSetting.setSetting("TIME_FORMAT");
//			generalSetting.setGeneralConfig(setting);
//			generalSetting.setSetupValue(setupValue);
//			generalSetting.setRepresentation(representation);
//			result = generalSettingMapper.update(generalSetting);
//
//			setting = generalSetting.getDateFormat();
//			setupValue = ApplicationUtils.DateFormat.valueOf(setting).getDateFormat();
//			SimpleDateFormat formatter = new SimpleDateFormat(setupValue);
//			String updatedOn = formatter.format(CodeUtils.convertDateOnTimeZonePluseForAWS(new Date(), 5, 30));
//			representation = updatedOn;
//			generalSetting.setSetting("DATE_FORMAT");
//			generalSetting.setGeneralConfig(setting);
//			generalSetting.setSetupValue(setupValue);
//			generalSetting.setRepresentation(representation);
//			result = generalSettingMapper.update(generalSetting);
//
//		} catch (CannotGetJdbcConnectionException ex) {
//			throw ex;
//		} catch (DataAccessException ex) {
//			throw ex;
//		} catch (Exception ex) {
//
//		}
//		return result;
//
//	}

	@Override
	public int updatevalue(Object b) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getAll1() {
		// TODO Auto-generated method stub
		return null;
	}
}
