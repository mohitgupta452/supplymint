package com.supplymint.layer.business.service.inventory;

import static java.nio.file.StandardOpenOption.CREATE_NEW;

import java.io.OutputStream;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.supplymint.layer.business.contract.inventory.AdHocRequestService;
import com.supplymint.layer.data.tenant.administration.entity.ADMItem;
import com.supplymint.layer.data.tenant.administration.entity.ADMSite;
import com.supplymint.layer.data.tenant.demand.entity.AdHocItemRequest;
import com.supplymint.layer.data.tenant.demand.entity.AdHocRequest;
import com.supplymint.layer.data.tenant.inventory.mapper.AdHocRequestMapper;

@Service
public class AdHocRequestServiceImpl implements AdHocRequestService {

	private AdHocRequestMapper adHocRequestMapper;

	@Autowired
	public AdHocRequestServiceImpl(AdHocRequestMapper mapper) {
		this.adHocRequestMapper = mapper;

	}

	@Override
	public int create(AdHocRequest adocRequest) {
		return adHocRequestMapper.create(adocRequest);
	}

	@Override
	public List<ADMItem> itemCode(Integer offset, Integer pagesize) {
		return adHocRequestMapper.itemCode(offset, pagesize);
	}

	@Override
	public Integer countItemCode() {
		return adHocRequestMapper.countItemCode();
	}

	@Override
	public List<ADMItem> searchItemCode(Integer offset, Integer pagesize, String search) {
		return adHocRequestMapper.searchItemCode(offset, pagesize, search);

	}

	@Override
	public Integer counterForSerarchItemCode(String search) {
		return adHocRequestMapper.counterForSerarchItemCode(search);
	}

	@Override
	public List<ADMSite> getLocation() {
		return adHocRequestMapper.getLocation();
	}

	@Override
	public List<AdHocRequest> getAll(Integer offset, Integer pagesize) {
		return adHocRequestMapper.getAll(offset, pagesize);

	}

	@Override
	public Integer count() {
		return adHocRequestMapper.count();
	}

	@Override
	public List<AdHocRequest> searchAll(Integer offset, Integer pagesize, String search) {

		return adHocRequestMapper.searchAll(offset, pagesize, search);
	}

	@Override
	public Integer searchCount(String search) {
		return adHocRequestMapper.searchCount(search);
	}

	@Override
	public String getLastWorkOrder() {
		return adHocRequestMapper.getLastWorkOrder();

	}

	@Override
	public List<AdHocItemRequest> getItem(String workOrder) {
		return adHocRequestMapper.getItem(workOrder);
	}

	@Override
	public int updateStatus(String workOrder, String ipAddress, OffsetDateTime updationTime, String status) {
		return adHocRequestMapper.updateStatus(workOrder, ipAddress, updationTime, status);
	}

	@Override
	public List<AdHocRequest> getByworkOrder(String workOrder) {

		return adHocRequestMapper.getByworkOrder(workOrder);
	}

	@Override
	public List<AdHocRequest> getAllRecords() {
		return adHocRequestMapper.getAllRecords();
	}

	@Override
	public List<AdHocRequest> filter(Integer offset, Integer pagesize, String workOrder, String status, String site,
			String createdOn) {
		return adHocRequestMapper.filter(offset, pagesize, workOrder, status, site, createdOn);
	}

	@Override
	public Integer filterRecord(String workOrder, String status, String site, String createdOn) {
		return adHocRequestMapper.filterRecord(workOrder, status, site, createdOn);
	}

	@Override
	public List<AdHocRequest> filterByDate(Integer offset, Integer pagesize, String createdOn) {

		return adHocRequestMapper.filterByDate(offset, pagesize, createdOn);
	}

	@Override
	public Integer getRecordFilterByDate(String createdOn) {

		return adHocRequestMapper.getRecordFilterByDate(createdOn);
	}

	@Override
	public Integer deleteByWorkOrderAndItemCode(String workOrder, String itemData) {
		return adHocRequestMapper.deleteByWorkOrderAndItemCode(workOrder, itemData);
	}

	@Override
	public List<AdHocRequest> validation(String workOrder, String itemData) {

		return adHocRequestMapper.validation(workOrder, itemData);
	}

	public List<AdHocRequest> getItemByworkOrder(String workOrder) {
		return adHocRequestMapper.getItemByworkOrder(workOrder);
	}

	@Override
	public Integer cancelWorkedOrder(String workeOrder, String ipAddress, OffsetDateTime updationTime) {
		return adHocRequestMapper.cancelWorkedOrder(workeOrder, ipAddress, updationTime);

	}

	@Override
	public int countActiveStatus(String workOrder) {
		return adHocRequestMapper.countActiveStatus(workOrder);
	}

	@Override
	public Integer removedWorkedOrder(String workOrder, String ipAddress, OffsetDateTime updationTime) {
		return adHocRequestMapper.removedWorkedOrder(workOrder, ipAddress, updationTime);
	}

	@Override
	public void generateAdHocRequestListTOExcel(List<AdHocRequest> list, String filePath) {
		Workbook workbook = new XSSFWorkbook();
		Sheet sheet = workbook.createSheet("SITE");

		int rowIndex = 1;
		Row row1 = sheet.createRow(0);
		row1.createCell(0).setCellValue("SITE");
		row1.createCell(1).setCellValue("REQUESTED DATE");
		row1.createCell(2).setCellValue("WORK ORDER");
		row1.createCell(3).setCellValue("CREATED ON");
		row1.createCell(5).setCellValue("ITEM DATA");
		row1.createCell(4).setCellValue("QUANTITY");
		row1.createCell(6).setCellValue("STATUS");

		for (AdHocRequest adHocRequest : list) {

			Row row = sheet.createRow(rowIndex++);

			int cellIndex = 0;

			row.createCell(cellIndex++).setCellValue(adHocRequest.getSite());
			row.createCell(cellIndex++)
					.setCellValue(adHocRequest.getRequestedDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
			row.createCell(cellIndex++).setCellValue(adHocRequest.getWorkOrder());
			row.createCell(cellIndex++).setCellValue(
					adHocRequest.getCreatedTime().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
			row.createCell(cellIndex++).setCellValue(adHocRequest.getItemCode());
			row.createCell(cellIndex++).setCellValue(adHocRequest.getQuantity());
			row.createCell(cellIndex++).setCellValue(adHocRequest.getReqStatus());
		}

		try {
			Path path = Paths.get(filePath);
			try (OutputStream out = Files.newOutputStream(path, CREATE_NEW)) {
				workbook.write(out);
				out.close();
			} catch (FileAlreadyExistsException incrCounterAndRetry) {
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

//		try {
//
//			FileOutputStream fos = new FileOutputStream(FILE_PATH);
//
//			workbook.write(fos);
//
//			fos.close();
//
//		} catch (FileNotFoundException e) {
//
//			e.printStackTrace();
//
//		} catch (IOException e) {
//
//			e.printStackTrace();
//
//		}
	}

	@Override
	public Map<String, List<String>> getAdHocRequestHeaders() {

		Map<String, List<String>> map = new HashMap<String, List<String>>();
		List<String> listOfKey = new ArrayList<String>();
		List<String> listOfValue = new ArrayList<String>();

		listOfValue.add("SITE");
		listOfValue.add("REQUESTED DATE");
		listOfValue.add("WORK ORDER");
		listOfValue.add("CREATED ON");
		listOfValue.add("ITEM DATA");
		listOfValue.add("QUANTITY");
		listOfValue.add("STATUS");

		listOfKey.add("site");
		listOfKey.add("requestedDate");
		listOfKey.add("workOrder");
		listOfKey.add("createdTime");
		listOfKey.add("itemCode");
		listOfKey.add("quantity");
		listOfKey.add("reqStatus");

		map.put("key", listOfKey);
		map.put("value", listOfValue);
		return map;

	}

}
