package com.supplymint.layer.business.service.procurement;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

import javax.servlet.http.HttpServletRequest;

import org.apache.ibatis.annotations.Param;
import org.mybatis.spring.MyBatisSystemException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.opencsv.CSVReader;
import com.supplymint.config.aws.utils.AWSUtils;
import com.supplymint.exception.StatusCodes.AppStatusMsg;
import com.supplymint.exception.SupplyMintException;
import com.supplymint.layer.business.contract.procurement.PurchaseOrderService;
import com.supplymint.layer.business.contract.s3Bucket.S3BucketInfoService;
import com.supplymint.layer.data.config.ThreadLocalStorage;
import com.supplymint.layer.data.metadata.ProcurementMetaData.CATDESCUDF;
import com.supplymint.layer.data.metadata.ProcurementMetaData.DeptItemUDFSettingsMetadeta;
import com.supplymint.layer.data.metadata.ProcurementMetaData.UDFSetting;
import com.supplymint.layer.data.metadata.PurchaseIndentMetaData;
import com.supplymint.layer.data.metadata.PurchaseOrderMetaData;
import com.supplymint.layer.data.tenant.administration.entity.ADMItem;
import com.supplymint.layer.data.tenant.administration.entity.PurchaseOrderPricePoint;
import com.supplymint.layer.data.tenant.entity.procurement.config.DeptItemUDFMappings;
import com.supplymint.layer.data.tenant.inventory.entity.S3BucketInfo;
import com.supplymint.layer.data.tenant.procurement.entity.CatDescMapping;
import com.supplymint.layer.data.tenant.procurement.entity.Category;
import com.supplymint.layer.data.tenant.procurement.entity.DeptItemUDFSettings;
import com.supplymint.layer.data.tenant.procurement.entity.DeptSizeData;
import com.supplymint.layer.data.tenant.procurement.entity.FinCharge;
import com.supplymint.layer.data.tenant.procurement.entity.IndtCatDescUDFList;
import com.supplymint.layer.data.tenant.procurement.entity.InvSetUDF;
import com.supplymint.layer.data.tenant.procurement.entity.ProcurementUploadSummary;
import com.supplymint.layer.data.tenant.procurement.entity.PurchaseIndent;
import com.supplymint.layer.data.tenant.procurement.entity.PurchaseIndentHistory;
import com.supplymint.layer.data.tenant.procurement.entity.PurchaseIndentMaster;
import com.supplymint.layer.data.tenant.procurement.entity.PurchaseOrder;
import com.supplymint.layer.data.tenant.procurement.entity.PurchaseOrderAudit;
import com.supplymint.layer.data.tenant.procurement.entity.PurchaseOrderChild;
import com.supplymint.layer.data.tenant.procurement.entity.PurchaseOrderConfiguration;
import com.supplymint.layer.data.tenant.procurement.entity.PurchaseOrderExportData;
import com.supplymint.layer.data.tenant.procurement.entity.PurchaseOrderLineItem;
import com.supplymint.layer.data.tenant.procurement.entity.PurchaseOrderParent;
import com.supplymint.layer.data.tenant.procurement.entity.PurchaseOrderTemp;
import com.supplymint.layer.data.tenant.procurement.entity.TempPurchaseOrderChild;
import com.supplymint.layer.data.tenant.procurement.entity.UDF_SettingsMaster;
import com.supplymint.layer.data.tenant.procurement.mapper.PurchaseIndentMapper;
import com.supplymint.layer.data.tenant.procurement.mapper.PurchaseOrderMapper;
import com.supplymint.layer.web.endpoint.abstracts.AbstractEndpoint;
import com.supplymint.layer.web.model.AppResponse;
import com.supplymint.util.ApplicationUtils.CustomParameter;
import com.supplymint.util.ApplicationUtils.TenantHash;
import com.supplymint.util.CSVUtils;
import com.supplymint.util.CodeUtils;
import com.supplymint.util.FileUtils;
import com.supplymint.util.ToCSV;
import com.supplymint.util.UploadUtils;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

@Service
public class PurchaseOrderServiceImpl extends AbstractEndpoint implements PurchaseOrderService {

	private static final String excel = "EXCEL";
	private static String PURCHASE_ORDER = "PURCHASE_ORDER";
	private static String PURCHASE_INDENT = "PURCHASE_INDENT";

	@Autowired
	private PurchaseIndentMapper piMapper;

	@Autowired
	private HttpServletRequest request;

	private PurchaseOrderMapper poMapper;

	private static final Logger log = LoggerFactory.getLogger(PurchaseIndentServiceImpl.class);

	private final String displayNameDeptItemUDFValidationMSG = "Display Name already exist";
	private final String orderByDeptItemUDFValidationMSG = "Order-By already exist";

	public PurchaseOrderServiceImpl(PurchaseOrderMapper poMapper) {
		this.poMapper = poMapper;
	}

	@Autowired
	private S3BucketInfoService s3BucketInfoService;

	@Override
	public List<String> getAllPIPattern(Integer offset, Integer pagesize) {
		return poMapper.getAllPIPattern(offset, pagesize);

	}

	@Override
	public Integer getCountAllPIPattern() {
		return poMapper.getCountAllPIPattern();

	}

	@Override
	public List<PurchaseOrderPricePoint> getArticleHierarchyLevelData(Integer offset, Integer pagesize) {
		return poMapper.getArticleHierarchyLevelData(offset, pagesize);
	}

	@Override
	public Integer getRecordArticleHierarchyLevelData() {
		return poMapper.getRecordArticleHierarchyLevelData();

	}

	@Override
	public List<PurchaseOrderPricePoint> searchAllArticleHierarchyLevelData(Integer offset, Integer pagesize,
			String search) {
		return poMapper.searchAllArticleHierarchyLevelData(offset, pagesize, search);
	}

	@Override
	public Integer getRecordForSearchAllArticleHierarchyLevelData(String search) {
		return poMapper.getRecordForSearchAllArticleHierarchyLevelData(search);

	}

	@Override
	public List<PurchaseOrderPricePoint> filterArticleHierarchyLevelData(Integer offset, Integer pagesize,
			String hl4Code, String hl4Name, String hl1Name, String hl2Name, String hl3Name) {
		return poMapper.filterArticleHierarchyLevelData(offset, pagesize, hl4Code, hl4Name, hl1Name, hl2Name, hl3Name);
	}

	@Override
	public Integer filterRecordArticleHierarchyLevelData(String hl4Code, String hl4Name, String hl1Name, String hl2Name,
			String hl3Name) {
		return poMapper.filterRecordArticleHierarchyLevelData(hl4Code, hl4Name, hl1Name, hl2Name, hl3Name);
	}

	@Override
	public List<ADMItem> getItemsByArtCode(Integer offset, Integer pagesize, String hl4Code) {
		return poMapper.getItemsByArtCode(offset, pagesize, hl4Code);
	}

	@Override
	public Integer getCountItemsByArtCode(String hl4Code) {
		return poMapper.getCountItemsByArtCode(hl4Code);
	}

	@Override
	public List<ADMItem> searchItemsByArtCode(Integer offset, Integer pagesize, String hl4Code, String search) {
		return poMapper.searchItemsByArtCode(offset, pagesize, hl4Code, search);
	}

	@Override
	public Integer searchCountItemsByArtCode(String hl4Code, String search) {
		return poMapper.searchCountItemsByArtCode(hl4Code, search);
	}

	@Override
	public List<ADMItem> filterItemsByArtCode(Integer offset, Integer pagesize, String hl4Code, String itemCode,
			String itemName, String mrp, String rsp) {
		return poMapper.filterItemsByArtCode(offset, pagesize, hl4Code, itemCode, itemName, mrp, rsp);
	}

	@Override
	public Integer filterCountItemsByArtCode(String hl4Code, String itemCode, String itemName, String mrp, String rsp) {

		return poMapper.filterCountItemsByArtCode(hl4Code, itemCode, itemName, mrp, rsp);
	}

	@Override
	public String getPI(String orderId, String orderDetailId) {
		return piMapper.findPIJSONByPattern(orderId, orderDetailId);
	}

	@Override
	public List<String> filterAllPIPattern(Integer offset, Integer pagesize, String pattern) {

		return poMapper.filterAllPIPattern(offset, pagesize, pattern);
	}

	@Override
	public Integer countFilterAllPIPattern(String pattern) {
		return poMapper.countFilterAllPIPattern(pattern);

	}

	@Override
	public List<String> searchAllPIPattern(Integer offset, Integer pagesize, String search) {

		return poMapper.searchAllPIPattern(offset, pagesize, search);
	}

	@Override
	public Integer countSearchAllPIPattern(String search) {
		return poMapper.countSearchAllPIPattern(search);
	}

	@Override
	public List<PurchaseIndent> getPIData(@Param("offset") Integer offset, @Param("pagesize") Integer pagesize) {
		return poMapper.getPIData(offset, pagesize);
	}

	@Override
	public Integer getCountPIData() {
		return poMapper.getCountPIData();
	}

	@Override
	public List<PurchaseIndent> searchPIData(Integer offset, Integer pagesize, String search) {
		return poMapper.searchPIData(offset, pagesize, search);
	}

	@Override
	public Integer searchCountPIData(String search) {
		return poMapper.searchCountPIData(search);
	}

	@Override
	public List<PurchaseIndent> filterPIData(Integer offset, Integer pagesize, String poAmount, String poQuantity,
			String pattern) {

		return poMapper.filterPIData(offset, pagesize, poAmount, poQuantity, pattern);
	}

	@Override
	public Integer filterCountPIData(String poAmount, String poQuantity, String pattern) {
		return poMapper.filterCountPIData(poAmount, poQuantity, pattern);
	}

	@Override
	public List<UDF_SettingsMaster> getUDFMappingData() {
		return poMapper.getUDFMappingData();
	}

	@Override
	public Integer getUDFMappingDataRecord() {
		return poMapper.getUDFMappingDataRecord();
	}

	@Override
	public List<UDF_SettingsMaster> searchUDFMappingData(String search) {
		return poMapper.searchUDFMappingData(search);
	}

	@Override
	public Integer searchUDFMappingDataRecord(String search) {
		return poMapper.searchUDFMappingDataRecord(search);
	}

	@Override
	public List<UDF_SettingsMaster> filterUDFMappingData(String udfType, String isCompulsary, String displayName) {
		return poMapper.filterUDFMappingData(udfType, isCompulsary, displayName);
	}

	@Override
	public Integer filterUDFMappingDataRecord(String udfType, String isCompulsary, String displayName) {
		return poMapper.filterUDFMappingDataRecord(udfType, isCompulsary, displayName);
	}

	@Override
	public PurchaseIndentMaster fromJson(String json) throws JsonParseException, JsonMappingException, IOException {

		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonNode = mapper.readTree(json);
		PurchaseIndentMaster piMasterJson = mapper.treeToValue(jsonNode, PurchaseIndentMaster.class);

		// tempData = getMapper().treeToValue(piItem.get("piKey"),
		// PurchaseIndent.class);

		return piMasterJson;
	}

	@Override
	public List<InvSetUDF> getInvSetUDFDataByUDFType(int offset, int pagesize, String udfType) {
		return poMapper.getInvSetUDFDataByUDFType(offset, pagesize, udfType);
	}

	@Override
	public List<InvSetUDF> getInvSetUDFDataByUDFTypeExt(int offset, int pagesize, String udfType) {
		return poMapper.getInvSetUDFDataByUDFTypeExt(offset, pagesize, udfType);
	}

	@Override
	public Integer getInvSetUDFDataByUDFTypeRecord(String udfType) {
		return poMapper.getInvSetUDFDataByUDFTypeRecord(udfType);
	}

	@Override
	public Integer getInvSetUDFDataByUDFTypeExtRecord(String udfType) {
		return poMapper.getInvSetUDFDataByUDFTypeExtRecord(udfType);
	}

	@Override
	public List<InvSetUDF> searchInvSetUDFDataByUDFType(int offset, int pagesize, String udfType, String search) {
		return poMapper.searchInvSetUDFDataByUDFType(offset, pagesize, udfType, search);
	}

	@Override
	public List<InvSetUDF> searchInvSetUDFDataByUDFTypeExt(int offset, int pagesize, String udfType, String search) {
		return poMapper.searchInvSetUDFDataByUDFTypeRecordExt(offset, pagesize, udfType, search);
	}

	@Override
	public Integer searchInvSetUDFDataByUDFTypeRecord(String udfType, String search) {
		return poMapper.searchInvSetUDFDataByUDFTypeRecord(udfType, search);
	}

	@Override
	public Integer searchInvSetUDFDataByUDFTypeExtRecord(String udfType, String search) {
		return poMapper.searchInvSetUDFDataByUDFTypeExtRecord(udfType, search);
	}

	@Override
	public List<InvSetUDF> filterInvSetUDFDataByUDFType(Integer offset, Integer pagesize, String udfType, String code,
			String name) {
		return poMapper.filterInvSetUDFDataByUDFType(offset, pagesize, udfType, code, name);
	}

	@Override
	public List<InvSetUDF> filterInvSetUDFDataByUDFTypeExt(Integer offset, Integer pagesize, String udfType,
			String code, String name) {
		return poMapper.filterInvSetUDFDataByUDFTypeExt(offset, pagesize, udfType, code, name);
	}

	@Override
	public Integer filterInvSetUDFDataByUDFTypeRecord(String udfType, String code, String name) {

		return poMapper.filterInvSetUDFDataByUDFTypeRecord(udfType, code, name);
	}

	PurchaseOrder setPurchaseOrder(PurchaseOrderTemp pot, PurchaseOrderLineItem pol, String poReferenceId,
			String intgLineId, String headerId) {
		log.info("Setting parameters  in IN_PURORD table");
		PurchaseOrder po = new PurchaseOrder();

//		po.setIntGCode('1');
		po.setIntGHeaderId(pot.getOrderNo());
		po.setIntGLineId(intgLineId);
		po.setOrderNo(pot.getOrderNo());
		po.setOrderDate(pot.getOrderDate());
//		po.setVendorId(pot.getVendorId());
//		po.setTransporterId(pot.getTransporterId());
//		po.setAgentId(0);
//		po.setAgentRate(0);
//		po.setPoRemarks(pot.getPoRemarks());
//		po.setCreatedById(pot.getUserId());
		po.setValidFrom(pot.getValidFrom());
		po.setValidTo(pot.getValidTo());
//		po.setMerchandiserId(0);
//		po.setSiteId(100361);
		po.setItemId(pol.getItemId());
//		po.setSetRemarks(pol.getSetRemarks());
//		po.setArticleId(pot.getArticleId());
		po.setItemName(pol.getItemName());
//		po.setcCode1(pol.getcCode1());
//		po.setcCode2(pol.getcCode2());
//		po.setcCode3(pol.getcCode3());
//		po.setcCode4(pol.getcCode4());
		po.setcName1(pol.getcName1());
		po.setcName2(pol.getcName2());
		po.setcName3(pol.getcName3());
		po.setcName4(pol.getcName4());
		po.setDesc2(pol.getDesc2());
		po.setDesc3(pol.getDesc3());
		po.setDesc5(pol.getDesc5());
		po.setDesc6(String.valueOf(pot.getMrp()));
		po.setMrp(pot.getMrp());
		po.setListedMRP(pot.getMrp());
//		po.setWsp(0);
		po.setUom("PCS");
//		po.setMaterialType('F');
//		po.setPoItemRemarks(null);
//		po.setIsImported('N');
//		po.setTermCode(pot.getTermCode());
//		po.setIsValidated('N');
//		po.setValidationError(null);
//		po.setDocCode(1204);
		po.setSetHeaderId(headerId);
		po.setPoudfStrin01(pol.getItemudf1());
		po.setPoudfStrin02(pol.getItemudf2());
		po.setPoudfStrin03(pol.getItemudf3());
		po.setPoudfStrin04(pol.getItemudf4());
		po.setPoudfStrin05(pol.getItemudf5());
		po.setPoudfStrin06(pol.getItemudf6());
		po.setPoudfStrin07(pol.getItemudf7());
		po.setPoudfStrin08(pol.getItemudf8());
		po.setPoudfStrin09(pol.getItemudf9());
		po.setPoudfStrin010(pol.getItemudf10());
//		po.setPoudfNum01(pol.getItemudf11());
//		po.setPoudfNum02(pol.getItemudf12());
//		po.setPoudfNum03(pol.getItemudf13());
//		po.setPoudfNum04(pol.getItemudf14());
//		po.setPoudfNum05(pol.getItemudf15());
		po.setPoudfDate01(pol.getItemudf16());
		po.setPoudfDate02(pol.getItemudf17());
		po.setPoudfDate03(pol.getItemudf18());
		po.setPoudfDate04(pol.getItemudf19());
		po.setPoudfDate05(pol.getItemudf20());
//		po.setSmUDFNum01(0);
//		po.setSmUDFNum03(0);
//		po.setSmUDFNum04(0);
//		po.setSmUDFNum05(0);
//		po.setSmUDFDate01(null);
//		po.setSmUDFDate02(null);
//		po.setSmUDFDate03(null);
//		po.setSmUDFDate04(null);
//		po.setSmUDFDate05(null);

//		po.setImUDFString01(null);
//		po.setImUDFString02(null);
//		po.setImUDFString03(null);
//		po.setImUDFString04(null);
//		po.setImUDFString05(null);
//		po.setImUDFString06(null);
//		po.setImUDFString07(null);
//		po.setImUDFString08(null);
//		po.setImUDFString09(null);
//		po.setImUDFString010(null);
//		po.setImUDFNum01(0);
//		po.setImUDFNum02(0);
//		po.setImUDFNum03(0);
//		po.setImUDFNum04(0);
//		po.setImUDFNum05(0);
//		po.setImUDFDate01(null);
//		po.setImUDFDate02(null);
//		po.setImUDFDate03(null);
//		po.setImUDFDate04(null);
//		po.setImUDFDate05(null);

		po.setQty(pol.getQty());
		po.setRate(pol.getRate());
		po.setDesc1(pol.getDesc1());
		po.setSmUDFStrin01(pol.getUdf1());
//		po.setSmUDFStrin02(null);
		po.setSmUDFStrin03(pol.getUdf3());
		po.setSmUDFStrin04(pol.getUdf4());
		po.setSmUDFStrin05(pol.getUdf5());
		po.setSmUDFStrin06(pol.getUdf6());
		po.setSmUDFStrin07(pol.getUdf7());
//		po.setSmUDFStrin08(null);
//		po.setSmUDFStrin09(null);
//		po.setSmUDFStrin010(null);
//		po.setSmUDFNum02(pol.getUdf12());

		po.setPoReferenceId(poReferenceId);
//		po.setStatus(pot.getStatus());
//		po.setActive(pot.getActive());
//		po.setCreatedBy(pot.getCreatedBy());
//		po.setCreatedTime(pot.getCreatedTime());
//		po.setIpAddress(pot.getIpAddress());

		return po;
	}

	PurchaseOrder setSMPartnerPO(PurchaseOrderParent pop, PurchaseOrderChild pol, String poReferenceId,
			String intgLineId, int key, int ratio) {
//		log.info("Setting parameters  in IN_PURORD table");
		PurchaseOrder po = new PurchaseOrder();
		Map<String, Object> udfData = new HashMap<>();

		po.setIntGCode(PurchaseOrderMetaData.SMPartnerValue.ING_CODE);
		po.setSetHeaderId(pol.getSetHeaderId());
		po.setIntGHeaderId(pop.getOrderNo());
		po.setIntGLineId(intgLineId);
		po.setOrderNo(pop.getOrderNo());
		po.setKey(key);
		po.setOrderDate(pop.getOrderDate());
		po.setVendorId(pop.getSlCode());
		po.setTransporterId(pop.getTransporterCode());
//		po.setAgentId(0);
//		po.setAgentRate(0);

		po.setPoRemarks(pol.getRemarks());
//		po.setSetRemarks(pol.getSetRemarks());
		po.setPoItemRemarks(pol.getRemarks());

		po.setCreatedById(pop.getCreatedBy());
		po.setValidFrom(pop.getValidFrom());
		po.setValidTo(pop.getValidTo());
//		po.setMerchandiserId(0);
		po.setSiteId(pop.getSiteCode());
//		po.setItemId(pol.getItemId());
		po.setHsnCode(pop.getHsnCode());

		po.setArticleId(pop.getHl4Code());
//		po.setItemName(pol.getItemName());
		po.setcCode1(pol.getCat1Code());
		po.setcCode2(pol.getCat2Code());
		po.setcCode3(pol.getCat3Code());
		po.setcCode4(pol.getCat4Code());
		po.setcName1(pol.getCat1Name());
		po.setcName2(pol.getCat2Name());
		po.setcName3(pol.getCat3Name());
		po.setcName4(pol.getCat4Name());
		po.setDesc1(pol.getDesign());
		po.setDesc2(pol.getDesc2Name());
		po.setDesc3(pol.getDesc3Name());
		po.setDesc5(pol.getDesc5Name());
		po.setDesc6(String.valueOf(pol.getMrp()));
		po.setMrp(pol.getMrp());
		po.setListedMRP(pol.getMrp());
//		po.setWsp(0);
		po.setUom(PurchaseOrderMetaData.SMPartnerValue.UOM);
		po.setMaterialType(PurchaseOrderMetaData.SMPartnerValue.MATERIAL_TYPE);

		po.setIsImported(PurchaseOrderMetaData.SMPartnerValue.IS_IMPORTED);
		po.setTermCode(pop.getTermCode());
		po.setIsValidated(PurchaseOrderMetaData.SMPartnerValue.IS_VALIDATED);
//		po.setValidationError(null);
		po.setDocCode(PurchaseOrderMetaData.SMPartnerValue.DOC_CODE);

		po.setQty(ratio * pol.getNoOfSets());
		po.setRate(pol.getRate());

//		po.setPoudfStrin01(pol.getItemudf1());
//		po.setPoudfStrin02(pol.getItemudf2());
//		po.setPoudfStrin03(pol.getItemudf3());
//		po.setPoudfStrin04(pol.getItemudf4());
//		po.setPoudfStrin05(pol.getItemudf5());
//		po.setPoudfStrin06(pol.getItemudf6());
//		po.setPoudfStrin07(pol.getItemudf7());
//		po.setPoudfStrin08(pol.getItemudf8());
//		po.setPoudfStrin09(pol.getItemudf9());
//		po.setPoudfStrin010(pol.getItemudf10());
//		po.setPoudfNum01(pol.getItemudf11());
//		po.setPoudfNum02(pol.getItemudf12());
//		po.setPoudfNum03(pol.getItemudf13());
//		po.setPoudfNum04(pol.getItemudf14());
//		po.setPoudfNum05(pol.getItemudf15());
//		po.setPoudfDate01(pol.getItemudf16());
//		po.setPoudfDate02(pol.getItemudf17());
//		po.setPoudfDate03(pol.getItemudf18());
//		po.setPoudfDate04(pol.getItemudf19());
//		po.setPoudfDate05(pol.getItemudf20());

		if (!CodeUtils.isEmpty(pol.getUdf())) {

			po.setImUDFString01(pol.getUdf().getItemudf1());
			po.setImUDFString02(pol.getUdf().getItemudf2());
			po.setImUDFString03(pol.getUdf().getItemudf3());
			po.setImUDFString04(pol.getUdf().getItemudf4());
			po.setImUDFString05(pol.getUdf().getItemudf5());
			po.setImUDFString06(pol.getUdf().getItemudf6());
			po.setImUDFString07(pol.getUdf().getItemudf7());
			po.setImUDFString08(pol.getUdf().getItemudf8());
			po.setImUDFString09(pol.getUdf().getItemudf9());
			po.setImUDFString010(pol.getUdf().getItemudf10());
			po.setImUDFNum01(pol.getUdf().getItemudf11());
			po.setImUDFNum02(pol.getUdf().getItemudf12());
			po.setImUDFNum03(pol.getUdf().getItemudf13());
			po.setImUDFNum04(pol.getUdf().getItemudf14());
			po.setImUDFNum05(pol.getUdf().getItemudf15());
			po.setImUDFDate01(pol.getUdf().getItemudf16());
			po.setImUDFDate02(pol.getUdf().getItemudf17());
			po.setImUDFDate03(pol.getUdf().getItemudf18());
			po.setImUDFDate04(pol.getUdf().getItemudf19());
			po.setImUDFDate05(pol.getUdf().getItemudf20());

			po.setSmUDFStrin01(pol.getUdf().getUdf1());
			po.setSmUDFStrin02(pol.getUdf().getUdf2());
			po.setSmUDFStrin03(pol.getUdf().getUdf3());
			po.setSmUDFStrin04(pol.getUdf().getUdf4());
			po.setSmUDFStrin05(pol.getUdf().getUdf5());
			po.setSmUDFStrin06(pol.getUdf().getUdf6());
			po.setSmUDFStrin07(pol.getUdf().getUdf7());
			po.setSmUDFStrin08(pol.getUdf().getUdf8());
			po.setSmUDFStrin09(pol.getUdf().getUdf9());
			po.setSmUDFStrin010(pol.getUdf().getUdf10());
			po.setSmUDFNum01(pol.getUdf().getUdf11());
			po.setSmUDFNum02(pol.getUdf().getUdf12());
			po.setSmUDFNum03(pol.getUdf().getUdf13());
			po.setSmUDFNum04(pol.getUdf().getUdf14());
			po.setSmUDFNum05(pol.getUdf().getUdf15());
			po.setSmUDFDate01(pol.getUdf().getUdf16());
			po.setSmUDFDate02(pol.getUdf().getUdf17());
			po.setSmUDFDate03(pol.getUdf().getUdf18());
			po.setSmUDFDate04(pol.getUdf().getUdf19());
			po.setSmUDFDate05(pol.getUdf().getUdf20());
		}

		po.setPoReferenceId(poReferenceId);
//		po.setStatus(pop.getStatus());
//		po.setActive(pop.getActive());
//		po.setCreatedBy(pop.getCreatedBy());
//		po.setCreatedTime(pop.getCreatedTime());
//		po.setIpAddress(pop.getIpAddress());
//
		return po;
	}

	@Override
	public String createPO(PurchaseOrderTemp pot)
			throws JsonParseException, JsonMappingException, IOException, SupplyMintException {

		ResponseEntity<AppResponse> responseEntity = null;
		PurchaseOrder po;
		List<PurchaseOrder> poList = new ArrayList<PurchaseOrder>();

		OffsetDateTime currentTime = OffsetDateTime.now();
		currentTime = currentTime.plusHours(new Long(5));
		currentTime = currentTime.plusMinutes(new Long(30));
		String ipAddress = InetAddress.getLocalHost().getHostAddress().trim().toString();

		// data found
		if (!CodeUtils.isEmpty(request)) {
			pot.setCreatedBy(request.getAttribute("email").toString());
		}
		pot.setCreatedTime(currentTime);
		pot.setIpAddress(ipAddress);
		pot.setActive(PurchaseOrderMetaData.POStatus.ACTIVE);
		pot.setStatus(PurchaseOrderMetaData.POStatus.APPROVED_STATUS);

		String poReferenceId = UUID.randomUUID().toString();
		int intgLineId = CodeUtils.getEightDigitRandomNumber();
		String setHeaderId = "";
		List<String> setHeaderIdList = new ArrayList<String>();
		String result = "";

		List<PurchaseOrderLineItem> pid = pot.getPol();

		AtomicInteger count = new AtomicInteger(1);
		AtomicInteger headerCount = new AtomicInteger(1);

		if (!CodeUtils.isEmpty(request)) {
			pot.setUserId(Integer.parseInt(request.getAttribute("id").toString()));
		}
		log.info("order date : " + pot.getOrderDate());
		pot.setOrderNo(CodeUtils.getPOStringOrderDate() + "/" + CodeUtils.getEightDigitRandomNumber());

		result = pot.getOrderNo() + ",";
		// setting combination of size and color
		for (int i = 0; i < pid.size(); i++) {
			setHeaderId = pot.getOrderNo() + "_" + headerCount.get();
			setHeaderIdList.add(setHeaderId);
			headerCount.getAndIncrement();
			for (int j = 0; j < pid.get(i).getSizes().size(); j++) {
				for (int k = 0; k < pid.get(i).getColors().size(); k++) {

					if (count.get() != 1)
						++intgLineId;

					po = new PurchaseOrder();
					po = setPurchaseOrder(pot, pid.get(i), poReferenceId,
							CodeUtils.getPOStringOrderDate() + "/" + String.valueOf(intgLineId), setHeaderId);

					po.setcCode5(pid.get(i).getSizes().get(j).getCode());
					po.setcName5(pid.get(i).getSizes().get(j).getCname());
					po.setSetRatio(pid.get(i).getRatio().get(j));
					po.setcCode6(pid.get(i).getColors().get(k).getCode());
					po.setcName6(pid.get(i).getColors().get(k).getCname());
					count.getAndIncrement();
					poList.add(po);
				}
			}
		}

		int[] poResult = new int[poList.size()];
		for (int i = 0; i < poList.size(); i++) {
			poResult[i] = poMapper.createPO(poList.get(i));
//			log.info("order date after setting : " + poList.get(i).getOrderDate());
		}

		result = result + Arrays.toString(poResult);

		ArrayNode arrNode = getMapper().convertValue(poList, ArrayNode.class);
		ObjectNode poJson = getMapper().createObjectNode();
		poJson.put("poData", arrNode);

		if (!Arrays.toString(poResult).contains("0")) {

			PurchaseOrderConfiguration poc = this.getPOConfiguration(1);

			JsonNode headerJson = getMapper().readTree(poc.getHeaders());
			String headerType = getMapper().treeToValue(headerJson.get("headerType"), String.class);

			ResponseEntity<AppResponse> responseEntityData = null;
			String temp = arrNode.toString();

			responseEntityData = sendPODetailsOnRestAPI(headerType, poJson, poc);

			String responseEntityStatus = responseEntityData.getBody().getStatus();
			log.info("response entity message :" + responseEntityData.getBody().getData().get(CodeUtils.RESPONSE_MSG));
			log.info("response calling :" + responseEntityStatus);
			if (responseEntityStatus.contains(CodeUtils.RESPONSE_SUCCESS_CODE)) {
				log.info("response true");
				String temp2 = arrNode.asText();
				PurchaseOrderAudit poa = new PurchaseOrderAudit();
				poa.setUrl(poc.getUrl());
				poa.setHeaders(headerType);
				poa.setBody(poJson.toString());
				poa.setPoReferenceId(poList.get(0).getPoReferenceId());
				poa.setHttpStatus(Integer.parseInt(responseEntityData.getBody().getStatus()));
				poa.setHttpMessage(responseEntityData.getBody().getData().get(CodeUtils.RESPONSE_MSG).toString());
				log.info("response completed@");
				int poaResult = insertPOAudit(poa);
				log.info("Audit Result :" + poaResult);
			}
		}
		return result;
	}

	@Override
	public int sendSMPartentPO(PurchaseOrderParent pop)
			throws JsonParseException, JsonMappingException, IOException, SupplyMintException {

		ResponseEntity<AppResponse> responseEntity = null;
		PurchaseOrder po;
		List<PurchaseOrder> poList = new ArrayList<PurchaseOrder>();
		Set auditSet = new HashSet<String>();

		int intgLineId = CodeUtils.getEightDigitRandomNumber();
		int key = CodeUtils.getSixDigitRandomNumber();
		String poReferenceId = UUID.randomUUID().toString();

		String setHeaderId = "";
		int result = 0;

		AtomicInteger count = new AtomicInteger(1);
		AtomicInteger headerCount = new AtomicInteger(1);

		// setting combination of size and color
		for (int i = 0; i < pop.getPol().size(); i++) {

			if (CodeUtils.isEmpty(pop.getPrevOrderNo())) {
				setHeaderId = pop.getOrderNo() + "_" + headerCount.get();
			} else {
				setHeaderId = pop.getPrevOrderNo() + "_" + headerCount.get();
			}

			headerCount.getAndIncrement();
			for (int j = 0; j < pop.getPol().get(i).getSizes().size(); j++) {
				for (int k = 0; k < pop.getPol().get(i).getColors().size(); k++) {

					if (count.get() != 1) {
						++intgLineId;
						++key;
					}

					po = new PurchaseOrder();
					po = setSMPartnerPO(pop, pop.getPol().get(i), poReferenceId,
							CodeUtils.getPODateString() + "/" + String.valueOf(intgLineId), key,
							pop.getPol().get(i).getRatios().get(j).getRatio());

					po.setcCode5(pop.getPol().get(i).getSizes().get(j).getCode());
					po.setcName5(pop.getPol().get(i).getSizes().get(j).getCname());
					po.setSetRatio(pop.getPol().get(i).getRatios().get(j).getRatio());
					po.setcCode6(pop.getPol().get(i).getColors().get(k).getCode());
					po.setcName6(pop.getPol().get(i).getColors().get(k).getCname());

					po.setItemId(pop.getPol().get(i).getItemId());

					count.getAndIncrement();
					poList.add(po);
				}
			}
		}

		ArrayNode arrNode = getMapper().convertValue(poList, ArrayNode.class);
		ObjectNode poJson = getMapper().createObjectNode();
		poJson.put("poData", arrNode);

		PurchaseOrderConfiguration poc = this.getPOConfiguration(1);

		JsonNode headerJson = getMapper().readTree(poc.getHeaders());
		String headerType = getMapper().treeToValue(headerJson.get("headerType"), String.class);

		ResponseEntity<AppResponse> responseEntityData = null;

		responseEntityData = sendPODetailsOnRestAPI(headerType, poJson, poc);

		if (!CodeUtils.isEmpty(responseEntityData)) {
			String responseEntityStatus = responseEntityData.getBody().getStatus();
			log.info("response entity message :" + responseEntityData.getBody().getData().get(CodeUtils.RESPONSE_MSG));
			log.info("error code response entity message :" + responseEntityData.getBody().getError().get("errorCode"));
			log.info("error message response entity message :"
					+ responseEntityData.getBody().getError().get("errorMessage"));
			log.info("response calling :" + responseEntityStatus);
			if (responseEntityStatus.contains(CodeUtils.RESPONSE_SUCCESS_CODE)) {
				log.info("response true");
				PurchaseOrderAudit poa = new PurchaseOrderAudit();
				poa.setUrl(poc.getUrl());
				poa.setHeaders(headerType);
//			poa.setBody(poJson.toString());
				poa.setPoReferenceId(pop.getOrderNo());
				poa.setHttpStatus(Integer.parseInt(responseEntityData.getBody().getStatus()));
				poa.setHttpMessage(responseEntityData.getBody().getData().get(CodeUtils.RESPONSE_MSG).toString());
				log.info("response completed@");

				try {
					result = insertPOAudit(poa);
				} catch (MyBatisSystemException ex) {
					log.info(ex.getMessage());

				} catch (DataAccessException ex) {
					log.info(ex.getMessage());

				}

				log.info("Audit Result :" + result);
			}
		}
		return result;
	}

	@Override
	public ResponseEntity<AppResponse> sendPODetailsOnRestAPI(String headerType, ObjectNode poNode,
			PurchaseOrderConfiguration poc) throws SupplyMintException {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<AppResponse> responseEntity = null;
		try {
			String authToken = request.getHeader("X-Auth-Token");
//			String authToken = poNode.get("token").textValue();
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set("X-AUTH-TOKEN", authToken);
//			log.info("x auth token :" + authToken);
			HttpEntity<String> request = new HttpEntity<String>(poNode.toString(), headers);
			responseEntity = restTemplate.exchange(poc.getUrl(), HttpMethod.POST, request, AppResponse.class);
			log.info("response entity :" + responseEntity);
		} catch (Throwable t) {
			throw new SupplyMintException(t.getMessage());
		} finally {
			return responseEntity;
		}
	}

	public ResponseEntity<AppResponse> sendFMCGPODetailsOnRestAPI(String headerType, ObjectNode poNode,
			PurchaseOrderConfiguration poc) throws SupplyMintException {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<AppResponse> responseEntity = null;
		try {
//			String authToken = request.getHeader("X-Auth-Token");
			String authToken = poNode.get("token").textValue();
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set("X-AUTH-TOKEN", authToken);
//			log.info("x auth token :" + authToken);
			HttpEntity<String> request = new HttpEntity<String>(poNode.toString(), headers);
			responseEntity = restTemplate.exchange(poc.getUrl(), HttpMethod.POST, request, AppResponse.class);
			log.info("response entity :" + responseEntity);
		} catch (Throwable t) {
			throw new SupplyMintException(t.getMessage());
		} finally {
			return responseEntity;
		}
	}

	public String buildPOReport(PurchaseOrderTemp pot, String setHeaderIdList) throws SupplyMintException {
		List<PurchaseOrder> setHeaderList = null;
		List<List<PurchaseOrder>> poList = new ArrayList<List<PurchaseOrder>>();
		S3BucketInfo s3BucketInfo = null;
		String enterpriseName = null;
		String enterpriseEmail = null;
		String Download_PDF = "DOWNLOAD_PDF";

		String fileName = null;
		String filePath = null;
		File saveBucket = null;
		String extension = "pdf";
		String pattern;

		String[] setHeaderArray = setHeaderIdList.split(",");

		for (String setHeaderId : setHeaderArray) {
			setHeaderList = null;
			setHeaderList = findPOBySetHeader(setHeaderId);
			poList.add(setHeaderList);
		}

		if (!CodeUtils.isEmpty(poList)) {
			log.info("PO size :" + poList.size());
		}

		ClassLoader classloader = Thread.currentThread().getContextClassLoader();
		InputStream invoiceJRXMLStream = classloader.getResourceAsStream("jrxml/PurchaseOrder.jrxml");

		// classloader.
//        invoiceParameters.put("imagesDir", request.getSession().getServletContext().getRealPath("/resources/images/logo.jpg"));

		if (!CodeUtils.isEmpty(request)) {
			enterpriseName = request.getAttribute("name").toString();
			enterpriseEmail = request.getAttribute("email").toString();
//			enterpriseName = "Admin";
		}

		log.debug("setting all parameters for invoice.....{}");

		List<JasperPrint> jasperPrintList = new ArrayList<JasperPrint>();

		JasperReport jasperFilePath = null;

		try {
			jasperFilePath = JasperCompileManager.compileReport(invoiceJRXMLStream);
			Map<String, Object> invoiceParameters = new HashMap<String, Object>();

//			invoiceParameters.put("enterpriseName",  enterpriseName);
//			invoiceParameters.put("fullAddress",  "");
//			invoiceParameters.put("gstNo",  "");
//			invoiceParameters.put("phone",  "");
//			invoiceParameters.put("email",  value);
			invoiceParameters.put("supplier", pot.getSupplierName());
			invoiceParameters.put("address", pot.getSupplierAddress());
			invoiceParameters.put("gstin", pot.getGstNo());
			invoiceParameters.put("orderFor", enterpriseName);
			invoiceParameters.put("stateCode", pot.getStateCode());
			invoiceParameters.put("distribution", "");
			invoiceParameters.put("company", enterpriseName);
			invoiceParameters.put("orderNo", setHeaderList.get(setHeaderList.size() - 1).getOrderNo());
			invoiceParameters.put("documentNo", "");
			invoiceParameters.put("consigment", "");
			invoiceParameters.put("date", setHeaderList.get(setHeaderList.size() - 1).getOrderDate());
			invoiceParameters.put("currency", "Rupees");
			invoiceParameters.put("exchangeRate", "1.000");
			invoiceParameters.put("department", "");
			invoiceParameters.put("artical", "");
			invoiceParameters.put("brand", pot.getPol().get(0).getcName2());

			// set
//			invoiceParameters.put("barCode",  value);
//			invoiceParameters.put("noColor",  value);
//			invoiceParameters.put("singleQty",  value);
//			invoiceParameters.put("totalSet",  value);
//			invoiceParameters.put("orderQty",  value);
//			invoiceParameters.put("setValue",  value);

//			invoiceParameters.put("barCode",  value);
//			invoiceParameters.put("hsn",  value);
//			invoiceParameters.put("description",  value);
//			invoiceParameters.put("color",  value);
//			invoiceParameters.put("size",  value);
//			invoiceParameters.put("desc",  value);
//			invoiceParameters.put("nbPer",  value);
//			invoiceParameters.put("mrpBarcode",  value);
//			invoiceParameters.put("orderQty",  value);
//			invoiceParameters.put("rateBarcode",  value);
//			invoiceParameters.put("basicAmt",  value);

//			invoiceParameters.put("gstNo",  value);
//			invoiceParameters.put("preparedBy",  value);
//			invoiceParameters.put("modifiedBy",  value);
//			invoiceParameters.put("printDate",  value);
//			invoiceParameters.put("PAGE_NUMBER",  value);
		} catch (JRException ex) {
			ex.printStackTrace();
			throw new SupplyMintException(ex.getMessage());

		} catch (Exception e) {
			throw new SupplyMintException(e.getMessage());

		}

		return null;
	}

	public List<PurchaseOrder> findPOBySetHeader(String setHeader) {
		return poMapper.findPOBySetHeader(setHeader);
	}

	@Override
	public List<PurchaseOrderParent> findAll(Integer offset, Integer pagesize, String orgId) {
		return poMapper.findAllPOHistory(offset, pagesize, orgId);
	}

	@Override
	public List<Map<String, String>> findAllCatDescUdf(Integer offset, Integer pagesize, String header) {
		if (header.contains("CAT"))
			return poMapper.findAllCatValues(offset, pagesize, header);
		else
			return poMapper.findAllDescUdf(offset, pagesize, header);
	}

	@Override
	public Integer count(String orgId) {
		return poMapper.countPOHistory(orgId);
	}

	@Override
	public Integer countCatDescUdf(String header) {

		if (header.contains("CAT"))
			return poMapper.countCatValues(header);
		else
			return poMapper.countDescUDFValues(header);
	}

	@Override
	public List<PurchaseOrderParent> filter(Integer offset, Integer pagesize, String orderNo, String orderDate,
			String validFrom, String validTo, String slCode, String slName, String slCityName, String hl1name,
			String hl2name, String hl3name, String orgId) {
		return poMapper.findAllPOHistoryFilter(orderNo, orderDate, validFrom, validTo, slCode, slName, slCityName,
				hl1name, hl2name, hl3name, offset, pagesize, orgId);

	}

	@Override
	public Integer filterCount(String orderNo, String orderDate, String validFrom, String validTo, String slCode,
			String slName, String slCityName, String hl1name, String hl2name, String hl3name, String orgId) {
		return poMapper.countPOHistoryFilter(orderNo, orderDate, validFrom, validTo, slCode, slName, slCityName,
				hl1name, hl2name, hl3name, orgId);
	}

	@Override
	public List<PurchaseOrderParent> search(Integer offset, Integer pagesize, String search, String orgId) {
		return poMapper.searchAllPOHistory(offset, pagesize, search, orgId);
	}

	@Override
	public List<Map<String, String>> searchCatDescUDFName(Integer offset, Integer pagesize, String search,
			String header) {
		if (header.contains("CAT"))
			return poMapper.searchCatValues(offset, pagesize, search, header);
		else
			return poMapper.searchDescUDFValues(offset, pagesize, search, header);
	}

	@Override
	public Integer searchCount(String search, String orgId) {
		return poMapper.searchPOHistoryCount(search, orgId);
	}

	@Override
	public List<DeptSizeData> getSizeByDept(String dept) {
		return poMapper.getSizeByDept(dept);
	}

	@Override
	public List<DeptSizeData> getOtherSizeByDept(String dept) {
		return poMapper.getOtherSizeByDept(dept);
	}

	@Override
	public String updateUDFMapping(JsonNode udfList) throws SupplyMintException {
		boolean flagOfDisplayName = true;
		boolean flagOfOrderBy = true;
		int index = 0;
		int i = 0;
		String displayName = null;
		String orderBy = null;
		List<UDF_SettingsMaster> udfSettingsMasters = null;

//		int[] result = new int[udfList.size()];
		String result = "";
		if (udfList != null) {
			Set<String> setOfCategoryDisplayName = new HashSet<>();
			Set<Integer> setOfCategoryOrderBy = new HashSet<>();
			String[] getAllPayloadData = new String[udfList.size()];
			for (JsonNode jsonNode : udfList) {
				index++;
				UDF_SettingsMaster checkValidation = getMapper().convertValue(jsonNode, UDF_SettingsMaster.class);
				if (index == 1) {
					udfSettingsMasters = getUDFMappingData();
				}
				displayName = checkValidation.getDisplayName();
				orderBy = checkValidation.getOrderBy();
				if (displayName != null) {
					displayName = displayName.trim();
				}
				if (orderBy != null) {
					orderBy = orderBy.trim();
				}

				if (!CodeUtils.isEmpty(displayName)) {
					flagOfDisplayName = setOfCategoryDisplayName.add(displayName.toLowerCase());
				}
				if (!CodeUtils.isEmpty(orderBy)) {
					flagOfOrderBy = setOfCategoryOrderBy.add(Integer.parseInt(orderBy));
				}
				if (!flagOfDisplayName) {
					throw new SupplyMintException(displayNameDeptItemUDFValidationMSG);
				} else if (!flagOfOrderBy) {
					throw new SupplyMintException(orderByDeptItemUDFValidationMSG);
				}
				getAllPayloadData[i] = checkValidation.getUdfType();
				i++;
				if (index == udfList.size()) {
					for (UDF_SettingsMaster udfSetting : udfSettingsMasters) {
						if (!Arrays.asList(getAllPayloadData).contains(udfSetting.getUdfType())) {

							if (udfSetting.getDisplayName() != null) {
								flagOfDisplayName = setOfCategoryDisplayName
										.add(udfSetting.getDisplayName().toLowerCase().trim());
							}
							if (!CodeUtils.isEmpty(udfSetting.getOrderBy())) {
								flagOfOrderBy = setOfCategoryOrderBy
										.add(Integer.parseInt(udfSetting.getOrderBy().trim()));
							}
							if (!flagOfDisplayName) {
								throw new SupplyMintException(displayNameDeptItemUDFValidationMSG);
							} else if (!flagOfOrderBy) {
								throw new SupplyMintException(orderByDeptItemUDFValidationMSG);
							}
						}

					}
				}

			}
			for (JsonNode jsonNode : udfList) {
				UDF_SettingsMaster udf = getMapper().convertValue(jsonNode, UDF_SettingsMaster.class);

				if (!CodeUtils.isEmpty(udf.getDisplayName())) {
					udf.setDisplayName(udf.getDisplayName().trim());
				}
				if (!CodeUtils.isEmpty(udf.getOrderBy())) {
					udf.setOrderBy(udf.getOrderBy().trim());
				}
				int udfResult = poMapper.updateUDFMap(udf.getDisplayName(), udf.getIsCompulsary(), udf.getOrderBy(),
						udf.getUdfType(), udf.getIsLov());

				result = result + udfResult + ",";
			}
		}
		return result;
	}

	/*
	 * @Override public String updateUDFMapping(JsonNode udfList) throws
	 * JsonProcessingException, SupplyMintException {
	 * 
	 * Set<String> displayNames = new HashSet<String>(); boolean isValidDisplayName
	 * = true; boolean isValidOrderBy = true; String result = ""; UDF_SettingsMaster
	 * settingsMaster = null;
	 * 
	 * List<UDF_SettingsMaster> udfSettingList = new
	 * ArrayList<UDF_SettingsMaster>();
	 * 
	 * try { if (!CodeUtils.isEmpty(udfList)) {
	 * 
	 * List<String> udfSettingType = new ArrayList<String>(); List<String>
	 * udfSettingDisplayName = new ArrayList<String>(); List<String>
	 * udfSettingOrderBy = new ArrayList<String>();
	 * 
	 * for (JsonNode jsonNode : udfList) { settingsMaster =
	 * getMapper().treeToValue(jsonNode, UDF_SettingsMaster.class);
	 * 
	 * if (!CodeUtils.isEmpty(settingsMaster.getDisplayName()) &&
	 * !CodeUtils.isEmpty(settingsMaster.getOrderBy())) {
	 * 
	 * udfSettingType.add(settingsMaster.getUdfType());
	 * udfSettingDisplayName.add(String.valueOf(settingsMaster.getDisplayName().trim
	 * ())); udfSettingOrderBy.add(String.valueOf(Integer.parseInt(settingsMaster.
	 * getOrderBy())));
	 * 
	 * settingsMaster.setDisplayName(String.valueOf(settingsMaster.getDisplayName().
	 * trim()));
	 * settingsMaster.setOrderBy(String.valueOf(Integer.parseInt(settingsMaster.
	 * getOrderBy().trim())));
	 * 
	 * udfSettingList.add(settingsMaster);
	 * 
	 * }
	 * 
	 * Set<String> names = new HashSet<String>(); Set<Integer> orders = new
	 * HashSet<Integer>();
	 * 
	 * if (!CodeUtils.isEmpty(udfSettingType)) {
	 * 
	 * names = poMapper.getDisplayNames(udfSettingType); orders =
	 * poMapper.getUdfSettingsOrderBy(udfSettingType);
	 * 
	 * } else {
	 * 
	 * names = poMapper.getAllDisplayNames(); orders =
	 * poMapper.getAllUdfSettingsOrderBy();
	 * 
	 * }
	 * 
	 * for (int i = 0; i < udfSettingType.size(); i++) { if
	 * (!CodeUtils.isEmpty(udfSettingDisplayName)) { isValidDisplayName =
	 * (names.add(udfSettingDisplayName.get(i))) ? true : false; }
	 * 
	 * if (!isValidDisplayName) { throw new
	 * SupplyMintException("This display name is already exist"); }
	 * 
	 * if (!CodeUtils.isEmpty(udfSettingOrderBy)) { isValidOrderBy =
	 * (names.add(udfSettingOrderBy.get(i))) ? true : false; }
	 * 
	 * if (!isValidOrderBy) { throw new
	 * SupplyMintException("This order by value is already exist"); }
	 * 
	 * }
	 * 
	 * }
	 * 
	 * } } catch (DataAccessException dataAccessException) { throw
	 * dataAccessException; } catch (JsonProcessingException
	 * jsonProcessingException) { throw jsonProcessingException; } return null; }
	 */

	@Override
	public String updateItemCatDescUDF(JsonNode catDescNode) throws JsonProcessingException, SupplyMintException {
//		int[] result = new int[udfList.size()];
		String result = "";
		List<DeptItemUDFSettings> data = null;
		boolean flagOfDisplayName;
		boolean flagOfOrderBy;
		int index = 0;
		int i = 0;
		String orderBy = null;
		String displayName = null;
		List<DeptItemUDFSettings> deptItemUDFSettingsData = null;
		JsonNode categoryList = catDescNode.get("catDescKey").get("categories");

		JsonNode udfList = catDescNode.get("catDescKey").get("cat_desc_udf");

//		tempData = getMapper().treeToValue(piItem.get("piKey"), PurchaseIndent.class);
		/* @Author Bishnu -> Line of code allows user to set the action with */
		if (categoryList != null) {
			flagOfDisplayName = true;
			flagOfOrderBy = true;
			index = 0;
			i = 0;
			Set<String> setOfCategoryDisplayName = new HashSet<>();
			Set<Integer> setOfCategoryOrderBy = new HashSet<>();
			String[] getAllPayloadData = new String[categoryList.size()];
			for (JsonNode categoryNode : categoryList) {
				index++;
				DeptItemUDFSettings checkValidation = getMapper().treeToValue(categoryNode, DeptItemUDFSettings.class);
				if (index == 1) {
					deptItemUDFSettingsData = getDepartmentItemUDFByHl3Name(checkValidation.getHl3code());
				}
				displayName = checkValidation.getDisplayName();
				orderBy = checkValidation.getOrderBy();
				if (displayName != null) {
					displayName = displayName.trim();
				}
				if (orderBy != null) {
					orderBy = orderBy.trim();
				}

				if (!CodeUtils.isEmpty(displayName)) {
					flagOfDisplayName = setOfCategoryDisplayName.add(displayName.toLowerCase());
				}
				if (!CodeUtils.isEmpty(orderBy)) {
					flagOfOrderBy = setOfCategoryOrderBy.add(Integer.parseInt(orderBy));
				}
				if (!flagOfDisplayName) {
					throw new SupplyMintException(displayNameDeptItemUDFValidationMSG);
				} else if (!flagOfOrderBy) {
					throw new SupplyMintException(orderByDeptItemUDFValidationMSG);
				}
				getAllPayloadData[i] = checkValidation.getCat_desc_udf();
				i++;
				if (index == categoryList.size()) {
					for (DeptItemUDFSettings deptItemUDFSettings : deptItemUDFSettingsData) {
						if (!Arrays.asList(getAllPayloadData).contains(deptItemUDFSettings.getCat_desc_udf())) {
							if (deptItemUDFSettings.getCat_desc_udf_type().equalsIgnoreCase("C")
									|| deptItemUDFSettings.getCat_desc_udf_type().equalsIgnoreCase("D")) {
								if (deptItemUDFSettings.getDisplayName() != null) {
									flagOfDisplayName = setOfCategoryDisplayName
											.add(deptItemUDFSettings.getDisplayName().toLowerCase().trim());
								}
								if (deptItemUDFSettings.getOrderBy() != null) {
									flagOfOrderBy = setOfCategoryOrderBy
											.add(Integer.parseInt(deptItemUDFSettings.getOrderBy().trim()));
								}
								if (!flagOfDisplayName) {
									throw new SupplyMintException(displayNameDeptItemUDFValidationMSG);
								} else if (!flagOfOrderBy) {
									throw new SupplyMintException(orderByDeptItemUDFValidationMSG);
								}
							}

						}
					}
				}
			}

			for (JsonNode categoryNode : categoryList) {
				DeptItemUDFSettings category = getMapper().treeToValue(categoryNode, DeptItemUDFSettings.class);
				int categoryResult = poMapper.updateDeptItemUDF(category);
				result = result + categoryResult + ",";
			}
		}
		if (udfList != null) {
			flagOfDisplayName = true;
			flagOfOrderBy = true;
			index = 0;
			i = 0;
			Set<String> setOfUdfDisplayName = new HashSet<>();
			Set<Integer> setOfUdfOrderBy = new HashSet<>();
			String[] getAllPayloadData = new String[udfList.size()];

			for (JsonNode udfNode : udfList) {
				index++;
				DeptItemUDFSettings checkValidation = getMapper().treeToValue(udfNode, DeptItemUDFSettings.class);
				if (index == 1) {
					deptItemUDFSettingsData = getDepartmentItemUDFByHl3Name(checkValidation.getHl3code());
				}
				displayName = checkValidation.getDisplayName();
				orderBy = checkValidation.getOrderBy();
				if (displayName != null) {
					displayName = displayName.trim();
				}
				if (orderBy != null) {
					orderBy = orderBy.trim();
				}

				if (!CodeUtils.isEmpty(displayName)) {
					flagOfDisplayName = setOfUdfDisplayName.add(displayName.toLowerCase());
				}
				if (!CodeUtils.isEmpty(orderBy)) {
					flagOfOrderBy = setOfUdfOrderBy.add(Integer.parseInt(orderBy));
				}
				if (!flagOfDisplayName) {
					throw new SupplyMintException(displayNameDeptItemUDFValidationMSG);
				} else if (!flagOfOrderBy) {
					throw new SupplyMintException(orderByDeptItemUDFValidationMSG);
				}
				getAllPayloadData[i] = checkValidation.getCat_desc_udf();
				i++;
				if (index == udfList.size()) {
					for (DeptItemUDFSettings deptItemUDFSettings : deptItemUDFSettingsData) {
						if (!Arrays.asList(getAllPayloadData).contains(deptItemUDFSettings.getCat_desc_udf())) {
							if (deptItemUDFSettings.getCat_desc_udf_type().equalsIgnoreCase("U")) {
								if (deptItemUDFSettings.getDisplayName() != null) {
									flagOfDisplayName = setOfUdfDisplayName
											.add(deptItemUDFSettings.getDisplayName().toLowerCase().trim());
								}
								if (deptItemUDFSettings.getOrderBy() != null) {
									flagOfOrderBy = setOfUdfOrderBy
											.add(Integer.parseInt(deptItemUDFSettings.getOrderBy().trim()));
								}
								if (!flagOfDisplayName) {
									throw new SupplyMintException(displayNameDeptItemUDFValidationMSG);
								} else if (!flagOfOrderBy) {
									throw new SupplyMintException(orderByDeptItemUDFValidationMSG);
								}
							}
						}
					}
				}
			}
			// End
			for (JsonNode udfNode : udfList) {

				DeptItemUDFSettings category = getMapper().treeToValue(udfNode, DeptItemUDFSettings.class);
				int udfResult = poMapper.updateDeptItemUDF(category);
				result = result + udfResult + ",";
			}
		}
		return result;

	}

	@Override
	public ObjectNode getUDFMappingNode(List<UDF_SettingsMaster> udfMappingList) {
		ObjectNode udfNode = getMapper().createObjectNode();
		ArrayNode arrNode = getMapper().createArrayNode();
		AtomicInteger count = new AtomicInteger(1);

		udfMappingList.stream().forEach(e -> {

			boolean valueChecked = true;
			if (e.getIsCompulsary() == 'N') {
				valueChecked = false;
			}
			ObjectNode tempNode = getMapper().createObjectNode();
			tempNode.put(PurchaseIndentMetaData.SetUDF.ID, count.get());
			tempNode.put(PurchaseIndentMetaData.SetUDF.UDF_TYPE, e.getUdfType());
			tempNode.put(PurchaseIndentMetaData.SetUDF.DISPAYNAME, e.getDisplayName());
			tempNode.put(PurchaseIndentMetaData.SetUDF.IS_COMPULSARY, String.valueOf(e.getIsCompulsary()));
			tempNode.put(PurchaseIndentMetaData.SetUDF.ORDER_BY, e.getOrderBy());
			tempNode.put(PurchaseIndentMetaData.SetUDF.CHECKED, valueChecked);

			arrNode.add(tempNode);
			count.getAndIncrement();
		});

		udfNode.put(CodeUtils.RESPONSE, arrNode);

		return udfNode;
	}

	@Override
	public Integer getInvSetPOItemUDFDataRecord(String udfType) {
		return poMapper.getInvSetPOItemUDFDataRecord(udfType);
	}

	@Override
	public List<InvSetUDF> getInvSetPOItemUDFData(String udfType) {

		return poMapper.getInvSetPOItemUDFData(udfType);
	}

	@Override
	public List<InvSetUDF> filterInvSetPOItemUDFData(String udfType, String code, String name) {
		return poMapper.filterInvSetPOItemUDFData(udfType, code, name);
	}

	@Override
	public Integer filterInvSetPOItemUDFDataRecord(String udfType, String code, String name) {

		return poMapper.filterInvSetPOItemUDFDataRecord(udfType, code, name);
	}

	@Override
	public List<InvSetUDF> searchInvSetPOItemUDFData(String udfType, String search) {

		return poMapper.searchInvSetPOItemUDFData(udfType, search);
	}

	@Override
	public Integer searchInvSetPOItemUDFDataRecord(String udfType, String search) {
		return poMapper.searchInvSetPOItemUDFDataRecord(udfType, search);
	}

	@Override
	public ObjectNode getPurchaseOrderNode(List<PurchaseOrder> purchaseOrderList) {
		ObjectNode udfNode = getMapper().createObjectNode();
		ArrayNode arrNode = getMapper().createArrayNode();

		purchaseOrderList.stream().forEach(po -> {

			ObjectNode tempNode = getMapper().createObjectNode();

			tempNode.put("vendorName", po.getVendorName());
			tempNode.put("intGCode", po.getIntGCode());
			tempNode.put("intGHeaderId", po.getIntGHeaderId());
			tempNode.put("intGLineId", po.getIntGLineId());
			tempNode.put("orderNo", po.getOrderNo());
			tempNode.put("orderDate", (po.getOrderDate() != null) ? po.getOrderDate().toString() : null);
			tempNode.put("vendorId", po.getVendorId());
			tempNode.put("transporterId", po.getTransporterId());
			tempNode.put("agentId", po.getAgentId());
			tempNode.put("agentRate", po.getAgentRate());
			tempNode.put("poRemarks", po.getPoRemarks());
			tempNode.put("createdById", po.getCreatedById());
			tempNode.put("validFrom", (po.getValidFrom() != null) ? po.getValidFrom().toString() : null);
			tempNode.put("validTo", (po.getValidTo() != null) ? po.getValidTo().toString() : null);
			tempNode.put("merchandiserId", po.getMerchandiserId());
			tempNode.put("siteId", po.getSiteId());
			tempNode.put("itemId", po.getItemId());
			tempNode.put("setRemarks", po.getSetRemarks());
			tempNode.put("setRatio", po.getSetRatio());
			tempNode.put("articleId", po.getArticleId());
			tempNode.put("itemName", po.getItemName());
			tempNode.put("cCode1", po.getcCode1());
			tempNode.put("cCode2", po.getcCode2());
			tempNode.put("cCode3", po.getcCode3());
			tempNode.put("cCode4", po.getcCode4());
			tempNode.put("cCode5", po.getcCode5());
			tempNode.put("cCode6", po.getcCode6());
			tempNode.put("cName1", po.getcName1());
			tempNode.put("cName2", po.getcName2());
			tempNode.put("cName3", po.getcName3());
			tempNode.put("cName4", po.getcName4());
			tempNode.put("cName5", po.getcName5());
			tempNode.put("cName6", po.getcName6());
			tempNode.put("desc1", po.getDesc1());
			tempNode.put("desc2", po.getDesc2());
			tempNode.put("desc3", po.getDesc3());
			tempNode.put("desc4", po.getDesc4());
			tempNode.put("desc5", po.getDesc5());
			tempNode.put("desc6", po.getDesc6());
			tempNode.put("mrp", po.getMrp());
			tempNode.put("listedMRP", po.getListedMRP());
			tempNode.put("wsp", po.getWsp());
			tempNode.put("uom", po.getUom());
			tempNode.put("materialType", po.getMaterialType());
			tempNode.put("qty", po.getQty());
			tempNode.put("rate", po.getRate());
			tempNode.put("poItemRemarks", po.getPoItemRemarks());
			tempNode.put("isImported", po.getIsImported());
			tempNode.put("termCode", po.getTermCode());
			tempNode.put("isValidated", po.getIsValidated());
			tempNode.put("validationError", po.getValidationError());
			tempNode.put("docCode", po.getDocCode());
			tempNode.put("setHeaderId", po.getSetHeaderId());
			tempNode.put("key", po.getKey());
			tempNode.put("poudfStrin01", po.getPoudfStrin01());
			tempNode.put("poudfStrin02", po.getPoudfStrin02());
			tempNode.put("poudfStrin03", po.getPoudfStrin03());
			tempNode.put("poudfStrin04", po.getPoudfStrin04());
			tempNode.put("poudfStrin05", po.getPoudfStrin05());
			tempNode.put("poudfStrin06", po.getPoudfStrin06());
			tempNode.put("poudfStrin07", po.getPoudfStrin07());
			tempNode.put("poudfStrin08", po.getPoudfStrin08());
			tempNode.put("poudfStrin09", po.getPoudfStrin09());
			tempNode.put("poudfStrin010", po.getPoudfStrin010());
			tempNode.put("poudfNum01", po.getPoudfNum01());
			tempNode.put("poudfNum02", po.getPoudfNum02());
			tempNode.put("poudfNum03", po.getPoudfNum03());
			tempNode.put("poudfNum04", po.getPoudfNum04());
			tempNode.put("poudfNum05", po.getPoudfNum05());
			tempNode.put("poudfDate01", (po.getPoudfDate01() != null) ? po.getPoudfDate01().toString() : null);
			tempNode.put("poudfDate02", (po.getPoudfDate02() != null) ? po.getPoudfDate02().toString() : null);
			tempNode.put("poudfDate03", (po.getPoudfDate03() != null) ? po.getPoudfDate03().toString() : null);
			tempNode.put("poudfDate04", (po.getPoudfDate04() != null) ? po.getPoudfDate04().toString() : null);
			tempNode.put("poudfDate05", (po.getPoudfDate05() != null) ? po.getPoudfDate05().toString() : null);
			tempNode.put("smUDFStrin01", po.getSmUDFStrin01());
			tempNode.put("smUDFStrin02", po.getSmUDFStrin02());
			tempNode.put("smUDFStrin03", po.getSmUDFStrin03());
			tempNode.put("smUDFStrin04", po.getSmUDFStrin04());
			tempNode.put("smUDFStrin05", po.getSmUDFStrin05());
			tempNode.put("smUDFStrin06", po.getSmUDFStrin06());
			tempNode.put("smUDFStrin07", po.getSmUDFStrin07());
			tempNode.put("smUDFStrin08", po.getSmUDFStrin08());
			tempNode.put("smUDFStrin09", po.getSmUDFStrin09());
			tempNode.put("smUDFStrin010", po.getSmUDFStrin010());
			tempNode.put("smUDFNum01", po.getSmUDFNum01());
			tempNode.put("smUDFNum02", po.getSmUDFNum02());
			tempNode.put("smUDFNum03", po.getSmUDFNum03());
			tempNode.put("smUDFNum04", po.getSmUDFNum04());
			tempNode.put("smUDFNum05", po.getSmUDFNum05());
			tempNode.put("smUDFDate01", (po.getSmUDFDate01() != null) ? po.getSmUDFDate01().toString() : null);
			tempNode.put("smUDFDate02", (po.getSmUDFDate02() != null) ? po.getSmUDFDate02().toString() : null);
			tempNode.put("smUDFDate03", (po.getSmUDFDate03() != null) ? po.getSmUDFDate03().toString() : null);
			tempNode.put("smUDFDate04", (po.getSmUDFDate04() != null) ? po.getSmUDFDate04().toString() : null);
			tempNode.put("smUDFDate05", (po.getSmUDFDate05() != null) ? po.getSmUDFDate05().toString() : null);
			tempNode.put("imUDFString01", po.getImUDFString01());
			tempNode.put("imUDFString02", po.getImUDFString02());
			tempNode.put("imUDFString03", po.getImUDFString03());
			tempNode.put("imUDFString04", po.getImUDFString04());
			tempNode.put("imUDFString05", po.getImUDFString05());
			tempNode.put("imUDFString06", po.getImUDFString06());
			tempNode.put("imUDFString07", po.getImUDFString07());
			tempNode.put("imUDFString08", po.getImUDFString08());
			tempNode.put("imUDFString09", po.getImUDFString09());
			tempNode.put("imUDFString010", po.getImUDFString010());
			tempNode.put("imUDFNum01", po.getImUDFNum01());
			tempNode.put("imUDFNum02", po.getImUDFNum02());
			tempNode.put("imUDFNum03", po.getImUDFNum03());
			tempNode.put("imUDFNum04", po.getImUDFNum04());
			tempNode.put("imUDFNum05", po.getImUDFNum05());
			tempNode.put("imUDFDate01", (po.getImUDFDate01() != null) ? po.getImUDFDate01().toString() : null);
			tempNode.put("imUDFDate02", (po.getImUDFDate02() != null) ? po.getImUDFDate02().toString() : null);
			tempNode.put("imUDFDate03", (po.getImUDFDate03() != null) ? po.getImUDFDate03().toString() : null);
			tempNode.put("imUDFDate04", (po.getImUDFDate04() != null) ? po.getImUDFDate04().toString() : null);
			tempNode.put("imUDFDate05", (po.getImUDFDate05() != null) ? po.getImUDFDate05().toString() : null);

			arrNode.add(tempNode);
		});

		udfNode.put(CodeUtils.RESPONSE, arrNode);

		return udfNode;
	}

	@Override
	public PurchaseOrderConfiguration getPOConfiguration(int cid) {
		return poMapper.getPOConfiguration(cid);
	}

	@Override
	public int insertPOAudit(PurchaseOrderAudit poa) {
		return poMapper.createPOAudit(poa);
	}

	@Override
	public List<IndtCatDescUDFList> getIndtCatDescUDFListData() {
		return poMapper.getIndtCatDescUDFListData();
	}

	@Override
	public List<IndtCatDescUDFList> getIndtCatDescUDFListActiveData() {
		return poMapper.getIndtCatDescUDFListActiveData();

	}

	/*
	 * @Override public List<DeptItemUDFSettings> getCategoriesByHl3Name(String
	 * hl3Name) { return poMapper.getCategoriesByHl3Name(hl3Name); }
	 * 
	 * @Override public List<DeptItemUDFSettings> getUDFByHl3Name(String hl3Name) {
	 * return poMapper.getUDFByHl3Name(hl3Name); }
	 * 
	 */
	@Override
	public List getDeptItemUDFByHl3Name(String hl3code) {
		return poMapper.getDeptItemUDFByHl3Name(hl3code);
	}

	@SuppressWarnings("deprecation")
	@Override
	public List<DeptItemUDFSettings> getDepartmentItemUDFByHl3Name(String hl3code) {
		List<DeptItemUDFSettings> data = null;

		data = getDeptItemUDFByHl3Name(hl3code);
		return data;
	}

	@Override
	public List<DeptItemUDFMappings> getItemUdfMappingByHl3NameAndUDF(int offSet, int pageSize, String hl3code,
			String cat_desc_udf, String description, String hl4code) {

		return poMapper.getItemUdfMappingByHl3NameAndUDF(offSet, pageSize, hl3code, cat_desc_udf, description, hl4code);
	}

	@Override
	public List<DeptItemUDFMappings> itemUdfMappingByHl3NameAndUDF(int offSet, int pageSize, String hl3code,
			String cat_desc_udf, String description, String hl4code) {

		return poMapper.getItemUdfMappingByHl3NameAndUDF(offSet, pageSize, hl3code, cat_desc_udf, description, hl4code);
	}

	@Override
	public List<DeptItemUDFMappings> getItemUDFMappingByHl3NameUDFEXT(int offSet, int pageSize, String hl3Name,
			String cat_desc_udf) {
		List<DeptItemUDFMappings> data = null;
		data = poMapper.getItemUdfMappingByHl3NameUDFEXT(offSet, pageSize, hl3Name, cat_desc_udf);
		return data;

	}

	@Override
	public ObjectNode getItemUDFMappingNode(List<DeptItemUDFMappings> data, String hl3code) {
		ObjectNode node = getMapper().createObjectNode();
		ArrayNode arrNode = getMapper().createArrayNode();
		data.stream().forEach(e -> {

			ObjectNode tempNode = getMapper().createObjectNode();

			tempNode.put(PurchaseOrderMetaData.ItemUdfMapping.ID, e.getId());
			tempNode.put(PurchaseOrderMetaData.ItemUdfMapping.hl3Code, hl3code);
			tempNode.put(PurchaseOrderMetaData.ItemUdfMapping.hl3Name, e.getHl3Name());
			tempNode.put(PurchaseOrderMetaData.ItemUdfMapping.DISPLAY_NAME, e.getDisplayName());
			tempNode.put(PurchaseOrderMetaData.ItemUdfMapping.DESCRIPTION, e.getDescription());
			tempNode.put(PurchaseOrderMetaData.ItemUdfMapping.MAP, e.getMap());
			tempNode.put(PurchaseOrderMetaData.ItemUdfMapping.EXT, e.getExt());

			arrNode.add(tempNode);

		});

		node.put(CodeUtils.RESPONSE, arrNode);

		return node;
	}

	@Override
	public int createInvSetUdf(InvSetUDF invSetUDF) {
		return poMapper.createInvSetUdf(invSetUDF);
	}

	@Override
	public int updateInvSetUdf(InvSetUDF invSetUDF) {
		return poMapper.updateInvSetUdf(invSetUDF);
	}

	@Override
	public int createAndUpdatePOItemUDF(JsonNode node) throws Exception {
		int result = 0;
		InvSetUDF invSetUDF = null;
		int checkUDFMappingValue = 0;
		try {

			JsonNode data = node.get("resultUDFType");
			String isInsert = node.get("isInsert").asText();
			if (isInsert.equalsIgnoreCase("true")) {
				if (!CodeUtils.isEmpty(data)) {

					// invSetUDF = getMapper().treeToValue(node, InvSetUDF.class);
					for (JsonNode jsonNode : data) {

						invSetUDF = getMapper().treeToValue(jsonNode, InvSetUDF.class);
						checkUDFMappingValue = poMapper.checkUDFMappingCreationTimeExistanceValidation(
								invSetUDF.getUdfType(), invSetUDF.getName());
						if (checkUDFMappingValue == 0) {
							result = createInvSetUdf(invSetUDF);
						} else {
							throw new SupplyMintException("this value is already exist");
						}
					}
				}

			} else if (isInsert.equalsIgnoreCase("false")) {
				if (!CodeUtils.isEmpty(data)) {

					// invSetUDF = getMapper().treeToValue(node, InvSetUDF.class);
					for (JsonNode jsonNode : data) {

						invSetUDF = getMapper().treeToValue(jsonNode, InvSetUDF.class);
						result = updateInvSetUdf(invSetUDF);
					}
				}
			}
		} catch (DataAccessException ex) {
			ex.printStackTrace();

			throw ex;
		} catch (Exception ex) {
			throw ex;
		}

		return result;

	}

	@Override
	public List<DeptItemUDFSettings> getPOItemHeader(String hl3code) {
		return poMapper.getItemHeaderForPOCreate(hl3code);
	}

	@SuppressWarnings("deprecation")
	@Override
	public List<DeptItemUDFSettings> getItemHeaderForPOCreate(String hl3code) {
		List<DeptItemUDFSettings> data = null;

		data = getPOItemHeader(hl3code);
		return data;

	}

	@SuppressWarnings("deprecation")
	@Override
	public ObjectNode getItemUDFSettingNode(List<DeptItemUDFSettings> dept) {
		ObjectNode node = getMapper().createObjectNode();
		ArrayNode arrNode = getMapper().createArrayNode();

		ArrayNode categoriesData = getMapper().createArrayNode();
		ArrayNode udfData = getMapper().createArrayNode();

		dept.stream().forEach(e -> {
			ObjectNode tempNode = getMapper().createObjectNode();
			tempNode.put(PurchaseIndentMetaData.DeptItemUDFSettingsMeta.CAT_DESC_UDF, e.getCat_desc_udf());
			tempNode.put(PurchaseIndentMetaData.DeptItemUDFSettingsMeta.CAT_DESC_UDF_TYPE, e.getCat_desc_udf_type());
			tempNode.put(PurchaseIndentMetaData.DeptItemUDFSettingsMeta.DISPLAY_NAME, e.getDisplayName());
			tempNode.put(PurchaseIndentMetaData.DeptItemUDFSettingsMeta.IS_COMPULSORY, e.getIsCompulsory());
			tempNode.put(PurchaseIndentMetaData.DeptItemUDFSettingsMeta.ORDER_BY, e.getOrderBy());
			tempNode.put(PurchaseIndentMetaData.DeptItemUDFSettingsMeta.IS_LOV, e.getIsLov());
			tempNode.put(PurchaseIndentMetaData.DeptItemUDFSettingsMeta.CAT_DESC_UDF_TYPE, e.getCat_desc_udf_type());
			tempNode.put(PurchaseIndentMetaData.DeptItemUDFSettingsMeta.ID, e.getId());

//			if (CodeUtils.isEmpty(hl4Code)) {
			if (e.getCat_desc_udf_type().equalsIgnoreCase("C") || e.getCat_desc_udf_type().equalsIgnoreCase("D")) {
				categoriesData.add(tempNode);

			} else if (e.getCat_desc_udf_type().equalsIgnoreCase("U")) {
				udfData.add(tempNode);

			}
//			} else {
//				if (e.getCat_desc_udf_type().equalsIgnoreCase("U")) {
//					udfData.add(tempNode);
//
//				}
//			}
		});

		ObjectNode mainNode = getMapper().createObjectNode();
		mainNode.put(PurchaseIndentMetaData.DeptItemUDFSettingsMeta.CATEGORIES, categoriesData);
		mainNode.put(PurchaseIndentMetaData.DeptItemUDFSettingsMeta.CAT_DESC_UDF, udfData);

		node.put(CodeUtils.RESPONSE, mainNode);

		return node;
	}

	@Override
	public int createDeptItemUDFMappings(DeptItemUDFMappings deptItemUDFMappings) {
		return poMapper.createDeptItemUDFMappings(deptItemUDFMappings);
	}

	@Override
	public int updateDeptItemUDFMappings(DeptItemUDFMappings deptItemUDFMappings) {
		return poMapper.updateDeptItemUDFMappings(deptItemUDFMappings);
	}

	public int updateDeptItemMappings(DeptItemUDFMappings deptItemUDFMappings) {
		return poMapper.updateDeptItemMappings(deptItemUDFMappings);
	}

	@Override
	public int createAndUpdatePODeptItemUDFMappings(JsonNode node, HttpServletRequest request) throws Exception {
		int result = 0;
		DeptItemUDFMappings deptItemUDFMappings = null;
		int checkDeptItemUdfMappingExistance = 0;
		String tenantHashKey = ThreadLocalStorage.getTenantHashKey();
		try {
			OffsetDateTime currentTime = OffsetDateTime.now();
			currentTime = currentTime.plusHours(new Long(5));
			currentTime = currentTime.plusMinutes(new Long(30));
			String ipAddress = InetAddress.getLocalHost().getHostAddress().trim().toString();
			JsonNode data = node.get("resultUDFType");
			String isInsert = node.get("isInsert").asText();
			if (isInsert.equalsIgnoreCase("true")) {
				if (!CodeUtils.isEmpty(data)) {

					for (JsonNode jsonNode : data) {

						deptItemUDFMappings = getMapper().treeToValue(jsonNode, DeptItemUDFMappings.class);
						checkDeptItemUdfMappingExistance = poMapper.checkDeptItemUdfMappingExistanceValidation(
								deptItemUDFMappings.getHl3code(), deptItemUDFMappings.getCat_desc_udf(),
								deptItemUDFMappings.getDescription(), deptItemUDFMappings.getHl4code());
						if (checkDeptItemUdfMappingExistance == 0) {

							deptItemUDFMappings.setCreatedAt(currentTime);
							deptItemUDFMappings.setCreatedBy(request.getAttribute("name").toString());
							deptItemUDFMappings.setIpaddress(ipAddress);
							result = createDeptItemUDFMappings(deptItemUDFMappings);
						} else {
							throw new SupplyMintException("this name is already exist");
						}
					}
				}

			} else if (isInsert.equalsIgnoreCase("false")) {
				if (!CodeUtils.isEmpty(data)) {

					for (JsonNode jsonNode : data) {
						if (tenantHashKey.equalsIgnoreCase(TenantHash.valueOf("VMART").getHashKey())) {
							deptItemUDFMappings = getMapper().treeToValue(jsonNode, DeptItemUDFMappings.class);
							deptItemUDFMappings.setIpaddress(ipAddress);
							deptItemUDFMappings.setUpdatedAt(currentTime);
							deptItemUDFMappings.setUpdatedBy(request.getAttribute("name").toString());
							result = updateDeptItemUDFMappings(deptItemUDFMappings);
						} else {

							deptItemUDFMappings = getMapper().treeToValue(jsonNode, DeptItemUDFMappings.class);
							String displayName = deptItemUDFMappings.getDisplayName().trim();
							int validateDisplayName = poMapper.checkDisplayNameExistance(displayName,
									deptItemUDFMappings.getHl3code(), deptItemUDFMappings.getCat_desc_udf(),
									deptItemUDFMappings.getHl4code(), deptItemUDFMappings.getDescription());
//							List<String> displayList=poMapper.getAllDisplayNameItemUdfMapping(deptItemUDFMappings.getHl3code(), deptItemUDFMappings.getCat_desc_udf(),
//									deptItemUDFMappings.getHl4code(), deptItemUDFMappings.getDescription());
							if (validateDisplayName == 0) {
								deptItemUDFMappings.setDisplayName(displayName);
								deptItemUDFMappings.setIpaddress(ipAddress);
								deptItemUDFMappings.setUpdatedAt(currentTime);
								deptItemUDFMappings.setUpdatedBy(request.getAttribute("name").toString());
//								result = updateDeptItemUDFMappings(deptItemUDFMappings);
								result = updateDeptItemMappings(deptItemUDFMappings);
							} else {
								throw new SupplyMintException("this name is already exist");
							}
						}
					}
				}
			}
		} catch (DataAccessException ex) {
			ex.printStackTrace();

			throw ex;
		} catch (Exception ex) {
			throw ex;
		}

		return result;

	}

	@Override
	public String updateSizeMapping(JsonNode node,String tenantHashKey)
			throws JsonProcessingException, DataAccessException, SupplyMintException {
		String result = "";
		DeptSizeData sizeMapping = null;
		int orderBy = 0;
		String hl3name = "";
		String hl3Code = "";
		try {
			Set<Integer> payloadData = new HashSet<Integer>();
			Set<String> sizes = new HashSet<String>();
			boolean isValidSize = true;

			List<DeptSizeData> sizeMappingList = new ArrayList<DeptSizeData>();

			boolean isValid = true;

			JsonNode data = node.get("sizes");
			if (!CodeUtils.isEmpty(data)) {

				sizeMapping = getMapper().treeToValue((data.get(0)), DeptSizeData.class);
				hl3name = sizeMapping.getHl3Name();
				hl3Code = sizeMapping.getHl3Code();

				List<String> sizeMappingCodes = new ArrayList<String>();
				List<String> sizeMappingSizes = new ArrayList<String>();

				for (JsonNode jsonNode : data) {
					sizeMapping = getMapper().treeToValue(jsonNode, DeptSizeData.class);
					if (sizeMapping.getOrderBy() != null && !CodeUtils.isEmpty(sizeMapping.getOrderBy())) {
						sizeMappingCodes.add(sizeMapping.getCode());
						sizeMappingSizes.add(String.valueOf(Integer.parseInt(sizeMapping.getOrderBy().trim())));

						sizeMapping.setOrderBy(String.valueOf(Integer.parseInt(sizeMapping.getOrderBy().trim())));

						sizeMappingList.add(sizeMapping);
					} else {
						sizeMapping.setOrderBy(null);
						sizeMappingList.add(sizeMapping);

					}
				}

				if (!CodeUtils.isEmpty(sizeMappingCodes))
					sizes = poMapper.getSizeMappingSizes(hl3name, sizeMappingCodes);
				else
					sizes = poMapper.getSizeMappingSizesByHl3name(hl3name);

				for (int i = 0; i < sizeMappingCodes.size(); i++) {

					if (!CodeUtils.isEmpty(sizeMappingSizes.get(i))) {
						isValidSize = (sizes.add(sizeMappingSizes.get(i))) ? true : false;
					}
					if (!isValidSize)
						throw new SupplyMintException("This order by value already exist");

				}

				if (tenantHashKey.equalsIgnoreCase(TenantHash.valueOf("VMART").getHashKey())) {
					for (DeptSizeData sizeMappingData : sizeMappingList) {
						int sizeResult = updateSizeMappingMapper(sizeMappingData);
						result = result + sizeResult + ",";
					}
				}else {
					for (DeptSizeData sizeMappingData : sizeMappingList) {
						int sizeResult=0;
						String catDescType=null;
						int isExistDepartment=poMapper.isExistDepartment(sizeMappingData.getHl3Code());
						if(isExistDepartment==0) {
							sizeMappingData.setHl3Code(hl3Code);
							List<CatDescMapping> descMappings = piMapper.getAllCatDescMapping();
							if (!CodeUtils.isEmpty(descMappings)) {
								for (int i = 0; i < descMappings.size(); i++) {
									if (descMappings.get(i).getSupplymintName().equalsIgnoreCase("SIZE")) {
										catDescType = descMappings.get(i).getClientKey();
									}
								}
							}
							sizeResult = poMapper.createSizeMappingForOthers(sizeMappingData,catDescType);
						}else {
							sizeResult = updateSizeMappingMapper(sizeMappingData);
						}
						result = result + sizeResult + ",";
					}
				}
			}
		} catch (DataAccessException dataAccessException) {
			throw dataAccessException;
		} catch (JsonProcessingException jsonProcessingException) {
			throw jsonProcessingException;
		}

		return result;
	}

	@Override
	public int updateSizeMappingMapper(DeptSizeData size) {
		return poMapper.updateSizeMapping(size);
	}

	@Override
	public Integer filterInvSetUDFDataByUDFTypeExtRecord(String udfType, String code, String name) {
		return poMapper.filterInvSetUDFDataByUDFTypeExtRecord(udfType, code, name);
	}

	@Override
	public List<Map<String, String>> getArticleByDept(Integer offset, Integer pagesize, String hl3Code,
			String catDescUDF) {
		return poMapper.getArticles(offset, pagesize, hl3Code, catDescUDF);
	}

	@Override
	public int countArticleByDept(String hl3Code, String catDescUDF) {
		return poMapper.countArticles(hl3Code, catDescUDF);
	}

	@Override
	public Integer searchCatDescUDFCount(String search, String header) {
		if (header.contains("CAT"))
			return poMapper.searchCountCatValues(search, header);
		else
			return poMapper.searchCountDescUDFValues(search, header);

	}

	@Override
	public int searchArticleCount(String search, String hl3Code, String catDescUDF) {
		return poMapper.searchArticleCount(search, hl3Code, catDescUDF);
	}

	@Override
	public List<Map<String, String>> searchArticle(Integer offset, Integer pagesize, String search, String hl3Code,
			String catDescUDF) {

		return poMapper.searchArticle(offset, pagesize, search, hl3Code, catDescUDF);
	}

	@Override
	public int countItemUdfMappingByHl3NameAndUDF(String hl3code, String cat_desc_udf, String description,
			String hl4code) {
		return poMapper.countItemUdfMappingByHl3NameAndUDF(hl3code, cat_desc_udf, description, hl4code);
	}

	@Override
	public int searchCountItemUdfMappingByHl3NameAndUDF(String hl3code, String cat_desc_udf, String search,
			String hl4code) {
		return poMapper.searchCountItemUDFMapping(search, hl3code, cat_desc_udf, hl4code);
	}

	@Override
	public List<DeptItemUDFMappings> searchItemUdfMappingByHl3NameAndUDF(Integer offset, Integer pagesize,
			String hl3code, String cat_desc_udf, String search, String hl4code) {
		return poMapper.searchItemUDFMapping(offset, pagesize, search, hl3code, cat_desc_udf, hl4code);
	}

	@Override
	public List<DeptItemUDFMappings> searchItemUdfMappingByHl3NameAndUDFEXT(Integer offset, Integer pagesize,
			String hl3code, String cat_desc_udf, String search) {
		return poMapper.searchItemUDFMappingEXT(offset, pagesize, search, hl3code, cat_desc_udf);
	}

	@Override
	public int searchCountPOItemUdfMappingByHl3NameAndUDF(String hl3code, String cat_desc_udf, String search) {
		return poMapper.searchCountPOItemUDFMapping(hl3code, cat_desc_udf, search);
	}

	@Override
	public List<DeptItemUDFMappings> searchItemUdfMappingByHl3NameAndUDF(Integer offset, Integer pagesize,
			String hl3code, String search) {
		return poMapper.searchPOItemUDFMapping(offset, pagesize, hl3code, search);
	}

	@Override
	public int countItemUDFMappingByHl3NameUDFEXT(String hl3code, String cat_desc_udf) {
		return poMapper.countItemUdfMappingByHl3NameUDFEXT(hl3code, cat_desc_udf);
	}

	@Override
	public List<UDF_SettingsMaster> getPOUDFMappingData() {
		return poMapper.getPOUDFSettingData();
	}

	@Override
	public List<DeptItemUDFSettings> getPODepartmentItemUDFByHl3Name(String hl3code) {
		return poMapper.getPODeptItemUDFByHl3Name(hl3code);

	}

	@Override
	public List<String> getPODesc6Values(String hl4Code, String mrpRangeFrom, String mrpRangeTo, Integer offset,
			Integer pagesize) {

		return poMapper.getDesc6Values(hl4Code, mrpRangeFrom, mrpRangeTo, offset, pagesize);
	}

	@Override
	public List<String> getOtherPODesc6Values(String hl4Code, String mrpRangeFrom, String mrpRangeTo, Integer offset,
			Integer pagesize) {
		return poMapper.getOtherDesc6Values(hl4Code, mrpRangeFrom, mrpRangeTo, offset, pagesize);
	}

	@Override
	public Integer getPOCountDesc6Values(String hl4Code, String mrpRangeFrom, String mrpRangeTo) {

		return poMapper.countDesc6Values(hl4Code, mrpRangeFrom, mrpRangeTo);
	}

	@Override
	public Integer getOtherPOCountDesc6Values(String hl4Code, String mrpRangeFrom, String mrpRangeTo) {

		return poMapper.countOtherDesc6Values(hl4Code, mrpRangeFrom, mrpRangeTo);
	}

	@Override
	public Integer countLoadIndent() {
		return piMapper.countLoadIndent();
	}

	@Override
	public List<PurchaseIndentHistory> getLoadIndent(int offSet, int pageSize) {
		return piMapper.getLoadIndent(offSet, pageSize);
	}

	@Override
	public Integer countLoadIndentFilter(String orderId, String suplier, String cityName, String piFromDate,
			String piToDate) {
		return poMapper.countLoadIndentFilter(orderId, suplier, cityName, piFromDate, piToDate);
	}

	@Override
	public List<PurchaseIndentHistory> getLoadIndentFilter(String orderId, String suplier, String cityName,
			String piFromDate, String piToDate, int offSet, int pageSize) {
		return poMapper.getLoadIndentFilter(orderId, suplier, cityName, piFromDate, piToDate, offSet, pageSize);
	}

	@Override
	public Integer countLoadIndentSearch(String search) {
		return poMapper.countLoadIndentSearch(search);
	}

	@Override
	public List<PurchaseIndentHistory> getLoadIndentSearch(String search, int offSet, int pageSize) {
		return poMapper.getLoadIndentSearch(search, offSet, pageSize);
	}

	@Override
	public int createParentPO(PurchaseOrderParent po) {

		return poMapper.createMainPO(po);
	}

	@Override
	public String createDetailPO(PurchaseOrderParent po, OffsetDateTime currentTime, String ipAddress,
			String userName) {

		List<PurchaseOrderChild> poDetailList = po.getPol();
		String detailResult = "";
		String setHeaderId = "";
		ArrayNode itemArray = null;
		String tenantHashKey = ThreadLocalStorage.getTenantHashKey();

		AtomicInteger headerCount = new AtomicInteger(1);

		for (PurchaseOrderChild poDetail : poDetailList) {
			ArrayNode colorArray = getMapper().valueToTree(poDetail.getColors());
			ArrayNode sizeArray = getMapper().valueToTree(poDetail.getSizes());
			ArrayNode ratioArray = getMapper().valueToTree(poDetail.getRatios());

//			if (!CodeUtils.isEmpty(poDetail.getItems())) {
//				itemArray = getMapper().valueToTree(poDetail.getItems());
//				poDetail.setItem(itemArray.toString());
//			}
			ArrayNode totalTaxArray = getMapper().valueToTree(poDetail.getDesignWiseTotalTax());
			ArrayNode totalCalculatedMarginArray = getMapper()
					.valueToTree(poDetail.getDesignWiseTotalCalculatedMargin());
			ArrayNode totalGSTArray = getMapper().valueToTree(poDetail.getDesignWiseTotalGST());
			ArrayNode totalIntakeMarginArray = getMapper().valueToTree(poDetail.getDesignWiseTotalIntakeMargin());

			ArrayNode totalFinChargesArray = getMapper().valueToTree(poDetail.getDesignWiseTotalFinCharges());

			poDetail.setColor(colorArray.toString());
			poDetail.setSize(sizeArray.toString());
			poDetail.setRatio(ratioArray.toString());
			poDetail.setOrderId(po.getOrderNo());

			if (CodeUtils.isEmpty(po.getPrevOrderNo()) && CodeUtils.isEmpty(poDetail.getSetHeaderId())) {
				setHeaderId = po.getOrderNo() + "_" + headerCount.get();
				headerCount.getAndIncrement();

				poDetail.setSetHeaderId(setHeaderId);
			}

			if (po.getIsUDFExist().equalsIgnoreCase("true")) {
				JsonNode udfNode = getMapper().valueToTree(poDetail.getUdf());
				poDetail.setUdfString(udfNode.toString());
			}

			poDetail.setDesignWiseTotalTaxString(totalTaxArray.toString());
			poDetail.setDesignWiseTotalCalculatedMargingString(totalCalculatedMarginArray.toString());
			poDetail.setDesignWiseTotalGSTString(totalGSTArray.toString());
			poDetail.setDesignWiseTotalIntakeMarginString(totalIntakeMarginArray.toString());
			poDetail.setDesignWiseTotalFinChargesString(totalFinChargesArray.toString());
			poDetail.setPoLineItemSequence(poDetailList.indexOf(poDetail) + 1);

			poDetail.setCreatedBy(userName);
			poDetail.setCreatedTime(currentTime);
			poDetail.setIpAddress(ipAddress);
			poDetail.setActive(PurchaseOrderMetaData.POStatus.ACTIVE);
			poDetail.setStatus(PurchaseOrderMetaData.POStatus.APPROVED_STATUS);

			for (int k = 0; k < poDetail.getImages().size(); k++) {
				if (k == 0) {
					poDetail.setImage1(poDetail.getImages().get(k));
				}
				if (k == 1) {
					poDetail.setImage2(poDetail.getImages().get(k));
				}
				if (k == 2) {
					poDetail.setImage3(poDetail.getImages().get(k));
				}
				if (k == 3) {
					poDetail.setImage4(poDetail.getImages().get(k));
				}
				if (k == 4) {
					poDetail.setImage5(poDetail.getImages().get(k));
				}
			}

			if (tenantHashKey.equalsIgnoreCase(TenantHash.valueOf("VMART").getHashKey())) {
				detailResult = detailResult + poMapper.createDetailPO(poDetail) + ",";
			} else {
				detailResult = detailResult + poMapper.createDetailPOForOthers(poDetail) + ",";
			}

			for (FinCharge fin : poDetail.getFinCharge()) {
				fin.setDetailId(poDetail.getId());
				fin.setCreatedBy(userName);
				fin.setCreatedTime(currentTime);
				fin.setIpAddress(ipAddress);
				fin.setActive(PurchaseOrderMetaData.POStatus.ACTIVE);
				fin.setStatus(PurchaseOrderMetaData.POStatus.APPROVED_STATUS);

				//add discount codes
				fin.setDiscountType(poDetail.getDiscountType());
				fin.setDiscountValue(poDetail.getDiscountValue());
				fin.setFinalRate(poDetail.getFinalRate());
				
				
				if (!CodeUtils.isEmpty(fin.getCharges())) {

//					ObjectNode charges = getMapper().valueToTree(
//							indentData.getPiDetails().get(i).getFinCharge().get(j).getCharges());

					fin.setChargesString(fin.getCharges().toString());

					if (!CodeUtils.isEmpty(fin.getRates())) {
						fin.setRatesString(fin.getRates().toString());

					}
					log.info("charge Node : " + fin.getCharges().toString());
//					log.info("rate Node : " + fin.getRates().toString());

				}
				if (tenantHashKey.equalsIgnoreCase(TenantHash.valueOf("VMART").getHashKey())) {
					detailResult = detailResult + poMapper.createFinCharge(fin) + ",";
				} else {
					detailResult = detailResult + poMapper.createFinChargeForOthers(fin) + ",";
				}
				
			}

		}

		return detailResult;
	}

	@Override
	public Integer countPOSavedDepartment() {
		return poMapper.countPOSavedDepartment();
	}

	@Override
	public List<Map<String, String>> getPOSavedDepartment(Integer offset, Integer pagesize) {
		return poMapper.getPOSavedDepartment(offset, pagesize);
	}

	@Override
	public Integer countPOSavedDepartmentsSearch(String search) {
		return poMapper.countPOSavedDepartmentSearch(search);
	}

	@Override
	public List<Map<String, String>> getPOSavedDepartmentSearch(Integer offset, Integer pagesize, String search) {
		return poMapper.getPOSavedDepartmentSearch(offset, pagesize, search);
	}

	@Override
	public Integer countPOSavedSupplier(String hl3code) {
		return poMapper.countPOSavedSupplier(hl3code);
	}

	@Override
	public List<Map<String, String>> getPOSavedSupplier(Integer offset, Integer pagesize, String hl3Code) {
		return poMapper.getPOSavedSupplier(offset, pagesize, hl3Code);
	}

	@Override
	public Integer countPOSavedSupplierSearch(String search, String hl3code) {
		return poMapper.countPOSavedSupplierSearch(hl3code, search);
	}

	@Override
	public List<Map<String, String>> getPOSavedSupplierSearch(Integer offset, Integer pagesize, String search,
			String hl3code) {
		return poMapper.getPOSavedSupplierSearch(offset, pagesize, hl3code, search);
	}

	@Override
	public Integer countPOSavedOrderNo(String hl3code, String supplierCode) {
		return poMapper.countPOSavedOrderNo(supplierCode, hl3code);
	}

	@Override
	public List<Map<String, String>> getPOSavedOrderNo(Integer offset, Integer pagesize, String hl3Code,
			String supplierCode) {
		return poMapper.getPOSavedOrderNo(offset, pagesize, hl3Code, supplierCode);
	}

	@Override
	public Integer countPOSavedOrderNoSearch(String search, String hl3code, String supplierCode) {
		return poMapper.countPOSavedOrderNoSearch(hl3code, supplierCode, search);
	}

	@Override
	public List<Map<String, String>> getPOSavedOrderNoSearch(Integer offset, Integer pagesize, String search,
			String hl3code, String supplierCode) {
		return poMapper.getPOSavedOrderNoSearch(offset, pagesize, hl3code, supplierCode, search);
	}

	@Override
	public PurchaseOrderParent getSetBasedPO(String orderNo) {
		return poMapper.getPurchaseOrderMain(orderNo);
	}

	@Override
	public List<PurchaseOrderChild> getSetBasedPOChild(String orderNo) {
		return poMapper.getPurchaseOrderChild(orderNo);
	}

	@Override
	public void validateMarginRule(List<PurchaseOrderChild> pocList) throws SupplyMintException {
		for (PurchaseOrderChild poc : pocList) {
			String calMargin = null;
			String actualMarkdown = null;
			try {
				calMargin = poc.getCalculatedMargin().trim();
				actualMarkdown = poc.getMarginRule().trim();

				if (Double.parseDouble(actualMarkdown) >= Double.parseDouble(calMargin))
					throw new SupplyMintException(AppStatusMsg.MARGIN_RULE_ERROR_MSG);

			} catch (NullPointerException ex) {
				throw new SupplyMintException(AppStatusMsg.MARGIN_RULE_EMPTY_MSG);
			} catch (NumberFormatException ex) {
				throw new SupplyMintException(AppStatusMsg.MARGIN_RULE_EMPTY_MSG);
			}

		}
	}

	@Override
	public Integer countSearchDesc6Values(String hl4Code, String mrpRangeFrom, String mrpRangeTo, String search) {
		return poMapper.countPOAdhocDesc6Search(hl4Code, mrpRangeFrom, mrpRangeTo, search);
	}

	@Override
	public Integer countOtherSearchDesc6Values(String hl4Code, String mrpRangeFrom, String mrpRangeTo, String search) {
		return poMapper.countOtherPOAdhocDesc6Search(hl4Code, mrpRangeFrom, mrpRangeTo, search);
	}

	@Override
	public List<String> getPOAdhocSearchDesc6Values(String hl4Code, String mrpRangeFrom, String mrpRangeTo,
			Integer offset, Integer pagesize, String search) {

		return poMapper.getPOAdhocDesc6Search(hl4Code, mrpRangeFrom, mrpRangeTo, offset, pagesize, search);
	}

	@Override
	public List<String> getOtherPOAdhocSearchDesc6Values(String hl4Code, String mrpRangeFrom, String mrpRangeTo,
			Integer offset, Integer pagesize, String search) {

		return poMapper.getOtherPOAdhocDesc6Search(hl4Code, mrpRangeFrom, mrpRangeTo, offset, pagesize, search);
	}

	@SuppressWarnings("unused")
	@Override
	public ObjectNode getItemUDFSettingMaster(String hl3Code) {
		ObjectNode node = getMapper().createObjectNode();
		ArrayNode arrNode = getMapper().createArrayNode();

		List<DeptItemUDFSettings> itemUDFSettings = new ArrayList<>();

		ArrayNode categoriesData = getMapper().createArrayNode();
		ArrayNode udfData = getMapper().createArrayNode();
//		String[] udfSettingArray = { "CAT", "DESC", "UDFSTRING", "UDFNUM", "UDFDATE" };
//		Map<String, Integer> udfSettingMap = new HashMap<>();
//		udfSettingMap.put("CAT", 6);
//		udfSettingMap.put("DESC", 6);
//		udfSettingMap.put("UDFSTRING", 10);
//		udfSettingMap.put("UDFNUM", 5);
//		udfSettingMap.put("UDFDATE", 5);

		int id = 1;
		for (String udf : UDFSetting.udfSettingArray) {
			String udfType = udf.equalsIgnoreCase("CAT") ? "C" : udf.equalsIgnoreCase("DESC") ? "D" : "U";
			int udfSettingLimit = Integer.parseInt(CATDESCUDF.valueOf(udf).getLimit());
			for (int i = 1; i <= udfSettingLimit; i++) {
				String append = Integer.toString(i).equalsIgnoreCase("10") ? "" : "0";
				String h3Code = hl3Code;
				DeptItemUDFSettings deptItemUDFSettings = new DeptItemUDFSettings();
				log.info("UDF Setting key -" + udfType);
				ObjectNode tempNode = getMapper().createObjectNode();
				tempNode.put(
						DeptItemUDFSettingsMetadeta.valueOf(DeptItemUDFSettingsMetadeta.CAT_DESC_UDF.toString())
								.getUdfSetting(),
						udfType.equalsIgnoreCase("U") ? udf + append + Integer.toString(i) : udf + Integer.toString(i));
				tempNode.put(DeptItemUDFSettingsMetadeta
						.valueOf(DeptItemUDFSettingsMetadeta.CAT_DESC_UDF_TYPE.toString()).getUdfSetting(), udfType);
				tempNode.put(DeptItemUDFSettingsMetadeta.valueOf(DeptItemUDFSettingsMetadeta.DISPLAY_NAME.toString())
						.getUdfSetting(), "");
				tempNode.put(DeptItemUDFSettingsMetadeta.valueOf(DeptItemUDFSettingsMetadeta.IS_COMPULSORY.toString())
						.getUdfSetting(), "N");
				tempNode.put(DeptItemUDFSettingsMetadeta.valueOf(DeptItemUDFSettingsMetadeta.ORDER_BY.toString())
						.getUdfSetting(), "");
				tempNode.put(DeptItemUDFSettingsMetadeta.valueOf(DeptItemUDFSettingsMetadeta.IS_LOV.toString())
						.getUdfSetting(), "N");
				tempNode.put(
						DeptItemUDFSettingsMetadeta.valueOf(DeptItemUDFSettingsMetadeta.ID.toString()).getUdfSetting(),
						"" + id++);

				if (udfType.equalsIgnoreCase("C") || udfType.equalsIgnoreCase("D")) {
					categoriesData.add(tempNode);

				} else {
					udfData.add(tempNode);
				}
				deptItemUDFSettings.setHl3Code(h3Code);
				deptItemUDFSettings.setId("" + id++);
				deptItemUDFSettings.setCat_desc_udf(
						udfType.equalsIgnoreCase("U") ? udf + append + Integer.toString(i) : udf + Integer.toString(i));
				deptItemUDFSettings.setDisplayName("");
				deptItemUDFSettings.setIsCompulsory("N");
				deptItemUDFSettings.setOrderBy("");
				deptItemUDFSettings.setIsLov("N");
				deptItemUDFSettings.setIsCompulsory("N");
				deptItemUDFSettings.setCat_desc_udf_type(udfType);

				itemUDFSettings.add(deptItemUDFSettings);
			}
		}
		int createBatchInsertUDFSetting = poMapper.createBatchInsertUDFSetting(itemUDFSettings);

		ObjectNode mainNode = getMapper().createObjectNode();
		mainNode.put(PurchaseIndentMetaData.DeptItemUDFSettingsMeta.CATEGORIES, categoriesData);
		mainNode.put(PurchaseIndentMetaData.DeptItemUDFSettingsMeta.CAT_DESC_UDF, udfData);
		node.put(CodeUtils.RESPONSE, mainNode);
		return node;
	}

	@Override
	public int countItemUdfMappingByHl3NameAndUDFForOthers(String hl3code, String cat_desc_udf, String description,
			String hl4code) {
		return poMapper.countItemUdfMappingByHl3NameAndUDFForOthers(hl3code, cat_desc_udf, description, hl4code);
	}

	@Override
	public List<DeptItemUDFMappings> itemUdfMappingByHl3NameAndUDFForOthers(int offset, int pageSize, String hl3code,
			String cat_desc_udf, String description, String hl4code) {
		return poMapper.itemUdfMappingByHl3NameAndUDFForOthers(offset, pageSize, hl3code, cat_desc_udf, description,
				hl4code);
	}

	@Override
	public int searchCountItemUdfMappingByHl3NameAndUDFforOther(String hl3code, String cat_desc_udf, String search,
			String hl4code) {
		return poMapper.searchCountItemUdfMappingByHl3NameAndUDFforOther(hl3code, cat_desc_udf, search, hl4code);
	}

	@Override
	public List<DeptItemUDFMappings> searchItemUdfMappingByHl3NameAndUDFforOthers(int offset, int pageSize,
			String hl3code, String cat_desc_udf, String search, String hl4code) {
		return poMapper.searchItemUdfMappingByHl3NameAndUDFforOthers(offset, pageSize, hl3code, cat_desc_udf, search,
				hl4code);
	}

	@Override
	public int insertUploadSummary(ProcurementUploadSummary summary) {
		return poMapper.insertUploadSummary(summary);

	}

	@Override
	public int updateLastRecord(String orgId, String uuId) {
		return poMapper.updateLastRecord(orgId, uuId);
	}

	@Override
	@SuppressWarnings("resource")
	public Map<String, String> uploadFileOnS3(InputStream inputStream, String userName, String fileName, String type,
			String orgId) throws Exception {
		String savedPath = null;
		Map<String, String> s3UploadData = null;
		String s3bucketType = "";
		try {

			if (type.equalsIgnoreCase("PurchaseIndent")) {
				s3bucketType = PURCHASE_INDENT;
			} else if (type.equalsIgnoreCase("FPO")) {
				s3bucketType = PURCHASE_ORDER;
			}

			S3BucketInfo s3BucketInfo = s3BucketInfoService.getBucketByType(s3bucketType, orgId);

			if (!CodeUtils.isEmpty(s3BucketInfo.getS3bucketName())) {
				savedPath = File.separator + excel + File.separator + orgId + File.separator + File.separator
						+ fileName;
				String filePath = FileUtils.createFilePath(FileUtils.getPlatformBasedParentDir(), savedPath);
				Files.copy(inputStream, Paths.get(filePath));

				if (CodeUtils.getExtensionByApacheCommonLib(fileName).equalsIgnoreCase("XLS")
						|| CodeUtils.getExtensionByApacheCommonLib(fileName).equalsIgnoreCase("XLSX")) {
					ToCSV tocsv = new ToCSV();
					tocsv.convertExcelToCSV(filePath, filePath.substring(0, filePath.lastIndexOf("/")));
					filePath = filePath.substring(0, filePath.lastIndexOf(".") + 1) + "csv";
				}

				File file = new File(filePath);
				CSVReader reader = new CSVReader(new FileReader(filePath));
				String str = String.join("", reader.readNext());
				String[] header = str.split("\\" + "|");
				List<String> listOfHeaders = Arrays.asList(header);
				s3UploadData = UploadUtils.uploadFile(file, s3bucketType, orgId);
				s3UploadData.put("totalHeaders", Integer.toString(header.length));
			}

		} catch (Exception e) {
			throw e;
		}
		return s3UploadData;

	}

	@Override
	public int insertDataInList(List<PurchaseOrder> data) {
		return poMapper.insertDataInList(data);

	}

	@Override
	public int updateStatus(String status, int count, String url, String uuId, String userName) {
		return poMapper.updateStatus(status, count, url, uuId, userName);
	}

	@Override
	public void insertDataFromFile(BufferedReader bufferedReader, int dataLength, String url, String uuId,
			String userName, String orgId, String token) {
		List<PurchaseOrder> storeData = new ArrayList<PurchaseOrder>();
		List<PurchaseOrder> storesAllData = new ArrayList<>();
		Set<String> storeUniqueKey = new HashSet<>();
		Map<String, String> storeOrderNumbers = new HashMap<>();
		Map<String, Integer> keyNumbers = new HashMap<>();
		Map<String, String> intgLineIdNumbers = new HashMap<>();
		String orderNumber = null;
		String OrderNumberKey = null;
		boolean isUniquekey = true;

		String line;
		boolean separatorCheck = true;
		String separator = "|";
		final int batchSize = 500;
		int count = 0;
		final String dateFormate = "dd/MM/yyyy";
		int dataCount = 0;
		int intgLineId = 0;
		int key = 0;
		int countRecord = 0;

		try {
			while ((line = bufferedReader.readLine()) != null) {

				if (separatorCheck) {
					if (!line.contains(separator)) {
						separator = ",";
					}
					separatorCheck = false;
				}
				String[] dataArr = line.split("\\" + separator);

				if (count != 0 && line.contains(separator)) {
					PurchaseOrder pOrder = new PurchaseOrder();

					boolean isDataExistence = false;
					pOrder.setOrgId(orgId);
					for (int k = 0; k < dataLength; k++) {

						String s = null;
						try {
							s = dataArr.length > k ? dataArr[k] : "";
						} catch (Exception e1) {
							s = "";
						}
						if (!s.isEmpty()) {
							isDataExistence = true;
							switch (k) {
							case 0:
								pOrder.setIntGCode(s);
								break;
							case 1:
								pOrder.setIntGHeaderId(s);
								break;

							case 2:
								pOrder.setIntGLineId(s);
								break;
							case 3:
								pOrder.setOrderNo(s);

								break;
							case 4:
								try {
									if (!s.isEmpty()) {

										LocalDate localDate = LocalDate.parse(s,
												DateTimeFormatter.ofPattern(dateFormate));
										pOrder.setOrderDate(
												OffsetDateTime.of(localDate, LocalTime.MAX, ZoneOffset.UTC));
									}
								} catch (Exception e) {
									// TODO: handle exception
								}

								break;
							case 5:
								pOrder.setVendorId(s);

								break;
							case 6:
								pOrder.setTransporterId(s);
								break;
							case 7:
								pOrder.setAgentId(s);

								break;
							case 8:
								if (!s.isEmpty())
									pOrder.setAgentRate(Double.parseDouble(s));
								else
									pOrder.setAgentRate(0);
								break;
							case 9:
								pOrder.setPoRemarks(s);
								break;
							case 10:
								pOrder.setCreatedById(s);
								break;
							case 11:
								try {
									if (!s.isEmpty()) {
										LocalDate localDate = LocalDate.parse(s,
												DateTimeFormatter.ofPattern(dateFormate));
										pOrder.setValidFrom(
												OffsetDateTime.of(localDate, LocalTime.NOON, ZoneOffset.UTC));
									}
								} catch (Exception e) {
									// TODO: handle exception
								}

								break;
							case 12:
								try {
									if (!s.isEmpty()) {

										LocalDate localDate = LocalDate.parse(s,
												DateTimeFormatter.ofPattern(dateFormate));
										pOrder.setValidTo(OffsetDateTime.of(localDate, LocalTime.NOON, ZoneOffset.UTC));
									}
								} catch (Exception e) {
									// TODO: handle exception
								}
								break;
							case 13:
								pOrder.setMerchandiserId(s);

								break;
							case 14:
								pOrder.setSiteId(s);
								break;
							case 15:
								pOrder.setItemId(s);
								break;
							case 16:
								pOrder.setSetRemarks(s);

								break;
							case 17:
								if (!s.isEmpty())
									pOrder.setSetRatio(Double.parseDouble(s));
								else
									pOrder.setSetRatio(0);

								break;
							case 18:

								pOrder.setArticleId(s);
								break;
							case 19:
								pOrder.setItemName(s);
								break;
							case 20:
								pOrder.setcCode1(s);

								break;
							case 21:
								pOrder.setcCode2(s);
								break;
							case 22:
								pOrder.setcCode3(s);
								break;
							case 23:
								pOrder.setcCode4(s);
								break;
							case 24:
								pOrder.setcCode5(s);
								break;
							case 25:
								pOrder.setcCode6(s);

								break;
							case 26:
								pOrder.setcName1(s);
								break;
							case 27:
								pOrder.setcName2(s);
								break;
							case 28:
								pOrder.setcName3(s);
								break;
							case 29:
								pOrder.setcName4(s);
								break;
							case 30:
								pOrder.setcName5(s);
								break;
							case 31:
								pOrder.setcName6(s);
								break;
							case 32:
								pOrder.setDesc1(s);
								break;
							case 33:
								pOrder.setDesc2(s);
								break;
							case 34:
								pOrder.setDesc3(s);
								break;
							case 35:
								pOrder.setDesc4(s);
								break;
							case 36:
								pOrder.setDesc5(s);
								break;
							case 37:
								pOrder.setDesc6(s);
								break;
							case 38:
								if (!s.isEmpty())
									pOrder.setMrp(Double.parseDouble(s));
								else
									pOrder.setMrp(0);
								break;
							case 39:
								if (!s.isEmpty())
									pOrder.setListedMRP(Double.parseDouble(s));
								else
									pOrder.setListedMRP(0);
								break;
							case 40:
								try {
									if (!s.isEmpty())
										pOrder.setWsp(Double.parseDouble(s));

									else
										pOrder.setWsp(0);
								} catch (Exception e) {
									pOrder.setWsp(0);
								}
								break;
							case 41:
								pOrder.setUom(s);
								break;
							case 42:
								pOrder.setMaterialType(s);
								break;
							case 43:
								if (!s.isEmpty())
									pOrder.setQty(Double.parseDouble(s));
								else
									pOrder.setQty(0);

								break;
							case 44:
								if (!s.isEmpty())
									pOrder.setRate(Double.parseDouble(s));
								else
									pOrder.setRate(0);

								break;
							case 45:
								pOrder.setPoItemRemarks(s);
								break;
							case 46:
								pOrder.setIsImported(s);
								break;
							case 47:
								pOrder.setTermCode(s);
								break;
							case 48:
								pOrder.setIsValidated(s);
								break;
							case 49:
								pOrder.setValidationError(s);
								break;
							case 50:
								pOrder.setDocCode(s);
								break;
							case 51:
								pOrder.setSetHeaderId(s);
								break;
							case 52:
								if (!s.isEmpty())
									pOrder.setKey(Integer.parseInt(s));
								else
									pOrder.setKey(0);

								break;
							case 53:
								pOrder.setPoudfStrin01(s);
								break;
							case 54:
								pOrder.setPoudfStrin02(s);
								break;
							case 55:
								pOrder.setPoudfStrin03(s);
								break;
							case 56:
								pOrder.setPoudfStrin04(s);
								break;
							case 57:
								pOrder.setPoudfStrin05(s);
								break;
							case 58:
								pOrder.setPoudfStrin06(s);
								break;
							case 59:
								pOrder.setPoudfStrin07(s);
								break;
							case 60:
								pOrder.setPoudfStrin08(s);
								break;
							case 61:
								pOrder.setPoudfStrin09(s);
								break;
							case 62:
								pOrder.setPoudfStrin010(s);
								break;
							case 63:
								pOrder.setPoudfNum01(s);

								break;
							case 64:
								pOrder.setPoudfNum02(s);

								break;
							case 65:
								pOrder.setPoudfNum03(s);
								break;
							case 66:
								pOrder.setPoudfNum04(s);
								break;
							case 67:
								pOrder.setPoudfNum05(s);
								break;
							case 68:
								try {
									if (!s.isEmpty()) {
										LocalDate localDate = LocalDate.parse(s,
												DateTimeFormatter.ofPattern(dateFormate));
										pOrder.setPoudfDate01(
												OffsetDateTime.of(localDate, LocalTime.NOON, ZoneOffset.UTC));
									}
								} catch (Exception e) {
									// TODO: handle exception
								}

								break;
							case 69:
								try {
									if (!s.isEmpty()) {
										LocalDate localDate = LocalDate.parse(s,
												DateTimeFormatter.ofPattern(dateFormate));
										pOrder.setPoudfDate02(
												OffsetDateTime.of(localDate, LocalTime.NOON, ZoneOffset.UTC));
									}
								} catch (Exception e) {
									// TODO: handle exception
								}
								break;
							case 70:
								try {
									if (!s.isEmpty()) {
										LocalDate localDate = LocalDate.parse(s,
												DateTimeFormatter.ofPattern(dateFormate));
										pOrder.setPoudfDate03(
												OffsetDateTime.of(localDate, LocalTime.NOON, ZoneOffset.UTC));
									}
								} catch (Exception e) {
									// TODO: handle exception
								}
								break;
							case 71:
								try {
									if (!s.isEmpty()) {
										LocalDate localDate = LocalDate.parse(s,
												DateTimeFormatter.ofPattern(dateFormate));
										pOrder.setPoudfDate04(
												OffsetDateTime.of(localDate, LocalTime.NOON, ZoneOffset.UTC));
									}
								} catch (Exception e) {
									// TODO: handle exception
								}
								break;
							case 72:
								try {
									if (!s.isEmpty()) {
										LocalDate localDate = LocalDate.parse(s,
												DateTimeFormatter.ofPattern(dateFormate));
										pOrder.setPoudfDate05(
												OffsetDateTime.of(localDate, LocalTime.NOON, ZoneOffset.UTC));
									}
								} catch (Exception e) {
									// TODO: handle exception
								}
								break;
							case 73:
								pOrder.setSmUDFStrin01(s);
								break;

							case 74:
								pOrder.setSmUDFStrin02(s);

								break;
							case 75:
								pOrder.setSmUDFStrin03(s);

								break;
							case 76:
								pOrder.setSmUDFStrin04(s);

								break;
							case 77:
								pOrder.setSmUDFStrin05(s);

								break;
							case 78:
								pOrder.setSmUDFStrin06(s);

								break;
							case 79:
								pOrder.setSmUDFStrin07(s);
								break;
							case 80:
								pOrder.setSmUDFStrin08(s);
								break;
							case 81:
								pOrder.setSmUDFStrin09(s);
								break;
							case 82:
								pOrder.setSmUDFStrin010(s);
								break;
							case 83:
								pOrder.setSmUDFNum01(s);
								break;
							case 84:
								pOrder.setSmUDFNum02(s);
								break;
							case 85:
								pOrder.setSmUDFNum03(s);
								break;
							case 86:
								pOrder.setSmUDFNum04(s);
								break;
							case 87:
								pOrder.setSmUDFNum05(s);
								break;
							case 88:
								try {
									if (!s.isEmpty()) {
										LocalDate localDate = LocalDate.parse(s,
												DateTimeFormatter.ofPattern(dateFormate));
										pOrder.setSmUDFDate01(
												OffsetDateTime.of(localDate, LocalTime.NOON, ZoneOffset.UTC));
									}
								} catch (Exception e) {
									// TODO: handle exception
								}

								break;
							case 89:
								try {
									if (!s.isEmpty()) {
										LocalDate localDate = LocalDate.parse(s,
												DateTimeFormatter.ofPattern(dateFormate));
										pOrder.setSmUDFDate02(
												OffsetDateTime.of(localDate, LocalTime.NOON, ZoneOffset.UTC));
									}
								} catch (Exception e) {
									// TODO: handle exception
								}

								break;
							case 90:
								try {
									if (!s.isEmpty()) {
										LocalDate localDate = LocalDate.parse(s,
												DateTimeFormatter.ofPattern(dateFormate));
										pOrder.setSmUDFDate03(
												OffsetDateTime.of(localDate, LocalTime.NOON, ZoneOffset.UTC));
									}
								} catch (Exception e) {
									// TODO: handle exception
								}
								break;
							case 91:
								try {
									if (!s.isEmpty()) {
										LocalDate localDate = LocalDate.parse(s,
												DateTimeFormatter.ofPattern(dateFormate));
										pOrder.setSmUDFDate04(
												OffsetDateTime.of(localDate, LocalTime.NOON, ZoneOffset.UTC));
									}
								} catch (Exception e) {
									// TODO: handle exception
								}

								break;
							case 92:
								try {
									if (!s.isEmpty()) {
										LocalDate localDate = LocalDate.parse(s,
												DateTimeFormatter.ofPattern(dateFormate));
										pOrder.setSmUDFDate05(
												OffsetDateTime.of(localDate, LocalTime.NOON, ZoneOffset.UTC));
									}
								} catch (Exception e) {
									// TODO: handle exception
								}
								break;
							case 93:
								pOrder.setImUDFString01(s);
								break;
							case 94:
								pOrder.setImUDFString02(s);
								break;
							case 95:
								pOrder.setImUDFString03(s);
								break;
							case 96:
								pOrder.setImUDFString04(s);
								break;
							case 97:
								pOrder.setImUDFString05(s);
								break;
							case 98:
								pOrder.setImUDFString06(s);
								break;
							case 99:
								pOrder.setImUDFString07(s);
								break;
							case 100:
								pOrder.setImUDFString08(s);
								break;
							case 101:
								pOrder.setImUDFString09(s);
								break;
							case 102:
								pOrder.setImUDFString010(s);
								break;
							case 103:
								pOrder.setImUDFNum01(s);
								break;
							case 104:
								pOrder.setImUDFNum02(s);
								break;
							case 105:
								pOrder.setImUDFNum03(s);
								break;
							case 106:
								pOrder.setImUDFNum04(s);
								break;
							case 107:
								pOrder.setImUDFNum05(s);
								break;
							case 108:
								try {
									if (!s.isEmpty()) {

										LocalDate localDate = LocalDate.parse(s,
												DateTimeFormatter.ofPattern(dateFormate));
										pOrder.setImUDFDate01(
												OffsetDateTime.of(localDate, LocalTime.NOON, ZoneOffset.UTC));
									}
								} catch (Exception e) {
									// TODO: handle exception
								}
								break;
							case 109:
								try {
									if (!s.isEmpty()) {
										LocalDate localDate = LocalDate.parse(s,
												DateTimeFormatter.ofPattern(dateFormate));
										pOrder.setImUDFDate02(
												OffsetDateTime.of(localDate, LocalTime.NOON, ZoneOffset.UTC));
									}
								} catch (Exception e) {
									// TODO: handle exception
								}

								break;
							case 110:
								try {
									if (!s.isEmpty()) {
										LocalDate localDate = LocalDate.parse(s,
												DateTimeFormatter.ofPattern(dateFormate));
										pOrder.setImUDFDate03(
												OffsetDateTime.of(localDate, LocalTime.NOON, ZoneOffset.UTC));
									}
								} catch (Exception e) {
									// TODO: handle exception
								}
								break;
							case 111:
								try {
									if (!s.isEmpty()) {

										LocalDate localDate = LocalDate.parse(s,
												DateTimeFormatter.ofPattern(dateFormate));
										pOrder.setImUDFDate04(
												OffsetDateTime.of(localDate, LocalTime.NOON, ZoneOffset.UTC));
									}
								} catch (Exception e) {
									// TODO: handle exception
								}
								break;
							case 112:
								try {

									if (!s.isEmpty()) {
										LocalDate localDate = LocalDate.parse(s,
												DateTimeFormatter.ofPattern(dateFormate));
										pOrder.setImUDFDate05(
												OffsetDateTime.of(localDate, LocalTime.NOON, ZoneOffset.UTC));
									}
								} catch (Exception e) {
									// TODO: handle exception
								}
								break;

							case 113:
								pOrder.setHsnCode(s);

							default:
								break;
							}
						}
					}
					if (isDataExistence) {

						OrderNumberKey = pOrder.getVendorId() + "-" + pOrder.getSiteId();
						storeUniqueKey.add(OrderNumberKey);
						if (isUniquekey) {
							orderNumber = CodeUtils.getPOStringOrderDate();
							storeOrderNumbers.put(OrderNumberKey, orderNumber);
							intgLineId = CodeUtils.getEightDigitRandomNumber();
							key = CodeUtils.getSixDigitRandomNumber();
							keyNumbers.put(OrderNumberKey, key);
							intgLineIdNumbers.put(OrderNumberKey, CodeUtils.getPODateString() + "/" + intgLineId);
							pOrder.setOrderNo(orderNumber);
							pOrder.setIntGHeaderId(orderNumber);
							pOrder.setKey(key);
							pOrder.setIntGLineId(intgLineIdNumbers.get(OrderNumberKey));
						} else {
							orderNumber = storeOrderNumbers.get(OrderNumberKey);
							pOrder.setOrderNo(orderNumber);
							pOrder.setIntGHeaderId(orderNumber);

							pOrder.setKey((keyNumbers.get(OrderNumberKey) + 1));
							pOrder.setIntGLineId(CodeUtils.getPODateString() + "/"
									+ (Integer.parseInt(intgLineIdNumbers.get(OrderNumberKey).substring(7)) + 1));

							keyNumbers.put(OrderNumberKey, (keyNumbers.get(OrderNumberKey) + 1));
							intgLineIdNumbers.put(OrderNumberKey, CodeUtils.getPODateString() + "/"
									+ (Integer.parseInt(intgLineIdNumbers.get(OrderNumberKey).substring(7)) + 1));

						}
						OffsetDateTime currentTime = OffsetDateTime.now();
						currentTime = currentTime.plusHours(5);
						currentTime = currentTime.plusMinutes(30);
						pOrder.setOrderDate(currentTime);
						pOrder.setIntGCode(PurchaseOrderMetaData.SMPartnerValue.ING_CODE);
						pOrder.setUom(PurchaseOrderMetaData.SMPartnerValue.UOM);
						pOrder.setIsImported(PurchaseOrderMetaData.SMPartnerValue.IS_IMPORTED);
						pOrder.setIsValidated(PurchaseOrderMetaData.SMPartnerValue.IS_VALIDATED);
						pOrder.setDocCode(PurchaseOrderMetaData.SMPartnerValue.DOC_CODE);
						pOrder.setKey(key);
						storeData.add(pOrder);
						storesAllData.add(pOrder);
					}
				}
				if (++count % batchSize == 0 && storeData.size() != 0) {
					dataCount += poMapper.insertDataInList(storeData);
					storeData.clear();
				}
			}

			if (!CodeUtils.isEmpty(storeData)) {
				dataCount += poMapper.insertDataInList(storeData);
			}

			ArrayNode arrNode = getMapper().convertValue(storesAllData, ArrayNode.class);
			ObjectNode poJson = getMapper().createObjectNode();
			poJson.put("fmcgData", arrNode);
			poJson.put("token", token);

			PurchaseOrderConfiguration poc = this.getPOConfiguration(6);

			JsonNode headerJson = getMapper().readTree(poc.getHeaders());
			String headerType = getMapper().treeToValue(headerJson.get("headerType"), String.class);

			ResponseEntity<AppResponse> responseEntityData = null;

			responseEntityData = sendFMCGPODetailsOnRestAPI(headerType, poJson, poc);

			if (!CodeUtils.isEmpty(responseEntityData)) {
				String responseEntityStatus = responseEntityData.getBody().getStatus();

				if (responseEntityStatus.contains("2000")) {
					countRecord = Integer.parseInt(responseEntityData.getBody().getData().get("resource").asText());

					poMapper.updateStatus(AWSUtils.Status.SUCCEEDED.toString(), countRecord, url, uuId, userName);
				} else {
					poMapper.updateStatus(AWSUtils.Status.FAILED.toString(), countRecord, url, uuId, userName);
				}

				log.info("response entity message :"
						+ responseEntityData.getBody().getData().get(CodeUtils.RESPONSE_MSG));
				log.info("error code response entity message :"
						+ responseEntityData.getBody().getError().get("errorCode"));
				log.info("error message response entity message :"
						+ responseEntityData.getBody().getError().get("errorMessage"));
				log.info("response calling :" + responseEntityStatus);

			} else {
				poMapper.updateStatus(AWSUtils.Status.FAILED.toString(), count - 1, url, uuId, userName);
			}
		} catch (Exception e) {
			log.debug(String.format("Error Occoured From Exception : %s", e));
			poMapper.updateStatus(AWSUtils.Status.FAILED.toString(), count - 1, url, uuId, userName);
		}
//		return storesAllData;
	}

	@Override
	public JsonNode getItemBarcode(ObjectNode itemNode)
			throws JsonParseException, JsonMappingException, IOException, SupplyMintException {

		ResponseEntity<AppResponse> responseEntity = null;
//		ObjectNode poJson = getMapper().createObjectNode();
		JsonNode itemJson = null;

		PurchaseOrderConfiguration poc = this.getPOConfiguration(4);

		JsonNode headerJson = getMapper().readTree(poc.getHeaders());
		String headerType = getMapper().treeToValue(headerJson.get("headerType"), String.class);

		ResponseEntity<AppResponse> responseEntityData = null;

		responseEntityData = sendPODetailsOnRestAPI(headerType, itemNode, poc);

		if (!CodeUtils.isEmpty(responseEntityData)) {
			String responseEntityStatus = responseEntityData.getBody().getStatus();
			log.info("response entity message :" + responseEntityData.getBody().getData().get(CodeUtils.RESPONSE_MSG));
			log.info("error code response entity message :" + responseEntityData.getBody().getError().get("errorCode"));
			log.info("error message response entity message :"
					+ responseEntityData.getBody().getError().get("errorMessage"));
			log.info("response calling :" + responseEntityStatus);
			if (responseEntityStatus.contains(CodeUtils.RESPONSE_SUCCESS_CODE)) {

				itemJson = responseEntityData.getBody().getData();
				// .get(CodeUtils.RESPONSE);
				log.info("item Json :" + itemJson.isNull());
			}
		}
		return itemJson;
	}

	@Override
	public JsonNode getItemBarcodeDetail(ObjectNode itemNode)
			throws JsonParseException, JsonMappingException, IOException, SupplyMintException {

		ResponseEntity<AppResponse> responseEntity = null;
//		ObjectNode poJson = getMapper().createObjectNode();
		JsonNode itemJson = null;

		PurchaseOrderConfiguration poc = this.getPOConfiguration(5);

		JsonNode headerJson = getMapper().readTree(poc.getHeaders());
		String headerType = getMapper().treeToValue(headerJson.get("headerType"), String.class);

		ResponseEntity<AppResponse> responseEntityData = null;

		responseEntityData = sendPODetailsOnRestAPI(headerType, itemNode, poc);

		if (!CodeUtils.isEmpty(responseEntityData)) {
			String responseEntityStatus = responseEntityData.getBody().getStatus();
			log.info("response entity message :" + responseEntityData.getBody().getData().get(CodeUtils.RESPONSE_MSG));
			log.info("error code response entity message :" + responseEntityData.getBody().getError().get("errorCode"));
			log.info("error message response entity message :"
					+ responseEntityData.getBody().getError().get("errorMessage"));
			log.info("response calling :" + responseEntityStatus);
			if (responseEntityStatus.contains(CodeUtils.RESPONSE_SUCCESS_CODE)) {

				itemJson = responseEntityData.getBody().getData().get(CodeUtils.RESPONSE);

				log.info("item Json :" + itemJson.isNull());

				log.info("response true");
				log.info("response completed@");

			}
		}
		return itemJson;
	}

	@SuppressWarnings("deprecation")
	@Override
	public ObjectNode getAvailablePOButtonKeys(String orgId) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode node = mapper.createObjectNode();
		ObjectNode insideNode = mapper.createObjectNode();
		try {

			log.info("getting keys to show button on PO pages");
			List<Map<String, String>> allRequireParamList = poMapper.getAvailablePOKeys(orgId);

			for (Map<String, String> mapOfList : allRequireParamList) {
				insideNode.put(mapOfList.get("KEY"), mapper.readValue(mapOfList.get("VALUE"), JsonNode.class));
			}

			log.info("data has been changed into json");
			node.put(CodeUtils.RESPONSE, insideNode);

		} catch (Exception ex) {
			throw ex;
		}
		return node;
	}

	@Override
	public Map<String, Object> getAllFCGHistory(int pageNo, int type, String search, String uploadedDate,
			String fileName, String fileRecord, String status, String orgId) {
		Map<String, Object> storeData = new HashMap<String, Object>();
		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;
		List<ProcurementUploadSummary> data = null;

		try {
			if (type == 1) {
				totalRecord = poMapper.recordOfFCGHistory(orgId);
				log.debug("Query,for checking all record,has ended...");
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
				log.debug("Query,for getting data,is going to start...");
				data = poMapper.getAllFCGHistory(offset, pageSize, orgId);
			} else if (type == 2) {

				totalRecord = poMapper.filterRecordOfFCGHistory(uploadedDate, fileName, fileRecord, status, orgId);

				log.debug("Query,for filtering all record,has ended...");
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;
				log.debug("Query,for getting all filter record,is going to start...");
				data = poMapper.filterFCGHistory(uploadedDate, fileName, fileRecord, status, offset, pageSize, orgId);

				log.debug("Query,for getting all filter record,has ended...");

			} else if (type == 3 && !search.isEmpty()) {

				log.debug("Query,for searching all record,is going to start...");
				totalRecord = poMapper.searchRecordOfFCGHistory(search, orgId);
				log.debug("Query,for searching all record,has ended...");
				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo - 1) * pageSize;
				log.debug("Query,for getting all searched record,is going to start...");
				data = poMapper.searchFCGHistory(search, offset, pageSize, orgId);
				log.debug("Query,for getting all searched record,has ended...");

			}
			storeData.put("currPage", pageNo);
			storeData.put("prePage", previousPage);
			storeData.put("maxPage", maxPage);
			ArrayNode array = getMapper().createArrayNode();
			if (!CodeUtils.isEmpty(data)) {
				data.stream().forEach(e -> {

					ObjectNode tempNode = getMapper().createObjectNode();
					/* commented by @Bishnu_Dutta for some times Starts ....*/
					
//					tempNode.put("id", e.getJobId());
//					tempNode.put("uploadedDate", e.getCreatedTime().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
//					tempNode.put("fileName", e.getFileName());
//					tempNode.put("totalRecord", e.getRowCount());
//					tempNode.put("uploadedBy", e.getUserName());
//					tempNode.put("status", e.getUploadStatus());
					
					/* Ends ....*/
					
					tempNode.put("id", e.getUuId());
					tempNode.put("jobId", e.getJobId());
					tempNode.put("uploadedDate", e.getCreatedTime().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
					tempNode.put("fileName", e.getFileName());
					tempNode.put("totalRecord", e.getRowCount());
					tempNode.put("failedPo", e.getErrorRecord());
					tempNode.put("successPo", e.getSuccessRecord());
					tempNode.put("uploadedBy", e.getUserName());
					tempNode.put("status", e.getUploadStatus());
					
					array.add(tempNode);

				});
			}
			storeData.put(CodeUtils.DATA, array);

		} catch (Exception e) {
			throw e;
		}

		return storeData;
	}
	
	@Override
	public int getUploadSummaryCount() {
		return poMapper.getUploadSummaryCount();
	}

	@Override
	public ProcurementUploadSummary getLastRecord(String userName, String orgId) {
		return poMapper.getLastRecord(userName, orgId);

	}

	@Override
	public ProcurementUploadSummary getSingleRecord(String userName, String orgId, String uuId) {
		return poMapper.getSingleRecord(userName, orgId, uuId);
	}

	@Override
	public Map<String, List> exportFaildDataFromFile(BufferedReader bufferedReader, String uuId, String userName,
			String orgId, int uploadedRecord, List<Integer> errorRows) {

		String line;
		String separator = "|";
		int count = 0;
		int dataLength = 0;
		List<PurchaseOrderExportData> eStoreData = new ArrayList<>();
		List<PurchaseOrderExportData> cStoreData = new ArrayList<>();
		Map<String, List> map = new HashMap<>();
		List<String> headers = null;
		List<String> fieldNames = null;

		try {
			while ((line = bufferedReader.readLine()) != null) {

				String[] dataArr = line.split("\\" + separator);
				if (count == 0 && line.contains(separator)) {
					dataLength = dataArr.length;
					headers = Arrays.asList(dataArr);
					String[] arrFieldNames = { "intGCode", "intGHeaderId", "intGLineId", "orderNo", "orderDate",
							"vendorId", "transporterId", "agentId", "agentRate", "poRemarks", "createdById",
							"validFrom", "validTo", "merchandiserId", "siteId", "itemId", "setRemarks", "setRatio",
							"articleId", "itemName", "cCode1", "cCode2", "cCode3", "cCode4", "cCode5", "cCode6",
							"cName1", "cName2", "cName3", "cName4", "cName5", "cName6", "desc1", "desc2", "desc3",
							"desc4", "desc5", "desc6", "mrp", "listedMRP", "wsp", "uom", "materialType", "qty", "rate",
							"poItemRemarks", "isImported", "termCode", "isValidated", "validationError", "docCode",
							"setHeaderId", "key", "poudfStrin01", "poudfStrin02", "poudfStrin03", "poudfStrin04",
							"poudfStrin05", "poudfStrin06", "poudfStrin07", "poudfStrin08", "poudfStrin09",
							"poudfStrin010", "poudfNum01", "poudfNum02", "poudfNum03", "poudfNum04", "poudfNum05",
							"poudfDate01", "poudfDate02", "poudfDate03", "poudfDate04", "poudfDate05", "smUDFStrin01",
							"smUDFStrin02", "smUDFStrin03", "smUDFStrin04", "smUDFStrin05", "smUDFStrin06",
							"smUDFStrin07", "smUDFStrin08", "smUDFStrin09", "smUDFStrin010", "smUDFNum01", "smUDFNum02",
							"smUDFNum03", "smUDFNum04", "smUDFNum05", "smUDFDate01", "smUDFDate02", "smUDFDate03",
							"smUDFDate04", "smUDFDate05", "imUDFString01", "imUDFString02", "imUDFString03",
							"imUDFString04", "imUDFString05", "imUDFString06", "imUDFString07", "imUDFString08",
							"imUDFString09", "imUDFString010", "imUDFNum01", "imUDFNum02", "imUDFNum03", "imUDFNum04",
							"imUDFNum05", "imUDFDate01", "imUDFDate02", "imUDFDate03", "imUDFDate04", "imUDFDate05",
							"hsnCode", "motherCompany", "sourcing" };
					fieldNames = Arrays.asList(arrFieldNames);

				}

				if (count != 0 && line.contains(separator)) {

					PurchaseOrderExportData pOrder = new PurchaseOrderExportData();
					for (int k = 0; k < dataLength; k++) {

						String s = null;
						try {
							s = dataArr.length > k ? dataArr[k] : "";
						} catch (Exception e1) {
							s = "";
						}
						if (!s.isEmpty()) {
							switch (k) {
							case 0:
								pOrder.setIntGCode(s);
								break;
							case 1:
								pOrder.setIntGHeaderId(s);
								break;

							case 2:
								pOrder.setIntGLineId(s);
								break;
							case 3:
								pOrder.setOrderNo(s);

								break;
							case 4:
								pOrder.setOrderDate(s);
								break;
							case 5:
								pOrder.setVendorId(s);

								break;
							case 6:
								pOrder.setTransporterId(s);
								break;
							case 7:
								pOrder.setAgentId(s);

								break;
							case 8:
								pOrder.setAgentRate(s);
								break;
							case 9:
								pOrder.setPoRemarks(s);
								break;
							case 10:
								pOrder.setCreatedById(s);
								break;
							case 11:
								pOrder.setValidFrom(s);

								break;
							case 12:
								pOrder.setValidTo(s);
								break;
							case 13:
								pOrder.setMerchandiserId(s);

								break;
							case 14:
								pOrder.setSiteId(s);
								break;
							case 15:
								pOrder.setItemId(s);
								break;
							case 16:
								pOrder.setSetRemarks(s);

								break;
							case 17:
								pOrder.setSetRatio(s);
								break;
							case 18:

								pOrder.setArticleId(s);
								break;
							case 19:
								pOrder.setItemName(s);
								break;
							case 20:
								pOrder.setcCode1(s);

								break;
							case 21:
								pOrder.setcCode2(s);
								break;
							case 22:
								pOrder.setcCode3(s);
								break;
							case 23:
								pOrder.setcCode4(s);
								break;
							case 24:
								pOrder.setcCode5(s);
								break;
							case 25:
								pOrder.setcCode6(s);

								break;
							case 26:
								pOrder.setcName1(s);
								break;
							case 27:
								pOrder.setcName2(s);
								break;
							case 28:
								pOrder.setcName3(s);
								break;
							case 29:
								pOrder.setcName4(s);
								break;
							case 30:
								pOrder.setcName5(s);
								break;
							case 31:
								pOrder.setcName6(s);
								break;
							case 32:
								pOrder.setDesc1(s);
								break;
							case 33:
								pOrder.setDesc2(s);
								break;
							case 34:
								pOrder.setDesc3(s);
								break;
							case 35:
								pOrder.setDesc4(s);
								break;
							case 36:
								pOrder.setDesc5(s);
								break;
							case 37:
								pOrder.setDesc6(s);
								break;
							case 38:

								pOrder.setMrp(s);
								break;
							case 39:
								pOrder.setListedMRP(s);
								break;
							case 40:
								pOrder.setWsp(s);
								break;
							case 41:
								pOrder.setUom(s);
								break;
							case 42:
								pOrder.setMaterialType(s);
								break;
							case 43:
								pOrder.setQty(s);
								break;
							case 44:
								pOrder.setRate(s);
								break;
							case 45:
								pOrder.setPoItemRemarks(s);
								break;
							case 46:
								pOrder.setIsImported(s);
								break;
							case 47:
								pOrder.setTermCode(s);
								break;
							case 48:
								pOrder.setIsValidated(s);
								break;
							case 49:
								pOrder.setValidationError(s);
								break;
							case 50:
								pOrder.setDocCode(s);
								break;
							case 51:
								pOrder.setSetHeaderId(s);
								break;
							case 52:
								pOrder.setKey(s);

								break;
							case 53:
								pOrder.setPoudfStrin01(s);
								break;
							case 54:
								pOrder.setPoudfStrin02(s);
								break;
							case 55:
								pOrder.setPoudfStrin03(s);
								break;
							case 56:
								pOrder.setPoudfStrin04(s);
								break;
							case 57:
								pOrder.setPoudfStrin05(s);
								break;
							case 58:
								pOrder.setPoudfStrin06(s);
								break;
							case 59:
								pOrder.setPoudfStrin07(s);
								break;
							case 60:
								pOrder.setPoudfStrin08(s);
								break;
							case 61:
								pOrder.setPoudfStrin09(s);
								break;
							case 62:
								pOrder.setPoudfStrin010(s);
								break;
							case 63:
								pOrder.setPoudfNum01(s);

								break;
							case 64:
								pOrder.setPoudfNum02(s);

								break;
							case 65:
								pOrder.setPoudfNum03(s);
								break;
							case 66:
								pOrder.setPoudfNum04(s);
								break;
							case 67:
								pOrder.setPoudfNum05(s);
								break;
							case 68:
								pOrder.setPoudfDate01(s);

								break;
							case 69:
								pOrder.setPoudfDate02(s);
								break;
							case 70:
								pOrder.setPoudfDate03(s);

								break;
							case 71:
								pOrder.setPoudfDate04(s);

								break;
							case 72:
								pOrder.setPoudfDate05(s);

								break;
							case 73:
								pOrder.setSmUDFStrin01(s);
								break;

							case 74:
								pOrder.setSmUDFStrin02(s);

								break;
							case 75:
								pOrder.setSmUDFStrin03(s);

								break;
							case 76:
								pOrder.setSmUDFStrin04(s);

								break;
							case 77:
								pOrder.setSmUDFStrin05(s);

								break;
							case 78:
								pOrder.setSmUDFStrin06(s);

								break;
							case 79:
								pOrder.setSmUDFStrin07(s);
								break;
							case 80:
								pOrder.setSmUDFStrin08(s);
								break;
							case 81:
								pOrder.setSmUDFStrin09(s);
								break;
							case 82:
								pOrder.setSmUDFStrin010(s);
								break;
							case 83:
								pOrder.setSmUDFNum01(s);
								break;
							case 84:
								pOrder.setSmUDFNum02(s);
								break;
							case 85:
								pOrder.setSmUDFNum03(s);
								break;
							case 86:
								pOrder.setSmUDFNum04(s);
								break;
							case 87:
								pOrder.setSmUDFNum05(s);
								break;
							case 88:
								pOrder.setSmUDFDate01(s);
								break;
							case 89:
								pOrder.setSmUDFDate02(s);
								break;
							case 90:
								pOrder.setSmUDFDate03(s);
								break;
							case 91:

								pOrder.setSmUDFDate04(s);
								break;
							case 92:
								pOrder.setSmUDFDate05(s);
								break;
							case 93:
								pOrder.setImUDFString01(s);
								break;
							case 94:
								pOrder.setImUDFString02(s);
								break;
							case 95:
								pOrder.setImUDFString03(s);
								break;
							case 96:
								pOrder.setImUDFString04(s);
								break;
							case 97:
								pOrder.setImUDFString05(s);
								break;
							case 98:
								pOrder.setImUDFString06(s);
								break;
							case 99:
								pOrder.setImUDFString07(s);
								break;
							case 100:
								pOrder.setImUDFString08(s);
								break;
							case 101:
								pOrder.setImUDFString09(s);
								break;
							case 102:
								pOrder.setImUDFString010(s);
								break;
							case 103:
								pOrder.setImUDFNum01(s);
								break;
							case 104:
								pOrder.setImUDFNum02(s);
								break;
							case 105:
								pOrder.setImUDFNum03(s);
								break;
							case 106:
								pOrder.setImUDFNum04(s);
								break;
							case 107:
								pOrder.setImUDFNum05(s);
								break;
							case 108:
								pOrder.setImUDFDate01(s);
								break;
							case 109:
								pOrder.setImUDFDate02(s);

								break;
							case 110:

								pOrder.setImUDFDate03(s);

								break;
							case 111:
								pOrder.setImUDFDate04(s);
								break;
							case 112:

								pOrder.setImUDFDate05(s);
								break;

							case 113:
								pOrder.setHsnCode(s);
								break;
							case 114:
								pOrder.setMotherCompany(s);
								break;
							case 115:
								pOrder.setSourcing(s);
								break;

							default:
								break;
							}
						}
					}
					if (!CodeUtils.isEmpty(errorRows)) {
						if (errorRows.contains(count))
							eStoreData.add(pOrder);
						else
							cStoreData.add(pOrder);
					} else {
						cStoreData.add(pOrder);
					}

				}
				count++;
			}
			map.put("currectData", cStoreData);
			map.put("errorData", eStoreData);
			headers.add("ERROR_MESSAGE");
			map.put("headers", headers);
			map.put("fieldNames", fieldNames);

		} catch (Exception e) {
			log.debug(String.format("Error Occoured From Exception : %s", e));
		}

		return map;
	}

	@Override
	public File uploadFile(List<String> headers, List<String> fieldNames, List<PurchaseOrderExportData> errorData,
			List<PurchaseOrderExportData> currectData, String orgId) {
		File file = null;
		try {
			String errorFileName = "ErrorFile";
			String currectFileName = "CurrectFile";
			String zipFileName = "ZIP_FILE";
			String errorFile = errorFileName + CustomParameter.SEPARATOR + CodeUtils.______dateFormat.format(new Date())
					+ FileUtils.CSVExtension;
			String currectFile = currectFileName + CustomParameter.SEPARATOR
					+ CodeUtils.______dateFormat.format(new Date()) + FileUtils.CSVExtension;

			String ZipFile = zipFileName + CustomParameter.SEPARATOR + CodeUtils.______dateFormat.format(new Date())
					+ FileUtils.ZIPExtension;

			CSVUtils.writeCsvFile(fieldNames, errorFile, errorData, headers);

			CSVUtils.writeCsvFile(fieldNames, currectFile, currectData, headers);

			File[] files = new File[2];

			files[0] = new File(errorFile);
			files[1] = new File(currectFile);
			file = new File(zipFileName);
			CodeUtils.createZip(file, files);

//			url = UploadUtils.uploadFile(file, "PURCHASE_ORDER", orgId).get("url");

		} catch (Exception e) {
			// TODO: handle exception
		}
		return file;

	}

	@Override
	public void importDataFromFile(BufferedReader bufferedReader, int dataLength, String url, String uuId,
			String userName, String orgId, String bucketName, String bucketPath, String token) {

		List<PurchaseOrder> storeData = new ArrayList<PurchaseOrder>();
		List<PurchaseOrder> storesAllData = new ArrayList<>();
		List<PurchaseOrderExportData> cData = new ArrayList<>();
		List<PurchaseOrderExportData> eData = new ArrayList<>();
		Set<String> storeUniqueKey = new HashSet<>();
		Map<String, String> storeOrderNumbers = new HashMap<>();
		Map<String, Integer> keyNumbers = new HashMap<>();
		Map<String, String> intgLineIdNumbers = new HashMap<>();
		String orderNumber = null;
		String OrderNumberKey = null;
		boolean isUniquekey = true;
		String line;
		boolean separatorCheck = true;
		String separator = "|";
		final int batchSize = 500;
		int count = 0;
		final String dateFormate = "dd/MM/yyyy";
		int dataCount = 0;
		int intgLineId = 0;
		int key = 0;
		int countRecord = 0;
		List<Integer> storeErrorRows = new ArrayList<>();
		String errorRows = null;
		List<String> fieldNames = null;
		List<String> headers = null;
		String downloadBucketKey = null;
		List<String> header = new ArrayList<String>();

		try {
			while ((line = bufferedReader.readLine()) != null) {

				if (separatorCheck) {
					if (!line.contains(separator)) {
						separator = ",";
					}
					separatorCheck = false;
				}
				String[] dataArr = line.split("\\" + separator);
				if (count == 0 && line.contains(separator)) {
					dataLength = dataArr.length;
					headers = Arrays.asList(dataArr);
					header.addAll(headers);
					header.add("ERROR_MESSAGE");
					String[] arrFieldNames = { "intGCode", "intGHeaderId", "intGLineId", "orderNo", "orderDate",
							"vendorId", "transporterId", "agentId", "agentRate", "poRemarks", "createdById",
							"validFrom", "validTo", "merchandiserId", "siteId", "itemId", "setRemarks", "setRatio",
							"articleId", "itemName", "cCode1", "cCode2", "cCode3", "cCode4", "cCode5", "cCode6",
							"cName1", "cName2", "cName3", "cName4", "cName5", "cName6", "desc1", "desc2", "desc3",
							"desc4", "desc5", "desc6", "mrp", "listedMRP", "wsp", "uom", "materialType", "qty", "rate",
							"poItemRemarks", "isImported", "termCode", "isValidated", "validationError", "docCode",
							"setHeaderId", "key", "poudfStrin01", "poudfStrin02", "poudfStrin03", "poudfStrin04",
							"poudfStrin05", "poudfStrin06", "poudfStrin07", "poudfStrin08", "poudfStrin09",
							"poudfStrin010", "poudfNum01", "poudfNum02", "poudfNum03", "poudfNum04", "poudfNum05",
							"poudfDate01", "poudfDate02", "poudfDate03", "poudfDate04", "poudfDate05", "smUDFStrin01",
							"smUDFStrin02", "smUDFStrin03", "smUDFStrin04", "smUDFStrin05", "smUDFStrin06",
							"smUDFStrin07", "smUDFStrin08", "smUDFStrin09", "smUDFStrin010", "smUDFNum01", "smUDFNum02",
							"smUDFNum03", "smUDFNum04", "smUDFNum05", "smUDFDate01", "smUDFDate02", "smUDFDate03",
							"smUDFDate04", "smUDFDate05", "imUDFString01", "imUDFString02", "imUDFString03",
							"imUDFString04", "imUDFString05", "imUDFString06", "imUDFString07", "imUDFString08",
							"imUDFString09", "imUDFString010", "imUDFNum01", "imUDFNum02", "imUDFNum03", "imUDFNum04",
							"imUDFNum05", "imUDFDate01", "imUDFDate02", "imUDFDate03", "imUDFDate04", "imUDFDate05",
							"hsnCode", "motherCompany", "sourcing", "errorMsg" };
					fieldNames = Arrays.asList(arrFieldNames);
				}

				if (count != 0 && line.contains(separator)) {
					PurchaseOrder pOrder = new PurchaseOrder();
					PurchaseOrderExportData poexportData = new PurchaseOrderExportData();
					boolean isGetError = false;
					String errorMsg = "";
					boolean isDataExistence = false;
					pOrder.setOrgId(orgId);
					for (int k = 0; k < dataLength; k++) {
//						if (isGetError) {
//							break;
//						}

						String s = null;
						try {
							s = dataArr.length > k ? dataArr[k] : "";
						} catch (Exception e1) {
							s = "";
						}
						if (!s.isEmpty()) {
							isDataExistence = true;
							switch (k) {
							case 0:
								pOrder.setIntGCode(s);
								poexportData.setIntGCode(s);
								break;
							case 1:
								pOrder.setIntGHeaderId(s);
								poexportData.setIntGHeaderId(s);
								break;

							case 2:
								pOrder.setIntGLineId(s);
								poexportData.setIntGLineId(s);
								break;
							case 3:
								pOrder.setOrderNo(s);
								poexportData.setOrderNo(s);
								break;
							case 4:
								try {
									if (!s.isEmpty()) {
										poexportData.setOrderDate(s);
										LocalDate localDate = LocalDate.parse(s,
												DateTimeFormatter.ofPattern(dateFormate));
										pOrder.setOrderDate(
												OffsetDateTime.of(localDate, LocalTime.MAX, ZoneOffset.UTC));
									}
								} catch (Exception e) {
									e = new Exception("error occurred from OrderDate column For input Data: " + s);
									isGetError = true;
									storeErrorRows.add(count);
									poexportData.setErrorMsg(errorMsg = e.getMessage());
								}

								break;
							case 5:
								pOrder.setVendorId(s);
								poexportData.setVendorId(s);

								break;
							case 6:
								poexportData.setTransporterId(s);
								pOrder.setTransporterId(s);
								break;
							case 7:
								poexportData.setAgentId(s);
								pOrder.setAgentId(s);

								break;
							case 8:
								try {
									if (!s.isEmpty()) {
										poexportData.setAgentRate(s);
										pOrder.setAgentRate(Double.parseDouble(s));
									} else
										pOrder.setAgentRate(0);
								} catch (Exception e) {
									e = new Exception("error occurred from AgentRate column For input Data:" + s);
									isGetError = true;
									storeErrorRows.add(count);
									if (!errorMsg.isEmpty())
										errorMsg = errorMsg + "," + e.getMessage();
									else
										errorMsg = e.getMessage();

									poexportData.setErrorMsg(errorMsg);
								}
								break;
							case 9:
								poexportData.setPoRemarks(s);
								pOrder.setPoRemarks(s);

								break;
							case 10:
								poexportData.setCreatedById(s);
								pOrder.setCreatedById(s);
								break;
							case 11:
								try {
									if (!s.isEmpty()) {
										poexportData.setValidFrom(s);
										LocalDate localDate = LocalDate.parse(s,
												DateTimeFormatter.ofPattern(dateFormate));
										pOrder.setValidFrom(
												OffsetDateTime.of(localDate, LocalTime.NOON, ZoneOffset.UTC));
									}
								} catch (Exception e) {
									e = new Exception("error occurred from ValidFrom column For input Data:" + s);
									isGetError = true;
									storeErrorRows.add(count);
									if (!errorMsg.isEmpty())
										errorMsg = errorMsg + "," + e.getMessage();
									else
										errorMsg = e.getMessage();

									poexportData.setErrorMsg(errorMsg);
								}

								break;
							case 12:
								try {
									if (!s.isEmpty()) {
										poexportData.setValidTo(s);
										LocalDate localDate = LocalDate.parse(s,
												DateTimeFormatter.ofPattern(dateFormate));
										pOrder.setValidTo(OffsetDateTime.of(localDate, LocalTime.NOON, ZoneOffset.UTC));
									}
								} catch (Exception e) {
									e = new Exception("error occurred from ValidTo column For input Data: " + s);
									isGetError = true;
									storeErrorRows.add(count);
									if (!errorMsg.isEmpty())
										errorMsg = errorMsg + "," + e.getMessage();
									else
										errorMsg = e.getMessage();

									poexportData.setErrorMsg(errorMsg);
								}
								break;
							case 13:
								poexportData.setMerchandiserId(s);
								pOrder.setMerchandiserId(s);

								break;
							case 14:
								poexportData.setSiteId(s);
								pOrder.setSiteId(s);
								break;
							case 15:
								poexportData.setItemId(s);
								pOrder.setItemId(s);
								break;
							case 16:
								poexportData.setSetRemarks(s);
								pOrder.setSetRemarks(s);

								break;
							case 17:
								try {
									if (!s.isEmpty()) {
										poexportData.setSetRatio(s);
										pOrder.setSetRatio(Double.parseDouble(s));
									} else
										pOrder.setSetRatio(0);
								} catch (Exception e) {
									e = new Exception("error occurred from SetRatio column For input Data: " + s);
									isGetError = true;
									storeErrorRows.add(count);
									if (!errorMsg.isEmpty())
										errorMsg = errorMsg + "," + e.getMessage();
									else
										errorMsg = e.getMessage();

									poexportData.setErrorMsg(errorMsg);
								}

								break;
							case 18:
								poexportData.setArticleId(s);
								pOrder.setArticleId(s);
								break;
							case 19:
								poexportData.setItemName(s);
								pOrder.setItemName(s);
								break;
							case 20:
								poexportData.setcCode1(s);
								pOrder.setcCode1(s);

								break;
							case 21:
								poexportData.setcCode2(s);
								pOrder.setcCode2(s);
								break;
							case 22:
								poexportData.setcCode3(s);
								pOrder.setcCode3(s);
								break;
							case 23:
								poexportData.setcCode4(s);
								pOrder.setcCode4(s);
								break;
							case 24:
								poexportData.setcCode5(s);
								pOrder.setcCode5(s);
								break;
							case 25:
								poexportData.setcCode6(s);
								pOrder.setcCode6(s);

								break;
							case 26:
								poexportData.setcName1(s);
								pOrder.setcName1(s);
								break;
							case 27:
								poexportData.setcName2(s);
								pOrder.setcName2(s);
								break;
							case 28:
								poexportData.setcName3(s);
								pOrder.setcName3(s);
								break;
							case 29:
								poexportData.setcName4(s);
								pOrder.setcName4(s);
								break;
							case 30:
								poexportData.setcName5(s);
								pOrder.setcName5(s);
								break;
							case 31:
								poexportData.setcName6(s);
								pOrder.setcName6(s);
								break;
							case 32:
								poexportData.setDesc1(s);
								pOrder.setDesc1(s);
								break;
							case 33:
								poexportData.setDesc2(s);
								pOrder.setDesc2(s);
								break;
							case 34:
								poexportData.setDesc3(s);
								pOrder.setDesc3(s);
								break;
							case 35:
								poexportData.setDesc4(s);
								pOrder.setDesc4(s);
								break;
							case 36:
								poexportData.setDesc5(s);
								pOrder.setDesc5(s);
								break;
							case 37:
								poexportData.setDesc6(s);
								pOrder.setDesc6(s);
								break;
							case 38:
								try {
									if (!s.isEmpty()) {
										poexportData.setMrp(s);
										pOrder.setMrp(Double.parseDouble(s));
									} else
										pOrder.setMrp(0);
								} catch (Exception e) {
									e = new Exception("error occurred from Mrp column For input Data:" + s);
									isGetError = true;
									storeErrorRows.add(count);
									if (!errorMsg.isEmpty())
										errorMsg = errorMsg + "," + e.getMessage();
									else
										errorMsg = e.getMessage();

									poexportData.setErrorMsg(errorMsg);
								}
								break;
							case 39:
								try {
									if (!s.isEmpty()) {
										poexportData.setListedMRP(s);
										pOrder.setListedMRP(Double.parseDouble(s));
									} else
										pOrder.setListedMRP(0);
								} catch (Exception e) {
									e = new Exception("error occurred from ListedMRP column For input Data:" + s);
									isGetError = true;
									storeErrorRows.add(count);
									if (!errorMsg.isEmpty())
										errorMsg = errorMsg + "," + e.getMessage();
									else
										errorMsg = e.getMessage();

									poexportData.setErrorMsg(errorMsg);
								}
								break;
							case 40:
								try {
									if (!s.isEmpty()) {
										poexportData.setWsp(s);
										pOrder.setWsp(Double.parseDouble(s));
									} else
										pOrder.setWsp(0);
								} catch (Exception e) {
									e = new Exception("error occurred from Wsp column For input Data:" + s);
									isGetError = true;
									storeErrorRows.add(count);
									if (!errorMsg.isEmpty())
										errorMsg = errorMsg + "," + e.getMessage();
									else
										errorMsg = e.getMessage();

									poexportData.setErrorMsg(errorMsg);
								}
								break;
							case 41:
								poexportData.setUom(s);
								pOrder.setUom(s);
								break;
							case 42:
								poexportData.setMaterialType(s);
								pOrder.setMaterialType(s);
								break;
							case 43:
								try {
									if (!s.isEmpty()) {
										poexportData.setQty(s);
										pOrder.setQty(Double.parseDouble(s));
									} else
										pOrder.setQty(0);
								} catch (Exception e) {
									e = new Exception("error occurred from Qty column For input Data: " + s);
									isGetError = true;
									storeErrorRows.add(count);
									if (!errorMsg.isEmpty())
										errorMsg = errorMsg + "," + e.getMessage();
									else
										errorMsg = e.getMessage();

									poexportData.setErrorMsg(errorMsg);
								}

								break;
							case 44:
								try {
									if (!s.isEmpty()) {
										poexportData.setRate(s);
										pOrder.setRate(Double.parseDouble(s));
									} else
										pOrder.setRate(0);
								} catch (Exception e) {
									e = new Exception("error occurred from Rate column For input Data: " + s);
									isGetError = true;
									storeErrorRows.add(count);
									if (!errorMsg.isEmpty())
										errorMsg = errorMsg + "," + e.getMessage();
									else
										errorMsg = e.getMessage();

									poexportData.setErrorMsg(errorMsg);
								}
								break;
							case 45:
								poexportData.setPoItemRemarks(s);
								pOrder.setPoItemRemarks(s);
								break;
							case 46:
								poexportData.setIsImported(s);
								pOrder.setIsImported(s);
								break;
							case 47:
								poexportData.setTermCode(s);
								pOrder.setTermCode(s);
								break;
							case 48:
								poexportData.setIsValidated(s);
								pOrder.setIsValidated(s);
								break;
							case 49:
								poexportData.setValidationError(s);
								pOrder.setValidationError(s);
								break;
							case 50:
								poexportData.setDocCode(s);
								pOrder.setDocCode(s);
								break;
							case 51:
								poexportData.setSetHeaderId(s);
								pOrder.setSetHeaderId(s);
								break;
							case 52:
								try {
									if (!s.isEmpty()) {
										poexportData.setKey(s);
										pOrder.setKey(Integer.parseInt(s));
									} else
										pOrder.setKey(0);
								} catch (Exception e) {
									e = new Exception("error occurred from Key column For input Data: " + s);
									isGetError = true;
									storeErrorRows.add(count);
									if (!errorMsg.isEmpty())
										errorMsg = errorMsg + "," + e.getMessage();
									else
										errorMsg = e.getMessage();

									poexportData.setErrorMsg(errorMsg);
								}
								break;
							case 53:
								poexportData.setPoudfStrin01(s);
								pOrder.setPoudfStrin01(s);
								break;
							case 54:
								poexportData.setPoudfStrin02(s);
								pOrder.setPoudfStrin02(s);
								break;
							case 55:
								poexportData.setPoudfStrin03(s);
								pOrder.setPoudfStrin03(s);
								break;
							case 56:
								poexportData.setPoudfStrin04(s);
								pOrder.setPoudfStrin04(s);
								break;
							case 57:
								poexportData.setPoudfStrin05(s);
								pOrder.setPoudfStrin05(s);
								break;
							case 58:
								poexportData.setPoudfStrin06(s);
								pOrder.setPoudfStrin06(s);
								break;
							case 59:
								poexportData.setPoudfStrin07(s);
								pOrder.setPoudfStrin07(s);
								break;
							case 60:
								poexportData.setPoudfStrin08(s);
								pOrder.setPoudfStrin08(s);
								break;
							case 61:
								poexportData.setPoudfStrin09(s);
								pOrder.setPoudfStrin09(s);
								break;
							case 62:
								poexportData.setPoudfStrin010(s);
								pOrder.setPoudfStrin010(s);
								break;
							case 63:
								poexportData.setPoudfNum01(s);
								pOrder.setPoudfNum01(s);

								break;
							case 64:
								poexportData.setPoudfNum02(s);
								pOrder.setPoudfNum02(s);

								break;
							case 65:
								poexportData.setPoudfNum03(s);
								pOrder.setPoudfNum03(s);
								break;
							case 66:
								poexportData.setPoudfNum04(s);
								pOrder.setPoudfNum04(s);
								break;
							case 67:
								poexportData.setPoudfNum05(s);
								pOrder.setPoudfNum05(s);
								break;
							case 68:
								try {
									if (!s.isEmpty()) {
										poexportData.setPoudfDate01(s);
										LocalDate localDate = LocalDate.parse(s,
												DateTimeFormatter.ofPattern(dateFormate));
										pOrder.setPoudfDate01(
												OffsetDateTime.of(localDate, LocalTime.NOON, ZoneOffset.UTC));
									}
								} catch (Exception e) {
									e = new Exception("error occurred from PoudfDate01 column For input Data: " + s);
									isGetError = true;
									storeErrorRows.add(count);
									if (!errorMsg.isEmpty())
										errorMsg = errorMsg + "," + e.getMessage();
									else
										errorMsg = e.getMessage();

									poexportData.setErrorMsg(errorMsg);
								}

								break;
							case 69:
								try {
									if (!s.isEmpty()) {
										poexportData.setPoudfDate02(s);
										LocalDate localDate = LocalDate.parse(s,
												DateTimeFormatter.ofPattern(dateFormate));
										pOrder.setPoudfDate02(
												OffsetDateTime.of(localDate, LocalTime.NOON, ZoneOffset.UTC));
									}
								} catch (Exception e) {
									e = new Exception("error occurred from PoudfDate02 column For input Data: " + s);
									isGetError = true;
									storeErrorRows.add(count);
									if (!errorMsg.isEmpty())
										errorMsg = errorMsg + "," + e.getMessage();
									else
										errorMsg = e.getMessage();

									poexportData.setErrorMsg(errorMsg);
								}
								break;
							case 70:
								try {
									if (!s.isEmpty()) {
										poexportData.setPoudfDate03(s);
										LocalDate localDate = LocalDate.parse(s,
												DateTimeFormatter.ofPattern(dateFormate));
										pOrder.setPoudfDate03(
												OffsetDateTime.of(localDate, LocalTime.NOON, ZoneOffset.UTC));
									}
								} catch (Exception e) {
									e = new Exception("error occurred from PoudfDate02 column:For input Data: " + s);
									isGetError = true;
									storeErrorRows.add(count);
									if (!errorMsg.isEmpty())
										errorMsg = errorMsg + "," + e.getMessage();
									else
										errorMsg = e.getMessage();

									poexportData.setErrorMsg(errorMsg);
								}
								break;
							case 71:
								try {
									if (!s.isEmpty()) {
										poexportData.setPoudfDate04(s);
										LocalDate localDate = LocalDate.parse(s,
												DateTimeFormatter.ofPattern(dateFormate));
										pOrder.setPoudfDate04(
												OffsetDateTime.of(localDate, LocalTime.NOON, ZoneOffset.UTC));
									}
								} catch (Exception e) {
									e = new Exception("error occurred from PoudfDate04 column For input Data: " + s);
									isGetError = true;
									storeErrorRows.add(count);
									if (!errorMsg.isEmpty())
										errorMsg = errorMsg + "," + e.getMessage();
									else
										errorMsg = e.getMessage();

									poexportData.setErrorMsg(errorMsg);
								}
								break;
							case 72:
								try {
									if (!s.isEmpty()) {
										poexportData.setPoudfDate05(s);
										LocalDate localDate = LocalDate.parse(s,
												DateTimeFormatter.ofPattern(dateFormate));
										pOrder.setPoudfDate05(
												OffsetDateTime.of(localDate, LocalTime.NOON, ZoneOffset.UTC));
									}
								} catch (Exception e) {
									e = new Exception("error occurred from PoudfDate05 column For input Data: " + s);
									isGetError = true;
									storeErrorRows.add(count);
									if (!errorMsg.isEmpty())
										errorMsg = errorMsg + "," + e.getMessage();
									else
										errorMsg = e.getMessage();

									poexportData.setErrorMsg(errorMsg);
								}
								break;
							case 73:
								poexportData.setSmUDFStrin01(s);
								pOrder.setSmUDFStrin01(s);
								break;

							case 74:
								poexportData.setSmUDFStrin02(s);
								pOrder.setSmUDFStrin02(s);

								break;
							case 75:
								poexportData.setSmUDFStrin03(s);
								pOrder.setSmUDFStrin03(s);

								break;
							case 76:
								poexportData.setSmUDFStrin04(s);
								pOrder.setSmUDFStrin04(s);

								break;
							case 77:
								poexportData.setSmUDFStrin05(s);
								pOrder.setSmUDFStrin05(s);

								break;
							case 78:
								poexportData.setSmUDFStrin06(s);
								pOrder.setSmUDFStrin06(s);

								break;
							case 79:
								poexportData.setSmUDFStrin07(s);
								pOrder.setSmUDFStrin07(s);
								break;
							case 80:
								poexportData.setSmUDFStrin08(s);
								pOrder.setSmUDFStrin08(s);
								break;
							case 81:
								poexportData.setSmUDFStrin09(s);
								pOrder.setSmUDFStrin09(s);
								break;
							case 82:
								poexportData.setSmUDFStrin010(s);
								pOrder.setSmUDFStrin010(s);
								break;
							case 83:
								poexportData.setSmUDFNum01(s);
								pOrder.setSmUDFNum01(s);
								break;
							case 84:
								poexportData.setSmUDFNum02(s);
								pOrder.setSmUDFNum02(s);
								break;
							case 85:
								poexportData.setSmUDFNum03(s);
								pOrder.setSmUDFNum03(s);
								break;
							case 86:
								poexportData.setSmUDFNum04(s);
								pOrder.setSmUDFNum04(s);
								break;
							case 87:
								poexportData.setSmUDFNum05(s);
								pOrder.setSmUDFNum05(s);
								break;
							case 88:
								try {
									if (!s.isEmpty()) {
										poexportData.setSmUDFDate01(s);
										LocalDate localDate = LocalDate.parse(s,
												DateTimeFormatter.ofPattern(dateFormate));
										pOrder.setSmUDFDate01(
												OffsetDateTime.of(localDate, LocalTime.NOON, ZoneOffset.UTC));
									}
								} catch (Exception e) {
									e = new Exception("error occurred from SmUDFDate01 column For input Data: " + s);
									isGetError = true;
									storeErrorRows.add(count);
									if (!errorMsg.isEmpty())
										errorMsg = errorMsg + "," + e.getMessage();
									else
										errorMsg = e.getMessage();

									poexportData.setErrorMsg(errorMsg);
								}

								break;
							case 89:
								try {
									if (!s.isEmpty()) {
										poexportData.setSmUDFDate02(s);
										LocalDate localDate = LocalDate.parse(s,
												DateTimeFormatter.ofPattern(dateFormate));
										pOrder.setSmUDFDate02(
												OffsetDateTime.of(localDate, LocalTime.NOON, ZoneOffset.UTC));
									}
								} catch (Exception e) {
									e = new Exception("error occurred from SmUDFDate02 column For input Data:" + s);
									isGetError = true;
									storeErrorRows.add(count);
									if (!errorMsg.isEmpty())
										errorMsg = errorMsg + "," + e.getMessage();
									else
										errorMsg = e.getMessage();

									poexportData.setErrorMsg(errorMsg);
								}

								break;
							case 90:
								try {
									if (!s.isEmpty()) {
										poexportData.setSmUDFDate03(s);
										LocalDate localDate = LocalDate.parse(s,
												DateTimeFormatter.ofPattern(dateFormate));
										pOrder.setSmUDFDate03(
												OffsetDateTime.of(localDate, LocalTime.NOON, ZoneOffset.UTC));
									}
								} catch (Exception e) {
									e = new Exception("error occurred from SmUDFDate03 column For input Data:" + s);
									isGetError = true;
									storeErrorRows.add(count);
									if (!errorMsg.isEmpty())
										errorMsg = errorMsg + "," + e.getMessage();
									else
										errorMsg = e.getMessage();

									poexportData.setErrorMsg(errorMsg);
								}
								break;
							case 91:
								try {
									if (!s.isEmpty()) {
										poexportData.setSmUDFDate04(s);
										LocalDate localDate = LocalDate.parse(s,
												DateTimeFormatter.ofPattern(dateFormate));
										pOrder.setSmUDFDate04(
												OffsetDateTime.of(localDate, LocalTime.NOON, ZoneOffset.UTC));
									}
								} catch (Exception e) {
									e = new Exception("error occurred from SmUDFDate04 column For input Data: " + s);
									isGetError = true;
									storeErrorRows.add(count);
									if (!errorMsg.isEmpty())
										errorMsg = errorMsg + "," + e.getMessage();
									else
										errorMsg = e.getMessage();
									poexportData.setErrorMsg(errorMsg);
								}

								break;
							case 92:
								try {
									if (!s.isEmpty()) {
										poexportData.setSmUDFDate05(s);
										LocalDate localDate = LocalDate.parse(s,
												DateTimeFormatter.ofPattern(dateFormate));
										pOrder.setSmUDFDate05(
												OffsetDateTime.of(localDate, LocalTime.NOON, ZoneOffset.UTC));
									}
								} catch (Exception e) {
									e = new Exception("error occurred from SmUDFDate05 column For input Data:" + s);
									isGetError = true;
									storeErrorRows.add(count);
									if (!errorMsg.isEmpty())
										errorMsg = errorMsg + "," + e.getMessage();
									else
										errorMsg = e.getMessage();

									poexportData.setErrorMsg(errorMsg);
								}
								break;
							case 93:
								poexportData.setImUDFString01(s);
								pOrder.setImUDFString01(s);
								break;
							case 94:
								poexportData.setImUDFString02(s);
								pOrder.setImUDFString02(s);
								break;
							case 95:
								poexportData.setImUDFString03(s);
								pOrder.setImUDFString03(s);
								break;
							case 96:
								poexportData.setImUDFString04(s);
								pOrder.setImUDFString04(s);
								break;
							case 97:
								poexportData.setImUDFString05(s);
								pOrder.setImUDFString05(s);
								break;
							case 98:
								poexportData.setImUDFString06(s);
								pOrder.setImUDFString06(s);
								break;
							case 99:
								poexportData.setImUDFString07(s);
								pOrder.setImUDFString07(s);
								break;
							case 100:
								poexportData.setImUDFString08(s);
								pOrder.setImUDFString08(s);
								break;
							case 101:
								poexportData.setImUDFString09(s);
								pOrder.setImUDFString09(s);
								break;
							case 102:
								poexportData.setImUDFString010(s);
								pOrder.setImUDFString010(s);
								break;
							case 103:
								poexportData.setImUDFNum01(s);
								pOrder.setImUDFNum01(s);
								break;
							case 104:
								poexportData.setImUDFNum02(s);
								pOrder.setImUDFNum02(s);
								break;
							case 105:
								poexportData.setImUDFNum03(s);
								pOrder.setImUDFNum03(s);
								break;
							case 106:
								poexportData.setImUDFNum04(s);
								pOrder.setImUDFNum04(s);
								break;
							case 107:
								poexportData.setImUDFNum05(s);
								pOrder.setImUDFNum05(s);
								break;
							case 108:
								try {
									if (!s.isEmpty()) {
										poexportData.setImUDFDate01(s);
										LocalDate localDate = LocalDate.parse(s,
												DateTimeFormatter.ofPattern(dateFormate));
										pOrder.setImUDFDate01(
												OffsetDateTime.of(localDate, LocalTime.NOON, ZoneOffset.UTC));
									}
								} catch (Exception e) {
									e = new Exception("error occurred from ImUDFDate01 column For input Data:" + s);
									isGetError = true;
									storeErrorRows.add(count);
									if (!errorMsg.isEmpty())
										errorMsg = errorMsg + "," + e.getMessage();
									else
										errorMsg = e.getMessage();

									poexportData.setErrorMsg(errorMsg);
								}
								break;
							case 109:
								try {
									if (!s.isEmpty()) {
										poexportData.setImUDFDate02(s);
										LocalDate localDate = LocalDate.parse(s,
												DateTimeFormatter.ofPattern(dateFormate));
										pOrder.setImUDFDate02(
												OffsetDateTime.of(localDate, LocalTime.NOON, ZoneOffset.UTC));
									}
								} catch (Exception e) {
									e = new Exception("error occurred from ImUDFDate02 column For input Data:" + s);
									isGetError = true;
									storeErrorRows.add(count);
									if (!errorMsg.isEmpty())
										errorMsg = errorMsg + "," + e.getMessage();
									else
										errorMsg = e.getMessage();

									poexportData.setErrorMsg(errorMsg);
								}

								break;
							case 110:
								try {
									if (!s.isEmpty()) {
										poexportData.setImUDFDate03(s);
										LocalDate localDate = LocalDate.parse(s,
												DateTimeFormatter.ofPattern(dateFormate));
										pOrder.setImUDFDate03(
												OffsetDateTime.of(localDate, LocalTime.NOON, ZoneOffset.UTC));
									}
								} catch (Exception e) {
									e = new Exception("error occurred from ImUDFDate03 column For input Data:" + s);
									isGetError = true;
									storeErrorRows.add(count);
									if (!errorMsg.isEmpty())
										errorMsg = errorMsg + "," + e.getMessage();
									else
										errorMsg = e.getMessage();

									poexportData.setErrorMsg(errorMsg);
								}
								break;
							case 111:
								try {
									if (!s.isEmpty()) {
										poexportData.setImUDFDate04(s);
										LocalDate localDate = LocalDate.parse(s,
												DateTimeFormatter.ofPattern(dateFormate));
										pOrder.setImUDFDate04(
												OffsetDateTime.of(localDate, LocalTime.NOON, ZoneOffset.UTC));
									}
								} catch (Exception e) {
									e = new Exception("error occurred from ImUDFDate04 column For input Data:" + s);
									isGetError = true;
									storeErrorRows.add(count);
									if (!errorMsg.isEmpty())
										errorMsg = errorMsg + "," + e.getMessage();
									else
										errorMsg = e.getMessage();

									poexportData.setErrorMsg(errorMsg);
								}
								break;
							case 112:
								try {

									if (!s.isEmpty()) {
										poexportData.setImUDFDate05(s);
										LocalDate localDate = LocalDate.parse(s,
												DateTimeFormatter.ofPattern(dateFormate));
										pOrder.setImUDFDate05(
												OffsetDateTime.of(localDate, LocalTime.NOON, ZoneOffset.UTC));
									}
								} catch (Exception e) {
									e = new Exception("error occurred from ImUDFDate05 column For input Data: " + s);
									isGetError = true;
									storeErrorRows.add(count);
									if (!errorMsg.isEmpty())
										errorMsg = errorMsg + "," + e.getMessage();
									else
										errorMsg = e.getMessage();

									poexportData.setErrorMsg(errorMsg);
								}
								break;

							case 113:
								poexportData.setHsnCode(s);
								pOrder.setHsnCode(s);
							default:
								break;
							}
						}
					}
					if (isDataExistence && !isGetError) {

						OrderNumberKey = pOrder.getVendorId() + "-" + pOrder.getSiteId();
						storeUniqueKey.add(OrderNumberKey);
						if (isUniquekey) {
							orderNumber = CodeUtils.getPOStringOrderDate();
							storeOrderNumbers.put(OrderNumberKey, orderNumber);
							intgLineId = CodeUtils.getEightDigitRandomNumber();
							key = CodeUtils.getSixDigitRandomNumber();
							keyNumbers.put(OrderNumberKey, key);
							intgLineIdNumbers.put(OrderNumberKey, CodeUtils.getPODateString() + "/" + intgLineId);
							pOrder.setOrderNo(orderNumber);
							pOrder.setIntGHeaderId(orderNumber);
							pOrder.setKey(key);
							pOrder.setIntGLineId(intgLineIdNumbers.get(OrderNumberKey));
						} else {
							orderNumber = storeOrderNumbers.get(OrderNumberKey);
							pOrder.setOrderNo(orderNumber);
							pOrder.setIntGHeaderId(orderNumber);

							pOrder.setKey((keyNumbers.get(OrderNumberKey) + 1));
							pOrder.setIntGLineId(CodeUtils.getPODateString() + "/"
									+ (Integer.parseInt(intgLineIdNumbers.get(OrderNumberKey).substring(7)) + 1));

							keyNumbers.put(OrderNumberKey, (keyNumbers.get(OrderNumberKey) + 1));
							intgLineIdNumbers.put(OrderNumberKey, CodeUtils.getPODateString() + "/"
									+ (Integer.parseInt(intgLineIdNumbers.get(OrderNumberKey).substring(7)) + 1));

						}
						OffsetDateTime currentTime = OffsetDateTime.now();
						currentTime = currentTime.plusHours(5);
						currentTime = currentTime.plusMinutes(30);
						pOrder.setOrderDate(currentTime);
						pOrder.setIntGCode(PurchaseOrderMetaData.SMPartnerValue.ING_CODE);
						pOrder.setUom(PurchaseOrderMetaData.SMPartnerValue.UOM);
						pOrder.setIsImported(PurchaseOrderMetaData.SMPartnerValue.IS_IMPORTED);
						pOrder.setIsValidated(PurchaseOrderMetaData.SMPartnerValue.IS_VALIDATED);
						pOrder.setDocCode(PurchaseOrderMetaData.SMPartnerValue.DOC_CODE);
						pOrder.setKey(key);
						storeData.add(pOrder);
						storesAllData.add(pOrder);
						cData.add(poexportData);
					} else {
						eData.add(poexportData);
					}

				}
				if (++count % batchSize == 0 && storeData.size() != 0) {
					log.info("batch inserte data start now ... ");
					dataCount += poMapper.insertDataInList(storeData);
					log.debug(String.format("total number of data inserted is :%s---", dataCount));
					storeData.clear();
				}
			}

			if (!CodeUtils.isEmpty(storeData)) {
				log.info("batch inserte data start now ... ");
				dataCount += poMapper.insertDataInList(storeData);
				log.debug(String.format("total number of data inserted is :%s---", dataCount));
			}

			File file = uploadFile(header, fieldNames, eData, cData, orgId);
			downloadBucketKey = UploadUtils.uploadFile(file, "PURCHASE_ORDER", orgId).get("bucketKey");

			ArrayNode arrNode = getMapper().convertValue(storesAllData, ArrayNode.class);
			ObjectNode poJson = getMapper().createObjectNode();
			poJson.put("fmcgData", arrNode);
			poJson.put("token", token);

			PurchaseOrderConfiguration poc = this.getPOConfiguration(6);

			JsonNode headerJson = getMapper().readTree(poc.getHeaders());
			String headerType = getMapper().treeToValue(headerJson.get("headerType"), String.class);

			ResponseEntity<AppResponse> responseEntityData = null;

			log.info("Call Ginesys API---");
			responseEntityData = sendFMCGPODetailsOnRestAPI(headerType, poJson, poc);
			if (!CodeUtils.isEmpty(responseEntityData)) {
				String responseEntityStatus = responseEntityData.getBody().getStatus();

				if (responseEntityStatus.contains("2000")) {
//					countRecord = Integer.parseInt(responseEntityData.getBody().getData().get("resource").asText());
					if (storeErrorRows.size() == 0) {
//						poMapper.updateStatus(AWSUtils.Status.SUCCEEDED.toString(), count - 1, url, uuId, userName,
//								bucketName, bucketPath, errorRows, storeErrorRows.size());
//						

						poMapper.updateStatus1(AWSUtils.Status.SUCCEEDED.toString(), count - 1, url, downloadBucketKey,
								uuId, bucketName, bucketPath,cData.size() ,storeErrorRows.size());

					} else {
//						poMapper.updateStatus(AWSUtils.Status.PARTIALERROR.toString(), count - 1, url, uuId, userName,
//								bucketName, bucketPath, errorRows, storeErrorRows.size());


						poMapper.updateStatus1(AWSUtils.Status.SUCCEEDED.toString(), count - 1, url, downloadBucketKey,
								uuId, bucketName, bucketPath,cData.size() ,storeErrorRows.size());

					}

				} else {

					poMapper.updateStatus1(AWSUtils.Status.SUCCEEDED.toString(), count - 1, url, downloadBucketKey,
							uuId, bucketName, bucketPath,cData.size() ,storeErrorRows.size());

				}

				log.info("response entity message :"
						+ responseEntityData.getBody().getData().get(CodeUtils.RESPONSE_MSG));
				log.info("error code response entity message :"
						+ responseEntityData.getBody().getError().get("errorCode"));
				log.info("error message response entity message :"
						+ responseEntityData.getBody().getError().get("errorMessage"));
				log.info("response calling :" + responseEntityStatus);

			} else {

				poMapper.updateStatus1(AWSUtils.Status.SUCCEEDED.toString(), count - 1, url, downloadBucketKey,
						uuId, bucketName, bucketPath,cData.size() ,storeErrorRows.size());
			}
		} catch (Exception e) {
			log.debug(String.format("Error Occoured From Exception : %s", e));

			poMapper.updateStatus1(AWSUtils.Status.SUCCEEDED.toString(), count - 1, url, downloadBucketKey,
					uuId, bucketName, bucketPath,cData.size() ,storeErrorRows.size());

		}
	}

	@Override
	public List<String> getJobIdFCGFile(String ogdId) {
		return poMapper.getJobIdFCGFile(ogdId);
	}

	@Override
	public Map<String, String> getHoldDescUDFData(PurchaseOrderChild poc, String hl3code) {

		return poMapper.getHoldDescUDF(hl3code, poc.getDesc2Name(), poc.getDesc3Name(), poc.getDesc4Name(),
				poc.getDesc5Name(), poc.getUdf().getItemudf1(), poc.getUdf().getItemudf2(), poc.getUdf().getItemudf3(),
				poc.getUdf().getItemudf4(), poc.getUdf().getItemudf5(), poc.getUdf().getItemudf6(),
				poc.getUdf().getItemudf7(), poc.getUdf().getItemudf8(), poc.getUdf().getItemudf9(),
				poc.getUdf().getItemudf10(), poc.getUdf().getItemudf11(), poc.getUdf().getItemudf12(),
				poc.getUdf().getItemudf13(), poc.getUdf().getItemudf14(), poc.getUdf().getItemudf15());
	}

	@Override
	public Map<String, String> getHoldCategoriesData(String hl3code, String cat1, String cat2, String cat3,
			String cat4) {

		return poMapper.getHoldCategoriesMaster(hl3code, cat1, cat2, cat3, cat4);

	}

	@Override
	public List<Category> getHoldSizeData(List<String> sizeColorList, String hl3code) {
		return poMapper.getHoldSize(sizeColorList, hl3code);
	}

	@Override
	public List<Category> getHoldColorData(List<String> sizeColorList, String hl3code) {
		return poMapper.getHoldColor(sizeColorList, hl3code);
	}

	@Override
	public List<TempPurchaseOrderChild> getNullSizeData() {

		return poMapper.getNullSizeCode();
	}

	public String getSizeCode(String sizeName) {
		return poMapper.getSizeCode(sizeName);
	}

	@Override
	public boolean repairNullSizes(List<TempPurchaseOrderChild> tpocList)
			throws JsonParseException, JsonMappingException, IOException {
		List<Category> sizeList = null;

		boolean nullCheck = false;
		for (int i = 0; i < tpocList.size(); i++) {
			sizeList = null;
			sizeList = getMapper().readValue(tpocList.get(i).getSize(),
					getMapper().getTypeFactory().constructCollectionType(List.class, Category.class));
			System.out.print("i :" + i);
			nullCheck = false;
			for (int j = 0; j < sizeList.size(); j++) {
				if (CodeUtils.isEmpty(sizeList.get(j).getCode())) {

					nullCheck = true;

					String sizeCode = getSizeCode(sizeList.get(j).getCname());

					if (!CodeUtils.isEmpty(sizeCode)) {
						sizeList.get(j).setCode(sizeCode);
					}

				}
			}

			if (nullCheck == true) {

				tpocList.get(i).setSizes(sizeList);
				ArrayNode sizeArray = getMapper().valueToTree(tpocList.get(i).getSizes());
				int updateSize = poMapper.updateSizeCode(tpocList.get(i).getSize(), sizeArray.toString());
				System.out.println("update size : " + updateSize);

			}

		}

		return true;
	}

	@Override
	public List<TempPurchaseOrderChild> getNullColorData() {

		return poMapper.getNullColorCode();
	}

	public String getColorCode(String sizeName) {
		return poMapper.getColorCode(sizeName);
	}

	@Override
	public boolean repairNullColors(List<TempPurchaseOrderChild> tpocList)
			throws JsonParseException, JsonMappingException, IOException {
		List<Category> colorList = null;

		boolean nullCheck = false;
		for (int i = 0; i < tpocList.size(); i++) {
			colorList = null;
			colorList = getMapper().readValue(tpocList.get(i).getColor(),
					getMapper().getTypeFactory().constructCollectionType(List.class, Category.class));
			System.out.print("i :" + i);
			nullCheck = false;
			for (int j = 0; j < colorList.size(); j++) {
				if (CodeUtils.isEmpty(colorList.get(j).getCode())) {

					nullCheck = true;

					String colorCode = getColorCode(colorList.get(j).getCname());

					if (!CodeUtils.isEmpty(colorCode)) {
						colorList.get(j).setCode(colorCode);
					}

				}
			}

			if (nullCheck == true) {

				tpocList.get(i).setColors(colorList);
				ArrayNode colorArray = getMapper().valueToTree(tpocList.get(i).getColors());
				int updateColor = poMapper.updateColorCode(tpocList.get(i).getColor(), colorArray.toString());
				System.out.println("update color : " + updateColor);

			}

		}
		return true;
	}

	@Override
	public int configProcurementDate(ObjectNode dateNode, String orgId) throws Exception {
		int result = 0;
		ObjectMapper mapper = new ObjectMapper();

		try {
			String dateString = mapper.writeValueAsString(dateNode);

			result = poMapper.configProcurementDate(dateString, orgId);
		} catch (Exception ex) {
			throw ex;
		}
		return result;
	}

	@Override
	public int countTotalDistinctCities() {
		return poMapper.countTotalDistinctCities();
	}

	@Override
	public List<String> getAllCitiesDetails(int offset, int pageSize) {
		return poMapper.getAllCitiesDetails(offset, pageSize);
	}

	@Override
	public int countAllSearchedCities(String search) {
		return poMapper.countAllSearchedCities(search);
	}

	@Override
	public List<String> getAllSearchedCities(int offset, int pageSize, String search) {
		return poMapper.getAllSearchedCities(offset, pageSize, search);
	}

	@Override
	public List<Map<String, String>> getPODiscount(String articleCode, String mrp, String discountType) {

		return poMapper.getPODiscount(articleCode, mrp, discountType);
	}

	@Override
	public List<PurchaseOrderChild> getSetBasedPOChildForOthers(String orderNo) {
		return poMapper.getPurchaseOrderChildForOthers(orderNo);
	}

}
