package com.supplymint.layer.business.service.core;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.supplymint.layer.data.core.entity.Enterprise;
import com.supplymint.layer.data.core.mapper.EnterpriseMapper;


@Service
public class EnterpriseService {

	private EnterpriseMapper enterpriseMapper;
	
	@Autowired
	public EnterpriseService(EnterpriseMapper enterpriseMapper) {
		
		this.enterpriseMapper=enterpriseMapper;
	}
	
	public Enterprise findById(Integer id) {
		
		return enterpriseMapper.findById(id); 
	}
	
}
