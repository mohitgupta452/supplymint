package com.supplymint.layer.business.service.administration;

import static java.nio.file.StandardOpenOption.CREATE_NEW;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.supplymint.exception.SupplyMintException;
import com.supplymint.layer.business.contract.administration.ADMUserRoleService;
import com.supplymint.layer.data.core.entity.Users;
import com.supplymint.layer.data.tenant.administration.entity.ADMUserRole;
import com.supplymint.layer.data.tenant.administration.mapper.ADMUserRoleMapper;
import com.supplymint.layer.web.model.AppResponse;
import com.supplymint.util.CodeUtils;

@Service
public class ADMUserRoleServiceImpl implements ADMUserRoleService {

	private ADMUserRoleMapper admUserRoleMapper;

	@Autowired
	public ADMUserRoleServiceImpl(ADMUserRoleMapper admUserRoleMapper) {
		this.admUserRoleMapper = admUserRoleMapper;
	}

	@Override
	public ADMUserRole findById(Integer id) {
		return admUserRoleMapper.findById(id);
	}

	@Override
	public List<ADMUserRole> findAll(Integer offset, Integer pagesize, String enterpriseUserName) {
		return admUserRoleMapper.findAll(offset, pagesize, enterpriseUserName);
	}

	@Override
	public Integer create(ADMUserRole admUserRole) {
		return admUserRoleMapper.create(admUserRole);
	}

	@Override
	public Integer update(ADMUserRole admUserRole) {
		return admUserRoleMapper.update(admUserRole);
	}

	@Override
	public Integer delete(Integer id) {
		return admUserRoleMapper.delete(id);
	}

	@Override
	public List<ADMUserRole> getStatus(String status) {
		return admUserRoleMapper.getStatus(status);
	}

	@Override
	public Integer updateStatus(String status, Integer uid, String ipAddress, OffsetDateTime updationTime) {
		return admUserRoleMapper.updateStatus(status, uid, ipAddress, updationTime);
	}

	@Override
	public List<ADMUserRole> getOnRole(String name) {
		return admUserRoleMapper.getOnRole(name);
	}

	@Override
	public List<ADMUserRole> getUserById(Integer id) {
		return admUserRoleMapper.getUserById(id);
	}

	@Override
	public Integer record() {

		return admUserRoleMapper.record();
	}

	public List<ADMUserRole> filter(ADMUserRole admUserRole) {

		return admUserRoleMapper.roleFilter(admUserRole);

	}

	@Override
	public int deleteUser(int id) {
		return admUserRoleMapper.deleteUser(id);
	}

	@Override
	public List<ADMUserRole> searchAll(Integer offset, Integer pagesize, String search) {
		return admUserRoleMapper.searchAll(offset, pagesize, search);
	}

	@Override
	public int searchRecord(String search) {
		return admUserRoleMapper.searchRecord(search);
	}

	public Integer filterRecord(ADMUserRole admUserRole) {
		return admUserRoleMapper.filterRecord(admUserRole);
	}

	public List<ADMUserRole> filterUserRole(int offset, int pageSize, ADMUserRole admUserRole) {
		// int totalRecord = 0;
		List<ADMUserRole> data = null;

		admUserRole.setUserName(admUserRole.getUserName().trim());
		admUserRole.setName(admUserRole.getName().trim());
		admUserRole.setCreatedBy(admUserRole.getCreatedBy().trim());
		admUserRole.setStatus(admUserRole.getStatus().trim());
		// totalRecord = record();
		// if (totalRecord != 0) {
		admUserRole.setOffset(offset);
		admUserRole.setPagesize(pageSize);
		data = filter(admUserRole);
		/*
		 * }else { data = null; }
		 */
		return data;
	}

	@Override
	public List<ADMUserRole> getAllRecords(String userName) {
		return admUserRoleMapper.getAllRecord(userName);
	}

	public ADMUserRole findByUserName(String userName) {
		return admUserRoleMapper.findByUserName(userName);

	}

	public Integer updateStatusByUserName(ADMUserRole admUserRole) {
		return admUserRoleMapper.updateStatusByUserName(admUserRole);

	}

	public void generateUserRoleListTOExcel(List<ADMUserRole> list, String filePath) {

		Workbook workbook = new XSSFWorkbook();
		Sheet sheet = workbook.createSheet("ADMUserRole");

		int rowIndex = 1;
		Row row1 = sheet.createRow(0);
		row1.createCell(0).setCellValue("USERNAME");
		row1.createCell(1).setCellValue("ASSIGNED ROLE");
		row1.createCell(2).setCellValue("CREATED BY");
		row1.createCell(3).setCellValue("CREATED ON");
		row1.createCell(4).setCellValue("STATUS");

		for (ADMUserRole userRole : list) {

			Row row = sheet.createRow(rowIndex++);

			int cellIndex = 0;

			row.createCell(cellIndex++).setCellValue(userRole.getUserName());
			row.createCell(cellIndex++).setCellValue(userRole.getName());
			row.createCell(cellIndex++).setCellValue(userRole.getCreatedBy());
			row.createCell(cellIndex++)
					.setCellValue(userRole.getCreatedTime().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
			row.createCell(cellIndex++).setCellValue(userRole.getStatus());
		}

		try {
			Path path = Paths.get(filePath);
			try (OutputStream out = Files.newOutputStream(path, CREATE_NEW)) {
				workbook.write(out);
				out.close();
			} catch (FileAlreadyExistsException incrCounterAndRetry) {
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

//		try {
//
//			FileOutputStream os = new FileOutputStream(filePath);
//			workbook.write(os);
//
//			os.close();
//			workbook.close();
//
//		} catch (FileNotFoundException e) {
//
//			e.printStackTrace();
//
//		} catch (IOException e) {
//
//			e.printStackTrace();
//
//		}

	}

	@Override
	public Map<String, List<String>> getUserRoleHeaders() {
		Map<String, List<String>> map = new HashMap<String, List<String>>();
		List<String> listOfKey = new ArrayList<String>();
		List<String> listOfValue = new ArrayList<String>();

		listOfValue.add("USERNAME");
		listOfValue.add("ASSIGNED ROLE");
		listOfValue.add("CREATED BY");
		listOfValue.add("CREATED ON");
		listOfValue.add("STATUS");

		listOfKey.add("userName");
		listOfKey.add("name");
		listOfKey.add("createdBy");
		listOfKey.add("createdTime");
		listOfKey.add("status");

		map.put("key", listOfKey);
		map.put("value", listOfValue);
		return map;

	}

	@SuppressWarnings("finally")
	@Override
	public ResponseEntity<AppResponse> sendUserRolesDetailsOnRestAPI(ADMUserRole user, String uri)
			throws SupplyMintException {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<AppResponse> responseEntity = null;
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			String requestJson = getRequetJson(user);
			HttpEntity<String> request = new HttpEntity<String>(requestJson, headers);
			responseEntity = restTemplate.exchange(uri, HttpMethod.POST, request, AppResponse.class);
		} catch (Throwable t) {
			throw new SupplyMintException(t.getMessage());
		} finally {
			return responseEntity;
		}
	}

	private String getRequetJson(ADMUserRole user) {
		Users userDetails = new Users();
		userDetails.setUsername(user.getUserName());
		userDetails.setEid(user.getEid());
		;
		userDetails.setRoles(user.getRoles());
		userDetails.setActive(Character.getNumericValue(user.getActive()));
		userDetails.setAccessMode("WEB");
		String jsonString = null;
		try {
			jsonString = CodeUtils.convertObjectToJSon(userDetails);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonString;

	}

}
