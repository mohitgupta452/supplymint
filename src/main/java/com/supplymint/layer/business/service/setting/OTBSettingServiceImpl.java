package com.supplymint.layer.business.service.setting;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.olap4j.metadata.Hierarchy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.CannotGetJdbcConnectionException;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.supplymint.layer.business.contract.setting.OTBSettingService;
import com.supplymint.layer.data.setting.mapper.OTBSettingMapper;
import com.supplymint.util.CodeUtils;
import com.supplymint.util.JsonUtils;

@Service
public class OTBSettingServiceImpl implements OTBSettingService {

	private static final Logger LOGGER = LoggerFactory.getLogger(FestivalSettingServiceImpl.class);

	private OTBSettingMapper otbSettingMapper;

//	private String TENANT_HASHKEY;

	@Autowired
	public OTBSettingServiceImpl(OTBSettingMapper otbSettingMapper) {
		this.otbSettingMapper = otbSettingMapper;
	}

//	@Autowired
//	private OTBMapper otbMapper;

	/**
	 * This method is used to get HL_VALUE & HL_NAME by using 'key=HLEVEL_CONFIG'
	 * from table "system_default_config".
	 * 
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public ObjectNode getHLevel(String hLevel) throws Exception {
		String hPatternD = null;
		String hPatternC = null;
		String udfPattern = null;
		String[] arrOfUdf =null;
		Map<String, Object> hlresult =new HashMap<>();
		Map<String, Object> udfresult =new HashMap<>();
		Map<String, Object> hlresultInOrder =new TreeMap<>();
		Map<String, Object> udfresultInOrder =new TreeMap<>();
		LOGGER.debug("Query,for getting HLEVEL,is going to start...");
		String hPattern = otbSettingMapper.getHPatternD("HLEVEL_CONFIG");
		String defaultUDF = otbSettingMapper.getDefaultAssortment("UDF_CONFIG");
		LOGGER.debug("Query,for getting HLEVEL,has ended...");
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode node = mapper.createObjectNode();
		ObjectNode objectNode = mapper.createObjectNode();
		List patternListC = new ArrayList<>();
		List patternListD = new ArrayList<>();
		List udfList = new ArrayList<>();
		try {
			JSONObject jsonObject = new JSONObject(hPattern);
			Iterator<?> keys = jsonObject.keys();
			while (keys.hasNext()) {
				String key = (String) keys.next();
				if (jsonObject.get(key) instanceof JSONObject) {
					JSONObject hljsonValue = new JSONObject(jsonObject.get(key).toString());
					if(key.equalsIgnoreCase("DEFAULT_HLEVEL_NAME")) {
						hPatternC = (String) hljsonValue.get("HPATTERN_C_NAME");
						hPatternD = (String) hljsonValue.get("HPATTERN_D_NAME");
					}else {
						hlresult =JsonUtils.toMap(hljsonValue);
					}
				}
			}
			String[] arrOfhPatternD = hPatternD.trim().split(",");
			String[] arrOfhPatternC = hPatternC.trim().split(",");
			for (String value : arrOfhPatternD) {
				patternListD.add(value);
			}
			for (String value : arrOfhPatternC) {
				patternListC.add(value);
			}
			jsonObject = new JSONObject(defaultUDF);
			Iterator udfIteratorKey = jsonObject.keys();
			while (udfIteratorKey.hasNext()) {
				String key = (String) udfIteratorKey.next();
				if (jsonObject.get(key) instanceof JSONObject) {
					JSONObject udfjsonKey = new JSONObject(jsonObject.get(key).toString());
					if(key.equalsIgnoreCase("DEFAULT_UDF")) {
						udfresult =JsonUtils.toMap(udfjsonKey);
				}
				}
			}
			if(!CodeUtils.isEmpty(udfPattern)) {
				arrOfUdf = udfPattern.trim().split(",");
				for (String value : arrOfUdf) {
					udfList.add(value);
				}
			}
			if (hLevel.equalsIgnoreCase("HLNAME")) {
				node.putPOJO(CodeUtils.RESPONSE, patternListD);
			}
			
			for (Map.Entry<String, Object> entry : hlresult.entrySet()) {
	            hlresultInOrder.put(entry.getKey(), entry.getValue());
			}
			
			for (Map.Entry<String, Object> entry : udfresult.entrySet()) {
	            udfresultInOrder.put(entry.getKey(), entry.getValue());
			}
	    
			
			if (hLevel.equalsIgnoreCase("HLVALUE")) {
				objectNode.putPOJO("hlevel", hlresultInOrder);
				objectNode.putPOJO("udf", udfresultInOrder);
				node.putPOJO(CodeUtils.RESPONSE, objectNode);
			}
		} catch (CannotGetJdbcConnectionException ex) {
			throw ex;
		} catch (DataAccessException ex) {
			throw ex;
		} catch (Exception ex) {
			throw ex;
		}
		return node;

	}

	/**
	 * This method is used to create 'key-value' by giving user requirement and
	 * insert into table "system_default_config" where key="default_hlevel_config"
	 */

	@Override
	public int create(JsonNode otbSetting) {
		int insertData = 0;
		int isExist = 0;
		int isExistUDF = 0;
		try {
			String hlevel=otbSetting.get("hlevel").toString();
			String udflevel=otbSetting.get("udflevel").toString();
			LOGGER.info("validation start...");
			isExist = otbSettingMapper.isValidHlevel();
			LOGGER.info("validation has ended...");
			if (isExist == 0) {
				LOGGER.info("creating otb_assortment...");
				insertData = otbSettingMapper.create(hlevel);
				LOGGER.info("creation done...");
			} else {
				LOGGER.info("updating old data...");
				insertData = otbSettingMapper.update(hlevel);
				LOGGER.info("updating process done...");
			}

			isExistUDF = otbSettingMapper.isValidUDFlevel();
			LOGGER.info("validation has ended...");
			if (isExistUDF == 0) {
				LOGGER.info("creating otb_assortment...");
				insertData = otbSettingMapper.createUDF(udflevel);
				LOGGER.info("creation done...");
			} else {
				LOGGER.info("updating old data...");
				insertData = otbSettingMapper.updateUDF(udflevel);
				LOGGER.info("updating process done...");
			}
		} catch (CannotGetJdbcConnectionException ex) {
			throw ex;
		} catch (DataAccessException ex) {
			throw ex;
		} catch (Exception ex) {
			throw ex;
		}

		return insertData;

	}

	/**
	 * This is method is used to get value from table "system_default_config" where
	 * key="default_hlevel_config"
	 * 
	 * @throws Exception
	 */

	@SuppressWarnings("rawtypes")
	@Override
	public ObjectNode getDefaultAssort() throws Exception {
		String defaultAssort = otbSettingMapper.getDefaultAssortment("DEFAULT_HLEVEL_CONFIG");
		Map<String, String> sortedMap = null;
		Map<String, String> defaultConfig = new HashMap<>();
		try {
			JSONObject jsonObject = new JSONObject(defaultAssort);
			Iterator udfIteratorKey = jsonObject.keys();
			while (udfIteratorKey.hasNext()) {
				String key = (String) udfIteratorKey.next();
				if (jsonObject.get(key) instanceof JSONObject) {
					JSONObject udfjsonKey = new JSONObject(jsonObject.get(key).toString());
					Iterator udfIteratorValue = udfjsonKey.keys();
					while (udfIteratorValue.hasNext()) {
						String keyValue = (String) udfIteratorValue.next();
						String value = (String) udfjsonKey.get(keyValue);
						defaultConfig.put(value, keyValue);
					}
					}
				}
			sortedMap = new TreeMap<String, String>(defaultConfig);
		} catch (CannotGetJdbcConnectionException ex) {
			throw ex;
		} catch (DataAccessException ex) {
			throw ex;
		} catch (Exception ex) {
			throw ex;
		}
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode node = mapper.createObjectNode();
		node.putPOJO(CodeUtils.RESPONSE, sortedMap);
		return node;
	}

	/**
	 * This method is used to create assortment according to user choice and set
	 * {@link Hierarchy} according to them
	 */

	@SuppressWarnings("unused")
	@Override
	public int createAssortment(JsonNode node) throws Exception{
		int createData = 0;
		String jsonResp = null;
		int isExist = 0;
		String assortmentCode = "";
		List<String> assortmentList = new LinkedList<>();
		try {

			Map<String, String> divMap = new LinkedHashMap<String, String>();

			if (node.get("otbObject").has("HL1NAME")) {
				divMap.put("HL1NAME", node.get("otbObject").get("HL1NAME").asText());
			}
			if (node.get("otbObject").has("HL2NAME")) {
				divMap.put("HL2NAME", node.get("otbObject").get("HL2NAME").asText());
			}
			if (node.get("otbObject").has("HL3NAME")) {
				divMap.put("HL3NAME", node.get("otbObject").get("HL3NAME").asText());
			}
			if (node.get("otbObject").has("HL4NAME")) {
				divMap.put("HL4NAME", node.get("otbObject").get("HL4NAME").asText());
			}
			if (node.get("otbObject").has("HL5NAME")) {
				divMap.put("HL5NAME", node.get("otbObject").get("HL5NAME").asText());
			}
			if (node.get("otbObject").has("HL6NAME")) {
				divMap.put("HL6NAME", node.get("otbObject").get("HL6NAME").asText());
			}
			ObjectMapper mapperObj = new ObjectMapper();
			try {
				jsonResp = mapperObj.writeValueAsString(divMap);
			} catch (Exception ex) {
				LOGGER.info("fail during converting into JsonString");
			}
			LOGGER.info("validation query starting...");
			isExist = otbSettingMapper.isValidAssortment(node);
			LOGGER.info("validation query stopped...");
			if (isExist == 0) {
				LOGGER.info("creation query starting...");
				createData = otbSettingMapper.createAssort(jsonResp);
				LOGGER.info("creation query stopped..");
			} else {
				LOGGER.info("updating query starting...");
				createData = otbSettingMapper.updateAssort(jsonResp);
				LOGGER.info("updaing query stopped...");
			}
			
//			JSONObject jsonObject = new JSONObject(jsonResp);
//			Iterator iterator = jsonObject.keys();
//			while (iterator.hasNext()) {
//				String hlname = (String) iterator.next();
//				assortmentList.add(hlname);
//			}
//			for (String list : assortmentList) {
//				if (!CodeUtils.isEmpty(list)) {
//					assortmentCode += list + "||'-'||";
//				}
//			}
//			assortmentCode = assortmentCode.substring(0, assortmentCode.length() - 7);
//			int updateOTBMVAssortment=otbSettingMapper.updateOTBMVAssortment(assortmentCode);
//			if (createData != 0) {
//				TENANT_HASHKEY = ThreadLocalStorage.getTenantHashKey();
//				ExecutorService executorService = Executors.newSingleThreadExecutor();
//				executorService.execute(new Runnable() {
//					@SuppressWarnings({ "rawtypes", "unused" })
//					@Override
//					public void run() {
//						try {
//							String billDate = null;
//							String billDateFormat = null;
//							String assortmentCode = "";
//							int updateMVStatus=0;
//							ThreadLocalStorage.setTenantHashKey(TENANT_HASHKEY);
//							LOGGER.info("Query start for getting data...");
//							billDateFormat = otbSettingMapper.getBillDateFormat();
//							LOGGER.info("Query stopped for getting data...");
//
//							if (CodeUtils.validateDate(billDateFormat, dateFormatOTB.dateFormatWithForwordSlash)) {
//								billDate = "to_char(to_date(to_char(to_date(billDate,'yyyy-mm-dd'),'mm-yyyy'),'mm-yyyy'),'yyyy-mm-dd')";
//							} else if (CodeUtils.validateDate(billDateFormat,
//									dateFormatOTB.dateFormatWithDateMonthYear)) {
////									  for bazaarKolkata date format
//								billDate = "to_char(to_date(to_date(to_char(to_date(billdate,'dd-mm-yyyy'),'MM-yyyy'),'MM-yyyy'),'dd-MM-yy'),'yyyy-MM-dd')";
//							} else if (CodeUtils.validateDate(billDateFormat,
//									dateFormatOTB.dateFormatWithMonthDateYear)) {
////									  for megashop date format
//								billDate = "to_char(to_date(to_char(to_date(billdate,'MM/dd/yyyy'),'mm-yyyy'),'mm-yyyy'),'yyyy-mm-dd')";
//							} else if (CodeUtils.validateDate(billDateFormat,
//									dateFormatOTB.dateFormatWithYearMonthDate)) {
////									  for mufti and style_bazaar date format
//								billDate = "to_char(to_date(to_char(to_date(billDate,'yyyy-mm-dd'),'mm-yyyy'),'mm-yyyy'),'yyyy-mm-dd')";
//							}
//							String assortment = otbSettingMapper.getUserAssortment("OTB_USER_ASSORTMENT");
//
//							List<String> assortmentList = new ArrayList<>();
//
//							JSONObject jsonObject = new JSONObject(assortment);
//							Iterator iterator = jsonObject.keys();
//							while (iterator.hasNext()) {
//								String hlname = (String) iterator.next();
//								assortmentList.add(hlname);
//							}
//							for (String list : assortmentList) {
//								if (!CodeUtils.isEmpty(list)) {
//									assortmentCode += list + "||'-'||";
//								}
//							}
//							assortmentCode = assortmentCode.substring(0, assortmentCode.length() - 7);
//							try {
//								MViewRefreshHistory mvHistory=new MViewRefreshHistory();
//								mvHistory.setModule(Module.DEMAND_PLANNING.toString());
//								mvHistory.setSubModule(SubModule.OTB.toString());
//								mvHistory.setStatus(CustomRunState.INPROGRESS.toString());
//								mvHistory.setActive('0');
//								mvHistory.setRefreshMView("OTB_ACTUAL_MONTHLY,OTB_FORECAST_MONTHLY");
//								
//								int isExistMVForOTB=otbMapper.isExistMVForOTB(Module.DEMAND_PLANNING.toString(),SubModule.OTB.toString());
//								if(isExistMVForOTB==0) {
//									int createMVforOTB=otbMapper.createMVforOTB(mvHistory);
//								}else {
//									updateMVStatus=otbMapper.updateMVStatus(Module.DEMAND_PLANNING.toString(),SubModule.OTB.toString(),CustomRunState.INPROGRESS.toString(),'0');
//
//								}
//								
//								try {
//									LOGGER.info("droping materialized view Actual OTB...");
//									otbSettingMapper.dropUserOTBMV();
//									
//									LOGGER.info("creating materialized view Actual OTB...");
//									otbMapper.createUserOTBMV(assortmentCode, billDate);
//									LOGGER.info("creation of materialized view done...");
//								}catch(Exception ex) {
//									LOGGER.info("creating materialized view Actual OTB...");
//									otbMapper.createUserOTBMV(assortmentCode, billDate);
//									LOGGER.info("creation of materialized view done...");
//								}
//								
//								try {
//									LOGGER.info("droping materialized view Forecast OTB...");
//									otbSettingMapper.dropUserOTBMVForecast();
//									
//									LOGGER.info("creating materialized view Forecast OTB...");
//									otbMapper.createUserOTBMVForecast(assortmentCode);
//								}catch(Exception ex) {
//									LOGGER.info("creating materialized view Forecast OTB...");
//									otbMapper.createUserOTBMVForecast(assortmentCode);
//								}
//								updateMVStatus=otbMapper.updateMVStatus(Module.DEMAND_PLANNING.toString(),SubModule.OTB.toString(),CustomRunState.SUCCEEDED.toString(),'1');
//							} catch (Exception ex) {
//								otbMapper.createUserOTBMV(assortmentCode, billDate);
//								updateMVStatus=otbMapper.updateMVStatus(Module.DEMAND_PLANNING.toString(),SubModule.OTB.toString(),CustomRunState.SUCCEEDED.toString(),'1');
//							}
//						} catch (Exception ex) {
//							ex.printStackTrace();
//							int updateMVStatus=otbMapper.updateMVStatus(Module.DEMAND_PLANNING.toString(),SubModule.OTB.toString(),CustomRunState.FAILED.toString(),'0');
//						}
//					}
//				});
//			}
		}catch (CannotGetJdbcConnectionException ex) {
			throw ex;
		} catch (DataAccessException ex) {
			throw ex;
		} catch (Exception ex) {
			throw ex;
		}
		return createData;
	}

	@Override
	public ObjectNode getAllConfig() throws Exception {
		Map<String, Object> sortedMap = new TreeMap<>();
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode objectNode = mapper.createObjectNode();
		try {
			String defaultAssort = otbSettingMapper.getDefaultAssortment("DEFAULT_HLEVEL_CONFIG");
			String defaultUDF = otbSettingMapper.getDefaultAssortment("DEFAULT_UDF_CONFIG");
			JSONObject jsonObject = new JSONObject(defaultAssort);
			Map<String, Object> hlevelData=JsonUtils.toMap(jsonObject);
			sortedMap = new TreeMap<String, Object>(hlevelData);
			
			objectNode.putPOJO("hlevel", sortedMap);
			
			jsonObject = new JSONObject(defaultUDF);
			Map<String, Object> udfData=JsonUtils.toMap(jsonObject);
			sortedMap = new TreeMap<String, Object>(udfData);
			objectNode.putPOJO("udflevel", sortedMap);
		} catch (CannotGetJdbcConnectionException ex) {
			throw ex;
		} catch (DataAccessException ex) {
			throw ex;
		} catch (Exception ex) {
			throw ex;
		}
		ObjectNode node = mapper.createObjectNode();
		node.putPOJO(CodeUtils.RESPONSE, objectNode);
		return node;
	}

	@Override
	public ObjectNode getAllOTBConfig() throws Exception {
		Map<String, String> sortedMap = null;
		Map<String, String> defaultConfig = new HashMap<>();
		try {
			String defaultAssort = otbSettingMapper.getDefaultAssortment("OTB_USER_ASSORTMENT");
			JSONObject jsonObject = new JSONObject(defaultAssort);
			Iterator iterator = jsonObject.keys();
			while (iterator.hasNext()) {
				String hlname = (String) iterator.next();
				String value = (String) jsonObject.get(hlname);
				defaultConfig.put(hlname, value);
			}
			sortedMap = new TreeMap<String, String>(defaultConfig);
		} catch (CannotGetJdbcConnectionException ex) {
			throw ex;
		} catch (DataAccessException ex) {
			throw ex;
		} catch (Exception ex) {
			throw ex;
		}
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode node = mapper.createObjectNode();
		node.putPOJO(CodeUtils.RESPONSE, sortedMap);
		return node;
	}
	
	

}
