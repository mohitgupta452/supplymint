package com.supplymint.layer.business.service.analytic;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.supplymint.exception.SupplyMintException;
import com.supplymint.layer.business.contract.analytic.AnalyticsService;
import com.supplymint.layer.data.tenant.analytic.entity.AnalyticStoreProfile;
import com.supplymint.layer.data.tenant.analytic.entity.SellThrough;
import com.supplymint.layer.data.tenant.analytic.entity.StoreProfileActualSaleData;
import com.supplymint.layer.data.tenant.analytic.entity.StoreProfileGraph;
import com.supplymint.layer.data.tenant.analytic.mapper.AnalyticsMapper;
import com.supplymint.util.CodeUtils;
import com.supplymint.util.CodeUtils.Analytics;

@Service
public class AnalyticsServiceImpl implements AnalyticsService {

	//private final static Logger LOGGER = LoggerFactory.getLogger(AnalyticsServiceImpl.class);

	private AnalyticsMapper analyticsMapper;

	
	@Autowired
	public AnalyticsServiceImpl(AnalyticsMapper analyticsMapper) {
		this.analyticsMapper = analyticsMapper;
	}

	@Override
	public List<Map<String, String>> getSiteCode() {
		List<Map<String, String>> storeData = new ArrayList<>();
		try {
			List<AnalyticStoreProfile> data = analyticsMapper.getSiteCode();

			data.stream().forEach(e -> {
				Map<String, String> storeWithStoreName = new HashMap<String, String>();
				storeWithStoreName.put(e.getSiteCode(), e.getSiteName());
				storeData.add(storeWithStoreName);
			});
		} catch (DataAccessException ex) {
			throw ex;
		} catch (Exception ex) {
			throw ex;
		}
		return storeData;
	}

	@Override
	public AnalyticStoreProfile getStoreInfo(String siteCode) throws Exception {

		AnalyticStoreProfile data = null;
		String bestPerformingMonth=null;
		String worstPerformingMonth=null;
		try {

			data = analyticsMapper.getStoreInfo(siteCode);
			if (CodeUtils.isEmpty(data)) {
				throw new SupplyMintException();
			}

			List<StoreProfileActualSaleData> perFormingMonth = analyticsMapper.getPerFormingMonth(siteCode);
			
			if(!CodeUtils.isEmpty(perFormingMonth)) {
			StoreProfileActualSaleData maxQty = Collections.max(perFormingMonth, Comparator.comparing(s -> s.getQty()));
			StoreProfileActualSaleData minQty = Collections.min(perFormingMonth, Comparator.comparing(s -> s.getQty()));

			bestPerformingMonth = CodeUtils.convertOTBMonthlyDateFormat(maxQty.getBillDate());
			worstPerformingMonth = CodeUtils.convertOTBMonthlyDateFormat(minQty.getBillDate());
			}
			data.setBestPermofmingMonth(bestPerformingMonth);
			data.setWorstPerformingMonth(worstPerformingMonth);
		} catch (Exception ex) {
			throw ex;
		}
		//
		return data;
	}

	@Override
	public ObjectNode getSellThrough(String sellThroughType, String storeCode) {

		List<SellThrough> topFiveData = null;
		List<SellThrough> bottomFiveData = null;
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode objectNode = mapper.createObjectNode();
		try {
			if (sellThroughType.equals("BARCODE")) {
				topFiveData = analyticsMapper.getTopBarCodeSellThrough(storeCode);
				bottomFiveData = analyticsMapper.getBottomBarCodeSellThrough(storeCode);
			} else if (sellThroughType.equals("SIZE")) {
				topFiveData = analyticsMapper.getTopSizeSellThrough(storeCode);
				bottomFiveData = analyticsMapper.getBottomSizeSellThrough(storeCode);
			} else if (sellThroughType.equals("PATTERN")) {
				topFiveData = analyticsMapper.getTopPatternSellThrough(storeCode);
				bottomFiveData = analyticsMapper.getBottomPatternSellThrough(storeCode);
			} else if (sellThroughType.equals("MATERIAL")) {
				topFiveData = analyticsMapper.getTopMaterialSellThrough(storeCode);
				bottomFiveData = analyticsMapper.getBottomMaterialSellThrough(storeCode);
			} else if (sellThroughType.equals("BRAND")) {
				topFiveData = analyticsMapper.getTopBrandSellThrough(storeCode);
				bottomFiveData = analyticsMapper.getBottomBrandSellThrough(storeCode);
			} else if (sellThroughType.equals("ASSORTMENT")) {
				topFiveData = analyticsMapper.getTopAssortmentSellThrough(storeCode);
				bottomFiveData = analyticsMapper.getBottomAssortmentSellThrough(storeCode);
			} else if (sellThroughType.equals("DESIGN")) {
				topFiveData = analyticsMapper.getTopDesignSellThrough(storeCode);
				bottomFiveData = analyticsMapper.getBottomDesignSellThrough(storeCode);
			}
			objectNode.putPOJO("top", topFiveData);
			objectNode.putPOJO("bottom", bottomFiveData);
		} catch (DataAccessException ex) {
			throw ex;
		} catch (Exception ex) {
			throw ex;
		}
		return objectNode;
	}

	@SuppressWarnings({ "unchecked" })
	@Override
	public ObjectNode getTopArticle() {

		ObjectMapper mapper = new ObjectMapper();
		ObjectNode objectNode = mapper.createObjectNode();
		try {
			List<StoreProfileActualSaleData> topFiveData = analyticsMapper.getTopArticleData();
			List<StoreProfileActualSaleData> bottomFiveData = analyticsMapper.getBottomArticleData();

			Map<String, List<Map<String, String>>> topArticle = new HashMap<String, List<Map<String, String>>>();
			Map<String, List<Map<String, String>>> bottomArticle = new HashMap<String, List<Map<String, String>>>();
			topFiveData.stream().forEach(e -> {
				Map<String, String> articleBillDateWise = new HashMap<>();
				List articleWiseData = new ArrayList<>();
				articleBillDateWise.put("lastYearQty", e.getLastYearQty());
				articleBillDateWise.put("lastThreeMonthQty", e.getLastThreeMonthQty());
				articleBillDateWise.put("currentMonthQty", e.getCurrentMonthQty());
				if (Integer.parseInt(e.getLastYearQty()) > Integer.parseInt(e.getSecondLastYearQty())) {
					articleBillDateWise.put("lastYearQtyPercentage", "HIGH");
				} else {
					articleBillDateWise.put("lastYearQtyPercentage", "LOW");
				}
				if (Integer.parseInt(e.getLastThreeMonthQty()) > Integer.parseInt(e.getSecondLastThreeMonthQty())) {
					articleBillDateWise.put("lastThreeMonthQtyPercentage", "HIGH");
				} else {
					articleBillDateWise.put("lastThreeMonthQtyPercentage", "LOW");
				}
				if (Integer.parseInt(e.getCurrentMonthQty()) > Integer.parseInt(e.getLastYearCurrentMonthQty())) {
					articleBillDateWise.put("currentMonthQtyPercentage", "HIGH");
				} else {
					articleBillDateWise.put("currentMonthQtyPercentage", "LOW");
				}
				articleWiseData.add(articleBillDateWise);
				topArticle.put(e.getArticleCode(), articleWiseData);
			});
			if (!CodeUtils.isEmpty(topArticle))
				objectNode.putPOJO("top", topArticle);

			bottomFiveData.stream().forEach(e -> {
				Map<String, String> articleBillDateWise = new HashMap<>();
				List articleWiseData = new ArrayList<>();
				articleBillDateWise.put("lastYearQty", e.getLastYearQty());
				articleBillDateWise.put("lastThreeMonthQty", e.getLastThreeMonthQty());
				articleBillDateWise.put("currentMonthQty", e.getCurrentMonthQty());
				if (Integer.parseInt(e.getLastYearQty()) > Integer.parseInt(e.getSecondLastYearQty())) {
					articleBillDateWise.put("lastYearQtyPercentage", "HIGH");
				} else {
					articleBillDateWise.put("lastYearQtyPercentage", "LOW");
				}
				if (Integer.parseInt(e.getLastThreeMonthQty()) > Integer.parseInt(e.getSecondLastThreeMonthQty())) {
					articleBillDateWise.put("lastThreeMonthQtyPercentage", "HIGH");
				} else {
					articleBillDateWise.put("lastThreeMonthQtyPercentage", "LOW");
				}
				if (Integer.parseInt(e.getCurrentMonthQty()) > Integer.parseInt(e.getLastYearCurrentMonthQty())) {
					articleBillDateWise.put("currentMonthQtyPercentage", "HIGH");
				} else {
					articleBillDateWise.put("currentMonthQtyPercentage", "LOW");
				}
				articleWiseData.add(articleBillDateWise);
				bottomArticle.put(e.getArticleCode(), articleWiseData);
			});
			if (!CodeUtils.isEmpty(bottomArticle))
				objectNode.putPOJO("bottom", bottomArticle);
		} catch (NumberFormatException ex) {
			throw ex;
		} catch (DataAccessException ex) {
			throw ex;
		} catch (Exception ex) {
			throw ex;
		}
		return objectNode;
	}

	@Override
	public Map<String, List<ObjectNode>> getSalesData(String flag) {

		List<StoreProfileActualSaleData> data = null;
		ObjectMapper mapper = new ObjectMapper();
		Map<String, List<ObjectNode>> map = new HashMap<String, List<ObjectNode>>();
		try {
			data = analyticsMapper.salesTrendGraph();
			if (flag.equalsIgnoreCase(Analytics.SalesTrend.toString())) {
				List<ObjectNode> listOfUnitSales = new ArrayList<ObjectNode>();
				List<ObjectNode> listOfSalesValue = new ArrayList<ObjectNode>();

				if (data.size() > 0 && !data.isEmpty()) {
					data.stream().forEach(e -> {
						ObjectNode unitSalesNode = mapper.createObjectNode();
						ObjectNode salesValueNode = mapper.createObjectNode();
						unitSalesNode.put("articleCode", e.getArticleCode());
						unitSalesNode.put("qty", e.getLastMonthQTY());
						salesValueNode.put("articleCode", e.getArticleCode());
						salesValueNode.put("salesValue", e.getLastMonthSV());
						listOfUnitSales.add(unitSalesNode);
						listOfSalesValue.add(salesValueNode);
					});
					map.put("Unit", listOfUnitSales);
					map.put("Sales", listOfSalesValue);
				}
			} else if (flag.equalsIgnoreCase(Analytics.SalesVsProfit.toString())) {

				List<ObjectNode> listOfSalesValue = new ArrayList<ObjectNode>();
				List<ObjectNode> listOfProfitValue = new ArrayList<ObjectNode>();

				if (data.size() > 0 && !data.isEmpty()) {
					data.stream().forEach(e -> {
						ObjectNode salesValueNode = mapper.createObjectNode();
						ObjectNode profitNode = mapper.createObjectNode();
						salesValueNode.put("articleCode", e.getArticleCode());
						salesValueNode.put("salesValue", e.getLastMonthSV());
						int profit = Integer.parseInt(e.getLastMonthSV()) - Integer.parseInt(e.getLastMonthCostValue());
						profitNode.put("articleCode", e.getArticleCode());
						profitNode.put("profit", profit);
						listOfProfitValue.add(profitNode);
						listOfSalesValue.add(salesValueNode);
					});
					map.put("profit", listOfProfitValue);
					map.put("sales", listOfSalesValue);
				}
			}

		} catch (DataAccessException ex) {
			throw ex;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
		return map;
	}

	@Override
	public Map<String, ArrayNode> getFiveTopRankStoresArticleData() {
		List<StoreProfileActualSaleData> data = null;
		Map<String, ArrayNode> map = new HashMap<String, ArrayNode>();
		ObjectMapper mapper = new ObjectMapper();
		try {
			data = analyticsMapper.getFiveTopRankStoresArticleData();
			ArrayNode array = mapper.createArrayNode();
			// ObjectNode objectNode = getMapper().createObjectNode();
			if (data.size() > 0 && !data.isEmpty()) {
				data.stream().forEach(e -> {
					ObjectNode tempNode = mapper.createObjectNode();
					tempNode.put("articleCode", e.getArticleCode());
					tempNode.put("qty", e.getLastMonthQTY());
					tempNode.put("salesValue", e.getLastMonthSV());
					int profit = Integer.parseInt(e.getLastMonthSV()) - Integer.parseInt(e.getLastMonthCostValue());
					tempNode.put("profit", Integer.toString(profit));
					array.add(tempNode);
				});
				map.put(CodeUtils.RESPONSE, array);

			}

		} catch (DataAccessException ex) {
			throw ex;
		} catch (Exception ex) {
			throw ex;
		}

		return map;

	}

	@Override
	public List<StoreProfileActualSaleData> getSellProfitTrends() {

		List<StoreProfileActualSaleData> data = analyticsMapper.getSellProfitTrends();

		data.stream().forEach(e -> {
			double profit = Double.parseDouble(e.getLastMonthSV()) - Double.parseDouble(e.getLastMonthCostValue());
			e.setLastMonthProfitValue(profit);
		});
		return data;
	}

	@Override
	public List<StoreProfileGraph> getSellData(String storeCode) throws Exception {
		List<StoreProfileGraph> data = null;
		try {
			data = analyticsMapper.getLastFiveMonthsSellData(storeCode);
			SimpleDateFormat formatter = new SimpleDateFormat("MMM yyyy");
			Calendar currentDate = Calendar.getInstance();
			List<String> storeDate = new ArrayList<String>();
			List<String> collectBillDate = new ArrayList<>();
			for (int i = 1; i <= 5; i++) {
				currentDate.add(Calendar.MONTH, -1);
				storeDate.add(formatter.format(currentDate.getTime()));
			}
			data.stream().forEach(e -> {
				try {
					Date date = new SimpleDateFormat("yyyy-MM-dd").parse(e.getBillDate());
					String billDate = formatter.format(date);
					e.setBillDate(billDate);
					collectBillDate.add(billDate);
				} catch (ParseException ex) {
					ex.printStackTrace();
				}
			});
			storeDate.removeAll(collectBillDate);
			// StoreProfileGraph storeProfileGraph = new StoreProfileGraph();
			for (int i = 0; i < storeDate.size(); i++) {
				StoreProfileGraph storeProfileGraph = new StoreProfileGraph();
				storeProfileGraph.setStoreCode(storeCode);
				storeProfileGraph.setBillDate(storeDate.get(i));
				storeProfileGraph.setTotalQty(0);
				storeProfileGraph.setTotalCostPrice(0);
				storeProfileGraph.setTotalSellPrice(0);
				storeProfileGraph.setProfit(0);
				storeProfileGraph.setAreaPersqft(0);
				data.add(storeProfileGraph);
			}

			Comparator<StoreProfileGraph> comparator = new Comparator<StoreProfileGraph>() {
				@Override
				public int compare(StoreProfileGraph obj1, StoreProfileGraph obj2) {
					try {
						return formatter.parse(obj1.getBillDate()).compareTo(formatter.parse(obj2.getBillDate()));
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					return 0;
				}
			};
			Collections.sort(data, comparator);
		} catch (NumberFormatException ex) {
			throw ex;
		} catch (DataAccessException ex) {
			ex.printStackTrace();
			throw ex;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
		return data;
	}

	@Override
	public List<StoreProfileActualSaleData> getStockInHand(String storeCode) throws Exception {
		// TODO Auto-generated method stub
		List<StoreProfileActualSaleData> getStockData = null;
		try {
			getStockData = analyticsMapper.getStockInHand(storeCode);
		} catch (DataAccessException ex) {
			ex.printStackTrace();
			throw ex;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
		return getStockData;
	}

}