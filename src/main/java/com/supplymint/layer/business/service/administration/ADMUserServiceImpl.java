package com.supplymint.layer.business.service.administration;

import static java.nio.file.StandardOpenOption.CREATE_NEW;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.supplymint.exception.SupplyMintException;
import com.supplymint.layer.business.contract.administration.ADMUserService;
import com.supplymint.layer.data.core.entity.Users;
import com.supplymint.layer.data.tenant.administration.entity.ADMUser;
import com.supplymint.layer.data.tenant.administration.mapper.ADMUserMapper;
import com.supplymint.layer.web.endpoint.abstracts.AbstractEndpoint;
import com.supplymint.layer.web.model.AppResponse;
import com.supplymint.util.CodeUtils;

@Service
public final class ADMUserServiceImpl extends AbstractEndpoint implements ADMUserService {

	private ADMUserMapper admUserMapper;
	private static final Logger LOGGER = LoggerFactory.getLogger(ADMUserServiceImpl.class);

	@Autowired
	public ADMUserServiceImpl(ADMUserMapper admUserMapper) {
		this.admUserMapper = admUserMapper;
	}

	@Override
	public ADMUser findById(Integer id) {

		return admUserMapper.findById(id);
	}

	@Override
	public ADMUser findByUserName(String userName) {
		return admUserMapper.findByUserName(userName);
	}

	@Override
	public List<ADMUser> findAll(Integer offset, Integer pagesize, String enterpriseUserName) {
		return admUserMapper.findAll(offset, pagesize, enterpriseUserName);
	}

	@Override
	public int create(ADMUser users) {

		return admUserMapper.create(users);
	}

	@Override
	public int update(ADMUser users) {
		return admUserMapper.update(users);
	}

	@Override
	public int delete(int id) {
		return admUserMapper.delete(id);
	}

	@Override
	public int updateStatus(String status, String userName, String ipAddress, OffsetDateTime updationTime,
			String updatedBy) {
		return admUserMapper.updateStatus(status, userName, ipAddress, updationTime, updatedBy);
	}

	@Override
	public List<ADMUser> getAccessmode(String accessmode) {
		return admUserMapper.getAccessmode(accessmode);
	}

	@Override
	public List<ADMUser> getByEnterPriseId(int id) {
		return admUserMapper.getEnterPriseId(id);
	}

	@Override
	public ADMUser convert(String jsonString) throws JsonParseException, JsonMappingException, IOException {

		ObjectMapper mapper = new ObjectMapper();
		mapper.registerModule(new JavaTimeModule());

		ADMUser user = mapper.readValue(jsonString, ADMUser.class);

		return user;
	}

	@Override
	public int record() {
		return admUserMapper.record();
	}

	@Override
	public ADMUser getByUserName(String name) {
		return admUserMapper.getUserName(name);
	}

	@Override
	public List<ADMUser> getByEmail(String email) {
		return admUserMapper.getEmail(email);
	}

	@Override
	public List<ADMUser> searchAll(Integer offset, Integer pagesize, String search) {
		return admUserMapper.searchAll(offset, pagesize, search);
	}

	@SuppressWarnings("finally")
	@Override
	public ResponseEntity<AppResponse> sendUserDetailsOnRestAPI(ADMUser user, String uri) throws SupplyMintException {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<AppResponse> responseEntity = null;
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			String requestJson = getRequetJson(user);
			HttpEntity<String> request = new HttpEntity<String>(requestJson, headers);
			responseEntity = restTemplate.exchange(uri, HttpMethod.POST, request, AppResponse.class);
			LOGGER.info(responseEntity.toString());

		} catch (Throwable t) {
			throw new SupplyMintException(t.getMessage());
		} finally {
			return responseEntity;
		}
	}

	@Override
	public int searchRecord(String search) {
		return admUserMapper.searchRecord(search);
	}

	public List<ADMUser> filter(ADMUser admUser) {

		return admUserMapper.filter(admUser);
	}

	@Override
	public int countUserRoleUser(int id) {
		return admUserMapper.getUserRolesRecords(id);
	}

	private String getRequetJson(ADMUser user) {
		Users userDetails = new Users();
		userDetails.setFirstName(user.getFirstName());
		userDetails.setLastName(user.getLastName());
		userDetails.setEmail(user.getEmail());
		userDetails.setMobileNumber(user.getMobileNumber());
		userDetails.setUsername(user.getUserName());
		userDetails.setEid(user.getPartnerEnterpriseId());
		userDetails.setRoles(user.getRoles());
		userDetails.setActive(Character.getNumericValue(user.getActive()));
		userDetails.setAccessMode("WEB");
		String jsonString = null;
		try {
			jsonString = CodeUtils.convertObjectToJSon(userDetails);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonString;

	}

	public List<ADMUser> getAllUser() {
		return admUserMapper.getAllUser();
	}

	@Override
	public int checkUsername(String username) {
		return admUserMapper.checkUsername(username);
	}

	@Override
	public List<ADMUser> checkUserExistance(ADMUser admUser) {
		return admUserMapper.checkUserExistance(admUser);

	}

	public List<ADMUser> checkUserUpdateExistance(ADMUser admUser) {
		return admUserMapper.checkUserUpdateExistance(admUser);
	}

	@Override
	public Integer filterRecord(ADMUser admUser) {
		return admUserMapper.filterRecord(admUser);
	}

	@Override
	public List<ADMUser> filterUser(int offset, int pageSize, ADMUser admUser, String enterpriseUserName) {
		// int totalRecord = 0;
		List<ADMUser> data = null;

		admUser.setPartnerEnterpriseName(admUser.getPartnerEnterpriseName().trim());
		admUser.setFirstName(admUser.getFirstName().trim());
		admUser.setMiddleName(admUser.getMiddleName().trim());
		admUser.setLastName(admUser.getLastName().trim());
		admUser.setUserName(admUser.getUserName().trim());
		admUser.setEmail(admUser.getEmail().trim());
		admUser.setStatus(admUser.getStatus().trim());
		admUser.setEnterpriseUserName(enterpriseUserName);

		// totalRecord = record();
		// if (totalRecord != 0) {

		admUser.setOffset(offset);
		admUser.setPagesize(pageSize);

		data = filter(admUser);
		// }else {
		// data = null;
		// }

		return data;
	}

	@Override
	public List<ADMUser> getAllRecords(String userName) {
		return admUserMapper.getAllRecord(userName);
	}

	@SuppressWarnings("finally")
	@Override
	public ResponseEntity<AppResponse> getUserDetailsOnRestAPI(String userName, String uri) throws SupplyMintException {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<AppResponse> responseEntity = null;
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			ObjectNode objectNode = getMapper().createObjectNode();
			objectNode.put("username", userName);
			String requestJson = objectNode.toString();
			HttpEntity<String> request = new HttpEntity<String>(requestJson, headers);
			responseEntity = restTemplate.exchange(uri, HttpMethod.POST, request, AppResponse.class);
		} catch (Throwable t) {
			throw new SupplyMintException();
		} finally {
			return responseEntity;
		}

	}

	@Override
	public void generateUserListTOExcel(List<ADMUser> list, String filePath) {

		Workbook workbook = new XSSFWorkbook();
		Sheet sheet = workbook.createSheet("ADMSiteMap");

		int rowIndex = 1;
		Row row1 = sheet.createRow(0);
		row1.createCell(0).setCellValue("ENTERPRISE");
		row1.createCell(1).setCellValue("NAME");
		row1.createCell(2).setCellValue("USER NAME");
		row1.createCell(3).setCellValue("EMAIL");
		row1.createCell(4).setCellValue("MOBILE");
		row1.createCell(5).setCellValue("ACCESS MODE");
		row1.createCell(6).setCellValue("STATUS");

		for (ADMUser user : list) {

			Row row = sheet.createRow(rowIndex++);

			int cellIndex = 0;

			row.createCell(cellIndex++).setCellValue(user.getPartnerEnterpriseName());
			if (!CodeUtils.isEmpty(user.getMiddleName())) {
				row.createCell(cellIndex++)
						.setCellValue(user.getFirstName() + " " + user.getMiddleName() + " " + user.getLastName());
			} else {
				row.createCell(cellIndex++).setCellValue(user.getFirstName() + " " + user.getLastName());

			}

			row.createCell(cellIndex++).setCellValue(user.getUserName());
			row.createCell(cellIndex++).setCellValue(user.getEmail());
			row.createCell(cellIndex++).setCellValue(user.getMobileNumber());
			row.createCell(cellIndex++).setCellValue(user.getAccessMode());
			row.createCell(cellIndex++).setCellValue(user.getStatus());
		}

		try {
			Path path = Paths.get(filePath);
			try (OutputStream out = Files.newOutputStream(path, CREATE_NEW)) {
				workbook.write(out);
				out.close();
			} catch (FileAlreadyExistsException incrCounterAndRetry) {
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

//		try {
//
//			FileOutputStream fos = new FileOutputStream(filePath);
//			workbook.write(fos);
//			fos.close();
//
//		} catch (FileNotFoundException e) {
//
//			e.printStackTrace();
//
//		} catch (IOException e) {
//
//			e.printStackTrace();
//
//		}

	}

	@Override
	public Map<String, List<String>> getUserHeaders() {

		Map<String, List<String>> map = new HashMap<String, List<String>>();
		List<String> listOfKey = new ArrayList<String>();
		List<String> listOfValue = new ArrayList<String>();

		listOfValue.add("ENTERPRISE");
		listOfValue.add("NAME");
		listOfValue.add("USER NAME");
		listOfValue.add("EMAIL");
		listOfValue.add("MOBILE");
		listOfValue.add("ACCESS MODE");
		listOfValue.add("STATUS");

		listOfKey.add("userName");
		listOfKey.add("email");
		listOfKey.add("mobileNumber");
		listOfKey.add("accessMode");
		listOfKey.add("status");

		map.put("key", listOfKey);
		map.put("value", listOfValue);
		return map;

	}

	@Override
	public ADMUser getUserDetail(String userName) {
		return admUserMapper.getUserDetail(userName);
	}

	@Override
	public int updateByUserName(ADMUser admUser) {
		return admUserMapper.updateByUserName(admUser);
	}

	@Override
	public int updateImageProfileUrl(String url, String userName) {
		return admUserMapper.updateImageProfileUrl(url, userName);
	}

	@Override
	public int updateTimeByUserName(String userName, Date updationTime) {
		return admUserMapper.updateTimeByUserName(userName, updationTime);
	}

}
