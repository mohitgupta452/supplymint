package com.supplymint.layer.business.service.demand;

import static java.nio.file.StandardOpenOption.CREATE_NEW;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.UUID;
import java.util.Vector;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFCell;
import org.apache.poi.xssf.streaming.SXSSFRow;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.joda.time.LocalDate;
import org.joda.time.Period;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.CannotGetJdbcConnectionException;
import org.springframework.stereotype.Service;

import com.amazonaws.services.s3.model.S3Object;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.supplymint.config.application.AppCodeConfig;
import com.supplymint.config.aws.config.AmazonS3Client;
import com.supplymint.config.aws.utils.AWSUtils;
import com.supplymint.config.aws.utils.AWSUtils.ConditionState;
import com.supplymint.config.aws.utils.AWSUtils.Conditions;
import com.supplymint.config.aws.utils.AWSUtils.Frequency;
import com.supplymint.config.aws.utils.AWSUtils.RunState;
import com.supplymint.config.aws.utils.AWSUtils.Status;
import com.supplymint.exception.StatusCodes.AppModuleErrors;
import com.supplymint.exception.StatusCodes.AppStatusMsg;
import com.supplymint.exception.StatusCodes.Pagination;
import com.supplymint.exception.SupplyMintException;
import com.supplymint.layer.business.contract.demand.DemandPlanningService;
import com.supplymint.layer.business.contract.s3Bucket.S3BucketInfoService;
import com.supplymint.layer.business.service.rconnection.RServeEngineProviderService;
import com.supplymint.layer.business.service.s3bucketService.S3WrapperService;
import com.supplymint.layer.data.config.ThreadLocalStorage;
import com.supplymint.layer.data.tenant.demand.entity.BudgetedDemandPlanning;
import com.supplymint.layer.data.tenant.demand.entity.BudgetedTemporary;
import com.supplymint.layer.data.tenant.demand.entity.DemandForecast;
import com.supplymint.layer.data.tenant.demand.entity.DemandForecastScheduler;
import com.supplymint.layer.data.tenant.demand.entity.DemandGraph;
import com.supplymint.layer.data.tenant.demand.entity.DemandPlanning;
import com.supplymint.layer.data.tenant.demand.entity.DemandPlanningViewLog;
import com.supplymint.layer.data.tenant.demand.entity.DpUploadSummary;
import com.supplymint.layer.data.tenant.demand.entity.FinalAssortment;
import com.supplymint.layer.data.tenant.demand.mapper.DemandPlanningMapper;
import com.supplymint.layer.data.tenant.inventory.entity.S3BucketInfo;
import com.supplymint.layer.web.filters.TenantFilter;
import com.supplymint.util.ApplicationUtils.BucketParameter;
import com.supplymint.util.ApplicationUtils.CustomExtension;
import com.supplymint.util.ApplicationUtils.CustomParameter;
import com.supplymint.util.ApplicationUtils.DemandPlanning.ChooseType;
import com.supplymint.util.ApplicationUtils.DemandPlanning.DemandPlanningGraph;
import com.supplymint.util.ApplicationUtils.DemandPlanningParameter;
import com.supplymint.util.CodeUtils;
import com.supplymint.util.CodeUtils.Demand;
import com.supplymint.util.CodeUtils.Module;
import com.supplymint.util.ExcelUtils;
import com.supplymint.util.FileUtils;
import com.supplymint.util.JsonUtils;
import com.supplymint.util.UploadUtils;

@Service
public class DemandPlanningServiceImpl implements DemandPlanningService {

	private final static Logger LOGGER = LoggerFactory.getLogger(DemandPlanningServiceImpl.class);

	private DemandPlanningMapper demandPlanningMapper;

	private String tenantHashkey;

	private String upnDc;

	private int successCode;

	@Autowired
	private AppCodeConfig appStatus;

	@Autowired
	private S3WrapperService s3Wrapper;

	@Autowired
	private S3BucketInfoService s3BucketInfoService;

	@Autowired
	private S3WrapperService s3WrapperService;
	
	@Autowired
	private AmazonS3Client amazonS3Client;

	@Autowired
	private HttpServletRequest servletRequest;

	@Autowired
	private TenantFilter tenantFilter;

	@Autowired
	private RServeEngineProviderService rServeEngineProviderService;

	@Autowired
	public DemandPlanningServiceImpl(DemandPlanningMapper demandPlanningMapper) {
		this.demandPlanningMapper = demandPlanningMapper;
	}

	@Override
	public int create(DemandPlanning demandPlanning) {
		return demandPlanningMapper.create(demandPlanning);
	}

	@Override
	public int record() {
		return demandPlanningMapper.record();
	}

	@Override
	public List<DemandPlanning> getAll(Integer offset, Integer pageSize) {
		return demandPlanningMapper.getAll(offset, pageSize);
	}

	@Override
	public int filterRecord(DemandPlanning demandPlanning) {
		return demandPlanningMapper.filterRecord(demandPlanning);
	}

	@Override
	public List<DemandPlanning> filter(DemandPlanning demandPlanning) {
		return demandPlanningMapper.filter(demandPlanning);
	}

	@Override
	public int countSearchRecord(String search) {
		return demandPlanningMapper.countSearchRecord(search);
	}

	@Override
	public List<DemandPlanning> search(Integer offset, Integer pageSize, String search) {
		return demandPlanningMapper.search(offset, pageSize, search);
	}

	@Override
	public List<String> getAssortment(Integer offset, Integer pageSize) {
		return demandPlanningMapper.getAssortment(offset, pageSize);
	}

	@Override
	public int assortmentRecord() {
		return demandPlanningMapper.assortmentRecord();

	}

	@Override
	public int assortmentSearchRecord(String search) {
		return demandPlanningMapper.assortmentSearchRecord(search);
	}

	@Override
	public List<String> assortmentSearch(Integer offset, Integer pageSize, String search) {
		return demandPlanningMapper.assortmentSearch(offset, pageSize, search);
	}

	@Override
	public int updateCodeWithStatus(int statusCode, String statusMessage, String runId, String duration,
			Date updationTime, String forcastOnAssortmentCode) {
		return demandPlanningMapper.updateCodeWithStatus(statusCode, statusMessage, runId, duration, updationTime,
				forcastOnAssortmentCode);
	}

	@Override
	public int validationCreate() {
		return demandPlanningMapper.validationCreate();
	}

	@Override
	public int insertDpUploadSummary(DpUploadSummary dpUploadSummary) {

		return demandPlanningMapper.insertDpUploadSummary(dpUploadSummary);
	}

	@Override
	public int insertDPUploadSummaryData(String fileName, String type, String userName, int rowCount) {
		int insert = 0;
		try {
			DpUploadSummary dpUploadSummary = new DpUploadSummary();
			OffsetDateTime currentTime = OffsetDateTime.now();
			currentTime = currentTime.plusHours(5);
			currentTime = currentTime.plusMinutes(30);
			String ipAddress = InetAddress.getLocalHost().getHostAddress().trim().toString();
			dpUploadSummary.setCreatedTime(currentTime);
			String id = UUID.randomUUID().toString();
			dpUploadSummary.setId(id);
			dpUploadSummary.setType(type);
			dpUploadSummary.setIpAddress(ipAddress);
			dpUploadSummary.setDemandType(Demand.BUDGETED.toString());
			dpUploadSummary.setRowCount(rowCount);
			dpUploadSummary.setStatus(Status.FAILED.toString());
			dpUploadSummary.setCreatedBy(servletRequest.getAttribute("email").toString());
			dpUploadSummary.setLastSummary("true");
			dpUploadSummary.setUserName(userName);
			insert = insertDpUploadSummary(dpUploadSummary);
			updateBudgetedSummary(userName, id);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return insert;
	}
	
	@SuppressWarnings({ "rawtypes",  "unused" })
	private boolean checkExistHeader(XSSFSheet mySheet) {
		LOGGER.info("Function called to check headers");
		boolean flag = false;
		int i = 0;
		try {
			Iterator rowIter = mySheet.rowIterator();
			XSSFRow myRow = (XSSFRow) rowIter.next();
			Iterator cellIter = myRow.cellIterator();
			Vector cellStoreVector = new Vector();
			while (cellIter.hasNext()) {
				XSSFCell myCell = (XSSFCell) cellIter.next();

				if (i == 0 && DemandPlanningParameter.ASSORTMENTCODE.toLowerCase()
						.contains(myCell.toString().trim().toLowerCase()))
					flag = true;
				else if (i == 1 && DemandPlanningParameter.BILLDATE.toLowerCase()
						.contains(myCell.toString().trim().toLowerCase()))
					flag = true;

				else if (i == 2 && DemandPlanningParameter.BUDGETED_QTY.toLowerCase()
						.contains(myCell.toString().trim().toLowerCase()))
					flag = true;

				i++;

			}
		} catch (Exception e) {
			return false;
		}
		return flag;
	}
	
	@SuppressWarnings({ "rawtypes", "resource", "unchecked", "finally" })
	private Vector read(InputStream fileName) {
		Vector cellVectorHolder = new Vector();
		int i = 0;
		try {
			LOGGER.info("Passing down input stream to read excel data");
			XSSFWorkbook myWorkBook = new XSSFWorkbook(fileName);
			XSSFSheet mySheet = myWorkBook.getSheetAt(0);
			Iterator rowIter = mySheet.rowIterator();
			boolean existHeader = checkExistHeader(mySheet);
			Vector cellStoreVector = new Vector();
			while (rowIter.hasNext()) {
				XSSFRow myRow = (XSSFRow) rowIter.next();
				Iterator cellIter = myRow.cellIterator();
				if (i == 0 && existHeader)
					LOGGER.info("Neglecting first row to read as it contains headers value exits");
				else {
					cellStoreVector = new Vector();
					while (cellIter.hasNext()) {
						XSSFCell myCell = (XSSFCell) cellIter.next();
						cellStoreVector.addElement(myCell);
					}
					cellVectorHolder.addElement(cellStoreVector);
				}
				i++;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			LOGGER.info(String.format("Return no. of size Data from read() method: : %s---", cellVectorHolder.size()));
			return cellVectorHolder;
		}

	}
	
	@SuppressWarnings({ "rawtypes" })
	private Vector validDataFromExcel(InputStream fileData) {
		Vector getAllExcelData = read(fileData);
		for (int i = getAllExcelData.size() - 1; i >= 0; i--) {
			Vector lastElement = (Vector) getAllExcelData.get(i);
			if (!lastElement.isEmpty()) {
				int index = 0;
				for (int j = 0; j < 3; j++) {
					if (lastElement.get(j).toString().isEmpty()) {
						getAllExcelData.remove(i);
						break;
					} else {
						index++;
					}
				}
				if (index == 3) {
					return getAllExcelData;
				}
			} else {
				getAllExcelData.remove(i);
			}
		}
		return getAllExcelData;

	}

	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<BudgetedTemporary> validDataInExcelFile(InputStream file, String userName, String type)
			throws SupplyMintException {
		Vector<Row> readDataFromExcel = null;
		Vector cellStoreVector = null;
		XSSFCell myCell = null;
		BudgetedTemporary budgetedTemporary = null;
		String assortmentCode = "";
		String billDate = "";
		String qtyForecast = "";
		String dateFormate = "yyyy-MM-dd";
		LOGGER.info("Valid Data Function Called");
		readDataFromExcel = validDataFromExcel(file);
		LOGGER.info("Data validation returned lists");
		int rowCount = readDataFromExcel.size();
		List<BudgetedTemporary> listOfExcelFileData = new ArrayList();
		
		LOGGER.info(String.format("forward Excel data into iteration to verify each data with excel size : %s---",
				rowCount));
		for (int i = 0; i < rowCount; i++) {
			
			
			if(i==160000 || i==100000 || i== 125000 || i==150000 || i==170000 || i==200000) {
				LOGGER.debug(String.format("Budgeted upload Iteration --> : %s", i));
			}
			cellStoreVector = (Vector) readDataFromExcel.elementAt(i);
			budgetedTemporary = new BudgetedTemporary();
			
			for (int j = 0; j < cellStoreVector.size(); j++) {
				myCell = (XSSFCell) cellStoreVector.elementAt(j);
				if (j == 0) {
					assortmentCode = myCell.toString();
					if (assortmentCode.isEmpty()) {
						LOGGER.debug(String.format("Error Occoured From assortmentCode : %s", i));
						insertDPUploadSummaryData("file", type, userName, rowCount);
						throw new SupplyMintException(
								appStatus.getMessage(AppModuleErrors.DEMAND_BUDGETED_SALES_EXCEL_READER_ERROR) + i);
					} else
						budgetedTemporary.setAssortmentCode(assortmentCode);
				} else if (j == 1) {
					billDate = myCell.toString();

					if (!billDate.isEmpty() && CodeUtils.isValidDate(billDate, dateFormate)) {
						/*
						 * LOGGER.debug(String.format("Error Occoured From billDate : %s", i));
						 * insertDPUploadSummaryData("file", type, userName, rowCount); throw new
						 * SupplyMintException( appStatus.getMessage(AppModuleErrors.
						 * DEMAND_BUDGETED_SALES_EXCEL_READER_ERROR) + i);
						 */
						
						budgetedTemporary.setBillDate(billDate);
					} else {
						LOGGER.debug(String.format("Error Occoured From billDate : %s", i));
						insertDPUploadSummaryData("file", type, userName, rowCount);
						throw new SupplyMintException(
								appStatus.getMessage(AppModuleErrors.DEMAND_BUDGETED_SALES_EXCEL_READER_ERROR) + i);
						/*
						 * Date date = new Date(billDate); billDate = dateFormat.format(date);
						 * budgetedTemporary.setBillDate(billDate);
						 */
					}
					} else if (j == 2) {
					try {
						qtyForecast = myCell.getRawValue();
						if (qtyForecast.isEmpty()) {
							budgetedTemporary.setQtyForeCast("0");
						} else {
							//budgetedTemporary.setQtyForeCast(Integer.toString((int)Float.parseFloat(qtyForecast)));
							budgetedTemporary.setQtyForeCast(qtyForecast);
						}
					} catch (Exception e) {
						LOGGER.debug(String.format("Error Occoured From qty_forecast : %s", i));
						insertDPUploadSummaryData("file", type, userName, rowCount);
						throw new SupplyMintException(
								appStatus.getMessage(AppModuleErrors.DEMAND_BUDGETED_SALES_EXCEL_READER_ERROR) + i);
					}
				}
			}
			budgetedTemporary.setUserName(userName);
			listOfExcelFileData.add(budgetedTemporary);
		}
		LOGGER.info("Returned excel data into lists");
		LOGGER.info(String.format("Returned no. of excel data into lists : %s---", listOfExcelFileData.size()));
		return listOfExcelFileData;
	}

	@Override
	public String getRunIdPattern() {
		return demandPlanningMapper.getRunIdPattern();
	}

	@Override
	public DemandPlanning getByRunId(String runId) {
		return demandPlanningMapper.getByRunId(runId);
	}

	@Override
	public int updateSummary(String summary, String runId) {
		return demandPlanningMapper.updateSummary(summary, runId);
	}

	@Override
	public int updatePreviousSummary(String runId) {
		return demandPlanningMapper.updatePreviousSummary(runId);
	}

	@Override
	public List<DemandPlanning> downloadForecastDetails() {
		return demandPlanningMapper.downloadForecastDetails();
	}

	@SuppressWarnings("resource")
	@Override
	public void generateForecastTOExcel(List<DemandPlanning> list, String filePath) {
		Workbook workbook = new XSSFWorkbook();
		Sheet sheet = workbook.createSheet("DEMANDPLANNING_REPORT");

		int rowIndex = 1;
		Row row1 = sheet.createRow(0);
		row1.createCell(0).setCellValue("RUNID");
		row1.createCell(1).setCellValue("FREQUENCY");
		row1.createCell(2).setCellValue("PREDICT PERIOD");
		row1.createCell(3).setCellValue("STARTED ON");
		row1.createCell(5).setCellValue("TOTAL STORE");
		row1.createCell(4).setCellValue("TOTAL ASSORTMENT");
		row1.createCell(6).setCellValue("STATUS");

		LOGGER.info("Write Forecast History to Excel");
		for (DemandPlanning demandPlanning : list) {

			Row row = sheet.createRow(rowIndex++);
			int cellIndex = 0;

			row.createCell(cellIndex++).setCellValue(demandPlanning.getRunId());
			row.createCell(cellIndex++).setCellValue(demandPlanning.getFrequency());
			row.createCell(cellIndex++).setCellValue(demandPlanning.getPredictPeriod());
			row.createCell(cellIndex++).setCellValue(demandPlanning.getStartedOn());
			row.createCell(cellIndex++).setCellValue(demandPlanning.getTotalStore());
			row.createCell(cellIndex++).setCellValue(demandPlanning.getTotalAssortment());
			row.createCell(cellIndex++).setCellValue(demandPlanning.getStatus());
		}
		
		try {
			Path path = Paths.get(filePath);
			try (OutputStream out = Files.newOutputStream(path, CREATE_NEW)) {
				workbook.write(out);
				out.close();
			} catch (FileAlreadyExistsException incrCounterAndRetry) {
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		
		
//		try {
//			FileOutputStream fos = new FileOutputStream(filePath);
//			workbook.write(fos);
//			fos.close();
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
	}

	@SuppressWarnings({ "unused", "rawtypes" })
	@Override
	public Set<DemandGraph> generateMonthlyForecast(DemandPlanningViewLog demandPlanningViewLog,
			HttpServletRequest request) {

		String assortment = demandPlanningViewLog.getAssortment();
		String startDate = demandPlanningViewLog.getStartDate();
		String endDate = demandPlanningViewLog.getEndDate();
		String userName = demandPlanningViewLog.getUserName();
		String uuid = demandPlanningViewLog.getId();

		List<DemandForecast> data = null;
		List<DemandForecast> data1 = null;
		List<DemandForecast> data2 = null;
		ArrayNode finalMapNode = null;
		String url = null;
		Map<String, Map<String, String>> generateForecast = new LinkedHashMap<String, Map<String, String>>();
		Map<String, String> actualAssortMap = new HashMap<String, String>();
		Map<String, String> predictedAssortMap = new HashMap<String, String>();
		Map<String, String> budgetedAssortMap = new HashMap<String, String>();
		Set<String> billDate = new LinkedHashSet<String>();
		Set<DemandGraph> finalMap = new LinkedHashSet<DemandGraph>();

		DemandPlanningViewLog planningViewLog = new DemandPlanningViewLog();

		try {
			
			String token = ((HttpServletRequest) request).getHeader("X-Auth-Token");
			JsonArray orgDetails=CodeUtils.decode(token).get("ORG_DETAILS").getAsJsonArray();
			
			JsonObject jNode=orgDetails.get(0).getAsJsonObject();
			String orgId = jNode.get("orgId").getAsString();
			
			
			S3BucketInfo s3BucketInfo = s3BucketInfoService.getBucketByType(BucketParameter.GENERATE_EXCELCSV,orgId);

			tenantHashkey = ThreadLocalStorage.getTenantHashKey();
			upnDc = tenantFilter.getTenantConfigProperty().get(tenantHashkey);
			LOGGER.debug(String.format("Configuring script with datasource configuration for user : %s", upnDc));
			LOGGER.info("Calling executor service");

			String bucketKey = "/FORECAST_REPORT" + CustomParameter.DELIMETER + uuid;
			String bucketName = CodeUtils.decode(token).get(FileUtils.BucketName).getAsString();
			
			
			String sDate = CodeUtils.convertForecastDateFormat(startDate);
			String eDate = CodeUtils.convertForecastDateFormat(endDate);
			LOGGER.debug("retrive actual monthly data...");
			data = demandPlanningMapper.actualMonthlyAssortment(assortment, sDate, eDate);
			for (int i = 0; i < data.size(); i++) {
				if (CodeUtils.convertDemandForecastMonthlyDateFormat(data.get(i).getBillDate()) != null)
					actualAssortMap.put(CodeUtils.convertDemandForecastMonthlyDateFormat(data.get(i).getBillDate()),
							data.get(i).getQty());
			}
			LOGGER.debug("obtaining actual monthly data...");
			generateForecast.put(DemandPlanningGraph.ACTUAL.toString(), actualAssortMap);
			LOGGER.debug("retrive forecast monthly data...");
			data1 = demandPlanningMapper.predictedMonthlyAssortment(assortment, sDate, eDate);
			for (int i = 0; i < data1.size(); i++) {
				if (CodeUtils.convertDemandForecastMonthlyDateFormat(data1.get(i).getBillDate()) != null)
					predictedAssortMap.put(CodeUtils.convertDemandForecastMonthlyDateFormat(data1.get(i).getBillDate()),
							data1.get(i).getQtyForecast());
			}
			LOGGER.debug("obtaining forecast monthly data...");
			generateForecast.put(DemandPlanningGraph.PREDICTED.toString(), predictedAssortMap);
			LOGGER.debug("retrive budgeted monthly data...");
			data2 = demandPlanningMapper.budgetedMonthlyAssortment(assortment, sDate, eDate);
			for (int i = 0; i < data2.size(); i++) {
				if (CodeUtils.convertDemandForecastMonthlyDateFormat(data2.get(i).getBillDate()) != null)
					budgetedAssortMap.put(CodeUtils.convertDemandForecastMonthlyDateFormat(data2.get(i).getBillDate()),
							data2.get(i).getQtyForecast());
			}
			LOGGER.debug("obtaining budgeted monthly data...");
			generateForecast.put(DemandPlanningGraph.BUDGETED.toString(), budgetedAssortMap);

			Set keys = generateForecast.keySet();
			Iterator itr = keys.iterator();
			String key11;
			Map<String, String> value11;
			String keyvalue11;
			String valueOf11;
			LOGGER.debug("start manipulating actual,pridicted,budgeted data for MONTHLY graph...");

			while (itr.hasNext()) {

				key11 = (String) itr.next();
				value11 = generateForecast.get(key11);
				Set valuekeys11 = value11.keySet();
				Iterator valueitr = valuekeys11.iterator();
				while (valueitr.hasNext()) {

					if (key11.equalsIgnoreCase(DemandPlanningGraph.ACTUAL.toString())
							&& !key11.equalsIgnoreCase(DemandPlanningGraph.PREDICTED.toString())
							&& !key11.equalsIgnoreCase(DemandPlanningGraph.BUDGETED.toString())) {

						DemandGraph demandGraph = new DemandGraph();
						keyvalue11 = (String) valueitr.next();
						valueOf11 = value11.get(keyvalue11);
						if (actualAssortMap.get(keyvalue11) != null) {
							demandGraph.setActual(Integer.parseInt(actualAssortMap.get(keyvalue11)));
							demandGraph.setBillDate(keyvalue11);
						} else {
							demandGraph.setActual(0);
							demandGraph.setBillDate(keyvalue11);
						}

						if (predictedAssortMap.get(keyvalue11) != null) {
							demandGraph.setPredicted(Integer.parseInt(predictedAssortMap.get(keyvalue11)));
							demandGraph.setBillDate(keyvalue11);
						} else {
							demandGraph.setPredicted(0);
							demandGraph.setBillDate(keyvalue11);
						}

						if (budgetedAssortMap.get(keyvalue11) != null) {
							demandGraph.setBudgeted(Integer.parseInt(budgetedAssortMap.get(keyvalue11)));
							demandGraph.setBillDate(keyvalue11);
						} else {
							demandGraph.setBudgeted(0);
							demandGraph.setBillDate(keyvalue11);
						}

						if (demandGraph != null) {
							if (demandGraph.getBillDate() != null && billDate.add(demandGraph.getBillDate())) {
								if (!CodeUtils.checkStringType(demandGraph.getActual() + "").equals("String")
										&& !CodeUtils.checkStringType(demandGraph.getPredicted() + "")
												.equals("String")) {
									int deviation = demandGraph.getActual() - demandGraph.getPredicted();
									demandGraph.setDeviation(deviation);
								} else {
									demandGraph.setDeviation(0);
								}
								finalMap.add(demandGraph);
							}
						}
					} else if (key11.equalsIgnoreCase(DemandPlanningGraph.PREDICTED.toString())
							&& !key11.equalsIgnoreCase(DemandPlanningGraph.ACTUAL.toString())
							&& !key11.equalsIgnoreCase(DemandPlanningGraph.BUDGETED.toString())) {
						DemandGraph demandGraph = new DemandGraph();
						keyvalue11 = (String) valueitr.next();
						valueOf11 = value11.get(keyvalue11);
						if (actualAssortMap.get(keyvalue11) != null && !finalMap.toString().contains(keyvalue11)) {
							demandGraph.setActual(Integer.parseInt(actualAssortMap.get(keyvalue11)));
							demandGraph.setBillDate(keyvalue11);
						} else {
							demandGraph.setActual(0);
							demandGraph.setBillDate(keyvalue11);
						}

						if (predictedAssortMap.get(keyvalue11) != null && !finalMap.toString().contains(keyvalue11)) {
							demandGraph.setPredicted(Integer.parseInt(predictedAssortMap.get(keyvalue11)));
							demandGraph.setBillDate(keyvalue11);
						} else {
							demandGraph.setActual(0);
							demandGraph.setPredicted(0);
							demandGraph.setBillDate(keyvalue11);
						}

						if (budgetedAssortMap.get(keyvalue11) != null && !finalMap.toString().contains(keyvalue11)) {
							demandGraph.setBudgeted(Integer.parseInt(budgetedAssortMap.get(keyvalue11)));
							demandGraph.setBillDate(keyvalue11);
						} else {
							demandGraph.setBudgeted(0);
							demandGraph.setBillDate(keyvalue11);
						}

						if (demandGraph != null) {
							if (demandGraph.getBillDate() != null && billDate.add(demandGraph.getBillDate())) {
								if (!CodeUtils.checkStringType(demandGraph.getActual() + "").equals("String")
										&& !CodeUtils.checkStringType(demandGraph.getPredicted() + "")
												.equals("String")) {
									int deviation = demandGraph.getActual() - demandGraph.getPredicted();
									demandGraph.setDeviation(deviation);
								} else {
									demandGraph.setDeviation(0);
								}
								finalMap.add(demandGraph);
							}
						}
					} else if (key11.equalsIgnoreCase(DemandPlanningGraph.BUDGETED.toString())
							&& !key11.equalsIgnoreCase(DemandPlanningGraph.ACTUAL.toString())
							&& !key11.equalsIgnoreCase(DemandPlanningGraph.PREDICTED.toString())) {
						DemandGraph demandGraph = new DemandGraph();
						keyvalue11 = (String) valueitr.next();
						valueOf11 = value11.get(keyvalue11);
						if (actualAssortMap.get(keyvalue11) != null && !finalMap.toString().contains(keyvalue11)) {
							demandGraph.setActual(Integer.parseInt(actualAssortMap.get(keyvalue11)));
							demandGraph.setBillDate(keyvalue11);
						} else {
							demandGraph.setActual(0);
							demandGraph.setBillDate(keyvalue11);
						}

						if (predictedAssortMap.get(keyvalue11) != null && !finalMap.toString().contains(keyvalue11)) {
							demandGraph.setPredicted(Integer.parseInt(predictedAssortMap.get(keyvalue11)));
							demandGraph.setBillDate(keyvalue11);
						} else {
							demandGraph.setActual(0);
							demandGraph.setPredicted(0);
							demandGraph.setBillDate(keyvalue11);
						}

						if (budgetedAssortMap.get(keyvalue11) != null && !finalMap.toString().contains(keyvalue11)) {
							demandGraph.setBudgeted(Integer.parseInt(budgetedAssortMap.get(keyvalue11)));
							demandGraph.setBillDate(keyvalue11);
							demandGraph.setPredicted(0);
							demandGraph.setActual(0);
						} else {
							demandGraph.setBudgeted(0);
							demandGraph.setPredicted(0);
							demandGraph.setBillDate(keyvalue11);
						}

						if (demandGraph != null) {
							if (demandGraph.getBillDate() != null && billDate.add(demandGraph.getBillDate())) {
								if (!CodeUtils.checkStringType(demandGraph.getActual() + "").equals("String")
										&& !CodeUtils.checkStringType(demandGraph.getPredicted() + "")
												.equals("String")) {
									int deviation = demandGraph.getActual() - demandGraph.getPredicted();
									demandGraph.setDeviation(deviation);
								} else {
									demandGraph.setDeviation(0);
								}
								finalMap.add(demandGraph);
							}
						}
					}
				}
			}

			List getAllMonths = CodeUtils.getOTBAllMonths(startDate, endDate);
			Set distinctBillDate = new HashSet<>();
			List<String> totalBillDateMonth = new LinkedList<String>();

			finalMap.stream().forEach(e -> {
				distinctBillDate.add(e.getBillDate());
			});
			totalBillDateMonth.addAll(distinctBillDate);
			List getAllMisMatchBillDate = CodeUtils.returnMisMatchList(getAllMonths, totalBillDateMonth);
			for (int i = 0; i < getAllMisMatchBillDate.size(); i++) {
				DemandGraph demandGraph = new DemandGraph();
				demandGraph.setActual(0);
				demandGraph.setBudgeted(0);
				demandGraph.setPredicted(0);
				demandGraph.setDeviation(0);
				demandGraph.setBillDate(getAllMisMatchBillDate.get(i).toString());
				finalMap.add(demandGraph);
			}
			distinctBillDate.clear();
			totalBillDateMonth.clear();

			if (CodeUtils.isEmpty(finalMap)) {
				LocalDate date1 = new LocalDate(startDate);
				LocalDate date2 = new LocalDate(endDate);
				while (date1.isBefore(date2)) {
					DemandGraph demandGraph = new DemandGraph();
					demandGraph.setActual(0);
					demandGraph.setBudgeted(0);
					demandGraph.setPredicted(0);
					demandGraph.setDeviation(0);
					demandGraph.setBillDate(date1.toString("MMM yyyy"));
					finalMap.add(demandGraph);
					date1 = date1.plus(Period.months(1));
				}
			}

			LOGGER.debug("completed manipulating actual,pridicted,budgeted data for MONTHLY graph..");
			ObjectMapper mapper = new ObjectMapper();
			finalMapNode = mapper.convertValue(finalMap, ArrayNode.class);
			LOGGER.info("finalMapNode {} ", finalMapNode);
			String convertfinalMapNode = finalMapNode + "";
			if (CodeUtils.isEmpty(generateForecast)) {
				return null;
			}
			planningViewLog = retriveLogData(uuid, assortment, startDate, endDate, Frequency.MONTHLY.toString(),
					convertfinalMapNode, userName, bucketName, bucketKey, url);
			planningViewLog.setAdditional("current");
			int createDemandPlanningLog = demandPlanningMapper.createDemandPlanningLog(planningViewLog);
			int updateSummaryStatus = demandPlanningMapper.updateSummaryStatus(uuid, Frequency.MONTHLY.toString());
			LOGGER.debug("start running myreport Script for download report");
			ExecutorService executorService = Executors.newSingleThreadExecutor();
			executorService.execute(new Runnable() {
				@Override
				public void run() {
					try {
						ThreadLocalStorage.setTenantHashKey(tenantHashkey);
						LOGGER.debug(
								"Passing down the configuration property to load the script with following attributes User :%s  Frequency :%s Predict Period :%s Start Pattern :%s",
								upnDc, Frequency.MONTHLY.toString(), startDate, endDate, bucketName, bucketKey);
						successCode = rServeEngineProviderService.downloadReport(upnDc, Frequency.MONTHLY.toString(),
								startDate, endDate, bucketName, bucketKey);
						LOGGER.info("Waiting for forecast result ..");

						if (successCode == 200) {
							String url = s3Wrapper.downloadPreSignedURLOnHistory(bucketName,
									bucketKey.substring(1, bucketKey.length()) + CustomParameter.DELIMETER,
									Integer.parseInt(s3BucketInfo.getTime()));
							LOGGER.debug("myReport download link updated");
							int updateStatus = demandPlanningMapper.updateStatus(ConditionState.SUCCEEDED.toString(),
									Conditions.TRUE.toString(), url, uuid);
							int updateSummaryStatus = demandPlanningMapper.updateSummaryStatus(uuid,
									Frequency.MONTHLY.toString());
						} else if (successCode == 500) {
							int updateStatus = demandPlanningMapper.updateStatus(ConditionState.FAILED.toString(),
									Conditions.FALSE.toString(), ConditionState.FAILED.toString(), uuid);
							int updateSummaryStatus = demandPlanningMapper.updateFailedSummaryStatus(uuid,
									Frequency.MONTHLY.toString());
						}
					} catch (Exception ex) {
						try {
							throw ex;
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			});
		} catch (CannotGetJdbcConnectionException ex) {
			throw ex;
		} catch (DataAccessException ex) {
			throw ex;
		} catch (Exception ex) {
			try {
				throw ex;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return finalMap;
	}

	@SuppressWarnings({ "unused", "rawtypes" })
	@Override
	public Set<DemandGraph> generateWeeklyForecast(DemandPlanningViewLog demandPlanningViewLog,
			HttpServletRequest request) {

		String assortment = demandPlanningViewLog.getAssortment();
		String startDate = demandPlanningViewLog.getStartDate();
		String endDate = demandPlanningViewLog.getEndDate();
		String userName = demandPlanningViewLog.getUserName();
		String uuid = demandPlanningViewLog.getId();

		List<DemandForecast> data = null;
		List<DemandForecast> data1 = null;
		List<DemandForecast> data2 = null;
		ArrayNode finalMapNode = null;
		String url = null;
		Set<String> billDate = new LinkedHashSet<String>();
		Map<String, Map<String, String>> generateForecast = new HashMap<String, Map<String, String>>();
		Map<String, String> actualAssortMap = new HashMap<String, String>();
		Map<String, String> predictedAssortMap = new HashMap<String, String>();
		Map<String, String> budgetedAssortMap = new HashMap<String, String>();
		Set<DemandGraph> finalMap = new LinkedHashSet<DemandGraph>();
		DemandPlanningViewLog planningViewLog = new DemandPlanningViewLog();
		
		
		try {
			
			String token = ((HttpServletRequest) request).getHeader("X-Auth-Token");
			JsonArray orgDetails=CodeUtils.decode(token).get("ORG_DETAILS").getAsJsonArray();
			
			JsonObject jNode=orgDetails.get(0).getAsJsonObject();
			String orgId = jNode.get("orgId").getAsString();
			
			
			S3BucketInfo s3BucketInfo = s3BucketInfoService.getBucketByType(BucketParameter.GENERATE_EXCELCSV,orgId);

			tenantHashkey = ThreadLocalStorage.getTenantHashKey();
			upnDc = tenantFilter.getTenantConfigProperty().get(tenantHashkey);
			LOGGER.debug(String.format("Configuring script with datasource configuration for user : %s", upnDc));
			LOGGER.info("Calling executor service");

			String bucketKey = "/FORECAST_REPORT" + CustomParameter.DELIMETER + uuid;
			String bucketName = CodeUtils.decode(token).get(FileUtils.BucketName).getAsString();

			startDate = CodeUtils.getPreviousMonday(startDate);
			LOGGER.info("retrive actual weekly data...");
			data = demandPlanningMapper.actualWeeklyAssortment(assortment, startDate, endDate);
			for (int i = 0; i < data.size(); i++) {
				if (CodeUtils.convertDemandForecastDateFormat(data.get(i).getBillDate()) != null)
					actualAssortMap.put(CodeUtils.convertDemandForecastDateFormat(data.get(i).getBillDate()),
							data.get(i).getQty());
			}
			LOGGER.info("obtaining actual weekly data...");
			generateForecast.put(DemandPlanningGraph.ACTUAL.toString(), actualAssortMap);
			LOGGER.info("retrive forecast weekly data...");
			data1 = demandPlanningMapper.predictedWeeklyAssortment(assortment, startDate, endDate);
			for (int i = 0; i < data1.size(); i++) {
				if (CodeUtils.convertDemandForecastDateFormat(data1.get(i).getBillDate()) != null)
					predictedAssortMap.put(CodeUtils.convertDemandForecastDateFormat(data1.get(i).getBillDate()),
							data1.get(i).getQtyForecast());
			}
			LOGGER.info("obtaining forecast weekly data...");
			generateForecast.put(DemandPlanningGraph.PREDICTED.toString(), predictedAssortMap);
			LOGGER.info("retrive budgeted weekly data...");
			data2 = demandPlanningMapper.budgetedWeeklyAssortment(assortment, startDate, endDate);
			for (int i = 0; i < data2.size(); i++) {
				if (CodeUtils.convertDemandForecastDateFormat(data2.get(i).getBillDate()) != null)
					budgetedAssortMap.put(CodeUtils.convertDemandForecastDateFormat(data2.get(i).getBillDate()),
							data2.get(i).getQtyForecast());
			}
			LOGGER.info("obtaining budgeted weekly data...");
			generateForecast.put(DemandPlanningGraph.BUDGETED.toString(), budgetedAssortMap);

			Set keys = generateForecast.keySet();
			Iterator itr = keys.iterator();
			String key11;
			Map<String, String> value11;
			String keyvalue11;
			String valueOf11;

			LOGGER.info("start manipulating actual,pridicted,budgeted data for WEEKLY graph...");
			while (itr.hasNext()) {
				key11 = (String) itr.next();
				value11 = generateForecast.get(key11);
				Set valuekeys11 = value11.keySet();
				Iterator valueitr = valuekeys11.iterator();
				while (valueitr.hasNext()) {

					if (key11.equalsIgnoreCase(DemandPlanningGraph.ACTUAL.toString())
							&& !key11.equalsIgnoreCase(DemandPlanningGraph.PREDICTED.toString())
							&& !key11.equalsIgnoreCase(DemandPlanningGraph.BUDGETED.toString())) {

						DemandGraph demandGraph = new DemandGraph();
						keyvalue11 = (String) valueitr.next();
						valueOf11 = value11.get(keyvalue11);
						if (actualAssortMap.get(keyvalue11) != null) {
							demandGraph.setActual(Integer.parseInt(actualAssortMap.get(keyvalue11)));
							demandGraph.setBillDate(keyvalue11);
						} else {
							demandGraph.setActual(0);
							demandGraph.setBillDate(keyvalue11);
						}

						if (predictedAssortMap.get(keyvalue11) != null) {
							demandGraph.setPredicted(Integer.parseInt(predictedAssortMap.get(keyvalue11)));
							demandGraph.setBillDate(keyvalue11);
						} else {
							demandGraph.setPredicted(0);
							demandGraph.setBillDate(keyvalue11);
						}

						if (budgetedAssortMap.get(keyvalue11) != null) {
							demandGraph.setBudgeted(Integer.parseInt(budgetedAssortMap.get(keyvalue11)));
							demandGraph.setBillDate(keyvalue11);
						} else {
							demandGraph.setBudgeted(0);
							demandGraph.setBillDate(keyvalue11);
						}

						if (demandGraph != null) {
							if (demandGraph.getBillDate() != null && billDate.add(demandGraph.getBillDate())) {
								if (!CodeUtils.checkStringType(demandGraph.getActual() + "").equals("String")
										&& !CodeUtils.checkStringType(demandGraph.getPredicted() + "")
												.equals("String")) {
									int deviation = demandGraph.getActual() - demandGraph.getPredicted();
									demandGraph.setDeviation(deviation);
								} else {
									demandGraph.setDeviation(0);
								}
								finalMap.add(demandGraph);
							}
						}
					} else if (key11.equalsIgnoreCase(DemandPlanningGraph.PREDICTED.toString())
							&& !key11.equalsIgnoreCase(DemandPlanningGraph.ACTUAL.toString())
							&& !key11.equalsIgnoreCase(DemandPlanningGraph.BUDGETED.toString())) {
						DemandGraph demandGraph = new DemandGraph();
						keyvalue11 = (String) valueitr.next();
						valueOf11 = value11.get(keyvalue11);
						if (actualAssortMap.get(keyvalue11) != null && !finalMap.toString().contains(keyvalue11)) {
							demandGraph.setActual(Integer.parseInt(actualAssortMap.get(keyvalue11)));
							demandGraph.setBillDate(keyvalue11);
						} else {
							demandGraph.setActual(0);
							demandGraph.setBillDate(keyvalue11);
						}

						if (predictedAssortMap.get(keyvalue11) != null && !finalMap.toString().contains(keyvalue11)) {
							demandGraph.setPredicted(Integer.parseInt(predictedAssortMap.get(keyvalue11)));
							demandGraph.setBillDate(keyvalue11);
						} else {
							demandGraph.setActual(0);
							demandGraph.setPredicted(0);
							demandGraph.setBillDate(keyvalue11);
						}

						if (budgetedAssortMap.get(keyvalue11) != null && !finalMap.toString().contains(keyvalue11)) {
							demandGraph.setBudgeted(Integer.parseInt(budgetedAssortMap.get(keyvalue11)));
							demandGraph.setBillDate(keyvalue11);
						} else {
							demandGraph.setBudgeted(0);
							demandGraph.setBillDate(keyvalue11);
						}

						if (demandGraph != null) {
							if (demandGraph.getBillDate() != null && billDate.add(demandGraph.getBillDate())) {
								if (!CodeUtils.checkStringType(demandGraph.getActual() + "").equals("String")
										&& !CodeUtils.checkStringType(demandGraph.getPredicted() + "")
												.equals("String")) {
									int deviation = demandGraph.getActual() - demandGraph.getPredicted();
									demandGraph.setDeviation(deviation);
								} else {
									demandGraph.setDeviation(0);
								}
								finalMap.add(demandGraph);
							}
						}
					} else if (key11.equalsIgnoreCase(DemandPlanningGraph.BUDGETED.toString())
							&& !key11.equalsIgnoreCase(DemandPlanningGraph.ACTUAL.toString())
							&& !key11.equalsIgnoreCase(DemandPlanningGraph.PREDICTED.toString())) {
						DemandGraph demandGraph = new DemandGraph();
						keyvalue11 = (String) valueitr.next();
						valueOf11 = value11.get(keyvalue11);
						if (actualAssortMap.get(keyvalue11) != null && !finalMap.toString().contains(keyvalue11)) {
							demandGraph.setActual(Integer.parseInt(actualAssortMap.get(keyvalue11)));
							demandGraph.setBillDate(keyvalue11);
						} else {
							demandGraph.setActual(0);
							demandGraph.setBillDate(keyvalue11);
						}

						if (predictedAssortMap.get(keyvalue11) != null && !finalMap.toString().contains(keyvalue11)) {
							demandGraph.setPredicted(Integer.parseInt(predictedAssortMap.get(keyvalue11)));
							demandGraph.setBillDate(keyvalue11);
						} else {
							demandGraph.setActual(0);
							demandGraph.setPredicted(0);
							demandGraph.setBillDate(keyvalue11);
						}

						if (budgetedAssortMap.get(keyvalue11) != null && !finalMap.toString().contains(keyvalue11)) {
							demandGraph.setBudgeted(Integer.parseInt(budgetedAssortMap.get(keyvalue11)));
							demandGraph.setBillDate(keyvalue11);
							demandGraph.setPredicted(0);
							demandGraph.setActual(0);
						} else {
							demandGraph.setBudgeted(0);
							demandGraph.setPredicted(0);
							demandGraph.setBillDate(keyvalue11);
						}

						if (demandGraph != null) {
							if (demandGraph.getBillDate() != null && billDate.add(demandGraph.getBillDate())) {
								if (!CodeUtils.checkStringType(demandGraph.getActual() + "").equals("String")
										&& !CodeUtils.checkStringType(demandGraph.getPredicted() + "")
												.equals("String")) {
									int deviation = demandGraph.getActual() - demandGraph.getPredicted();
									demandGraph.setDeviation(deviation);
								} else {
									demandGraph.setDeviation(0);
								}
								finalMap.add(demandGraph);
							}
						}
					}
				}
			}

			LOGGER.info("completed manipulating actual,pridicted,budgeted data for WEEKLY graph...");
			List getAllWeek = CodeUtils.getAllWeekFromMonday(startDate, endDate);
			Set distinctBillDate = new HashSet<>();
			List<String> totalBillDateMonth = new LinkedList<String>();

			finalMap.stream().forEach(e -> {
				distinctBillDate.add(e.getBillDate());
			});
			totalBillDateMonth.addAll(distinctBillDate);
			List getAllMisMatchBillDate = CodeUtils.returnMisMatchList(getAllWeek, totalBillDateMonth);
			for (int i = 0; i < getAllMisMatchBillDate.size(); i++) {
				DemandGraph demandGraph = new DemandGraph();
				demandGraph.setActual(0);
				demandGraph.setBudgeted(0);
				demandGraph.setPredicted(0);
				demandGraph.setDeviation(0);
				demandGraph.setBillDate(getAllMisMatchBillDate.get(i).toString());
				finalMap.add(demandGraph);
			}

			distinctBillDate.clear();
			totalBillDateMonth.clear();

			if (CodeUtils.isEmpty(finalMap)) {
				LocalDate date1 = new LocalDate(startDate);
				LocalDate date2 = new LocalDate(endDate);
				while (date1.isBefore(date2)) {
					DemandGraph demandGraph = new DemandGraph();
					demandGraph.setActual(0);
					demandGraph.setBudgeted(0);
					demandGraph.setPredicted(0);
					demandGraph.setDeviation(0);
					demandGraph.setBillDate(date1.toString("dd MMM yyyy"));
					finalMap.add(demandGraph);
					date1 = date1.plus(Period.weeks(1));
				}
			}
			ObjectMapper mapper = new ObjectMapper();
			finalMapNode = mapper.convertValue(finalMap, ArrayNode.class);
			LOGGER.info("finalMapNode {} ", finalMapNode);
			String convertfinalMapNode = finalMapNode + "";
			if (CodeUtils.isEmpty(generateForecast)) {
				return null;
			}
			planningViewLog = retriveLogData(uuid, assortment, startDate, endDate, Frequency.WEEKLY.toString(),
					convertfinalMapNode, userName, bucketName, bucketKey, url);
			planningViewLog.setAdditional("current");
			int createDemandPlanningLog = demandPlanningMapper.createDemandPlanningLog(planningViewLog);
			int updateSummaryStatus = demandPlanningMapper.updateSummaryStatus(uuid, Frequency.WEEKLY.toString());

			String sDate = startDate;
			ExecutorService executorService = Executors.newSingleThreadExecutor();
			executorService.execute(new Runnable() {
				@Override
				public void run() {
					try {
						ThreadLocalStorage.setTenantHashKey(tenantHashkey);
						LOGGER.debug(
								"Passing down the configuration property to load the script with following attributes User :%s  Frequency :%s Predict Period :%s Start Pattern :%s",
								upnDc, Frequency.WEEKLY.toString(), sDate, endDate, bucketName, bucketKey);
						successCode = rServeEngineProviderService.downloadReport(upnDc, Frequency.WEEKLY.toString(),
								sDate, endDate, bucketName, bucketKey);
						LOGGER.info("Waiting for forecast result ..");
						if (successCode == 200) {
							String url = s3Wrapper.downloadPreSignedURLOnHistory(bucketName,
									bucketKey.substring(1, bucketKey.length()) + CustomParameter.DELIMETER,
									Integer.parseInt(s3BucketInfo.getTime()));

							int updateSummaryStatus = demandPlanningMapper.updateSummaryStatus(uuid,
									Frequency.WEEKLY.toString());
							int updateStatus = demandPlanningMapper.updateStatus(ConditionState.SUCCEEDED.toString(),
									Conditions.TRUE.toString(), url, uuid);
							// int createDemandPlanningLog =
							// demandPlanningMapper.createDemandPlanningLog(planningViewLog);
						} else if (successCode == 500) {
							DemandPlanningViewLog planningViewLog = retriveLogData(uuid, assortment, sDate, endDate,
									Frequency.WEEKLY.toString(), finalMap.toString(), userName, bucketName, bucketKey,
									url);
							int createDemandPlanningLog = demandPlanningMapper.createDemandPlanningLog(planningViewLog);
							int updateStatus = demandPlanningMapper.updateStatus(ConditionState.FAILED.toString(),
									Conditions.FALSE.toString(), url, uuid);
							int updateSummaryStatus = demandPlanningMapper.updateFailedSummaryStatus(uuid,
									Frequency.WEEKLY.toString());
						}
					} catch (Exception ex) {
						try {
							throw ex;
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			});
		} catch (CannotGetJdbcConnectionException ex) {
			throw ex;
		} catch (DataAccessException ex) {
			throw ex;
		} catch (Exception ex) {
			try {
				throw ex;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return finalMap;
	}

	@Override
	public int truncateTempBudgetedData(String userName) {
		return demandPlanningMapper.truncateTempBudgetedData(userName);
	}

	@Override
	public int updateDPUploadSummary(String status, int valid, int invalid, String url, String id) {
		return demandPlanningMapper.updateDPUploadSummary(status, valid, invalid, url, id);

	}

	@Override
	public int updateDownloadUrl(String savedPath, String downloadUrl, String runId) {
		return demandPlanningMapper.updateDownloadUrl(savedPath, downloadUrl, runId);
	}

	@Override
	public List<FinalAssortment> getFinalAssortmentData() {
		return demandPlanningMapper.getFinalAssortmentData();

	}

	@Override
	public List<BudgetedDemandPlanning> getBudgetedMonthlyData(String from, String to) {
		return demandPlanningMapper.getBudgetedMonthlyData(from, to);
	}

	@Override
	public List<BudgetedDemandPlanning> getBudgetedWeeklyData(String from, String to) {

		return demandPlanningMapper.getBudgetedWeeklyData(from, to);
	}

	@Override
	public void generateBudgetedDemandListTOExcel(List<BudgetedDemandPlanning> list, String filePath) {

		Workbook workbook = new XSSFWorkbook();
		Sheet sheet = workbook.createSheet("BudgetedDemand");
		CellStyle style = workbook.createCellStyle();
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setFillForegroundColor(IndexedColors.YELLOW.getIndex());

		CellStyle style1 = workbook.createCellStyle();
		style1.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style1.setFillForegroundColor(IndexedColors.LIGHT_ORANGE.getIndex());

		int rowIndex = 1;
		Row row1 = sheet.createRow(0);

		Cell cell1 = row1.createCell(0);
		cell1.setCellValue("Assortment Code");
		cell1.setCellStyle(style1);

		Cell cell2 = row1.createCell(1);
		cell2.setCellValue("Bill Date");
		cell2.setCellStyle(style1);

		Cell cell3 = row1.createCell(2);
		cell3.setCellValue("Budgeted QTY");
		cell3.setCellStyle(style1);

		Cell cell4 = row1.createCell(3);
		cell4.setCellValue("Status");
		cell4.setCellStyle(style1);
		for (BudgetedDemandPlanning budgetedDemandPlanning : list) {
			Row row = sheet.createRow(rowIndex++);
			int cellIndex = 0;
			row.createCell(cellIndex++).setCellValue(budgetedDemandPlanning.getAssortmentCode());
			row.createCell(cellIndex++).setCellValue(budgetedDemandPlanning.getBillDate());
			row.createCell(cellIndex++).setCellValue(budgetedDemandPlanning.getQtyForeCast());
			Cell cell = row.createCell(cellIndex);
			cell.setCellValue(budgetedDemandPlanning.getFlag());
			cell.setCellStyle(style);
		}
		
		try {
			Path path = Paths.get(filePath);
			try (OutputStream out = Files.newOutputStream(path, CREATE_NEW)) {
				workbook.write(out);
				out.close();
			} catch (FileAlreadyExistsException incrCounterAndRetry) {
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

//		try {
//			FileOutputStream fos = new FileOutputStream(filePath);
//			workbook.write(fos);
//			fos.close();
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
	}

	@SuppressWarnings({ "finally" })
	@Override
	public String getBudgetedDownloadURL(String frequency, String fromDate, String toDate,HttpServletRequest request) {

		String savedPath = null;
		List<BudgetedDemandPlanning> dataList = null;
		File excelFile = null;
		String url = null;
		String moduleName = null;

		try {
			
			String token = ((HttpServletRequest) request).getHeader("X-Auth-Token");
			JsonArray orgDetails=CodeUtils.decode(token).get("ORG_DETAILS").getAsJsonArray();
			
			JsonObject jNode=orgDetails.get(0).getAsJsonObject();
			String orgId = jNode.get("orgId").getAsString();

			if (frequency.equalsIgnoreCase(ChooseType.WEEKLY.toString()))
				moduleName = Module.DemandBudgetedWeekly.toString();
			else if (frequency.equalsIgnoreCase(ChooseType.MONTHLY.toString()))
				moduleName = Module.DemandBudgetedMonthly.toString();

			String nameOfFile = moduleName + CustomParameter.SEPARATOR + CodeUtils.______dateFormat.format(new Date())
					+ FileUtils.EXCELExtension;
			S3BucketInfo s3BucketInfo = s3BucketInfoService.getBucketByType(BucketParameter.GENERATE_EXCELCSV,orgId);
			if (!CodeUtils.isEmpty(s3BucketInfo.getS3bucketName())) {

				savedPath = FileUtils.getPlatformBasedParentDir().toString() + File.separator
						+ CustomExtension.EXCEL.toString() + File.separator + moduleName;
				File existFilePath = new File(savedPath);
				if (!existFilePath.exists())
					existFilePath.mkdirs();
				if (frequency.equalsIgnoreCase(ChooseType.WEEKLY.toString()))
					dataList = getBudgetedWeeklyData(fromDate, toDate);
				else if (frequency.equalsIgnoreCase(ChooseType.MONTHLY.toString()))
					dataList = getBudgetedMonthlyData(fromDate, toDate);

				String savePathWithFileName = savedPath + File.separator + nameOfFile;
				generateBudgetedDemandListTOExcel(dataList, savePathWithFileName);

				String randomFolder = UUID.randomUUID().toString();
				String s3BucketFilePath = s3BucketInfo.getBucketPath() + CustomParameter.DELIMETER + randomFolder;
				excelFile = new File(savePathWithFileName);
				nameOfFile = s3BucketFilePath + CustomParameter.DELIMETER + nameOfFile;
				String etag = s3Wrapper.upload(excelFile, nameOfFile, s3BucketInfo.getS3bucketName()).getETag();

				if (!CodeUtils.isEmpty(etag)) {
					s3BucketFilePath = BucketParameter.BUCKETPREFIX + s3BucketInfo.getS3bucketName()
							+ CustomParameter.DELIMETER + s3BucketFilePath;
					s3BucketFilePath = s3BucketFilePath.split("\\//")[1]
							.replaceAll(s3BucketInfo.getS3bucketName() + CustomParameter.DELIMETER, "")
							+ CustomParameter.DELIMETER.trim().toString();
					url = s3Wrapper.downloadPreSignedURLOnHistory(s3BucketInfo.getS3bucketName(), s3BucketFilePath,
							Integer.parseInt(s3BucketInfo.getTime()));
				}
			}
		} catch (CannotGetJdbcConnectionException ex) {

			throw ex;

		} catch (DataAccessException ex) {
			throw ex;

		} catch (Exception e) {
			throw e;
		} finally {
			return url;
		}

	}

	public DemandPlanningViewLog retriveLogData(String uuid, String assortment, String startDate, String endDate,
			String frequency, String generatedOutputJson, String userName, String bucketPath, String bucketKey,
			String downloadUrl) throws UnknownHostException {
		DemandPlanningViewLog planningViewLog = new DemandPlanningViewLog();
		Map<String, String> reportJson = new LinkedHashMap<String, String>();
		planningViewLog.setId(uuid);
		planningViewLog.setAssortment(assortment);
		planningViewLog.setStartDate(CodeUtils.convertDemandForecastDateFormat(startDate));
		planningViewLog.setEndDate(CodeUtils.convertDemandForecastDateFormat(endDate));
		planningViewLog.setFrequency(frequency);
		reportJson.put("assortment", assortment);
		reportJson.put("startDate", CodeUtils.convertDemandForecastDateFormat(startDate));
		reportJson.put("endDate", CodeUtils.convertDemandForecastDateFormat(endDate));
		reportJson.put("frequency", frequency);
		planningViewLog.setReportJson(reportJson.toString());
		planningViewLog.setGeneratedOn(CodeUtils.convertDateOnTimeZonePluseForAWS(new Date(), 5, 30));
		planningViewLog.setGeneratedOutputJson(generatedOutputJson);
		planningViewLog.setGeneratedBy(userName);
		planningViewLog.setLastSummary(Conditions.TRUE.toString());
		planningViewLog.setBucketPath(bucketPath);
		planningViewLog.setBucketKey(bucketKey);
		planningViewLog.setFolderName(uuid);
		planningViewLog.setGeneratedBy(userName);
		planningViewLog.setDownloadUrl(downloadUrl);
		planningViewLog.setIsDownload(Conditions.TRUE.toString());
		planningViewLog.setIsViewedGraph(Conditions.TRUE.toString());
		planningViewLog.setIsViewedTabular(Conditions.TRUE.toString());
		planningViewLog.setIsActiveSession(Conditions.TRUE.toString());
		planningViewLog.setRepetition("1");
		planningViewLog.setCreatedOn(CodeUtils.convertDateOnTimeZonePluseForAWS(new Date(), 5, 30));
		planningViewLog.setActive('1');
		planningViewLog.setStatus(RunState.SUCCEEDED.toString());
		planningViewLog.setIpAddress(InetAddress.getLocalHost().getHostAddress().trim().toString());

		return planningViewLog;
	}

	@Override
	public void generateExcelTemplate(String filePath) {

		Workbook workbook = new XSSFWorkbook();
		Sheet sheet = workbook.createSheet("ExcelTemplate");

		Row row1 = sheet.createRow(0);
		row1.createCell(0).setCellValue("Assortment Code");
		row1.createCell(1).setCellValue("Bill Date");
		row1.createCell(2).setCellValue("Budgeted QTY");
		
		try {
			Path path = Paths.get(filePath);
			try (OutputStream out = Files.newOutputStream(path, CREATE_NEW)) {
				workbook.write(out);
				out.close();
			} catch (FileAlreadyExistsException incrCounterAndRetry) {
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

//		try {
//
//			FileOutputStream fos = new FileOutputStream(filePath);
//
//			workbook.write(fos);
//
//			fos.close();
//
//		} catch (FileNotFoundException e) {
//
//			e.printStackTrace();
//
//		} catch (IOException e) {
//
//			e.printStackTrace();
//
//		}

	}

	@Override
	public DpUploadSummary getLastStatus(String userName) {
		return demandPlanningMapper.getLastStatus(userName);
	}

	@Override
	public DemandPlanningViewLog getLastReportByFrequency(String frequency, String uuid) {
		return demandPlanningMapper.getLastReportByFrequency(frequency, uuid);
	}

	@Override
	public List<DpUploadSummary> getBudgetedHistoryData(Integer offset, Integer pagesize) {
		return demandPlanningMapper.getBudgetedHistoryData(offset, pagesize);
	}

	@Override
	public int countBudgetedHistoryData() {
		return demandPlanningMapper.countBudgetedHistoryData();

	}

	@Override
	public int countFilterBudgetedHistoryData(String uploadedDate, String fileName, String totalRecord, String valid,
			String invalid) {
		return demandPlanningMapper.countFilterBudgetedHistoryData(uploadedDate, fileName, totalRecord, valid, invalid);
	}

	@Override
	public List<DpUploadSummary> filterBudgetedHistoryData(Integer offset, Integer pagesize, String uploadedDate,
			String fileName, String totalRecord, String valid, String invalid) {
		return demandPlanningMapper.filterBudgetedHistoryData(offset, pagesize, uploadedDate, fileName, totalRecord,
				valid, invalid);
	}

	@Override
	public int countSearchBudgetedHistoryData(String search) {
		return demandPlanningMapper.countSearchBudgetedHistoryData(search);
	}

	@Override
	public List<DpUploadSummary> searchBudgetedHistoryData(Integer offset, Integer pagesize, String search) {
		return demandPlanningMapper.searchBudgetedHistoryData(offset, pagesize, search);
	}

	@SuppressWarnings({ "finally" })
	@Override
	public String downloadURLDpUploadSummary(String frequency, String userName,String orgId) {

		String savedPath = null;
		List<BudgetedDemandPlanning> dataList = null;
		File excelFile = null;
		String moduleName = null;
		String s3BucketFilePath = null;

		try {
			
			
			
			moduleName = Module.DpUploadSummary.toString();
			String nameOfFile = moduleName + CustomParameter.SEPARATOR + CodeUtils.______dateFormat.format(new Date())
					+ FileUtils.EXCELExtension;
			S3BucketInfo s3BucketInfo = s3BucketInfoService.getBucketByType(BucketParameter.GENERATE_EXCELCSV,orgId);
			if (!CodeUtils.isEmpty(s3BucketInfo.getS3bucketName())) {

				savedPath = FileUtils.getPlatformBasedParentDir().toString() + File.separator
						+ CustomExtension.EXCEL.toString() + File.separator + moduleName;
				File existFilePath = new File(savedPath);
				if (!existFilePath.exists())
					existFilePath.mkdirs();

				if (frequency.equalsIgnoreCase(ChooseType.WEEKLY.toString()))
					dataList = demandPlanningMapper.getAllWeeklyLastData(userName);

				else if (frequency.equalsIgnoreCase(ChooseType.MONTHLY.toString()))
					dataList = demandPlanningMapper.getAllMonthlyLastData(userName);

				String savePathWithFileName = savedPath + File.separator + nameOfFile;
				generateBudgetedDemandListTOExcel(dataList, savePathWithFileName);

				String randomFolder = UUID.randomUUID().toString();
				s3BucketFilePath = s3BucketInfo.getBucketPath() + CustomParameter.DELIMETER + randomFolder;
				excelFile = new File(savePathWithFileName);
				nameOfFile = s3BucketFilePath + CustomParameter.DELIMETER + nameOfFile;
				String etag = s3Wrapper.upload(excelFile, nameOfFile, s3BucketInfo.getS3bucketName()).getETag();

				if (!CodeUtils.isEmpty(etag)) {
					s3BucketFilePath = BucketParameter.BUCKETPREFIX + s3BucketInfo.getS3bucketName()
							+ CustomParameter.DELIMETER + s3BucketFilePath;
				}
			}
		} catch (CannotGetJdbcConnectionException ex) {

			throw ex;

		} catch (DataAccessException ex) {
			throw ex;

		} catch (Exception e) {
			throw e;
		} finally {
			return s3BucketFilePath;
		}
	}

	@Override
	public int updateLastDemandBudgetedWeeklyData(String userName) {
		return demandPlanningMapper.updateLastDemandBudgetedWeeklyData(userName);
	}

	@Override
	public int updateLastDemandBudgeteMonthlyData(String userName) {

		return demandPlanningMapper.updateLastDemandBudgeteMonthlyData(userName);
	}

	@Override
	public List<BudgetedDemandPlanning> getAllInvalidWeeklyData() {
		return demandPlanningMapper.getAllInvalidWeeklyData();

	}

	@Override
	public List<BudgetedDemandPlanning> getAllInvalidMonthlyData() {
		return demandPlanningMapper.getAllInvalidMonthlyData();
	}

//	@Override
//	public List<BudgetedDemandPlanning> getAllWeeklyLastData(String userName) {
//		return demandPlanningMapper.getAllWeeklyLastData(userName);
//	}

//	@Override
//	public List<BudgetedDemandPlanning> getAllMonthlyLastData(String userName) {
//		return demandPlanningMapper.getAllMonthlyLastData(userName);
//	}

	@Override
	public int insertFromExcelFileInList(List<BudgetedTemporary> data) {
		return demandPlanningMapper.insertFromExcelFileInList(data);
	}

	@Override
	public int updateBudgetedSummary(String userName, String id) {
		return demandPlanningMapper.updateBudgetedSummary(userName, id);
	}

	@Override
	public DemandPlanningViewLog getLastReport(String frequency, String userName) {
		return demandPlanningMapper.getLastReport(frequency, userName);
	}

	@Override
	public int getAllHistotyCount(String frequency) {
		return demandPlanningMapper.getAllHistotyCount(frequency);
	}

	@Override
	public List<DemandPlanningViewLog> getAllHistory(Integer offset, Integer pageSize, String frequency) {
		return demandPlanningMapper.getAllHistory(offset, pageSize, frequency);
	}

	@Override
	public int filterHistoryRecord(String assortment, String startDate, String endDate, String createdOn,
			String frequency) {
		return demandPlanningMapper.filterHistoryRecord(assortment, startDate, endDate, createdOn, frequency);
	}

	@Override
	public List<DemandPlanningViewLog> filterHistory(Integer offset, Integer pageSize, String assortment,
			String startDate, String endDate, String createdOn, String frequency) {
		return demandPlanningMapper.filterHistory(offset, pageSize, assortment, startDate, endDate, createdOn,
				frequency);
	}

	@Override
	public int searchHistoryRecord(String search, String frequency) {
		return demandPlanningMapper.searchHistoryRecord(search, frequency);
	}

	@Override
	public List<DemandPlanningViewLog> searchHistory(Integer offset, Integer pageSize, String search,
			String frequency) {
		return demandPlanningMapper.searchHistory(offset, pageSize, search, frequency);
	}
	
	

	@Override
	public int updatedownloadUrlLogById(String downloadUrl, String uuid) {
		return demandPlanningMapper.updatedownloadUrlLogById(downloadUrl, uuid);
	}

	public List<BudgetedDemandPlanning> deleteMatchedAssortmentCodeTempDataForWeeklyBudgeted(String userName) {
		return demandPlanningMapper.deleteMatchedAssortmentCodeTempDataForWeeklyBudgeted(userName);

	}

	@Override
	public List<BudgetedDemandPlanning> deleteMatchedAssortmentCodeTempDataForMonthlyBudgeted(String userName) {
		return demandPlanningMapper.deleteMatchedAssortmentCodeTempDataForMonthlyBudgeted(userName);
	}

	@Override
	public DemandPlanning getLastSummaryData() {
		return demandPlanningMapper.getLastSummaryData();
	}

	@Override
	public String getRunningId() {
		return demandPlanningMapper.getRunningId();
	}

	@Override
	public List<String> fiveSucceededTime() {
		return demandPlanningMapper.fiveSucceededTime();
	}

	@Override
	public int insertMatchBudgetedWeeklyData(String userName) {
		return demandPlanningMapper.insertMatchBudgetedWeeklyData(userName);
	}

	@Override
	public int insertNotMatchBudgetedWeeklyData(String userName) {
		return demandPlanningMapper.insertNotMatchBudgetedWeeklyData(userName);
	}

	@Override
	public int insertMatchBudgetedMonthlyData(String userName) {
		return demandPlanningMapper.insertMatchBudgetedMonthlyData(userName);
	}

	@Override
	public int insertNotMatchBudgetedMonthlyData(String userName) {
		return demandPlanningMapper.insertNotMatchBudgetedMonthlyData(userName);
	}

	@Override
	public DpUploadSummary getBudgetedHistoryDataByID(String id) {
		return demandPlanningMapper.getBudgetedHistoryDataByID(id);
	}

	@SuppressWarnings({ "finally" })
	@Override
	public String getProgressStatus(String userName) {

		String msg = null;
		try {
			DpUploadSummary dpUploadSummary = getLastStatus(userName);
			String status = dpUploadSummary.getStatus();
			String id = dpUploadSummary.getId();
			if (!status.equalsIgnoreCase(AWSUtils.Status.VERIFY.toString())
					&& !status.equalsIgnoreCase(AWSUtils.Status.SUCCEEDED.toString())) {
				if (status.equalsIgnoreCase(AWSUtils.Status.UPLOADED.toString())) {
					updateDPUploadSummary(AWSUtils.Status.PROCESS.toString(), 0, 0, null, id);
					msg = AWSUtils.Status.PROCESS.toString();
				}
			} else {
				msg = AppStatusMsg.SUCCESS;
			}
		} catch (CannotGetJdbcConnectionException ex) {
			throw ex;

		} catch (DataAccessException ex) {
			throw ex;

		} catch (Exception ex) {
			throw ex;

		}
			return msg;
	}

	@Override
	public String getVerifiedAllData(String userName) {
		String msg = null;
		try {
			DpUploadSummary dpUploadSummary = getLastStatus(userName);
			String id = dpUploadSummary.getId();
			String status = dpUploadSummary.getStatus();
			if (!status.equalsIgnoreCase(AWSUtils.Status.SUCCEEDED.toString())) {
				if (status.equalsIgnoreCase(AWSUtils.Status.PROCESS.toString())) {
					updateDPUploadSummary(AWSUtils.Status.VERIFY.toString(), 0, 0, null, id);
					msg = AWSUtils.Status.VERIFY.toString();
				}

			} else {
				msg = getLastStatus(userName).getStatus();
			}
		} catch (CannotGetJdbcConnectionException ex) {
			throw ex;
		} catch (DataAccessException ex) {
			throw ex;
		} catch (Exception ex) {
			throw ex;
		} 
		return msg;
	}

	@SuppressWarnings({ "finally" })
	@Override
	public String getFinalyData(String type, String userName,String orgId) {

		int totalValidLastRecord = 0;
		int totalInvalidLastRecord = 0;
		int totalLastRecord = 0;
		String msg = null;

		try {

			totalValidLastRecord = demandPlanningMapper.getCompareData(userName).size();
			totalInvalidLastRecord = demandPlanningMapper.getNotMatchCompareData(userName).size();
			totalLastRecord = totalValidLastRecord + totalInvalidLastRecord;
			String urlRecord = downloadURLDpUploadSummary(type, userName,orgId);
			DpUploadSummary dpUploadSummary = getLastStatus(userName);
			String staus = dpUploadSummary.getStatus();
			String id = dpUploadSummary.getId();
			if (staus.equalsIgnoreCase(AWSUtils.Status.VERIFY.toString())) {
				updateDPUploadSummary(AWSUtils.Status.SUCCEEDED.toString(), totalValidLastRecord,
						totalInvalidLastRecord, urlRecord, id);
				msg = AWSUtils.Status.SUCCEEDED.toString();
			} else {
				msg = getLastStatus(userName).getStatus();
			}
		} catch (CannotGetJdbcConnectionException ex) {
			throw ex;
		} catch (DataAccessException ex) {
			throw ex;
		} catch (Exception ex) {
			throw ex;
		} finally {
			return msg;
		}

	}

	@SuppressWarnings("finally")
	@Override
	public int createForecastSchedulerAutoConfig(DemandForecastScheduler forecastScheduler, ServletRequest request) {
		int result = 0;

		try {
			OffsetDateTime dateTime = OffsetDateTime.now();
			String ipAddress = InetAddress.getLocalHost().getHostAddress().trim().toString();
			String nextTime = forecastScheduler.getFrequency();
			forecastScheduler.setCreatedOn(dateTime);
			String bucketKey = "/DEMAND_PLANNER/" + UUID.randomUUID().toString();
			forecastScheduler.setIpAddress(ipAddress);
			String scheduler = forecastScheduler.getSchedule();
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Calendar calendarScheduler = Calendar.getInstance();
			calendarScheduler.setTime(simpleDateFormat.parse(scheduler));
			// int period = Integer.parseInt(forecastScheduler.getPeriod());
			String token = ((HttpServletRequest) request).getHeader("X-Auth-Token");
			String tenantHashKey = ThreadLocalStorage.getTenantHashKey();
			String enterpriseId = CodeUtils.decode(token).get("eid").getAsString();
			demandPlanningMapper.updateStatusForecastSchedule(enterpriseId);
			String bucketName = CodeUtils.decode(token).get(FileUtils.BucketName).getAsString();
			S3BucketInfo s3BucketInfo = s3BucketInfoService.getBucketByType(bucketName, FileUtils.FileDownload);
			forecastScheduler.setBucketName(bucketName);
			forecastScheduler.setEnterpriseId(enterpriseId);
			forecastScheduler.setEnterpriseUserName(s3BucketInfo.getEnterprise());
			forecastScheduler.setTenantHashKey(tenantHashKey);
			// forecastScheduler.setBucketKey(bucketKey);
			forecastScheduler.setStatus("ACTIVE");
			// forecastScheduler.setFrequency("MONTHLY");
			if (nextTime.equalsIgnoreCase("30 Days")) {
				calendarScheduler.add(Calendar.DAY_OF_MONTH, 30);
			} else if (nextTime.equalsIgnoreCase("45 Days")) {
				calendarScheduler.add(Calendar.DAY_OF_MONTH, 45);
			} else if (nextTime.equalsIgnoreCase("60 Days")) {
				calendarScheduler.add(Calendar.DAY_OF_MONTH, 60);
			}
			String nextSchedule = simpleDateFormat.format(calendarScheduler.getTime());
			forecastScheduler.setNextSchedule(nextSchedule);
			result = demandPlanningMapper.createForecastSchedulerAutoConfig(forecastScheduler);
		} catch (CannotGetJdbcConnectionException ex) {
			throw ex;

		} catch (DataAccessException ex) {
			ex.printStackTrace();
			throw ex;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		} finally {
			return result;
		}

	}

	@Override
	public DemandForecastScheduler getForecastSchedulerAutoConfigData(String eid) {
		return demandPlanningMapper.getForecastSchedulerAutoConfigData(eid);
	}

	@Override
	public int updateForecastSchedule(String schedule, String nextSchedule, String eid) {
		return demandPlanningMapper.updateForecastSchedule(schedule, nextSchedule, eid);
	}

	@Override
	public int updateMVStatus(String status, char active, String forecastReadStatus) {
		return demandPlanningMapper.updateMVStatus(status, active, forecastReadStatus);
	}

//	@Override
//	public String getStatus() {
//		return demandPlanningMapper.getStatus();
//	}

	@Override
	public int updateProcessingToFailed() {
		return demandPlanningMapper.updateProcessingToFailed();
	}

	@Override
	public void refreshMView() {
		demandPlanningMapper.refreshMView();
	}

	@Override
	public void refreshActualDataMView() {
		demandPlanningMapper.refreshActualDataMView();
	}

	@Override
	public String getForcastOnAssortmentCode() {
		return demandPlanningMapper.getForcastOnAssortmentCode();
	}

	@Override
	public int assortmentHistoryRecord(String startDate, String endDate,String frequency) {
		return demandPlanningMapper.assortmentHistoryRecord(startDate, endDate,frequency);
	}

	@Override
	public List<String> getAssortmentHistory(int offset, int pageSize, String startDate, String endDate,String frequency) {
		return demandPlanningMapper.getAssortmentHistory(offset, pageSize, startDate, endDate,frequency);
	}

	@Override
	public int assortmentSearchHistoryRecord(String search, String startDate, String endDate,String frequency) {
		return demandPlanningMapper.assortmentSearchHistoryRecord(search, startDate, endDate,frequency);
	}

	@Override
	public List<String> assortmentHistorySearch(int offset, int pageSize, String search, String startDate,
			String endDate,String frequency) {
		return demandPlanningMapper.assortmentHistorySearch(offset, pageSize, search, startDate, endDate,frequency);
	}

	@SuppressWarnings("unused")
	@Override
	public ObjectNode getPreviousAssortmentCodeHistory(int pageNo, int type, int totalCount, String assortmentCode,
			String runId, String startDate,String endDate,String search, HttpServletRequest request) throws Exception {
		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int maxPage = 0;
		String key = null;
		LocalDate sDate=null;
		LocalDate eDate=null;
		List<String> assortmentSearchList = new ArrayList<>();

		Set<String> distinctAssortment = new HashSet<>();
		Set<String> finalAssortment = new HashSet<>();
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode objectNode = mapper.createObjectNode();

		List<Map<String, Object>> metaData = new LinkedList<>();
		List<String> viewLogData = new LinkedList<>();
		try {
			DemandPlanning demandPlanning = demandPlanningMapper.getByRunId(runId);
			String token = ((HttpServletRequest) request).getHeader("X-Auth-Token");
			String bucketName  = CodeUtils.decode(token).get(FileUtils.BucketName).getAsString();
			String userName = CodeUtils.decode(token).get("prn").getAsString();
			tenantHashkey = ThreadLocalStorage.getTenantHashKey();
			key = demandPlanning.getSavedPath();
			ArrayNode forecastData = s3WrapperService.readDataFromS3(bucketName, key.substring(1, key.length()));
			
			if(!assortmentCode.equals("NA")){
			 sDate=new LocalDate(startDate);
			 eDate=new LocalDate(endDate);
			}

			if (totalCount == 0) {
				for (int i = 0; i < forecastData.size(); i++) {
						distinctAssortment.add(forecastData.get(i).get("assortmentCode").asText());
					}
				totalRecord = distinctAssortment.size();
			} else {
				totalRecord = totalCount * 10;
			}

			maxPage = (totalRecord + pageSize - 1) / pageSize;
			previousPage = pageNo - 1;
			int offset = (pageNo - 1) * pageSize;
			LOGGER.debug("traversing array node for retrive assortmentcode");
			distinctAssortment.clear();

			if (type == 1 && assortmentCode.equals("NA")) {
				for (int i = 0; i < forecastData.size(); i++) {
					distinctAssortment.add(forecastData.get(i).get("assortmentCode").asText());
					if (distinctAssortment.size() > offset && (distinctAssortment.size() - offset) <= 10) {
						finalAssortment.add(forecastData.get(i).get("assortmentCode").asText());
					}

				}
			} else if (type == 3 && assortmentCode.equals("NA")) {
				
				for (int i = 0; i < forecastData.size(); i++) {
						 distinctAssortment.add(forecastData.get(i).get("assortmentCode").asText());
				}
				for (int j = 0; j < distinctAssortment.size(); j++) {
					List<String> data = distinctAssortment.stream().collect(Collectors.toList());
					if (data.get(j).contains(search)) {
						finalAssortment.add(data.get(j));
					}
				}
				assortmentSearchList = finalAssortment.stream().collect(Collectors.toList());
				if (assortmentSearchList.isEmpty()) {
					return null;
				}
				if(assortmentSearchList.size()<10){
					pageSize=assortmentSearchList.size();
				}
				assortmentSearchList = assortmentSearchList.subList(offset, pageSize);
				totalRecord = assortmentSearchList.size();
				maxPage = (totalRecord + pageSize - 1) / pageSize;
			} else if (!assortmentCode.equals("NA")) {
				for (int i = 0; i < forecastData.size(); i++) {
					LocalDate forecastDate = new LocalDate(CodeUtils.convertNormalDateFormat(forecastData.get(i).get("billdate").asText()));

					if (forecastDate.isBefore(eDate.plusMonths(1)) && forecastDate.isAfter(sDate.minusMonths(1))) {
						if (forecastData.get(i).get("assortmentCode").asText().equals(assortmentCode)) {
							Map<String, Object> assortmentData = new TreeMap<>();
							Map<String, Object> viewLogMap = new TreeMap<>();
							assortmentData.put("assortmentCode", forecastData.get(i).get("assortmentCode").asText());
							assortmentData.put("billDate", forecastData.get(i).get("billdate").asText());
							assortmentData.put("actual", "-");
							assortmentData.put("predicted", forecastData.get(i).get("qty").asInt());
							assortmentData.put("budgeted", "-");
							assortmentData.put("deviation", "-");
							viewLogMap.put("actual", "-");
							viewLogMap.put("predicted", forecastData.get(i).get("qty").asInt());
							viewLogMap.put("budgeted", "-");
							viewLogMap.put("billDate", forecastData.get(i).get("billdate").asText());
							viewLogMap.put("deviation", "-");
							metaData.add(assortmentData);
							viewLogData.add(JsonUtils.objectToSingleLineJson(viewLogMap));
						}
					}
				}
			}

			LOGGER.info("retrive finalAssortment data from s3 excel file");
			if (finalAssortment.isEmpty() && assortmentCode.equals("NA")) {
				return null;
			}
			if (assortmentCode.equals("NA") || assortmentCode.equals("")) {
				objectNode.put(Pagination.CURRENT_PAGE, pageNo);
				objectNode.put(Pagination.PREVIOUS_PAGE, previousPage);
				objectNode.put(Pagination.MAXIMUM_PAGE, maxPage);
				if (type == 1) {
					objectNode.putPOJO(CodeUtils.RESPONSE, finalAssortment);
				} else if (type == 3) {
					objectNode.putPOJO(CodeUtils.RESPONSE, assortmentSearchList);
				}
			} else {

				Collections.sort(metaData, new Comparator<Map<String, Object>>() {
					public int compare(Map<String, Object> s1, Map<String, Object> s2) {
						return CodeUtils.convertNormalDateFormat(s1.get("billDate").toString())
								.compareTo(CodeUtils.convertNormalDateFormat(s2.get("billDate").toString()));
					}
				});
				
				ExecutorService executorService = Executors.newSingleThreadExecutor();
				executorService.execute(new Runnable() {
					@Override
					public void run() {
						try {
							ThreadLocalStorage.setTenantHashKey(tenantHashkey);
							String uuid = UUID.randomUUID().toString();
							String bucketKey = "/FORECAST_REPORT/" + uuid;
							DemandPlanningViewLog planningViewLog = retriveLogData(uuid,
									metaData.get(0).get("assortmentCode").toString(), startDate, endDate,
									demandPlanning.getFrequency(), viewLogData.toString(), userName, bucketName, bucketKey,
									null);
							planningViewLog.setAdditional("previous");
							int createDemandPlanningLog = demandPlanningMapper.createDemandPlanningLog(planningViewLog);
							int updateSummaryStatus = demandPlanningMapper.updateSummaryStatus(uuid,
									demandPlanning.getFrequency());

						} catch (Exception ex) {
							try {
								throw ex;
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}
				});
				objectNode.put("status", "previous");
				objectNode.putPOJO(CodeUtils.RESPONSE, metaData);
			}
		} catch (CannotGetJdbcConnectionException ex) {
			throw ex;
		} catch (DataAccessException ex) {
			throw ex;
		} catch (Exception ex) {
			throw ex;
		}
		return objectNode;
	}
	
	
	@SuppressWarnings({ "unchecked", "rawtypes", "resource" })
	public static void writeForecastReportExcel(List headers,String filePath,Object object) {
		
		Workbook workbook = new XSSFWorkbook();
		Sheet sheet = workbook.createSheet("DEMAND_REPORT");
		int rowIndex = 1;
		Row row1 = sheet.createRow(0);
		row1.createCell(0).setCellValue("ASSORTMENT_CODE");
		for(int i=0;i<headers.size();i++) {
			row1.createCell(i+1).setCellValue(headers.get(i).toString());
		}
		row1 = sheet.createRow(1);
		
		if (object instanceof List) {
			List list = (List) object;
			for (Object insideObject : list) {
				if (insideObject instanceof HashMap) {
					Map<String, String> map = (Map<String, String>) insideObject;
					Row row = sheet.createRow(rowIndex++);
					for (Entry<String, String> entry : map.entrySet()) {
						try {
							if(headers.contains(entry.getKey()) || entry.getKey().equals("ASSORTMENT_CODE")) {
								if(entry.getKey().equals("ASSORTMENT_CODE"))
									row.createCell(0).setCellValue(entry.getValue());
								else if(!entry.getKey().equals("ASSORTMENT_CODE")) {
									int getCellIndex=ExcelUtils.cellIndex(entry.getKey(),headers);
									row.createCell(getCellIndex).setCellValue(entry.getValue());
								}
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			}
			try {
				Path path = Paths.get(filePath);
				try (OutputStream out = Files.newOutputStream(path, CREATE_NEW)) {
					workbook.write(out);
					out.close();
				} catch (FileAlreadyExistsException incrCounterAndRetry) {
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			
			
//			try {
//				FileOutputStream fos = new FileOutputStream(fileName);
//				workbook.write(fos);
//				fos.close();
//			} catch (FileNotFoundException e) {
//				e.printStackTrace();
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
		}
	}

	@SuppressWarnings({ "rawtypes", "unused" })
	@Override
	public String getForecastUrl(String runId, String sDate, String eDate,String frequency, ServletRequest request) {

		String bucketName = null;
		String key = null;
		S3Object s3Object = null;
		String fileName = null;
		String filePath = null;
		String url = null;
		int updateForecastUrl=0;
		try {
			String token = ((HttpServletRequest) request).getHeader("X-Auth-Token");
			JsonArray orgDetails=CodeUtils.decode(token).get("ORG_DETAILS").getAsJsonArray();
			
			JsonObject jNode=orgDetails.get(0).getAsJsonObject();
			String orgId = jNode.get("orgId").getAsString();
			
			
			String startDate=sDate;
			String endDate=eDate;
			if(frequency.equalsIgnoreCase(ChooseType.MONTHLY.toString())) {
				 startDate=CodeUtils.getFirstDayOfTheMonth(startDate);
				 endDate=CodeUtils.getFirstDayOfTheMonth(endDate);
			}else {
				startDate=CodeUtils.getPreviousMonday(startDate);
				endDate=CodeUtils.getPreviousMonday(endDate);
			}
			
			DemandPlanning demandPlanning = demandPlanningMapper.getByRunId(runId);
			bucketName = CodeUtils.decode(token).get(FileUtils.BucketName).getAsString();
			String userName = CodeUtils.decode(token).get("prn").getAsString();
			
			DemandPlanningViewLog viewLog = demandPlanningMapper.getLastReport(frequency, userName);
			
			key = demandPlanning.getSavedPath();
			key = key.substring(1, key.length());

			key = amazonS3Client.getObjectSummary(bucketName, key).getKey();
			s3Object = amazonS3Client.getObjectRequest(bucketName, key);
			List<Map<String, String>> excelData = ExcelUtils.getExcelList(s3Object.getObjectContent());
			S3BucketInfo s3BucketInfo = s3BucketInfoService.getBucketByType(BucketParameter.GENERATE_EXCELCSV,orgId);
			fileName = s3BucketInfo.getEnterprise() + CustomParameter.SEPARATOR + Module.DemandPlanning
					+ CustomParameter.SEPARATOR + CodeUtils.convertDateToString(new Date());
			
			String bucketPath = "FORECAST_REPORT" + CustomParameter.DELIMETER + viewLog.getId()+ CustomParameter.DELIMETER + fileName + FileUtils.EXCELExtension;
			filePath = FileUtils.createFilePath(FileUtils.getPlatformBasedParentDir(), bucketPath);

			filePath = FileUtils.createFilePath(FileUtils.getPlatformBasedParentDir(), bucketPath);
			List headers = CodeUtils.getAllNormalMonths(startDate, endDate);
			writeForecastReportExcel(headers, filePath, excelData);

			File uploadFile = new File(filePath);
			String etag = s3Wrapper.upload(uploadFile, bucketPath, s3BucketInfo.getS3bucketName()).getETag();

			if (!CodeUtils.isEmpty(etag)) {
				String downloadUrl=BucketParameter.BUCKETPREFIX +s3BucketInfo.getS3bucketName()+viewLog.getBucketKey();
				url = UploadUtils.downloadHistory(downloadUrl, BucketParameter.GENERATE_EXCELCSV, request);
				updateForecastUrl=demandPlanningMapper.updatedownloadUrlLogById(url,viewLog.getId());
			} else {
				return null;
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return url;
	}

	@Override
	public String getLastRunId() {
		return demandPlanningMapper.getLastRunId();
	}
	
}
