package com.supplymint.layer.business.service.administration;

import static java.nio.file.StandardOpenOption.CREATE_NEW;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.supplymint.layer.business.contract.administration.ADMSiteMapService;
import com.supplymint.layer.data.tenant.administration.entity.ADMSiteMap;
import com.supplymint.layer.data.tenant.administration.mapper.ADMSiteMapMapper;
import com.supplymint.util.CodeUtils;

@Service
public class ADMSiteMapServiceImpl implements ADMSiteMapService {

	private ADMSiteMapMapper admSiteMapMapper;

	@Autowired
	public ADMSiteMapServiceImpl(ADMSiteMapMapper admSiteMapMapper) {
		this.admSiteMapMapper = admSiteMapMapper;
	}

	@Override
	public ADMSiteMap getById(Integer id) {
		return admSiteMapMapper.findById(id);
	}

	@Override
	public List<ADMSiteMap> getAll(Integer offset, Integer pageSize) {

		return admSiteMapMapper.findAll(offset, pageSize);
	}

	@Override
	public Integer create(ADMSiteMap admSiteMap) {
		return admSiteMapMapper.create(admSiteMap);
	}

	@Override
	public Integer update(ADMSiteMap admSiteMap) {
		return admSiteMapMapper.update(admSiteMap);
	}

	@Override
	public Integer delete(int id) {
		return admSiteMapMapper.delete(id);
	}

	@Override
	public List<ADMSiteMap> getStatus(String status) {
		return admSiteMapMapper.getStatus(status);
	}

	@Override
	public Integer updateStatus(String status, Integer sid, String ipAddress, OffsetDateTime updationTime) {
		return admSiteMapMapper.updateStatus(status, sid, ipAddress, updationTime);
	}

	@Override
	public List<ADMSiteMap> getDetailsOnFromIdtoToId(Integer fromid, Integer toid) {
		return admSiteMapMapper.getDetailsOnFromIdtoToId(fromid, toid);
	}

	@Override
	public List<ADMSiteMap> getDetailsOnFromSiteId(Integer fromsiteid) {
		return admSiteMapMapper.getDetailsOnFromSiteId(fromsiteid);
	}

	@Override
	public Integer record() {
		return admSiteMapMapper.record();
	}

	@Override
	public List<ADMSiteMap> searchAll(Integer offset, Integer pagesize, String search) {
		return admSiteMapMapper.searchAll(offset, pagesize, search);
	}

	@Override
	public int searchRecord(String search) {
		return admSiteMapMapper.searchRecord(search);
	}

	public List<ADMSiteMap> filter(Integer offset, Integer pagesize, String fromSiteName, String toSiteName,
			String startDate, String tptLeadTime, String status) {
		return admSiteMapMapper.filter(offset, pagesize, fromSiteName, toSiteName, startDate, tptLeadTime, status);
	}

	public Integer filterRecord(String fromSiteName, String toSiteName, String startDate, String tptLeadTime,
			String status) {
		return admSiteMapMapper.filterRecord(fromSiteName, toSiteName, startDate, tptLeadTime, status);
	}

	@Override
	public List<ADMSiteMap> getAllRecords() {
		return admSiteMapMapper.getAllRecord();
	}

	@SuppressWarnings("resource")
	@Override
	public void generateSiteMapListTOExcel(List<ADMSiteMap> list, String filePath) {
		Workbook workbook = new XSSFWorkbook();
		Sheet sheet = workbook.createSheet("ADMSiteMap");
		int rowIndex = 1;
		Row row1 = sheet.createRow(0);
		row1.createCell(0).setCellValue("FROM SITE");
		row1.createCell(1).setCellValue("TO SITE");
		row1.createCell(2).setCellValue("START DATE");
		row1.createCell(3).setCellValue("TRANSPORTATION LEAD TIME");
		row1.createCell(4).setCellValue("STATUS");

		for (ADMSiteMap siteMap : list) {

			Row row = sheet.createRow(rowIndex++);

			int cellIndex = 0;
			row.createCell(cellIndex++).setCellValue(siteMap.getFromSite());
			row.createCell(cellIndex++).setCellValue(siteMap.getToSite());
			if (siteMap.getStartDate() == null) {
				row.createCell(cellIndex++).setCellValue("");
			} else {
				row.createCell(cellIndex++).setCellValue(CodeUtils._____dateTimeFormat.format(siteMap.getStartDate()));
			}
			row.createCell(cellIndex++).setCellValue(siteMap.getTptLeadTime());
			row.createCell(cellIndex++).setCellValue(siteMap.getStatus());
		}

		try {
			Path path = Paths.get(filePath);
			try (OutputStream out = Files.newOutputStream(path, CREATE_NEW)) {
				workbook.write(out);
				out.close();
			} catch (FileAlreadyExistsException incrCounterAndRetry) {
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

//		try {
//
//			FileOutputStream fos = new FileOutputStream(filePath);
//
//			workbook.write(fos);
//
//			fos.close();
//
//		} catch (FileNotFoundException e) {
//
//			e.printStackTrace();
//
//		} catch (IOException e) {
//
//			e.printStackTrace();
//
//		}

	}

	@Override
	public Map<String, List<String>> getSiteMapHeaders() {

		Map<String, List<String>> map = new HashMap<String, List<String>>();
		List<String> listOfKey = new ArrayList<String>();
		List<String> listOfValue = new ArrayList<String>();
		listOfValue.add("FROM SITE");
		listOfValue.add("TO SITE");
		listOfValue.add("START DATE");
		listOfValue.add("TRANSPORTATION LEAD TIME");
		listOfValue.add("STATUS");

		listOfKey.add("fromSite");
		listOfKey.add("toSite");
		listOfKey.add("startDate");
		listOfKey.add("tptLeadTime");
		listOfKey.add("status");

		map.put("key", listOfKey);
		map.put("value", listOfValue);

		return map;

	}

}
