
package com.supplymint.layer.business.service.procurement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.supplymint.layer.data.tenant.administration.entity.ADMItem;
import com.supplymint.layer.data.tenant.administration.mapper.ItemMapper;

/**
 * @Authhor Manoj Singh
 * @Date 24-Sep-2018
 * @Version 1.0
 */
@Service
public class ItemService {

	private ItemMapper itemMapper;

	@Autowired
	public ItemService(ItemMapper itemMapper) {
		this.itemMapper = itemMapper;
	}

	public ADMItem getItemById(Integer id) {
		return itemMapper.findById(id);
	}

}
