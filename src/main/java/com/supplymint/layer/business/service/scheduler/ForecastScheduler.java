package com.supplymint.layer.business.service.scheduler;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.supplymint.config.aws.utils.AWSUtils.Conditions;
import com.supplymint.exception.StatusCodes.AppStatusCodes;
import com.supplymint.exception.SupplyMintException;
import com.supplymint.layer.business.contract.demand.DemandPlanningService;
import com.supplymint.layer.business.service.rconnection.RServeEngineProviderService;
import com.supplymint.layer.data.config.ThreadLocalStorage;
import com.supplymint.layer.data.tenant.demand.entity.DemandForecastScheduler;
import com.supplymint.layer.data.tenant.demand.entity.DemandPlanning;
import com.supplymint.layer.web.filters.TenantFilter;
import com.supplymint.util.ApplicationUtils.CustomRunState;
import com.supplymint.util.ApplicationUtils.DemandPlanning.DemandPlanningGraph;
import com.supplymint.util.ApplicationUtils.DemandPlanningParameter;
import com.supplymint.util.CodeUtils;

@Service
public class ForecastScheduler {

	private final static Logger LOGGER = LoggerFactory.getLogger(ForecastScheduler.class);

	@Autowired
	private RServeEngineProviderService rServeEngineProviderService;

	@Autowired
	private TenantFilter tenantFilter;

	private String tenantHashkey;

	private int successCode;

	private String upnDc;

	@Autowired
	private DemandPlanningService demandPlanningService;

//	 @Scheduled(cron = "0 0 12 * * ?")
	public void scheduleForcast() {

		DemandForecastScheduler demandForecastScheduler = null;
		try {
			// tcloud
			ThreadLocalStorage.setTenantHashKey("d16c149919433dbcd4ba5c3255ae8fe1c906a99b967f5193f19ecd4bd4b51901");
			// vmart
			// ThreadLocalStorage.setTenantHashKey("fd40841a596821e2b0d5efff6b7a69f6bf8fe5bdbb525fbac609c22d2dfd93f3");
			tenantHashkey = ThreadLocalStorage.getTenantHashKey();
			upnDc = tenantFilter.getTenantConfigProperty().get(tenantHashkey);
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date today = Calendar.getInstance().getTime();
			String currentDate = dateFormat.format(today);
			String runId = null;
			if (upnDc.equalsIgnoreCase("TCLOUD")) {
				DemandPlanning demandPlanning = new DemandPlanning();
				demandForecastScheduler = demandPlanningService.getForecastSchedulerAutoConfigData("1");
				if (currentDate.equalsIgnoreCase(demandForecastScheduler.getSchedule())) {

					runId = getRunId(demandPlanning);
					String bucketKey = "/DEMAND_PLANNER/" + UUID.randomUUID().toString();

					successCode = rServeEngineProviderService.establishConnection(
							"TRUE/FALSE",demandForecastScheduler.getEnterpriseUserName(), runId, "MONTHLY", "1",
							demandForecastScheduler.getSchedule(), demandForecastScheduler.getBucketName(), bucketKey);
					String scheduler = demandForecastScheduler.getNextSchedule();
					String nextTime = demandForecastScheduler.getFrequency();

					SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
					Calendar calendarScheduler = Calendar.getInstance();
					calendarScheduler.setTime(simpleDateFormat.parse(scheduler));
					if (nextTime.equalsIgnoreCase("30 Days")) {
						calendarScheduler.add(Calendar.DAY_OF_MONTH, 30);
					} else if (nextTime.equalsIgnoreCase("45 Days")) {
						calendarScheduler.add(Calendar.DAY_OF_MONTH, 45);
					} else if (nextTime.equalsIgnoreCase("60 Days")) {
						calendarScheduler.add(Calendar.DAY_OF_MONTH, 60);
					}
					String nextSchedule = simpleDateFormat.format(calendarScheduler.getTime());
					demandPlanningService.updateForecastSchedule(demandForecastScheduler.getNextSchedule(),
							nextSchedule, "1");

				}
			}
			// successCode =
			// rServeEngineProviderService.establishConnection("TCLOUD","MONTHLY","1","2019-2-01","tcloud-supplymint-develop-com","/DEMAND_PLANNER/e536c666-115b-4d70-829e-0c9710d86349");
			LOGGER.info("forecast schedule completed...");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	@SuppressWarnings("finally")
	public String getRunId(DemandPlanning demandPlanning) {
		int result = 0;
		String actualPattern = null;
		String runId = null;
		int validationCreate = 0;
		try {

			actualPattern = demandPlanningService.getRunIdPattern();
			if (actualPattern == null) {
				runId = DemandPlanningParameter.PATTERN;
			} else {
				runId = CodeUtils.createPattern(actualPattern);
			}

			// demandPlanning.setStartedOn(CodeUtils.convertDemandForecastWeeklyDateFormat(startedOn));
			demandPlanning.setDemandForecast(DemandPlanningGraph.DEMAND_ON_FORECAST.toString());
			// demandPlanning.setIpAddress(ipAddress);
			// demandPlanning.setCreatedOn(creationTime);
			demandPlanning.setRunId(runId);
			demandPlanning.setStatus(CustomRunState.Processing.toString());
			demandPlanning.setForecastCode(AppStatusCodes.GENERIC_PROCESSING_CODE);
			demandPlanning.setDbReadStatus(Conditions.FALSE.toString());
			demandPlanning.setSummary(Conditions.FALSE.toString());
			// demandPlanning.setCreatedBy(servletRequest.getAttribute("email").toString());

			LOGGER.info("validate only 1 processing job is created...");
			validationCreate = demandPlanningService.validationCreate();
			if (validationCreate == 0) {
				LOGGER.info("creating a job with processing status");
				result = demandPlanningService.create(demandPlanning);

			} else {
				throw new SupplyMintException();
			}
		} catch (SupplyMintException ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			throw ex;
		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			throw ex;
		} finally {
			return runId;
		}

	}

}
