package com.supplymint.layer.business.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.supplymint.layer.business.contract.MCSTRService;
import com.supplymint.layer.data.tenant.mcstr.entity.MCSTR;
import com.supplymint.layer.data.tenant.mcstr.mapper.MCSTRMapper;

@Service
public class MCSTRServiceImpl implements MCSTRService {

	private MCSTRMapper mcstrMapper;

	@Autowired
	public MCSTRServiceImpl(MCSTRMapper mcstrMapper) {
		this.mcstrMapper = mcstrMapper;

	}

	public Integer insert(MCSTR mcstr) {
		return mcstrMapper.create(mcstr);

	}

	@Override
	public Integer deleteByStoreId(Integer storeId) {

		return mcstrMapper.removeByStoreId(storeId);

	}

}
