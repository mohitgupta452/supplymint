package com.supplymint.layer.business.service.scheduler;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.supplymint.layer.business.contract.dashboard.DashboardService;
import com.supplymint.layer.business.service.core.InventoryAutoConfigService;
import com.supplymint.layer.data.config.ThreadLocalStorage;
import com.supplymint.layer.data.core.entity.InventoryAutoConfig;
import com.supplymint.layer.data.tenant.dashboard.mapper.DashboardMapper;
import com.supplymint.util.ApplicationUtils.AppEnvironment;

@Service
public class DashBoardScheduler {

	@Autowired
	DashboardMapper dashboardMapper;
	
	@Autowired
	DashboardService dashboardService;
	
	@Autowired
	Environment environment;

	@Autowired
	private InventoryAutoConfigService inventoryAutoConfigService;

	private final static Logger LOGGER = LoggerFactory.getLogger(DashBoardScheduler.class);
	
	@Scheduled(cron = "0 30 10 * * ?")
	public void dashBoardScheduler() {

		try {
			ThreadLocalStorage.setTenantHashKey(null);

			List<InventoryAutoConfig> autoConfigList = inventoryAutoConfigService.getAll();
			LOGGER.info(String.format("Obtaining all Configuration from core DB :%s", autoConfigList));
			LOGGER.debug(String.format("DashBoard MV Refresh start :"));

			if (environment.getActiveProfiles()[0].equalsIgnoreCase(AppEnvironment.Profiles.DEV.name())
					|| environment.getActiveProfiles()[0].equalsIgnoreCase(AppEnvironment.Profiles.PROD.name())) {

				for (int i = 0; i < autoConfigList.size(); i++) {
					try {
						ThreadLocalStorage.setTenantHashKey(autoConfigList.get(i).getTenantHashKey());
						LOGGER.info(ThreadLocalStorage.getTenantHashKey());
						LOGGER.debug(String.format("DashBoard MV Refresh start :"));
						dashboardMapper.resfreshDashBoardMv();
						LOGGER.debug(String.format("DashBoard MV Refresh end :"));
						dashboardService.createStoreArticleRank();
						LOGGER.debug(String.format("DashBoard MV insert data successfully :"));
					} catch (Exception ex) {
						LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
					}
				}
			}
			LOGGER.debug(String.format("DashBoard MV Refresh end :"));
		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
		}
	}
	
//	@Scheduled(cron = "0 30 11 * * ?")
	public void createdashBoardScheduler() {

		try {
			ThreadLocalStorage.setTenantHashKey(null);

			List<InventoryAutoConfig> autoConfigList = inventoryAutoConfigService.getAll();
			LOGGER.info(String.format("Obtaining all Configuration from core DB :%s", autoConfigList));
			LOGGER.debug(String.format("DashBoard create store article rank start :"));

//			if (environment.getActiveProfiles().length > 0) {
//				if (environment.getActiveProfiles()[0].equalsIgnoreCase(AppEnvironment.Profiles.DEV.name())
//						|| environment.getActiveProfiles()[0].equalsIgnoreCase(AppEnvironment.Profiles.PROD.name())) {

					for (int i = 0; i < autoConfigList.size(); i++) {
						try {
							ThreadLocalStorage.setTenantHashKey(autoConfigList.get(i).getTenantHashKey());
							LOGGER.info(ThreadLocalStorage.getTenantHashKey());
							dashboardService.createStoreArticleRank();
						} catch (Exception ex) {
							LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
						}
					}
//				}
//			}
			LOGGER.debug(String.format("DashBoard create store article rank end :"));
		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
		}
	}

}
