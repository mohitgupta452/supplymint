package com.supplymint.layer.business.service.scheduler;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.supplymint.layer.business.service.core.InventoryAutoConfigService;
import com.supplymint.layer.data.config.ThreadLocalStorage;
import com.supplymint.layer.data.core.entity.InventoryAutoConfig;
import com.supplymint.layer.data.tenant.procurement.mapper.PurchaseIndentMapper;
import com.supplymint.util.ApplicationUtils.AppEnvironment;

@Service
public class PurchaseIndentScheduler {
	
	private final static Logger LOGGER = LoggerFactory.getLogger(PurchaseIndentScheduler.class);
	
	@Autowired
	PurchaseIndentMapper indentMapper;
	
	@Autowired
	private InventoryAutoConfigService inventoryAutoConfigService;
	
	@Autowired
	Environment environment;
	
	
	@Scheduled(cron = "0 30 9 * * ?")
	public void dashBoardScheduler() {

		try {
			ThreadLocalStorage.setTenantHashKey(null);

			List<InventoryAutoConfig> autoConfigList = inventoryAutoConfigService.getAll();
			LOGGER.info(String.format("Obtaining all Configuration from core DB :%s", autoConfigList));
			LOGGER.debug(String.format("Purchase Indent MV Refresh start :"));
			if (environment.getActiveProfiles()[0].equalsIgnoreCase(AppEnvironment.Profiles.DEV.name())
					|| environment.getActiveProfiles()[0].equalsIgnoreCase(AppEnvironment.Profiles.PROD.name())) {

				for (int i = 0; i < autoConfigList.size(); i++) {
					try {
						ThreadLocalStorage.setTenantHashKey(autoConfigList.get(i).getTenantHashKey());
						LOGGER.info(ThreadLocalStorage.getTenantHashKey());
						indentMapper.resfreshPIMv();
					} catch (Exception ex) {
						LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
					}
				}
			}
			LOGGER.debug(String.format("Purchase Indent MV Refresh end :"));
		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
		}
	}

}
