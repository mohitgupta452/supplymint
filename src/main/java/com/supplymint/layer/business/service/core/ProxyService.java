package com.supplymint.layer.business.service.core;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.supplymint.layer.data.core.entity.Proxy;
import com.supplymint.layer.data.core.mapper.ProxyMapper;

/**
 * 
 * 
 * @author Ashish Singh Dev
 * @version 1.0
 * @since 19-Sep-2018
 *
 */
@Service
public class ProxyService {

	private ProxyMapper proxyMapper;

	@Autowired
	public ProxyService(ProxyMapper proxyMapper) {
		this.proxyMapper = proxyMapper;
	}

	public Proxy get1(Integer identifier, Integer module) {

		return proxyMapper.findByIdAndModule(identifier, module);

	}

}
