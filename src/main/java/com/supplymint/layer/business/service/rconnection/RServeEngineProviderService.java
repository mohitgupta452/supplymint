
package com.supplymint.layer.business.service.rconnection;

/**
 * @author Manoj Singh
 *
 */

import static java.lang.String.format;

import javax.inject.Inject;

import org.rosuda.REngine.REXP;
import org.rosuda.REngine.REXPDouble;
import org.rosuda.REngine.REXPInteger;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REngine;
import org.rosuda.REngine.RList;
import org.rosuda.REngine.Rserve.RConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import com.supplymint.util.ApplicationUtils.AppEnvironment;

@Service
@PropertySource("classpath:rserve.properties")
public class RServeEngineProviderService implements REngineProviderService {

	private static final Logger LOGGER = LoggerFactory.getLogger(RServeEngineProviderService.class);

	private static final int SUCCESS_CODE = 0;

	private String rserveConf;

	private String RCMD = "/usr/lib/R/bin/R CMD";

	public String eval = null;

	public String path = null;

	/** Path to the R executable file */
	private String rexe = "/usr/local/lib/R/site-library/rexe/libs//Rserve";

	Resource resourceFile;

	@Autowired
	private Environment env;

	public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
		return new PropertySourcesPlaceholderConfigurer();
	}

	private final static String HOST = "rServe.rengine.host";

	private final String PORT = "rServe.rengine.port";

	private final static String DEV_SOURCEFILE = "rServe.rengine.dev.source";

	private final static String PROD_SOURCEFILE = "rServe.rengine.production.source";

	private final static String QA_SOURCEFILE = "rServe.rengine.quality.source";

	private final static String DEV_REPORTFILE = "rServe.rengine.dev.report";
	
	private final static String QA_REPORTFILE = "rServe.rengine.quality.report";

	private final static String PROD_REPORTFILE = "rServe.rengine.production.report";

	private final static String WD = "rServe.rengine.wd";

	private final static String MONTH = "rServe.rengine.month";

	private final static String WEEKLY = "rServe.rengine.week";

	@Inject
	private RConnectionFactory rConnectionFactory;

	/**
	 * This method initializes REngine properly and RServe implementation must run R
	 * in a separate process and check the connection.
	 * 
	 */
//	@PostConstruct
	public void setUpR() {
		// R CMD Rserve --RS-port 9000 --RS-enable-remote --RS-enable-control
		// ResourceUtils.getFile("classpath:TEST_PLANING.R").getPath();
		REngine engine = null;
		String dataSourceName = "CITYLIFE";
		String frequency = "MONTHLY";
		int predictTimeline = 06;
		String startDatePattern = "2018-05-01";
		try {
			int PORT = Integer.parseInt(env.getProperty(this.PORT));
			rserveConf = env.getProperty(HOST);

			String command = String.format("echo ';library(\"Rserve\");Rserve(args=\"---RS-enable-remote\")'",
					rserveConf, rexe);

			if (LOGGER.isInfoEnabled()) {
				LOGGER.info("Starting RServe process using this command:\n{}", command);
			}

			ProcessBuilder builder = new ProcessBuilder("/bin/bash", "-c", command);
			builder.inheritIO();
			Process rProcess = builder.start();

			if (LOGGER.isInfoEnabled()) {
				LOGGER.info("Waiting for Rserve to start...");
			}
			int execCodeResult = rProcess.waitFor();

			if (execCodeResult != SUCCESS_CODE) {
				LOGGER.error(String.format("Unexpected error code starting RServe: %d", execCodeResult));
			} else {
				LOGGER.error("RServe started successfully");
			}

			if (LOGGER.isInfoEnabled()) {
				LOGGER.info("Opening connection to RServe daemon....");
			}

			engine = rConnectionFactory.getConnection(env.getProperty(HOST), PORT);
			path = env.getProperty(WD) + env.getProperty(DEV_SOURCEFILE);
			LOGGER.debug(path);
			((RConnection) engine).eval("source('" + path + "')");

			eval = "myforecast(\'" + dataSourceName + "\',\'" + frequency + "\'," + predictTimeline + ",\'"
					+ startDatePattern + "\')";

			LOGGER.debug(eval);

			LOGGER.debug("Executing demand planing function call");
			REXP check = ((RConnection) engine).eval(eval);

			LOGGER.debug(String.format("Obtaining R server result", check.asIntegers()[0]));

			if (LOGGER.isInfoEnabled()) {
				LOGGER.info(String.format("Obtaining R server version: %d", ((RConnection) engine).getServerVersion()));
			}

		} catch (Exception e) {
			LOGGER.error("Unexpected error setting up RServe environment", e);
		} finally {
			rConnectionFactory.releaseConnection(engine);
		}
	}

	public Integer establishConnection(String isBranded,String dataSourceName,String runId,String frequency, String predictTimeline,
			String startDatePattern, String bucketName, String bucketKey) throws REXPMismatchException {
		REngine engine = null;
		REXP rexp = null;

		try {
			int PORT = Integer.parseInt(env.getProperty(this.PORT));
			rserveConf = env.getProperty(HOST);
			String command = String.format("echo ';library(\"Rserve\");Rserve(args=\"---RS-enable-remote\")'",
					rserveConf, rexe);

			if (LOGGER.isInfoEnabled()) {
				LOGGER.info("Starting RServe process using this command:\n{}", command);
			}

			ProcessBuilder builder = new ProcessBuilder("/bin/bash", "-c", command);
			builder.inheritIO();
			Process rProcess = builder.start();

			if (LOGGER.isInfoEnabled()) {
				LOGGER.info("Waiting for Rserve to start...");
			}
			int execCodeResult = rProcess.waitFor();

			if (execCodeResult != SUCCESS_CODE) {
				LOGGER.error(String.format("Unexpected error code starting RServe: %d", execCodeResult));
			} else {
				LOGGER.error("RServe started successfully");
			}

			if (LOGGER.isInfoEnabled()) {
				LOGGER.info("Opening connection to RServe daemon....");
			}

			engine = rConnectionFactory.getConnection(env.getProperty(HOST), PORT);
			path = env.getProperty(WD) + env.getProperty(env.getActiveProfiles().length == 0
					|| env.getActiveProfiles()[0].equalsIgnoreCase(AppEnvironment.Profiles.DEV.name()) ? DEV_SOURCEFILE
							:env.getActiveProfiles()[0].equalsIgnoreCase(AppEnvironment.Profiles.QA.name()) ? QA_SOURCEFILE:PROD_SOURCEFILE);

//			path = env.getProperty(WD) + env.getProperty(DEV_SOURCEFILE);
			LOGGER.debug(path);
			((RConnection) engine).eval("source('" + path + "')");

			eval = "myforecast(\'" + isBranded + "\',\'" + dataSourceName + "\',\'" + runId + "\',\'" + frequency + "\'," + predictTimeline + ",\'"
					+ startDatePattern + "',\'" + bucketName + "',\'" + bucketKey + "\')";
			LOGGER.debug(eval);

			LOGGER.debug("Executing demand planing function call");
			rexp = ((RConnection) engine).eval(eval);

			LOGGER.debug(String.format("Obtaining R server result", rexp.asIntegers()[0]));

			if (LOGGER.isInfoEnabled()) {
				LOGGER.info(String.format("Obtaining R server version: %d", ((RConnection) engine).getServerVersion()));
			}
		} catch (Exception e) {
			LOGGER.error("Unexpected error setting up RServe environment", e);
		} finally {
			rConnectionFactory.releaseConnection(engine);
		}
		return rexp.asIntegers()[0];
	}

	protected void shutdownPreviousRuningInstances() {
		if (LOGGER.isInfoEnabled()) {
			LOGGER.info("Cleaning old Rserve running instances...");
		}

		tearDown();
	}

//	@PreDestroy
	public void tearDown() {
		try {
			if (LOGGER.isInfoEnabled()) {
				LOGGER.info("Shuting down Rserve daemon....");
			}
			RConnection rConnection = rConnectionFactory.getConnection();
			rConnection.shutdown();
			rConnectionFactory.releaseConnection(rConnection);
			if (LOGGER.isInfoEnabled()) {
				LOGGER.info("Shutdown signal sent to Rserve daemon");
			}
		} catch (Exception e) {
			LOGGER.error("Unexpected error shuting down RServe", e);
		}
	}

	@Override
	public double SupplymintRTest(REXPDouble x, REXPDouble y) {

		REngine engine = null;
		try {
			engine = rConnectionFactory.getConnection();
			engine.assign("x", x);
			engine.assign("y", y);
			return engine.parseAndEval("s.test(x, y)['p.value']").asList().at(0).asDouble();
		} catch (Exception e) {
			throw new REngineException("Unexpected error while executing supplymint test", e);
		} finally {
			rConnectionFactory.releaseConnection(engine);
		}
	}

	@Override
	public REXP blockFunction(REXPInteger ids, REXPDouble... values) {

		RList data = new RList();
		data.add(ids);
		data.setKeyAt(0, "ids");
		StringBuilder valueNames = new StringBuilder();
		for (int i = 0; i < values.length; i++) {
			data.add(values[i]);
			valueNames.append(format("\"values%d\",", i));
			data.setKeyAt(i + 1, format("values%d", i));
		}

		String variableNames = valueNames.substring(0, valueNames.length() - 1);
		REngine engine = null;
		try {
			engine = rConnectionFactory.getConnection();
			engine.assign("data", REXP.createDataFrame(data));
			return engine.parseAndEval(format("blockFunction(data,c(\"ids\"),c(%s))", variableNames));
		} catch (Exception e) {
			throw new REngineException("Unexpected error while executing blockFunction", e);
		} finally {
			rConnectionFactory.releaseConnection(engine);
		}
	}

	public String getRserveConf() {
		return rserveConf;
	}

	public void setRserveConf(String rserveConf) {
		this.rserveConf = rserveConf;
	}

	public String getRexe() {
		return rexe;
	}

	public void setRexe(String rexe) {
		this.rexe = rexe;
	}

	public Integer downloadReport(String dataSourceName, String frequency, String startDate, String endDate,
			String bucketName, String bucketKey) throws REXPMismatchException {
		REngine engine = null;
		REXP rexp = null;

		try {
			int PORT = Integer.parseInt(env.getProperty(this.PORT));
			rserveConf = env.getProperty(HOST);
			String command = String.format("echo ';library(\"Rserve\");Rserve(args=\"---RS-enable-remote\")'",
					rserveConf, rexe);

			if (LOGGER.isInfoEnabled()) {
				LOGGER.info("Starting RServe process using this command:\n{}", command);
			}

			ProcessBuilder builder = new ProcessBuilder("/bin/bash", "-c", command);
			builder.inheritIO();
			Process rProcess = builder.start();

			if (LOGGER.isInfoEnabled()) {
				LOGGER.info("Waiting for Rserve to start...");
			}
			int execCodeResult = rProcess.waitFor();

			if (execCodeResult != SUCCESS_CODE) {
				LOGGER.error(String.format("Unexpected error code starting RServe: %d", execCodeResult));
			} else {
				LOGGER.error("RServe started successfully");
			}

			if (LOGGER.isInfoEnabled()) {
				LOGGER.info("Opening connection to RServe daemon....");
			}

			engine = rConnectionFactory.getConnection(env.getProperty(HOST), PORT);
			path = env.getProperty(WD) + env.getProperty(env.getActiveProfiles().length == 0
					|| env.getActiveProfiles()[0].equalsIgnoreCase(AppEnvironment.Profiles.DEV.name()) ? DEV_REPORTFILE
							: env.getActiveProfiles()[0].equalsIgnoreCase(AppEnvironment.Profiles.QA.name()) ?QA_REPORTFILE:PROD_REPORTFILE);
//			path = env.getProperty(WD) + env.getProperty(QA_REPORTFILE);
			LOGGER.debug(path);
			((RConnection) engine).eval("source('" + path + "')");

			eval = "myreport(\'" + dataSourceName + "\',\'" + frequency + "\',\'" + startDate + "\',\'" + endDate
					+ "',\'" + bucketName + "',\'" + bucketKey + "\')";
			LOGGER.debug(eval);

			LOGGER.debug("Executing demand planing function call");
			rexp = ((RConnection) engine).eval(eval);

			LOGGER.debug(String.format("Obtaining R server result", rexp.asIntegers()[0]));

			if (LOGGER.isInfoEnabled()) {
				LOGGER.info(String.format("Obtaining R server version: %d", ((RConnection) engine).getServerVersion()));
			}
		} catch (Exception e) {
			LOGGER.error("Unexpected error setting up RServe environment", e);
		} finally {
			rConnectionFactory.releaseConnection(engine);
		}
		return rexp.asIntegers()[0];
	}

}