
package com.supplymint.layer.business.service.rconnection;

import org.rosuda.REngine.REXP;
import org.rosuda.REngine.REXPDouble;
import org.rosuda.REngine.REXPInteger;

/**
 * Interface used to obtain access to the low-level engine
 */
public interface REngineProviderService {

	/**
	 * Executes R block function against a list of values.
	 * 
	 * @param ids    Identifiers associated to each value
	 * @param values Values used to perform the matching
	 * @return An object containing all the IDs grouped in pairs based on the
	 *         distance of each one
	 */
	REXP blockFunction(REXPInteger ids, REXPDouble... values);

	/**
	 * 
	 * @param x A numeric vector of data values.
	 * @param y A numeric vector of data values.
	 * @return p-value associated with the null hypothesis that x and y come from
	 *         the same distribution (are equivalent)
	 */
	double SupplymintRTest(REXPDouble x, REXPDouble y);
}
