package com.supplymint.layer.business.service.demand;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.CannotGetJdbcConnectionException;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.supplymint.layer.business.contract.demand.AssortmentService;
import com.supplymint.layer.data.tenant.administration.entity.ADMItem;
import com.supplymint.layer.data.tenant.demand.entity.Assortment;
import com.supplymint.layer.data.tenant.demand.entity.AssortmentViewLog;
import com.supplymint.layer.data.tenant.demand.entity.MViewRefreshHistory;
import com.supplymint.layer.data.tenant.demand.mapper.AssortmentMapper;
import com.supplymint.util.ApplicationUtils.DateFormatOTB;
import com.supplymint.util.ApplicationUtils.ModuleName;
import com.supplymint.util.ApplicationUtils.SubModuleName;
import com.supplymint.util.CodeUtils;
import com.supplymint.util.JsonUtils;

/**
 * This Class describes all service Implementation for Assortment
 * @author Prabhakar Srivastava
 * @Date 15 Dec 2018
 * @version 2.0
 */
@Service
public final class AssortmentServiceImpl implements AssortmentService {

	/**
	 * This variable describes for AssortmentMapper instance 
	 */
	private transient final AssortmentMapper assortmentMapper;

	/**
	 * This constructor describes instantiating Assortment Mapper Object
	 */
	@Autowired
	public AssortmentServiceImpl(final AssortmentMapper assortmentMapper) {
		this.assortmentMapper = assortmentMapper;

	}

	@Override
	public int getHl1Record() {
		return assortmentMapper.getHl1Record();
	}

	@Override
	public List<ADMItem> getHL1Name(final int offset,final int pageSize) {
		return assortmentMapper.getHL1Name(offset, pageSize);
	}

	@Override
	public int getHl1SearchRecord(final String search) {
		return assortmentMapper.getHl1SearchRecord(search);
	}

	@Override
	public List<ADMItem> getHl1Search(final int offset,final int pageSize,final String search) {
		return assortmentMapper.getHL1NameSearch(offset, pageSize, search);
	}

	@Override
	public int getHl2Record(final String hl1Name) {
		return assortmentMapper.getHl2Record(hl1Name);
	}

	@Override
	public List<ADMItem> getHL2Name(final int offset,final int pageSize,final String hl1Name) {
		return assortmentMapper.getHL2Name(offset, pageSize, hl1Name);
	}

	@Override
	public int getHl2SearchRecord(final String hl1Name,final String search) {
		return assortmentMapper.getHl2SearchRecord(hl1Name, search);
	}

	@Override
	public List<ADMItem> getHl2Search(final int offset,final int pageSize,final String hl1Name,final String search) {
		return assortmentMapper.getHl2Search(offset, pageSize, hl1Name, search);
	}

	@Override
	public int getHl3Record(final String hl1Name,final String hl2Name) {
		return assortmentMapper.getHl3Record(hl1Name, hl2Name);
	}

	@Override
	public List<ADMItem> getHL3Name(final int offset,final int pageSize,final String hl1Name,final String hl2Name) {
		return assortmentMapper.getHL3Name(offset, pageSize, hl1Name, hl2Name);
	}

	@Override
	public int getHl3SearchRecord(final String hl1Name,final String hl2Name,final String search) {
		return assortmentMapper.getHl3SearchRecord(hl1Name, hl2Name, search);
	}

	@Override
	public List<ADMItem> getHl3Search(final int offset,final int pageSize,final String hl1Name,final String hl2Name,final String search) {
		return assortmentMapper.getHl3Search(offset, pageSize, hl1Name, hl2Name, search);
	}

	@Override
	public int getHl4Record(final Assortment container) {
		return assortmentMapper.getHl4Record(container);
	}

	@Override
	public List<ADMItem> getHL4Name(final Assortment container) {
		return assortmentMapper.getHL4Name(container);
	}

	@Override
	public int getHl4SearchRecord(final Assortment container) {
		return assortmentMapper.getHl4SearchRecord(container);
	}

	@Override
	public List<ADMItem> getHl4Search(final Assortment container) {
		return assortmentMapper.getHl4Search(container);
	}

	@Override
	public int getMRP(final Assortment container) {
		return assortmentMapper.getMRP(container);
	}

	@Override
	public int assortmentRecord() {
		return assortmentMapper.assortmentRecord();
	}
	
	@Override
	public List<Assortment> getAllAssortment(final Integer offset, final Integer pagesize) {
		return assortmentMapper.getAllAssortment(offset, pagesize);

	}
	
	@Override
	public int deleteAssortmentCode(final Assortment container) {
		return assortmentMapper.deleteAssortmentCode(container);
	}

	@Override
	public int createAssortmentCode(final Assortment container) {
		return assortmentMapper.createAssortmentCode(container);
	}

	@Override
	public int getAllHistotyCount() {
		return assortmentMapper.getAllHistotyCount();
	}

	@Override
	public List<AssortmentViewLog> getAllHistoty(final Assortment container) {
		return assortmentMapper.getAllHistoty(container);
	}

	@Override
	public int filterHistoryRecord(final Assortment container) {
		return assortmentMapper.filterHistoryRecord(container);
	}

	@Override
	public List<AssortmentViewLog> filterHistory(final Assortment container) {
		return assortmentMapper.filterHistory(container);
	}

	@Override
	public int searchHistoryRecord(final Assortment container) {
		return assortmentMapper.searchHistoryRecord(container);
	}

	@Override
	public List<AssortmentViewLog> searchHistory(final Assortment container) {
		return assortmentMapper.searchHistory(container);
	}

	@Override
	public int createLog(final AssortmentViewLog viewLog) {
		return assortmentMapper.createLog(viewLog);
	}

	@Override
	public int searchAssortmentRecord(final String search) {
		return assortmentMapper.searchAssortmentRecord(search);
	}

	@Override
	public List<Assortment> searchAssortment(final Integer offset, final Integer pageSize, final String search) {
		return assortmentMapper.searchAssortment(offset, pageSize, search);
	}

	@Override
	public void refreshAssortmentMV() {
		assortmentMapper.refreshAssortmentMV();
	}

	@Override
	public int updateMViewRefreshHistory(final MViewRefreshHistory mvRefresh) {
		return assortmentMapper.updateMViewRefresh(mvRefresh);
	}

	@Override
	public int isExistenceMViewWithModule(final String module, final String subModule) {
		return assortmentMapper.isExistenceMViewWithModule(module, subModule);
	}

	@Override
	public int createMViewRefresh(final MViewRefreshHistory mvRefresh) {
		return assortmentMapper.createMViewRefresh(mvRefresh);
	}

	@Override
	public String getDefaultAssortment(final String defaultAssortment) {
		return assortmentMapper.getDefaultAssortment(defaultAssortment);
	}

	@Override
	public String getMVStatus() {
		return assortmentMapper.getMVStatus();
	}

	@Override
	public int viewedAssortmentMessage() {
		return assortmentMapper.viewedAssortmentMessage(ModuleName.INVENTRY_PLANNING,
				SubModuleName.ASSORTMENT);
	}

	@Override
	public String getAssortmentStatus() {
		return assortmentMapper.getAssortmentStatus();
	}

	@Override
	public int updateMVForecastStatus(final String module, final String subModule) {
		return assortmentMapper.updateMVForecastStatus(module, subModule);
	}

	@Override
	public String forecastReadSatus() {
		return assortmentMapper.forecastReadSatus();
	}

	@Override
	public ObjectNode getAllHlevelDropDown() throws Exception {
		Map<String, Object> sortedMap = new TreeMap<>();
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode objectNode = mapper.createObjectNode();
		String defaultAssort = assortmentMapper.getDefaultAssortment("DEFAULT_HLEVEL_CONFIG");
		try {
			JSONObject jsonObject = new JSONObject(defaultAssort);
			Map<String, Object> hlevelData = JsonUtils.toMap(jsonObject);
			sortedMap = new TreeMap<String, Object>(hlevelData);
			objectNode.putPOJO("hlevel", sortedMap);
		} catch (CannotGetJdbcConnectionException ex) {
			throw ex;
		} catch (DataAccessException ex) {
			throw ex;
		} catch (Exception ex) {
			throw ex;
		}
		ObjectNode node = mapper.createObjectNode();
		node.putPOJO(CodeUtils.RESPONSE, objectNode);
		return node;
	}

	@Override
	public void createMainHierarchyViewForForecast(String forecastMainHierarchy, String forecastSubHierarchy,String enterpriseType) {

		String billDateMonthly = null;
		String billDateWeekly = null;
		try {
			
			String billDateFormat = assortmentMapper.getBillDateFormat();
			String assortmentHierarchy = "";
			String invSubHierarchy = "";
			String desSubHierarchy = "";
			String[] values = forecastSubHierarchy.split(",");
			for (String val : values) {
				if(!CodeUtils.isEmpty(val)) {
					assortmentHierarchy += "tabl." + val + "||'-'||";
					invSubHierarchy += "inv." + val + ",";
					desSubHierarchy += "des." + val + ",";
				}
			}
			if(!CodeUtils.isEmpty(assortmentHierarchy)) {
				assortmentHierarchy = "tabl." + forecastMainHierarchy + "||'-'||"
						+ assortmentHierarchy.substring(0, assortmentHierarchy.length() - 7);
				invSubHierarchy = "inv." + forecastMainHierarchy + ","
						+ invSubHierarchy.substring(0, invSubHierarchy.length() - 1);
				desSubHierarchy = "des." + forecastMainHierarchy + ","
						+ desSubHierarchy.substring(0, desSubHierarchy.length() - 1);
			}else {
				assortmentHierarchy = "tabl." + forecastMainHierarchy;
				invSubHierarchy = "inv." + forecastMainHierarchy;
				desSubHierarchy = "des." + forecastMainHierarchy;
			}

			if (CodeUtils.validateDate(billDateFormat, DateFormatOTB.DATEWITHFORWORDSLASH)) {
				billDateMonthly = "to_char(to_date(to_char(to_date(billDate,'yyyy-mm-dd'),'mm-yyyy'),'mm-yyyy'),'yyyy-mm-dd')";
				billDateWeekly = "to_char(next_day(to_date(billdate, 'MM/dd/yyyy')-7, 'monday'),'yyyy-MM-dd')";
			} else if (CodeUtils.validateDate(billDateFormat, DateFormatOTB.DATEWITHDATEMONTHYEAR)) {
				// for bazaarKolkata date format
				billDateMonthly = "to_char(to_date(to_date(to_char(to_date(billdate,'dd-mm-yyyy'),'MM-yyyy'),'MM-yyyy'),'dd-MM-yy'),'yyyy-MM-dd')";
				billDateWeekly = "to_char(to_date(to_char(next_day(to_date(billdate,'dd-mm-yyyy')-7, 'monday'),'dd-mm-yy'),'dd-mm-yy'),'yyyy-mm-dd')";
			} else if (CodeUtils.validateDate(billDateFormat, DateFormatOTB.DATEWITHMONTHDATEYEAR)) {
				// for megashop date format
				billDateMonthly = "to_char(to_date(to_char(to_date(billdate,'MM/dd/yyyy'),'mm-yyyy'),'mm-yyyy'),'yyyy-mm-dd')";
				billDateWeekly = "to_char(next_day(to_date(billdate,'MM/dd/yyyy')-7, 'monday'),'yyyy-MM-dd')";
			} else if (CodeUtils.validateDate(billDateFormat, DateFormatOTB.DATEWITHYEARMONTHDATE)) {
				// for mufti and style_bazaar date format
				billDateMonthly = "to_char(to_date(to_char(to_date(billDate,'yyyy-mm-dd'),'mm-yyyy'),'mm-yyyy'),'yyyy-mm-dd')";
				billDateWeekly = "to_char(next_day(to_date(billdate,'yyyy-mm-dd')-7, 'monday'),'yyyy-MM-dd')";
			}

			try {
				try {
					assortmentMapper.dropMainHierarchyViewForForecastMonthly();
					assortmentMapper.dropMainHierarchyViewForForecastWeekly();
					assortmentMapper.createMainHierarchyViewForForecastMonthly(forecastMainHierarchy, billDateMonthly);
					assortmentMapper.createMainHierarchyViewForForecastWeekly(forecastMainHierarchy, billDateWeekly);

					assortmentMapper.dropMainHierarchyViewForForecastMonthlyFinal();
					assortmentMapper.dropMainHierarchyViewForForecastWeeklyFinal();
					assortmentMapper.createMainHierarchyViewForForecastMonthlyFinal(assortmentHierarchy,
							forecastMainHierarchy, invSubHierarchy, desSubHierarchy);
					assortmentMapper.createMainHierarchyViewForForecastWeeklyFinal(assortmentHierarchy,
							forecastMainHierarchy, invSubHierarchy, desSubHierarchy);
				} catch (Exception ex) {
					assortmentMapper.createMainHierarchyViewForForecastMonthlyFinal(assortmentHierarchy,
							forecastMainHierarchy, invSubHierarchy, desSubHierarchy);
					assortmentMapper.createMainHierarchyViewForForecastWeeklyFinal(assortmentHierarchy,
							forecastMainHierarchy, invSubHierarchy, desSubHierarchy);
				}
			} catch (Exception ex) {
				assortmentMapper.createMainHierarchyViewForForecastMonthly(forecastMainHierarchy, billDateMonthly);
				assortmentMapper.createMainHierarchyViewForForecastWeekly(forecastMainHierarchy, billDateWeekly);

				assortmentMapper.createMainHierarchyViewForForecastMonthlyFinal(assortmentHierarchy,
						forecastMainHierarchy, invSubHierarchy, desSubHierarchy);
				assortmentMapper.createMainHierarchyViewForForecastWeeklyFinal(assortmentHierarchy,
						forecastMainHierarchy, invSubHierarchy, desSubHierarchy);
			}
		} catch (CannotGetJdbcConnectionException ex) {
			throw ex;
		} catch (DataAccessException ex) {
			throw ex;
		} catch (Exception ex) {
			throw ex;
		}
	}

}
