package com.supplymint.layer.business.service.tenant;

import java.security.NoSuchAlgorithmException;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.stereotype.Service;

import com.supplymint.config.datasource.ConfigProperties;
import com.supplymint.layer.data.core.entity.Tenant;
import com.supplymint.util.SecurityUtils;

/**
 * 
 * 
 * @author Ashish Singh Dev
 * @version 1.0
 * @since 20-Sep-2018
 *
 */
@Service
public class TenantHelper {

	private final static Logger LOGGER = LoggerFactory.getLogger(TenantHelper.class);

	@Autowired
	private ConfigProperties configProperties;

	public Tenant getCoreTenant() {
		Tenant core = new Tenant();
		core.setId(configProperties.getCore_id());
		core.setUuid(configProperties.getCore_uid());
		core.setName(configProperties.getCore_name());
		return core;
	}

	public DataSource getCoreDs() {
		DataSource coreDataSource = DataSourceBuilder.create().driverClassName(configProperties.getDriverClassName())
				.username(configProperties.getUsername()).password(configProperties.getPassword())
				.url(configProperties.getUrl()).build();

		return coreDataSource;
	}

	public String getTenantHashKey(Tenant tenant) {
		try {
			LOGGER.info("Generated tenant hash key :%s"
					, SecurityUtils.hashSHA256(String.format("%s:%s", tenant.getId(), tenant.getUuid())));
			return SecurityUtils.hashSHA256(String.format("%s:%s", tenant.getId(), tenant.getUuid()));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		return null;
	}
}
