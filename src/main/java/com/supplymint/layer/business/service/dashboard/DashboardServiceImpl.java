package com.supplymint.layer.business.service.dashboard;

import static java.nio.file.StandardOpenOption.CREATE_NEW;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import javax.servlet.http.HttpServletRequest;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.supplymint.exception.StatusCodes.DashboardMsg;
import com.supplymint.layer.business.contract.dashboard.DashboardService;
import com.supplymint.layer.business.contract.s3Bucket.S3BucketInfoService;
import com.supplymint.layer.data.config.ThreadLocalStorage;
import com.supplymint.layer.data.tenant.dashboard.entity.ArticleMoving;
import com.supplymint.layer.data.tenant.dashboard.entity.DashboardWindow;
import com.supplymint.layer.data.tenant.dashboard.entity.SalesTrend;
import com.supplymint.layer.data.tenant.dashboard.entity.StoreArticleRank;
import com.supplymint.layer.data.tenant.dashboard.entity.StoreRank;
import com.supplymint.layer.data.tenant.dashboard.mapper.DashboardMapper;
import com.supplymint.util.CodeUtils;

@Service
public class DashboardServiceImpl implements DashboardService {

	private static final Logger LOGGER = LoggerFactory.getLogger(DashboardServiceImpl.class);

	@Autowired
	private DashboardMapper dashboardMapper;

	public DashboardServiceImpl(DashboardMapper dashboardMapper) {
		this.dashboardMapper = dashboardMapper;
	}

	@Override
	public Map<String, String> getDashboardWindowValues() {

		Map<String, String> windowMap = new HashMap<String, String>();

//		long totalSales = dashboardMapper.getPreviousYearTotalSales();
//
//		int totalUnits = dashboardMapper.getPreviousYearTotalUnits();
//
//		int avgSalesUnit = dashboardMapper.getPreviousWeekSalesUnit();
//
//		int avgAssortmentUnit = dashboardMapper.getLastMonthAssortmentUnit();

		LOGGER.info(ThreadLocalStorage.getTenantHashKey());
		DashboardWindow window = dashboardMapper.getDashboardWindow();

		if (!CodeUtils.isEmpty(window)) {

//			if (window.getTotalSales() > 0 && window.getTotalUnits() > 0 && window.getAvgSalesUnit() > 0
//					&& window.getAvgAssortmentUnit() > 0) {

			windowMap.put("totalSales", String.valueOf(window.getTotalSales()));
			windowMap.put("totalUnits", String.valueOf(window.getTotalUnits()));
			windowMap.put("avgSalesUnit", String.valueOf(window.getAvgSalesUnit()));
			windowMap.put("avgAssortmentUnit", String.valueOf(window.getAvgAssortmentUnit()));
			windowMap.put("isValid", "true");

//			} else {
//				windowMap.put("totalSales", String.valueOf("0"));
//				windowMap.put("totalUnits", String.valueOf("0"));
//				windowMap.put("avgSalesUnit", String.valueOf("0"));
//				windowMap.put("avgAssortmentUnit", String.valueOf("0"));
//				windowMap.put("isValid", "true");
//
//			}

		} else {
			windowMap = null;
		}
		return windowMap;
	}

	@Override
	public Map<String, List<StoreArticleRank>> getTopStoreArticleRankNode() {
		int size = 5;
		List<StoreArticleRank> storeArticleRank = dashboardMapper.getStoresArticleRank();
		Map<String, List<StoreArticleRank>> topStoreArticleMap = null;

		if (!CodeUtils.isEmpty(storeArticleRank)) {
			topStoreArticleMap = new HashMap<String, List<StoreArticleRank>>();
			List<StoreArticleRank> topArticles = new ArrayList<StoreArticleRank>();
			List<StoreArticleRank> bottomArticles = new ArrayList<StoreArticleRank>();
			List<StoreArticleRank> topStores = new ArrayList<StoreArticleRank>();
			List<StoreArticleRank> bottomStores = new ArrayList<StoreArticleRank>();

			for (int i = 0; i < storeArticleRank.size(); i++) {

				if (storeArticleRank.get(i).getType().equalsIgnoreCase("article")
						&& Integer.parseInt(storeArticleRank.get(i).getRank()) > 0) {
					topArticles.add(storeArticleRank.get(i));
				}
				if (storeArticleRank.get(i).getType().equalsIgnoreCase("article")
						&& Integer.parseInt(storeArticleRank.get(i).getRank()) < 0) {
					bottomArticles.add(storeArticleRank.get(i));
				}
				if (storeArticleRank.get(i).getType().equalsIgnoreCase("store")
						&& Integer.parseInt(storeArticleRank.get(i).getRank()) > 0) {
					topStores.add(storeArticleRank.get(i));
				}
				if (storeArticleRank.get(i).getType().equalsIgnoreCase("store")
						&& Integer.parseInt(storeArticleRank.get(i).getRank()) < 0) {
					bottomStores.add(storeArticleRank.get(i));
				}
			}

			/* @Author Bishnu -> Line of code allows user to set the action with */

//			topStoreArticleMap.put("topArticles", topArticles.subList(0, 5));
//			topStoreArticleMap.put("bottomArticles", bottomArticles.subList(0, 5));
//			topStoreArticleMap.put("topStores", topStores.subList(0, 5));
//			topStoreArticleMap.put("bottomStores", bottomStores.subList(0, 5));

			int endTopArticles = Math.min(size, topArticles.size());
			int endBottomArticles = Math.min(size, bottomArticles.size());
			int endTopStores = Math.min(size, topStores.size());
			int endBottomStores = Math.min(size, bottomStores.size());

			topStoreArticleMap.put("topArticles", topArticles.subList(0, endTopArticles));
			topStoreArticleMap.put("bottomArticles", bottomArticles.subList(0, endBottomArticles));
			topStoreArticleMap.put("topStores", topStores.subList(0, endTopStores));
			topStoreArticleMap.put("bottomStores", bottomStores.subList(0, endBottomStores));

			/* Ends */
		}

		return topStoreArticleMap;
	}

	@Override
	public Map<String, Object> getSalesTrend(String type) {

		List<SalesTrend> salesTrend = null;
		List<SalesTrend> unitsTrend = null;
		Map<String, List<SalesTrend>> salesTrendMap = null;
		Map<String, String> yearsMap = null;

		Map<String, Object> mainMap = null;

		String trendSalesType = "";
		String trendUnitType = "";
		String firstYear = "";
		String lastYear = "";
		String years = "";

		if (type.equalsIgnoreCase("currentmonth")) {
			trendSalesType = DashboardMsg.TOTALSALES_CURRENTMONTH;
			trendUnitType = DashboardMsg.TOTALUNITS_CURRENTMONTH;
			salesTrend = dashboardMapper.getCurrentMonthSalesTrend(trendSalesType);
			unitsTrend = dashboardMapper.getCurrentMonthUnitsTrend(trendUnitType);

//			firstYear = unitsTrend.get(0).getYear();
//			lastYear = unitsTrend.get(unitsTrend.size() - 1).getYear();
//
//			years = firstYear.equalsIgnoreCase(lastYear) ? firstYear : firstYear + "-" + lastYear;

		} else if (type.equalsIgnoreCase("lastmonth")) {
			trendSalesType = DashboardMsg.TOTALSALES_LASTMONTH;
			trendUnitType = DashboardMsg.TOTALUNITS_LASTMONTH;

			salesTrend = dashboardMapper.getLastMonthSalesTrend(trendSalesType);
			unitsTrend = dashboardMapper.getLastMonthUnitsTrend(trendUnitType);

//			firstYear = unitsTrend.get(0).getYear();
//			lastYear = unitsTrend.get(unitsTrend.size() - 1).getYear();
//
//			years = firstYear.equalsIgnoreCase(lastYear) ? firstYear : firstYear + "-" + lastYear;

		} else if (type.equalsIgnoreCase("lastthreemonth")) {
			trendSalesType = DashboardMsg.TOTALSALES_LASTTHREEMONTHS;
			trendUnitType = DashboardMsg.TOTALUNITS_LASTTHREEMONTHS;

			salesTrend = dashboardMapper.getLastThreeMonthSalesTrend(trendSalesType);
			unitsTrend = dashboardMapper.getLastThreeMonthUnitsTrend(trendUnitType);

//			firstYear = unitsTrend.get(0).getYear();
//			lastYear = unitsTrend.get(unitsTrend.size() - 1).getYear();
//
//			years = firstYear.equalsIgnoreCase(lastYear) ? firstYear : firstYear + "-" + lastYear;

		} else if (type.equalsIgnoreCase("lastsixmonth")) {
			trendSalesType = DashboardMsg.TOTALSALES_LASTSIXMONTHS;
			trendUnitType = DashboardMsg.TOTALUNITS_LASTSIXMONTHS;

			salesTrend = dashboardMapper.getSixMonthSalesTrend(trendSalesType);
			unitsTrend = dashboardMapper.getSixMonthUnitsTrend(trendUnitType);

//			firstYear = unitsTrend.get(0).getYear();
//			lastYear = unitsTrend.get(unitsTrend.size() - 1).getYear();
//			years = firstYear.equalsIgnoreCase(lastYear) ? firstYear : firstYear + "-" + lastYear;

		} else if (type.equalsIgnoreCase("lastfinancialyear")) {

			trendSalesType = DashboardMsg.TOTALSALES_LASTFINANCIALYEAR;
			trendUnitType = DashboardMsg.TOTALUNITS_LASTFINANCIALYEAR;

			salesTrend = dashboardMapper.getLastFinancialYearSalesTrend(trendSalesType);
			unitsTrend = dashboardMapper.getLastFinancialYearUnitsTrend(trendUnitType);

//			firstYear = unitsTrend.get(0).getYear();
//			lastYear = unitsTrend.get(unitsTrend.size() - 1).getYear();
//			years = firstYear.equalsIgnoreCase(lastYear) ? firstYear : firstYear + "-" + lastYear;

		} else {
			trendSalesType = DashboardMsg.TOTALSALES_LASTTWELFTHMONTHS;
			trendUnitType = DashboardMsg.TOTALUNITS_LASTTWELFTHMONTHS;

			salesTrend = dashboardMapper.getLastTwelfthMonthSalesTrend(trendSalesType);
			unitsTrend = dashboardMapper.getLastTwelfthMonthUnitsTrend(trendUnitType);

//			firstYear = unitsTrend.get(0).getYear();
//			lastYear = unitsTrend.get(unitsTrend.size() - 1).getYear();
//			years = firstYear.equalsIgnoreCase(lastYear) ? firstYear : firstYear + "-" + lastYear;
		}

		if (!CodeUtils.isEmpty(salesTrend) && !CodeUtils.isEmpty(unitsTrend)) {
			firstYear = unitsTrend.get(0).getYear();
			lastYear = unitsTrend.get(unitsTrend.size() - 1).getYear();
			years = firstYear.equalsIgnoreCase(lastYear) ? firstYear : firstYear + "-" + lastYear;

			mainMap = new HashMap<String, Object>();
			salesTrendMap = new HashMap<String, List<SalesTrend>>();
			yearsMap = new HashMap<String, String>();

			salesTrendMap.put("unitsTrend", unitsTrend);
			salesTrendMap.put("salesTrend", salesTrend);
			yearsMap.put("years", years);

			mainMap.put("sales", salesTrendMap);
			mainMap.put("year", yearsMap);

		}
		return mainMap;
	}

	@Override
	public List<StoreArticleRank> getStoreArticleRank() {
//		List<StoreArticleRank> storeArticleRank = dashboardMapper.getStoresArticleRank();

		OffsetDateTime date = OffsetDateTime.now();
		date.minusMinutes(10);

		LOGGER.info("Month value :%s ", date.getMonthValue());
		LOGGER.info("Minute :%s", date.minusMinutes(10).getMinute());
		LOGGER.info("Year :%s", date.getYear());

		return null;
	}

	@Override
	public void createStoreArticleRank() {

		List<StoreRank> storeRank = dashboardMapper.getStoresRank();
		List<StoreRank> articleRank = dashboardMapper.getArticlesRank();

		LOGGER.info(storeRank.toString());
		LOGGER.info(articleRank.toString());
		int deleteStoreArticleRanks = 0;
		int deleteArticleMoving = 0;

		if (!CodeUtils.isEmpty(storeRank) && !CodeUtils.isEmpty(articleRank)) {
			deleteStoreArticleRanks = deleteStoreRank();
			deleteArticleMoving = dashboardMapper.deleteArticleRank();

			for (StoreRank sRank : storeRank) {
				int rank = dashboardMapper.createStoreRank(sRank.getStoreCode(),sRank.getStoreName(), "STORE", sRank.getStoreRank());
				LOGGER.info("Store Rank inserted :%s", sRank.toString());
			}

			for (int i = 0; i < articleRank.size(); i++) {

				int rank = dashboardMapper.createArticleRank(articleRank.get(i).getStoreCode(),articleRank.get(i).getStoreName(), "ARTICLE",
						articleRank.get(i).getStoreRank());
				LOGGER.info("Article Rank inserted :%s", articleRank.get(i).toString());

				int articleMoving = dashboardMapper.createArticleMoving(articleRank.get(i).getStoreCode(),
						articleRank.get(i).getStoreRank());
				LOGGER.info("Article Moving created :%s", articleRank.get(i).toString());
			}

		}
	}

	@Override
	public int deleteStoreRank() {
		int deleteRanks = dashboardMapper.deleteStoreRanks();
		return deleteRanks;
	}

	@Override
	public Map<String, List<ArticleMoving>> getArticleMoving() {
		List<ArticleMoving> articleRank = dashboardMapper.getArticlesMoving();

		Map<String, List<ArticleMoving>> articleMovingMap = null;

		if (!CodeUtils.isEmpty(articleRank)) {
			articleMovingMap = new HashMap<String, List<ArticleMoving>>();

			List<ArticleMoving> slowMoving = new ArrayList<ArticleMoving>();
			List<ArticleMoving> fastMoving = new ArrayList<ArticleMoving>();

			for (int i = 0; i < articleRank.size(); i++) {

				LOGGER.info("article rank %s:", articleRank.get(i));

				if (Integer.parseInt(articleRank.get(i).getArticleRank()) > 0) {
					fastMoving.add(articleRank.get(i));
				}
				if (Integer.parseInt(articleRank.get(i).getArticleRank()) < 0) {
					slowMoving.add(articleRank.get(i));
				}
			}
			articleMovingMap.put("slowMoving", slowMoving);
			articleMovingMap.put("fastMoving", fastMoving);

		}
		return articleMovingMap;
	}

	/* @Author Bishnu -> Line of code allows user to set the action with */
	@Override
	public List<StoreArticleRank> getStoresArticleRank() {
		return dashboardMapper.getStoresArticleRank();
	}

	@Override
	public void generateDashBoardListTOExcel(Map<String, List<StoreArticleRank>> storeRank, String filePath) {

		int index = 1;
		Workbook workbook = new XSSFWorkbook();

		List<StoreArticleRank> topArticles = storeRank.get("topArticles");
		List<StoreArticleRank> bottomArticles = storeRank.get("bottomArticles");
		List<StoreArticleRank> topStores = storeRank.get("topStores");
		List<StoreArticleRank> bottomStores = storeRank.get("bottomStores");

		Sheet topStoresSheet = workbook.createSheet("TOP STORES");
		Sheet bottomStoresSheet = workbook.createSheet("BOTTOM STORES");
		Sheet topArticleSheet = workbook.createSheet("TOP ARTICLE");
		Sheet bottomArticleSheet = workbook.createSheet("BOTTOM ARTICLE");

		Row topStoresRow1 = topStoresSheet.createRow(0);
		Row bottomStoresRow1 = bottomStoresSheet.createRow(0);
		Row topArticleRow1 = topArticleSheet.createRow(0);
		Row bottomArticleRow1 = bottomArticleSheet.createRow(0);

		topStoresRow1.createCell(0).setCellValue("STORE NAME");
		topStoresRow1.createCell(1).setCellValue("LAST YEAR");
		topStoresRow1.createCell(2).setCellValue("THIS YEAR");
		topStoresRow1.createCell(3).setCellValue("LAST MONTH");
		topStoresRow1.createCell(4).setCellValue("PREV YEAR LAST MONTH");

		bottomStoresRow1.createCell(0).setCellValue("STORE NAME");
		bottomStoresRow1.createCell(1).setCellValue("LAST YEAR");
		bottomStoresRow1.createCell(2).setCellValue("THIS YEAR");
		bottomStoresRow1.createCell(3).setCellValue("LAST MONTH");
		bottomStoresRow1.createCell(4).setCellValue("PREV YEAR LAST MONTH");

		topArticleRow1.createCell(0).setCellValue("ARTICLE NAME");
		topArticleRow1.createCell(1).setCellValue("LAST YEAR");
		topArticleRow1.createCell(2).setCellValue("THIS YEAR");
		topArticleRow1.createCell(3).setCellValue("LAST MONTH");
		topArticleRow1.createCell(4).setCellValue("PREV YEAR LAST MONTH");

		bottomArticleRow1.createCell(0).setCellValue("ARTICLE NAME");
		bottomArticleRow1.createCell(1).setCellValue("LAST YEAR");
		bottomArticleRow1.createCell(2).setCellValue("THIS YEAR");
		bottomArticleRow1.createCell(3).setCellValue("LAST MONTH");
		bottomArticleRow1.createCell(4).setCellValue("PREV YEAR LAST MONTH");

		for (StoreArticleRank topStore : topStores) {
			Row row = topStoresSheet.createRow(index++);
			int cellIndex = 0;
			row.createCell(cellIndex++).setCellValue(topStore.getCode());
			row.createCell(cellIndex++).setCellValue(topStore.getLastYearSV());
			row.createCell(cellIndex++).setCellValue(topStore.getCurrentYearSV());
			row.createCell(cellIndex++).setCellValue(topStore.getLastMonthSV());
			row.createCell(cellIndex++).setCellValue(topStore.getPyLastMonthSV());
		}
		index = 1;

		for (StoreArticleRank bottomStore : bottomStores) {
			Row row = bottomStoresSheet.createRow(index++);
			int cellIndex = 0;
			row.createCell(cellIndex++).setCellValue(bottomStore.getCode());
			row.createCell(cellIndex++).setCellValue(bottomStore.getLastYearSV());
			row.createCell(cellIndex++).setCellValue(bottomStore.getCurrentYearSV());
			row.createCell(cellIndex++).setCellValue(bottomStore.getLastMonthSV());
			row.createCell(cellIndex++).setCellValue(bottomStore.getPyLastMonthSV());
		}

		index = 1;

		for (StoreArticleRank bottomStore : topArticles) {
			Row row = topArticleSheet.createRow(index++);
			int cellIndex = 0;
			row.createCell(cellIndex++).setCellValue(bottomStore.getCode());
			row.createCell(cellIndex++).setCellValue(bottomStore.getLastYearSV());
			row.createCell(cellIndex++).setCellValue(bottomStore.getCurrentYearSV());
			row.createCell(cellIndex++).setCellValue(bottomStore.getLastMonthSV());
			row.createCell(cellIndex++).setCellValue(bottomStore.getPyLastMonthSV());
		}

		index = 1;
		for (StoreArticleRank bottomStore : bottomArticles) {
			Row row = bottomArticleSheet.createRow(index++);
			int cellIndex = 0;
			row.createCell(cellIndex++).setCellValue(bottomStore.getCode());
			row.createCell(cellIndex++).setCellValue(bottomStore.getLastYearSV());
			row.createCell(cellIndex++).setCellValue(bottomStore.getCurrentYearSV());
			row.createCell(cellIndex++).setCellValue(bottomStore.getLastMonthSV());
			row.createCell(cellIndex++).setCellValue(bottomStore.getPyLastMonthSV());
		}

		try {
			Path path = Paths.get(filePath);
			try (OutputStream out = Files.newOutputStream(path, CREATE_NEW)) {
				workbook.write(out);
				out.close();
			} catch (FileAlreadyExistsException incrCounterAndRetry) {
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

//		try {
//			FileOutputStream fos = new FileOutputStream(filePath);
//			workbook.write(fos);
//			fos.close();
//
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
	}
	// End

//		
	@SuppressWarnings({ "hiding" })
	@Override
	public void generateDashBoardListTOPdfData(Map<String, List<StoreArticleRank>> topStoreArticle, String filePath,
			HttpServletRequest request) {

		Document doc = new Document();
		try {
			List<StoreArticleRank> topArticles = new ArrayList<StoreArticleRank>();
			List<StoreArticleRank> bottomArticles = new ArrayList<StoreArticleRank>();
			List<StoreArticleRank> topStores = new ArrayList<StoreArticleRank>();
			List<StoreArticleRank> bottomStores = new ArrayList<StoreArticleRank>();
			topArticles = topStoreArticle.get("topArticles");
			bottomArticles = topStoreArticle.get("bottomArticles");
			topStores = topStoreArticle.get("topStores");
			bottomStores = topStoreArticle.get("bottomStores");
			List<String> topArticleslist = new LinkedList<String>();
			List<String> bottomArticlesList = new LinkedList<String>();
			List<String> topStoreslist = new LinkedList<String>();
			List<String> bottomStoresList = new LinkedList<String>();

			topArticles.stream().forEach(e -> {
				topArticleslist.add(e.getCode());
				topArticleslist.add(e.getLastYearSV());
				topArticleslist.add(e.getCurrentYearSV());
				topArticleslist.add(e.getLastMonthSV());
				topArticleslist.add(e.getPyLastMonthSV());
			});
			bottomArticles.stream().forEach(e -> {
				bottomArticlesList.add(e.getCode());
				bottomArticlesList.add(e.getLastYearSV());
				bottomArticlesList.add(e.getCurrentYearSV());
				bottomArticlesList.add(e.getLastMonthSV());
				bottomArticlesList.add(e.getPyLastMonthSV());
			});
			topStores.stream().forEach(e -> {
				topStoreslist.add(e.getCode());
				topStoreslist.add(e.getLastYearSV());
				topStoreslist.add(e.getCurrentYearSV());
				topStoreslist.add(e.getLastMonthSV());
				topStoreslist.add(e.getPyLastMonthSV());
			});
			bottomStores.stream().forEach(e -> {
				bottomStoresList.add(e.getCode());
				bottomStoresList.add(e.getLastYearSV());
				bottomStoresList.add(e.getCurrentYearSV());
				bottomStoresList.add(e.getLastMonthSV());
				bottomStoresList.add(e.getPyLastMonthSV());
			});

			PdfWriter writer = PdfWriter.getInstance(doc, new FileOutputStream(filePath));
			doc.open();

			PdfPTable table1 = new PdfPTable(5);
			PdfPTable table2 = new PdfPTable(5);
			PdfPTable table3 = new PdfPTable(5);
			PdfPTable table4 = new PdfPTable(5);

			table1.setWidthPercentage(100);
			table1.setSpacingBefore(10f);
			table1.setSpacingAfter(10f);
			float[] column1 = { 2.5f, 2f, 2f, 2.5f, 4f };
			table1.setWidths(column1);

			table2.setWidthPercentage(100);
			table2.setSpacingBefore(10f);
			table2.setSpacingAfter(10f);
			float[] column2 = { 2.5f, 2f, 2f, 2.5f, 4f };
			table2.setWidths(column2);

			table3.setWidthPercentage(100);
			table3.setSpacingBefore(10f);
			table3.setSpacingAfter(10f);
			float[] column3 = { 2.5f, 2f, 2f, 2.5f, 4f };
			table3.setWidths(column3);

			table4.setWidthPercentage(100);
			table4.setSpacingBefore(10f);
			table4.setSpacingAfter(10f);
			float[] column4 = { 2.5f, 2f, 2f, 2.5f, 4f };
			table4.setWidths(column4);

			Stream.of("STORE CODE", "LAST YEAR", "THIS YEAR", "LAST MONTH", "PREV YEAR LAST MONTH")
					.forEach(columnTitle -> {
						PdfPCell header = new PdfPCell();
						header.setBorderWidth(1);
						header.setFixedHeight(20);
						header.setPhrase(new Phrase(columnTitle));
						table3.addCell(header);
						table4.addCell(header);
					});

			Stream.of("ARTICLE CODE", "LAST YEAR", "THIS YEAR", "LAST MONTH", "PREV YEAR LAST MONTH")
					.forEach(columnTitle -> {
						PdfPCell header = new PdfPCell();
						header.setBorderWidth(1);
						header.setFixedHeight(20);
						header.setPhrase(new Phrase(columnTitle));
						table1.addCell(header);
						table2.addCell(header);
					});

			for (String str : topArticleslist) {
				PdfPCell header = new PdfPCell();
				header.setFixedHeight(18);
				header.setPhrase(new Phrase(str));
				table1.addCell(header);
			}
			for (String str : bottomArticlesList) {
				PdfPCell header = new PdfPCell();
				header.setFixedHeight(18);
				header.setPhrase(new Phrase(str));
				table2.addCell(header);
			}
			for (String str : topStoreslist) {
				PdfPCell header = new PdfPCell();
				header.setFixedHeight(18);
				header.setPhrase(new Phrase(str));
				table3.addCell(header);
			}
			for (String str : bottomStoresList) {
				PdfPCell header = new PdfPCell();
				header.setFixedHeight(18);
				header.setPhrase(new Phrase(str));
				table4.addCell(header);
			}

			Paragraph p = new Paragraph("TOP STORES AND ARTICLES SALES",
					FontFactory.getFont(FontFactory.HELVETICA, 18, Font.BOLD));
			p.setAlignment(Element.ALIGN_CENTER);

			doc.add(p);
			doc.add(new Paragraph(" "));
			doc.add(new Paragraph("TOP STORES", FontFactory.getFont(FontFactory.HELVETICA_BOLD, 12)));
			doc.add(table3);
			doc.add(new Paragraph(" "));
			doc.add(new Paragraph("BOTTOM STORES", FontFactory.getFont(FontFactory.HELVETICA_BOLD, 12)));
			doc.add(table4);
			doc.add(new Paragraph(" "));
			doc.add(new Paragraph("TOP ARTICLE", FontFactory.getFont(FontFactory.HELVETICA_BOLD, 12)));
			doc.add(table1);
			doc.add(new Paragraph(" "));
			doc.add(new Paragraph("BOTTOM ARTICLE", FontFactory.getFont(FontFactory.HELVETICA_BOLD, 12)));
			doc.add(table2);
			doc.close();

			writer.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	@Override
	public int deleteArticleMoving() {
		// TODO Auto-generated method stub
		return 0;
	}
}
