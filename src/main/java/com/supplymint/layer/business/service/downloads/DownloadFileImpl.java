package com.supplymint.layer.business.service.downloads;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.supplymint.layer.business.contract.administration.ADMSiteMapService;
import com.supplymint.layer.business.contract.administration.ADMSiteService;
import com.supplymint.layer.business.contract.administration.ADMUserRoleService;
import com.supplymint.layer.business.contract.administration.ADMUserService;
import com.supplymint.layer.business.contract.administration.OrganisationService;
import com.supplymint.layer.business.contract.dashboard.DashboardService;
import com.supplymint.layer.business.contract.demand.DemandPlanningService;
import com.supplymint.layer.business.contract.downloads.DownloadFile;
import com.supplymint.layer.business.contract.inventory.AdHocRequestService;
import com.supplymint.layer.data.tenant.administration.entity.ADMOrganisation;
import com.supplymint.layer.data.tenant.administration.entity.ADMSite;
import com.supplymint.layer.data.tenant.administration.entity.ADMSiteMap;
import com.supplymint.layer.data.tenant.administration.entity.ADMUser;
import com.supplymint.layer.data.tenant.administration.entity.ADMUserRole;
import com.supplymint.layer.data.tenant.dashboard.entity.StoreArticleRank;
import com.supplymint.layer.data.tenant.demand.entity.AdHocRequest;
import com.supplymint.layer.data.tenant.demand.entity.BudgetedDemandPlanning;
import com.supplymint.layer.data.tenant.demand.entity.DemandPlanning;
import com.supplymint.layer.data.tenant.demand.entity.DpUploadSummary;
import com.supplymint.layer.data.tenant.demand.entity.FinalAssortment;
import com.supplymint.util.CSVUtils;
import com.supplymint.util.CodeUtils;
import com.supplymint.util.CodeUtils.Module;
import com.supplymint.util.ExcelUtils;
import com.supplymint.util.FileUtils;

@Service
public class DownloadFileImpl implements DownloadFile {

	static String delimeter = "/";
	public static final String CSV = "csv";
	public static final String XLS = "xls";
	public static String separator = "_";
	private String excel = "EXCEL";
	private static final String WEEKLY = "WEEKLY";
	private static final String MONTHLY = "MONTHLY";
	public static final String PDF = "pdf";

	@Autowired
	OrganisationService organisationService;

	@Autowired
	ADMUserService userService;

	@Autowired
	ADMUserRoleService userRoleService;

	@Autowired
	ADMSiteService siteService;

	@Autowired
	ADMSiteMapService siteMapService;

	@Autowired
	AdHocRequestService adHocRequestService;

	@Autowired
	private DemandPlanningService demandPlanningService;
	@Autowired
	private DashboardService dashboardService;

	@Override
	public Map<String, String> generateExcelMap(String fileType, String mName, HttpServletRequest request) {

		Map<String, String> excelDownloadData = new HashMap<>();

		String savedPath = null;
		String fileName = null;
		String moduleName = mName;

		savedPath = FileUtils.getPlatformBasedParentDir().toString() + File.separator + excel + File.separator
				+ moduleName;
		File existFilePath = new File(savedPath);
		if (!existFilePath.exists())
			existFilePath.mkdirs();

		if (fileType.equalsIgnoreCase(CSV)) {

			fileName = moduleName + separator + CodeUtils.______dateFormat.format(new Date()) + FileUtils.CSVExtension;

		} else if (fileType.equalsIgnoreCase(XLS)) {

			fileName = moduleName + separator + CodeUtils.______dateFormat.format(new Date())
					+ FileUtils.EXCELExtension;
		} else if (fileType.equalsIgnoreCase(PDF)) {

			fileName = moduleName + separator + CodeUtils.______dateFormat.format(new Date()) + FileUtils.PDFExtension;
		}

		if (moduleName.equalsIgnoreCase(Module.Organisation.toString())) {

			List<ADMOrganisation> dataList = organisationService.getAllData();
			String savePathWithFileName = savedPath + File.separator + fileName;
			List<String> value = organisationService.getOrganisationHeaders().get("value");
			List<String> key = organisationService.getOrganisationHeaders().get("key");
			
			if (fileType.equalsIgnoreCase(XLS))
				ExcelUtils.customizedHeaderWriteToExcel(key, savePathWithFileName, dataList, value);
			else
				CSVUtils.writeCsvFile(key, savePathWithFileName, dataList, value);
			
			// organisationService.generateOrganisationListTOExcel(dataList,
			// savePathWithFileName);
		} else if (moduleName.equalsIgnoreCase(Module.Site.toString())) {
			List<ADMSite> dataList = siteService.getAllRecords();
			String savePathWithFileName = savedPath + File.separator + fileName;
			List<String> value = siteService.getSiteHeaders().get("value");
			List<String> key = siteService.getSiteHeaders().get("key");
			
			if (fileType.equalsIgnoreCase(XLS))
				ExcelUtils.customizedHeaderWriteToExcel(key, savePathWithFileName, dataList, value);
			else
				CSVUtils.writeCsvFile(key, savePathWithFileName, dataList, value);
			// siteService.generateSiteListTOExcel(dataList, savePathWithFileName);
		} else if (moduleName.equalsIgnoreCase(Module.SiteMap.toString())) {
			List<ADMSiteMap> dataList = siteMapService.getAllRecords();
			String savePathWithFileName = savedPath + File.separator + fileName;
			List<String> value = siteMapService.getSiteMapHeaders().get("value");
			List<String> key = siteMapService.getSiteMapHeaders().get("key");
			
			if (fileType.equalsIgnoreCase(XLS))
				ExcelUtils.customizedHeaderWriteToExcel(key, savePathWithFileName, dataList, value);
			else
				CSVUtils.writeCsvFile(key, savePathWithFileName, dataList, value);
			//siteMapService.generateSiteMapListTOExcel(dataList, savePathWithFileName);
		} else if (moduleName.equalsIgnoreCase(Module.User.toString())) {
			String token = ((HttpServletRequest) request).getHeader("X-Auth-Token");
			String userName = CodeUtils.decode(token).get("prn").getAsString();
			List<ADMUser> dataList = userService.getAllRecords(userName);
			String savePathWithFileName = savedPath + File.separator + fileName;
			List<String> value =userService.getUserHeaders().get("value");
			List<String> key =userService.getUserHeaders().get("key");
			
			if (fileType.equalsIgnoreCase(XLS))
				ExcelUtils.customizedHeaderWriteToExcel(key, savePathWithFileName, dataList, value);
			else
				CSVUtils.writeCsvFile(key, savePathWithFileName, dataList, value);
			//userService.generateUserListTOExcel(dataList, savePathWithFileName);
		} else if (moduleName.equalsIgnoreCase(Module.UserRole.toString())) {
			String token = ((HttpServletRequest) request).getHeader("X-Auth-Token");
			String userName = CodeUtils.decode(token).get("prn").getAsString();
			List<ADMUserRole> dataList = userRoleService.getAllRecords(userName);
			String savePathWithFileName = savedPath + File.separator + fileName;
			List<String> value =userRoleService.getUserRoleHeaders().get("value");
			List<String> key =userRoleService.getUserRoleHeaders().get("key");
			
			if (fileType.equalsIgnoreCase(XLS))
				ExcelUtils.customizedHeaderWriteToExcel(key, savePathWithFileName, dataList, value);
			else
				CSVUtils.writeCsvFile(key, savePathWithFileName, dataList, value);
//			userRoleService.generateUserRoleListTOExcel(dataList, savePathWithFileName);
		} else if (moduleName.equalsIgnoreCase(Module.AdHocRequest.toString())) {
			List<AdHocRequest> dataList = adHocRequestService.getAllRecords();
			String savePathWithFileName = savedPath + File.separator + fileName;
			List<String> value =adHocRequestService.getAdHocRequestHeaders().get("value");
			List<String> key =adHocRequestService.getAdHocRequestHeaders().get("key");
			
			if (fileType.equalsIgnoreCase(XLS))
				ExcelUtils.customizedHeaderWriteToExcel(key, savePathWithFileName, dataList, value);
			else
				CSVUtils.writeCsvFile(key, savePathWithFileName, dataList, value);
			//adHocRequestService.generateAdHocRequestListTOExcel(dataList, savePathWithFileName);
		} else if (moduleName.equalsIgnoreCase(Module.DemandPlanning.toString())) {
			List<DemandPlanning> dataList = demandPlanningService.downloadForecastDetails();
			String savePathWithFileName = savedPath + File.separator + fileName;
			demandPlanningService.generateForecastTOExcel(dataList, savePathWithFileName);
		} else if (moduleName.equalsIgnoreCase(Module.FinalAssortment.toString())) {
			List<FinalAssortment> dataList = demandPlanningService.getFinalAssortmentData();
			String savePathWithFileName = savedPath + File.separator + fileName;
			ExcelUtils.writeToExcel(savePathWithFileName, dataList);
		} else if (moduleName.equalsIgnoreCase(Module.ExcelTemplate.toString())) {
			String savePathWithFileName = savedPath + File.separator + fileName;
			demandPlanningService.generateExcelTemplate(savePathWithFileName);
		} else if (moduleName.equalsIgnoreCase(Module.DemandBudgeted.toString())) {
			List<BudgetedDemandPlanning> dataList = null;
			String token = ((HttpServletRequest) request).getHeader("X-Auth-Token");
			String userName = CodeUtils.decode(token).get("prn").getAsString();
			DpUploadSummary dpUploadSummary = demandPlanningService.getLastStatus(userName);
			String frequency = dpUploadSummary.getType();
			if (frequency.equalsIgnoreCase(WEEKLY)) {
				moduleName = Module.DemandBudgetedWeekly.toString();
				dataList = demandPlanningService.getAllInvalidWeeklyData();
			} else if (frequency.equalsIgnoreCase(MONTHLY)) {
				moduleName = Module.DemandBudgetedMonthly.toString();
				dataList = demandPlanningService.getAllInvalidMonthlyData();
			}
			String savePathWithFileName = savedPath + File.separator + fileName;
			demandPlanningService.generateBudgetedDemandListTOExcel(dataList, savePathWithFileName);
		} else if (!fileType.equalsIgnoreCase(PDF) && moduleName.equalsIgnoreCase(Module.DashBoard.toString())) {
			// List<StoreArticleRank> dataList = dashboardService.getStoresArticleRank();
			Map<String, List<StoreArticleRank>> dataList = dashboardService.getTopStoreArticleRankNode();
			String savePathWithFileName = savedPath + File.separator + fileName;
			dashboardService.generateDashBoardListTOExcel(dataList, savePathWithFileName);
		} else if (fileType.equalsIgnoreCase(PDF) && moduleName.equalsIgnoreCase(Module.DashBoard.toString())) {
			Map<String, List<StoreArticleRank>> topStoreArticle = dashboardService.getTopStoreArticleRankNode();
			String savePathWithFileName = savedPath + File.separator + fileName;
			dashboardService.generateDashBoardListTOPdfData(topStoreArticle, savePathWithFileName, request);
		}
		excelDownloadData.put("FILENAME", fileName);
		excelDownloadData.put("FILEPATH", savedPath);
		return excelDownloadData;
	}

}
