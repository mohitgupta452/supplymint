package com.supplymint.layer.business.contract.setting;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * This interface describes all service for OTB and HLEVEL Configuration
 * @author Prabhakar Srivastava 
 * @author Varun Sharma
 * @Date   10 May 2019
 * @version 1.0
 */
public interface OTBSettingService {

	/**
	 * This method describes getting HLEVEL Data
	 * @param HLEVEL Type
	 * @Table SYSTEM_DEFAULT_CONFIG
	 */
	ObjectNode getHLevel(String hlevel) throws Exception;

	/**
	 * This method describes create HLEVEL Configuration Data
	 * @param HLEVEL Pay load
	 * @Table SYSTEM_DEFAULT_CONFIG
	 */
	int create(JsonNode hlevelConfig);

	/**
	 * This method describes get all HLEVEL and UDF
	 * @Table SYSTEM_DEFAULT_CONFIG
	 */
	ObjectNode getDefaultAssort() throws Exception;

	/**
	 * This method describes create OTB Configuration Data
	 * @param HLEVEL Pay load
	 * @Table SYSTEM_DEFAULT_CONFIG
	 */
	int createAssortment(JsonNode otbConfig) throws Exception;
	
	/**
	 * This method describes get Selected HLEVEL AND UDF Data
	 * @Table SYSTEM_DEFAULT_CONFIG
	 */
	ObjectNode getAllConfig() throws Exception;
	
	/**
	 * This method describes get Selected OTB Data
	 * @Table SYSTEM_DEFAULT_CONFIG
	 */
	ObjectNode getAllOTBConfig() throws Exception;

}
