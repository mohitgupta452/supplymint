package com.supplymint.layer.business.contract.s3Bucket;

import java.util.List;

import com.supplymint.layer.data.tenant.inventory.entity.S3BucketInfo;

/**
 * @author Manoj Singh
 * @since 25 OCT 2018
 * @version 1.0
 */

public interface S3BucketInfoService {

	/**
	 * This method describes get Bucket Info
	 * 
	 * @param Bucket Name
	 * @Table S3BUCKETINFO
	 */
	List<S3BucketInfo> getBucketByName(String bucketName);

	/**
	 * This method describes get Bucket Status
	 * 
	 * @param Status
	 * @Table S3BUCKETINFO
	 */
	List<S3BucketInfo> getBucketStatus(String status);

	/**
	 * This method describes get all Bucket Info
	 * 
	 * @Table S3BUCKETINFO
	 */
	List<S3BucketInfo> getAllBucket();

	/**
	 * This method describes create S3Bucket Entry For AWS Configuration
	 * 
	 * @param S3BucketInfo
	 * @Table S3BUCKETINFO
	 */
	Integer s3InsertFileName(S3BucketInfo s3BucketInfo);

	/**
	 * This method describes get All File Name
	 * 
	 * @Table S3BUCKETINFO
	 */
	List<S3BucketInfo> getAllFileName();

	/**
	 * This method describes update fileName
	 * 
	 * @param S3BucketInfo
	 * @Table S3BUCKETINFO
	 */
	Integer s3UpdateFileName(S3BucketInfo s3BucketInfo);

	/**
	 * This method describes update S3Bucket Information For AWS Configuration
	 * 
	 * @param Bucket Name , Type
	 * @Table S3BUCKETINFO
	 */
	S3BucketInfo getBucketByType(String bucketName, String type, String orgId);

	/**
	 * This method describes update S3Bucket Information For AWS Configuration
	 * 
	 * @param Type
	 * @Table S3BUCKETINFO
	 */
	S3BucketInfo getBucketByType(String type, String orgId);

	/**
	 * This method describes update S3Bucket Information For AWS Configuration
	 * 
	 * @param Type
	 * @Table S3BUCKETINFO
	 */
	List<S3BucketInfo> getBucketByType(String type);

	/**
	 * This method describes check Unique Output File Name
	 * 
	 * @param Output File Name
	 * @Table S3BUCKETINFO
	 */
	List<S3BucketInfo> isExistS3FileName(String outputFileName);

	/**
	 * This method describes insert Bucket Info
	 * 
	 * @param S3BucketInfo
	 * @Table S3BUCKETINFO
	 */
	Integer insertBucketName(S3BucketInfo s3BucketInfo);

	/**
	 * This method describes check Unique Bucket Name
	 * 
	 * @param Bucket Name
	 * @Table S3BUCKETINFO
	 */
	List<S3BucketInfo> isExistBucketName(String bucketName);

	/**
	 * This method describes update Bucket Info By Bucket Name
	 * 
	 * @param S3BucketInfo
	 * @Table S3BUCKETINFO
	 */
	Integer updateBucketName(S3BucketInfo s3BucketInfo);

	/**
	 * This method describes get Bucket Info
	 * 
	 * @param S3BucketInfo
	 * @Table S3BUCKETINFO
	 */
	List<S3BucketInfo> getByURL(String url);

	/**
	 * This method describes update Bucket Info By Bucket Type
	 * 
	 * @param S3BucketInfo
	 * @Table S3BUCKETINFO
	 */
	int updateBucketByType(S3BucketInfo s3BucketInfo);

	/**
	 * This method describes update Profile Image path
	 * 
	 * @param Path
	 * @Table S3BUCKETINFO
	 */
	int updateBucketByProfile(String path);

	int updateManualTransferOrder(String manualTransferUrl, String bucketType);

}
