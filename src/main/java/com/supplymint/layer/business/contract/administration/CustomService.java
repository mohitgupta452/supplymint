package com.supplymint.layer.business.contract.administration;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.supplymint.exception.SupplyMintException;
import com.supplymint.layer.data.tenant.administration.entity.CustomDataTemplate;
import com.supplymint.layer.data.tenant.administration.entity.CustomFileUpload;
import com.supplymint.layer.data.tenant.administration.entity.CustomMaster;
import com.supplymint.layer.data.tenant.administration.entity.DataReference;
import com.supplymint.layer.data.tenant.administration.entity.EventMaster;

public interface CustomService {

	int createCustomFileUpload(CustomFileUpload customFileUpload);

	int getAllCustomRecord();

	List<CustomFileUpload> getAllCustomFileUpload(int offset, int pageSize);

	List<CustomMaster> getAllZoneByChannel(String partner, String channel);

	List<CustomMaster> getAllGradeByZone(String partner, String channel, String zone);

	List<CustomMaster> getAllStoreCode(String partner, String channel, String zone, String grade);

	List<CustomDataTemplate> getAllTemplate(String channel);

	List<DataReference> getAllDataReference(Integer offset, Integer pageSize, String channel);

	int refRecordCount(String channel);

	int searchDataReferenceRecord(String name, String key, String eventStart, String eventEnd, String status,
			Integer dataCount, String channel);

	List<DataReference> searchDataReference(Integer pageSize, Integer offset, String name, String key,
			String eventStart, String eventEnd, String status, Integer dataCount, String channel);

	List<DataReference> getAllRecords();

	List<DataReference> searchAllDataReference(Integer offset, Integer pageSize, String search, String channel);

	Integer searchAllRecordDataReference(String search, String channel);

	List<CustomMaster> getAllPartnerByChannel(String partner);

	List<DataReference> getTop5DataReference();

	List<CustomMaster> filterStoreCode(String channel, String partner, String zone, String grade);

	String getCustomByKey(String key);

	String getCustomDateFormat();

	int createCustomMaster(JsonNode payload, HttpServletRequest request) throws Exception, SupplyMintException;

	int getAllCountEventMaster(String orgId);

	ObjectNode getAllEventMasterData(int offset, int pageSize, String orgId);

	int filterCountEventMasterRecord(String eventName, String startDate, String endDate, String stockDays,
			String multiplierROS, String partner, String grade, String storeCode, String zone, String orgId);

	ObjectNode getFilterCustomEventMaster(int pageSize, int offset, String eventName, String startDate, String endDate,
			String stockDays, String multiplierROS, String partner, String grade, String storeCode, String zone,
			String orgId);

	int searchCountCustomMasterData(String search, String orgId);

	ObjectNode searchAllCustomMasterData(int offset, int pageSize, String search, String orgId);

	int deleteEventMaster(JsonNode payload, String orgId);
	
	Map<String, Object> getAllMotherComp( int pageNo,int type,String search);

	Map<String,Object> getAllArticleByMotherComp(int pageNo,int type,String motherComp,String search);
	
	Map<String,Object> getAllVendor(int pageNo,int type,String motherComp,String articleCode,String search);
	
	Map<String,Object> getAllStore(int pageNo,int type,String motherComp,String articleCode,String vendorCode,String search);
	
	public String getAllFMCGFilterStore(JsonNode payload);


}
