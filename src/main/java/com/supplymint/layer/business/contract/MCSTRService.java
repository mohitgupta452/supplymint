package com.supplymint.layer.business.contract;

import com.supplymint.layer.data.tenant.mcstr.entity.MCSTR;

public interface MCSTRService {

	Integer insert(MCSTR mcstr);

	Integer deleteByStoreId(Integer storeId);
}
