package com.supplymint.layer.business.contract.dashboard;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.supplymint.layer.data.tenant.dashboard.entity.ArticleMoving;
import com.supplymint.layer.data.tenant.dashboard.entity.StoreArticleRank;

public interface DashboardService {

	Map<String, String> getDashboardWindowValues();

	Map<String, List<StoreArticleRank>> getTopStoreArticleRankNode();

	Map<String, Object> getSalesTrend(String type);

	List<StoreArticleRank> getStoreArticleRank();

	void createStoreArticleRank();

	int deleteStoreRank();

	int deleteArticleMoving();

	Map<String, List<ArticleMoving>> getArticleMoving();

	List<StoreArticleRank> getStoresArticleRank();

	void generateDashBoardListTOExcel(Map<String, List<StoreArticleRank>> list, String filePath);

	void generateDashBoardListTOPdfData(Map<String, List<StoreArticleRank>> topStoreArticle,
			String savePathWithFileName, HttpServletRequest request);
}
