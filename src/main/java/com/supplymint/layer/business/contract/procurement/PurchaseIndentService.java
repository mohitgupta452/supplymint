package com.supplymint.layer.business.contract.procurement;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.ibatis.annotations.Param;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.supplymint.exception.SupplyMintException;
import com.supplymint.layer.data.tenant.administration.entity.ADMItem;
import com.supplymint.layer.data.tenant.procurement.entity.CatMaster;
import com.supplymint.layer.data.tenant.procurement.entity.DeptItemUDFSettings;
import com.supplymint.layer.data.tenant.procurement.entity.DescriptionMaster;
import com.supplymint.layer.data.tenant.procurement.entity.FinCharge;
import com.supplymint.layer.data.tenant.procurement.entity.HLevelData;
import com.supplymint.layer.data.tenant.procurement.entity.HsnData;
import com.supplymint.layer.data.tenant.procurement.entity.HsnGstData;
import com.supplymint.layer.data.tenant.procurement.entity.ItemValidation;
import com.supplymint.layer.data.tenant.procurement.entity.PurchaseIndent;
import com.supplymint.layer.data.tenant.procurement.entity.PurchaseIndentDetail;
import com.supplymint.layer.data.tenant.procurement.entity.PurchaseIndentHistory;
import com.supplymint.layer.data.tenant.procurement.entity.PurchaseIndentMaster;
import com.supplymint.layer.data.tenant.procurement.entity.PurchaseIndentProperty;
import com.supplymint.layer.data.tenant.procurement.entity.PurchaseTermData;
import com.supplymint.layer.data.tenant.procurement.entity.PurtermCharge;
import com.supplymint.layer.data.tenant.procurement.entity.PurtermDet;
import com.supplymint.layer.data.tenant.procurement.entity.PurtermMaster;
import com.supplymint.layer.data.tenant.procurement.entity.SupplierData;
import com.supplymint.layer.data.tenant.procurement.entity.Transporters;

public interface PurchaseIndentService {

	List<HLevelData> getDistinctHierarachyLevels(Integer offset, Integer pagesize);

	Integer recordDistinctHierarachyLevels();

	List<Transporters> getTransportersBySlcode(String slCode, Integer offset, Integer pagesize);

	Integer getRecordTransportersBySlcode(String slCode);

	List<SupplierData> getSupplierData(String department, String siteCode, Integer offset, Integer pagesize);

	List<SupplierData> getCustomSupplierData(String department, Integer offset, Integer pagesize);

	Integer getRecordSupplierData(String department, String siteCode);

	Integer getRecordCustomSupplierData(String department);

	Integer getSupplierLeadTime(String slCode);

	List<ADMItem> getPricePoint(String slCode, String department, Integer offset, Integer pagesize);

	Integer getRecordPricePoint(String slCode, String department);

	List<CatMaster> getAllColors(Integer offset, Integer pagesize, String hl4Code);

	Integer getRecordAllColors(String hl4Code);

	List<CatMaster> getAllSizeByArticleCode(Integer offset, Integer pagesize, String hl4Code);

	Integer getRecordAllSizeByArticleCode(String hl4Code);

	List<CatMaster> getAllUniqueCodeByArticleCode(Integer offset, Integer pagesize, String hl4Code);

	Integer getRecordAllUniqueCodeByArticleCode(String hl4Code);

	List<CatMaster> getAllBrandsByArticleCode(Integer offset, Integer pagesize, String hl4Code);

	Integer getRecordAllBrandsByArticleCode(String hl4Code);

	List<CatMaster> getAllPatternsByArticleCode(Integer offset, Integer pagesize, String hl4Code);

	Integer getRecordAllPatternsByArticleCode(String hl4Code);

	List<CatMaster> getAllAssortmentsByArticleCode(Integer offset, Integer pagesize, String hl4Code);

	Integer getRecordAllAssortmentsByArticleCode(String hl4Code);

	List<ADMItem> getArticleData(String hl4Code);

	Integer calculateQtyForArticleData(ObjectNode node);

	Map<String, Double> calculateInTakeMarginForArticleData(ObjectNode node);

	List<DescriptionMaster> getAllSeasonByArticleCode(Integer offset, @Param("pagesize") Integer pagesize,
			String hl4Code);

	Integer getRecordAllSeasonByArticleCode(String hl4Code);

	List<DescriptionMaster> getAllFabricByArticleCode(Integer offset, Integer pagesize, String hl4Code);

	Integer getRecordAllFabricByArticleCode(String hl4Code);

	List<DescriptionMaster> getAllDarwinByArticleCode(Integer offset, Integer pagesize, String hl4Code);

	Integer getRecordAllDarwinByArticleCode(String hl4Code);

	List<DescriptionMaster> getAllWeavedByArticleCode(Integer offset, Integer pagesize, String hl4Code);

	Integer getRecordAllWeavedByArticleCode(String hl4Code);

	int validateItem(ItemValidation item);

	Integer insertPI(PurchaseIndent item);

	Integer insertPIDetails(PurchaseIndentDetail items);

	Integer insertItem(ADMItem item);

	Integer insertFinCharge(FinCharge charge);

	List<HsnGstData> getHsnGstDataByHsnSacCode(String hsnSacCode, String piDate);

	HsnGstData getMaxEffectiveDateByHsnSacCode(String hsnSacCode, String piDate);

	HsnGstData getMaxAmountFrom(String hsnSacCode, String piDate);

	HsnGstData getMinAmountFrom(String hsnSacCode, String piDate);

	HsnGstData getApplicableGstSlabForTransactionPrice(String hsnSacCode, String piDate, Double transactionPrice);

	List<PurtermMaster> getPurchaseTermDataForCGST(String tradeGrpCode);

	List<PurtermMaster> getPurchaseTermDataForIgst(String tradeGrpCode);

	PurtermDet getPurchaseTermDetailsByChgCode(Integer purchaseTermCode, BigDecimal chgCode);

	List<PurtermCharge> getPurchaseTermChargeDetailsByPurchaseTermCode(Integer purchaseTermCode);

	boolean compareSupplierAndClientGstin(String clientGstInNo, Integer supplierGstInStateCode);

	Map<String, Double> calulcateTotalForCgstSgstRates(Double subTotal, Double basicValue, Double cgstRate,
			Double sgstRate, Double cessRate, Double ch1, Double ch2, Double ch3, String lineItermSign,
			String formulae);

	Map<String, Double> calulcateTotalForIgstRates(Double subTotal, Double basicValue, Double igstRate, Double cessRate,
			Double ch1, Double ch2, String lineItermSign, String formulae);

	ObjectNode getGstSlabForIgstRates(HsnGstData applicableGstSlab);

	ObjectNode getPurtermChargeObjectNode(PurtermCharge purtermCharge);

	ObjectNode getPurtermDetObjectNode(PurtermDet purTermDet);

	Integer getGstRateByTaxName(String taxName);

	List<ADMItem> searchByArticlePricePoint(Integer offset, Integer pagesize, String slCode, String hL4Code,
			String department, String costPrice, String sellPrice);

	Integer recordSearchByArticlePricePoint(String slCode, String hL4Code, String department, String costPrice,
			String sellPrice);

	List<ADMItem> searchArticlePricePoint(Integer offset, Integer pagesize, String slCode, String department,
			String search);

	Integer recordArticlePricePoint(String slCode, String department, String search);

	List<Transporters> searchTransportersBySlcode(Integer offset, Integer pagesize, String slCode,
			String transporterName);

	Integer recordTransportersBySlcode(String slCode, String transporterName);

	List<CatMaster> searchAllColors(Integer offset, Integer pagesize, String hl4Code, String cname);

	Integer recordAllColors(String hl4Code, String cname);

	List<CatMaster> searchAllBrandsByArticleCode(Integer offset, Integer pagesize, String hl4Code, String cname);

	Integer recordAllBrandsByArticleCode(String hl4Code, String cname);

	List<CatMaster> searchAllPatternsByArticleCode(Integer offset, Integer pagesize, String hl4Code, String cname);

	Integer recordAllPatternsByArticleCode(String hl4Code, String cname);

	List<CatMaster> searchAllUniqueCodeByArticleCode(Integer offset, Integer pagesize, String hl4Code, String cname);

	Integer recordAllUniqueCodeByArticleCode(String hl4Code, String cname);

	List<CatMaster> searchAllAssortmentsByArticleCode(Integer offset, Integer pagesize, String hl4Code, String cname);

	Integer recordAllAssortmentsByArticleCode(String hl4Code, String cname);

	List<CatMaster> searchAllSizeByArticleCode(Integer offset, Integer pagesize, String hl4Code, String cname);

	Integer recordAllSizeByArticleCode(String hl4Code, String cname);

	List<DescriptionMaster> searchAllSeasonByArticleCode(Integer offset, Integer pagesize, String hl4Code,
			String cname);

	Integer recordAllSeasonByArticleCode(String hl4Code, String cname);

	List<DescriptionMaster> searchAllFabricByArticleCode(Integer offset, Integer pagesize, String hl4Code,
			String description);

	Integer recordAllFabricByArticleCode(String hl4Code, String description);

	List<DescriptionMaster> searchAllDarwinByArticleCode(Integer offset, Integer pagesize, String hl4code,
			String description);

	Integer recordAllDarwinByArticleCode(String hl4Code, String description);

	List<DescriptionMaster> searchAllWeavedByArticleCode(Integer offset, Integer pagesize, String hl4code,
			String description);

	Integer recordAllWeavedByArticleCode(String hl4Code, String description);

	List<HLevelData> searchByDistinctHierarachyLevels(Integer offset, Integer pagesize, String hl1name, String hl2name,
			String hl3name);

	Integer recordSearchByDistinctHierarachyLevels(String hl1name, String hl2name, String hl3name);

	PurchaseIndentDetail setPurchaseIndentDetail(PurchaseIndentDetail detail);

	ItemValidation setItemValidation(int i, PurchaseIndent pi);

	ADMItem setItem(int i, PurchaseIndent pi) throws Exception;

	List<String> findAll(Integer offset, Integer pagesize);

	Integer record(String orgId);

	List<SupplierData> searchBySupplierData(Integer offset, Integer pagesize, String slCode, String slName,
			String slAddr, String department);

	List<SupplierData> searchByCustomSupplierData(Integer offset, Integer pagesize, String slCode, String slName,
			String slAddr, String department);

	Integer recordsearchBySupplierData(String slCode, String slName, String slAddr, String department);

	Integer recordsearchByCustomSupplierData(String slCode, String slName, String slAddr, String department);

	List<SupplierData> searchSupplierData(String department, String siteCode, Integer offset, Integer pagesize,
			String search);

	List<SupplierData> searchCustomSupplierData(String department, Integer offset, Integer pagesize, String search);

	Integer recordSearchSupplierData(String department, String siteCode, String search);

	Integer recordSearchCustomSupplierData(String department, String search);

	List<HLevelData> searchAllDistinctHierarachyLevels(Integer offset, Integer pagesize, String search);

	Integer recordALLSearchDistinctHierarachyLevels(String search);

	String createPattern(String pattern);

	String getLastPattern();

	PurchaseTermData getPurchaseTermDataBySlCode(Integer slCode);

	List<PurtermMaster> getAllPurchaseTerms();

	PurchaseIndent getPIByPattern(String pattern);

	String buildReport(PurchaseIndent pi, HttpServletRequest request) throws SupplyMintException;

	String getPDFPathByPattern(String pattern);

	Integer updateStatus(String status, String pattern, String ipAddress, OffsetDateTime updationTime,
			String updatedBy);

	// int countColor(CatMaster catMaster);

	// int countSize(CatMaster catMaster);

	int countBrand(CatMaster catMaster);

	int countUniqueCode(CatMaster catMaster);

	int insertColor(CatMaster catMaster);

	int insertBrand(CatMaster catMaster);

	int insertSize(CatMaster catMaster);

	int insertUniqueCode(CatMaster catMaster);

	List<PurchaseIndent> filter(Integer offset, Integer pagesize, String pattern, String supplierName,
			String articleCode, String division, String status, String section, String department,
			String generatedDate);

	Integer filterRecord(String pattern, String supplierName, String articleCode, String division, String status,
			String section, String department, String generatedDate);

	List<PurchaseIndent> search(Integer offset, Integer pagesize, String search);

	Integer searchRecord(String search);

	List<Transporters> getAllTransporters(Integer offset, Integer pagesize);

	Integer getAllTransportersRecord();

	List<Transporters> getAllFilterTransporters(Integer offset, Integer pagesize, String transporterName,
			String transporterCode);

	Integer getAllFilterTransportersRecord(String transporterName, String transporterCode);

	List<Transporters> getAllSearchTransporters(Integer offset, Integer pagesize, String search);

	// Integer searchRecord(String search);

	PurchaseIndentProperty getPIProperty(int id);

	Integer getAllSearchTransportersRecord(String search);

	ObjectNode getIgstChargeRateNode(String taxName, String gstSlab, String slabEffectiveDate, Double igstRate,
			Double cessRate);

	ObjectNode getCgstChargeRateNode(String taxName, String gstSlab, String slabEffectiveDate, Double cgstRate,
			Double sgstRate, Double cessRate);

	int getExistanceUniqueCodeByArticleCode(String hl3Code, String categoryName);

	int getExistanceBrandsByArticleCode(String hl3Code, String categoryName);

	int getExistancePatternsByArticleCode(String hl3Code, String categoryName);

	int getExistanceAssortmentsByArticleCode(String hl3Code, String categoryName);

	int getExistanceSeasonByArticleCode(String hl3Code, String description);

	int getExistanceFabricByArticleCode(String hl3Code, String description);

	int getExistanceDarwinByArticleCode(String hl3Code, String description);

	int getExistanceWeavedByArticleCode(String hl3Code, String description);

	int getExistanceSizeByArticleCode(String hl3name, String categoryName);

	int getExistanceColorByArticleCode(String hl3Code, String categoryName);

	int getExistanceHierarachyLevels(String hl1name, String hl2name, String hl3name);

	String marginRule(String slCode, String articleCode, String tenantHashKey, String siteCode);

	String getActualOtbData(String articleCode, String mrp, String month, String year);

	ObjectNode getItemCatDescData(Integer pageNo, String hl3Code, String item, String hl3Name, String tenantHashKey);

	ObjectNode searchItemCatDescData(Integer pageNo, String hl3Code, String item, String hl3Name, String search,String tenantHashKey);

	List<DeptItemUDFSettings> getCatDescHeader(String hl3code);

	ObjectNode getCatDescHeaderNode(List<DeptItemUDFSettings> dept);

	PurchaseIndentMaster getPurchaseIndentMaster(PurchaseIndentMaster pim);

	List<PurchaseIndentHistory> getIndentHistory(Integer offset, Integer pagesize, String orgId);

	Integer countIndentHistoryFilter(String pattern, String supplierName, String articleCode, String division,
			String status, String section, String department, String generatedDate, String slCityName,
			String deliveryDateFrom, String deliveryDateTo, String desc2Code, String orgId);

	List<PurchaseIndentHistory> getIndentHistoryFilter(Integer offset, Integer pagesize, String pattern,
			String supplierName, String articleCode, String division, String status, String section, String department,
			String generatedDate, String slCityName, String deliveryDateFrom, String deliveryDateTo, String desc2Code,
			String orgId);

	Integer countIndentHistorySearch(String search, String orgId);

	List<PurchaseIndentHistory> getIndentHistorySearch(Integer offset, Integer pagesize, String search, String orgId);

	PurchaseIndent setPurchaseIndent(PurchaseIndent pi);

	boolean isPIQuanityValues(ObjectNode ob);

	PurchaseIndent getPIMain(int orderId);

	List<PurchaseIndentDetail> getPIDetail(int orderId, String orderDetailId);

	public Integer countHSNCode();

	public List<HsnData> getHSNCode(Integer offset, Integer pagesize);

	Integer countHSNCodeSearch(String search);

	public List<HsnData> getHSNCodeSearch(Integer offset, Integer pagesize, String search);

	public String getUDFExistenceStatus(String orgId);

	Integer getCustomRecordPricePoint(String department);

	List<ADMItem> getCustomPricePoint(String department, Integer offset, Integer pagesize);

	Integer recordCustomSearchByArticlePricePoint(String hL4Code, String department, String costPrice,
			String sellPrice);

	List<ADMItem> searchByCustomArticlePricePoint(Integer offset, Integer pagesize, String hL4Code, String department,
			String costPrice, String sellPrice);

	Integer recordCustomArticlePricePoint(String department, String search);

	List<ADMItem> searchCustomArticlePricePoint(Integer offset, Integer pagesize, String department, String search);

	String getSystemDefaultConfig(String key, String orgId);

	int getRecordSuppliersByCity(String cityName);

	List<SupplierData> searchByCustomSuppliersByCity(int offset, int pageSize, String slCode, String slName,
			String slAddr, String cityName);

	int recordsearchByCustomSuppliersByCity(String slCode, String slName, String slAddr, String cityName);

	List<SupplierData> getCustomSuppliersByCity(int offset, int pageSize, String cityName);

	int recordSearchCustomSuppliersByCity(String cityName, String search);

	List<SupplierData> searchCustomSuppliersByCity(String cityName, int offset, int pageSize, String search);

///////transporter code by city name
	int getAllTransportersRecordByCity(String cityName);

	List<Transporters> getAllTransportersByCity(int offset, int pageSize, String cityName);

	int getAllFilterTransportersRecordByCity(String transporterName, String transporterCode, String cityName);

	List<Transporters> getAllFilterTransportersByCity(int offset, int pageSize, String transporterName,
			String transporterCode, String cityName);

	int getAllSearchTransportersRecordByCity(String search, String cityName);

	List<Transporters> getAllSearchTransportersByCity(int offset, int pageSize, String search, String cityName);

	int insertPIDetailsForOthers(PurchaseIndentDetail purchaseIndentDetail);

	int insertFinChargeForOthers(FinCharge finCharge);

	List<PurchaseIndentDetail> getPIDetailForOthers(int parseInt, String orderDetailId);

	int recordDistinctArticle(String hl3Code);

	List<Map<String, String>> getByDepartmentCode(String hl3Code, int offset, int pageSize);

	int searchRecordDistinctArticle(String search);

	List<Map<String, String>> searchByDepartmentCode(int offset, int pageSize, String search);

}