package com.supplymint.layer.business.contract.administration;

import java.io.IOException;
import java.time.OffsetDateTime;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.http.ResponseEntity;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.supplymint.exception.SupplyMintException;
import com.supplymint.layer.data.tenant.administration.entity.ADMUser;
import com.supplymint.layer.web.model.AppResponse;

/**
 * This interface describes all service for ADM User
 * 
 * @author Prabhakar Srivastava
 * @Date 10 Jan 2019
 * @version 1.0
 */

public interface ADMUserService {

	/**
	 * This method describes for get data by specific id
	 * 
	 * @Table ADM_USERS
	 */
	ADMUser findById(Integer id);

	/**
	 * This method describes for Get data by specific user name
	 * 
	 * @Table ADM_USERS
	 */

	ADMUser findByUserName(String userName);

	/**
	 * This method describes for Get all data by specific enterprise user name
	 * according to pagination
	 * 
	 * @Table ADM_USERS
	 */

	List<ADMUser> findAll(Integer offset, Integer pagesize, String enterpriseUserName);

	/**
	 * This method describes for create one user record
	 * 
	 * @Table ADM_USERS
	 */

	int create(ADMUser users);

	/**
	 * This method describes for update one user record
	 * 
	 * @Table ADM_USERS
	 */

	int update(ADMUser users);

	/**
	 * This method describes for remove record by specific id
	 * 
	 * @Table ADM_USERS
	 */

	int delete(int id);

	/**
	 * This method describes for update status by specific user name
	 * 
	 * @Table ADM_USERS
	 */

	int updateStatus(String status, String userName, String ipAddress, OffsetDateTime updationTime, String updatedBy);

	/**
	 * This method describes for Get all record by specific access mode
	 * 
	 * @Table ADM_USERS
	 */
	List<ADMUser> getAccessmode(String accessmode);

	/**
	 * This method describes for Get record by specific partner enterprise id
	 * 
	 * @Table ADM_USERS
	 */

	List<ADMUser> getByEnterPriseId(int id);

	/**
	 * This method describes for convert Json String type to ADMUser Object type
	 * 
	 */

	ADMUser convert(String jsonString) throws JsonParseException, JsonMappingException, IOException;

	/**
	 * This method describes for get all ADM Users record * @Table ADM_USERS
	 */

	int record();

	/**
	 * This method describes getting all Search record for ADM USERS
	 * 
	 * @param Search
	 * @Table ADM_USERS
	 */

	int searchRecord(String search);

	/**
	 * This method describes getting all Search for ADM USERS according to
	 * pagination
	 * 
	 * @param Search
	 * @Table ADM_USERS
	 */

	List<ADMUser> searchAll(Integer offset, Integer pagesize, String search);

	/**
	 * This method describes getting all Filter for ADM USERS according to
	 * pagination
	 * 
	 * @param Search
	 * @Table ADM_USERS
	 */

	List<ADMUser> filter(ADMUser admUser);

	/**
	 * This method describes getting record by specific user name ADM USERS
	 * 
	 * @param Search
	 * @Table ADM_USERS
	 */

	ADMUser getByUserName(String name);

	/**
	 * This method describes getting record by specific user email ADM USERS
	 * 
	 * @param Search
	 * @Table ADM_USERS
	 */

	List<ADMUser> getByEmail(String email);

	/**
	 * This method describes getting user information form core DB using Lambda
	 * function
	 */

	ResponseEntity<AppResponse> sendUserDetailsOnRestAPI(ADMUser user, String uri) throws SupplyMintException;

	/**
	 * This method describes getting all number of record by specific user id
	 * 
	 * @param Search
	 * @Table ADM_USERROLES
	 */

	int countUserRoleUser(int id);

	List<ADMUser> getAllUser();

	int checkUsername(String username);

	List<ADMUser> checkUserExistance(ADMUser admUser);

	List<ADMUser> checkUserUpdateExistance(ADMUser admUser);

	List<ADMUser> filterUser(int offset, int pageSize, ADMUser admUser, String enterpriseUserName);

	Integer filterRecord(ADMUser admUser);

	List<ADMUser> getAllRecords(String userName);

	void generateUserListTOExcel(List<ADMUser> list, String filePath);

	Map<String, List<String>> getUserHeaders();

	ResponseEntity<AppResponse> getUserDetailsOnRestAPI(String userName, String uri) throws SupplyMintException;

	ADMUser getUserDetail(String userName);

	int updateByUserName(ADMUser admUser);

	int updateImageProfileUrl(String url, String userName);

	int updateTimeByUserName(String userName, Date updationTime);

}
