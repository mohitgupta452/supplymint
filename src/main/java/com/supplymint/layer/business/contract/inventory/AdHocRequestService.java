package com.supplymint.layer.business.contract.inventory;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Map;

import com.supplymint.layer.data.tenant.administration.entity.ADMItem;
import com.supplymint.layer.data.tenant.administration.entity.ADMSite;
import com.supplymint.layer.data.tenant.demand.entity.AdHocItemRequest;
import com.supplymint.layer.data.tenant.demand.entity.AdHocRequest;

public interface AdHocRequestService {

	int create(AdHocRequest adHocRequest);

	// List<String> itemCode(Integer offset, Integer pagesize);
	List<ADMItem> itemCode(Integer offset, Integer pagesize);

	Integer countItemCode();

	// List<String> searchItemCode(Integer offset, Integer pagesize, String search);
	List<ADMItem> searchItemCode(Integer offset, Integer pagesize, String search);

	Integer counterForSerarchItemCode(String search);

	List<ADMSite> getLocation();

	List<AdHocRequest> getAll(Integer offset, Integer pagesize);

	Integer count();

	List<AdHocRequest> searchAll(Integer offset, Integer pagesize, String search);

	Integer searchCount(String search);

	String getLastWorkOrder();

	List<AdHocItemRequest> getItem(String workOrder);

	int updateStatus(String workOrder, String ipAddress, OffsetDateTime updationTime, String status);

	List<AdHocRequest> getByworkOrder(String workOrder);

	List<AdHocRequest> getAllRecords();

	void generateAdHocRequestListTOExcel(List<AdHocRequest> list, String filePath);

	List<AdHocRequest> filter(Integer offset, Integer pagesize, String workOrder, String status, String site,
			String createdOn);

	Integer filterRecord(String workOrder, String status, String site, String createdOn);

	List<AdHocRequest> filterByDate(Integer offset, Integer pagesize, String createdOn);

	Integer getRecordFilterByDate(String createdOn);

	Integer deleteByWorkOrderAndItemCode(String workOrder, String itemData);

	List<AdHocRequest> validation(String workOrder, String itemData);

	List<AdHocRequest> getItemByworkOrder(String workOrder);

	Integer cancelWorkedOrder(String workeOrder, String ipAddress, OffsetDateTime updationTime);

	int countActiveStatus(String workOrder);

	Integer removedWorkedOrder(String workOrder, String ipAddress, OffsetDateTime updationTime);

	Map<String, List<String>> getAdHocRequestHeaders();

}
