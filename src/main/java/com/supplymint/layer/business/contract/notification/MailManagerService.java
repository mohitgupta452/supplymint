package com.supplymint.layer.business.contract.notification;

import java.util.List;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.supplymint.layer.data.notification.entity.DropDown;
import com.supplymint.layer.data.notification.entity.MailManager;
import com.supplymint.layer.data.notification.entity.MailManagerViewLog;

public interface MailManagerService {

	MailManager getById(Integer id);

	ObjectNode getAll(Integer pageNo, Integer type, String module, String subModule, String configuration,
			String search,HttpServletRequest request);

	Integer create(MailManager emailConfig,HttpServletRequest request);

	Integer update(MailManager emailConfig,HttpServletRequest request);

	Integer delete(int id);
	
	MailManager getByStatus(String templateName,String status,String orgId);
	
	int createEmailActivityLog(MailManagerViewLog mailManagerViewLog,String orgId);
	
	DropDown getAllModule(String moduleName);
	
	DropDown getAllSubModule(String moduleName);
	
	DropDown getAllConfiguration(String subModuleName);
	
	int getEmailStatus(String orgId);
	
	int updateEmailStatus(char active,String orgId);
	
	MailManager getEmailConfiguration(String moduleName,String subModuleName,String configurationProperty,String orgId);
	
	List<MailManager> getAllEmailData(String orgId);

}
