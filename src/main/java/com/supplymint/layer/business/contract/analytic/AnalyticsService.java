package com.supplymint.layer.business.contract.analytic;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.supplymint.layer.data.tenant.analytic.entity.AnalyticStoreProfile;
import com.supplymint.layer.data.tenant.analytic.entity.StoreProfileActualSaleData;
import com.supplymint.layer.data.tenant.analytic.entity.StoreProfileGraph;

public interface AnalyticsService {

	List<Map<String, String>> getSiteCode();

	AnalyticStoreProfile getStoreInfo(String siteCode) throws Exception;

	ObjectNode getSellThrough(String sellThroughType, String storeCode);

	ObjectNode getTopArticle();

//	Map<String, List<ObjectNode>> salesTrendGraph();

	List<StoreProfileActualSaleData> getSellProfitTrends();
	// Map<String, List<ObjectNode>> salesTrendGraph();

	Map<String, List<ObjectNode>> getSalesData(String flag);

	Map<String, ArrayNode> getFiveTopRankStoresArticleData();

	List<StoreProfileGraph> getSellData(String storeCode) throws Exception;

	List<StoreProfileActualSaleData> getStockInHand(String storeCode) throws Exception;

}
