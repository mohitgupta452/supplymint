package com.supplymint.layer.business.contract.demand;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.supplymint.exception.SupplyMintException;
import com.supplymint.layer.data.tenant.demand.entity.OTBPlanASD;
import com.supplymint.layer.data.tenant.demand.entity.OTBPlanDFD;
import com.supplymint.layer.data.tenant.demand.entity.OpenToBuy;

/**
 * This interface describes all service for OTB
 * @author Prabhakar Srivastava 
 * @author Bishnu Dutta
 * @Date 20 Mar 2019
 * @version 1.0
 */
public interface OTBService {

	/**
	 * This method describes getting OTB plan Info
	 * @param Plan Name
	 * @Table OTB_CREATE_PLAN
	 */
	OpenToBuy getByPlanName(String planName);

	/**
	 * This method describes create OTB plan
	 * @param OpenToBuy
	 * @Table OTB_CREATE_PLAN, OTB_USER_ASD, OTB_USER_DFD
	 */
	int create(OpenToBuy openToBuy) throws SupplyMintException, Exception;

	/**
	 * This method describes update OTB plan
	 * @param OpenToBuy
	 * @Table OTB_CREATE_PLAN, OTB_USER_ASD, OTB_USER_DFD
	 */
	int updateASDAndDFD(OpenToBuy openToBuy);

	/**
	 * This method describes delete OTB plan
	 * @param ID
	 * @Table OTB_CREATE_PLAN
	 */
	int delete(int otbId);

	/**
	 * This method describes get all available OTB plan
	 * @Table OTB_CREATE_PLAN
	 */
	List<OpenToBuy> getActiveOTBPlan();

	/**
	 * This method describes update active OTB plan status
	 * @param OpenToBuy
	 * @Table OTB_CREATE_PLAN
	 */
	int updateActivePlan(OpenToBuy openToBuy);

	/**
	 * This method describes update previous OTB plan status
	 * @param OpenToBuy
	 * @Table OTB_CREATE_PLAN
	 */
	int updatePreviousActivePlan(OpenToBuy openToBuy);

	/**
	 * This method describes remove active OTB plan by status
	 * @param OpenToBuy
	 * @Table OTB_CREATE_PLAN
	 */
	int removeActivePlan(OpenToBuy openToBuy);

	/**
	 * This method describes get OTB plan Info By ID
	 * @param Plan ID
	 * @Table OTB_CREATE_PLAN
	 */
	OpenToBuy getByPlanId(String planId);

	/**
	 * This method describes get active OTB plan graph 
	 * Assortment wise according to parameter
	 * @Table OTB_USER_ASD, OTB_USER_DFD
	 */
	ObjectNode getOTBPlan(Integer pageNo, String activePlan, String frequency, String startDate, String endDate,
			String planId);

	/**
	 * This method describes get active OTB plan graph 
	 * Total Assortment wise according to parameter
	 * @Table OTB_USER_ASD, OTB_USER_DFD
	 */
	ObjectNode getTotalOTBPlan(String activePlan, String frequency, String startDate, String endDate, String planId);

	/**
	 * This method describes export active OTB plan data 
	 * according to parameter
	 * @Table OTB_USER_ASD, OTB_USER_DFD
	 */
	String exportOTBPlan(String planId, String activePlan, String frequency, String startDate, String endDate,
			HttpServletRequest request);

	/**
	 * This method describes update active OTB planned sales 
	 * according to user
	 * @Table OTB_CREATE_PLAN, OTB_USER_ASD, OTB_USER_DFD
	 */
	int modificationDetails(JsonNode jsonNode);

	/**
	 * This method describes get OTB plan Info due to modify
	 * Planned Sales according to parameter
	 * @Table OTB_USER_ASD
	 */
	OTBPlanASD getByPlanIdAndAssortmentcodeASD(String planId, String assortmentCode, String billDate);

	/**
	 * This method describes get OTB plan Info due to modify
	 * Planned Sales according to parameter
	 * @Table OTB_USER_DFD
	 */
	OTBPlanDFD getByPlanIdAndAssortmentcodeDFD(String planId, String assortmentCode, String billDate);

	/**
	 * This method describes get OTB plan creation status
	 * @Table OTB_CREATE_PLAN
	 */
	String getOTBCreateStatus() throws SupplyMintException, InterruptedException;
	
	String getConfig(String key);

}
