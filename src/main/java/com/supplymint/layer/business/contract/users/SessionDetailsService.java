package com.supplymint.layer.business.contract.users;

import com.supplymint.layer.data.tenant.users.entity.SessionDetails;

public interface SessionDetailsService {

	int create(SessionDetails sessionDetails) throws Exception;

	SessionDetails getByUserName(String userName);

}
