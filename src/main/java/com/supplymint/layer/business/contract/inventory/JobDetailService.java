package com.supplymint.layer.business.contract.inventory;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.amazonaws.services.glue.model.GetJobRunResult;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.supplymint.exception.SupplyMintException;
import com.supplymint.layer.data.downloads.entity.DownloadRuleEngine;
import com.supplymint.layer.data.tenant.inventory.entity.EnterpriseSpecificOutput;
import com.supplymint.layer.data.tenant.inventory.entity.EnterpriseSpecificOutputNysaa;
import com.supplymint.layer.data.tenant.inventory.entity.HeadersSpecificOutput;
import com.supplymint.layer.data.tenant.inventory.entity.JobDetail;
import com.supplymint.layer.data.tenant.inventory.entity.JobTrigger;
import com.supplymint.layer.data.tenant.inventory.entity.RepAllocOutput;

/**
 * This interface describes all service for Rule Engine
 * 
 * @author Prabhakar Srivastava
 * @Date 05 Oct 2018
 * @version 1.0
 */
public interface JobDetailService {

	/**
	 * This method describes creating Job with Job ID NULL
	 * 
	 * @param JobDetail
	 * @Table JOBDETAILS
	 */
	int createJob(JobDetail jobDetail);

	/**
	 * This method describes creating Job Trigger for scheduling Job to run on
	 * schedule time
	 * 
	 * @param JobTrigger
	 * @Table JOBTRIGGERS
	 */
	int createTrigger(JobTrigger jobTrigger);

	/**
	 * This method describes Update Running Job Details Information When Job Run
	 * 
	 * @param JobDetail
	 * @Table JOBDETAILS
	 */
	int updateMetaData(JobDetail jobDetail);

	/**
	 * This method describes Stop Running Job From The End User on demand
	 * 
	 * @param JobDetail
	 * @Table JOBDETAILS
	 */
	JobDetail stopOnDemand();

	/**
	 * This method describes getting all record for Job Details History
	 * 
	 * @param Organization ID
	 * @Table JOBDETAILS
	 */
	int record(String orgId, String jobName);

	/**
	 * This method describes getting all Job Details history according to pagination
	 * 
	 * @Table JOBDETAILS
	 */
	List<JobDetail> getAll(Integer offset, Integer pageSize, String orgId, String jobName);

	/**
	 * This method describes get by Job ID
	 * 
	 * @param Job ID
	 * @Table JOBDETAILS
	 */
	JobDetail getByJobId(String jobId);

	/**
	 * This method describes get by Job Name with Last Engine True and Job is not
	 * Running
	 * 
	 * @param Job Name,Organization ID
	 * @Table JOBDETAILS
	 */
	JobDetail getByJobName(String jobName, String orgId);

	/**
	 * This method describes update Job Details By Job ID
	 * 
	 * @param Job ID
	 * @Table JOBDETAILS
	 */
	JobDetail updateByJobId(String updateByJobId);

	/**
	 * This method describes get Job Run State By Job Status
	 * 
	 * @param Status
	 * @Table JOBDETAILS
	 */
	List<JobDetail> getByState(String jobName, String jobRunState);

	/**
	 * This method describes get Last Engine Run Details with status LASTENGINE_RUN
	 * By Job Name
	 * 
	 * @param Job Name
	 * @Table JOBDETAILS
	 */
	JobDetail getLastEngineRunDetails(String jobName);

//	JobDetail getById(int id);

//	int updateTrigger(String triggerName, String jobName);

	/**
	 * This method describes get Job Trigger Details By Trigger ID
	 * 
	 * @param ID
	 * @Table JOBTRIGGERS
	 */
	JobTrigger getByTriggerId(int getByTriggerId);

//	int validate(String jobName);

	/**
	 * This method describes validate create Job Details With Job IS NULL
	 * 
	 * @param Job Name
	 * @Table JOBDETAILS
	 */
	@SuppressWarnings("rawtypes")
	List validatCreateJob(String jobName);

	/**
	 * This method describes update run state for Job Details
	 * 
	 * @param Job ID, Job Name, Job Run State
	 * @Table JOBDETAILS
	 */
	int updateState(String jobId, String jobName, String state);

	/**
	 * This method describes validate Trigger Details By Unique Trigger Name
	 * 
	 * @param Trigger Name
	 * @Table JOBTRIGGERS
	 */
	int validateTrigger(String triggerName);

	/**
	 * This method describes update Trigger Details
	 * 
	 * @param JobTrigger
	 * @Table JOBTRIGGERS
	 */
	int uTrigger(JobTrigger jobTrigger);

	/**
	 * This method describes get Trigger Details By Trigger Name
	 * 
	 * @param Trigger Name
	 * @Table JOBTRIGGERS
	 */
	JobTrigger getByTriggerName(String triggerName, String orgId);

	/**
	 * This method describes update Trigger Name
	 * 
	 * @param Job Name, Job Id
	 * @Table JOBDETAILS
	 */
	int updateTriggerName(String jobId, String jobName, String triggerName);

	/**
	 * This method describes getting all filter record for all Job History Details
	 * 
	 * @param Start Date, End Date
	 * @Table JOBDETAILS
	 */
	int filterRecord(String fromDate, String toDate, String orgId, String jobName);

	/**
	 * This method describes getting filter for all Job History Details according to
	 * pagination
	 * 
	 * @param Start Date, End Date
	 * @Table JOBDETAILS
	 */
	List<JobDetail> filter(Integer offset, Integer pagesize, String fromDate, String toDate, String orgId,
			String jobName);

	/**
	 * This method describes store Running Job Details From AWS glue
	 * 
	 * @param GetJobRunResult
	 */
	// @Author : - Manoj Singh
	JobDetail storeJobRunMetaData(GetJobRunResult runResultMetaDeta);

	/**
	 * This method describes Retrieve last Job Details while run state is not
	 * running
	 * 
	 * @param Job ID
	 * @Table JOBDETAILS
	 */
	String getLastEngineRun(String jobId);

	/**
	 * This method describes validate Job Details while run state is not running
	 * 
	 * @param Job ID
	 * @Table JOBDETAILS
	 */
	int validateJobName(String jobName);

	/**
	 * This method describes validate Job Details while run state is not running
	 * 
	 * @param Job ID
	 * @Table JOBDETAILS
	 */
	int updateStatus(JobDetail jobDetail);

	/**
	 * This method describes update next schedule for Active AWS Job
	 * 
	 * @param Next Schedule, TriggerName
	 * @Table JOBTRIGGERS
	 */
	int updateNextSchedule(String nextSchedule, String triggerName);

	/**
	 * This method describes get last succeeded Job Details
	 * 
	 * @param Job Name
	 * @Table JOBDETAILS
	 */
	JobDetail getLastSummary(String jobName, String orgId);

	/**
	 * This method describes get update previous Job Details SUMMARY FALSE
	 * 
	 * @param JobDetail
	 * @Table JOBDETAILS
	 */
	int updateSummary(JobDetail jobDetail);

	/**
	 * This method describes validate count STATUS to TRUE
	 * 
	 * @param JobDetail
	 * @Table JOBDETAILS
	 */
	int validateSummaryUpdate(JobDetail jobDetail);

	/**
	 * This method describes validate Status LASTENGINE_RUN
	 * 
	 * @param Job ID
	 * @Table JOBDETAILS
	 */
	int validateRow(String jobId);

	/**
	 * This method describes getting all filter record for same date Job Details
	 * History
	 * 
	 * @param End Date
	 * @Table JOBDETAILS
	 */
	Integer filterSameDateRecord(String toDate, String orgId, String jobName);

	/**
	 * This method describes getting all filter for same date Job Details History
	 * according to pagination
	 * 
	 * @param End Date
	 * @Table JOBDETAILS
	 */
	List<JobDetail> filterSameDate(Integer offset, Integer pagesize, String toDate, String orgId, String jobName);

	/**
	 * This method describes creating Job with Job ID NULL From Run Details API
	 * 
	 * @param JobDetail
	 * @Table JOBDETAILS
	 */
	boolean createJobs(JobDetail jobDetail);

	/**
	 * This method describes getting distinct store count for Succeeded Job Summary
	 * 
	 * @Table REP_ALLOC_OUTPUT
	 */
	int globalStoreCount(String tableName);

	/**
	 * This method describes getting distinct item count for Succeeded Job Summary
	 * 
	 * @Table REP_ALLOC_OUTPUT
	 */
	int globalitemCount(String tableName);

	/**
	 * This method describes getting distinct transfer order count for Succeeded Job
	 * Summary
	 * 
	 * @Table REP_ALLOC_OUTPUT
	 */
	String globaltotalCount(String tableName);

	/**
	 * This method describes getting update all count for Succeeded Job Summary
	 * 
	 * @param JobDetail
	 * @Table REP_ALLOC_OUTPUT
	 */
	int globalupdateCount(JobDetail jobDetail);

	// get summary details method

	/**
	 * This method describes getting all record for last Succeeded Job Summary
	 * Details
	 * 
	 * @Table REP_ALLOC_OUTPUT
	 */
	int globalrecordRAO(String tableName);

	/**
	 * This method describes getting all last Succeeded Job Summary Details
	 * according to pagination
	 * 
	 * @Table REP_ALLOC_OUTPUT
	 */
	List<HeadersSpecificOutput> globalAllRAO(Integer offset, Integer pagesize, String tableName);

	/**
	 * This method describes getting all filter record for last Succeeded Job
	 * Details Summary
	 * 
	 * @param label
	 * @param Store Code
	 * @Table REP_ALLOC_OUTPUT
	 */
	int globalSummaryFilterRecord(String storeCode, String tableName);

	/**
	 * This method describes getting all filter for last Succeeded Job Details
	 * Summary according to pagination
	 * 
	 * @param Store Code
	 * @Table REP_ALLOC_OUTPUT
	 */
	List<HeadersSpecificOutput> globalSummaryFilter(Integer offset, Integer pagesize, String storeCode,
			String tableName);

	/**
	 * This method describes getting all Store Code last Succeeded Job Details
	 * Summary
	 * 
	 * @Table REP_ALLOC_OUTPUT
	 */
	List<String> globalAllStoreCode(String tableName);

	/**
	 * This method describes getting all warehouse Code last Succeeded Job Details
	 * Summary
	 * 
	 * @Table REP_ALLOC_OUTPUT
	 */
	List<String> getWhCode(String tableName);

	/**
	 * This method describes retrieve transfer order data for Excel last Succeeded
	 * Job Details Summary
	 * 
	 * @param warehouse code
	 * @Table REP_ALLOC_OUTPUT
	 */
	List<DownloadRuleEngine> downloadExcelSummary(String whCode);

	/**
	 * This method describes retrieve transfer order data for Excel last Succeeded
	 * Job Details Summary EnterPrise Specific
	 * 
	 * @param warehouse code
	 * @Table REP_ALLOC_OUTPUT
	 */
	List<EnterpriseSpecificOutput> downloadExcelSummaryForSkecters(String whCode);

	/**
	 * This method describes retrieve transfer order data for Excel last Succeeded
	 * Job Details Summary EnterPrise Specific
	 * 
	 * @param warehouse code
	 * @Table REP_ALLOC_OUTPUT
	 */
	List<EnterpriseSpecificOutputNysaa> downloadExcelSummaryForNysaa1();

	/**
	 * This method describes retrieve transfer order data for Excel last Succeeded
	 * Job Details Summary EnterPrise Specific
	 * 
	 * @param warehouse code
	 * @Table REP_ALLOC_OUTPUT
	 */
	List<EnterpriseSpecificOutputNysaa> downloadExcelSummaryForNysaa2();

	/**
	 * This method describes retrieve transfer order data for Excel last Succeeded
	 * Job Details Summary EnterPrise Specific
	 * 
	 * @param warehouse code
	 * @Table REP_ALLOC_OUTPUT
	 */
	int updateFileName(String folderName, String argument, String jobId);

	/**
	 * This method describes update current LASTENGINE to TRUE to current Job ID
	 * 
	 * @param Job Name, Job ID
	 * @Table JOBDETAILS
	 */
	int updateLastEngine(String jobId, String jobName);

	/**
	 * This method describes update previous LASTENGINE to FALSE to Current Job ID
	 * 
	 * @param Job ID
	 * @Table JOBDETAILS
	 */
	int updatePreviousEngine(String jobId);

	/**
	 * This method describes update Transfer Order Bucket Key
	 * 
	 * @param mailCOunt
	 * @param Job       ID
	 * @Table JOBDETAILS
	 */
	int updateTransferOrderKey(String jobId, String bucketKey, int mailCOunt);

	/**
	 * This method describes get Previous Job ID
	 * 
	 * @Table JOBDETAILS
	 */
	String getPreviousJobId();

	/**
	 * This method describes update previous LASTENGINE to FALSE Except Current Job
	 * ID
	 * 
	 * @param Job ID
	 * @Table JOBDETAILS
	 */
	int updatePreviousLastEngine(String jobId, String jobName);

	/**
	 * This method describes update current LASTENGINE to TRUE By Current Job ID
	 * 
	 * @param Job ID
	 * @Table JOBDETAILS
	 */
	int updateCurrentLastEngine(String jobId);

	/**
	 * This method describes update replenishment Date and Duration By Job ID
	 * 
	 * @param Job ID
	 * @Table JOBDETAILS
	 */
	int updateEndTimeWithRepDate(String jobId, Date endDate, String duration);

	/**
	 * This method describes get last 5 Succeeded Job Duration for Job Details Run
	 * On Demand Dialer
	 * 
	 * @param Job ID
	 * @Table JOBDETAILS
	 */
	List<String> fiveSucceededTime();

	/**
	 * This method describes get fixed header for Transfer Order according to
	 * parameter
	 * 
	 * @param string
	 * @Table HEADER_CONFIG_LOG
	 */
	ObjectNode getCustomHeaders(String isDefault, String attributeType, String displayName, String orgId)
			throws Exception;

	/**
	 * This method describes get user header for Transfer Order according to
	 * parameter
	 * 
	 * @Table HEADER_CONFIG_LOG
	 */
	public Map<String, List<String>> getHeadersData() throws Exception;

	/**
	 * This method describes retrieve transfer order data for Excel last Succeeded
	 * Job Details Summary
	 * 
	 * @param warehouse code
	 * @Table REP_ALLOC_OUTPUT
	 */
	List<HeadersSpecificOutput> getDownloadExcelSummaryData(String whCode, String tableName);

	/**
	 * This method describes updateLast Engine status
	 * 
	 * @Table JOBDETAILS
	 */
	int updateLastEngineRun();

	int updatePreviousSchedule(String previousNextSchedule, String triggerName);

	void updateMailStatus(String jobId);

	int globalrecordReq(String tableName);

	List<HeadersSpecificOutput> globalAllReq(int offset, int pageSize, String tableName);

	List<HeadersSpecificOutput> globalSummaryFilterReq(int offset, int pageSize, String storeCode, String tableName);

	int globalSummaryFilterRecordReq(String storeCode, String tableName);

	String getLabelOfStoreCode(String value, String displayName);

	int updateOrgId(String orgId, String jobId);

	ObjectNode getFiveTriggers(String orgId, int enterpriseId, String orgName);

	int countTriggerTest(String orgId, int enterpriseId);

	int deleteTrigger(String triggerName, String orgId);

	String getPresentTriggers(int maxTrigger, String triggerName, String orgId, int enterpriseId);

	Date getPastScheduleTime(String orgId, int enterpriseId);

	boolean sendMail(String generation, String state, String enterprise, String jobId, String orgId);

	int updateMailCounter(String jobId, int mailCount, String orgId);

	JobDetail getJobDetailByJobId(String jobId, String orgId);

}
