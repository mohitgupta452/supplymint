package com.supplymint.layer.business.contract.administration;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Map;

import com.supplymint.layer.data.tenant.administration.entity.ADMSiteMap;

public interface ADMSiteMapService {

	/**
	 * 
	 * @return POJO data as per @param
	 */
	ADMSiteMap getById(Integer id);

	/**
	 * 
	 * @return List of data as per  @param
	 */
	List<ADMSiteMap> getAll(Integer offset, Integer pageSize);

	/**
	 * 
	 * @return count of creating the data as per @param
	 */
	Integer create(ADMSiteMap aDMSite);

	/**
	 * 
	 * @return count of updating the data as per @param 
	 */
	Integer update(ADMSiteMap aDMSite);

	/**
	 *  
	 * @return delete the data as per @param
	 */
	Integer delete(int id);

	/**
	 * 
	 * @return get ADMSiteMap details as per @param
	 */
	List<ADMSiteMap> getStatus(String status);

	/**
	 * 
	 * @return count of updating the data as per @param
	 */
	Integer updateStatus(String status, Integer sid, String ipAddress, OffsetDateTime updationTime);

	/**
	 * 
	 * @return list of ADMSiteMap detail as per @param
	 */
	List<ADMSiteMap> getDetailsOnFromIdtoToId(Integer fromid, Integer toid);

	/**
	 *  
	 * @return List of ADMsiteMap detail as per @param
	 */
	List<ADMSiteMap> getDetailsOnFromSiteId(Integer fromsiteid);

	/**
	 * 
	 * @return count of all present record.
	 */
	Integer record();

	/**
	 * 
	 * @return count of search record in data.
	 */
	int searchRecord(String search);

	/**
	 * 
	 * @return List of searched record as per @param.
	 */
	List<ADMSiteMap> searchAll(Integer offset, Integer pagesize, String search);

	/**
	 * 
	 * @return List of ADM site map records.
	 */
	List<ADMSiteMap> getAllRecords();

	/**
	 * 
	 * method to generate excel file with the help of @param.
	 */
	void generateSiteMapListTOExcel(List<ADMSiteMap> list, String filePath);

	Map<String, List<String>> getSiteMapHeaders();

	/**
	 * 
	 * @return List of filter record as per @param.
	 */
	List<ADMSiteMap> filter(Integer offset, Integer pagesize, String fromSiteName, String toSiteName, String startDate,
			String tptLeadTime, String status);

	/**
	 *
	 * @return count of filtered record as per @param.
	 */
	Integer filterRecord(String fromSiteName, String toSiteName, String startDate, String tptLeadTime, String status);

}
