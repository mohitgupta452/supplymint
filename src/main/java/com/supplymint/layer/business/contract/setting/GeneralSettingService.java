package com.supplymint.layer.business.contract.setting;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.supplymint.layer.data.setting.entity.GeneralSetting;

public interface GeneralSettingService {
	
	int create(GeneralSetting generalSetting,HttpServletRequest request) throws Exception;
	
	Map<String,String> getAll();
	
//	int update(GeneralSetting generalSetting) throws Exception;

	int updatevalue(Object b);

	String getAll1();

}
