package com.supplymint.layer.business.contract.inventory;

import com.supplymint.layer.data.tenant.inventory.entity.Allocation;

public interface AllocationService {

	Integer insert(Allocation allocation);

	Integer deleteById(Integer id);

}
