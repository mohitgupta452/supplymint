package com.supplymint.layer.business.contract.administration;

import java.util.List;
import java.util.Map;

import com.supplymint.layer.data.tenant.administration.entity.ADMSite;
import com.supplymint.layer.data.tenant.administration.entity.ADMSiteDetail;

public interface ADMSiteService {

	/**
	 * 
	 * @param id
	 * @return site data according to POJO
	 */
	ADMSite getById(Integer id);

	/**
	 * 
	 * @param offset
	 * @param pagesize
	 * @return all data according to page on UI.
	 */
	List<ADMSite> findAll(Integer offset, Integer pagesize);

	/**
	 * 
	 * @param aDMSite
	 * @return //create the site data
	 */
	Integer create(ADMSite aDMSite);

	/**
	 * 
	 * @param aDMSite
	 * @return //update the site data
	 */
	Integer udpate(ADMSite aDMSite);

	/**
	 * 
	 * @param id
	 * @return delete the site data according to ID
	 */
	Integer delete(int id);

	/**
	 * 
	 * @param admSiteDetail
	 * @return count of creating ADM site detail data
	 */
	Integer createSiteDetail(ADMSiteDetail admSiteDetail);

	/**
	 * 
	 * @param admSiteDetail
	 * @return count of updating ADM site detail data
	 */
	Integer udpateSiteDetail(ADMSiteDetail admSiteDetail);

	/**
	 * 
	 * @param id
	 * @return count of deleting ADM site detail data
	 */
	Integer deleteSiteDetail(int id);

	/**
	 * 
	 * @return count of record
	 */
	Integer record();

	/**
	 * 
	 * @return List of all ADM site
	 */
	List<ADMSite> getAllSite();

	/**
	 * 
	 * @param offset
	 * @param pageSize
	 * @param admSite
	 * @return List of filter data according to @param
	 */
	List<ADMSite> filterSite(int offset, int pageSize, ADMSite admSite);

	/**
	 * 
	 * @param offset
	 * @param pagesize
	 * @param search
	 * @return List of all searched data according to @param
	 */
	List<ADMSite> searchAll(Integer offset, Integer pagesize, String search);

	/**
	 * 
	 * @param search
	 * @return count of search record
	 */
	int searchRecord(String search);

	/**
	 * 
	 * @param id
	 * @return count of deleting site data
	 */
	Integer deleteSiteMap(int id);

	/**
	 * 
	 * @param admSiteDetail
	 * @return List of checking data as its Existence
	 */
	List<ADMSiteDetail> checkSiteExistance(ADMSiteDetail admSiteDetail);

	/**
	 * 
	 * @param admSiteDetail
	 * @return List of update data in ADM site detail
	 */
	List<ADMSiteDetail> checkSiteUpdateExistance(ADMSiteDetail admSiteDetail);

	/**
	 * 
	 * @param admSite
	 * @return count of presenting data in ADM site
	 */
	Integer filterRecord(ADMSite admSite);

	/**
	 * 
	 * @return List of all ADM site record
	 */
	List<ADMSite> getAllRecords();

	/**
	 * 
	 * @param list
	 * @param filePath use to generate site excel file
	 */
	void generateSiteListTOExcel(List<ADMSite> list, String filePath);

	Map<String, List<String>> getSiteHeaders();

	int batchSiteInsert(List<ADMSite> listAdmSite);

	int countPOSite(String username);

	List<Map<String, String>> getPOSite(String username, int offset, int pagesize);

	int countSearchPOSite(String username, String search);

	List<Map<String, String>> getSearchPOSite(String username, String search, int offset, int pagesize);

	Map<String, String> getDefaultPOSite(String username);

}
