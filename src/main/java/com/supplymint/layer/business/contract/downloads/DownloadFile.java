package com.supplymint.layer.business.contract.downloads;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

public interface DownloadFile {
	
	public Map<String, String> generateExcelMap(String fileType ,String moduleName,HttpServletRequest request);
	
	

}
