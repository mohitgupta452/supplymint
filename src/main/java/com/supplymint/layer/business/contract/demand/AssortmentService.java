package com.supplymint.layer.business.contract.demand;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.supplymint.layer.data.tenant.administration.entity.ADMItem;
import com.supplymint.layer.data.tenant.demand.entity.Assortment;
import com.supplymint.layer.data.tenant.demand.entity.AssortmentViewLog;
import com.supplymint.layer.data.tenant.demand.entity.MViewRefreshHistory;

/**
 * This interface describes all service for Assortment
 * @author Prabhakar Srivastava
 * @Date 15 Dec 2018
 * @version 2.0
 */
public interface AssortmentService {
	
	/**
	 * This method describes getting all record of Division 
	 * @Table INVITEM
	 */
	int getHl1Record();

	/**
	 * This method describes getting all Division according to pagination 
	 * @Table INVITEM
	 */
	List<ADMItem> getHL1Name(int offset,int pageSize);
	
	/**
	 * This method describes getting all search record for Division
	 * @param Search
	 * @Table INVITEM
	 */
	int getHl1SearchRecord(String search);
	
	/**
	 * This method describes getting all search for Division according to pagination 
	 * @Table INVITEM
	 */
	List<ADMItem> getHl1Search(int offset,int pageSize,String search);
	
	/**
	 * This method describes getting all record of Section
	 * @param Division 
	 * @Table INVITEM
	 */
	int getHl2Record(String hl1Name);

	/**
	 * This method describes getting all Section according to pagination
	 * @param Division 
	 * @Table INVITEM
	 */
	List<ADMItem> getHL2Name(int offset,int pageSize,String hl1Name);
	
	/**
	 * This method describes getting all search record for Section 
	 * @param Division 
	 * @Table INVITEM
	 */
	int getHl2SearchRecord(String hl1Name,String search);
	
	/**
	 * This method describes getting all search for Section according to pagination 
	 * @param Division
	 * @Table INVITEM
	 */
	List<ADMItem> getHl2Search(int offset,int pageSize,String hl1Name,String search);
	
	/**
	 * This method describes getting all record of Department
	 * @param Division, Section
	 * @Table INVITEM
	 */
	int getHl3Record(String hl1Name, String hl2Name);

	/**
	 * This method describes getting all Department according to pagination
	 * @param Division, Section
	 * @Table INVITEM
	 */
	List<ADMItem> getHL3Name(int offset,int pageSize,String hl1Name, String hl2Name);
	
	/**
	 * This method describes getting all search record for Department 
	 * @param Division, Section
	 * @Table INVITEM
	 */
	int getHl3SearchRecord(String hl1Name,String hl2Name,String search);
	
	/**
	 * This method describes getting all search for Department according to pagination 
	 * @param Division, Section
	 * @Table INVITEM
	 */
	List<ADMItem> getHl3Search(int offset,int pageSize,String hl1Name,String hl2Name,String search);
	
	/**
	 * This method describes getting all record of Article
	 * @param Division, Section, Department
	 * @Table INVITEM
	 */
	int getHl4Record(Assortment container);

	/**
	 * This method describes getting all Article according to pagination
	 * @param Division, Section, Department
	 * @Table INVITEM
	 */
	List<ADMItem> getHL4Name(Assortment container);
	
	/**
	 * This method describes getting all search record for Article 
	 * @param Division, Section, Department
	 * @Table INVITEM
	 */
	int getHl4SearchRecord(Assortment container);
	
	/**
	 * This method describes getting all search for Article according to pagination 
	 * @param Division, Section, Department
	 * @Table INVITEM
	 */
	List<ADMItem> getHl4Search(Assortment container);

	/**
	 * This method describes getting all Item count according to parameter
	 * @param Division, Section, Department, Article
	 * @Table INVITEM
	 */
	int getMRP(Assortment container);
	
	/**
	 * This method describes delete assortmentCode according to matching ICODE
	 * @param Division, Section, Department, Article
	 * @Table ASSORTMENT, INVITEM
	 */
	int deleteAssortmentCode(Assortment container);

	/**
	 * This method describes create assortment according to user
	 * @param Division, Section, Department, Article, USER ASSORTMENTCODE
	 * @Table ASSORTMENT
	 */
	int createAssortmentCode(Assortment container);

	/**
	 * This method describes getting all record of Final Assortment
	 * @Table FINAL_ASSORTMENT
	 */
	int assortmentRecord();
	
	/**
	 * This method describes getting all Final Assortment according to pagination 
	 * @Table FINAL_ASSORTMENT
	 */
	List<Assortment> getAllAssortment(Integer offset, Integer pagesize);
	
	/**
	 * This method describes getting all search record for Final Assortment 
	 * @param Search
	 * @Table FINAL_ASSORTMENT
	 */
	int searchAssortmentRecord(String search);
	
	/**
	 * This method describes getting all search for Final Assortment 
	 * according to pagination 
	 * @param Search
	 * @Table FINAL_ASSORTMENT
	 */
	List<Assortment> searchAssortment(Integer offset, Integer pageSize, String search);
	
	/**
	 * This method describes create Assortment Activity log according to user
	 * @param Division, Section, Department, Article, AssortmentCode ,etc.
	 * @Table ASSORTMENT_VIEWLOG
	 */
	int createLog(AssortmentViewLog viewLog);

	/**
	 * This method describes getting all record of Assortment Activity log
	 * @Table ASSORTMENT_VIEWLOG
	 */
	int getAllHistotyCount();

	/**
	 * This method describes getting all Assortment Activity log
	 * according to pagination 
	 * @Table ASSORTMENT_VIEWLOG
	 */
	List<AssortmentViewLog> getAllHistoty(Assortment container);

	/**
	 * This method describes getting all filter record of Assortment Activity log
	 * @param itemCount, startDate, endDate, createdOn, status 
	 * @Table ASSORTMENT_VIEWLOG
	 */
	int filterHistoryRecord(Assortment container);

	/**
	 * This method describes getting all filter for Assortment Activity log 
	 * according to pagination
	 * @param itemCount, startDate, endDate, createdOn, status 
	 * @Table ASSORTMENT_VIEWLOG
	 */
	List<AssortmentViewLog> filterHistory(Assortment container);

	/**
	 * This method describes getting all search record for Assortment Activity log 
	 * @param Search
	 * @Table ASSORTMENT_VIEWLOG
	 */
	int searchHistoryRecord(Assortment container);

	/**
	 * This method describes getting all search for Assortment Activity log 
	 * according to pagination 
	 * @param Search
	 * @Table ASSORTMENT_VIEWLOG
	 */
	List<AssortmentViewLog> searchHistory(Assortment container);
	
	/**
	 * This method describes checking condition for existence of MV 
	 * according to parameter
	 * @param Module, Sub Module
	 * @Table MVIEW_REFRESH_HISTORY
	 */
	int isExistenceMViewWithModule(String module,String subModule);
	
	/**
	 * This method describes checking creating MV History according to parameter
	 * @param MViewRefreshHistory Object
	 * @Table MVIEW_REFRESH_HISTORY
	 */
	int createMViewRefresh(MViewRefreshHistory assortmentMViewRefresh);

	/**
	 * This method describes refreshing all MV after creating Assortment 
	 * @MaterlizedView DEFAULT_ASSORTMENT, FINAL_ASSORTMENT
	 */
	void refreshAssortmentMV();
	
	/**
	 * This method describes update materialized refresh status
	 * @param MViewRefreshHistory Object
	 * @Table MVIEW_REFRESH_HISTORY
	 */
	int updateMViewRefreshHistory(MViewRefreshHistory assortmentMViewRefresh);
	
	/**
	 * This method describes getting default assortment according to parameter
	 * @param defaultAssortment key
	 * @Table SYSTEM_DEFAULT_CONFIG
	 */
	String getDefaultAssortment(String defaultAssortment);
	
	/**
	 * This method describes getting MV refresh status
	 * @Table MVIEW_REFRESH_HISTORY
	 */
	String getMVStatus();
	
	/**
	 * This method describes update MV status viewed
	 * @Table MVIEW_REFRESH_HISTORY
	 */
	int viewedAssortmentMessage();
	
	/**
	 * This method describes get fetching data status
	 * @Table DEMAND_PLANNING
	 */
	String getAssortmentStatus();
	
	/**
	 * This method describes update forecast status succeeded
	 * @Table MVIEW_REFRESH_HISTORY
	 */
	int updateMVForecastStatus(String module,String subModule);
	
	/**
	 * This method describes get forecast read status
	 * @Table MVIEW_REFRESH_HISTORY
	 */
	String forecastReadSatus();
	
	/**
	 * This method describes get all drop down of hierarchy level according to user 
	 * @Table SYSTEM_DEFAULT_CONFIG
	 */
	ObjectNode getAllHlevelDropDown() throws Exception;
	
	/**
	 * This method describes create view for branded enterprise 
	 * @Table BRANDED_ASSORTMENT_MONTHLY, BRANDED_ASSORTMENT_WEEKLY
	 * DEMAND_FORECAST_MONTHLY_FINAL, DEMAND_FORECAST_WEEKLY_FINAL
	 */
	void createMainHierarchyViewForForecast(String forecastMainHierarchy,String forecastSubHierarchy,String enterpriseType);
	

}
