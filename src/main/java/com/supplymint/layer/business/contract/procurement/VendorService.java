package com.supplymint.layer.business.contract.procurement;

import java.util.List;

import com.supplymint.layer.data.tenant.procurement.entity.Vendor;

public interface VendorService {
	
	public Vendor getById(Integer id);
	
	public List<Vendor> getAll(Integer offset,Integer pageSize);
	
	public Integer create(Vendor vendor);
	
	public Integer update(Vendor vendor);
	
	public Integer delete(int id);
	
	public List<Vendor> getByOrganisationName(String ename);
	
	public Vendor getVendorName(String name);
	
	public Vendor getVendorCode(String code);
	
	public int record();
	
	public int isValid(Vendor vendor);

}
