package com.supplymint.layer.business.contract.administration;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Map;

import org.springframework.http.ResponseEntity;

import com.supplymint.exception.SupplyMintException;
import com.supplymint.layer.data.tenant.administration.entity.ADMUserRole;
import com.supplymint.layer.web.model.AppResponse;

public interface ADMUserRoleService {

	ADMUserRole findById(Integer id);

	List<ADMUserRole> findAll(Integer offset, Integer pagesize, String enterpriseUserName);

	Integer create(ADMUserRole admUserRole);

	Integer update(ADMUserRole admUserRole);

	Integer delete(Integer id);

	List<ADMUserRole> getStatus(String status);

	Integer updateStatus(String status, Integer uid, String ipAddress, OffsetDateTime updationTime);

	List<ADMUserRole> getOnRole(String name);

	List<ADMUserRole> getUserById(Integer id);

	List<ADMUserRole> searchAll(Integer offset, Integer pagesize, String search);

	int searchRecord(String search);

	int deleteUser(int id);

	Integer record();

	List<ADMUserRole> filter(ADMUserRole admUserRole);

	List<ADMUserRole> filterUserRole(int offset, int pageSize, ADMUserRole admUserRole);

	Integer filterRecord(ADMUserRole admUserRole);

	List<ADMUserRole> getAllRecords(String userName);

	ADMUserRole findByUserName(String userName);

	Integer updateStatusByUserName(ADMUserRole admUserRole);

	void generateUserRoleListTOExcel(List<ADMUserRole> list, String filePath);

	Map<String, List<String>> getUserRoleHeaders();

	ResponseEntity<AppResponse> sendUserRolesDetailsOnRestAPI(ADMUserRole user, String uri) throws SupplyMintException;

}
