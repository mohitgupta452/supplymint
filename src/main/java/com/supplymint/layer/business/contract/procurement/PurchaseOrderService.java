package com.supplymint.layer.business.contract.procurement;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.dao.DataAccessException;
import org.springframework.http.ResponseEntity;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.supplymint.exception.SupplyMintException;
import com.supplymint.layer.data.tenant.administration.entity.ADMItem;
import com.supplymint.layer.data.tenant.administration.entity.PurchaseOrderPricePoint;
import com.supplymint.layer.data.tenant.entity.procurement.config.DeptItemUDFMappings;
import com.supplymint.layer.data.tenant.procurement.entity.Category;
import com.supplymint.layer.data.tenant.procurement.entity.DeptItemUDFSettings;
import com.supplymint.layer.data.tenant.procurement.entity.DeptSizeData;
import com.supplymint.layer.data.tenant.procurement.entity.IndtCatDescUDFList;
import com.supplymint.layer.data.tenant.procurement.entity.InvSetUDF;
import com.supplymint.layer.data.tenant.procurement.entity.ProcurementUploadSummary;
import com.supplymint.layer.data.tenant.procurement.entity.PurchaseIndent;
import com.supplymint.layer.data.tenant.procurement.entity.PurchaseIndentHistory;
import com.supplymint.layer.data.tenant.procurement.entity.PurchaseIndentMaster;
import com.supplymint.layer.data.tenant.procurement.entity.PurchaseOrder;
import com.supplymint.layer.data.tenant.procurement.entity.PurchaseOrderAudit;
import com.supplymint.layer.data.tenant.procurement.entity.PurchaseOrderChild;
import com.supplymint.layer.data.tenant.procurement.entity.PurchaseOrderConfiguration;
import com.supplymint.layer.data.tenant.procurement.entity.PurchaseOrderExportData;
import com.supplymint.layer.data.tenant.procurement.entity.PurchaseOrderParent;
import com.supplymint.layer.data.tenant.procurement.entity.PurchaseOrderTemp;
import com.supplymint.layer.data.tenant.procurement.entity.TempPurchaseOrderChild;
import com.supplymint.layer.data.tenant.procurement.entity.UDF_SettingsMaster;
import com.supplymint.layer.web.model.AppResponse;

public interface PurchaseOrderService {

	List<String> getAllPIPattern(Integer offset, Integer pagesize);

	Integer getCountAllPIPattern();

	List<PurchaseOrderPricePoint> getArticleHierarchyLevelData(Integer offset, Integer pagesize);

	Integer getRecordArticleHierarchyLevelData();

	List<PurchaseOrderPricePoint> searchAllArticleHierarchyLevelData(Integer offset, Integer pagesize, String search);

	Integer getRecordForSearchAllArticleHierarchyLevelData(String search);

	List<PurchaseOrderPricePoint> filterArticleHierarchyLevelData(Integer offset, Integer pagesize, String hl4Code,
			String hl4Name, String hl1Name, String hl2Name, String hl3Name);

	Integer filterRecordArticleHierarchyLevelData(String hl4Code, String hl4Name, String hl1Name, String hl2Name,
			String hl3Name);

	List<ADMItem> getItemsByArtCode(Integer offset, Integer pagesize, String hl4Code);

	Integer getCountItemsByArtCode(String hl4Code);

	List<ADMItem> searchItemsByArtCode(Integer offset, Integer pagesize, String hl4Code, String search);

	Integer searchCountItemsByArtCode(String hl4Code, String search);

	List<ADMItem> filterItemsByArtCode(Integer offset, Integer pagesize, String hl4Code, String itemCode,
			String itemName, String mrp, String rsp);

	Integer filterCountItemsByArtCode(String hl4Code, String itemCode, String itemName, String mrp, String rsp);

	String getPI(String orderId, String orderDetailId);

	List<String> filterAllPIPattern(Integer offset, Integer pagesize, String pattern);

	Integer countFilterAllPIPattern(String pattern);

	List<String> searchAllPIPattern(Integer offset, Integer pagesize, String search);

	Integer countSearchAllPIPattern(String search);

	List<PurchaseIndent> getPIData(Integer offset, Integer pagesize);

	Integer getCountPIData();

	List<PurchaseIndent> searchPIData(Integer offset, Integer pagesize, String search);

	Integer searchCountPIData(String search);

	List<PurchaseIndent> filterPIData(Integer offset, Integer pagesize, String poAmount, String poQuantity,
			String pattern);

	Integer filterCountPIData(String poAmount, String poQuantity, String pattern);

	List<UDF_SettingsMaster> getUDFMappingData();

	List<UDF_SettingsMaster> getPOUDFMappingData();

	Integer getUDFMappingDataRecord();

	List<UDF_SettingsMaster> searchUDFMappingData(String search);

	Integer searchUDFMappingDataRecord(String search);

	List<UDF_SettingsMaster> filterUDFMappingData(String udfType, String isCompulsary, String displayName);

	Integer filterUDFMappingDataRecord(String udfType, String isCompulsary, String displayName);

	PurchaseIndentMaster fromJson(String json) throws JsonParseException, JsonMappingException, IOException;

	List<InvSetUDF> getInvSetUDFDataByUDFType(int offset, int pagesize, String udfType);

	List<InvSetUDF> getInvSetUDFDataByUDFTypeExt(int offset, int pagesize, String udfType);

	Integer getInvSetUDFDataByUDFTypeRecord(String udfType);

	Integer getInvSetUDFDataByUDFTypeExtRecord(String udfType);

	List<InvSetUDF> searchInvSetUDFDataByUDFType(int offset, int pagesize, String udfType, String search);

	List<InvSetUDF> searchInvSetUDFDataByUDFTypeExt(int offset, int pagesize, String udfType, String search);

	Integer searchInvSetUDFDataByUDFTypeRecord(String udfType, String search);

	Integer searchInvSetUDFDataByUDFTypeExtRecord(String udfType, String search);

	List<InvSetUDF> filterInvSetUDFDataByUDFType(Integer offset, Integer pagesize, String udfType, String code,
			String name);

	List<InvSetUDF> filterInvSetUDFDataByUDFTypeExt(Integer offset, Integer pagesize, String udfType, String code,
			String name);

	Integer filterInvSetUDFDataByUDFTypeRecord(String udfType, String code, String name);

	Integer filterInvSetUDFDataByUDFTypeExtRecord(String udfType, String code, String name);

	String createPO(PurchaseOrderTemp pot)
			throws JsonParseException, JsonMappingException, IOException, SupplyMintException;

	List<PurchaseOrderParent> findAll(Integer offset, Integer pagesize, String orgId);

	List<Map<String, String>> findAllCatDescUdf(Integer offset, Integer pagesize, String header);

	Integer count(String orgId);

	Integer countCatDescUdf(String header);

	List<PurchaseOrderParent> filter(Integer offset, Integer pagesize, String orderNo, String orderDate,
			String validFrom, String validTo, String slCode, String slName, String slCityName, String hl1name,
			String hl2name, String hl3name, String orgId);

	Integer filterCount(String orderNo, String orderDate, String validFrom, String validTo, String slCode,
			String slName, String slCityName, String hl1name, String hl2name, String hl3name, String orgId);

	List<PurchaseOrderParent> search(Integer offset, Integer pagesize, String search, String orgId);

	List<Map<String, String>> searchCatDescUDFName(Integer offset, Integer pagesize, String search, String header);

	List<Map<String, String>> searchArticle(Integer offset, Integer pagesize, String search, String hl3Code,
			String catDescUDF);

	Integer searchCount(String search, String orgId);

	Integer searchCatDescUDFCount(String search, String header);

	int searchArticleCount(String search, String hl3Code, String catDescUDF);

	List<DeptSizeData> getSizeByDept(String dept);

//	String updateUDFMapping(List<UDF_SettingsMaster> udf);

	String updateUDFMapping(JsonNode udf) throws SupplyMintException, JsonProcessingException;

	ObjectNode getUDFMappingNode(List<UDF_SettingsMaster> udfNode);

	Integer getInvSetPOItemUDFDataRecord(String udfType);

	List<InvSetUDF> getInvSetPOItemUDFData(String udfType);

	List<InvSetUDF> filterInvSetPOItemUDFData(String udfType, String code, String name);

	Integer filterInvSetPOItemUDFDataRecord(String udfType, String code, String name);

	List<InvSetUDF> searchInvSetPOItemUDFData(String udfType, String search);

	Integer searchInvSetPOItemUDFDataRecord(String udfType, String search);

	ObjectNode getPurchaseOrderNode(List<PurchaseOrder> purchaseOrderList);

	PurchaseOrderConfiguration getPOConfiguration(int cid);

	int insertPOAudit(PurchaseOrderAudit poa);

	ResponseEntity<AppResponse> sendPODetailsOnRestAPI(String headerType, ObjectNode poNode,
			PurchaseOrderConfiguration poc) throws SupplyMintException;

	List<IndtCatDescUDFList> getIndtCatDescUDFListData();

	List<IndtCatDescUDFList> getIndtCatDescUDFListActiveData();

	/*
	 * List<DeptItemUDFSettings> getCategoriesByHl3Name(String hl3Name);
	 * 
	 * List<DeptItemUDFSettings> getUDFByHl3Name(String hl3Name);
	 */

	List<DeptItemUDFSettings> getDeptItemUDFByHl3Name(String hl3code);

	List<DeptItemUDFSettings> getDepartmentItemUDFByHl3Name(String hl3code);

	List<DeptItemUDFSettings> getPODepartmentItemUDFByHl3Name(String hl3code);

	List<DeptItemUDFMappings> getItemUdfMappingByHl3NameAndUDF(int offset, int pagesize, String hl3code,
			String catDescUdf, String description, String hl4code);

	List<DeptItemUDFMappings> itemUdfMappingByHl3NameAndUDF(int offSet, int pageSize, String hl3code, String catDescUdf,
			String description, String hl4code);

	int countItemUdfMappingByHl3NameAndUDF(String hl3code, String catDescUdf, String description, String hl4Code);

	int searchCountItemUdfMappingByHl3NameAndUDF(String hl3code, String catDescUdf, String search, String hl4code);

	int searchCountPOItemUdfMappingByHl3NameAndUDF(String hl3code, String catDescUdf, String search);

	List<DeptItemUDFMappings> searchItemUdfMappingByHl3NameAndUDF(Integer offset, Integer pagesize, String hl3code,
			String catDescUdf, String search, String hl4code);

	List<DeptItemUDFMappings> searchItemUdfMappingByHl3NameAndUDFEXT(Integer offset, Integer pagesize, String hl3code,
			String catDescUdf, String search);

	List<DeptItemUDFMappings> searchItemUdfMappingByHl3NameAndUDF(Integer offset, Integer pagesize, String hl3code,
			String search);

	List<DeptItemUDFMappings> getItemUDFMappingByHl3NameUDFEXT(int offSet, int pageSize, String hl3Name,
			String catDescUdf);

	int countItemUDFMappingByHl3NameUDFEXT(String hl3code, String catDescUdf);

	String updateItemCatDescUDF(JsonNode catDescNode) throws JsonProcessingException, SupplyMintException;

	int createInvSetUdf(InvSetUDF invSetUDF);

	int updateInvSetUdf(InvSetUDF invSetUDF);

	int createAndUpdatePOItemUDF(JsonNode node) throws Exception;

	List<DeptItemUDFSettings> getPOItemHeader(String hl3Name);

	List<DeptItemUDFSettings> getItemHeaderForPOCreate(String hl3code);

	int createDeptItemUDFMappings(DeptItemUDFMappings deptItemUDFMappings);

	int updateDeptItemUDFMappings(DeptItemUDFMappings deptItemUDFMappings);

	int createAndUpdatePODeptItemUDFMappings(JsonNode node, HttpServletRequest request) throws Exception;

	String updateSizeMapping(JsonNode node,String tenantHashKey) throws JsonProcessingException, DataAccessException, SupplyMintException;

	int updateSizeMappingMapper(DeptSizeData size);

	ObjectNode getItemUDFSettingNode(List<DeptItemUDFSettings> dept);

	// Prabhakar
	ObjectNode getItemUDFSettingMaster(String hl3code);

	ObjectNode getItemUDFMappingNode(List<DeptItemUDFMappings> data, String hl3code);

	List<Map<String, String>> getArticleByDept(Integer offset, Integer pagesize, String hl3Code, String catDescUDF);

	int countArticleByDept(String hl3Code, String catDescUDF);

	List<String> getPODesc6Values(String hl4Code, String mrpRangeFrom, String mrpRangeTo, Integer offset,
			Integer pagesize);

	List<String> getOtherPODesc6Values(String hl4Code, String mrpRangeFrom, String mrpRangeTo, Integer offset,
			Integer pagesize);

	Integer getPOCountDesc6Values(String hl4Code, String mrpRangeFrom, String mrpRangeTo);

	Integer getOtherPOCountDesc6Values(String hl4Code, String mrpRangeFrom, String mrpRangeTo);

	Integer countLoadIndent();

	List<PurchaseIndentHistory> getLoadIndent(int offSet, int pageSize);

	Integer countLoadIndentFilter(String orderId, String suplier, String cityName, String piFromDate, String piToDate);

	List<PurchaseIndentHistory> getLoadIndentFilter(String orderId, String suplier, String cityName, String piFromDate,
			String piToDate, int offSet, int pageSize);

	Integer countLoadIndentSearch(String search);

	List<PurchaseIndentHistory> getLoadIndentSearch(String search, int offSet, int pageSize);

	int createParentPO(PurchaseOrderParent po);

	String createDetailPO(PurchaseOrderParent po, OffsetDateTime currentTime, String ipAddress, String userName);

	Integer countPOSavedDepartment();

	List<Map<String, String>> getPOSavedDepartment(Integer offset, Integer pagesize);

	Integer countPOSavedDepartmentsSearch(String search);

	List<Map<String, String>> getPOSavedDepartmentSearch(Integer offset, Integer pagesize, String search);

	Integer countPOSavedSupplier(String hl3code);

	List<Map<String, String>> getPOSavedSupplier(Integer offset, Integer pagesize, String hl3Code);

	Integer countPOSavedSupplierSearch(String search, String hl3code);

	List<Map<String, String>> getPOSavedSupplierSearch(Integer offset, Integer pagesize, String search, String hl3code);

	Integer countPOSavedOrderNo(String hl3code, String supplierCode);

	List<Map<String, String>> getPOSavedOrderNo(Integer offset, Integer pagesize, String hl3Code, String supplierCode);

	Integer countPOSavedOrderNoSearch(String search, String hl3code, String supplierCode);

	List<Map<String, String>> getPOSavedOrderNoSearch(Integer offset, Integer pagesize, String search, String hl3code,
			String supplierCode);

	PurchaseOrderParent getSetBasedPO(String orderNo);

	List<PurchaseOrderChild> getSetBasedPOChild(String orderNo);

	int sendSMPartentPO(PurchaseOrderParent pop)
			throws JsonParseException, JsonMappingException, IOException, SupplyMintException;

	public void validateMarginRule(List<PurchaseOrderChild> poc) throws SupplyMintException;

	public Integer countSearchDesc6Values(String hl4Code, String mrpRangeFrom, String mrpRangeTo, String search);

	public Integer countOtherSearchDesc6Values(String hl4Code, String mrpRangeFrom, String mrpRangeTo, String search);

	public List<String> getPOAdhocSearchDesc6Values(String hl4Code, String mrpRangeFrom, String mrpRangeTo,
			Integer offset, Integer pagesize, String search);

	public List<String> getOtherPOAdhocSearchDesc6Values(String hl4Code, String mrpRangeFrom, String mrpRangeTo,
			Integer offset, Integer pagesize, String search);

	public List<DeptSizeData> getOtherSizeByDept(String dept);

	int countItemUdfMappingByHl3NameAndUDFForOthers(String hl3code, String cat_desc_udf, String description,
			String hl4code);

	List<DeptItemUDFMappings> itemUdfMappingByHl3NameAndUDFForOthers(int offset, int pageSize, String hl3code,
			String cat_desc_udf, String description, String hl4code);

	int searchCountItemUdfMappingByHl3NameAndUDFforOther(String hl3code, String cat_desc_udf, String search,
			String hl4code);

	List<DeptItemUDFMappings> searchItemUdfMappingByHl3NameAndUDFforOthers(int offset, int pageSize, String hl3code,
			String cat_desc_udf, String search, String hl4code);

	int insertUploadSummary(ProcurementUploadSummary summary);

	int updateLastRecord(String orgId, String uuId);

	Map<String, String> uploadFileOnS3(InputStream inputStream, String userName, String fileName, String type,
			String orgId) throws Exception;

	int insertDataInList(List<PurchaseOrder> data);

	int updateStatus(String status, int count, String url, String uuId, String userName);

	void insertDataFromFile(BufferedReader bufferedReader, int dataLength, String url, String uuId, String userName,
			String orgId, String token);

	public JsonNode getItemBarcode(ObjectNode itemNode)
			throws JsonParseException, JsonMappingException, IOException, SupplyMintException;

	ObjectNode getAvailablePOButtonKeys(String orgId) throws Exception;

	public JsonNode getItemBarcodeDetail(ObjectNode barcode)
			throws JsonParseException, JsonMappingException, IOException, SupplyMintException;

	Map<String, Object> getAllFCGHistory(int pageNo, int type, String search, String uploadedDate, String fileName,
			String fileRecord, String status, String orgId);

	int getUploadSummaryCount();

	ProcurementUploadSummary getLastRecord(String userName, String orgId);

	Map<String, String> getHoldDescUDFData(PurchaseOrderChild poc, String hl3code);

	Map<String, String> getHoldCategoriesData(String hl3code, String cat1, String cat2, String cat3, String cat4);

	List<Category> getHoldSizeData(List<String> sizeList, String hl3code);

	List<Category> getHoldColorData(List<String> colorList, String hl3code);

	ProcurementUploadSummary getSingleRecord(String userName, String orgId, String uuId);

	@SuppressWarnings("rawtypes")
	public Map<String, List> exportFaildDataFromFile(BufferedReader bufferedReader, String uuId, String userName,
			String orgId, int uploadedRecord, List<Integer> errorRows);

	File uploadFile(List<String> headers, List<String> fieldNames, List<PurchaseOrderExportData> errorData,
			List<PurchaseOrderExportData> currectData, String orgId);

	void importDataFromFile(BufferedReader bufferedReader, int dataLength, String url, String uuId, String userName,
			String orgId, String bucketName, String bucketPath, String token);

	List<String> getJobIdFCGFile(String ogdId);

	List<TempPurchaseOrderChild> getNullSizeData();

	public boolean repairNullSizes(List<TempPurchaseOrderChild> tpocList)
			throws JsonParseException, JsonMappingException, IOException;

	List<TempPurchaseOrderChild> getNullColorData();

	public boolean repairNullColors(List<TempPurchaseOrderChild> tpocList)
			throws JsonParseException, JsonMappingException, IOException;

	int configProcurementDate(ObjectNode dateNode, String orgId) throws Exception;

	int countTotalDistinctCities();

	List<String> getAllCitiesDetails(int offset, int pageSize);

	int countAllSearchedCities(String search);

	List<String> getAllSearchedCities(int offset, int pageSize, String search);

	List<Map<String, String>> getPODiscount(String articleCode, String mrp, String discountType);

	List<PurchaseOrderChild> getSetBasedPOChildForOthers(String orderNo);

}
