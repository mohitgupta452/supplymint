package com.supplymint.layer.business.contract.setting;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.supplymint.layer.data.setting.entity.FestivalSetting;

public interface FestivalSettingService {



	/**
	 * This method is used to create new festival according to given param
	 * 
	 * @return the intger value
	 */
	int createNew(List<FestivalSetting> defaultFestivalSetting, List<FestivalSetting> customFestivalSetting,
			HttpServletRequest request);

	/**
	 * This method is used to get all cheked festival list with startDate and
	 * endDate
	 * 
	 * @return output in node format
	 */
	ObjectNode getAllNew();

	/**
	 * This method is used to get all customlist with pagination of 9 at a time
	 * 
	 * @return output in node format
	 */
	ObjectNode getAllCustomList(int offset, int pageSize);

	/**
	 * This method is used count all custom festival data
	 * 
	 * @return integer value
	 */
	int countCustomList();

	/**
	 * This method is used to get all default list
	 * 
	 * @return output in node format
	 */
	ObjectNode getAllDefaultList();



	/**
	 * This method is used to get all default list with startDate and
	 * endDate
	 * 
	 * @return output in node format
	 */
	ObjectNode getAllDefaultListOldMethod();

}
