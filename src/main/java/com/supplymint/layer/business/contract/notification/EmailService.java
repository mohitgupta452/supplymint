package com.supplymint.layer.business.contract.notification;

import org.springframework.mail.SimpleMailMessage;

/**
 * @Author Manoj Singh
 * @Since 12-Feb-2019
 */

public interface EmailService {

	void sendSimpleMessage(String[] to, String[] cc, String subject, String text);

	void sendSimpleMessageUsingTemplate(SimpleMailMessage template, String... templateArgs);

	void sendMessageWithAttachment(String to, String subject, String text, String pathToAttachment);

	void sendEmailUsingMailTemplate(SimpleMailMessage template, String mailTemplate, String... templateArgs);

}
