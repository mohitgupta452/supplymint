package com.supplymint.layer.business.contract.demand;

import java.io.InputStream;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.supplymint.exception.SupplyMintException;
import com.supplymint.layer.data.tenant.demand.entity.BudgetedDemandPlanning;
import com.supplymint.layer.data.tenant.demand.entity.BudgetedTemporary;
import com.supplymint.layer.data.tenant.demand.entity.DemandForecastScheduler;
import com.supplymint.layer.data.tenant.demand.entity.DemandGraph;
import com.supplymint.layer.data.tenant.demand.entity.DemandPlanning;
import com.supplymint.layer.data.tenant.demand.entity.DemandPlanningViewLog;
import com.supplymint.layer.data.tenant.demand.entity.DpUploadSummary;
import com.supplymint.layer.data.tenant.demand.entity.FinalAssortment;

/**
 * This interface describes all service for Demand Planning
 * @author Prabhakar Srivastava
 * @author Bishnu Dutta
 * @Date 10 Jan 2019
 * @version 1.0
 */
public interface DemandPlanningService {
	
	/**
	 * This method describes checking condition
	 * for running Forecast job
	 * @Table DEMAND_PLANNING
	 */
	int validationCreate();

	/**
	 * This method describes create Demand Plan
	 * @param DemandPlanning
	 * @Table DEMAND_PLANNING
	 */
	int create(DemandPlanning demandPlanning);
	
	/**
	 * This method describes update status with code
	 * for running Forecast job
	 * @Table DEMAND_PLANNING
	 */
	int updateCodeWithStatus(int statusCode, String statusMessage, String runId, String duration,
			Date updationTime,String forcastOnAssortmentCode);

	/**
	 * This method describes getting all record
	 * for demand planning history 
	 * @Table DEMAND_PLANNING
	 */
	int record();

	/**
	 * This method describes getting all demand planning history
	 * according to pagination
	 * @Table DEMAND_PLANNING
	 */
	List<DemandPlanning> getAll(Integer offset, Integer pageSize);

	/**
	 * This method describes getting all filter record for
	 * demand planning history
	 * @param DemandPlanning
	 * @Table DEMAND_PLANNING
	 */
	int filterRecord(DemandPlanning demandPlanning);

	/**
	 * This method describes getting all filter for
	 * demand planning history according to pagination
	 * @param DemandPlanning
	 * @Table DEMAND_PLANNING
	 */
	List<DemandPlanning> filter(DemandPlanning demandPlanning);

	/**
	 * This method describes getting all Search record for
	 * demand planning history
	 * @param Search
	 * @Table DEMAND_PLANNING
	 */
	int countSearchRecord(String search);

	/**
	 * This method describes getting all Search for
	 * demand planning history according to pagination
	 * @param Search
	 * @Table DEMAND_PLANNING
	 */
	List<DemandPlanning> search(Integer offset, Integer pageSize, String search);
	
	/**
	 * This method describes getting all Distinct Assortment
	 * record for demand planning graph
	 * @Table DEMAND_ASSORTMENT_MONTHLY
	 */
	int assortmentRecord();

	/**
	 * This method describes getting all Distinct Assortment
	 * for demand planning graph according to pagination
	 * @Table DEMAND_ASSORTMENT_MONTHLY
	 */
	List<String> getAssortment(Integer offset, Integer pageSize);
	
	/**
	 * This method describes getting all Search record
	 * for distinct assortmentCode
	 * @param Search
	 * @Table DEMAND_ASSORTMENT_MONTHLY
	 */
	int assortmentSearchRecord(String search);

	/**
	 * This method describes getting all Search for
	 * distinct assortmentCode according to pagination
	 * @param Search
	 * @Table DEMAND_ASSORTMENT_MONTHLY
	 */
	List<String> assortmentSearch(Integer offset, Integer pageSize, String search);

	/**
	 * This method describes generate graph
	 * for Monthly according to user date range
	 * @Table DEMAND_ASSORTMENT_MONTHLY, DEMAND_FORECAST_MONTHLY,
	 * DEMAND_BUDGETED_MONTHLY
	 */
	Set<DemandGraph> generateMonthlyForecast(DemandPlanningViewLog demandPlanningViewLog, HttpServletRequest request);

	/**
	 * This method describes generate graph
	 * for Weekly according to user date range
	 * @Table DEMAND_ASSORTMENT_WEEKLY, DEMAND_FORECAST_WEEKLY,
	 * DEMAND_BUDGETED_WEEKLY
	 */
	Set<DemandGraph> generateWeeklyForecast(DemandPlanningViewLog demandPlanningViewLog, HttpServletRequest request);
	
	/**
	 * This method describes insert record if 
	 * invalid Excel Occurred
	 * @Table DP_UPLOAD_SUMMARY
	 */
	int insertDPUploadSummaryData(String fileName, String type, String userName, int rowCount);

	/**
	 * This method describes validate Budgeted Excel file
	 * @param User Name, Type, File
	 */
	List<BudgetedTemporary> validDataInExcelFile(InputStream file, String userName, String type)
			throws SupplyMintException;

	/**
	 * This method describes insert record
	 * for successfully Upload Excel File
	 * @Table DP_UPLOAD_SUMMARY
	 */
	int insertDpUploadSummary(DpUploadSummary dpUploadSummary);

	/**
	 * This method describes get previous forecast
	 * @Table DEMAND_PLANNING
	 */
	String getRunIdPattern();

	/**
	 * This method describes get by Run ID
	 * @param RUN ID
	 * @Table DEMAND_PLANNING
	 */
	DemandPlanning getByRunId(String runId);

	/**
	 * This method describes update summary status
	 * @param RUN ID, Summary status
	 * @Table DEMAND_PLANNING
	 */
	int updateSummary(String summary, String runId);

	/**
	 * This method describes update previous summary status
	 * @param RUN ID
	 * @Table DEMAND_PLANNING
	 */
	int updatePreviousSummary(String runId);

	/**
	 * This method describes Retrieve all details
	 * for demand planning
	 * @Table DEMAND_PLANNING
	 */
	List<DemandPlanning> downloadForecastDetails();

	/**
	 * This method describes export forecast to excel
	 * for demand planning
	 */
	void generateForecastTOExcel(List<DemandPlanning> list, String filePath);

	/**
	 * This method describes update Forecast download URL
	 * after succeeded the forecast
	 * @param Saved Path, Download Path, Run ID
	 * @Table DEMAND_PLANNING
	 */
	int updateDownloadUrl(String savedPath, String downloadUrl, String runId);
	
	void generateBudgetedDemandListTOExcel(List<BudgetedDemandPlanning> list, String filePath);

	/**
	 * This method describes TRUNCATE Budgeted Temporary Data
	 * @Table DEMAND_BUDGETED_TEMP
	 */
	int truncateTempBudgetedData(String userName);

	/**
	 * This method describes update File upload status
	 * @Table DP_UPLOAD_SUMMARY
	 */
	int updateDPUploadSummary(String status, int valid, int invalid, String url, String id);

	/**
	 * This method describes get Final Assortment Data For Excel
	 * @Table FINAL_ASSORTMENT
	 */
	List<FinalAssortment> getFinalAssortmentData();

	/**
	 * This method describes get valid Budgeted Monthly Data For Excel
	 * for user according date range
	 * @Table DEMAND_BUDGETED_MONTHLY
	 */
	List<BudgetedDemandPlanning> getBudgetedMonthlyData(String from, String to);

	/**
	 * This method describes get valid Budgeted Weekly Data For Excel
	 * for user according date range
	 * @Table DEMAND_BUDGETED_WEEKLY
	 */
	List<BudgetedDemandPlanning> getBudgetedWeeklyData(String from, String to);

	/**
	 * This method describes get download URL valid Budgeted MONTHLY and WEEKLY Data For
	 * Excel for user according date range
	 * @Table DEMAND_BUDGETED_MONTHLY, DEMAND_BUDGETED_WEEKLY
	 */
	String getBudgetedDownloadURL(String frequency, String fromDate, String toDate,HttpServletRequest request);

	/**
	 * This method describes get demand Budgeted template For Excel
	 * @param File Path
	 * @Reference AWS S3 Bucket
	 */
	void generateExcelTemplate(String filePath);

	/**
	 * This method describes get last File upload status
	 * user specific
	 * @param User Name
	 * @Table DP_UPLOAD_SUMMARY
	 */
	DpUploadSummary getLastStatus(String userName);
	
	/**
	 * This method describes getting all record
	 * for demand Budgeted history 
	 * @Table DP_UPLOAD_SUMMARY
	 */
	int countBudgetedHistoryData();

	/**
	 * This method describes getting all data
	 * for demand Budgeted history according to pagination
	 * @Table DP_UPLOAD_SUMMARY
	 */
	List<DpUploadSummary> getBudgetedHistoryData(Integer offset, Integer pagesize);

	/**
	 * This method describes getting all filter record for
	 * demand budgeted history
	 * @Table DP_UPLOAD_SUMMARY
	 */
	int countFilterBudgetedHistoryData(String uploadedDate, String fileName, String totalRecord, String valid,
			String invalid);

	/**
	 * This method describes getting all filter for
	 * demand budgeted history according to pagination
	 * @Table DP_UPLOAD_SUMMARY
	 */
	List<DpUploadSummary> filterBudgetedHistoryData(Integer offset, Integer pagesize, String uploadedDate,
			String fileName, String totalRecord, String valid, String invalid);

	/**
	 * This method describes getting all Search record for
	 * demand Budgeted history
	 * @param Search
	 * @Table DP_UPLOAD_SUMMARY
	 */
	int countSearchBudgetedHistoryData(String search);

	/**
	 * This method describes getting all Search for
	 * demand budgeted history according to pagination
	 * @param Search
	 * @Table DP_UPLOAD_SUMMARY
	 */
	List<DpUploadSummary> searchBudgetedHistoryData(Integer offset, Integer pagesize, String search);

	/**
	 * This method describes get URL valid demand Budgeted History For Excel
	 * @param Frequency, User Name
	 * @Reference AWS S3 Bucket
	 */
	String downloadURLDpUploadSummary(String frequency, String userName,String orgId);

	/**
	 * This method describes update last Demand Budgeted Weekly Status False
	 * @param User Name
	 * @Table DEMAND_BUDGETED_WEEKLY
	 */
	int updateLastDemandBudgetedWeeklyData(String userName);

	/**
	 * This method describes update last Demand Budgeted Monthly Status False
	 * @param User Name
	 * @Table DEMAND_BUDGETED_MONTHLY
	 */
	int updateLastDemandBudgeteMonthlyData(String userName);

	/**
	 * This method describes get All invalid Demand Budgeted Weekly Data
	 * @Table DEMAND_BUDGETED_WEEKLY
	 */
	List<BudgetedDemandPlanning> getAllInvalidWeeklyData();

	/**
	 * This method describes get All invalid Demand Budgeted Monthly Data
	 * @Table DEMAND_BUDGETED_MONTHLY
	 */
	List<BudgetedDemandPlanning> getAllInvalidMonthlyData();

	/**
	 * This method describes batch insert for
	 * Demand Budgeted Temporary table
	 * @param List<BudgetedTemporary>
	 * @Table DEMAND_BUDGETED_TEMP
	 */
	int insertFromExcelFileInList(List<BudgetedTemporary> data);
	
	/**
	 * This method describes delete matching assortmentCode for
	 * Demand Budgeted Weekly Data
	 * @param User Name
	 * @Table DEMAND_BUDGETED_WEEKLY
	 */
	List<BudgetedDemandPlanning> deleteMatchedAssortmentCodeTempDataForWeeklyBudgeted(String userName);

	/**
	 * This method describes delete matching assortmentCode for
	 * Demand Budgeted Monthly Data
	 * @param User Name
	 * @Table DEMAND_BUDGETED_MONTHLY
	 */
	List<BudgetedDemandPlanning> deleteMatchedAssortmentCodeTempDataForMonthlyBudgeted(String userName);

	/**
	 * This method describes update previous status False
	 * @param User Name, ID
	 * @Table DP_UPLOAD_SUMMARY
	 */
	int updateBudgetedSummary(String userName, String id);
	
	/**
	 * This method describes batch insert From
	 * valid Demand Budgeted Temporary table 
	 * @param User Name
	 * @Table DEMAND_BUDGETED_TEMP, DEMAND_BUDGETED_WEEKLY
	 */
	int insertMatchBudgetedWeeklyData(String userName);

	/**
	 * This method describes batch insert From
	 * Invalid Demand Budgeted Temporary table 
	 * @param User Name
	 * @Table DEMAND_BUDGETED_TEMP, DEMAND_BUDGETED_WEEKLY
	 */
	int insertNotMatchBudgetedWeeklyData(String userName);

	/**
	 * This method describes batch insert From
	 * valid Demand Budgeted Temporary table 
	 * @param User Name
	 * @Table DEMAND_BUDGETED_TEMP, DEMAND_BUDGETED_MONTHLY
	 */
	int insertMatchBudgetedMonthlyData(String userName);

	/**
	 * This method describes batch insert From
	 * Invalid Demand Budgeted Temporary table 
	 * @param User Name
	 * @Table DEMAND_BUDGETED_TEMP, DEMAND_BUDGETED_MONTHLY
	 */
	int insertNotMatchBudgetedMonthlyData(String userName);

	/**
	 * This method describes get Budgeted History Information
	 * @param ID
	 * @Table DP_UPLOAD_SUMMARY
	 */
	DpUploadSummary getBudgetedHistoryDataByID(String id);

	/**
	 * This method describes get and update file upload status
	 * @param User Name
	 * @Table DP_UPLOAD_SUMMARY
	 */
	String getProgressStatus(String userName);

	/**
	 * This method describes get and update file upload status
	 * after Processing
	 * @param User Name
	 * @Table DP_UPLOAD_SUMMARY
	 */
	String getVerifiedAllData(String userName);

	/**
	 * This method describes get and update file upload status
	 * after Verifying
	 * @param User Name
	 * @Table DP_UPLOAD_SUMMARY
	 */
	String getFinalyData(String type, String userName,String orgId);

	/**
	 * PENDING
	 * @Table 
	 */
	int createForecastSchedulerAutoConfig(DemandForecastScheduler forecastScheduler, ServletRequest request);

	/**
	 * PENDING
	 * @Table 
	 */
	DemandForecastScheduler getForecastSchedulerAutoConfigData(String eid);

	/**
	 * PENDING
	 * @Table 
	 */
	int updateForecastSchedule(String schedule, String nextSchedule, String eid);
	
	/**
	 * This method describes getting Demand Planning
	 * Graph Info
	 * @param Frequency, UUID
	 * @Table DEMAND_PLANNING_VIEWLOG
	 */
	DemandPlanningViewLog getLastReportByFrequency(String frequency, String uuid);

	/**
	 * This method describes getting previous Demand Planning
	 * Graph Info
	 * @param Frequency, User Name
	 * @Table DEMAND_PLANNING_VIEWLOG
	 */
	DemandPlanningViewLog getLastReport(String frequency, String userName);

	/**
	 * This method describes getting all record
	 * for demand planning Insight history 
	 * @Table DEMAND_PLANNING_VIEWLOG
	 */
	int getAllHistotyCount(String frequency);

	/**
	 * This method describes getting all demand planning Insight
	 * history according to pagination
	 * @Table DEMAND_PLANNING_VIEWLOG
	 */
	List<DemandPlanningViewLog> getAllHistory(Integer offset, Integer pageSize, String frequency);

	/**
	 * This method describes getting all filter record for
	 * demand planning Insight history
	 * @param DemandPlanning
	 * @Table DEMAND_PLANNING_VIEWLOG
	 */
	int filterHistoryRecord(String assortment, String startDate, String endDate, String createdOn,
			String frequency);
	/**
	 * This method describes getting all filter for
	 * demand planning Insight history according to pagination
	 * @param DemandPlanning
	 * @Table DEMAND_PLANNING_VIEWLOG
	 */
	List<DemandPlanningViewLog> filterHistory(Integer offset, Integer pageSize, String assortment,
			String startDate, String endDate, String createdOn, String frequency);
	/**
	 * This method describes getting all Search record for
	 * demand planning Insight history
	 * @param Search
	 * @Table DEMAND_PLANNING_VIEWLOG
	 */
	int searchHistoryRecord(String search, String frequency);
	
	/**
	 * This method describes getting all Search for
	 * demand planning Insight history according to pagination
	 * @param Search
	 * @Table DEMAND_PLANNING_VIEWLOG
	 */
	List<DemandPlanningViewLog> searchHistory(Integer offset, Integer pageSize, String search, String frequency);

	/**
	 * This method describes update Forecast download URL
	 * In Graph Area
	 * @param URL, UUID
	 * @Table DEMAND_PLANNING_VIEWLOG
	 */
	int updatedownloadUrlLogById(String downloadUrl, String uuid);

	/**
	 * This method describes get last Forecast
	 * Information where status is not Processing
	 * @Table DEMAND_PLANNING
	 */
	DemandPlanning getLastSummaryData();

	/**
	 * This method describes get Running Forecast
	 * ID where status is Processing
	 * @Table DEMAND_PLANNING
	 */
	String getRunningId();

	/**
	 * This method describes get last 5 Succeeded Forecast
	 * Run Time
	 * @Table DEMAND_PLANNING
	 */
	List<String> fiveSucceededTime();

	
	/**
	 * This method describes update MV refresh status
	 * according to parameter
	 * @Table MVIEW_REFRESH_HISTORY
	 */
	int updateMVStatus(String status, char active, String foreacastReadStatus);

//	String getStatus();
	
	/**
	 * This method describes update processing status to Failed
	 * due to processing time take more than expecting time
	 * @Table DEMAND_PLANNING
	 */
	int updateProcessingToFailed();

	/**
	 * This method describes refresh OTB FORECAST MONTHLY
	 * after Succeeded the forecast Job
	 * @MaterializedView OTB_FORECAST_MONTHLY
	 */
	void refreshMView();
	
	/**
	 * This method describes refresh Actual Data materialized view
	 * before Running the forecast Job
	 * @MaterializedView DEMAND_ASSORTMENT_MONTHLY, DEMAND_ASSORTMENT_WEEKLY
	 */
	void refreshActualDataMView();
	
	/**
	 * This method describes get last Forecast Assortment
	 * @Table DEMAND_PLANNING
	 */
	String getForcastOnAssortmentCode();
	
	/**
	 * This method describes get all previous Forecast Assortment record
	 * according to parameter
	 * @Table DEMAND_PLANNING
	 */
	int assortmentHistoryRecord(String startDate,String endDate,String frequency);
	
	/**
	 * This method describes get all previous Forecast Assortment list
	 * according to pagination and parameter
	 * @Table DEMAND_PLANNING
	 */
	List<String> getAssortmentHistory(int offset,int pageSize,String startDate,String endDate,String frequency);
	
	/**
	 * This method describes get previous Forecast Assortment search 
	 * record according to pagination and parameter
	 * @Table DEMAND_PLANNING
	 */
	int assortmentSearchHistoryRecord(String search,String startDate,String endDate,String frequency);
	
	/**
	 * This method describes get previous Forecast Assortment search
	 * according to pagination and parameter
	 * @Table DEMAND_PLANNING
	 */
	List<String> assortmentHistorySearch(int offset,int pageSize,String search,String startDate,String endDate,String frequency);
	
	/**
	 * This method describes get Previous Forecast AssortmentCode
	 * and graph according to parameter
	 * @Reference AWS S3 Bucket
	 */
	ObjectNode getPreviousAssortmentCodeHistory(int pageNo,int type,int totalCount,String assortmentCode,String runId,String startDate,String endDate,String search,HttpServletRequest request) throws Exception;

	/**
	 * This method describes generate previous forecast data
	 * download URL according to parameter
	 * @Reference AWS S3 Bucket
	 */
	String getForecastUrl(String runId,String startDate,String endDate,String frequency,ServletRequest request);
	
	/**
	 * This method describes get last succeeded Forecast ID
	 * @Table DEMAND_PLANNING
	 */
	String getLastRunId();

}
