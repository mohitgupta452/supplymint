package com.supplymint.layer.business.contract.administration;

import java.util.List;
import java.util.Map;

import com.supplymint.layer.data.tenant.administration.entity.ADMOrganisation;

public interface OrganisationService {

	Integer insert(ADMOrganisation organisation);

	ADMOrganisation getById(Integer orgId);

	List<ADMOrganisation> getAll(Integer pageNo, Integer pageSize);

	Integer deleteById(Integer orgId);

	Integer update(ADMOrganisation admOrganisation);

	ADMOrganisation convert(String value);

	Integer record();

	List<ADMOrganisation> searchAll(Integer offset, Integer pagesize, String search);

	Integer searchRecord(String search);

	List<ADMOrganisation> filter(ADMOrganisation admOrganisation);

	int validationCheck(ADMOrganisation admOrganisation);

	List<ADMOrganisation> checkOrgExistance(ADMOrganisation admOrganisation);

	List<ADMOrganisation> checkOrgUpdateExistance(ADMOrganisation admOrganisation);

	List<ADMOrganisation> filterOrganisation(int offset, int pageSize, ADMOrganisation admOrganisation);

	Integer filterRecord(ADMOrganisation admOrganisation);

	List<ADMOrganisation> getAllRecords();

	List<ADMOrganisation> getAllData();

	void generateOrganisationListTOExcel(List<ADMOrganisation> list, String filePath);

	Map<String, List<String>> getOrganisationHeaders();

	List<ADMOrganisation> checkOrgExistanceValidation(String contEmail, String contNumber);

	Map<String, String> uploadImage(ADMOrganisation org);

}
