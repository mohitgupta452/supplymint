package com.supplymint.util;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

/**
 * A utility that downloads a file from a URL.
 * 
 * @author Prabhakar Srivastava
 * @Date 09-Jun-2019
 * @Version 1.0
 */
public class HttpDownloadUtils {
	private static final int BUFFER_SIZE = 4096;

	/**
	 * Downloads a file from a URL
	 * 
	 * @param fileURL HTTP URL of the file to be downloaded
	 * @param saveDir path of the directory to save the file
	 * @throws IOException
	 */
	public static void downloadFile(String fileURL, String saveDir) throws IOException {
		URL url = new URL(fileURL);
		HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
		int responseCode = httpConn.getResponseCode();

		// always check HTTP response code first
		if (responseCode == HttpURLConnection.HTTP_OK) {
			String tempDir = System.getProperty("java.io.tmpdir");
			File file = new File(tempDir + FilenameUtils.getName(url.getPath()));
			FileUtils.copyURLToFile(url, file);
		}
	}
}