package com.supplymint.util;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.supplymint.exception.SupplyMintException;

@Service
public class JsonValidator {

	public String serialize(Object object) throws SupplyMintException {
		try {
			Class<?> objectClass = Objects.requireNonNull(object).getClass();
			Map<String, String> jsonElements = new HashMap<>();
			for (Field field : objectClass.getDeclaredFields()) {
				field.setAccessible(true);
				if (field.isAnnotationPresent(JsonField.class)) {
					String fieldValueName = (String) field.get(object);
					String serializedKey = getSerializedKey(field);
					if (fieldValueName == null) {
						String msg = field.getAnnotation(JsonField.class).msg();
						throw new SupplyMintException( msg +" : "+serializedKey);
					}

					jsonElements.put(serializedKey, fieldValueName);
				}
			}
			return toJsonString(jsonElements);
		} catch (IllegalAccessException e) {
			throw new SupplyMintException(e.getMessage());
		}
	}

	private String toJsonString(Map<String, String> jsonMap) {
		String elementsString = jsonMap.entrySet().stream()
				.map(entry -> "\"" + entry.getKey() + "\":\"" + entry.getValue() + "\"")
				.collect(Collectors.joining(","));
		return "{" + elementsString + "}";
	}

	private String getSerializedKey(Field field) {
		String annotationValue = field.getAnnotation(JsonField.class).value();
		if (annotationValue.isEmpty()) {
			return field.getName();
		} else {
			return annotationValue;
		}
	}


}