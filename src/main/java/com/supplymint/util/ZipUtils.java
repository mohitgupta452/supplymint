package com.supplymint.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Enumeration;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @Authhor Prabhakar Srivastava
 * @Date 08-Jun-2019
 * @Version 1.0
 */

@SuppressWarnings("PMD")
public class ZipUtils {

	private static final int BUFFER_SIZE = 8192;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(LocalDatetimeDeserializer.class);

	/*
	 * Reading Zip archive using ZipFile class
	 */

	@SuppressWarnings("unused")
	private static void readUsingZipFile(String fileName) throws IOException {
		String savedPath = File.separator + "ZIP";

		String outputFolderName = FileUtils.createFilePath(FileUtils.getPlatformBasedParentDir(), savedPath);

		final ZipFile file = new ZipFile(fileName);
		LOGGER.debug("Iterating over zip file : %s" , fileName);
		try {
			final Enumeration<? extends ZipEntry> entries = file.entries();
			while (entries.hasMoreElements()) {
				final ZipEntry entry = entries.nextElement();
				LOGGER.debug("File: %s Size %d Modified on %TD %n", entry.getName(), entry.getSize(),
						new Date(entry.getTime()));
				extractEntry(entry, file.getInputStream(entry), outputFolderName);
			}
			LOGGER.debug("Zip file %s extracted successfully in %s", fileName, outputFolderName);
		} finally {
			file.close();
		}
	}

	/*
	 * Reading Zip file using ZipInputStream
	 */
	@SuppressWarnings("unused")
	private static void readUsingZipInputStream(String fileName) throws IOException {

		String savedPath = File.separator + "ZIP";
		String outputFolderName = FileUtils.createFilePath(FileUtils.getPlatformBasedParentDir(), savedPath);

		BufferedInputStream bis = new BufferedInputStream(new FileInputStream(fileName));
		final ZipInputStream is = new ZipInputStream(bis);
		try {
			ZipEntry entry;
			while ((entry = is.getNextEntry()) != null) {
				LOGGER.debug("File: %s Size %d Modified on %TD %n", entry.getName(), entry.getSize(),
						new Date(entry.getTime()));
				extractEntry(entry, is, outputFolderName);
			}
		} finally {
			is.close();
		}
	}
	

	private static void extractEntry(final ZipEntry entry, InputStream is, String outputDir) throws IOException {
		String exractedFile = outputDir + entry.getName();
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(exractedFile);
			final byte[] buf = new byte[BUFFER_SIZE];
			int read = 0;
			int length;
			while ((length = is.read(buf, 0, buf.length)) >= 0) {
				fos.write(buf, 0, length);
			}
		} catch (IOException ioex) {
			fos.close();
		}
	}
	
	/*
	 * Creating a Zip File
	 */
	public static void createZip(File zipFileName, File[] selected) {

		try {
			byte[] buffer = new byte[1024];
			ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipFileName));
			out.setLevel(Deflater.DEFAULT_COMPRESSION);

			for (int i = 0; i < selected.length; i++) {
				FileInputStream in = new FileInputStream(selected[i]);
				out.putNextEntry(new ZipEntry(selected[i].getName()));
				int len;
				while ((len = in.read(buffer)) > 0) {
					out.write(buffer, 0, len);
				}
				out.closeEntry();
				in.close();
			}
			out.close();

		} catch (IllegalArgumentException iae) {
			iae.printStackTrace();
		} catch (FileNotFoundException fnfe) {
			fnfe.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}

}
