package com.supplymint.util;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * 
 * @author Ashish Singh Dev
 * @version 1.0
 * @since 20-Sep-2018
 *
 */
public class SecurityUtils {

	private static final Logger LOGGER = LoggerFactory.getLogger(SecurityUtils.class);
	
	public static String hashSHA256(String data) throws NoSuchAlgorithmException {

		MessageDigest md = MessageDigest.getInstance("SHA-256");
		md.update(data.getBytes(StandardCharsets.UTF_8));
		byte[] digest = md.digest();

		return String.format("%064x", new BigInteger(1, digest));
	}

	public static void main(String[] args) throws NoSuchAlgorithmException {
		LOGGER.debug(hashSHA256("7:97918ab4-a974-4260-a9c4-872413669c8a"));
	}
}
