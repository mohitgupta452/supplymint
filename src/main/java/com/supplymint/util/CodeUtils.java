package com.supplymint.util;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.net.InetAddress;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.net.ntp.NTPUDPClient;
import org.apache.commons.net.ntp.TimeInfo;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.LocalDate;
import org.joda.time.Period;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

/**
 * @Authhor Manoj Singh
 * @Date 24-Sep-2018
 * @Version 1.0
 */
public interface CodeUtils {

	String _REDIS_KEY_SECURE_KEY = "_SECKEY";
	String _REDIS_KEY_IP_ADDRESS = "_IPA";

	static SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");

	static SimpleDateFormat dateTimeFormat = new SimpleDateFormat("dd MMM yyy HH:MM a");

	static SimpleDateFormat _dateTimeFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm a");

	static SimpleDateFormat ___dateTimeFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");

	static SimpleDateFormat _____dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

	static SimpleDateFormat ______dateTimeFormat = new SimpleDateFormat("yyyy_MM_dd_HH:mm");

	static SimpleDateFormat ________dateTimeFormat = new SimpleDateFormat("dd_MM_yyyy");

	static SimpleDateFormat ddDateTimeFormat = new SimpleDateFormat("dd");

	static SimpleDateFormat __________dateTimeFormat = new SimpleDateFormat("yyyy_MM_dd");

	static SimpleDateFormat _24HourTimeFormat = new SimpleDateFormat("HH:mm");

	static SimpleDateFormat _________dateTimeFormat = new SimpleDateFormat("dd_MM_yyyy_HH:mm:ss");

	static SimpleDateFormat ___________dateTimeFormat = new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss");

	static SimpleDateFormat __dateTimeFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

	static SimpleDateFormat ____dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	static SimpleDateFormat _____dateFormat = new SimpleDateFormat("dd MMM yyy HH:mm");

	static SimpleDateFormat __dateFormat = new SimpleDateFormat("yyyy-MM-dd");

	static SimpleDateFormat ______dateFormat = new SimpleDateFormat("MM_yyyy");

	static SimpleDateFormat dateTimeMonthFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm");

	static SimpleDateFormat _dateFormatIST = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");

	static DecimalFormat decimalFormat = new DecimalFormat("#0.00");

	static DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

	static DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME;

	static String key = "aesEncryptionKey";

	static String initVector = "encryptionIntVec";

	String DATA = "data";
	String RESPONSE = "resource";
	String RESPONSE_MSG = "message";
	String RESPONSE_SUCCESS_CODE = "2000";

	@SuppressWarnings("rawtypes")
	static <T> boolean isEmpty(T data) {

		if (data == null) {
			return true;
		} else if (data instanceof String) {
			return data.toString().isEmpty();
		} else if (data instanceof List) {
			return ((List) data).isEmpty();
		} else if (data instanceof Set) {
			return ((Set) data).isEmpty();
		} else if (data instanceof Map) {
			return ((Map) data).isEmpty();
		} else if (data instanceof ObjectNode) {
			return ((ObjectNode) data).size() == 0 ? true : false;
		}
		return false;

	}

	static Date convertISOStringToDate(String date, String timeZoneId) {
		if (isEmpty(date)) {
			new RuntimeException("Date is empty");
		}
		TimeZone.setDefault(TimeZone.getTimeZone(timeZoneId));
		return Date.from(Instant.parse(date));
	}

	static Date getTodaysDate(String timeZoneId) {
		TimeZone.setDefault(TimeZone.getTimeZone(timeZoneId));
		return Calendar.getInstance().getTime();
	}

	static Date convertDateByZoneId(String dateInString, String timeZone) {
		TimeZone timezone = TimeZone.getTimeZone(timeZone);
		if (isEmpty(dateInString) || isEmpty(timeZone))
			new RuntimeException("Please check either Date/Timezone is empty");
		_____dateTimeFormat.setTimeZone(timezone);
		try {
			return _____dateTimeFormat.parse(dateInString);
		} catch (ParseException e) {
			return null;
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes", "deprecation" })
	public static Map<String, String> schedule(Date date) {
		Map cronMap = new HashMap<>();
		cronMap.put(Schedule.MINUTES, Integer.toString(date.getMinutes()));
		cronMap.put(Schedule.HOURS, Integer.toString(date.getHours()));
		cronMap.put(Schedule.DAY_OF_MONTH, Integer.toString(date.getDate()));
		cronMap.put(Schedule.MONTH, Integer.toString(date.getMonth()));
		cronMap.put(Schedule.DAY_OF_WEEK, Integer.toString(date.getDay()));
		cronMap.put(Schedule.YEAR, Integer.toString(date.getYear()));
		return cronMap;
	}

	static String convertDatetoString(Date date) {
		if (isEmpty(date)) {
			new RuntimeException("Date is empty");
		}
		return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").format(date);

	}

	static String convertDateToString(Date date) {
		if (isEmpty(date)) {
			new RuntimeException("Date is empty");
		}
		return new SimpleDateFormat("yyyy-MM-dd_HH_mm").format(date);

	}

	static <T> T capatiliseString(T data) {

		if (!isEmpty(data)) {
			Field[] fields = data.getClass().getDeclaredFields();

			for (Field field : fields) {
				if (field.getType().isAssignableFrom(String.class)) {
					field.setAccessible(true);
					try {
						field.set(data, capatilise(field.get(data)));

					} catch (IllegalArgumentException | IllegalAccessException e) {
					}
				}
			}
		}
		return data;
	}

	static String capatilise(Object data) {
		if (data instanceof String && !isEmpty(data)) {

			char[] chars = ((String) data).toLowerCase().toCharArray();
			boolean found = false;
			for (int i = 0; i < chars.length; i++) {
				if (!found && Character.isLetter(chars[i])) {
					chars[i] = Character.toUpperCase(chars[i]);
					found = true;
				} else if (Character.isWhitespace(chars[i]) || chars[i] == '.' || chars[i] == '\'') {
					found = false;
				}
			}

			return String.valueOf(chars);
		} else {
			return String.valueOf(data);
		}
	}

	// static JsonObject getReturnJsonSuccess(String data) {
	// JsonObject response = new JsonObject();
	// response.addProperty("errorCode", CodeUtil.CODE_SUCCESS);
	// response.addProperty("errorMessage", CodeUtil.EMPTY_MSG);
	// return response;
	//
	// }

	public static boolean validateEmail(String email) {
		String regex = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(email);
		System.out.println(email + " : " + matcher.matches());
		return matcher.matches();
	}

	public static String formatDate(Long time) {
		Date date = Calendar.getInstance().getTime();
		date.setTime(time);
		return dateFormat.format(date);
	}

	public static String formatDateTime(Long time) {
		Date date = Calendar.getInstance().getTime();
		date.setTime(time);
		return dateTimeFormat.format(date);
	}

	public static String formatDecimalNumber(double number) {
		return decimalFormat.format(number);
	}

	public static String checkValidIntegerValue(String value) {

		try {
			if (value.trim().length() == 0)
				return "0";
		} catch (Exception e) {
			return "0";
		}

		return value;
	}

	public static String checkValidDoubleValue(String value) {

		try {
			if (value.trim().length() == 0)
				return "0.0";
		} catch (Exception e) {
			return "0.0";
		}

		return value;
	}

	public static String getFirstLetter(String word) {
		String resp = "";
		for (String s : word.trim().split("\\s+")) {
			if (s.length() > 0)
				resp += s.charAt(0);
		}
		return resp.toUpperCase();
	}

	public static String validateIntegerValue(String value) {
		try {
			Integer.parseInt(value);
			return Integer.toString(Integer.parseInt(value));
		} catch (Exception e) {
			return "0";
		}
	}

	public static String validateStringValue(String value) {
		try {
			if (value != null)
				if (value.trim().length() == 0)
					return "NA";
				else
					return value.trim();
			else
				return "NA";
		} catch (Exception e) {
			return "NA";
		}
	}

	public static boolean isNumber(String str) {
		return str.chars().allMatch(Character::isDigit);
	}

	public static String convertObjectToJSon(Object data) {
		ObjectMapper mapper = new ObjectMapper();
		JsonNode node = mapper.convertValue(data, JsonNode.class);
		String jsonString = null;
		try {
			jsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(node);
		} catch (JsonProcessingException e) {

			e.printStackTrace();
		}
		return jsonString;
	}

	enum Frequency {
		WEEKLY, MONTHLY, HOURLY, FORTNIGHTLY;
	}

	public enum Schedule {
		MINUTES, HOURS, DAY_OF_MONTH, MONTH, DAY_OF_WEEK, YEAR
	}

	public static OffsetDateTime parseOffset(String date) {
		OffsetDateTime oDate = null;

		if (!isEmpty(date)) {
			try {
				oDate = OffsetDateTime.parse(date, dateTimeFormatter);
			} catch (DateTimeParseException e) {
				return null;
			}

		} else {
			oDate = null;
		}

		return oDate;
	}

	public static String convertStandardDateFormat(String date) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Date dateo = null;
		try {
			dateo = formatter.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		SimpleDateFormat formatter1 = new SimpleDateFormat("dd MMM yyy HH:mm");
		String str = formatter1.format(dateo);
		return str;
	}

	public static String convertStandardToNormalDateFormat(String date) {
		SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyy HH:mm");
		Date dateo = null;
		try {
			dateo = formatter.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		String str = formatter1.format(dateo);
		return str;
	}

	public static Date convertDateOnTimeZoneForAWS(Date date, int hours, int minute) {
		Calendar mCal = Calendar.getInstance();
		mCal.setTime(date);
		DateTime dateTime = new DateTime(mCal.get(Calendar.YEAR), mCal.get(Calendar.MONTH) + 1,
				mCal.get(Calendar.DAY_OF_MONTH), mCal.get(Calendar.HOUR_OF_DAY), mCal.get(Calendar.MINUTE), 0, 0);
		dateTime = dateTime.minusHours(5).minusMinutes(30);
		return dateTime.toDate();
	}

	public static Date convertDateOnTimeZonePluseForAWS(Date date, int hours, int minute) {
		Calendar mCal = new GregorianCalendar();
		mCal.setTime(date);

		DateTime dateTime = new DateTime(mCal.get(Calendar.YEAR), mCal.get(Calendar.MONTH) + 1,
				mCal.get(Calendar.DAY_OF_MONTH), mCal.get(Calendar.HOUR_OF_DAY), mCal.get(Calendar.MINUTE), 0, 0);
		dateTime = dateTime.plusHours(hours).plusMinutes(minute);
		return dateTime.toDate();
	}

	public static String convertISTtoNormalDateFormat(String date) {
		SimpleDateFormat formatter = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");
		Date dateo = null;
		try {
			dateo = formatter.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		SimpleDateFormat formatter1 = new SimpleDateFormat("dd MMM yyy HH:mm");
		String str = formatter1.format(dateo);
		return str;
	}

	public static JsonObject decode(String token) {
		Base64 base64Url = new Base64(true);
		String decodeData = new String(base64Url.decode(token.split("\\.")[1]));
		return new Gson().fromJson(decodeData, JsonObject.class);
	}

	public static String elapsedTime(Date startedOn, Date completedOn) {
		String duration = null;
		long elapsedtime = 0L;
		long preElapsedtime = 0L;
		if (completedOn == null) {
			preElapsedtime = new Date().getTime() - startedOn.getTime();
			preElapsedtime = (preElapsedtime / (60 * 1000));
			duration = Long.toString(preElapsedtime);
		} else {
			elapsedtime = completedOn.getTime() - startedOn.getTime();
			elapsedtime = (elapsedtime / (60 * 1000));
			duration = Long.toString(elapsedtime);
		}
		return duration;
	}

	public static String encrypt(String value) {
		try {
			IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));
			SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");

			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
			cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);
			byte[] encrypted = cipher.doFinal(value.getBytes());

			return Base64.encodeBase64String(encrypted);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public static String decrypt(String encrypted) {
		try {
			IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));
			SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");

			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
			cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
			byte[] original = cipher.doFinal(Base64.decodeBase64(encrypted));

			return new String(original);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public static void createZip(File zipFileName, File[] selected) {

		try {
			byte[] buffer = new byte[1024];
			ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipFileName));
			out.setLevel(Deflater.DEFAULT_COMPRESSION);

			for (int i = 0; i < selected.length; i++) {
				if (!CodeUtils.isEmpty(selected[i])) {
					FileInputStream in = new FileInputStream(selected[i]);
					out.putNextEntry(new ZipEntry(selected[i].getName()));
					int len;
					while ((len = in.read(buffer)) > 0) {
						out.write(buffer, 0, len);
					}
					out.closeEntry();
					in.close();
				}
			}
			out.close();

		} catch (IllegalArgumentException iae) {
			iae.printStackTrace();
		} catch (FileNotFoundException fnfe) {
			fnfe.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}

	public static int calculatePercentage(String obtained, String total) {
		int obt = Integer.parseInt(obtained);
		int tot = Integer.parseInt(total);
		int result = 100 / tot * obt;
		return result;
	}

	public static void createFolder(String bucketName, String folderName, AmazonS3 client) {
		// create meta-data for your folder and set content-length to 0
		String SUFFIX = "";
		ObjectMetadata metadata = new ObjectMetadata();
		metadata.setContentLength(0);
		// create empty content
		InputStream emptyContent = new ByteArrayInputStream(new byte[0]);
		// create a PutObjectRequest passing the folder name suffixed by /
		PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, folderName + SUFFIX, emptyContent,
				metadata);
		// send request to S3 to create folder
		client.putObject(putObjectRequest);
	}

	public enum Module {
		Organisation, User, UserRole, Site, SiteMap, AdHocRequest, DemandPlanning, DemandPlanning_Summary,
		FinalAssortment, ExcelTemplate, DemandBudgetedWeekly, DemandBudgetedMonthly, DpUploadSummary, DemandBudgeted,
		DashBoard, Dash_Board
	}

	public enum Demand {
		BUDGETED
	}

	public enum Analytics {
		SalesTrend, SalesVsProfit
	}

	public static String getExtensionByApacheCommonLib(String filename) {
		return FilenameUtils.getExtension(filename);
	}

	public static String convertDemandForecastMonthlyDateFormat(String date) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM");
		Date dateo = null;
		try {
			dateo = formatter.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		SimpleDateFormat formatter1 = new SimpleDateFormat("MMM yyyy");
		String str = formatter1.format(dateo);
		return str;
	}

	public static String convertOTBMonthlyDateFormat(String date) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date dateo = null;
		try {
			dateo = formatter.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		SimpleDateFormat formatter1 = new SimpleDateFormat("MMM yyyy");
		String str = formatter1.format(dateo);
		return str;
	}

	public static String convertOTBMonthlySampleDateFormat(String date) {
		SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
		Date dateo = null;
		try {
			dateo = formatter.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		SimpleDateFormat formatter1 = new SimpleDateFormat("MMM yyyy");
		String str = formatter1.format(dateo);
		return str;
	}

	public static String convertOTBMonthlyFormat(String date) {
		SimpleDateFormat formatter = new SimpleDateFormat("MMM yyyy");
		Date dateo = null;
		try {
			dateo = formatter.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
		String str = formatter1.format(dateo);
		return str;
	}

	public static String convertOTByearmonthToyearmonthday(String date) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM");
		Date dateo = null;
		try {
			dateo = formatter.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
		String str = formatter1.format(dateo);
		return str;
	}

	//// \\\\

	//// \\\\

	public static String convertDataReferenceDateFormat(String date) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date dateo = null;
		try {
			dateo = formatter.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		SimpleDateFormat formatter1 = new SimpleDateFormat("dd-MMM-yyyy");
		String str = formatter1.format(dateo);
		return str;
	}

	public static String convertForecastDateFormat(String date) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date dateo = null;
		try {
			dateo = formatter.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM");
		String str = formatter1.format(dateo);
		return str;
	}

	public static Date addDay(Date date, int i) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DAY_OF_YEAR, i);
		return cal.getTime();
	}

	public static Date addMonth(Date date, int i) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MONTH, i);
		return cal.getTime();
	}

	public static Date addYear(Date date, int i) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.YEAR, i);
		return cal.getTime();
	}

	public static <T> List<List<T>> getPages(Collection<T> c, Integer pageSize) {
		if (c == null)
			return Collections.emptyList();
		List<T> list = new ArrayList<T>(c);
		if (pageSize == null || pageSize <= 0 || pageSize > list.size())
			pageSize = list.size();
		int numPages = (int) Math.ceil((double) list.size() / (double) pageSize);
		List<List<T>> pages = new ArrayList<List<T>>(numPages);
		for (int pageNum = 0; pageNum < numPages;)
			pages.add(list.subList(pageNum * pageSize, Math.min(++pageNum * pageSize, list.size())));
		return pages;
	}

	public static String convertDemandForecastDateFormat(String date) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date dateo = null;
		try {
			dateo = formatter.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		SimpleDateFormat formatter1 = new SimpleDateFormat("dd MMM yyyy");
		String str = formatter1.format(dateo);
		return str;
	}

	public static String convertNormalDateFormat(String date) {
		SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
		Date dateo = null;
		try {
			dateo = formatter.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
		String str = formatter1.format(dateo);
		return str;
	}

	public static String convertNormalDateFormatWithTime(String date) {
		SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy HH:mm");
		Date dateo = null;
		try {
			dateo = formatter.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		SimpleDateFormat formatter1 = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
		String str = formatter1.format(dateo);
		return str;
	}

	public static String CommaSeparatedNumberFormat(double number) {
		NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.ENGLISH);
		String numberAsString = numberFormat.format(number);
		return numberAsString;
	}

	@SuppressWarnings({ "deprecation" })
	public static boolean isValidDate(String inputDate, String dateFormat) {
		boolean flag = true;
		try {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
			Date date = new Date(inputDate);
			simpleDateFormat.format(date);
		} catch (Exception e) {
			flag = false;
		}
		return flag;
	}

	public static String getPOStringOrderDate() {

		OffsetDateTime currentTime = OffsetDateTime.now();
		currentTime = currentTime.plusHours(new Long(5));
		currentTime = currentTime.plusMinutes(new Long(30));

		String date = currentTime.format(DateTimeFormatter.ofPattern("ddMMYY/HHmmssSSS"));
		return date;
	}

	public static String getPODateString() {

		OffsetDateTime currentTime = OffsetDateTime.now();
		currentTime = currentTime.plusHours(new Long(5));
		currentTime = currentTime.plusMinutes(new Long(30));

		String date = currentTime.format(DateTimeFormatter.ofPattern("ddMMYY"));
		return date;
	}

	public static String getOffsetDateTimeHourMinute(OffsetDateTime date) {

		if (!CodeUtils.isEmpty(date)) {
			String offsetString = date.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
			return offsetString;
		} else {
			return null;
		}
	}

	public static String getOffsetDate(OffsetDateTime date) {

		if (!CodeUtils.isEmpty(date)) {
			String offsetString = date.format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
			return offsetString;
		} else {
			return null;
		}
	}

	public static OffsetDateTime getRealTime(OffsetDateTime date) {
		date = date.plusHours(new Long(5));
		date = date.plusMinutes(new Long(30));
		return date;
	}

	public static int getEightDigitRandomNumber() {
		Random rnd = new Random();
		int number = 10000000 + rnd.nextInt(90000000);
		return number;
	}

	public static int getSixDigitRandomNumber() {
		Random rnd = new Random();
		int number = 100000 + rnd.nextInt(900000);
		return number;
	}

	public enum PIValidation {
		UNIQUECODE, BRAND, PATTERN, ASSORTMENT, SEASON, FABRIC, DARWIN, WEAVED, SIZE, COLOR, HIERARACHY
	}

	public enum PICarDesType {
		CAT, CAT1, CAT2, CAT3, CAT4, CAT5, CAT6, DESC, DESC1, DESC2, DESC3, DESC4, DESC5, DESC6
	}

	public static boolean validateDate(String strDate, String dateFormat) {
		if (strDate.trim().equals(""))
			return true;
		else {
			SimpleDateFormat sdfrmt = new SimpleDateFormat(dateFormat);
			sdfrmt.setLenient(false);
			try {
				/* Date format is valid */
				Date javaDate = sdfrmt.parse(strDate);
			}
			/* Date format is invalid */
			catch (ParseException e) {
				return false;
			}
			/* Return true if date format is valid */
			return true;
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static List getAllMonths(String startDate, String endDate) {
		LocalDate date1 = new LocalDate(startDate);
		LocalDate date2 = new LocalDate(endDate);
		List dateList = new ArrayList();
		while (date1.isBefore(date2.plusMonths(1))) {
			dateList.add(date1.toString("MMM yyyy"));
			date1 = date1.plus(Period.months(1));
		}
		return dateList;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static List getAllNormalMonths(String startDate, String endDate) {
		LocalDate date1 = new LocalDate(startDate);
		LocalDate date2 = new LocalDate(endDate);
		List dateList = new ArrayList();
		while (date1.isBefore(date2.plusMonths(1))) {
			dateList.add(date1.toString("yyyy-MM-dd"));
			date1 = date1.plus(Period.months(1));
		}
		return dateList;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static List getOTBAllMonths(String startDate, String endDate) {
		LocalDate date1 = new LocalDate(startDate);
		LocalDate date2 = new LocalDate(endDate);
		List dateList = new ArrayList();
		while (date1.isBefore(date2)) {
			dateList.add(date1.toString("MMM yyyy"));
			date1 = date1.plus(Period.months(1));
		}
		return dateList;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static List getAllWeekFromMonday(String startDate, String endDate) {
		LocalDate date1 = new LocalDate(startDate);
		LocalDate date2 = new LocalDate(endDate);
		LocalDate thisMonday = date1.withDayOfWeek(DateTimeConstants.MONDAY);
		if (date1.isBefore(thisMonday)) {
			date1 = thisMonday.plusWeeks(1); // start on previous monday
		} else {
			date1 = thisMonday; // start on this monday
		}
		List dateList = new ArrayList();
		while (date1.isBefore(date2)) {
			dateList.add(date1.toString("dd MMM yyyy"));
			date1 = date1.plus(Period.weeks(1));
		}
		return dateList;
	}

	public static String checkStringType(String val) {

		String valType = null;
		String x = val.trim();
		try {
			int i = Integer.parseInt(x);
			valType = "Integer";
		} catch (NumberFormatException e) {
			try {
				float d = Float.parseFloat(x);
				valType = "Float";
			} catch (NumberFormatException e2) {
				try {
					double d = Double.parseDouble(x);
					valType = "Double";
				} catch (NumberFormatException e3) {
					valType = "String";
				}
			}
		}
		return valType;
	}

	/////
	public static Object convertStringToObject(String value) {
		Object newValue = 0;
		if (CodeUtils.checkStringType(value).equals("Float")) {
			newValue = Float.parseFloat(value);
		} else if (CodeUtils.checkStringType(value).equals("Double")) {
			newValue = Double.parseDouble(value);
		} else if (CodeUtils.checkStringType(value).equals("Integer")) {
			newValue = Integer.parseInt(value);
		}
		return newValue;
	}

	public static String commaSeperator(double number, int n) {
		String hashFormat = "#,###.";
		for (int i = 1; i <= n; i++) {
			hashFormat += "#";
		}
		DecimalFormat myFormatter = new DecimalFormat(hashFormat);
		String output = myFormatter.format(number);
		return output;

	}
	/////

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static List returnMisMatchList(List listValue1, List listValue2) {

		List misMatchList = new LinkedList<>();
		try {
			if (listValue1.size() > listValue2.size()) {
				listValue1.removeAll(listValue2);
				misMatchList = listValue1;
			} else if (listValue2.size() > listValue1.size()) {
				listValue2.removeAll(listValue1);
				misMatchList = listValue2;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return misMatchList;
	}

	public static String createPattern(String pattern) {

		char starting = 65;
		char ending = 90;

		int tempId = Integer.parseInt(pattern.substring(pattern.lastIndexOf("/") + 3)); // 0001
		String newId;
		if (tempId < 9) {
			newId = "000" + (++tempId);
		} else if (tempId < 99) {
			newId = "00" + (++tempId);
		} else if (tempId < 999) {
			newId = "0" + (++tempId);
		} else {
			newId = "" + (++tempId);
		}

		String newPattern = pattern.substring(0, pattern.lastIndexOf("/") + 1);

		String charString = pattern.substring(pattern.lastIndexOf("/") + 1, pattern.lastIndexOf("/") + 3); // AA
		char firstChar = charString.charAt(0); // A
		char secondChar = charString.charAt(1); // B

		if (tempId <= 9999) {
			newPattern = newPattern + firstChar + secondChar + newId;
		} else {
			if (secondChar < ending) {
				newPattern = pattern + firstChar + (++secondChar) + newId;
			} else {
				secondChar = starting;
				newPattern = newPattern + (++firstChar) + secondChar + newId;
			}

		}
		return newPattern;
	}

	public static List convertMaptoList(Map<String, String> data) {

		class KeyValueClass {
			private String key;
			private String value;

			public KeyValueClass() {

			}

			public KeyValueClass(String key, String value) {
				super();
				this.key = key;
				this.value = value;
			}

			public String getKey() {
				return key;
			}

			public void setKey(String key) {
				this.key = key;
			}

			public String getValue() {
				return value;
			}

			public void setValue(String value) {
				this.value = value;
			}

			@Override
			public String toString() {
				return "KeyValueClass [key=" + key + ", value=" + value + "]";
			}

		}
		List<KeyValueClass> convertList = new LinkedList<>();
		Set keys = data.keySet();
		Iterator itr = keys.iterator();
		int count = 1;
		while (itr.hasNext()) {
			KeyValueClass keyvalueClass = new KeyValueClass();
			String key = (String) itr.next();
			keyvalueClass.setKey("file_" + count);
			String value = data.get(key);
			keyvalueClass.setValue(value);
			convertList.add(keyvalueClass);
			count++;
		}
		return convertList;
	}

	public static List genericsMaptoList(Map<String, String> data) {

		class KeyValueClass {
			private String key;
			private String value;

			public KeyValueClass() {

			}

			public KeyValueClass(String key, String value) {
				super();
				this.key = key;
				this.value = value;
			}

			public String getKey() {
				return key;
			}

			public void setKey(String key) {
				this.key = key;
			}

			public String getValue() {
				return value;
			}

			public void setValue(String value) {
				this.value = value;
			}

			@Override
			public String toString() {
				return "KeyValueClass [key=" + key + ", value=" + value + "]";
			}

		}
		List<KeyValueClass> convertList = new LinkedList<>();
		Set keys = data.keySet();
		Iterator itr = keys.iterator();
		int count = 1;
		while (itr.hasNext()) {
			KeyValueClass keyvalueClass = new KeyValueClass();
			String key = (String) itr.next();
			keyvalueClass.setKey(key);
			String value = data.get(key);
			keyvalueClass.setValue(value);
			convertList.add(keyvalueClass);
			count++;
		}
		return convertList;
	}

	public static String getPreviousMonday(String startDate) {
		LocalDate date1 = new LocalDate(startDate);
		LocalDate thisMonday = date1.withDayOfWeek(DateTimeConstants.MONDAY);
		if (date1.isBefore(thisMonday)) {
			date1 = thisMonday.plusWeeks(1); // start on previous monday
		} else {
			date1 = thisMonday; // start on this monday
		}
		String previousMonday = date1.toString("yyyy-MM-dd");
		return previousMonday;
	}

	@SuppressWarnings("unchecked")
	public static ArrayNode toJSON(Object object, boolean isCustom) {
		ObjectMapper mapper = new ObjectMapper();

		ArrayNode arrayNode = mapper.createArrayNode();
		if (object instanceof List) {
			@SuppressWarnings("rawtypes")
			List list = (List) object;
			for (Object insideObject : list) {
				if (insideObject instanceof HashMap) {

					Map<String, String> map = (Map<String, String>) insideObject;
					for (Entry<String, String> entry : map.entrySet()) {
						try {
							ObjectNode objectNode = mapper.createObjectNode();
							if (isCustom) {
//								String key = entry.getKey();
								String assortmentCode = map.get("ASSORTMENT_CODE");
								if (!entry.getKey().equals("ASSORTMENT_CODE")) {
									objectNode.put("assortmentCode", assortmentCode);
									objectNode.put("billdate", convertDemandForecastDateFormat(entry.getKey()));
									objectNode.put("qty", entry.getValue());
									arrayNode.add(objectNode);
								}
							} else {
								objectNode.put(entry.getKey(), entry.getValue());
								arrayNode.add(objectNode);
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}

				}
			}

		}
		return arrayNode;
	}

	public static Map<String, String> convertFiscalYearToFiscalDate(String fiscalYear) {
		Map<String, String> fiscalDate = new HashMap<>();
		try {
			String[] years = fiscalYear.split("-");
			String startDate = years[0] + "-04-01";
			String endDate = years[1] + "-03-31";
			fiscalDate.put("startDate", startDate);
			fiscalDate.put("endDate", endDate);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return fiscalDate;
	}

	public static String getFirstDayOfTheMonth(String date) {
		String firstDayOfTheMonth = "";

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			java.util.Date dt = formatter.parse(date);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(dt);

			calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));

			java.util.Date firstDay = calendar.getTime();

			firstDayOfTheMonth = formatter.format(firstDay);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return firstDayOfTheMonth;
	}

	public static String getLastDayOfTheMonth(String date) {

		String lastDayOfTheMonth = "";

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			java.util.Date dt = formatter.parse(date);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(dt);

			calendar.add(Calendar.MONTH, 1);
			calendar.set(Calendar.DAY_OF_MONTH, 1);
			calendar.add(Calendar.DATE, -1);

			java.util.Date lastDay = calendar.getTime();

			lastDayOfTheMonth = formatter.format(lastDay);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return lastDayOfTheMonth;
	}

	public static String getTimeAgo(long time_ago) {
		time_ago = time_ago / 1000;
		long cur_time = (Calendar.getInstance().getTimeInMillis()) / 1000;
		long time_elapsed = cur_time - time_ago;
		long seconds = time_elapsed;
		// Seconds
		if (seconds <= 60) {
			return "Just now";
		}
		// Minutes
		else {
			int minutes = Math.round(time_elapsed / 60);

			if (minutes <= 60) {
				if (minutes == 1) {
					return "a minute ago";
				} else {
					return minutes + " minutes ago";
				}
			}
			// Hours
			else {
				int hours = Math.round(time_elapsed / 3600);
				if (hours <= 24) {
					if (hours == 1) {
						return "An hour ago";
					} else {
						return hours + " hrs ago";
					}
				}
				// Days
				else {
					int days = Math.round(time_elapsed / 86400);
					if (days <= 7) {
						if (days == 1) {
							return "Yesterday";
						} else {
							return days + " days ago";
						}
					}
					// Weeks
					else {
						int weeks = Math.round(time_elapsed / 604800);
						if (weeks <= 4.3) {
							if (weeks == 1) {
								return "A week ago";
							} else {
								return weeks + " weeks ago";
							}
						}
						// Months
						else {
							int months = Math.round(time_elapsed / 2600640);
							if (months <= 12) {
								if (months == 1) {
									return "A month ago";
								} else {
									return months + " months ago";
								}
							}
							// Years
							else {
								int years = Math.round(time_elapsed / 31207680);
								if (years == 1) {
									return "One year ago";
								} else {
									return years + " years ago";
								}
							}
						}
					}
				}
			}
		}
	}
	
	public static Date getCurrentTime() throws Exception {
		String TIME_SERVER = "time-a.nist.gov";
		NTPUDPClient timeClient = new NTPUDPClient();
		InetAddress inetAddress = InetAddress.getByName(TIME_SERVER);
		TimeInfo timeInfo = timeClient.getTime(inetAddress);
		long returnTime = timeInfo.getReturnTime();
		Date time = new Date(returnTime);
		return time;
	}

}
