package com.supplymint.util;

import java.io.IOException;
import java.time.OffsetDateTime;
import java.time.format.DateTimeParseException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class PILocalDatetimeSerializer extends JsonSerializer<OffsetDateTime> {

	@Override
	public void serialize(OffsetDateTime value, JsonGenerator gen, SerializerProvider provider) throws IOException {
		try {
			gen.writeString(value.toString());
		} catch (DateTimeParseException e) {
			//System.err.println(e);
			gen.writeString("null");
		} catch (Exception e) {
			//System.err.println(e);
			gen.writeString("null");
		}
	}
	
			
}



