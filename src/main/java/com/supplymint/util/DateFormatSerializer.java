package com.supplymint.util;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.supplymint.layer.business.contract.setting.GeneralSettingService;

@Service
public class DateFormatSerializer extends JsonSerializer<Date> {

	public static GeneralSettingService generalSettingService;

	@SuppressWarnings("static-access")
	@Autowired
	public DateFormatSerializer(GeneralSettingService generalSettingService) {
		this.generalSettingService = generalSettingService;
	}

	public DateFormatSerializer() {
		// TODO Auto-generated constructor stub
	}

	private SimpleDateFormat dateFormat = null;

	@Override
	public void serialize(Date date, JsonGenerator jsonGenerator, SerializerProvider provider) throws IOException {
		try {
		Map<String, String> data = generalSettingService.getAll();
		String newDateFormat = null;
		String newDate = data.get("dateFormat");
		String timeFormat = data.get("timeFormat");
		String[] parts = new String[newDate.length()];
		if (newDate.contains("HH")) {
			if (timeFormat.equalsIgnoreCase("24 Hour")) {
				newDateFormat = newDate;
			}
			if (timeFormat.equalsIgnoreCase("12 Hour")) {
				parts = newDate.split("H");
				String h2r = "hh" + parts[2];
				newDateFormat = parts[0] + h2r+" a";
			}
		}else {
			newDateFormat=newDate;
		}
		dateFormat = new SimpleDateFormat(newDateFormat);
		String dateString = dateFormat.format(date);
		jsonGenerator.writeString(dateString);
		}catch(Exception ex) {
			dateFormat=new SimpleDateFormat("dd MMM yyyy HH:mm");
			String dateString=dateFormat.format(date);
			jsonGenerator.writeString(dateString);
		}

	}

}