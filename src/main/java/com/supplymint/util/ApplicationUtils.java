package com.supplymint.util;

/**
 * @author Prabhakar Srivastava
 * @since 15 JAN 2019
 */
public interface ApplicationUtils {

	public interface DemandPlanning {

		enum ChooseType {
			MONTHLY, WEEKLY
		}

		enum DemandPlanningGraph {
			ACTUAL, PREDICTED, BUDGETED, DEMAND_ON_FORECAST
		}


	}

	public static interface DemandPlanningParameter {
		String PATTERN = "S/DP-FD/AA0001";
		final String OTB_PATTERN = "S/DP-OTB/AA0001";
		final String ACTIVEDATA_ASD = "UF_ASD";
		final String ACTIVEDATA_DFD = "UF_DFD";
		String ASSORTMENTCODE = "Assortment Code";
		String BILLDATE = "Bill Date";
		final String BUDGETED_QTY = "Budgeted QTY";
	}

	public interface AppEnvironment {
		enum Profiles {
			DEV, PROD , QA
		}
	}

	public enum User {
		USERNAME, PASSWORD
	}

	public enum EmailTemplateName {
		TO_SUCCESS_GENERATION, TO_FAILED_GENERATION, FORECAST_SUCCESS_GENERATION, FORECAST_FAILED_GENERATION, DATA_SYNC_FAILED
	}

	public enum CustomExtension {
		EXCEL, CSV, PDF, JPEG
	}

	public static interface CustomParameter {
		String SEPARATOR = "_";
		String DELIMETER = "/";
		String HYPHAN = "-";
	}

	public static interface BucketParameter {
		String GENERATE_EXCELCSV = "DOWNLOAD_EXCEL_CSV";
		String BUCKETPREFIX = "s3://";
		String FILE_NAME = "FILENAME";
		String FILE_PATH = "FILEPATH";
		String USER_BUCKET = "BUCKET";
	}

	public static enum Assortments {
		DEFAULT_ASSORTMENT
	}

	enum Module {
		ADMINISTRATION, DEMAND_PLANNING, ANALYTICS, PROCRUMENT, INVENTRY_PLANNING, VENDOR
	}
	
	public interface ModuleName {
		String ADMINISTRATION="ADMINISTRATION";
		String DEMAND_PLANNING="DEMAND_PLANNING";
		String ANALYTICS="ANALYTICS";
		String PROCRUMENT="PROCRUMENT";
		String INVENTRY_PLANNING="INVENTRY_PLANNING";
		String VENDOR="VENDOR";
	}
	
	public interface SubModuleName{
		String ASSORTMENT="ASSORTMENT";
	}

	public enum SubModule {
		ORGANISATION, SITE, SITEMAP, USER, USER_ROLE, AUTO_CONFIGURATION, RUN_ON_DEMAND, ASSORTMENT, ADHOC_REQUEST,
		FORECAST_ON_DEMAND, OTB, DATA_SYNC
	}

	public enum Days {
		SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY
	}

	public enum CurrencyParameters {
		AED("د.إ"), AFN("Af"), ALL("L"), AMD("Դ"), AOA("Kz"), ARS("$"), AUD("$"), AWG("ƒ"), AZN("ман"), BAM("КМ"),
		BBD("$"), BDT("৳"), BGN("лв"), BHD("ب.د"), BIF("₣"), BMD("$"), BND("$"), BOB("Bs."), BRL("R$"), BSD("$"),
		BTN(""), BWP("P"), BYR("Br"), BZD("$"), CAD("$"), CDF("₣"), CHF("₣"), CLP("$"), CNY("¥"), COP("$"), CRC("₡"),
		CUP("$"), CVE("$"), CZK("Kč"), DJF("₣"), DKK("kr"), DOP("$"), DZD("د.ج"), EGP("£"), ERN("Nfk"), ETB(""),
		EUR("€"), FJD("$"), FKP("£"), GBP("£"), GEL("ლ"), GHS("₵"), GIP("£"), GMD("D"), GNF("₣"), GTQ("Q"), GYD("$"),
		HKD("$"), HNL("L"), HRK("Kn"), HTG("G"), HUF("Ft"), IDR("Rp"), ILS("₪"), INR("₹"), IQD("ع.د"), IRR("﷼"),
		ISK("Kr"), JMD("$"), JOD("د.ا"), JPY("¥"), KES("Sh"), KGS(""), KHR("៛"), KPW("₩"), KRW("₩"), KWD("د.ك"),
		KYD("$"), KZT("〒"), LAK("₭"), LBP("ل.ل"), LKR("Rs"), LRD("$"), LSL("L"), LYD("ل.د"), MAD("د.م."), MDL("L"),
		MGA(""), MKD("ден"), MMK("K"), MNT("₮"), MOP("P"), MRO("UM"), MUR("₨"), MVR("ރ."), MWK("MK"), MXN("$"),
		MYR("RM"), MZN("MTn"), NAD("$"), NGN("₦"), NIO("C$"), NOK("kr"), NPR("₨"), NZD("$"), OMR("ر.ع."), PAB("B/."),
		PEN("S/."), PGK("K"), PHP("₱"), PKR("₨"), PLN("zł"), PYG("₲"), QAR("ر.ق"), RON("L"), RSD("din"), RUB("р."),
		RWF("₣"), SAR("ر.س"), SBD("$"), SCR("₨"), SDG("£"), SEK("kr"), SGD("$"), SHP("£"), SLL("Le"), SOS("Sh"),
		SRD("$"), STD("Db"), SYP("ل.س"), SZL("L"), THB("฿"), TJS("ЅМ"), TMT("m"), TND("د.ت"), TOP("T$"), TRY("£"),
		TTD("$"), TWD("$"), TZS("Sh"), UAH("₴"), UGX("Sh"), USD("$"), UYU("$"), UZS(""), VEF("Bs F"), VND("₫"),
		VUV("Vt"), WST("T"), XAF("₣"), XCD("$"), XPF("₣"), YER("﷼"), ZAR("R"), ZMW("ZK"), ZWL("$");

		public String currency;

		CurrencyParameters(String currency) {
			this.currency = currency;
		}

		public String getCurrency() {
			return currency;
		}

		public void setCurrency(String currency) {
			this.currency = currency;
		}

	}

	public static enum CustomRunState {
		STOPPED, RUNNING, Failed, SUCCESS, FAILED, SUCCEEDED, Succeeded, Exception, STARTING, STOPPING, TIMEOUT,
		Processing, INPROGRESS, PROGRESS, ERROR
	}

	public enum Conditions {
		EQUALS, AND, ANY, TRUE, FALSE, OR, Active, Inactive, status, YES, NO, NA
	}

	public enum HLevel {
		HL1NAME, HL2NAME, HL3NAME, HL4NAME, HL5NAME, HL6NAME
	}

	public static enum EmailSubModule {
		EMAIL_RUN_ON_DEMAND_SUBMODULE("TO_SUCCESS_GENERATION"),
		EMAIL_AUTO_CONFIGURATION_SUBMODULE("TO_SUCCESS_GENERATION"),
		FORECAST_ON_DEMAND_SUBMODULE("FORECAST_SUCCESS_GENERATION");

		public String subModule;

		EmailSubModule(String subModule) {
			this.subModule = subModule;
		}

		public String getSubModule() {
			return subModule;
		}

		public void setSubModule(String subModule) {
			this.subModule = subModule;
		}

	}

	public static interface EmailSubject {
		String TO_SUCCESS_GENERATED = "Transfer Order Generated";
		String FORECAST_SUCCESS_GENERATED = "Forecast Generated";
		String TO_FAILED_GENERATED = "Transfer Order Failed";
		String FORECAST_FAILED_GENERATED = "Forecast Failed";
		String DATA_SYNC_FAILED="Data Sync Failed";
	}

	public static interface DateFormatOTB {
		String DATEMONTHYEARHYPHEN = "dd-MM-yyyy";
		String DATEWITHFORWORDSLASH = "dd/MM/yyyy";
		String DATEWITHYEARFIRST = "yyyy/MM/dd";
		String DATEWITHYEARMONTHDATE = "yyyy-mm-dd";
		String DATEWITHMONTHDATEYEAR = "MM/dd/yyyy";
		String DATEWITHDATEMONTHYEAR = "dd-MMM-yyyy";

	}

	public enum AssortmentParameter {
		HL1NAME("Division"), HL2NAME("Section"), HL3NAME("Department"), HL4NAME("Article"), BRAND("Brand"),
		COLOR("Color"), SIZE1("Size"), PATTERN1("Pattern"), MATERIAL("Material"), DESIGN("Design"), TYPE1("Type"),
		SELL_PRICE("Mrp"), UDF6("Attribute6");

		public String defaultAssortment;

		AssortmentParameter(String defaultAssortment) {
			this.defaultAssortment = defaultAssortment;
		}
		public String getDefaultAssortment() {
			return defaultAssortment;
		}
		public void setDefaultAssortment(String defaultAssortment) {
			this.defaultAssortment = defaultAssortment;
		}
	}
	
	enum TenantHash{
		VMART("f71b609842ca103b916ceec05d9cc8ff8a354b77da12e425f49166dc1cf41c5e"),
		STYLE_BAAZAR("aea9647d0e25b55a8edee8d42e1187c71d697fe02eae2e723f439b15128e7843"),
		TCLOUD("d16c149919433dbcd4ba5c3255ae8fe1c906a99b967f5193f19ecd4bd4b51901"),
		CITYKART("2f197d441a3e6aa97a736485efdb4c7810b20ccafdac5340f34064113f2ae120"),
		MEGASHOP("fd40841a596821e2b0d5efff6b7a69f6bf8fe5bdbb525fbac609c22d2dfd93f3"),
		CORE("33a6e2ffd86e1c92f89b1d979dc7466f59f6760758c77e2866e3f32bc2e39383");
		private String hashKey;
		private TenantHash() {
		}
		private TenantHash(String hashKey) {
			this.hashKey = hashKey;
		}
		public String getHashKey() {
			return hashKey;
		}
		public void setHashKey(String hashKey) {
			this.hashKey = hashKey;
		}
	}
	
	public interface DefaultParameter{
		String OTB_VALUE="50000";
	}

	enum FiscalYear {
		PREVIOUS_FISCAL_YEAR, CURRENT_FISCAL_YEAR, NEXT_FISCAL_YEAR
	}

	enum Quarter {
		Q1("1"), Q2("2"), Q3("3"), Q4("4");
		private String qtr;

		private Quarter(String qtr) {
			this.qtr = qtr;
		}

		public String getQtr() {
			return qtr;
		}

		public void setQtr(String qtr) {
			this.qtr = qtr;
		}
	}
}
