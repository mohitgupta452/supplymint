package com.supplymint.util;

import java.io.IOException;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.supplymint.layer.business.contract.setting.GeneralSettingService;

@Service
public class CurrencySerializer extends JsonSerializer<String> {

	public static GeneralSettingService generalSettingService;

	@SuppressWarnings("static-access")
	@Autowired
	public CurrencySerializer(GeneralSettingService generalSettingService) {
		this.generalSettingService = generalSettingService;
	}

	public CurrencySerializer() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void serialize(String value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
		String currencyValue = null;
		try {
			Map<String, String> data = generalSettingService.getAll();
			String currencySymbolFormat = data.get("currencyUseSymbol");
			if (currencySymbolFormat.equalsIgnoreCase("TRUE")) {
				String currencyFormat = data.get("currency");
				if (currencyFormat.equals("CAN")) {
					currencyValue = "$ " + value;
				} else if (currencyFormat.equals("USD")) {
					currencyValue = "$ " + value;
				} else if (currencyFormat.equals("EUR")) {
					currencyValue = "€ " + value;
				} else {
					currencyValue = "₹ " + value;
				}
			} else {
				currencyValue = "₹ " + value;
			}
			gen.writeString(currencyValue);
		} catch (Exception ex) {
			currencyValue = "₹ " + value;
			gen.writeString(currencyValue);
		}
	}
}
