package com.supplymint.util;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class StringToNumberSerializer extends JsonSerializer<String>{

	@Override
	public void serialize(String value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
		
		if(CodeUtils.checkStringType(value).equals("Float")) {
			Float floatValue=Float.parseFloat(value);
			gen.writeNumber(floatValue);
		}else if(CodeUtils.checkStringType(value).equals("Double")) {
			Double doubleValue=Double.parseDouble(value);
			gen.writeNumber(doubleValue);
		}else if(CodeUtils.checkStringType(value).equals("Integer")) {
			Integer integerValue=Integer.parseInt(value);
			gen.writeNumber(integerValue);
		}
	}
}
