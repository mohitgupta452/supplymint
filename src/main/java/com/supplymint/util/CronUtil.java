package com.supplymint.util;

import java.util.Calendar;
import java.util.Date;

import org.joda.time.DateTime;

/**
 * Cron Util help to generate cron expression which is a string consisting of
 * six or seven subexpressions (fields) that describe individual details of the
 * schedule.
 * 
 * @author Manoj Singh
 * @since 25 OCT 2018
 * @version 1.1
 *
 */
public class CronUtil {

	private final Date mDate;
	private final Calendar mCal;
	private final String mSeconds = "0";
	private final String hour = "0";
	private final String minute = "0";

	private String mMins;
	private String mHours;
	private String mDaysOfMonth;
	private String mMonths;
	private String mYears;
	private String mDaysOfWeek;

	public CronUtil(Date pDate) {
		this.mDate = pDate;
		mCal = Calendar.getInstance();
		this.generateCronExpression();
	}

	private void generateCronExpression() {
		mCal.setTime(mDate);

		// Code to manage UTC+5:30
		int hour = Calendar.HOUR_OF_DAY;
		int minute = Calendar.MINUTE;
		int months = mCal.MONTH;

		DateTime dateTime = new DateTime(mCal.get(Calendar.YEAR), mCal.get(Calendar.MONTH) + 1,
				mCal.get(Calendar.DAY_OF_MONTH), mCal.get(Calendar.HOUR_OF_DAY), mCal.get(Calendar.MINUTE), 0, 0);

		dateTime = dateTime.minusHours(5).minusMinutes(30);
		String hours = String.valueOf(dateTime.getHourOfDay());
		this.mHours = hours;

		String mins = String.valueOf(dateTime.getMinuteOfHour());
		this.mMins = mins;

		String days = String.valueOf(dateTime.getDayOfMonth());
		this.mDaysOfMonth = days;

		String month = String.valueOf(dateTime.getMonthOfYear());
//		String months = new java.text.SimpleDateFormat("MM").format();
		this.mMonths = month;

		String daysOfWeek = String.valueOf(dateTime.getDayOfWeek());
		this.mDaysOfWeek = daysOfWeek;

		String years = String.valueOf(dateTime.getYear());
		this.mYears = years;

	}

	public String getHour() {
		return hour;
	}

	public String getMinute() {
		return minute;
	}

	public Date getDate() {
		return mDate;
	}

	public String getSeconds() {
		return mSeconds;
	}

	public String getMins() {
		return mMins;
	}

	public String getDaysOfWeek() {
		return mDaysOfWeek;
	}

	public String getHours() {
		return mHours;
	}

	public String getDaysOfMonth() {
		return mDaysOfMonth;
	}

	public String getMonths() {
		return mMonths;
	}

	public String getYears() {
		return mYears;
	}

}