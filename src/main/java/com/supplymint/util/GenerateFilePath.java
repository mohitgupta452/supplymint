package com.supplymint.util;


/**
 * @Authhor Manoj Singh
 * @Date 27-Sep-2018
 * @Version 1.0
 */
public class GenerateFilePath {

	public static String filePath() {
		String operSys = System.getProperty("os.name").toLowerCase();
		String parentDir = null;

		if (operSys.contains("win")) {
			parentDir = "c:\\supplymint";
		} else if (operSys.contains("nux") || operSys.contains("nix") || operSys.contains("aix")) {
			parentDir = System.getProperty("user.home") + "/supplymint";
		}
		return parentDir;

	}
}
