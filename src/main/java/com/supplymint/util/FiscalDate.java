package com.supplymint.util;

import java.util.Calendar;
import java.util.Date;
/**
 * @author Prabhakar Srivastava
 * @since 13 Jun 2019
 * @version 1.0
 */
public class FiscalDate {

    private static final int  FIRST_FISCAL_MONTH  = Calendar.MARCH;

    private Calendar calendarDate;

    public FiscalDate(Calendar calendarDate) {
        this.calendarDate = calendarDate;
    }

    public FiscalDate(Date date) {
        this.calendarDate = Calendar.getInstance();
        this.calendarDate.setTime(date);
    }

    public int getFiscalMonth() {
        int month = calendarDate.get(Calendar.MONTH);
        int result = ((month - FIRST_FISCAL_MONTH - 1) % 12) + 1;
        if (result < 0) {
            result += 12;
        }
        return result;
    }

    public int getFiscalYear() {
        int month = calendarDate.get(Calendar.MONTH);
        int year = calendarDate.get(Calendar.YEAR);
        return (month >= FIRST_FISCAL_MONTH) ? year : year - 1;
    }

    public int getCalendarMonth() {
        return calendarDate.get(Calendar.MONTH);
    }

    public int getCalendarYear() {
        return calendarDate.get(Calendar.YEAR);
    }

    @SuppressWarnings("unused")
	private static Calendar setDate(int year, int month, int day) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, day);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);

        return calendar;
    }
    
    public static Date getFirstDayOfQuarter(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.MONTH, cal.get(Calendar.MONTH)/3 * 3);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        return cal.getTime();
    }
    
    public static Date getLastDayOfQuarter(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.MONTH, cal.get(Calendar.MONTH)/3 * 3 + 2);
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
        return cal.getTime();
    }
    
    public static int getQuarterValue(Date date) {
    	Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        switch (cal.MONTH) {
		case 1:
		case 2:
		case 3:
			return 4;
		case 4:
		case 5:
		case 6:
			return 1;
		case 7:
		case 8:
		case 9:
			return 2;
		case 10:
		case 11:
		case 12:
			return 3;
		default:
			break;
		}
    	return 0;
    }
    
    public static void main(String[] args) {
    	Calendar cal = Calendar.getInstance();
//		String startDate = CodeUtils.__dateFormat.format(cal.getTime());
    	String startDate="2019-02-01";
    	int value=getQuarterValue(DateUtils.stringToDate(startDate));
		Date currentQuarter=FiscalDate.getFirstDayOfQuarter(DateUtils.stringToDate(startDate));
		cal.setTime(currentQuarter);
		cal.add(Calendar.MONTH, -1);
		System.out.println(currentQuarter);
		System.out.println(cal.getTime());
		Date previousQuarter=FiscalDate.getFirstDayOfQuarter(cal.getTime());
		System.out.println(previousQuarter);
    }

}