package com.supplymint.util;

import static java.nio.file.StandardOpenOption.CREATE_NEW;

/**
 * @author Prabhakar Srivastava
 *
 */

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.supplymint.layer.business.contract.s3Bucket.S3BucketInfoService;
import com.supplymint.layer.business.service.s3bucketService.S3WrapperService;
import com.supplymint.layer.data.tenant.inventory.entity.S3BucketInfo;
import com.supplymint.util.ApplicationUtils.BucketParameter;
import com.supplymint.util.ApplicationUtils.CustomParameter;

@Service
public final class UploadUtils {

	public static S3WrapperService s3Wrapper;

	public static S3BucketInfoService s3BucketInfoService;

	@SuppressWarnings("static-access")
	@Autowired
	public UploadUtils(S3WrapperService s3Wrapper, S3BucketInfoService s3BucketInfoService) {
		this.s3Wrapper = s3Wrapper;
		this.s3BucketInfoService = s3BucketInfoService;
	}

	public static Map<String, String> uploadBase64Image(String file, String fileName, String bucketType,
			HttpServletRequest request) throws IOException {

		String fName = fileName;
		String extension = CodeUtils.getExtensionByApacheCommonLib(fName);
		String url = null;
//		BufferedOutputStream stream;
		String filePath = null;
		File uploadFile = null;

		String token = ((HttpServletRequest) request).getHeader("X-Auth-Token");
		JsonArray orgDetails = CodeUtils.decode(token).get("ORG_DETAILS").getAsJsonArray();

		JsonObject jNode = orgDetails.get(0).getAsJsonObject();
		String orgId = jNode.get("orgId").getAsString();

		Map<String, String> imageMap = new HashMap<String, String>();
		S3BucketInfo s3BucketInfo = s3BucketInfoService.getBucketByType(bucketType, orgId);
		if (!file.isEmpty() && !CodeUtils.isEmpty(s3BucketInfo)) {
			String randomFolder = UUID.randomUUID().toString();
			String s3BucketFilePath = s3BucketInfo.getBucketPath() + CustomParameter.DELIMETER + randomFolder;
			fName = s3BucketInfo.getEnterprise() + CustomParameter.SEPARATOR
					+ CodeUtils.______dateFormat.format(new Date());

			String bucketPath = s3BucketFilePath + CustomParameter.DELIMETER + fName + "." + extension;
			filePath = FileUtils.createFilePath(FileUtils.getPlatformBasedParentDir(), bucketPath);

			String base64Image = file.split(",")[1];

			byte[] imageBytes = javax.xml.bind.DatatypeConverter.parseBase64Binary(base64Image);

			try {
				Path path = Paths.get(filePath);
				try (OutputStream stream = Files.newOutputStream(path, CREATE_NEW)) {
					stream.write(imageBytes);
					stream.close();
				} catch (FileAlreadyExistsException incrCounterAndRetry) {
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
//			stream = new BufferedOutputStream(new FileOutputStream(filePath));
//			stream.write(imageBytes);
//			stream.close();
			uploadFile = new File(filePath);
			String etag = s3Wrapper.upload(uploadFile, bucketPath, s3BucketInfo.getS3bucketName()).getETag();

			if (!CodeUtils.isEmpty(etag)) {
				s3BucketFilePath = BucketParameter.BUCKETPREFIX + s3BucketInfo.getS3bucketName()
						+ CustomParameter.DELIMETER + s3BucketFilePath;
				imageMap.put("bucketKey", s3BucketFilePath);
				s3BucketFilePath = s3BucketFilePath.split("\\//")[1]
						.replaceAll(s3BucketInfo.getS3bucketName() + CustomParameter.DELIMETER, "")
						+ CustomParameter.DELIMETER.trim().toString();
				url = s3Wrapper.downloadPreSignedURLOnHistory(s3BucketInfo.getS3bucketName(), bucketPath,
						Integer.parseInt(s3BucketInfo.getTime()));
				imageMap.put("url", url);
			}
		}
		return imageMap;
	}

	public static Map<String, String> uploadImageFile(MultipartFile file, String bucketType, HttpServletRequest request)
			throws IOException {

		String extension = CodeUtils.getExtensionByApacheCommonLib(file.getOriginalFilename());
		String url = null;
//		BufferedOutputStream stream;
		String filePath = null;
		String fileName = null;
		File uploadFile = null;
		Map<String, String> imageMap = new HashMap<String, String>();

		String token = ((HttpServletRequest) request).getHeader("X-Auth-Token");
		JsonArray orgDetails = CodeUtils.decode(token).get("ORG_DETAILS").getAsJsonArray();

		JsonObject jNode = orgDetails.get(0).getAsJsonObject();
		String orgId = jNode.get("orgId").getAsString();

		S3BucketInfo s3BucketInfo = s3BucketInfoService.getBucketByType(bucketType, orgId);

		if (!file.isEmpty() && !CodeUtils.isEmpty(s3BucketInfo)) {

			String randomFolder = UUID.randomUUID().toString();
			String s3BucketFilePath = s3BucketInfo.getBucketPath() + CustomParameter.DELIMETER + randomFolder;
			fileName = s3BucketInfo.getEnterprise() + CustomParameter.SEPARATOR
					+ CodeUtils.______dateFormat.format(new Date());

			String bucketPath = s3BucketFilePath + CustomParameter.DELIMETER + fileName + "." + extension;
			filePath = FileUtils.createFilePath(FileUtils.getPlatformBasedParentDir(), bucketPath);

			byte[] imageBytes = file.getBytes();

			try {
				Path path = Paths.get(filePath);
				try (OutputStream stream = Files.newOutputStream(path, CREATE_NEW)) {
					stream.write(imageBytes);
					stream.close();
				} catch (FileAlreadyExistsException incrCounterAndRetry) {
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
//			stream = new BufferedOutputStream(new FileOutputStream(filePath));
//			stream.write(imageBytes);
//			stream.close();
			uploadFile = new File(filePath);
			String etag = s3Wrapper.upload(uploadFile, bucketPath, s3BucketInfo.getS3bucketName()).getETag();

			if (!CodeUtils.isEmpty(etag)) {
				s3BucketFilePath = BucketParameter.BUCKETPREFIX + s3BucketInfo.getS3bucketName()
						+ CustomParameter.DELIMETER + s3BucketFilePath;
				imageMap.put("bucketKey", s3BucketFilePath);
				s3BucketFilePath = s3BucketFilePath.split("\\//")[1]
						.replaceAll(s3BucketInfo.getS3bucketName() + CustomParameter.DELIMETER, "")
						+ CustomParameter.DELIMETER.trim().toString();
				url = s3Wrapper.downloadPreSignedURLOnHistory(s3BucketInfo.getS3bucketName(), bucketPath,
						Integer.parseInt(s3BucketInfo.getTime()));
				imageMap.put("url", url);
			}
		}
		return imageMap;
	}

	public static Map<String, String> uploadFile(File file, String bucketType, HttpServletRequest request)
			throws IOException {

		String fileName = file.getAbsolutePath().substring(file.getAbsolutePath().lastIndexOf("/") + 1);
		String url = null;
		String filePath = null;
		Map<String, String> fileMap = new HashMap<String, String>();

		String token = ((HttpServletRequest) request).getHeader("X-Auth-Token");
		JsonArray orgDetails = CodeUtils.decode(token).get("ORG_DETAILS").getAsJsonArray();

		JsonObject jNode = orgDetails.get(0).getAsJsonObject();
		String orgId = jNode.get("orgId").getAsString();

		S3BucketInfo s3BucketInfo = s3BucketInfoService.getBucketByType(bucketType, orgId);

		if (!CodeUtils.isEmpty(file) && !CodeUtils.isEmpty(s3BucketInfo)) {

			String randomFolder = UUID.randomUUID().toString();
			String s3BucketFilePath = s3BucketInfo.getBucketPath() + CustomParameter.DELIMETER + randomFolder;

			String bucketPath = s3BucketFilePath + CustomParameter.DELIMETER + fileName;
			filePath = FileUtils.createFilePath(FileUtils.getPlatformBasedParentDir(), bucketPath);

			String etag = s3Wrapper.upload(file, bucketPath, s3BucketInfo.getS3bucketName()).getETag();

			if (!CodeUtils.isEmpty(etag)) {
				s3BucketFilePath = BucketParameter.BUCKETPREFIX + s3BucketInfo.getS3bucketName()
						+ CustomParameter.DELIMETER + s3BucketFilePath;
				fileMap.put("bucketKey", s3BucketFilePath);
				s3BucketFilePath = s3BucketFilePath.split("\\//")[1]
						.replaceAll(s3BucketInfo.getS3bucketName() + CustomParameter.DELIMETER, "")
						+ CustomParameter.DELIMETER.trim().toString();
				url = s3Wrapper.downloadPreSignedURLOnHistory(s3BucketInfo.getS3bucketName(), bucketPath,
						Integer.parseInt(s3BucketInfo.getTime()));
				fileMap.put("url", url);
			}
		}
		return fileMap;
	}

	public static String downloadHistory(String bucketKey, String bucketType, ServletRequest request) {
		String url = null;
		String bKey = bucketKey;
		String token = ((HttpServletRequest) request).getHeader("X-Auth-Token");

		JsonArray orgDetails = CodeUtils.decode(token).get("ORG_DETAILS").getAsJsonArray();

		JsonObject jNode = orgDetails.get(0).getAsJsonObject();
		String orgId = jNode.get("orgId").getAsString();

		String bucketName = CodeUtils.decode(token).get(BucketParameter.USER_BUCKET).getAsString();
		S3BucketInfo s3BucketInfo = s3BucketInfoService.getBucketByType(bucketType, orgId);
		if (!CodeUtils.isEmpty(bKey)) {
			String expire = s3BucketInfo.getTime();
			bKey = bKey.split("\\//")[1].replaceAll(bucketName + CustomParameter.DELIMETER, "")
					+ CustomParameter.DELIMETER.trim().toString();
			url = s3Wrapper.downloadPreSignedURLOnHistory(bucketName, bKey, Integer.parseInt(expire));
		}
		return url;
	}

	public static Map<String, String> uploadFile(File file, String bucketType, String orgId) throws IOException {

		String fileName = file.getAbsolutePath().substring(file.getAbsolutePath().lastIndexOf("/") + 1);
		String url = null;
		String filePath = null;
		Map<String, String> fileMap = new HashMap<String, String>();
		S3BucketInfo s3BucketInfo = s3BucketInfoService.getBucketByType(bucketType, orgId);

		if (!CodeUtils.isEmpty(file) && !CodeUtils.isEmpty(s3BucketInfo)) {

			String randomFolder = UUID.randomUUID().toString();
			String s3BucketFilePath = s3BucketInfo.getBucketPath() + CustomParameter.DELIMETER + randomFolder;

			String bucketPath = s3BucketFilePath + CustomParameter.DELIMETER + fileName;
			filePath = FileUtils.createFilePath(FileUtils.getPlatformBasedParentDir(), bucketPath);

			String etag = s3Wrapper.upload(file, bucketPath, s3BucketInfo.getS3bucketName()).getETag();

			if (!CodeUtils.isEmpty(etag)) {
				fileMap.put("bucketPath", s3BucketFilePath);
				s3BucketFilePath = BucketParameter.BUCKETPREFIX + s3BucketInfo.getS3bucketName()
						+ CustomParameter.DELIMETER + s3BucketFilePath;
				fileMap.put("bucketKey", s3BucketFilePath);
				fileMap.put("bucketName", s3BucketInfo.getS3bucketName());
				s3BucketFilePath = s3BucketFilePath.split("\\//")[1]
						.replaceAll(s3BucketInfo.getS3bucketName() + CustomParameter.DELIMETER, "")
						+ CustomParameter.DELIMETER.trim().toString();
				url = s3Wrapper.downloadPreSignedURLOnHistory(s3BucketInfo.getS3bucketName(), bucketPath,
						Integer.parseInt(s3BucketInfo.getTime()));
				fileMap.put("url", url);
			}
		}
		return fileMap;
	}

	public static String downloadHistory(String bucketKey, String bucketType, String orgId) {
		String url = null;
		String bKey = bucketKey;

		S3BucketInfo s3BucketInfo = s3BucketInfoService.getBucketByType(bucketType, orgId);
		String bucketName = s3BucketInfo.getS3bucketName();
		if (!CodeUtils.isEmpty(bKey)) {
			String expire = s3BucketInfo.getTime();
			bKey = bKey.split("\\//")[1].replaceAll(bucketName + CustomParameter.DELIMETER, "")
					+ CustomParameter.DELIMETER.trim().toString();
			url = s3Wrapper.downloadPreSignedURLOnHistory(bucketName, bKey, Integer.parseInt(expire));
		}
		return url;
	}

}
