package com.supplymint.util;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.supplymint.layer.business.contract.setting.GeneralSettingService;

@Service
public class StringDateSerializer extends JsonSerializer<String> {
	
	public static GeneralSettingService generalSettingService;

	@SuppressWarnings("static-access")
	@Autowired
	public StringDateSerializer(GeneralSettingService generalSettingService) {
		this.generalSettingService = generalSettingService;
	}

	public StringDateSerializer() {
		// TODO Auto-generated constructor stub
	}

	private SimpleDateFormat dateFormat = null;


	@Override
	public void serialize(String value, JsonGenerator jsonGenerator, SerializerProvider serializers) throws IOException {
		
		Date userDateValue=DateUtils.stringToDate(value);
		try {
			Map<String, String> data = generalSettingService.getAll();
			String newDateFormat = null;
			String newDate = data.get("dateFormat");
			String timeFormat = data.get("timeFormat");
			String[] splitDate = new String[newDate.length()];
			if (newDate.contains("HH")) {
				if (timeFormat.equalsIgnoreCase("24 Hour")) {
					newDateFormat = newDate;
				}
				if (timeFormat.equalsIgnoreCase("12 Hour")) {
					splitDate = newDate.split("H");
					String splitHour = "hh" + splitDate[2];
					newDateFormat = splitDate[0] + splitHour+" a";
				}
			}else {
				newDateFormat=newDate;
			}
			dateFormat = new SimpleDateFormat(newDateFormat);
			String dateString = dateFormat.format(userDateValue);
			jsonGenerator.writeString(dateString);
			}catch(Exception ex) {
				dateFormat=new SimpleDateFormat("dd MMM yyyy HH:mm");
				String dateString=dateFormat.format(userDateValue);
				jsonGenerator.writeString(dateString);
			}
	}

}