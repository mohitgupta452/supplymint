package com.supplymint.util;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.poi.ss.formula.functions.T;
import org.springframework.boot.configurationprocessor.json.JSONArray;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

/**
 * @Authhor Prabhakar Srivastava
 * @Date 18-Jun-2019
 * @Version 1.0
 */
public class JsonUtils {
	
	public static T jsonToPojo(String json) {
		T pojo = null;
		try {
			ObjectMapper mapper = new ObjectMapper();
			pojo = mapper.readValue(json, T.class);
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return pojo;
	}
	
	public static String objectToPrettyJson(Object object) {
		String json = null;
		try {
			ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
			json = ow.writeValueAsString(object);
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return json;
	}
	
	public static String objectToSingleLineJson(Object object) {
		String json = null;
		try {
			ObjectMapper mapper = new ObjectMapper();
			json = mapper.writeValueAsString(object);
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return json;
	}
	
	@SuppressWarnings({ "rawtypes", "unused" })
	public static String toJSON(List list ) throws JSONException, IllegalAccessException {
        JSONArray jsonArray = new JSONArray();
        for (Object i : list) {
            String jstr = toJSON(i);
            JSONObject jsonObject = new JSONObject(jstr);
            jsonArray.put(jsonArray);
        }
        return jsonArray.toString();
    }
	
	@SuppressWarnings("rawtypes")
	public static String toJSON(Object object) throws JSONException, IllegalAccessException {
        Class c = object.getClass();
        JSONObject jsonObject = new JSONObject();
        for (Field field : c.getDeclaredFields()) {
            field.setAccessible(true);
            String name = field.getName();
            String value = String.valueOf(field.get(object));
            jsonObject.put(name, value);
        }
        return jsonObject.toString();
    }
	
	@SuppressWarnings("unchecked")
	public static Map<String, Object> jsonToObject(String json) {
		Map<String, Object> map = null;
		try {
			ObjectMapper mapper = new ObjectMapper();
			map = mapper.readValue(json, Map.class);
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return map;
	}
	
	public static List<T> jsonArrayToList(String json) {
		List<T> list = null;
		try {
			ObjectMapper mapper = new ObjectMapper();
			list = Arrays.asList(mapper.readValue(json, T[].class));
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public static Map<String, Object> toMap(JSONObject jsonobj) throws JSONException {
		Map<String, Object> map = new HashMap<String, Object>();
		Iterator<String> keys = jsonobj.keys();
		while (keys.hasNext()) {
			String key = keys.next();
			Object value = jsonobj.get(key);
			if (value instanceof JSONArray) {
				value = toList((JSONArray) value);
			} else if (value instanceof JSONObject) {
				value = toMap((JSONObject) value);
			}
			map.put(key, value);
		}
		return map;
	}
	
	public static List<Object> toList(JSONArray array) throws JSONException {
		List<Object> list = new ArrayList<Object>();
		for (int i = 0; i < array.length(); i++) {
			Object value = array.get(i);
			if (value instanceof JSONArray) {
				value = toList((JSONArray) value);
			} else if (value instanceof JSONObject) {
				value = toMap((JSONObject) value);
			}
			list.add(value);
		}
		return list;
	}
	

	public static JsonNode toJsonNode(JsonObject jsonObj) throws IOException {
	    ObjectMapper objectMapper = new ObjectMapper();
	    return objectMapper.readTree(jsonObj.toString());
	}
	
	public static String toJsonStringFromJsonNode(JsonNode jsonNode) {
	    try {
	        ObjectMapper mapper = new ObjectMapper();
	        Object json = mapper.readValue(jsonNode.toString(), Object.class);
	        return mapper.writeValueAsString(json);
	    } catch (Exception e) {
	        return "Sorry, pretty print didn't work";
	    }
	}
	
	public static JSONObject toJsonObject(String jsonString) throws IOException, JSONException {
	    JSONObject object=new JSONObject(jsonString);
	    return object;
	}
	
	public static Map<String, Object> toMapFromJsonNode(JsonNode jsonNode) throws JSONException {
		Map<String, Object> map = new HashMap<String, Object>();
		Iterator<String> keys = jsonNode.fieldNames();
		while (keys.hasNext()) {
			String key = keys.next();
			Object value = jsonNode.get(key);
			if (value instanceof JSONArray) {
				value = toListFromJsonNode((JSONArray) value);
			} else if (value instanceof JsonNode) {
				value = toMapFromJsonNode((JsonNode) value);
			}
			map.put(key, value);
		}
		return map;
	}
	
	public static List<Object> toListFromJsonNode(JSONArray array) throws JSONException {
		List<Object> list = new ArrayList<Object>();
		for (int i = 0; i < array.length(); i++) {
			Object value = array.get(i);
			if (value instanceof JSONArray) {
				value = toListFromJsonNode((JSONArray) value);
			} else if (value instanceof JsonNode) {
				value = toMapFromJsonNode((JsonNode) value);
			}
			list.add(value);
		}
		return list;
	}
	
	public static String toJsonFromList(List<?> data) {
		GsonBuilder gsonBuilder = new GsonBuilder();
		Gson gson = gsonBuilder.create();
		return gson.toJson(data);
	}
	
	public static String jsonToPrettyJson(String jsonString) {
		ObjectMapper mapper = new ObjectMapper();
		Object json;
		String prettyJson = null;
		try {
			json = mapper.readValue(jsonString, Object.class);
			ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
			prettyJson = ow.writeValueAsString(json);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return prettyJson;
	}
	

}
