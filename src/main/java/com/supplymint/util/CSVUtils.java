
package com.supplymint.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Method;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.apache.commons.lang.StringUtils;

/**
 * @Authhor Manoj Singh
 * @Date 27-Sep-2018
 * @Version 1.0
 */
public class CSVUtils {

	private static final char DEFAULT_SEPARATOR = ',';
	private static final char DEFAULT_QUOTE = '"';
	
	 //Delimiter used in CSV file
	private static final String DEFAULT_DELIMITER = "|";
	private static final String NEW_LINE_SEPARATOR = "\n";

	public static void main(String[] args) throws Exception {

		String csvFile = GenerateFilePath.filePath()+"";

		Scanner scanner = new Scanner(new File(csvFile));
		while (scanner.hasNext()) {
			List<String> line = parseLine(scanner.nextLine());
		}
		scanner.close();

	}

	public static List<String> parseLine(String cvsLine) {
		return parseLine(cvsLine, DEFAULT_SEPARATOR, DEFAULT_QUOTE);
	}

	public static List<String> parseLine(String cvsLine, char separators) {
		return parseLine(cvsLine, separators, DEFAULT_QUOTE);
	}

	public static List<String> parseLine(String cvsLine, char separators, char customQuote) {

		List<String> result = new ArrayList<>();

		// if empty, return!
		if (cvsLine == null && cvsLine.isEmpty()) {
			return result;
		}

		if (customQuote == ' ') {
			customQuote = DEFAULT_QUOTE;
		}

		if (separators == ' ') {
			separators = DEFAULT_SEPARATOR;
		}

		StringBuffer curVal = new StringBuffer();
		boolean inQuotes = false;
		boolean startCollectChar = false;
		boolean doubleQuotesInColumn = false;

		char[] chars = cvsLine.toCharArray();

		for (char ch : chars) {

			if (inQuotes) {
				startCollectChar = true;
				if (ch == customQuote) {
					inQuotes = false;
					doubleQuotesInColumn = false;
				} else {

					// Fixed : allow "" in custom quote enclosed
					if (ch == '\"') {
						if (!doubleQuotesInColumn) {
							curVal.append(ch);
							doubleQuotesInColumn = true;
						}
					} else {
						curVal.append(ch);
					}

				}
			} else {
				if (ch == customQuote) {

					inQuotes = true;

					// Fixed : allow "" in empty quote enclosed
					if (chars[0] != '"' && customQuote == '\"') {
						curVal.append('"');
					}

					// double quotes in column will hit this!
					if (startCollectChar) {
						curVal.append('"');
					}

				} else if (ch == separators) {

					result.add(curVal.toString());

					curVal = new StringBuffer();
					startCollectChar = false;

				} else if (ch == '\r') {
					// ignore LF characters
					continue;
				} else if (ch == '\n') {
					// the end, break!
					break;
				} else {
					curVal.append(ch);
				}
			}

		}

		result.add(curVal.toString());

		return result;
	}
	
	public static <T> void writeCsvFile(List<String> fieldNames, String fileName, List<T> data, List<String> headers) {

		FileWriter fileWriter = null;
		String headerWithDelimeter = "";

		try {
			fileWriter = new FileWriter(fileName);
			for (String header : headers) {
				headerWithDelimeter += header + DEFAULT_DELIMITER;
			}

			// Write the CSV file header
			fileWriter.append(headerWithDelimeter.substring(0, headerWithDelimeter.length() - 1));

			// Add a new line separator after the header
			fileWriter.append(NEW_LINE_SEPARATOR);

			// Write a new student object list to the CSV file

			if (!CodeUtils.isEmpty(data)) {
				Class<? extends Object> classz = data.get(0).getClass();

				for (T t : data) {
					Method method = null;
					for (String fieldName : fieldNames) {
						try {
							method = classz.getMethod("get" + StringUtils.capitalize(fieldName));
						} catch (NoSuchMethodException nme) {
							method = classz.getMethod("get" + fieldName);
						}

						Object value = method.invoke(t, (Object[]) null);

						if (value != null) {
							if (value instanceof String) {
								fileWriter.append((String) value);
							} else if (value instanceof Long) {
								fileWriter.append(String.valueOf(value));
							} else if (value instanceof Integer) {
								fileWriter.append(String.valueOf(value));
							} else if (value instanceof Double) {
								fileWriter.append(String.valueOf(value));
							} else if (value instanceof Date) {
								fileWriter.append((String) CodeUtils.__dateFormat.format(value));
							} else if (value instanceof OffsetDateTime) {
								fileWriter.append(
										((OffsetDateTime) value).format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
							}
						}
						fileWriter.append(DEFAULT_DELIMITER);
					}
					fileWriter.append(NEW_LINE_SEPARATOR);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				fileWriter.flush();
				fileWriter.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
