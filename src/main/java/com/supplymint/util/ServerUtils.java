package com.supplymint.util;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.springframework.http.MediaType;

public interface ServerUtils {

	public static String createURL(HttpServletRequest request, String resourcePath) {

		int port = request.getServerPort();
		StringBuilder result = new StringBuilder();
		result.append(request.getScheme())
		        .append("://")
		        .append(request.getServerName());

		if ( (request.getScheme().equals("http") && port != 80) || (request.getScheme().equals("https") && port != 443) ) {
			result.append(':')
				.append(port);
		}

		result.append(request.getContextPath());

		if(resourcePath != null && resourcePath.length() > 0) {
			if( ! resourcePath.startsWith("/")) {
				result.append("/");
			}
			result.append(resourcePath);
		}

		return result.toString();

	}
	
	 public static MediaType getMediaTypeForFileName(ServletContext servletContext, String fileName) {
	        // application/pdf
	        // application/xml
	        // image/gif, ...
	        String mineType = servletContext.getMimeType(fileName);
	        try {
	            MediaType mediaType = MediaType.parseMediaType(mineType);
	            return mediaType;
	        } catch (Exception e) {
	            return MediaType.APPLICATION_OCTET_STREAM;
	        }
	    }
	
}
