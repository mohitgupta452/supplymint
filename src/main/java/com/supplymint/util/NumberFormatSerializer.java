package com.supplymint.util;

import java.io.IOException;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.ibm.icu.text.DecimalFormat;
import com.supplymint.layer.business.contract.setting.GeneralSettingService;

@Service
public class NumberFormatSerializer extends JsonSerializer<String> {
	public static GeneralSettingService generalSettingService;

	@SuppressWarnings("static-access")
	@Autowired
	public NumberFormatSerializer(GeneralSettingService generalSettingService) {
		this.generalSettingService = generalSettingService;
	}

	public NumberFormatSerializer() {
	}

	@Override
	public void serialize(String value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
		Map<String, String> data = generalSettingService.getAll();
		String s = data.get("numberFormat");
		String s1 = data.get("numberFormatDecimalValue");
		String s2 = data.get("numberFormatUseSeperator");
		if (s.equalsIgnoreCase("CUSTOM")) {
			Integer d = Integer.parseInt(s1);
			Double a = Math.pow(10, d);
			if (s2.equalsIgnoreCase("false")) {
				if (CodeUtils.checkStringType(value).equals("Float")) {
					Float floatValue = Float.parseFloat(value);
					double decimal = (Math.round(floatValue * a) / a);
					String decimalToString = decimal + "";
					gen.writeString(decimalToString);
				} else if (CodeUtils.checkStringType(value).equals("Double")) {
					Double doubleValue = Double.parseDouble(value);
					double decimal = Math.round((doubleValue * a) / a);
					String decimalToString = decimal + "";
					gen.writeString(decimalToString);
				} else if (CodeUtils.checkStringType(value).equals("Integer")) {
					Integer integerValue = Integer.parseInt(value);
					String decimalToString = integerValue + "";
					gen.writeString(decimalToString);
				}
			}
			if (s2.equalsIgnoreCase("true")) {
				if (CodeUtils.checkStringType(value).equals("Float")) {
					Float floatValue = Float.parseFloat(value);
					double decimal = (Math.round(floatValue * a) / a);
					String output = CodeUtils.commaSeperator(decimal, d);
					gen.writeString(output);
				} else if (CodeUtils.checkStringType(value).equals("Double")) {
					Double doubleValue = Double.parseDouble(value);
					double decimal = Math.round((doubleValue * a) / a);
					DecimalFormat myFormatter = new DecimalFormat("#,###");
					String output = myFormatter.format(decimal);
					gen.writeString(output);
				} else if (CodeUtils.checkStringType(value).equals("Integer")) {
					Integer integerValue = Integer.parseInt(value);
					DecimalFormat myFormatter = new DecimalFormat("#,###");
					String output = myFormatter.format(integerValue);
					gen.writeString(output);
				}
			}

		} else {
			gen.writeString(value);
		}
	}
}
