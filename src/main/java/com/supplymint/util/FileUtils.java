package com.supplymint.util;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.Part;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.lang3.SystemUtils;

/**
 * @author Manoj Singh
 * @since 20 JAN 2019
 * @version 1.0
 */
public final class FileUtils {

	public static final String FileType = "fileType";
	public static final String FileUpload = "UPLOAD";
	public static final String FileDownload = "OUTPUT";
	public static final String GenerateTransferOrder = "GENERATE_TRANSFER_ORDER";
	public static final String BucketName = "BUCKET";
	public static final String CSVExtension = ".CSV";
	public static final String EXCELExtension = ".XLSX";
	public static final String PDFExtension = ".pdf";
	public static final String ZIPExtension = ".ZIP";
	public static final String ImageExtension = ".jpeg";
	public static final String TransferOrder_Success = "Transfer Order generated successfully";
	public static final String TransferOrder_Inprogress = "Transfer Order Inprogress ...";
	public static final String Allocation_Not_Found = "No Transfer Order Generated";
	public static final String JRXMLExtension = ".jrxml";
	public static final String FileDropdownValue = "type";
	public static final String FileFullPath = "filePath";

	public static String saveFile(File parentDir, Part part) {

		File originalFile = null;
		String savedPath = null;

		try (InputStream input = part.getInputStream()) {

			originalFile = new File(parentDir, extractFileNameWithExtension(part));

			if (!(originalFile.exists())) {
				(new File(originalFile.getParent())).mkdirs();
			}

			if (originalFile.exists()) {
				originalFile.delete();
			}

			Files.copy(input, originalFile.toPath());
			savedPath = originalFile.getAbsolutePath();

		} catch (IOException e) {
		} finally {

		}

		return savedPath;
	}

	public static String createFilePath(File parentDir, String savedPath) {

		File originalFile = null;
		try {

			originalFile = new File(parentDir, savedPath);

			if (!(originalFile.exists())) {
				(new File(originalFile.getParent())).mkdirs();
			}

			if (originalFile.exists()) {
				originalFile.delete();
			}

			savedPath = originalFile.getAbsolutePath();

		} catch (Exception e) {
		}

		return savedPath;
	}

	public static File getPlatformBasedParentDir() {
		File parentDir = null;

		if (SystemUtils.IS_OS_WINDOWS) {
			parentDir = new File("c:/SUPPLYMINT");

		} else if (SystemUtils.IS_OS_LINUX) {
			parentDir = new File(SystemUtils.USER_HOME + File.separator + "SUPPLYMINT");

		} else if (SystemUtils.IS_OS_MAC) {
			parentDir = new File(SystemUtils.USER_HOME + File.separator + "SUPPLYMINT");
		}

		return parentDir;
	}

	public static String extractFileNameWithOutExtension(Part part) {
		String contentDisp = part.getHeader("content-disposition");
		String[] items = contentDisp.split(";");
		for (String s : items) {
			if (s.trim().startsWith("filename")) {
				return s.substring(s.indexOf("=") + 2, s.length() - 6);
			}
		}
		return "";
	}

	public static String extractFileNameWithExtension(Part part) {
		String contentDisp = part.getHeader("content-disposition");
		String[] items = contentDisp.split(";");
		for (String s : items) {
			if (s.trim().startsWith("filename")) {
				return s.substring(s.indexOf("=") + 2, s.length() - 1);
			}
		}
		return "";
	}

	public static Map<String, String> getStorageLocation(String fileArray, String filePath) throws IOException {
		Map<String, String> returnNameAndData = new HashMap<>();

		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy_HH:mm:ss");
		Date date = new Date();
		String ex = filePath.split("\\.")[1];
		String[] fileName = filePath.split("\\.")[0].split("\\/");
		String key = fileName[fileName.length - 1];
		String outPutFilePath = filePath + key.replace(" ", "") + "_" + sdf.format(date);
		byte[] data = null;
		File file = null;
		String base64String = fileArray;
		String[] strings = base64String.split(",");
		String extension;
		switch (strings[0]) {
		// check image's extension
		case "data:image/jpeg;base64":
			extension = "jpeg";
			break;
		case "data:image/png;base64":
			extension = "png";
			break;
		case "data:image/jpg;base64":
			extension = "jpg";
			break;
		default:
			// should write cases for more types
			extension = "csv";
			break;
		}
		// convert base64 string to binary data
		data = DatatypeConverter.parseBase64Binary(strings[1]);
		file = new File(outPutFilePath);
		try (OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(file))) {
			outputStream.write(data);
		} catch (IOException e) {
			e.printStackTrace();
		}
		returnNameAndData.put("FILE_PATH", outPutFilePath);
		return returnNameAndData;
	}
	
	public static String readTextFile(String fileName) throws IOException {
        String content = new String(Files.readAllBytes(Paths.get(fileName)));
        return content;
    }

    public static List<String> readTextFileByLines(String fileName) throws IOException {
        List<String> lines = Files.readAllLines(Paths.get(fileName));
        return lines;
    }

    public static void writeToTextFile(String fileName, String content) throws IOException {
        Files.write(Paths.get(fileName), content.getBytes(), StandardOpenOption.CREATE);
    }

}
