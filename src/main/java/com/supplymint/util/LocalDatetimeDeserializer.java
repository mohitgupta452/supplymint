package com.supplymint.util;

import java.io.IOException;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

public class LocalDatetimeDeserializer extends JsonDeserializer<OffsetDateTime> {

	private static final Logger LOGGER = LoggerFactory.getLogger(LocalDatetimeDeserializer.class);

	@Override
	public OffsetDateTime deserialize(JsonParser p, DeserializationContext ctx) throws IOException {
		String str = p.getText();
		try {
			// ISO_LOCAL_DATE
			DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME;

//			DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ISO_LOCAL_DATE;
			// String date = "2017-12-03T10:15:30+01:00";
			OffsetDateTime date1 = OffsetDateTime.parse(str, dateTimeFormatter);
			date1 = date1.plusHours(5);
			date1 = date1.plusMinutes(30);

			LOGGER.debug("deserialize date :%s" , date1.toString());
			return date1;

			// return LocalDateTime.parse(str, LocalDateTimeSerializer.DATE_FORMATTER);
		} catch (DateTimeParseException e) {
			// System.err.println(e);
			return null;
		} catch (Exception ex) {
			return null;
		}
	}
}
