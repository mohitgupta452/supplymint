package com.supplymint.util;

/**
 * @author Prabhakar
 * @since 25 APR 2019
 */

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DateUtils {

	// List of all date formats that we want to parse.
	// Add your own format here.
	@SuppressWarnings("serial")
	private static List<SimpleDateFormat> 
			dateFormats = new ArrayList<SimpleDateFormat>() {{
			add(new SimpleDateFormat("M/dd/yyyy"));
			add(new SimpleDateFormat("dd/mm/yyyy"));
			add(new SimpleDateFormat("dd.M.yyyy"));
			add(new SimpleDateFormat("M/dd/yyyy hh:mm:ss a"));
			add(new SimpleDateFormat("dd.M.yyyy hh:mm:ss a"));
			add(new SimpleDateFormat("dd.MMM.yyyy"));
			add(new SimpleDateFormat("dd-MMM-yyyy"));
			add(new SimpleDateFormat("dd MMM yyyy"));
			add(new SimpleDateFormat("yyyy-MM-dd"));
			add(new SimpleDateFormat("yyyy-MM-dd HH:mm"));
			add(new SimpleDateFormat("dd MMMM yyyy"));
			add(new SimpleDateFormat("dd-MM-yyyy"));
			add(new SimpleDateFormat("yyyy/MM/dd"));
			add(new SimpleDateFormat("yyyy/MM/dd HH:mm"));
			add(new SimpleDateFormat("dd MMMM yyyy HH:mm"));
			add(new SimpleDateFormat("dd MMM yyyy HH:mm"));
			add(new SimpleDateFormat("dd-MM-yyyy HH:mm"));
			add(new SimpleDateFormat("dd MMM yyyy 'AT' HH:mm"));
			add(new SimpleDateFormat("E MMM dd HH:mm"));
			
		}
	};

	/**
	 * Convert String with various formats into java.util.Date
	 * 
	 * @param input
	 *            Date as a string
	 * @return java.util.Date object if input string is parsed 
	 * 			successfully else returns null
	 */
	public static Date stringToDate(String input) {
		Date date = null;
		if(null == input) {
			return null;
		}
		for (SimpleDateFormat format : dateFormats) {
			try {
				format.setLenient(false);
				date = format.parse(input);
			} catch (ParseException e) {
				//Shhh.. try other formats
			}
			if (date != null) {
				break;
			}
		}

		return date;
	}
}