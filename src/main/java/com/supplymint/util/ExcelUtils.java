package com.supplymint.util;


/**
 * @author Manoj Singh 
 * @author Prabhakar Srivastava
 * @Version 2.0
 */
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import com.amazonaws.services.s3.model.S3ObjectInputStream;

public class ExcelUtils {

	private final static Logger LOGGER = LoggerFactory.getLogger(ExcelUtils.class);

	/**
	 * This function will generate excel with multiple sheets according to the size
	 * of the physical sheet.
	 * 
	 * @param fileName
	 * @param data
	 * @author Manoj Singh
	 */
	public static <T> void writeToExcel(String fileName, List<T> data) {
		OutputStream fos = null;
		SXSSFWorkbook workbook = null;
		try {
			File file = new File(fileName);
			workbook = new SXSSFWorkbook();

//			SXSSFSheet sheet = workbook.createSheet();
//			List<String> fieldNames = getFieldNamesForClass(data.get(0).getClass());
//			int rowCount = 0;
//			int columnCount = 0;
//			Row row = sheet.createRow(rowCount++);

			int fileSize = 1;
			int rowCount = 0;
			int columnCount = 0;
			int sheetNumber = 0;
			List<SXSSFSheet> sheets = null;
			String sheetName = "Sheet-";
			SXSSFSheet sheet = null;
			Row row = null;
			List<String> fieldNames = null;
			int finalSize = 1048574;
			int sheetSize = data.size() / finalSize;
			fileSize = (int) (data.size() % finalSize == 0 ? sheetSize : sheetSize + 1);
			sheets = new ArrayList<>();
			for (int i = 0; i < fileSize; i++) {
				sheets.add(workbook.createSheet(sheetName.concat(Integer.toString(i))));
				sheet = sheets.get(i);
				LOGGER.debug("Starting with Sheet name - %s ", sheet.getSheetName());
				fieldNames = getFieldNamesForClass(data.get(0).getClass());
				rowCount = 0;
				row = sheet.createRow(rowCount++);
				columnCount = 0;
				for (String fieldName : fieldNames) {
					if (!CodeUtils.isEmpty(fieldName)) {
						Cell cell = row.createCell(columnCount++);
						cell.setCellValue(fieldName);
					}
				}
			}
			if (!CodeUtils.isEmpty(data)) {
				Class<? extends Object> classz = data.get(0).getClass();
				sheet = sheets.get(sheetNumber);
				for (T t : data) {
					if (sheet.getPhysicalNumberOfRows() > finalSize) {
						sheetNumber++;
						sheet = sheets.get(sheetNumber);
						rowCount = 1;
					}
					row = sheet.createRow(rowCount++);
					columnCount = 0;
					for (String fieldName : fieldNames) {
						Cell cell = row.createCell(columnCount);
						Method method = null;
						try {
							method = classz.getMethod("get" + capitalize(fieldName));
						} catch (NoSuchMethodException nme) {
							method = classz.getMethod("get" + fieldName);
						}
						Object value = method.invoke(t, (Object[]) null);
						if (value != null) {
							if (value instanceof String) {
								cell.setCellValue((String) value);
							} else if (value instanceof Long) {
								cell.setCellValue((Long) value);
							} else if (value instanceof Integer) {
								cell.setCellValue((Integer) value);
							} else if (value instanceof Double) {
								cell.setCellValue((Double) value);
							}
						}
						columnCount++;
					}
				}
			}

			try {
				Path path = Paths.get(fileName);
				try (OutputStream out = Files.newOutputStream(path)) {
					workbook.write(out);
					out.close();
					out.flush();
				} catch (FileAlreadyExistsException incrCounterAndRetry) {
					LOGGER.debug(
							String.format("Error Occoured From FileAlreadyExistsException : %s", incrCounterAndRetry));
				}
			} catch (Exception ex) {
				LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			}
		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
		}

//			fos = new FileOutputStream(file);
//			workbook.write(fos);
//			fos.flush();
//
//		} catch (Exception e) {
//			e.printStackTrace();
//		} finally {
//			try {
//				if (fos != null) {
//					fos.close();
//				}
//			} catch (IOException e) {
//			}
//			try {
//				if (workbook != null) {
//					workbook.close();
//				}
//			} catch (IOException e) {
//			}
//		}
	}

	// using auto flush and default window size 100
	public static <T> void writeToExcelAutoFlush(String fileName, List<T> excelModels) {
		SXSSFWorkbook wb = null;
		FileOutputStream fos = null;
		try {
			File file = new File(fileName);
			// keep 100 rows in memory, exceeding rows will be flushed to disk
			wb = new SXSSFWorkbook(SXSSFWorkbook.DEFAULT_WINDOW_SIZE/* 100 */);
			Sheet sh = wb.createSheet();

			Class<? extends Object> classz = excelModels.get(0).getClass();

			Field[] fields = classz.getDeclaredFields();
			int noOfFields = fields.length;

			int rownum = 0;
			Row row = sh.createRow(rownum);
			for (int i = 0; i < noOfFields; i++) {
				Cell cell = row.createCell(i);
				cell.setCellValue(fields[i].getName());
			}

			for (T excelModel : excelModels) {
				row = sh.createRow(rownum + 1);
				int colnum = 0;
				for (Field field : fields) {
					String fieldName = field.getName();
					Cell cell = row.createCell(colnum);
					Method method = null;
					try {
						method = classz.getMethod("get" + capitalize(fieldName));
					} catch (NoSuchMethodException nme) {
						method = classz.getMethod("get" + fieldName);
					}
					Object value = method.invoke(excelModel, (Object[]) null);
//						cell.setCellValue((String) value);
					if (value != null) {
						if (value instanceof String) {
							cell.setCellValue((String) value);
						} else if (value instanceof Long) {
							cell.setCellValue((Long) value);
						} else if (value instanceof Integer) {
							cell.setCellValue((Integer) value);
						} else if (value instanceof Double) {
							cell.setCellValue((Double) value);
						}
					}
					colnum++;
				}
				rownum++;
			}

			try {
				Path path = Paths.get(fileName);
				try (OutputStream out = Files.newOutputStream(path)) {
					wb.write(out);
					out.close();
					out.flush();
				} catch (FileAlreadyExistsException incrCounterAndRetry) {
					LOGGER.debug(
							String.format("Error Occoured From FileAlreadyExistsException : %s", incrCounterAndRetry));
				}
			} catch (Exception ex) {
				LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			}
		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
		}
//			fos = new FileOutputStream(file);
//			wb.write(fos);
//		} catch (Exception ex) {
//
//		} finally {
//			try {
//				if (fos != null) {
//					fos.close();
//				}
//			} catch (IOException e) {
//			}
//			try {
//				if (wb != null) {
//					wb.close();
//				}
//			} catch (IOException e) {
//			}
//		}
	}

	// using manual flush and default window size 100
	public static <T> void writeToExcelManualFlush(String fileName, List<T> excelModels) {
		SXSSFWorkbook wb = null;
		FileOutputStream fos = null;
		try {
			File file = new File(fileName);
			// turn off auto-flushing and accumulate all rows in memory
			wb = new SXSSFWorkbook(-1);
			Sheet sh = wb.createSheet();

			Class<? extends Object> classz = excelModels.get(0).getClass();

			Field[] fields = classz.getDeclaredFields();
			int noOfFields = fields.length;

			int rownum = 0;
			Row row = sh.createRow(rownum);
			for (int i = 0; i < noOfFields; i++) {
				Cell cell = row.createCell(i);
				cell.setCellValue(fields[i].getName());
			}

			for (T excelModel : excelModels) {
				row = sh.createRow(rownum + 1);
				int colnum = 0;
				for (Field field : fields) {
					String fieldName = field.getName();
					Cell cell = row.createCell(colnum);
					Method method = null;
					try {
						method = classz.getMethod("get" + capitalize(fieldName));
					} catch (NoSuchMethodException nme) {
						method = classz.getMethod("get" + fieldName);
					}
					Object value = method.invoke(excelModel, (Object[]) null);
//						cell.setCellValue((String) value);
					if (value != null) {
						if (value instanceof String) {
							cell.setCellValue((String) value);
						} else if (value instanceof Long) {
							cell.setCellValue((Long) value);
						} else if (value instanceof Integer) {
							cell.setCellValue((Integer) value);
						} else if (value instanceof Double) {
							cell.setCellValue((Double) value);
						}
					}
					colnum++;
				}
				// manually control how rows are flushed to disk
				if (rownum % 100 == 0) {
					// retain 100 last rows and flush all others
					((SXSSFSheet) sh).flushRows(100);
				}
				rownum++;
			}
			try {
				Path path = Paths.get(fileName);
				try (OutputStream out = Files.newOutputStream(path)) {
					wb.write(out);
					out.close();
					out.flush();
				} catch (FileAlreadyExistsException incrCounterAndRetry) {
					LOGGER.debug(
							String.format("Error Occoured From FileAlreadyExistsException : %s", incrCounterAndRetry));
				}
			} catch (Exception ex) {
				LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			}
		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
		}
	}

	// write to excel with customized column

	public static <T> void customizedWriteToExcel(List<String> fieldNames, String fileName, List<T> data) {
		SXSSFWorkbook workbook = null;
		try {
			int fileSize = 1;
			int rowCount = 0;
			int columnCount = 0;
			int sheetNumber = 0;
			List<SXSSFSheet> sheets = null;
			workbook = new SXSSFWorkbook();
			String sheetName = "Sheet-";
			SXSSFSheet sheet = null;
			Row row = null;

			int finalSize = 1048574;
			int sheetSize = data.size() / finalSize;
			fileSize = (int) (data.size() % finalSize == 0 ? sheetSize : sheetSize + 1);
			sheets = new ArrayList<>();
			for (int i = 0; i < fileSize; i++) {
				sheets.add(workbook.createSheet(sheetName.concat(Integer.toString(i))));
				sheet = sheets.get(i);

				LOGGER.debug(String.format("Starting with Sheet name - %s", sheet.getSheetName()));
				// fieldNames = getFieldNamesForClass(data.get(0).getClass());

				rowCount = 0;
				row = sheet.createRow(rowCount++);
				columnCount = 0;
				for (String fieldName : fieldNames) {
					Cell cell = row.createCell(columnCount++);
					cell.setCellValue(fieldName);
				}
			}
			if (!CodeUtils.isEmpty(data)) {
				Class<? extends Object> classz = data.get(0).getClass();
				sheet = sheets.get(sheetNumber);
				for (T t : data) {
					if (sheet.getPhysicalNumberOfRows() > finalSize) {
						sheetNumber++;
						sheet = sheets.get(sheetNumber);
						rowCount = 1;
					}
					row = sheet.createRow(rowCount++);
					columnCount = 0;
					for (String fieldName : fieldNames) {
						Cell cell = row.createCell(columnCount);
						Method method = null;
						try {
							method = classz.getMethod("get" + capitalize(fieldName));
						} catch (NoSuchMethodException nme) {
							method = classz.getMethod("get" + fieldName);
						}
						Object value = method.invoke(t, (Object[]) null);
						if (value != null) {
							if (value instanceof String) {
								cell.setCellValue((String) value);
							} else if (value instanceof Long) {
								cell.setCellValue((Long) value);
							} else if (value instanceof Integer) {
								cell.setCellValue((Integer) value);
							} else if (value instanceof Double) {
								cell.setCellValue((Double) value);
							}
						}
						columnCount++;
					}
				}
			}
			try {
				Path path = Paths.get(fileName);
				try (OutputStream out = Files.newOutputStream(path)) {
					workbook.write(out);
					out.close();
					out.flush();
				} catch (FileAlreadyExistsException incrCounterAndRetry) {
					LOGGER.debug(
							String.format("Error Occoured From FileAlreadyExistsException : %s", incrCounterAndRetry));
				}
			} catch (Exception ex) {
				LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			}
		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
		}

//			fos = new FileOutputStream(file);
//			workbook.write(fos);
//			fos.flush();
//		} catch (Exception e) {
//			e.printStackTrace();
//		} finally {
//			try {
//				if (fos != null) {
//					fos.close();
//				}
//			} catch (IOException e) {
//			}
//			try {
//				if (workbook != null) {
//					workbook.close();
//				}
//			} catch (IOException e) {
//			}
//		}
	}

	// retrieve field names from a POJO class
	private static List<String> getFieldNamesForClass(Class<?> clazz) throws Exception {
		List<String> fieldNames = new ArrayList<String>();
		Field[] fields = clazz.getDeclaredFields();
		for (int i = 0; i < fields.length; i++) {
			fieldNames.add(fields[i].getName());
		}
		return fieldNames;
	}

	// capitalize the first letter of the field name for retriving value of the
	// field later
	private static String capitalize(String s) {
		if (s.length() == 0)
			return s;
		return s.substring(0, 1).toUpperCase() + s.substring(1);
	}

	@SuppressWarnings({ "rawtypes", "resource", "unchecked", "finally" })
	public static Vector readExcel(String fileName) {
		Vector cellVectorHolder = new Vector();
		int i = 0;
		try {
			Path path = Paths.get(fileName);
			InputStream myInput = Files.newInputStream(path);
//			FileInputStream myInput = new FileInputStream(fileName);
			// POIFSFileSystem myFileSystem = new POIFSFileSystem(myInput);
			XSSFWorkbook myWorkBook = new XSSFWorkbook(myInput);
			XSSFSheet mySheet = myWorkBook.getSheetAt(0);
			Iterator rowIter = mySheet.rowIterator();
			while (rowIter.hasNext()) {

				XSSFRow myRow = (XSSFRow) rowIter.next();
				Iterator cellIter = myRow.cellIterator();
				if (i > 0) {

					Vector cellStoreVector = new Vector();
					while (cellIter.hasNext()) {
						XSSFCell myCell = (XSSFCell) cellIter.next();
						cellStoreVector.addElement(myCell);
					}
					cellVectorHolder.addElement(cellStoreVector);
				}
				i++;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			return cellVectorHolder;
		}

	}

	public static List<Map<String, String>> getExcelList(String fileName, String headerData) throws IOException {

		Path path = Paths.get(fileName);
		InputStream myInput = Files.newInputStream(path);
		XSSFWorkbook wb = new XSSFWorkbook(myInput);

		List<Map<String, String>> tempList = new ArrayList<Map<String, String>>();
		Map<Integer, String> header = new HashMap<Integer, String>();
		String[] arg = headerData.split(",");

		for (int i = 0; i < arg.length; i++) {
			if (!arg[i].equals(""))
				header.put(i, arg[i]);
		}

		for (Row row : wb.getSheetAt(0)) {

			Map<String, String> tempMap = new HashMap<String, String>();
			for (Cell cell : row) {
				if (header.get(cell.getColumnIndex()) != null) {
					switch (cell.getCellType()) {
					case Cell.CELL_TYPE_STRING:
						tempMap.put(header.get(cell.getColumnIndex()), cell.getRichStringCellValue().getString());
						break;
					case Cell.CELL_TYPE_NUMERIC:
						if (DateUtil.isCellDateFormatted(cell))
							tempMap.put(header.get(cell.getColumnIndex()), cell.getDateCellValue().toString());
						else
							tempMap.put(header.get(cell.getColumnIndex()),
									Integer.toString((int) cell.getNumericCellValue()));
						break;
					case Cell.CELL_TYPE_FORMULA:
						tempMap.put(header.get(cell.getColumnIndex()), cell.getCellFormula());
						break;
					}
				}
			}

			tempList.add(tempMap);
		}

		return tempList;
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "resource", "static-access" })
	public static Vector readXLSFileWithBlankCells(String fileName) throws FileNotFoundException {
		Vector cellVectorHolder = new Vector();
		try {
			Path path = Paths.get(fileName);
			InputStream myInput = Files.newInputStream(path);
			XSSFWorkbook wb = new XSSFWorkbook(myInput);

			XSSFSheet sheet = wb.getSheetAt(0);
			XSSFRow row;
			XSSFCell cell;

			Iterator rows = sheet.rowIterator();
			Row r = sheet.getRow(0);
			int maxCell = r.getLastCellNum();
			while (rows.hasNext()) {
				row = (XSSFRow) rows.next();
				Vector cellStoreVector = new Vector();
				for (int i = 0; i < maxCell; i++) {
					cell = (XSSFCell) row.getCell(i, Row.CREATE_NULL_AS_BLANK);
					if (cell.getCellType() == cell.CELL_TYPE_NUMERIC) {
						if (DateUtil.isCellDateFormatted(cell)) {
							cellStoreVector.add(cell);
						} else {
							int num = (int) cell.getNumericCellValue();
							cellStoreVector.add(String.valueOf(num));
						}
					} else if (cell.getCellType() == cell.CELL_TYPE_STRING) {
						cellStoreVector.add(cell.getStringCellValue());
					} else {
						cellStoreVector.add(cell);
					}
				}
				cellVectorHolder.add(cellStoreVector);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return cellVectorHolder;
	}

	@SuppressWarnings("resource")
	public static List<Map<String, String>> getExcelList(InputStream inputData) {
		List<Map<String, String>> tempList = new LinkedList<Map<String, String>>();

		try {
			XSSFWorkbook wb = new XSSFWorkbook(inputData);
			Map<Integer, String> headers = new LinkedHashMap<Integer, String>();
			boolean headerMetaDeta = true;
			for (Row row : wb.getSheetAt(0)) {
				if (headerMetaDeta) {
					for (Cell cell : row) {
						headers.put(cell.getColumnIndex(), cell.getStringCellValue());
					}
					headerMetaDeta = false;
				} else {
					Map<String, String> tempMap = new HashMap<>();
					for (Cell cell : row) {
						if (headers.get(cell.getColumnIndex()) != null)
							;
						switch (cell.getCellType()) {
						case Cell.CELL_TYPE_STRING:
							tempMap.put(headers.get(cell.getColumnIndex()), cell.getStringCellValue());
							break;
						case Cell.CELL_TYPE_NUMERIC:
							if (DateUtil.isCellDateFormatted(cell))
								tempMap.put(headers.get(cell.getColumnIndex()), cell.getDateCellValue().toString());
							else
								tempMap.put(headers.get(cell.getColumnIndex()),
										Integer.toString((int) cell.getNumericCellValue()));
							break;

						case Cell.CELL_TYPE_FORMULA:
							tempMap.put(headers.get(cell.getColumnIndex()), cell.getCellFormula());
							break;

						}

					}
					tempList.add(tempMap);
				}

			}

		} catch (Exception e) {
		}

		return tempList;
	}

	public static int cellIndex(String cellValue, List<String> headers) {
		int index = 0;
		for (int i = 0; i < headers.size(); i++) {
			if (headers.get(i).equals(cellValue)) {
				index = i + 1;
				break;
			}
		}
		return index;
	}

	public static <T> void customizedHeaderWriteToExcel(List<String> fieldNames, String fileName, List<T> data,
			List<String> headers) {
		SXSSFWorkbook workbook = null;
		try {
			int fileSize = 1;
			int rowCount = 0;
			int columnCount = 0;
			int sheetNumber = 0;
			List<SXSSFSheet> sheets = null;
			File file = new File(fileName);
			workbook = new SXSSFWorkbook();
			String sheetName = "Sheet-";
			SXSSFSheet sheet = null;
			Row row = null;

			int finalSize = 1048574;
			int sheetSize = data.size() / finalSize;
			fileSize = (int) (data.size() % finalSize == 0 ? sheetSize : sheetSize + 1);
			sheets = new ArrayList<>();
			for (int i = 0; i < fileSize; i++) {
				sheets.add(workbook.createSheet(sheetName.concat(Integer.toString(i))));
				sheet = sheets.get(i);
				LOGGER.debug(String.format("Starting with Sheet name - %s", sheet.getSheetName()));
				rowCount = 0;
				row = sheet.createRow(rowCount++);
				columnCount = 0;
				for (String fieldName : headers) {
					Cell cell = row.createCell(columnCount++);
					cell.setCellValue(fieldName);
				}
			}
			if (!CodeUtils.isEmpty(data)) {
				Class<? extends Object> classz = data.get(0).getClass();
				sheet = sheets.get(sheetNumber);
				for (T t : data) {
					if (sheet.getPhysicalNumberOfRows() > finalSize) {
						sheetNumber++;
						sheet = sheets.get(sheetNumber);
						rowCount = 1;
					}
					row = sheet.createRow(rowCount++);
					columnCount = 0;
					for (String fieldName : fieldNames) {
						Cell cell = row.createCell(columnCount);
						Method method = null;
						try {
							method = classz.getMethod("get" + StringUtils.capitalize(fieldName));
						} catch (NoSuchMethodException nme) {
							method = classz.getMethod("get" + fieldName);
						}
						Object value = method.invoke(t, (Object[]) null);
						if (value != null) {
							if (value instanceof String) {
								cell.setCellValue((String) value);
							} else if (value instanceof Long) {
								cell.setCellValue((Long) value);
							} else if (value instanceof Integer) {
								cell.setCellValue((Integer) value);
							} else if (value instanceof Double) {
								cell.setCellValue((Double) value);
							} else if (value instanceof Date) {
								cell.setCellValue((String) CodeUtils.__dateFormat.format(value));
							} else if (value instanceof OffsetDateTime) {
								cell.setCellValue(
										((OffsetDateTime) value).format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
							}

						}
						columnCount++;
					}
				}
			}
			try {
				Path path = Paths.get(fileName);
				try (OutputStream out = Files.newOutputStream(path)) {
					workbook.write(out);
					out.close();
					out.flush();
				} catch (FileAlreadyExistsException incrCounterAndRetry) {
					LOGGER.debug(
							String.format("Error Occoured From FileAlreadyExistsException : %s", incrCounterAndRetry));
				}
			} catch (Exception ex) {
				LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
			}
		} catch (Exception ex) {
			LOGGER.debug(String.format("Error Occoured From Exception : %s", ex));
		}

	}

}
