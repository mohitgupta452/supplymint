package com.supplymint.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.poi.hssf.util.CellReference;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.DataValidation;
import org.apache.poi.ss.usermodel.DataValidationConstraint;
import org.apache.poi.ss.usermodel.DataValidationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Name;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFDataValidationHelper;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Manoj Singh
 * @since 19 JAN 2018
 * @version 1.0
 */

public abstract class Excel {

	private static Logger log = LoggerFactory.getLogger(Excel.class);

	private String workBookName = null;
	protected XSSFWorkbook wb = null;
	protected XSSFSheet sheet = null;
	protected XSSFSheet hiddenSheet = null;

	protected File file = null;

	protected Map<Integer, String> companyMap;
	protected Map<Integer, String> locationMap;
	protected Map<Integer, String> functionMap;

	protected Map<Integer, Map<String, List<Integer>>> apExcelRecord;

	// will create a new xlsx file in specified location on executing
	// generateExcelFile() method
	@SuppressWarnings("deprecation")
	public void Excel(String path, String workBookName, Boolean protectedExcel) throws Exception {
		if (path == null || path.equals("")) {
			throw new Exception("please provide parentDirectory");
		}
		if (workBookName == null || workBookName.equals("")) {
			this.workBookName = "default[" + new Date().getDate() + "].xlsx";
		} else
			this.workBookName = workBookName;

		this.file = new File(path + File.separator + this.workBookName);
		if (!(this.file.exists())) {
			log.debug("folders not exists..");
			(new File(file.getParent())).mkdirs();
			log.debug("folder created...");
		}
		this.wb = new XSSFWorkbook();
		this.sheet = this.wb.createSheet();
		if (protectedExcel)
			this.sheet.protectSheet("createCellStyle();");
		log.debug("Excel Created....");
	}

	public void createHiddenSheet() {
		this.hiddenSheet = this.wb.createSheet("hidden");
	}

	// will read the specified Excel file
	public void Excel(File file) throws Exception {
		wb = new XSSFWorkbook(file);
		sheet = wb.getSheetAt(0);

		this.file = file;
		log.debug("Excel Created....");
	}

	protected void closeExcelFile() {
		try {
			this.wb.close();
			log.debug("file closed");
		} catch (IOException ioe) {
			log.error(this.getClass().getName() + ioe.getMessage());
		}
	}

	protected void setLocked(String colomn, int startCell, int endCell, boolean status) {
		CellStyle style = this.wb.createCellStyle();
		style.setLocked(false);
		style.setWrapText(true);
		XSSFCell cell;

		for (int i = startCell; i <= endCell; i++) {
			cell = this.getCell(colomn + i);
			cell.setCellStyle(style);
		}
	}

	protected void setDatePatternWithLock(String datePattern, String colomn, int startCell, int endCell,
			boolean status) {
		CellStyle style = this.wb.createCellStyle();
		style.setLocked(false);
		style.setWrapText(true);
		CreationHelper createHelper = wb.getCreationHelper();
		style.setDataFormat(createHelper.createDataFormat().getFormat(datePattern));
		XSSFCell cell;

		for (int i = startCell; i <= endCell; i++) {
			cell = this.getCell(colomn + i);
			cell.setCellStyle(style);
		}
	}

	protected XSSFCell getCell(String cellName) {
		CellReference ref = new CellReference(cellName);
		XSSFRow row = sheet.getRow(ref.getRow());
		if (row == null) {
			row = sheet.createRow(ref.getRow());
		}

		XSSFCell cell = row.getCell(ref.getCol());
		if (cell == null) {
			cell = row.createCell(ref.getCol());
		}

		return cell;
	}

	public void mergeCell(String startCell, String endCell) {
		CellRangeAddress region = CellRangeAddress.valueOf(startCell + ":" + endCell);
		sheet.addMergedRegion(region);
	}

	public void mergeCell(String startCell, String endCell, String value) {
		CellRangeAddress region = CellRangeAddress.valueOf(startCell + ":" + endCell);
		sheet.addMergedRegion(region);
		this.setCellVal(startCell, value);
	}

	public String excelReadSingleRawValue(String cellName) throws IOException {
		// return this.getCell(cellName).getRawValue();
		return new DecimalFormat("#.##").format(new Double(this.getCell(cellName).getRawValue()));

	}

	private void setCellVal(String cellName, String value) {

		XSSFCell cell = this.getCell(cellName);
		CellStyle cs = wb.createCellStyle();

		cs.setWrapText(true);
		if (value != null && !(value.equals(""))) {

			if (value.contains(","))
				cs.setAlignment(cs.ALIGN_LEFT);
			else
				cs.setAlignment(cs.ALIGN_CENTER);
			cs.setVerticalAlignment(cs.VERTICAL_CENTER);
			String crLf = Character.toString((char) 13) + Character.toString((char) 10);
			cell.setCellStyle(cs);
			cell.setCellValue(value.replaceAll(", ", "," + crLf));
		} else {
			cell.setCellStyle(cs);
			cell.setCellValue("");
		}

	}

	public void setCellValue(String cellName, String value) {

		XSSFCell cell = this.getCell(cellName);
		CellStyle cs = wb.createCellStyle();
		cs.setWrapText(true);
		cs.setBorderLeft((short) 1);
		cs.setBorderRight((short) 1);
		cs.setBorderBottom((short) 1);
		cs.setBorderTop((short) 1);
		cs.setWrapText(true);
		if (value != null && !(value.equals(""))) {

			if (value.contains(","))
				cs.setAlignment(cs.ALIGN_LEFT);
			else
				cs.setAlignment(cs.ALIGN_CENTER);
			cs.setVerticalAlignment(cs.VERTICAL_CENTER);
			String crLf = Character.toString((char) 13) + Character.toString((char) 10);
			cell.setCellStyle(cs);
			cell.setCellValue(value.replaceAll(", ", "," + crLf));
		} else {
			cell.setCellStyle(cs);
			cell.setCellValue("");
		}

	}

	public void setCellValueDouble(String cellName, double value) {

		XSSFCell cell = this.getCell(cellName);
		CellStyle cs = wb.createCellStyle();
		cs.setWrapText(true);
		cs.setBorderLeft((short) 1);
		cs.setBorderRight((short) 1);
		cs.setBorderBottom((short) 1);
		cs.setBorderTop((short) 1);
		cs.setWrapText(true);
		cs.setAlignment(cs.ALIGN_CENTER);
		cs.setVerticalAlignment(cs.VERTICAL_CENTER);
		if (value != 0) {

			String crLf = Character.toString((char) 13) + Character.toString((char) 10);
			cell.setCellStyle(cs);
			cell.setCellValue(value);
		} else {
			cell.setCellStyle(cs);
			cell.setCellValue(0);
		}

	}

	public void setCellValue(String cellName, String value, int width, short xAlign, short yAlign) {

		XSSFCell cell = this.getCell(cellName);
		CellStyle cs = wb.createCellStyle();
		cs.setWrapText(true);
		cs.setBorderLeft((short) 1);
		cs.setBorderRight((short) 1);
		cs.setBorderBottom((short) 1);
		cs.setBorderTop((short) 1);
		cs.setWrapText(true);
		sheet.setColumnWidth(cell.getColumnIndex(), width);
		if (value != null && !(value.equals(""))) {

			if (value.contains(","))
				cs.setAlignment(xAlign);
			else
				cs.setAlignment(xAlign);
			cs.setVerticalAlignment(yAlign);
			String crLf = Character.toString((char) 13) + Character.toString((char) 10);
			cell.setCellStyle(cs);
			cell.setCellValue(value.replaceAll(", ", "," + crLf));
		} else {
			cell.setCellStyle(cs);
			cell.setCellValue("");
		}

	}

	public void setCellValue(String cellName, Date value) {
		CellStyle style = this.wb.createCellStyle();
		style.setBorderLeft((short) 1);
		style.setBorderRight((short) 1);
		style.setBorderBottom((short) 1);
		style.setBorderTop((short) 1);
		style.setAlignment(style.ALIGN_CENTER);
		style.setVerticalAlignment(style.VERTICAL_CENTER);
		CreationHelper createHelper = wb.getCreationHelper();
		style.setDataFormat(createHelper.createDataFormat().getFormat("MMMM dd, yyyy"));
		XSSFCell cell = this.getCell(cellName);
		cell.setCellStyle(style);
		cell.setCellValue(value);

	}

	public void setHeaders(Map<String, String> headers, XSSFColor color) {
		log.debug(this.getClass() + "setHeaders() call starts..");
		for (Entry<String, String> header : headers.entrySet()) {
			XSSFCell cell = this.getCell(header.getKey());
			cell.setCellValue(header.getValue());
			this.sheet.autoSizeColumn(cell.getColumnIndex());

			XSSFCellStyle Cell = wb.createCellStyle();
			// DEclare Font
			XSSFFont font = wb.createFont();
			font.setColor((short) 1);
			Cell.setLocked(true);
			Cell.setBorderLeft((short) 1);
			Cell.setBorderRight((short) 1);
			Cell.setBorderTop((short) 1);
			Cell.setBorderBottom((short) 1);
			Cell.setWrapText(true);
			Cell.setFillBackgroundColor(color);
			Cell.setFillPattern(CellStyle.ALIGN_FILL);
			sheet.setColumnWidth(cell.getColumnIndex(), 6500);
			font.setBold(true);
			font.setBoldweight(Font.BOLDWEIGHT_BOLD);
			Cell.setFont(font);
			// Declare rowExcel Format
			Cell.setDataFormat(wb.createDataFormat().getFormat("#,#0.00"));
			// Declare Alignment
			Cell.setAlignment(Cell.ALIGN_CENTER);

			cell.setCellStyle(Cell);

		}
		log.debug(this.getClass() + "setHeaders() call End..");
	}

	public void setHeader(String cellName, String value, XSSFColor color, int width) {
		XSSFCell cell = this.getCell(cellName);
		cell.setCellValue(value);
		this.sheet.autoSizeColumn(cell.getColumnIndex());

		XSSFCellStyle Cell = wb.createCellStyle();
		// DEclare Font
		XSSFFont font = wb.createFont();
		font.setColor((short) 1);
		Cell.setLocked(true);
		Cell.setBorderLeft((short) 1);
		Cell.setBorderRight((short) 1);
		Cell.setBorderTop((short) 1);
		Cell.setBorderBottom((short) 1);
		Cell.setWrapText(true);
		Cell.setFillBackgroundColor(color);
		Cell.setFillPattern(CellStyle.ALIGN_FILL);
		Cell.setVerticalAlignment(Cell.VERTICAL_CENTER);
		sheet.setColumnWidth(cell.getColumnIndex(), width);
		font.setBold(true);
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		Cell.setFont(font);
		// Declare rowExcel Format
		Cell.setDataFormat(wb.createDataFormat().getFormat("#,#0.00"));
		// Declare Alignment
		Cell.setAlignment(Cell.ALIGN_CENTER);

		cell.setCellStyle(Cell);

	}

	public void setDataValidations(Map<String, String[]> validations) {
		log.debug(this.getClass() + "setDataValidations() call starts..");
		XSSFCell firstCell;
		XSSFCell lastCell;

		DataValidationHelper validationHelper = new XSSFDataValidationHelper(this.sheet);

		for (Entry<String, String[]> entry : validations.entrySet()) {

			String[] cellRange = entry.getKey().split(":");
			firstCell = getCell(cellRange[0]);
			lastCell = getCell(cellRange[1]);

			CellRangeAddressList range = new CellRangeAddressList();
			range.addCellRangeAddress(firstCell.getRowIndex(), firstCell.getColumnIndex(), lastCell.getRowIndex(),
					lastCell.getColumnIndex());

			DataValidationConstraint constraint = validationHelper.createExplicitListConstraint(entry.getValue());

			DataValidation dataValidation = validationHelper.createValidation(constraint, range);
			dataValidation.setErrorStyle(DataValidation.ErrorStyle.WARNING);
			dataValidation.setSuppressDropDownArrow(true);
			sheet.addValidationData(dataValidation);
		}
		log.debug(this.getClass() + "setDataValidations() call End..");
	}

	public void setDataValidations(String rangeAdd, String[] validation) {
		log.debug(this.getClass() + "setDataValidations() call starts..");
		XSSFCell firstCell;
		XSSFCell lastCell;

		String[] cellRange = rangeAdd.split(":");
		firstCell = getCell(cellRange[0]);
		lastCell = getCell(cellRange[1]);

		DataValidationHelper validationHelper = new XSSFDataValidationHelper(this.hiddenSheet);

		for (int i = 0, length = validation.length; i < length; i++) {

			String name = validation[i];
			XSSFRow row = this.hiddenSheet.createRow(i);
			XSSFCell cell = row.createCell(0);
			cell.setCellValue(name);

		}

		Name namedCell = wb.createName();
		namedCell.setNameName("hidden");
		namedCell.setRefersToFormula("hidden!$A$1:$A$" + validation.length);
		DataValidationConstraint constraint = validationHelper.createFormulaListConstraint("hidden");
		CellRangeAddressList addressList = new CellRangeAddressList();
		addressList.addCellRangeAddress(firstCell.getRowIndex(), firstCell.getColumnIndex(), lastCell.getRowIndex(),
				lastCell.getColumnIndex());

		DataValidation dataValidation = validationHelper.createValidation(constraint, addressList);
		wb.setSheetHidden(1, true);

		dataValidation.setErrorStyle(DataValidation.ErrorStyle.WARNING);
		dataValidation.setSuppressDropDownArrow(true);
		sheet.addValidationData(dataValidation);

		log.debug(this.getClass() + "setDataValidations() call End..");
	}

	public String excelReadSingleValue(String cellName) throws IOException {
		log.debug(cellName + "Reads");
		String data = "";
		XSSFCell cell = this.getCell(cellName);
		switch (cell.getCellType()) {
		case Cell.CELL_TYPE_STRING:
			data = cell.getStringCellValue();
			break;

		case Cell.CELL_TYPE_NUMERIC:
			data = "" + cell.getNumericCellValue();
			break;
		}

		return data;

	}

	public Date excelDateValue(String cellName) throws IOException {
		log.debug(cellName + "Reads");
		Date data = null;
		XSSFCell cell = this.getCell(cellName);
		switch (cell.getCellType()) {
		case Cell.CELL_TYPE_NUMERIC:
			data = cell.getDateCellValue();
			break;
		}

		return data;

	}

	public String[] excelReadRow(String cellRange) throws IOException {
		log.debug(cellRange + "Range Reads");
		String[] rowExcel;
		String[] cells = cellRange.split(":");

		CellReference stRow = new CellReference(cells[0]);
		CellReference endRow = new CellReference(cells[1]);
		rowExcel = new String[(endRow.getCol() - stRow.getCol()) + 1];
		int j = 0;
		for (int i = stRow.getCol(); i <= endRow.getCol(); i++) {
			rowExcel[j++] = excelReadSingleValue("" + ((char) (i + 65)) + (stRow.getRow() + 1));
		}
		return rowExcel;
	}

	public String[] excelReadDateFormatRow(String cellRange) throws IOException {
		log.debug(cellRange + "Range Reads");
		String[] rowExcel;
		String[] cells = cellRange.split(":");

		CellReference stRow = new CellReference(cells[0]);
		CellReference endRow = new CellReference(cells[1]);
		rowExcel = new String[(endRow.getCol() - stRow.getCol()) + 1];
		int j = 0;
		for (int i = stRow.getCol(); i <= endRow.getCol(); i++) {
			Date date = excelDateValue("" + ((char) (i + 65)) + (stRow.getRow() + 1));
			DateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
			rowExcel[j++] = df.format(date);

		}
		return rowExcel;
	}

	@SuppressWarnings("null")
	public String[] excelReadColumn(String cellRange) throws IOException {
		log.debug(cellRange + "Range Reads");
		int index = 0;
		String[] columnExcel = new String[100];
		// String[] cells = cellRange.split(regex);

		sheet.getLastRowNum();

		String cellValue = null;
		XSSFRow row = null;
		int colIndex = 1;

		for (int rowIndex = 4; rowIndex <= sheet.getLastRowNum(); rowIndex++) {
			row = sheet.getRow(rowIndex);
			if (row != null) {
				Cell cell = row.getCell(colIndex);
				if (cell != null) {
					cellValue = cell.getStringCellValue();
					if (cellValue != null) {
						columnExcel[index++] = cellValue;
					}
				}
			}

		}
		return columnExcel;
	}

	public List<String> excelReadMultipleRowValue(String cellRange) throws IOException {
		return null;

	}

	public File generateExcelFile() {
		log.debug(this.getClass() + "generateExcelFile() call Starts..");
		FileOutputStream outputFile = null;
		try {
			if (file.exists()) {
				file.delete();
				log.info("file updated....by deleating previous file");
			}
			outputFile = new FileOutputStream(this.file);

			wb.write(outputFile);
			outputFile.flush();
			log.info("Date Successfully Inserted....");
			this.file.setReadOnly();

		} catch (Exception e) {
			log.error(this.getClass() + "generateExcelFile() having error" + e.getMessage());
		} finally {
			try {
				outputFile.close();
				this.closeExcelFile();
			} catch (IOException ioe) {

				log.error(this.getClass() + "generateExcelFile() file not close having error" + ioe.getMessage());
			}
		}

		log.debug(this.getClass() + "generateExcelFile() call Ends..");
		return this.file;
	}

	public void loadSheet(int i) throws Exception {

		wb = new XSSFWorkbook(file);
		sheet = wb.getSheetAt(i);

	}
	
}