package com.supplymint.config.datasource;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 
 * 
 * @author Ashish Singh Dev
 * @version 1.0
 * @since 19-Sep-2018
 *
 */
@Configuration
@ConfigurationProperties("spring.datasource")
public class ConfigProperties {

	private String driverClassName;
	private String username;
	private String password;
	private String url;
	private Integer core_id;
	private String core_uid;
	private String core_name;

	public ConfigProperties() {
	}

	public String getDriverClassName() {
		return driverClassName;
	}

	public void setDriverClassName(String driverClassName) {
		this.driverClassName = driverClassName;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Integer getCore_id() {
		return core_id;
	}

	public void setCore_id(Integer core_id) {
		this.core_id = core_id;
	}

	public String getCore_uid() {
		return core_uid;
	}

	public void setCore_uid(String core_uid) {
		this.core_uid = core_uid;
	}

	public String getCore_name() {
		return core_name;
	}

	public void setCore_name(String core_name) {
		this.core_name = core_name;
	}

}
