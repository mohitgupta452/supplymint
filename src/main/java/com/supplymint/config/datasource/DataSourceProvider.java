package com.supplymint.config.datasource;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

import com.supplymint.layer.business.service.tenant.TenantHelper;
import com.supplymint.layer.data.config.TenantAwareRoutingSource;

/**
 * 
 * 
 * @author Ashish Singh Dev
 * @version 1.0
 * @since 19-Sep-2018
 *
 */
@Configuration
public class DataSourceProvider {

	@Autowired
	private TenantHelper tenantHelper;

	@Bean
	public DataSource dataSource() {

		AbstractRoutingDataSource dataSource = new TenantAwareRoutingSource();
		Map<Object, Object> targetDataSources = new HashMap<>();
		targetDataSources.put(tenantHelper.getTenantHashKey(tenantHelper.getCoreTenant()), tenantHelper.getCoreDs());
		dataSource.setTargetDataSources(targetDataSources);
		dataSource.setDefaultTargetDataSource(tenantHelper.getCoreDs());
		dataSource.afterPropertiesSet();
		return dataSource;
	}

}
