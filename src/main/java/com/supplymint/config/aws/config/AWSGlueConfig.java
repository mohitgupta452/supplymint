package com.supplymint.config.aws.config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.internal.StaticCredentialsProvider;
import com.amazonaws.services.glue.AWSGlue;
import com.amazonaws.services.glue.AWSGlueClient;
import com.amazonaws.services.glue.model.Action;
import com.amazonaws.services.glue.model.BatchStopJobRunRequest;
import com.amazonaws.services.glue.model.BatchStopJobRunResult;
import com.amazonaws.services.glue.model.Column;
import com.amazonaws.services.glue.model.Condition;
import com.amazonaws.services.glue.model.ConnectionInput;
import com.amazonaws.services.glue.model.CreateConnectionRequest;
import com.amazonaws.services.glue.model.CreateTriggerRequest;
import com.amazonaws.services.glue.model.CreateTriggerResult;
import com.amazonaws.services.glue.model.DeleteTriggerRequest;
import com.amazonaws.services.glue.model.DeleteTriggerResult;
import com.amazonaws.services.glue.model.GetJobRequest;
import com.amazonaws.services.glue.model.GetJobResult;
import com.amazonaws.services.glue.model.GetJobRunRequest;
import com.amazonaws.services.glue.model.GetJobRunResult;
import com.amazonaws.services.glue.model.GetJobRunsRequest;
import com.amazonaws.services.glue.model.GetJobRunsResult;
import com.amazonaws.services.glue.model.GetJobsRequest;
import com.amazonaws.services.glue.model.GetJobsResult;
import com.amazonaws.services.glue.model.GetTableVersionsRequest;
import com.amazonaws.services.glue.model.GetTableVersionsResult;
import com.amazonaws.services.glue.model.JobRun;
import com.amazonaws.services.glue.model.PhysicalConnectionRequirements;
import com.amazonaws.services.glue.model.Predicate;
import com.amazonaws.services.glue.model.StartJobRunRequest;
import com.amazonaws.services.glue.model.StartJobRunResult;
import com.amazonaws.services.glue.model.StartTriggerRequest;
import com.amazonaws.services.glue.model.StartTriggerResult;
import com.amazonaws.services.glue.model.StopTriggerRequest;
import com.amazonaws.services.glue.model.StopTriggerResult;
import com.amazonaws.services.glue.model.TableVersion;
import com.amazonaws.services.glue.model.TriggerUpdate;
import com.amazonaws.services.glue.model.UpdateTriggerRequest;
import com.amazonaws.services.glue.model.UpdateTriggerResult;
import com.supplymint.config.aws.utils.AWSUtils;
import com.supplymint.config.aws.utils.AWSUtils.ConnectionProperties;
import com.supplymint.config.aws.utils.AWSUtils.RunState;
import com.supplymint.config.aws.utils.AWSUtils.Type;
import com.supplymint.util.CodeUtils;
import com.supplymint.util.CronUtil;

/**
 * Class AWSGlueConfig used to configure Glue client for accessing AWS glue
 * services , generate triggers , run your jobs and populate Glue metadata
 * 
 * @author Manoj Singh
 * @since 18 OCT 2018
 * @version 1.0
 */

@Configuration
@PropertySource("classpath:aws.properties")
public class AWSGlueConfig {

	private final static Logger logger = LoggerFactory.getLogger(AWSGlueConfig.class);

	private String connectionType = "aws.glue.connectiontype";

	private String description = "aws.glue.description";

	private String name = "aws.glue.name";

	private String availabilityZone = "aws.glue.physical.azone";

	private String region = "aws.glue.client.region";

	private String catalogId = "aws.glue.catalogId";

	private String subnetID = "aws.glue.physical.subnetid";

	private String vpcID = "aws.glue.physical.vpcid";

	private String securityGroups = "aws.glue.physical.securityGrp";

	private String connectionURL = "aws.glue.connection.url";

	private String driverClassName = "aws.glue.driverClassName";

	private String access_key_id = "aws.credentails.access_key_id";

	private String secret_access_key = "aws.credentails.secret_access_key";

	private String jobRunId = null;

	private String jobRunName = null;

	private String[] conditionStateArray = { "SUCCEEDED", "STOPPED", "TIMEOUT", "FAILED" };

	@Autowired
	private Environment env;

	public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
		return new PropertySourcesPlaceholderConfigurer();
	}

	/**
	 * Public Client for accessing AWS Glue. All service calls made using this
	 * client.
	 * 
	 * @return {@link AWSGlue}
	 */
//	@Bean
	public AWSGlue awsGlueClient() {
		logger.debug(String.format("AWS glue client for region %s", region));
		return AWSGlueClient.builder().withRegion(env.getProperty(region)).build();

	}

	/**
	 * Provides access to the AWS credentials used for accessing AWS services: AWS
	 * access key ID and secret access key. These credentials are used to securely
	 * sign requests to AWS services.
	 */
	private AWSCredentials awsCredentials() {
		AWSCredentials awsCredentials = new BasicAWSCredentials(env.getProperty(access_key_id),
				env.getProperty(secret_access_key));
		return awsCredentials;
	}

	@SuppressWarnings("deprecation")
	private AWSCredentialsProvider getCredentialsProvider() {
		AWSCredentialsProvider credentialsProvider;
		if (!env.getProperty(access_key_id).isEmpty() && !env.getProperty(secret_access_key).isEmpty()) {
			credentialsProvider = new StaticCredentialsProvider(
					new BasicAWSCredentials(env.getProperty(access_key_id), env.getProperty(secret_access_key)));
		} else {
			credentialsProvider = new DefaultAWSCredentialsProviderChain();
		}
		return credentialsProvider;
	}

	/**
	 * Create Data catalog connection
	 * 
	 * @return {@link CreateConnectionRequest}
	 */
	public CreateConnectionRequest CreateConnectionRequest() {
		CreateConnectionRequest connection = new CreateConnectionRequest();
		connection.setCatalogId(env.getProperty(catalogId));
		connection.setConnectionInput(connectionInput());
		logger.debug(String.format("Connection result %s", connection));
		return connection;

	}

	/**
	 * Specify a connection properties to create or update
	 * 
	 * @return {@link ConnectionInput}
	 */
	private ConnectionInput connectionInput() {
		ConnectionInput connectionInput = new ConnectionInput();
		connectionInput.setConnectionProperties(connectionProperties());
		connectionInput.setConnectionType(env.getProperty(connectionType));
		connectionInput.setDescription(description);
		connectionInput.setPhysicalConnectionRequirements(physicalConnectionRequirements());
		connectionInput.setMatchCriteria(matchCriteria());
		connectionInput.setName(env.getProperty(name));
		return connectionInput;
	}

	/**
	 * Return list of connection configuration
	 * 
	 * @return {@link Map}
	 */
	private Map<String, String> connectionProperties() {
		Map<String, String> connectionProperties = new HashMap<>();
		connectionProperties.put(ConnectionProperties.JDBC_CONNECTION_URL.toString(), env.getProperty(connectionURL));
		connectionProperties.put(ConnectionProperties.JDBC_DRIVER_CLASS_NAME.toString(),
				env.getProperty(driverClassName));
		return connectionProperties;
	}

	private Collection<String> matchCriteria() {
		Collection<String> matchCriteria = new ArrayList<>();
		return matchCriteria;
	}

	/**
	 * Collections of security groups
	 * 
	 * @return {@link Collection}
	 */
	private Collection<String> securityGroupIdList() {
		Collection<String> securityGroupIdList = new ArrayList<>();
		securityGroupIdList.add(env.getProperty(securityGroups));
		return securityGroupIdList;
	}

	/**
	 * Specifies the physical requirements for a connection.
	 * 
	 * @return {@link PhysicalConnectionRequirements}
	 */
	private PhysicalConnectionRequirements physicalConnectionRequirements() {
		PhysicalConnectionRequirements physicalConnectionRequirements = new PhysicalConnectionRequirements();
		physicalConnectionRequirements.setAvailabilityZone(availabilityZone);
		physicalConnectionRequirements.setSecurityGroupIdList(securityGroupIdList());
		physicalConnectionRequirements.setSubnetId(env.getProperty(subnetID));
		return physicalConnectionRequirements;
	}

	/**
	 * Return List of tables from AWS Glue data catalog
	 * 
	 * @param dbName
	 * @param catalog
	 * @return {@link Collection}
	 */
	public Collection<Column> getTableColumns(String dbName, String tableName) {
		GetTableVersionsRequest tableVersionsRequest = new GetTableVersionsRequest().withDatabaseName(dbName)
				.withTableName(tableName).withRequestCredentialsProvider(getCredentialsProvider());
		GetTableVersionsResult results = awsGlueClient().getTableVersions(tableVersionsRequest);
		List<TableVersion> versions = results.getTableVersions();
		List<Column> tableColumns = versions.get(0).getTable().getStorageDescriptor().getColumns();
		return tableColumns;
	}

	/**
	 * Return requested job definition
	 * 
	 * @param jobName
	 * @return {@link GetJobResult}
	 */
	@SuppressWarnings("deprecation")
	public GetJobResult getJobDetails(String jobName) {
		GetJobRequest getJobRequest = new GetJobRequest();
		getJobRequest.setJobName(jobName);
		getJobRequest.setRequestCredentials(awsCredentials());
		getJobRequest.setSdkRequestTimeout(3000);
//		getJobRequest.setGeneralProgressListener(progressListener);
		return awsGlueClient().getJob(getJobRequest);
	}

	/**
	 * Return all specified job details on AWS Glue
	 * 
	 * @return {@link GetJobsResult}
	 */
	@SuppressWarnings("deprecation")
	public GetJobsResult getAllJobsDetail() {
		GetJobsRequest getJobsRequest = new GetJobsRequest();
		getJobsRequest.setRequestCredentials(awsCredentials());
		return awsGlueClient().getJobs(getJobsRequest);
	}

	private Collection<String> jobRunIds(String jobRunId) {
		Collection<String> jobRunIds = new ArrayList<>();
		jobRunIds.add(jobRunId);
		return jobRunIds;
	}

	/**
	 * Specifies to run job and return run ID on succeed
	 * 
	 * @param jobName
	 * @return {@link StartJobRunResult}
	 */
	@SuppressWarnings("deprecation")
	public StartJobRunResult jobRunRequest(String jobName) {
		StartJobRunRequest jobRunRequest = new StartJobRunRequest();
		jobRunRequest.setJobName(jobName);
		jobRunRequest.setRequestCredentials(awsCredentials());
		StartJobRunResult jobRunResult = awsGlueClient().startJobRun(jobRunRequest);
		return jobRunResult;
	}

	/**
	 * Specifies to run job with custom fields and return run ID on succeed
	 * 
	 * @param jobName
	 * @return {@link StartJobRunResult}
	 */
	@SuppressWarnings("deprecation")
	public GetJobRunResult jobRunRequestWithCustomFields(String jobName, Map<String, String> arguments) {
		StartJobRunRequest jobRunRequest = new StartJobRunRequest();
		jobRunRequest.setJobName(jobName);
		jobRunRequest.setRequestCredentials(awsCredentials());
		jobRunRequest.setArguments(arguments);
		return getJobRunResult(awsGlueClient().startJobRun(jobRunRequest).getJobRunId(), jobName);
	}

	/**
	 * Specifies a request to run job and return job-run metadata
	 * 
	 * @param jobName
	 * @return {@link GetJobRunResult}
	 */
	@SuppressWarnings("deprecation")
	public GetJobRunResult jobRunResult(String jobName) {
		StartJobRunRequest jobRunRequest = new StartJobRunRequest();
		jobRunRequest.setJobName(jobName);
		jobRunRequest.setRequestCredentials(awsCredentials());
		return getJobRunResult(awsGlueClient().startJobRun(jobRunRequest).getJobRunId(), jobName);
	}

	/**
	 * Specifies requested job-run metadata
	 * 
	 * @param runId
	 * @param jobName
	 * @return {@link GetJobRunResult}
	 */
	public GetJobRunResult getJobRunResult(String runId, String jobName) {
		GetJobRunRequest getJobRunRequest = new GetJobRunRequest();
		getJobRunRequest.setRunId(runId);
		getJobRunRequest.setJobName(jobName);
		getJobRunRequest.setRequestCredentials(awsCredentials());
		return awsGlueClient().getJobRun(getJobRunRequest);
	}

	/**
	 * Specifies name of a job to be executed
	 * 
	 * @param jobName
	 * @return {@link Collection}
	 */
	private Collection<Action> actions(String jobName) {
		Action action = new Action();
		Collection<Action> actions = new ArrayList<>();
		action.setJobName(jobName);
		actions.add(action);

		return actions;
	}

	/**
	 * Specifies name of a job to be executed
	 * 
	 * @param jobName
	 * @return {@link Collection}
	 */
	private Collection<Action> actionsWithArguments(String jobName, Map<String, String> arguments) {
		Action action = new Action();
		Collection<Action> actions = new ArrayList<>();
		action.setJobName(jobName);
		action.setArguments(arguments);
		actions.add(action);
		return actions;
	}

	/**
	 * Generate regular cron expression based on scheduled date with zone and
	 * defined frequency
	 * 
	 * @param date
	 * @param timeZone
	 * @param frequency
	 * @return {@link String}
	 */
	public String generateCronExpression(String date, String timeZone, String frequency, String dayWeek,
			String dayMonth) {
		Date cronDate = CodeUtils.convertDateByZoneId(date, timeZone);
		CronUtil cronHelper = new CronUtil(cronDate);
		String cronExpression = "cron";
		switch (frequency) {
		case "WEEKLY":
			// cron(Minutes Hours Day-of-month Month Day-of-week Year)

			cronExpression += "(" + cronHelper.getMins() + " " + cronHelper.getHours() + " "
					+ AWSUtils.WildCard.DONTCARE + " " + AWSUtils.WildCard.EVERY + " "
					+ (dayWeek == "NA" ? cronHelper.getDaysOfWeek() : dayWeek) + " " + cronHelper.getYears() + ")";
			logger.debug(String.format("Cron Expression : %s", cronExpression));
			break;
		case "MONTHLY":
			cronExpression += "(" + cronHelper.getMins() + " " + cronHelper.getHours() + " "
					+ (dayMonth != "NA" ? cronHelper.getDaysOfMonth() : dayMonth) + " " + dayMonth + " "
					+ AWSUtils.WildCard.DONTCARE + " " + cronHelper.getYears() + ")";
			logger.debug(String.format("Cron Expression : %s", cronExpression));
			break;
		case "HOURLY":
			cronExpression += "(" + cronHelper.getMins() + " " + cronHelper.getHour() + AWSUtils.WildCard.INCREMENTS
					+ cronHelper.getHours() + " " + AWSUtils.WildCard.EVERY + " " + AWSUtils.WildCard.EVERY + " "
					+ AWSUtils.WildCard.DONTCARE + " " + cronHelper.getYears() + ")";
			break;
		case "FORTNIGHTLY":
			String dayOfMonth = Integer.parseInt(cronHelper.getDaysOfMonth()) <= 15 ? (cronHelper.getDaysOfMonth())
					: (cronHelper.getDaysOfMonth() + AWSUtils.WildCard.INCLUDE + "14");
			cronExpression += "(" + cronHelper.getMins() + " " + cronHelper.getHours() + " " + dayOfMonth + " "
					+ AWSUtils.WildCard.EVERY + " " + AWSUtils.WildCard.DONTCARE + " " + cronHelper.getYears() + ")";
			logger.debug(String.format("Cron Expression : %s", cronExpression));
			break;
		default:
			cronExpression += "(" + cronHelper.getMins() + " " + cronHelper.getHours() + " " + AWSUtils.WildCard.EVERY
					+ " " + AWSUtils.WildCard.EVERY + " " + AWSUtils.WildCard.DONTCARE + " " + AWSUtils.WildCard.EVERY
					+ ")";
			logger.debug(String.format("Cron Expression : %s", cronExpression));
			break;
		}
		logger.debug(String.format("Cron Expression %s", cronExpression));
		return cronExpression;
	}

	/**
	 * Defines the predicate of the trigger, which determines when it fires
	 * 
	 * @param jobName
	 * @return {@link Predicate}
	 */
	private Predicate predicate(String jobName) {
		Predicate predicate = new Predicate();
		Collection<Condition> conditions = new ArrayList<>();
		for (String condtionState : Arrays.asList(conditionStateArray)) {
			Condition condition = new Condition();
			condition.setJobName(jobName);
			condition.setLogicalOperator(com.supplymint.config.aws.utils.AWSUtils.Condition.EQUALS.toString());
			condition.setState(condtionState);
			conditions.add(condition);
		}
		predicate.setConditions(conditions);
		return predicate;
	}

	/**
	 * Specifies triggers that run AWS Glue jobs.
	 * 
	 * @param triggerName
	 * @param jobName
	 * @param schedule
	 * @param timeZone
	 * @param frequency
	 * @param type
	 * @return {@link CreateTriggerResult}
	 */
	@SuppressWarnings("deprecation")
	public CreateTriggerResult createTrigger(String triggerName, String jobName, String schedule, String timeZone,
			String frequency, String type, String dayWeek, String dayMonth, Map<String, String> arguments) {
		logger.debug("Invoking fuction to create Trigger on AWS Glue");
		CreateTriggerRequest createTriggerRequest = new CreateTriggerRequest();
		createTriggerRequest.setRequestCredentials(awsCredentials());
		createTriggerRequest.setName(triggerName);
		createTriggerRequest.setActions(actionsWithArguments(jobName, arguments));
		createTriggerRequest.setPredicate(predicate(jobName));
		switch (type) {
		case "CONDITIONAL":
			createTriggerRequest.setType(String.valueOf(Type.CONDITIONAL));
			break;
		case "ON_DEMAND":
			createTriggerRequest.setType(String.valueOf(Type.ON_DEMAND));
			break;
		default:
			createTriggerRequest.setStartOnCreation(true);
			createTriggerRequest.setSchedule(generateCronExpression(schedule, timeZone, frequency, dayWeek, dayMonth));
			createTriggerRequest.setType(String.valueOf(Type.SCHEDULED));
			break;
		}
		logger.debug(String.format("Configured Trigger Information : %s", createTriggerRequest));
		return awsGlueClient().createTrigger(createTriggerRequest);
	}

	/**
	 * Specifies triggers that stop AWS Glue jobs.
	 * 
	 * @param triggerName
	 * @return {@link StopTriggerResult}
	 */
	@SuppressWarnings("deprecation")
	public StopTriggerResult stopTrigger(String triggerName) {
		StopTriggerRequest stopTriggerRequest = new StopTriggerRequest();
		stopTriggerRequest.setName(triggerName);
		stopTriggerRequest.setRequestCredentials(awsCredentials());
		return awsGlueClient().stopTrigger(stopTriggerRequest);
	}

	private TriggerUpdate triggerUpdate(String triggerName, String jobName, String schedule, String timeZone,
			String frequency, String type, String dayWeek, String dayMonth, Map<String, String> arguments) {
		TriggerUpdate triggerUpdate = new TriggerUpdate();
		triggerUpdate.setActions(actionsWithArguments(jobName, arguments));
		triggerUpdate.setDescription(triggerName);
		triggerUpdate.setName(triggerName);
//		triggerUpdate.setPredicate(predicate(jobName));
		triggerUpdate.setSchedule(generateCronExpression(schedule, timeZone, frequency, dayWeek, dayMonth));
		return triggerUpdate;
	}

	/**
	 * Specifies the trigger definition
	 * 
	 * @param triggerName
	 * @param jobName
	 * @param schedule
	 * @param timeZone
	 * @param frequency
	 * @param type
	 * @return {@link UpdateTriggerResult}
	 */
	@SuppressWarnings("deprecation")
	public UpdateTriggerResult updateTrigger(String triggerName, String jobName, String schedule, String timeZone,
			String frequency, String type, String dayWeek, String dayMonth, Map<String, String> arguments) {
		UpdateTriggerRequest updateTriggerRequest = new UpdateTriggerRequest();
		updateTriggerRequest.setName(triggerName);
		updateTriggerRequest.setRequestCredentials(awsCredentials());
		updateTriggerRequest.setTriggerUpdate(
				triggerUpdate(triggerName, jobName, schedule, timeZone, frequency, type, dayWeek, dayMonth, arguments));
		return awsGlueClient().updateTrigger(updateTriggerRequest);
	}

	/**
	 * Specifies the trigger that need to be activated
	 * 
	 * @param jobName
	 * @param triggerName
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public StartTriggerResult startTriggerOnDemand(String triggerName) {
		StartTriggerRequest startTriggerRequest = new StartTriggerRequest();
		startTriggerRequest.setName(triggerName);
		startTriggerRequest.setRequestCredentials(awsCredentials());
		return awsGlueClient().startTrigger(startTriggerRequest);
	}

	/**
	 * Specifies the job definition for which to stop job runs
	 * 
	 * @param jobName
	 * @param jobId
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public BatchStopJobRunResult stopJobRunRequest(String jobName, String jobId) {
		BatchStopJobRunRequest batchStopJobRunRequest = new BatchStopJobRunRequest();
		batchStopJobRunRequest.setJobName(jobName);
		batchStopJobRunRequest.setJobRunIds(jobRunIds(jobId));
		batchStopJobRunRequest.setRequestCredentials(awsCredentials());
		return awsGlueClient().batchStopJobRun(batchStopJobRunRequest);
	}

	/**
	 * Return JobID which is scheduled and run state equals to RUNNING for
	 * corresponding JobName .
	 * 
	 * @param jobName
	 * @return {@link Map}
	 */
	@SuppressWarnings({ "deprecation" })
	public Map<String, String> getJobRunsResult(String jobName) {
		Map<String, String> jobRunResult = new HashMap<>();
		GetJobRunsRequest getJobRunsRequest = new GetJobRunsRequest();
		getJobRunsRequest.setJobName(jobName);
		getJobRunsRequest.setRequestCredentials(awsCredentials());
		awsGlueClient().getJobRuns(getJobRunsRequest).getJobRuns().stream().forEach(e -> {
			if (e.getJobRunState().equalsIgnoreCase(RunState.RUNNING.toString())) {
				jobRunResult.put(e.getJobName(), e.getId());
			}
		});
		return jobRunResult;
	}

	private String dayOfWeekAsString(int value, String frequency) {
		if (frequency.equalsIgnoreCase("WEEKLY")) {
			String[] stringValueOfDM = { "SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT" };
			return stringValueOfDM[value];
		} else {
			String[] stringValueOfDM = { "MONTH", "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT",
					"NOV", "DEC" };
			return stringValueOfDM[value];
		}
	}

	private Map getNextString(List<String> listOfDM, int index, String frequency) {
		Map<Integer, String> nextValue = new HashMap<>();
		for (int i = 0, j = 0; i < (frequency.equalsIgnoreCase("WEEKLY") ? 7 : 12); i++) {
			index = index < (frequency.equalsIgnoreCase("WEEKLY") ? 6 : 11) ? index + 1 : j++;
			String dmString = listOfDM.contains(dayOfWeekAsString(index, frequency))
					? dayOfWeekAsString(index, frequency)
					: null;
			if (dmString != null) {
				nextValue.put(i + 1, dmString);
				break;
			}
		}
		return nextValue;
	}

	/**
	 * Specifies next scheduled of the Cron Job
	 * 
	 * @param frequency
	 * @param schedule
	 * @param timeZone
	 * @param dayWeek
	 * @param dayMonth
	 * @return {@link String}
	 */
	@SuppressWarnings({ "deprecation", "unchecked" })
	public String retrieveNextScheduled(String frequency, String schedule, String timeZone, String dayWeek,
			String dayMonth) {
		Date cronDate = CodeUtils.convertDateByZoneId(schedule, timeZone);
		Calendar mCal = Calendar.getInstance();
		mCal.setTime(cronDate);
		String nextScdeuled = null;
		switch (frequency) {
		case "WEEKLY":
			List<String> dWeeks = Arrays.stream(dayWeek.split(",")).collect(Collectors.toList());
			Map<Integer, String> nextWeek = getNextString(dWeeks, mCal.getTime().getDay(), frequency);
			for (Map.Entry<Integer, String> entry : nextWeek.entrySet()) {
				mCal.add(Calendar.DAY_OF_MONTH, entry.getKey());
				nextScdeuled = CodeUtils._____dateFormat.format(mCal.getTime());
				logger.debug(String.format("Next WeekDays Scheduled : %s ", nextScdeuled));
			}
			break;
		case "MONTHLY":
			List<String> dMonths = Arrays.stream(dayMonth.split(",")).collect(Collectors.toList());
			Map<Integer, String> nextMonth = getNextString(dMonths, mCal.getTime().getDay(), frequency);
			for (Map.Entry<Integer, String> entry : nextMonth.entrySet()) {
				mCal.add(Calendar.MONTH, entry.getKey());
				nextScdeuled = CodeUtils._____dateFormat.format(mCal.getTime());
				logger.debug(String.format("Next Month Scheduled : %s ", nextScdeuled));
			}
			break;
		default:
			mCal.add(Calendar.DAY_OF_MONTH, 1);
			nextScdeuled = CodeUtils._____dateFormat.format(mCal.getTime());
			logger.debug(String.format("Next Daily Scheduled : %s ", nextScdeuled));
			break;
		}
		return nextScdeuled;
	}

	/**
	 * Specifies triggers that delete AWS Glue jobs.
	 * 
	 * @param triggerName
	 * @return {@link DeleteTriggerResult}
	 */
	@SuppressWarnings("deprecation")
	public DeleteTriggerResult deleteTriggerResult(String triggerName) {
		DeleteTriggerRequest deleteTriggerRequest = new DeleteTriggerRequest();
		deleteTriggerRequest.setName(triggerName);
		deleteTriggerRequest.setRequestCredentials(awsCredentials());
		return awsGlueClient().deleteTrigger(deleteTriggerRequest);
	}

	@SuppressWarnings({ "deprecation" })
	public GetJobRunsResult getAllJobRunsResult(String jobName) {
		List<JobRun> jobRunResult = new ArrayList<>();
		GetJobRunsRequest getJobRunsRequest = new GetJobRunsRequest();
		getJobRunsRequest.setJobName(jobName);
		getJobRunsRequest.setRequestCredentials(awsCredentials());
		awsGlueClient().getJobRuns(getJobRunsRequest).getJobRuns().stream().forEach(e -> {
			if (e.getJobRunState().equalsIgnoreCase(RunState.SUCCEEDED.toString())) {
				jobRunResult.add(e);
			}
		});
		logger.debug(String.format("List of Job Runs : %s", jobRunResult));
		return awsGlueClient().getJobRuns(getJobRunsRequest);
	}

	@SuppressWarnings({ "deprecation" })
	public List<JobRun> listAllJobRunsResult(String jobName) {
		GetJobRunsRequest getJobRunsRequest = new GetJobRunsRequest();
		getJobRunsRequest.setJobName(jobName);
		getJobRunsRequest.setRequestCredentials(awsCredentials());
		return awsGlueClient().getJobRuns(getJobRunsRequest).getJobRuns();
	}

}
