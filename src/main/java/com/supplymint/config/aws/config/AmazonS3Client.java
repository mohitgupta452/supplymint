/**
 * 
 */
package com.supplymint.config.aws.config;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.internal.StaticCredentialsProvider;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ListObjectsV2Request;
import com.amazonaws.services.s3.model.ListObjectsV2Result;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.ResponseHeaderOverrides;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.supplymint.util.FileUtils;

/**
 * @author Manoj Singh
 * @since 20 OCT 2018
 * @version 1.0
 */
@Configuration
@PropertySource("classpath:aws.properties")
public class AmazonS3Client {

	/**
	 * Case insensitive Logger constant used to retrieve the logger for various
	 * APIs.
	 *
	 */
	private final static Logger LOG = LoggerFactory.getLogger(AmazonS3Client.class);

	private String region = "aws.s3.client.region";

	private String access_key_id = "aws.s3.credentails.access_key_id";

	private String secret_access_key = "aws.s3.credentails.secret_access_key";

	@Autowired
	private Environment env;

	public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
		return new PropertySourcesPlaceholderConfigurer();
	}

	/**
	 * Public Client for accessing Amazon S3. All service calls made using this
	 * client.
	 * 
	 * @return {@link AmazonS3}
	 */
//	@Bean
	public AmazonS3 awsS3ClientWithCredentials() {
		LOG.debug(String.format("Amazon S3 client for region %s", region));
		return AmazonS3ClientBuilder.standard().withPathStyleAccessEnabled(true).disableChunkedEncoding()
				.withCredentials(getCredentialsProvider()).withRegion(env.getProperty(region)).build();

	}

	/**
	 * Public Client for accessing Amazon S3. All service calls made using this
	 * client.
	 * 
	 * 
	 * @return {@link AmazonS3}
	 */
//	@Bean
	public AmazonS3 awsS3Client() {
		LOG.debug(String.format("Amazon S3 client for region %s", region));
		return AmazonS3ClientBuilder.standard().withRegion(region).build();

	}

	/**
	 * Provides access to the AWS credentials used for accessing AWS services: AWS
	 * access key ID and secret access key. These credentials are used to securely
	 * sign requests to AWS services.
	 */
	private AWSCredentials awsCredentials() {
		AWSCredentials awsCredentials = new BasicAWSCredentials(env.getProperty(access_key_id),
				env.getProperty(secret_access_key));
		return awsCredentials;
	}

	@SuppressWarnings("deprecation")
	private AWSCredentialsProvider getCredentialsProvider() {
		AWSCredentialsProvider credentialsProvider;
		if (!env.getProperty(access_key_id).isEmpty() && !env.getProperty(secret_access_key).isEmpty()) {
			credentialsProvider = new StaticCredentialsProvider(
					new BasicAWSCredentials("AKIAJ4VC37HSIUDIPMYA", "vP+tA9PyJdrvZDLQS0NqZPbKqrpr4XyFAKcmg/4U"));
		} else {
			credentialsProvider = new DefaultAWSCredentialsProviderChain();
		}
		return credentialsProvider;
	}

	/**
	 * Return the list of buckets specified on S3
	 * 
	 * @return {@link List}
	 */
	public List getBuckets() {
		return awsS3ClientWithCredentials().listBuckets();
	}

	public List<S3ObjectSummary> list(String bucket) {
		ObjectListing objectListing = awsS3ClientWithCredentials()
				.listObjects(new ListObjectsRequest().withBucketName(bucket));
		return objectListing.getObjectSummaries();
	}

	public String getFileSave(String bucket) {
		File localFile = new File(FileUtils.getPlatformBasedParentDir().getAbsolutePath());
		ObjectMetadata object = awsS3ClientWithCredentials()
				.getObject(new GetObjectRequest("bucket", localFile.getAbsolutePath()), localFile);
		LOG.debug(String.format("File path : %s , S3 File Object%s", localFile, object));
		return null;

	}

	public List getFiles(String bucket) {
		ListObjectsV2Request req = new ListObjectsV2Request().withBucketName(bucket)
				.withPrefix("dev_etl/citylife/output").withDelimiter("/");
		ListObjectsV2Result listing = awsS3ClientWithCredentials().listObjectsV2(req);
		for (String commonPrefix : listing.getCommonPrefixes()) {
			System.out.println(commonPrefix);
		}
		for (S3ObjectSummary summary : listing.getObjectSummaries()) {
			System.out.println(summary.getKey());
		}
		return null;
	}

	public S3ObjectSummary getLastRunObject(String bucket, String key, Date date) {
//		ListObjectsV2Result result = awsS3ClientWithCredentials().listObjectsV2(bucket);
		ObjectListing results = awsS3ClientWithCredentials().listObjects(bucket, key);
		List<S3ObjectSummary> objects = new ArrayList<>();
		results.getObjectSummaries().stream().filter(e -> e.getLastModified().after(date)).forEach(summary -> {
			objects.add(summary);
		});
		for (S3ObjectSummary os : objects) {
			LOG.debug(String.format("Bucket Keys ouput : %s", os.getKey()) + " : " + os.getLastModified());
		}
		return objects.get(0);
	}

	public S3Object getObject(String bucket, String key) {
		S3Object s3Object = awsS3ClientWithCredentials().getObject(bucket, key);
		return s3Object;
	}

	public Bucket createS3Bucket(String bucket) {
		Bucket s3ObjectBucket = awsS3ClientWithCredentials().createBucket(bucket);
		return s3ObjectBucket;
	}

	public S3ObjectSummary getObjectSummary(String bucket, String key) {
		ObjectListing results = awsS3ClientWithCredentials().listObjects(bucket, key);
		List<S3ObjectSummary> objects = new ArrayList<>();
		results.getObjectSummaries().stream().forEach(summary -> {
			objects.add(summary);
		});
		return objects.get(0);
	}

	public List<S3ObjectSummary> getObjectSummaries(String bucket, String key) {
		ObjectListing results = awsS3ClientWithCredentials().listObjects(bucket, key);
		List<S3ObjectSummary> objects = new ArrayList<>();
		results.getObjectSummaries().stream().forEach(summary -> {
			objects.add(summary);
		});
		LOG.info(objects.toString());
		return objects;
	}

	public S3Object getObjectList(String bucket, String key) {
		S3Object s3Object = awsS3ClientWithCredentials().getObject(bucket, key);
		return s3Object;
	}

	public S3Object getObjectRequest(String bucket, String key) {
		GetObjectRequest rangeObjectRequest = new GetObjectRequest(bucket, key);
		S3Object s3Object = awsS3ClientWithCredentials().getObject(rangeObjectRequest);
		return s3Object;
	}

	public S3Object getRangeOfBytes(String bucket, String key) {
		GetObjectRequest rangeObjectRequest = new GetObjectRequest(bucket, key).withRange(0, 9);
		S3Object s3Object = awsS3ClientWithCredentials().getObject(rangeObjectRequest);
		return s3Object;
	}
}
