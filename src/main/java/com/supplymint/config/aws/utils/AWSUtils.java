/**
 * 
 */
package com.supplymint.config.aws.utils;

/**
 * Interface AWSUtils define the constant and enums which result into defining
 * configuring and scheduling the AWS glue connection .
 * 
 * @author Manoj Singh
 * @since 18 OCT 2018
 * @version 1.0
 *
 */
public interface AWSUtils {

	public enum ConnectionProperties {
		HOST, PORT, USERNAME, PASSWORD, JDBC_DRIVER_JAR_URI, JDBC_DRIVER_CLASS_NAME, JDBC_ENGINE, JDBC_ENGINE_VERSION,
		CONFIG_FILES, INSTANCE_ID, JDBC_CONNECTION_URL
	}

	public enum ConnectionType {
		JDBC, SFTP
	}

	public enum Frequency {
		WEEKLY, MONTHLY, HOURLY, FORTNIGHTLY, FREQUENCY
	}
	
	public enum SessionDetail {
		LOGGEDIN,LOGGEDOUT,FALSE,TRUE
	}


	public enum Schedule {
		MINUTES, HOURS, DAY_OF_MONTH, MONTH, DAY_OF_WEEK, YEAR
	}

	public enum ScheduleTime {
		DATETIME, TIMEZONE
	}

	public enum Type {
		SCHEDULED, CONDITIONAL, ON_DEMAND
	}
	
	public enum Status {
		UPLOADED,PROCESS,VERIFY,SUCCEEDED, FAILED,INPROGRESS,PARTIALERROR
	}


	public enum RunState {
		STOPPED, RUNNING, FAILED, SUCCEEDED, STARTING, STOPPING, TIMEOUT, PROCESSING, INPROGRESS, PROGRESS, ERROR
	}

	public enum Condition {
		EQUALS, AND, ANY ,TRUE , FALSE
	}
	
	public enum Conditions {
		EQUALS, AND, ANY ,TRUE , FALSE, OR
	}

	public enum ConditionState {
		SUCCEEDED, STOPPED, TIMEOUT, FAILED, ERROR, EXCEPTION
	}

	interface WildCard {
		final String INCLUDE = ",";
		final String SPECIFIED = "-";
		final String EVERY = "*";
		final String INCREMENTS = "/";
		final String DONTCARE = "?";
		final String LASTDAY = "L";
		final String WEEKDAY = "W";
		final String CERTAIN = "#";
	}

	interface Percentage {
		final String COMPLETED = "100%";
		final String INPROGRESS = "50%";
	}

	interface CustomParam {
		final String NOT_APPLICABLE = "NA";
		final String APPLICABLE = "OK";
		final String PATH = "--path";
		final String STORE = "--store";
		final String STORECODE = "--store_code";
		final String STOCKPOINT="--stock_point";
		final String TRIGGER = "--trigger";
		final String ONDEMAND = "ONDEMAND";
		final String SCHEDULED = "SCHEDULED";
		final String PREFIX="--prefix";
	}
	
	public static enum EmailModuleSubModule{
		EMAIL_DEMANDPLANNING_MODULE, EMAIL_INVENTORY_MODULE,EMAIL_ADMINISTRATION_MODULE,
		EMAIL_RUN_ON_DEMAND_SUBMODULE, EMAIL_AUTO_CONFIGURATION_SUBMODULE, FORECAST_ON_DEMAND_SUBMODULE
	}
}
