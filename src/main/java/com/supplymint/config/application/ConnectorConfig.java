package com.supplymint.config.application;

import org.apache.catalina.connector.Connector;
import org.springframework.context.annotation.Configuration;

/**
 * @author Manoj Singh
 * @since 20 OCT 2018
 * @version 1.0
 */
@Configuration
public class ConnectorConfig {

	// @Bean
	// public EmbeddedServletContainerFactory servletContainer() {
	// TomcatEmbeddedServletContainerFactory tomcat = new
	// TomcatEmbeddedServletContainerFactory() {
	// @Override
	// protected void postProcessContext(Context context) {
	// SecurityConstraint securityConstraint = new SecurityConstraint();
	// securityConstraint.setUserConstraint("CONFIDENTIAL");
	// SecurityCollection collection = new SecurityCollection();
	// collection.addPattern("/*");
	// securityConstraint.addCollection(collection);
	// context.addConstraint(securityConstraint);
	// }
	// };
	// tomcat.addAdditionalTomcatConnectors(getHttpConnector());
	// return tomcat;
	// }

	@SuppressWarnings("unused")
	private Connector getHttpConnector() {
		Connector connector = new Connector("org.apache.coyote.http11.Http11NioProtocol");
		connector.setScheme("http");
		connector.setPort(5000);
		connector.setSecure(false);
		connector.setRedirectPort(5000);
		return connector;
	}
}