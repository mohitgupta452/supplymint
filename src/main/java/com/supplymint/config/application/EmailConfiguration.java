package com.supplymint.config.application;

import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

/**
 * @Author Manoj Singh
 * @Since 12-Feb-2019
 * @version 1.0
 */

@Configuration
@ConfigurationProperties("spring.mail")
@PropertySource("classpath:email.properties")
public class EmailConfiguration {

	/**
	 * Case insensitive Logger constant used to retrieve the logger for various
	 * APIs.
	 *
	 */
	private final static Logger LOG = LoggerFactory.getLogger(EmailConfiguration.class);

	private String host;
	private String username;
	private String password;

	@Autowired
	private Environment env;

	@Bean
	public JavaMailSender getJavaMailSender() {
		LOG.info("Initialized Mail sender to load default SES config properties");
		JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
		mailSender.setHost(this.getHost());
		mailSender.setPort(587);

		mailSender.setUsername(this.getUsername());
		mailSender.setPassword(this.getPassword());

		Properties props = mailSender.getJavaMailProperties();
		props.put("mail.transport.protocol", "smtp");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.debug", "true");

		return mailSender;
	}

	public String getTextMessage(String template) {
		return env.getProperty(template).replace("%;", "--");
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
