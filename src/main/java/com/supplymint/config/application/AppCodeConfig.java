package com.supplymint.config.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

/**
 * @author Manoj Singh
 * @since 17 OCT 2018
 * @version 1.0
 *
 */

@Configuration
@PropertySource("classpath:applicationcode.properties")
public class AppCodeConfig {

	@Autowired
	private Environment env;

	public String getMessage(String key) {
		return env.getProperty(key);
	}

	public boolean contains(String key) {
		return env.containsProperty(key);
	}

	public boolean equals(Object t) {
		return env.equals(t);
	}

	public String[] getActiveProfiles() {
		return env.getActiveProfiles();
	}

}
