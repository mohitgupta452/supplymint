package com.supplymint.config.security;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.security.web.header.writers.ReferrerPolicyHeaderWriter.ReferrerPolicy;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

/**
 * @Author Manoj Singh
 * @Since 21-Jan-2019
 * @version 1.0
 */

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		// Cross site request
		http.csrf().csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse()).disable();
		// Cacher Control
		http.headers().defaultsDisabled().cacheControl().disable();
		// Content Type
		http.headers().defaultsDisabled().contentTypeOptions().disable();
		// HTTP Strict Transport Security
		http.headers().httpStrictTransportSecurity().includeSubDomains(true).maxAgeInSeconds(31536000).disable();
		http.cors();
		// http.cors().disable();
		http.headers().referrerPolicy(ReferrerPolicy.SAME_ORIGIN);
		// Actuator HTTP Endpoints
		// http.requestMatcher(EndpointRequest.toAnyEndpoint()).authorizeRequests().anyRequest().permitAll();
	}

	@Bean
	CorsConfigurationSource corsConfigurationSource() {
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		CorsConfiguration config = new CorsConfiguration();
		config.setAllowCredentials(true);
		config.setAllowedOrigins(Arrays.asList("https://dev.supplymint.com", "https://app.supplymint.com",
				"http://localhost:5000", "https://quality.supplymint.com"));
		config.addAllowedHeader("*");
		config.addAllowedMethod("*");
		source.registerCorsConfiguration("/**", config);
		return source;
	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.inMemoryAuthentication().withUser("user").password(passwordEncoder().encode("password")).roles("USER")
				.and().withUser("actuator").password(passwordEncoder().encode("password")).roles("ACTUATOR").and()
				.withUser("swagger").password(passwordEncoder().encode("password")).roles("SWAGGER").and()
				.withUser("admin").password(passwordEncoder().encode("password"))
				.roles("USER", "ADMIN", "ACTUATOR", "SWAGGER");
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

}