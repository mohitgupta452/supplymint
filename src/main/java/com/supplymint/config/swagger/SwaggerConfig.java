package com.supplymint.config.swagger;

import java.util.Collections;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.google.common.base.Predicates;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @Authhor Prabhakar Srivastava
 * @Date 06-Jun-2019
 * @Version 1.0
 */

@SuppressWarnings("deprecation")
@Configuration
@EnableSwagger2
public class SwaggerConfig extends WebMvcConfigurerAdapter {

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				//.apis(RequestHandlerSelectors.any())
				.apis(Predicates.not(RequestHandlerSelectors.basePackage("org.springframework.boot")))
//				.paths(PathSelectors.any())
				.build()
				.apiInfo(getApiInformation());
	}
	
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");
		registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
	}

    private ApiInfo getApiInformation(){
        return new ApiInfo("Supplymint" /*title*/,
                "choose better get Smarter!!" /*description*/,
                "1.0"/*version*/,
                "Term and Condition apply!!" /*term of services*/,
                new Contact("Sumeet Agarwal" /*name*/, "https://www.supplymint.com/#/"/*url*/, "info@turningcloud.com"/*email*/),
                "Turning Cloud Solutions pvt. ltd." /*license name*/,
                "https://www.supplymint.com/#/" /*license url */,
                Collections.emptyList()
                );
    }
}
