package com.supplymint.exception;

import java.lang.invoke.MethodHandles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Manoj Singh
 * @since 18 OCT 2018
 * @version 1.0
 */

public class SupplyMintException extends Exception {

	private static final long serialVersionUID = 651108797057643815L;

	protected static final  String CLASS_NAME = MethodHandles.lookup().lookupClass().toString();

	protected static final Logger LOGGER = LoggerFactory.getLogger(CLASS_NAME);

	public SupplyMintException() {
		super();
	}

	public SupplyMintException(String message) {
		super(message);
	}

	public SupplyMintException(Throwable cause) {
		super(cause);
	}

	public SupplyMintException(String message, Throwable cause) {
		super(message, cause);
	}

}
