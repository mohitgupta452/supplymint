package com.supplymint.exception;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

/**
 * @author Manoj Singh
 * @since 18 OCT 2018
 * @version 1.0
 */

//	@ControllerAdvice
public class RestExceptionHandler {

	private static final String UNEXPECTED_ERROR = "Exception.unexpected";

	private final MessageSource messageSource;

	@Autowired
	public RestExceptionHandler(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	/*
	 * @ExceptionHandler(RestException.class) public ResponseEntity<RestMessage>
	 * handleIllegalArgument(RestException ex, Locale locale) { String errorMessage
	 * = messageSource.getMessage(ex.getMessage(), ex.getArgs(), locale); return new
	 * ResponseEntity<>(new RestMessage(errorMessage), HttpStatus.BAD_REQUEST); }
	 * 
	 * @ExceptionHandler(MethodArgumentNotValidException.class) public
	 * ResponseEntity<RestMessage>
	 * handleArgumentNotValidException(MethodArgumentNotValidException ex, Locale
	 * locale) { BindingResult result = ex.getBindingResult(); List<String>
	 * errorMessages = result.getAllErrors() .stream() .map(objectError ->
	 * messageSource.getMessage(objectError, locale)) .collect(Collectors.toList());
	 * return new ResponseEntity<>(new RestMessage(errorMessages),
	 * HttpStatus.BAD_REQUEST); }
	 * 
	 * @ExceptionHandler(Exception.class) public ResponseEntity<RestMessage>
	 * handleExceptions(Exception ex, Locale locale) { String errorMessage =
	 * messageSource.getMessage(UNEXPECTED_ERROR, null, locale);
	 * ex.printStackTrace(); return new ResponseEntity<>(new
	 * RestMessage(errorMessage), HttpStatus.INTERNAL_SERVER_ERROR); }
	 */
}
