package com.supplymint.exception;

/**
 * 
 * @author Ashish Singh Dev && Manoj Singh
 * @version 1.0
 * @since Sept 14, 2018
 *
 */
public class StatusCodes {

	public static interface FileStatusCode {
		String FILE_SUCCESS = "SUCCESS";
		String FILE_INPROGRESS = "INPROGRESS";
		String FILE_FAILED = "Failed";

		String ALLOCATION_NOT_FOUND = "No Data Allocation Found";
	}

	public static interface AppStatusCodes {
		String GENERIC_SUCCESS_CODE = "2000";
		String GENERIC_CLIENT_ERROR_CODE = "4000";
		String GENERIC_SECURITY_ERROR_CODE = "4010";
		String GENERIC_SERVER_ERROR_CODE = "5000";
		String GENERIC_CONNECTION_ERROR_CODE = "5000";
		String GENERIC_API_NOT_FOUND = "4040";
		String GENERIC_PROCESSING_CODE = "1020";

	}

	public static interface AppErrorCodes {

		String GENERIC_EVENT_NOT_AVAILABLE_ERROR_CODE = "9999";

	}

	public static interface AppSecurityErrorCodes {
		String TOKEN_EXPIRED = "4011";
		String TOKEN_TAMPERED = "4012";
		String TOKEN_NOT_ACTIVE = "4013";

	}

	public static interface Success {
		String CREATED = "2001";
		String ACCEPTED = "2002";
		String NON_AUTHORISED_INFORMATION = "2003";
		String NO_CONTENT = "2004";
		String RESET_CONTENT = "2005";
		String ALREADY_REPORTED = "2008";
	}

	public static interface Redirection {
		String FOUND = "3002";
	}

	public static interface ClientErrors {
		String BAD_REQUEST = "4000"; // The server cannot or will not process the request due to invalid request.
		String UNAUTHORIZED = "4001"; // Authentication is required and has failed or has not yet been provided.
		String FORBIDDEN = "4003"; // The request was valid, but the server is refusing action.
		String METHOD_NOT_ALLOWED = "4005";
		String TIMEOUT = "4008";
		String PAYLOAD_TOO_LARGE = "4013";
		String UNSUPPORTED_MEDIA_TYPE = "4015";
		String TOO_MANY_REQUESTS = "4029";
		String NOT_FOUND = "4040";

	}

	public static interface AppModuleErrors {
		String ORG_CREATE_ERROR = "ADMORGXXX1";
		String ORG_READER_ERROR = "ADMORGXXX2";
		String ORG_UPDATE_ERROR = "ADMORGXXX3";
		String ORG_DELETE_ERROR = "ADMORGXXX4";

		String SITE_CREATOR_ERROR = "ADMSITXXX1";
		String SITE_READER_ERROR = "ADMSITXXX2";
		String SITE_UPDATE_ERROR = "ADMSITXXX3";
		String SITE_DELETE_ERROR = "ADMSITXXX4";

		String SITE_DETAIL_CREATOR_ERROR = "ADMSITSDM1";
		String SITE_DETAIL_READER_ERROR = "ADMSITSDM2";
		String SITE_DETAIL_UPDATE_ERROR = "ADMSITSDM3";
		String SITE_DETAIL_DELETE_ERROR = "ADMSITSDM4";

		String USER_CREATOR_ERROR = "ADMUSRXXX1";
		String USER_READER_ERROR = "ADMUSRXXX2";
		String USER_UPDATE_ERROR = "ADMUSRXXX3";
		String USER_DELETE_ERROR = "ADMUSRXXX4";

		String USER_ROLE_CREATOR_ERROR = "ADMUSRURM1";
		String USER_ROLE_READER_ERROR = "ADMUSRURM2";
		String USER_ROLE_UPDATE_ERROR = "ADMUSRURM3";
		String USER_ROLE_DELETE_ERROR = "ADMUSRURM4";

		String SITEMAP_CREATOR_ERROR = "ADMSITMAP1";
		String SITEMAP_READER_ERROR = "ADMSITMAP2";
		String SITEMAP_UPDATE_ERROR = "ADMSITMAP3";
		String SITEMAP_DELETE_ERROR = "ADMSITMAP4";

		String VENDOR_CREATOR_ERROR = "VENXXX1";
		String VENDOR_READER_ERROR = "VENXXX2";
		String VENDOR_UPDATE_ERROR = "VENXXX3";
		String VENDOR_DELETE_ERROR = "VENXXX4";

		String MCSTR_CREATOR_ERROR = "MCSXXX1";
		String MCSTR_DELETE_ERROR = "MCSXXX4";

		String ALLOCATION_CREATOR_ERROR = "ALLXXX1";
		String ALLOCATION_DELETE_ERROR = "ALLXXX4";

		String RULE_ENGINE_CREATOR_ERROR = "RENXXX1";
		String RULE_ENGINE_READER_ERROR = "RENXXX2";
		String RULE_ENGINE_UPDATE_ERROR = "RENXXX3";
		String RULE_ENGINE_DELETE_ERROR = "RENXXX4";

		String HIERARCHY_LEVEL_READER_ERROR = "PROPINHIE2";
		String TRANSPORTER_READER_ERROR = "PROPINTRA2";
		String SUPPLIER_READER_ERROR = "PROPINSUP2";
		String ARTICLE_READER_ERROR = "PROPINART2";
		String PI_VALIDATION_ERROR = "PROPIVALI2";

		String LEADTIME_READER_ERROR = "PROPINLEA2";

		String PURCHASE_TERM_READER__ERROR = "PURINDPUR2";

		String PI_CREATOR_ERROR = "PROPINXXX1";
		String PI_PDF_CREATOR_ERROR = "PROPINPDF1";
		String PI_LINE_ITEM_ERROR = "PROPINLIT2";
		String PI_READER_ERROR = "PURINDXXX2";
		String PI_MARGIN_RULE_READER_ERROR = "PURINDMRR2";
		String PI_OTB_READER_ERROR = "PURINDOTB2";
		String PI_SMPARTNER_OTB_READER_ERROR = "PROSMPOTB2";
		String PI_IMAGE_UPLOAD_ERROR = "PURINDIMG2";

		String PO_CREATOR_ERROR = "PROPORXXX1";

		String PO_AUDIT_CREATOR_ERROR = "PROPORAUD1";

		String PO_ITEM_READER_ERROR = "PURORDITM2";

		String INVITEM_CREATOR_ERROR = "PROPININV1";
		String PI_HISTORY_ERROR = "PROPINHIS2";
		String PI_PDF_READER_ERROR = "PURINDPDF2";
		String PI_UPDATE_ERROR = "PURINDXXX3";
		String PI_COLOR_CREATOR_ERROR = "PROPINCOL1";
		String PI_BRAND_CREATOR_ERROR = "PROPINBRA1";
		String PI_SIZE_CREATOR_ERROR = "PROPINSIZ1";
		String PI_UNIQUE_CODE_CREATOR_ERROR = "PROPINUNI1";

		String CUSTOM_CREATOR_ERROR = "CUSTXXX1";
		String CUSTOM_READER_ERROR = "CUSTXXX2";
		String CUSTOM_UPDATE_ERROR = "CUSTXXX3";

		String ASSORTMENT_CREATOR_ERROR = "ASRTMXXX1";
		String ASSORTMENT_READER_ERROR = "ASRTMXXX2";
		String ASSORTMENT_UPDATE_ERROR = "ASRTMXXX3";
		String ASSORTMENT_DELETE_ERROR = "ASRTMXXX4";

		String ADHOC_CREATOR_ERROR = "INVADHXXX1";
		String ADHOC_READER_ERROR = "INVADHXXX2";
		String ADHOC_UPDATE_ERROR = "INVADHXXX3";
		String ADHOC_DELETE_ERROR = "INVADHXXX4";
		String ADHOC_READ_SITE_ERROR = "INVADHSIT2";
		String ADHOC_READ_ICODE_ERROR = "INVADHITM2";

		String PO_GETALL_PATTERN_ERROR = "PURORDGAP2";
		String PO_GET_PI_DATA = "POGETPIDATA2";
		String PO_UDF_MAPPING = "POUDFXXX2";
		String PO_INV_UDF_MAPPING = "POINVUDFXXX2";
		String PO_HISTORY = "POHISXXX2";
		String PO_SET_UDF_SETTING_UPDATE_ERROR = "PROPORUDF3";
		String INDT_CAT_DESC_UDF_LIST = "PROPORCDS2";
		String DEP_ITEM_UDF_MAPPING = "PROPORITM2";
		String ITEM_UDF_MAPPING_ARTICLE = "PROPORIMA2";

		String PROCUREMENT_DEPT_SIZE = "PROPORDEP2";

		String DEMAND_PLANNING_CREATOR_ERROR = "DMDPLNXXX1";
		String DEMAND_PLANNING_READER_ERROR = "DMDPLNXXX2";
		String DEMAND_PLANNING_UPDATE_ERROR = "DMDPLNXXX3";
		String DEMAND_PLANNING_DELETE_ERROR = "DMDPLNXXX4";

		String DEMAND_BUDGETED_SALES_CREATOR_ERROR = "DMPBUGXXX1";
		String DEMAND_BUDGETED_SALES_READER_ERROR = "DMPBUGXXX2";
		String DEMAND_BUDGETED_SALES_EXCEL_READER_ERROR = "DMPBUGEXC2";

		String EMAIL_CONFIG_CREATOR_ERROR = "NOTEMLXXX1";
		String EMAIL_CONFIG_READER_ERROR = "NOTEMLXXX2";
		String EMAIL_CONFIG_UPDATE_ERROR = "NOTEMLXXX3";
		String EMAIL_CONFIG_DELETE_ERROR = "NOTEMLXXX4";

		String GENERAL_SETTING_CREATOR_ERROR = "GENSETXXX1";
		String GENERAL_SETTING_READER_ERROR = "GENSETXXX2";
		String GENERAL_SETTING_UPDATE_ERROR = "GENSETXXX3";
		String GENERAL_SETTING_DELETE_ERROR = "GENSETXXX4";

		String FESTIVAL_SETTING_CREATOR_ERROR = "FEVSETXXX1";
		String FESTIVAL_SETTING_READER_ERROR = "FEVSETXXX2";
		String FESTIVAL_SETTING_UPDATE_ERROR = "FEVSETXXX3";
		String FESTIVAL_SETTING_DELETE_ERROR = "FEVSETXXX4";
		////
		String OTB_SETTING_CREATOR_ERROR = "OTBSETXXX1";
		String OTB_SETTING_READER_ERROR = "OTBSETXXX2";
		String OTB_SETTING_UPDATE_ERROR = "OTBSETXXX3";
		String OTB_SETTING_DELETE_ERROR = "OTBSETXXX4";
		////
		String OTB_CREATOR_ERROR = "DMDPLNOTB1";
		String OTB_READER_ERROR = "DMDPLNOTB2";
		String OTB_UPDATE_ERROR = "DMDPLNOTB3";
		String OTB_DELETE_ERROR = "DMDPLNOTB4";

		String PO_SIZE_MAPPING_UPDATE_ERROR = "PROPORDSM3";

		String SYSTEM_DEFAULT_CONFIG_ERROR = "PROPINSDC2";

		String ITEM_CAT_DESC_UPDATE = "PROPORICD3";

		String INSERT_ITEM_UDF_MAPPING = "PROPORIUM1";
		String UPDATE_ITEM_UDF_MAPPING = "PROPORIUM3";

		String INSERT_SET_UDF_SETTING = "PROPORSUS1";
		String UPDATE_SET_UDF_SETTING = "PROPORSUS3";
		String GET_PO_ITEM_UDF_MAPPING_CAT_DESCRIPTION = "PROPORCUD2";
		String FORECAST_AUTOCONFIG_CREATOR_ERROR = "DMPFACXXX1";

		String DASHBOARD_READER_ERROR = "DASXXXXXX2";

		String SESSION_DETAIL_CREATOR_ERROR = "SESDETXXXX1";
		String SESSION_DETAIL_READER_ERROR = "SESDETXXXX2";
		String ANALYTIC_READER_ERROR = "ANYSTPXXXX2";

		String CAT_DESC_HEADER = "PROPINCDH2";
		String HEADER_CONFIG_CREATE_ERROR = "HEDCFGXXX1";
		String HEADER_CONFIG_READER_ERROR = "HEDCFGXXX2";
		String HEADER_CONFIG_UPDATE_ERROR = "HEDCFGXXX3";
		String HEADER_CONFIG_DELETE_ERROR = "HEDCFGXXX4";

		String PO_DESC6_READER_ERROR = "PROPORDS62";

		String PO_DEPT_READER_ERROR = "PROPORDEP2";

		String PO_SUPPLIER_READER_ERROR = "PROPORSUP2";

		String PO_ORDERNO_READER_ERROR = "PROPORORD2";

		String PI_HSN_CODE_ERROR = "PROPIDHSN2";

		String PI_SMPARTNER_HSN_CODE_ERROR = "PROSMPHSN2";

		String FMCG_READER_ERROR = "PROFMGXXX2";
		String FMCG_DOWNLOAD_ERROR_FILE = "PROFMGXXX2";

	}

	public static interface ClientErrorMessage {
		String BAD_REQUEST = "The server cannot or will not process the request due to invalid request ";
		String UNAUTHORIZED = "Authentication is required and has failed or has not yet been provided ";
		String FORBIDDEN = "The request was valid, but the server is refusing action";
		String METHOD_NOT_ALLOWED = "";
		String TIMEOUT = "";
		String PAYLOAD_TOO_LARGE = "";
		String UNSUPPORTED_MEDIA_TYPE = "";
		String TOO_MANY_REQUESTS = "";

	}

	public static interface ServerErrors {
		String INTERNAL_SERVER_ERROR = "5000";
		String NOT_IMPLEMENTED = "5001";
		String BAD_GATEWAY = "5002";
		String SERVICE_UNAVAILABLE = "5003"; // The server is currently unavailable
		String GATEWAY_TIMEOUT = "5004";// The server was acting as a gateway or proxy and did not
		// receive a timely response
		String NETWORK_AUTHENTICATION_REQUIRED = "5011"; // The client needs to authenticate to gain network access.

	}

	public static interface ServerErrorMessage {
		String INTERNAL_SERVER_ERROR = "";
		String NOT_IMPLEMENTED = "";
		String BAD_GATEWAY = "Invalid response from the upstream server";
		String SERVICE_UNAVAILABLE = "The server is currently unavailable";
		String GATEWAY_TIMEOUT = "";
		String NETWORK_AUTHENTICATION_REQUIRED = ""; //

	}

	public static interface AppModuleMsg {
		String PI_SUCCESS = "Purchase indent saved successfully having purchase indent ID:";
		String PO_SUCCESS = "Purchase order saved successfully having purchase order number:";
		String PO_UDFSETTING_UPDATE = "Udf setting updated successfully";
		String PO_SIZEMAPPING_UPDATE = "Department size mapping updated successfully";
		String PO_SETUDFMAPPING_UPDATE = "Udf mapping updated successfully";
		String PO_SETUDFMAPPING_INSERT = "Udf mapping saved successfully";
		String PO_ITEMUDFSETTING_UPDATE = "Item udf setting updated successfully";
		String PO_ITEMUDFMAPPING_INSERT = "Item udf mapping saved successfully";
		String PO_ITEMUDFMAPPING_UPDATE = "Item udf mapping updated successfully";
		String PI_CLIENT_ERROR = "Items are not found";
		String PI_CAT_DESC_VALIDATION = "Unable to process your request, Please try with valid inputs";
		String FESTIVAL_SETTING_UPDATION = "Custom festival list updated successfully!!";

	}

	public static interface DashboardMsg {
		String TOTAL_SALES = "totalSales";
		String TOTAL_UNITS = "totalUnits";
		String TOTAL_AVGSALESUNIT = "avgSalesUnit";
		String TOTAL_AVGSALESASSORTMENT = "avgSalesAssortment";

		String TOPSTORES = "topStores";
		String TOPARTICLES = "topArticles";
		String BOTTOMSTORES = "bottomStores";
		String BOTTOMARTICLES = "bottomArticles";

		String STORES = "stores";
		String STORES_NAME = "storeName";
		String ARTICLES_NAME = "articleName";
		String LASTYEAR = "lastYear";
		String LASTYEARSIGN = "lastYearSign";
		String THISYEAR = "thisYear";
		String THISYEARSIGN = "thisYearSign";
		String LASTMONTH = "lastMonth";
		String LASTMONTHSIGN = "lastMonthSign";
		String PREVYEARLASTMONTH = "prevYearLastMonth";
		String PREVYEARLASTMONTHSIGN = "prevYearLastMonthSign";

		String CURRENTMONTH = "currentMonth";
		String LASTMONTHS = "lastMonth";
		String LASTTHREEMONTH = "lastThreeMonth";
		String LASTSIXMONTHS = "lastSixMonth";
		String LASTTWELFTHMONTH = "lastTwelfthMonth";
		String LASTFINANCIALYEAR = "lastFinancialYear";

		String SALESTREND = "salesTrend";

		String UNITSALES = "unitSales";
		String SALESVALUE = "salesValue";
		String YEARS = "years";

		String ARTICLECODE = "articleCode";
		String ARTICLENAME = "articleName";
		String THIRTYDAYS = "thirtyDays";
		String THIRTYDAYSSIGN = "thirtyDaysSign";
		String FOURTYFIVEDAYS = "fourtyFiveDays";
		String FOURTYFIVEDAYSSIGN = "fourtyFiveDaysSign";
		String NINETYDAYS = "ninetyDays";
		String NINETYDAYSSIGN = "ninetyDaysSign";
		String DAYSSIGN = "daysSign";
		String DAYS = "days";
		String THIRTY = "thirty";
		String FOURTY = "fourty";
		String NINETY = "ninety";
		String SLOWMOVING = "slowMoving";
		String FASTMOVING = "fastMoving";

		String TOPTHREE = "topThree";
		String TOPFIVE = "topFive";
		String TOPTEN = "topTen";

		String TOTALSALES_LASTTHREEMONTHS = "TOTALSALES_LASTTHREEMONTHS";

		String TOTALSALES_LASTSIXMONTHS = "TOTALSALES_LASTSIXMONTHS";

		String TOTALUNITS_LASTTHREEMONTHS = "TOTALUNITS_LASTTHREEMONTHS";

		String TOTALUNITS_LASTSIXMONTHS = "TOTALUNITS_LASTSIXMONTHS";

		String TOTALSALES_LASTFINANCIALYEAR = "TOTALSALES_LASTFINANCIALYEAR";

		String TOTALSALES_LASTTWELFTHMONTHS = "TOTALSALES_LASTTWELFTHMONTHS";

		String TOTALUNITS_LASTFINANCIALYEAR = "TOTALUNITS_LASTFINANCIALYEAR";

		String TOTALUNITS_LASTTWELFTHMONTHS = "TOTALUNITS_LASTTWELFTHMONTHS";

		String TOTALUNITS_CURRENTMONTH = "TOTALUNITS_CURRENTMONTH";

		String TOTALUNITS_LASTMONTH = "TOTALUNITS_LASTMONTH";

		String TOTALSALES_CURRENTMONTH = "TOTALSALES_CURRENTMONTH";

		String TOTALSALES_LASTMONTH = "TOTALSALES_LASTMONTH";
	}

	public static interface OtbMsg {
		String PLAN_SALES = "Planned Sales";
		String PLAN_MARKDOWN = "Planned MarkDown";
		String PLAN_CLOSING_INVENTORY = "Planned Closing Inventory";
		String PLAN_OPENING_STOCK = "Planned Opening Stock";
		String PLAN_PURCHASE_ORDER = "Planned Purchase Order";
		String PLAN_OTB_VALUE = "Planned OTB Value";
	}

	public static interface AppStatusMsg {
		String BLANK = "No Data Found";
		String MISMATCH = "Found Mismatch Item Combination";
		String TOKEN_EXPIRED = "Token expired unable to process your request .Please try again !!";
		String NUMBER_FORMAT_MSG = "Unable to process your request, Please try with valid ID";
		String FAILURE_MSG = "Unable to process your request, Please check error for more information";
		String SEARCH_NOT_AVAILABLE_MSG = "Search key is not available";
		String FAILURE_JOB_MSG = "Unable to process your request, Please try again";
		String NO_LOCATION_FOUND = "No Location Found";
		String NO_USER_FOUND = "No User Found";
		String MSG_INCORRECT_LOGIN = "Invalid username and password. Please try again !!";
		String MSG_INCORRECT_SIGNIN = "Invalid credentials details . Please try again !!";
		String MSG_INCORRECT_CREDENTIALS = "Login Failed. Incorrect user id and credential combination";
		String INVALID_JSON = "Invalid JSON structure. Please check the request payload";
		String SUCCESS_MSG = "Request completed successfully";
		String EXCEL_UPLOAD = "uploaded successfully";
		String UPLOAD = "uploaded successfully";
		String FILE_UPLOAD = "File uploading in progress";
		String EXCEL_DOWNLOAD = "%s downloaded successfully";
		String CHANGE_PASSWORD_MSG = "Password has been changed to new password. Please use new password for sign in next time";
		String SUCCESS_DELETE = "Records has been deleted successfully.";
		String EXIST_USER = "Username already exists";
		String EXIST_USER_EMAIL = "Email already exist";
		String EXIST_USER_MOBILE = "Mobile Number already exist";
		String EXIST_VENDOR = "VendorName/VendorCode already exist";
		String EXIST_SITEMAP = "Resource already exist";
		String EXIST_SITE = "Resource already exist";
		String EXIST_ORGANISATION_EMAIL = "Email already exist";
		String EXIST_ORGANISATION_MOBILE = "Mobile Number already exist";
		String EXIST_SITE_DETAIL_EMAIL = "Email already exist";
		String EXIST_SITE_DETAIL_MOBILE = "Mobile Number already exist";
		String CONCURRENT_EXIST = "JobName is already in queue";
		String ENTITY_NOT_FOUND = "Job run with job name not found";
		String EXIST_JOB = "Job Name already created";
		String EXIST_TRIGGER = " Trigger with name already submitted with different configuration";
		String TRIGGER_MSG = "Updated configuration saved successfully";
		String FILE_UPLOADED = "File upload successfully";
		String FILE_NOT_UPLOADED = "Image upload failed!";
		String JOB_FAILED = "Sorry ! No allocation file is created";
		String CONNECTION_NOT_AVAILABLE = "Network Unavailable Please try again";
		String ITEM_NOT_FOUND = "Requested Items not found";
		String FAILED_JDBC_CONNECTION = "Failed to obtain JDBC Connection";
		String PI_PDF_NOT_FOUND = "Requested PI PDF not found";
		String PROGRESS = "Progress";
		String SUCCESS = "Success";
		String EXIST_COLOR = "Color already exist";
		String EXIST_BRAND = "Brand already exist";
		String EXIST_SIZE = "Size already exist";
		String EXIST_UNIQUECODE = "UniqueCode already exist";
		String SUCCESS_USER_MSG = "User registered successfully. A verification link has been sent to the registered email account";
		String ENTERPRISE_DATA = "enterprise data not found";
		String DATA_SYNC_MSG = "Request completed successfullly!! Please check Data Sync Table for Data Processing Update";
		String ASSORTMENT_MSG = "Assortment created successfully!!";
		String ADHOC_MSG = "WorkOrder has been saved successfully having WorkOrder ID:-";
		String SUCCESS_ADHOC_STATUS_MSG = "The WorkOrder has been made";
		String DELETE_SUCCESS_MSG = "Selected Item Code deleted successfully";
		String SUCCESS_USER_STATUS_MSG = "The user has been made";
		String PI_APPROVED = "Purchase Indent approved successfully";
		String PI_REJECTED = "Purchase Indent rejected successfully";
		String PI_INPUTS = "Division, Section, Department, Supplier, PI Amount or PI Quantity can't be null, Please try with valid inputs";
		String PO_INPUTS = "Order Date, Valid_from date, Valid_to date or article can't be can't be null, Please try with valid inputs";
		String PROCESS = "Processing";
		String AUTH_USER_EXIST = "UserName already exits!!";
		String INVALID_EXCEL_FILE = "Uploaded file is invalid";
		String CANCEL_WORK_ORDER_SUCCESS_MSG = "Work order cancelled successfully!!";
		String SINGLE_ITEM_REMOVED_SUCCESS_MSG = "Item code removed successfully !!";
		String ALL_ITEM_REMOVED_SUCCESS_MSG = "Work order removed successfully !!";

		String UNIQUE_CODE_VALIDATION_MSG = "This uniquecode is already exist";
		String BRAND_VALIDATION_MSG = "This brand is already exist";
		String PATTERN_VALIDATION_MSG = "This pattern is already exist";
		String ASSORTMENT_VALIDATION_MSG = "This assortment is already exist";
		String SEASONS_VALIDATION_MSG = "This seasons is already exist";
		String FABRIC_VALIDATION_MSG = "This fabric is already exist";
		String DARWIN_VALIDATION_MSG = "This darwin is already exist";
		String WEAVED_VALIDATION_MSG = "this weaved is already exist";
		String SIZE_VALIDATION_MSG = "This size is already exist";
		String COLOR_VALIDATION_MSG = "This color is already exist";
		String DEPARTMENT_VALIDATION_MSG = "This department is already exist";
		String FORECAST_ALREADY_QUEUE = "Forecast already in queue";
		String OTB_PLANNAME_EXIST = "OTB plan name already exist";
		String OTB_SUCCESS_MSG = "OTB plan created successfully!!";
		String FORECAST_SUCCESS_MSG = "Forecast script triggered successfully";
		String EMAIL_SUCCESS_MSG = "Email registered successfully!!";
		String USER_UPDATE_SUCCESS_MSG = "User details updated successfully";
		String USER_ROLE_UPDATE_SUCCESS_MSG = "User role updated successfully";
		String SITE_SUCCESS_MSG = "Site registered successfully";
		String ORG_SUCCESS_MSG = "Organisation added successfully";
		String ORG_UPDATE_MSG = "Organisation details updated successfully";
		String STORE_PROFILE_BLANK = "Please Enter Valid Store Code";
		String EMAIL_ENABLE_MSG = "Email notification trigger has been activated successfully!!";
		String EMAIL_DISABLE_MSG = "Email notification trigger has been disabled successfully!!";
		String EMAIL_CONFIGURE_MSG = "At least one email configuration required to enable Email Notification !!";
		String SETTING_SUCCESS_MSG = "General Settings saved successfully!!";
		String PROFILE_SUCCESS_MSG = "Profile updated successfully!!";
		String FESTIVAL_SUCCESS_MSG = "Custom festival list updated successfully!!";
		String OTB_SETTING_SUCCESS_MSG = "OTB configuration saved successfully!!";
		String CONFIG_SUCCESS_MSG = "Configuration saved successfully!!";
		String OTB_DELETE_MSG = "Plan deleted successfully!!";
		String OTB_UPDATE_MSG = "Plan updated successfully!!";
		String OTB_PLAN_ERROR = "Unable to create plan. Please try again.";
		String PI_ACTUAL_MARKUP_FAILURE = "Please provide appropriate value for rate or rsp";
		String PI_CALCULATE_QTY = "Please provide appropriate value for ratio or color or number of sets";
		String ANALYTICS_STORE_NOT_FOUND = "Store not found";
		String PI_OTB_FAILURE = "Please provide values for article or desc6 or month or year";
		String HEADER_SUCCESS_MSG = "Saved successfully!";
		String MARGIN_RULE_ERROR_MSG = "Please put valid rate because margin rule is greater than calculated margin";
		String MARGIN_RULE_EMPTY_MSG = "Margin rule is not available against article and supplier code";
	}

	public static interface AppException {
		String NUMBER_FORMAT_EXCEPTION = "Getting  'number format exceptions' Unable to process your request , Please try with valid ID";
		String EMPTY_LIST_EXCEPTION = "No element found in the list";
	}

	public static interface Pagination {
		String CURRENT_PAGE = "currPage";
		String PREVIOUS_PAGE = "prePage";
		String MAXIMUM_PAGE = "maxPage";
	}

}