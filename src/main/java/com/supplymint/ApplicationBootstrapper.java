package com.supplymint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * 
 * @author Ashish Singh Dev
 * @version 1.0
 * @since Sept 14, 2018
 *
 */

//@EnableAutoConfiguration(exclude = { DataSourceAutoConfiguration.class })
@SpringBootApplication
@ComponentScan("com.supplymint")
@EnableScheduling
//@EnableTransactionManagement
//@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class ApplicationBootstrapper {

	private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationBootstrapper.class);

	public static void main(String[] args) throws Exception {
		SpringApplication.run(ApplicationBootstrapper.class, args);
	}

//	public static void main(String[] args) throws Exception {
//		SpringApplication app = new SpringApplication(ApplicationBootstrapper.class);
//		SimpleCommandLinePropertySource source = new SimpleCommandLinePropertySource(args);
//
//		// Check if the selected profile has been set as argument.
//		// if not the development profile will be added
//		addDefaultProfile(app, source);
//		app.run(args);
//	}

	/**
	 * Set a default profile if it has not been set
	 */

//	private static void addDefaultProfile(SpringApplication app, SimpleCommandLinePropertySource source) {
//
//		if (!source.containsProperty("spring.profiles.active")) {
//			app.setAdditionalProfiles("dev");
//			LOGGER.info(String.format("Staring application with profiles: %s",
//					source.getProperty("spring.profiles.active")));
//		} else {
//			LOGGER.info("Starting application with profiles: {}", source.getProperty("spring.profiles.active"));
//		}
//	}

}
